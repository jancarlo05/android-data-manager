package com.procuro.apimmdatamanagerlib;

import java.util.Date;

/**
 * Created by jophenmarieweeks on 11/07/2017.
 */

public class DriverLogRestBreakInfoEntry extends PimmBaseObject {
    public Date Start;
    public Date End;
    public String StartText;
    public String EndText ;
    public String UnscheduledStopPOIId;
    public String AuditBy;
    public Date AuditDate;
    public DriverLogTimeInfoEntryDetail StartDetail;
    public DriverLogTimeInfoEntryDetail EndDetail;

//-(id) init;

}
