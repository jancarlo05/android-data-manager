package com.procuro.apimmdatamanagerlib;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by jophenmarieweeks on 12/07/2017.
 */

public class LTO extends PimmBaseObject {

    public String ltoId;
    public String name;
    public String userId;
    public String userName;
    public Date creationDate;
    public Date startDate;
    public Date endDate;
    public Boolean shift0;
    public Boolean shift1;
    public Boolean shift2;
    public Boolean shift3;
    public Boolean shift4;
    public Boolean shift5;
    public Boolean shift6;
    public Boolean shift7;

    public Boolean dow0;
    public Boolean dow1;
    public Boolean dow2;
    public Boolean dow3;
    public Boolean dow4;
    public Boolean dow5;
    public Boolean dow6;

    public Number lowerThreshold;
    public Number upperThreshold;

    public String category;

    public ArrayList<String> siteInfoList; //List of SiteInfo objects

    @Override
    public void setValueForKey(Object value, String key)
    {
        if(key.equalsIgnoreCase("siteInfoList"))
        {
            if(this.siteInfoList == null)
            {
                this.siteInfoList = new ArrayList<String>();
            }

            List<String> siteInfoList = (List<String>) value;
            for(String siteInfoLists : siteInfoList)
            {
                this.siteInfoList.add(siteInfoLists);
            }
        }

        else
        {
            super.setValueForKey(value, key);
        }
    }


}
