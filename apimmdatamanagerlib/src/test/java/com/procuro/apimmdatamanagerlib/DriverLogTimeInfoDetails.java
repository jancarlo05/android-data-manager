package com.procuro.apimmdatamanagerlib;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jophenmarieweeks on 11/07/2017.
 */

public class DriverLogTimeInfoDetails extends PimmBaseObject {


   public enum DriverLastStatusEnum
    {
        DriverLastStatusEnum_Undefined(0),
        DriverLastStatusEnum_StayOnDuty(1),
        DriverLastStatusEnum_GoOnBreak(2),
        DriverLastStatusEnum_GoHome(3),
        DriverLastStatusEnum_Sleeper(4);

        private int value;
        private DriverLastStatusEnum(int value)
        {
            this.value = value;
        }
    };

   public enum DriverStatusEnum {
        DriverStatusEnum_Undefined(0),
        DriverStatusEnum_OnDuty(1),
        DriverStatusEnum_OnBreak(2),
        DriverStatusEnum_Driving(3),
        DriverStatusEnum_Sleeper(4),
        DriverStatusEnum_PunchOut(5);

        private int value;
        private DriverStatusEnum(int value)
        {
            this.value = value;
        }

    };



    public  DriverLogTimeInfoEntry Punch;
    public  DriverLogTimeInfoEntry PreTrip;
    public  DriverLogTimeInfoEntry PostTrip;
    public  DriverLogTimeInfoEntry Dispatch;

    public  DriverLogTimeInfoEntry LayOver;
    public ArrayList<String> NonRouteOnDuty;

    public ArrayList<String> RestStops; //Array of DriverLogRestBreakInfoEntry objects

    public ArrayList<String> DriveSegments; //Array of DriverLogTimeInfoEntry objects

    public DriverLastStatusEnum LastStatus;
    public DriverStatusEnum DriverStatus;

    public ArrayList<String> AuditTrail;

    public ArrayList<String> PunchTimes; //DriverLogTimeInfoEntry
    public ArrayList<String> Sleeper; //DriverLogTimeInfoEntry

//-(NSTimeInterval) getTotalBreakTime;
//-(NSTimeInterval) getTotalSleeperTime;
//-(NSTimeInterval) getTotalDrivingTime;
//
//-(NSDate*) getLastOpenBreakStartTime;
//
//-(DriverLogRestBreakInfoEntry*) getLastOpenRestBreakEntry;
//
//-(NSDictionary*) dictionaryWithValuesForKeysForPrint;
//
//-(id) init;


    @Override
    public void setValueForKey(Object value, String key)
    {
        if(key.equalsIgnoreCase("NonRouteOnDuty"))
        {
            if(this.NonRouteOnDuty == null)
            {
                this.NonRouteOnDuty = new ArrayList<String>();
            }

            List<String> NonRouteOnDuty = (List<String>) value;
            for(String NonRouteOnDutyOne : NonRouteOnDuty)
            {
                this.NonRouteOnDuty.add(NonRouteOnDutyOne);
            }
        }
        else if(key.equalsIgnoreCase("RestStops"))
        {
            if(this.RestStops == null)
            {
                this.RestStops = new ArrayList<String>();
            }

            List<String> RestStops = (List<String>) value;
            for(String RestStop : RestStops)
            {
                this.RestStops.add(RestStop);
            }
        }
        else if(key.equalsIgnoreCase("DriveSegments"))
        {
            if(this.DriveSegments == null)
            {
                this.DriveSegments = new ArrayList<String>();
            }

            List<String> DriveSegments = (List<String>) value;
            for(String DriveSegment : DriveSegments)
            {
                this.DriveSegments.add(DriveSegment);
            }
        }
        else if(key.equalsIgnoreCase("AuditTrail"))
        {
            if(this.AuditTrail == null)
            {
                this.AuditTrail = new ArrayList<String>();
            }

            List<String> AuditTrail = (List<String>) value;
            for(String AuditTrails : AuditTrail)
            {
                this.AuditTrail.add(AuditTrails);
            }
        }
        else if(key.equalsIgnoreCase("PunchTimes"))
        {
            if(this.PunchTimes == null)
            {
                this.PunchTimes = new ArrayList<String>();
            }

            List<String> PunchTimes = (List<String>) value;
            for(String PunchTime : PunchTimes)
            {
                this.PunchTimes.add(PunchTime);
            }
        }
        else if(key.equalsIgnoreCase("Sleeper"))
        {
            if(this.Sleeper == null)
            {
                this.Sleeper = new ArrayList<String>();
            }

            List<String> Sleeper = (List<String>) value;
            for(String Sleepers : Sleeper)
            {
                this.Sleeper.add(Sleepers);
            }
        }
        else
        {
            super.setValueForKey(value, key);
        }
    }


}
