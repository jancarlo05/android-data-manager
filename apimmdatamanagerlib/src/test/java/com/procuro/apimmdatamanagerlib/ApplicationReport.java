package com.procuro.apimmdatamanagerlib;

/**
 * Created by jophenmarieweeks on 12/07/2017.
 */

public class ApplicationReport extends PimmBaseObject {

   // NS_ASSUME_NONNULL_BEGIN
    public String applicationReportsID;
    public String application;
    public String perspective;
    public String filter;
    public int selector;
    public int order;
    public String reportName;
    public String urlTemplate;
    public Boolean displayInShell;
    public String reportLevel;
    public String category;
    public Boolean isActive;

//    NS_ASSUME_NONNULL_END
}
