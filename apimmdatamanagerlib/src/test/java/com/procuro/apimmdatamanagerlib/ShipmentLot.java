package com.procuro.apimmdatamanagerlib;

import java.util.Date;

/**
 * Created by jophenmarieweeks on 12/07/2017.
 */

public class ShipmentLot extends PimmBaseObject {

    public String shipmentLotId;
    public String shipmentId;
    public String lotId;
    public String lotDetailId;
    public Date timestamp;
    public String userId;
    public Number binCount; //int
    public Number caseCount;
    public Number minorDefectCount;
    public Number majorDefectCount;
    public Date codeDate;

}
