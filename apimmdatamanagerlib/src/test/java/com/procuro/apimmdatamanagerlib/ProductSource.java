package com.procuro.apimmdatamanagerlib;

/**
 * Created by jophenmarieweeks on 11/07/2017.
 */

public class ProductSource extends PimmBaseObject {

    /// <summary>ProductSource info for a stop.  Intended to be used in a list, so client can send many updates in a single call (all stops on a route for example).
/// </summary>

    public String stopID;
    public Number productSource;
    /* NULL = primary container (trailer)
     * 1    = secondary container (tractor)
     * 2    = secondary container (second trailer)
     */


}
