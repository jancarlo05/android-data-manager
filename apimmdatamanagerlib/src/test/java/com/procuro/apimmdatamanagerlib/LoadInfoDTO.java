package com.procuro.apimmdatamanagerlib;

import java.util.Date;

/**
 * Created by jophenmarieweeks on 12/07/2017.
 */

public class LoadInfoDTO extends PimmBaseObject {

    public String deliveryId;
//public String freezerLoader;
    public User freezerLoader;
    public Date freezerLoadStartTime;
    public Date freezerLoadEndTime;
    public Number freezerCases; //int
    public Number freezerCubes; //int
    public Number freezerSetPoint; //double
    public Number freezerTargetTemp; //double
    public Number freezerActualTemp; //double
    public Date freezerActualTime;
    public Number freezerPrecoolTemp; //double
    public Date freezerPrecoolTime;

//public String coolerLoader;
    public User coolerLoader;
    public Date coolerLoadStartTime;
    public Date coolerLoadEndTime;
    public Number coolerCases;
    public Number coolerCubes;
    public Number coolerSetPoint;
    public Number coolerTargetTemp;
    public Number coolerActualTemp;
    public Date coolerActualTime;
    public Number coolerPrecoolTemp;
    public Date coolerPrecoolTime;

    public Number dryCases;
    public Number dryCubes;

}
