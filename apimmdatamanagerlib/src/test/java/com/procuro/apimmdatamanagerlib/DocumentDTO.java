package com.procuro.apimmdatamanagerlib;

import java.util.Date;

/**
 * Created by jophenmarieweeks on 12/07/2017.
 */

public class DocumentDTO extends PimmBaseObject {

    public String documentId;
    public String customerId;
    public String userId;
    public String name;
    public String userName;
    public String docType ;
    public int version;
    public Date creationDate;
    public String refx;
    public PimmRefTypeEnum refType;
    public String oid;

    public String category;
    public String appString;
    public String refString;
}
