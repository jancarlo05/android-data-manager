package com.procuro.apimmdatamanagerlib;

import java.util.Date;

/**
 * Created by jophenmarieweeks on 11/07/2017.
 */

public class iPimmAPILogObject extends PimmBaseObject {

    public String username;
    public Date enqueueTimestamp; // Timestamp at which request was queued in DataManager
    public Date sentTimestamp; //Timestamp at which request was sent to the server
    public Date responseTimestamp; //Timestamp at which response was received from the server
//    @property (nonatomic,strong) NSMutableURLRequest* Request;
//    @property (nonatomic,strong) NSHTTPURLResponse* Response;
//    @property (nonatomic,strong) NSError* Error;
//    @property (nonatomic,strong) NSData* ResponseData;


}
