package com.procuro.apimmdatamanagerlib;

/**
 * Created by EmmanKusumi on 7/4/17.
 */

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.Dictionary;
import java.util.HashMap;


public class User extends PimmBaseObject
{
    public enum AuthenticationStatus
    {
        AUTHENTICATED_LOCALLY,
        AUTHENTICATED_WITH_SERVER,
        AUTHENTICATION_FAILURE,
        INTERNAL_ERROR
    }

    public enum UserExitReasonEnum
    {
        UserExitReason_None(-1),
        UserExitReason_CurrentlyEmployed(0),
        UserExitReason_Fired(1),
        UserExitReason_Retired(2),
        UserExitReason_LaidOff(3),
        UserExitReason_Death(4),
        UserExitReason_Resigned(5),
        UserExitReason_EndOfContract(6),
        UserExitReason_Medical(7),
        UserExitReason_Other(100);

        private int value;
        private UserExitReasonEnum(int value)
        {
            this.value = value;
        }

        public int getValue() {

            return this.value;
        }

        public static UserExitReasonEnum setIntValue (int i) {
            for (UserExitReasonEnum type : UserExitReasonEnum.values()) {
                if (type.value == i) { return type; }
            }
            return UserExitReason_None;
        }

    }

    public String userId;
    public String username;
    public String password;
    public String firstName;
    public String lastName;

    public String address1;
    public String address2;
    public String city;
    public String state;
    public String postal;
    public String employeeID;
    public String pID;
    public String cellPhone;
    public String dayPhone;
    public String jobTitle;
    public Date hireDate;
    public Date exitDate;

    public Date DOB;
    public UserExitReasonEnum exitReason;

    public String customerName;
    public String customerId;

    public String culture;
    public String language;
    public String defaultSiteID;

    public ArrayList<String> roles;
    public ArrayList<String> rights;
    public boolean authenticated;
    public String defaultPerspective;
    public ArrayList<String> applications;
    public ArrayList<String> dcPollers;
    public ArrayList<String> suPollers;
    public int temperatureUnits;
    public int unitSystem;

    public AuthenticationStatus authenticationStatus;
    public Dictionary<String, String> properties;
    public ArrayList<String> modules;

    public String contactEmail;

    public  OutboundDCDriverPermission outboundDCDriverPermission;
    public ArrayList<OutboundDCDriverLicense> outboundDCDriverLicense;

    public String mobileCarrier;
    public Date reviewDate;
    public ArrayList<String>old_certificationList;
    public ArrayList<Certification>certificationList;
    public boolean active;


    //Dashboard Property
    public ArrayList<Award>awardList;

    @Override
    public void setValueForKey(Object value, String key) {
        super.setValueForKey(value, key);


        if (key.equalsIgnoreCase("active")){
            if (value!=null){
                if (!value.toString().equalsIgnoreCase("null")){

                    if (value instanceof Boolean){

                        this.active = (boolean) value;

                    }else if (value instanceof Integer){

                        this.active = (int) value == 1;
                    }
                }
            }
        }

        if (key.equalsIgnoreCase("awardList")){
            ArrayList<Award> items = new ArrayList<>();
            if (value!=null){
                if (!value.toString().equalsIgnoreCase("null")){
                    JSONArray jsonArray = (JSONArray) value;
                    for (int i = 0; i <jsonArray.length() ; i++) {
                        try {
                            Award award = new Award();
                            award.readFromJSONObject(jsonArray.getJSONObject(i));
                            items.add(award);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
                this.awardList = items;
            }

        }

        if (key.equalsIgnoreCase("roles")){
            if (value!=null){
                JSONArray jsonArray = (JSONArray) value;
                ArrayList<String>list = new ArrayList<>();
                for (int i = 0; i <jsonArray.length() ; i++) {
                    try {
                        String jsonObject = (String) jsonArray.get(i);
                        list.add(jsonObject);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                this.roles = list;
            }

        }

        if (key.equalsIgnoreCase("rights")){
            if (value!=null){
                JSONArray jsonArray = (JSONArray) value;
                ArrayList<String>list = new ArrayList<>();
                for (int i = 0; i <jsonArray.length() ; i++) {
                    try {
                        String jsonObject = (String) jsonArray.get(i);
                        list.add(jsonObject);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                this.rights = list;
            }

        }

        if (key.equalsIgnoreCase("applications")){
            if (value!=null){
                JSONArray jsonArray = (JSONArray) value;
                ArrayList<String>list = new ArrayList<>();
                for (int i = 0; i <jsonArray.length() ; i++) {
                    try {
                        String jsonObject = (String) jsonArray.get(i);
                        list.add(jsonObject);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                this.applications = list;
            }

        }

        if (key.equalsIgnoreCase("suPollers")){
            if (value!=null){
                JSONArray jsonArray = (JSONArray) value;
                ArrayList<String>list = new ArrayList<>();
                for (int i = 0; i <jsonArray.length() ; i++) {
                    try {
                        String jsonObject = (String) jsonArray.get(i);
                        list.add(jsonObject);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                this.suPollers = list;
            }
        }

        if (key.equalsIgnoreCase("dcPollers")){
            if (value!=null){
                JSONArray jsonArray = (JSONArray) value;
                ArrayList<String>list = new ArrayList<>();
                for (int i = 0; i <jsonArray.length() ; i++) {
                    try {
                        String jsonObject = (String) jsonArray.get(i);
                        list.add(jsonObject);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                this.dcPollers = list;
            }

        }

        if (key.equalsIgnoreCase("certificationList")){
            ArrayList<String>list = new ArrayList<>();
            ArrayList<Certification>certifications = new ArrayList<>();
            if (value!=null){
                if (value instanceof  JSONArray){
                    JSONArray jsonArray = (JSONArray) value;
                    for (int i = 0; i <jsonArray.length() ; i++) {
                        try {
                            Certification certification = new Certification();
                            certification.readFromJSONObject(jsonArray.getJSONObject(i));
                            list.add(jsonArray.getJSONObject(i).getString("Certification"));
                            certifications.add(certification);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
                else if (value instanceof JSONObject){
                    JSONObject jsonObject = (JSONObject) value;
                    Certification certification = new Certification();
                    certification.readFromJSONObject(jsonObject);
                    certifications.add(certification);
                }

            }
            this.old_certificationList = list;
            this.certificationList = certifications;
        }


        if (key.equalsIgnoreCase("exitReason")) {
            if (!value.equals(null)) {
                UserExitReasonEnum deliveryPOITypes = UserExitReasonEnum.setIntValue((int) value);
                this.exitReason = deliveryPOITypes;
            }
        }
        else if (key.equalsIgnoreCase("outboundDCDriverPermission")) {
            if (!value.equals(null)) {
                OutboundDCDriverPermission outboundDCDriver = new OutboundDCDriverPermission();
                JSONObject jsonObject = (JSONObject) value;
                outboundDCDriver.readFromJSONObject(jsonObject);
                this.outboundDCDriverPermission = outboundDCDriver;
            }
        }
        else if (key.equalsIgnoreCase("outboundDCDriverLicense")) {

                if (this.outboundDCDriverLicense == null) {
                    this.outboundDCDriverLicense = new ArrayList<OutboundDCDriverLicense>();
                }

                if (!value.equals(null)) {
                    if (value instanceof JSONArray) {

                        JSONArray arrStops = (JSONArray) value;

                        if (arrStops != null) {
                            for (int i = 0; i < arrStops.length(); i++) {
                                try {
                                    OutboundDCDriverLicense outboundDCDriverLicense = new OutboundDCDriverLicense();
                                    outboundDCDriverLicense.readFromJSONObject(arrStops.getJSONObject(i));

                                    this.outboundDCDriverLicense.add(outboundDCDriverLicense);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                    }
                }

        }

    }

    public JSONObject objectKVP() {
        JSONObject dictionary = new JSONObject();


        try {
            dictionary.put("userId", this.userId);
            dictionary.put("username", this.username);
            dictionary.put("password", this.password);
            dictionary.put("firstName", this.firstName);
            dictionary.put("lastName", this.lastName);
            dictionary.put("address1", this.address1);
            dictionary.put("address2", this.address2);
            dictionary.put("city", this.city);
            dictionary.put("state", this.state);
            dictionary.put("postal", this.postal);
            dictionary.put("employeeID", this.employeeID);
            dictionary.put("pID", this.pID);
            dictionary.put("dayPhone", this.dayPhone);
            dictionary.put("jobTitle", this.jobTitle);
            dictionary.put("hireDate", JSONDate.convertJavaDatetoJsonDate(this.hireDate));
            dictionary.put("exitDate", JSONDate.convertJavaDatetoJsonDate(this.exitDate));
            dictionary.put("DOB", JSONDate.convertJavaDatetoJsonDate(this.DOB));
            dictionary.put("exitReason", this.exitReason.getValue());
            dictionary.put("customerName", this.customerName);
            dictionary.put("customerId", this.customerId);
            dictionary.put("culture", this.culture);
            dictionary.put("language", this.language);
            dictionary.put("defaultSiteID", this.defaultSiteID);
            dictionary.put("roles", CreateJsonArray(this.roles));
            dictionary.put("rights", CreateJsonArray(this.rights));
            dictionary.put("authenticated", (this.authenticated == false) ? false : true);
            dictionary.put("defaultPerspective", this.defaultPerspective);
            dictionary.put("dcPollers", CreateJsonArray(this.dcPollers));
            dictionary.put("suPollers", CreateJsonArray(this.suPollers));
            dictionary.put("applications", CreateJsonArray(this.applications));
            dictionary.put("temperatureUnits", this.temperatureUnits);
            dictionary.put("unitSystem", this.unitSystem);
            dictionary.put("authenticationStatus", this.authenticationStatus);
            dictionary.put("modules", CreateJsonArray(this.modules));
            dictionary.put("contactEmail", this.contactEmail);
            dictionary.put("mobileCarrier", this.mobileCarrier);
            dictionary.put("reviewDate", JSONDate.convertJavaDatetoJsonDate(this.reviewDate));
            dictionary.put("certificationList", new JSONArray());
            dictionary.put("active", ConvertBoleantoInt(this.active));
            dictionary.put("cellPhone", this.cellPhone);


        }catch (Exception e){
            e.printStackTrace();
        }

        return dictionary;
    }

    public HashMap<String,Object> dictionaryWithValuesForKeys() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();

        dictionary.put("userId", this.userId);
        dictionary.put("username", this.username);
        dictionary.put("password", this.password);
        dictionary.put("firstName", this.firstName);
        dictionary.put("lastName", this.lastName);
        dictionary.put("address1", this.address1);
        dictionary.put("address2", this.address2);
        dictionary.put("city", this.city);
        dictionary.put("state", this.state);
        dictionary.put("postal", this.postal);
        dictionary.put("employeeID", this.employeeID);
        dictionary.put("pID", this.pID);
        dictionary.put("dayPhone", this.dayPhone);
        dictionary.put("jobTitle", this.jobTitle);
        dictionary.put("hireDate", JSONDate.convertJavaDatetoJsonDate(this.hireDate));
        dictionary.put("exitDate", JSONDate.convertJavaDatetoJsonDate(this.exitDate));
        dictionary.put("DOB", JSONDate.convertJavaDatetoJsonDate(this.DOB));
        dictionary.put("exitReason", this.exitReason.getValue());
        dictionary.put("customerName", this.customerName);
        dictionary.put("customerId", this.customerId);
        dictionary.put("culture", this.culture);
        dictionary.put("language", this.language);
        dictionary.put("defaultSiteID", this.defaultSiteID);
        dictionary.put("roles", CreateJsonArray(this.roles));
        dictionary.put("rights", CreateJsonArray(this.rights));
        dictionary.put("authenticated", (this.authenticated == false) ? false : true);
        dictionary.put("defaultPerspective", this.defaultPerspective);
        dictionary.put("dcPollers", CreateJsonArray(this.dcPollers));
        dictionary.put("suPollers", CreateJsonArray(this.suPollers));
        dictionary.put("applications", CreateJsonArray(this.applications));
        dictionary.put("temperatureUnits", this.temperatureUnits);
        dictionary.put("unitSystem", this.unitSystem);
        dictionary.put("authenticationStatus", this.authenticationStatus);
        dictionary.put("modules", CreateJsonArray(this.modules));
        dictionary.put("contactEmail", this.contactEmail);
        dictionary.put("mobileCarrier", this.mobileCarrier);
        dictionary.put("reviewDate", JSONDate.convertJavaDatetoJsonDate(this.reviewDate));
        dictionary.put("certificationList", new ArrayList<>());
        dictionary.put("active", ConvertBoleantoInt(this.active));
        dictionary.put("cellPhone", this.cellPhone);


//        if(reviewDate != null)
//        {
//            String jsonHireDate = DateTimeFormat.convertToJsonDateTime(reviewDate);
//            dictionary.put("reviewDate", jsonHireDate);
//
//        } else {
//            dictionary.put("reviewDate", null);
//        }
//
//
//        int exitReasonVal= exitReason.getValue();
//
//        if(exitReasonVal < 0) {
//            exitReasonVal = 0;
//        }
//        dictionary.put("exitReason", exitReasonVal);
//
//        if(this.properties != null){
//            dictionary.put("properties",this.properties);
//        }
//
//        if(hireDate != null)
//        {
//            String jsonHireDate = DateTimeFormat.convertToJsonDateTime(hireDate);
//            dictionary.put("hireDate", jsonHireDate);
//
//        } else {
//            dictionary.put("hireDate", null);
//        }
//
//         if(exitDate != null)
//        {
//            String jsonExitDate = DateTimeFormat.convertToJsonDateTime(exitDate);
//            dictionary.put("exitDate", jsonExitDate);
//
//        } else {
//            dictionary.put("exitDate", null);
//        }
//
//       if(DOB != null)
//        {
//            String jsonDOB = DateTimeFormat.convertToJsonDateTime(DOB);
//            dictionary.put("DOB", jsonDOB);
//
//        } else {
//            dictionary.put("DOB", null);
//        }

        return dictionary;
    }

    public String getFullName(){

        StringBuilder driverName = new StringBuilder();

        if(this.firstName != null )
        {
            driverName.append(this.firstName);
            driverName.append(" ");

        }

        if(this.lastName != null)
        {
            driverName.append(this.lastName);
        }

        return driverName.toString();
    }


    private JSONArray CreateJsonArray(ArrayList<String> arrayList){
        JSONArray array = new JSONArray();
        if (arrayList!=null){
         for (String string : arrayList){
             array.put(string);
         }

        }
        return array;
    }

    private int ConvertBoleantoInt(boolean status){
        int data = 0;

        if (status){
            data = 1;
        }
        return data;
    }



}
