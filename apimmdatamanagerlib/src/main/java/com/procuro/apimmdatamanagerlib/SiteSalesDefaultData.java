package com.procuro.apimmdatamanagerlib;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class SiteSalesDefaultData extends  PimmBaseObject {

   public String startTime;
   public String endTime;
   public ArrayList<SiteSalesForecastTime>forecast;


   @Override
   public void setValueForKey(Object value, String key) {
      super.setValueForKey(value, key);

      if (key.equalsIgnoreCase("forecast")){
         ArrayList<SiteSalesForecastTime>list = new ArrayList<>();
         if (value!=null){
            JSONArray jsonArray = (JSONArray) value;
            for (int i = 0; i <jsonArray.length() ; i++) {
               try {
                  JSONObject jsonObject = jsonArray.getJSONObject(i);
                  SiteSalesForecastTime item = new SiteSalesForecastTime();
                  item.readFromJSONObject(jsonObject);
                  list.add(item);
               }catch (Exception e){
                  e.printStackTrace();
               }
            }
         }
         this.forecast = list;
      }
   }
}
