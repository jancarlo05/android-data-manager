package com.procuro.apimmdatamanagerlib;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by jophenmarieweeks on 07/07/2017.
 */

public class QualificationAttachment extends PimmBaseObject {

     public String AttachmentID;
     public String QualificationID;
     public String AttachmentType;
     public String AttachmentName;
     public Date CreationDate;
     public String Oid;
    //@property (nonatomic,retain) NSData* AttachmentContent;
     public Object AttachmentContent;

     public ArrayList<String> Body;
//     @Override
//     public void setValueForKey(Object value, String key) {
//
//          if (key.equalsIgnoreCase("Body")) {
//               if (this.Body == null) {
//                    this.Body = new ArrayList<String>();
//               }
//
//               List<String> Body = (List<String>) value;
//               for (String values : Body) {
//                    this.Body.add(values);
//               }
//          } else {
//               super.setValueForKey(value, key);
//          }
//     }
}
