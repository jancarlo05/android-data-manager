package com.procuro.apimmdatamanagerlib;

/**
 * Created by jophenmarieweeks on 06/07/2017.
 */

public class BorderData extends PimmBaseObject {

    public String DeviceId;
    public String DriverId;
    public String Entering;
    public String Leaving;

    public int Odometer;
    public int TimestampTicks;
}
