package com.procuro.apimmdatamanagerlib;

import org.json.JSONObject;

public class FSLAggregateSiteData extends PimmBaseObject {

    public String siteID;
    public String name;
    public FSLCompliance compliance;
    public FSLTemperature temperature;


    @Override
    public void setValueForKey(Object value, String key) {
        super.setValueForKey(value, key);

        if (key.equalsIgnoreCase("compliance")){
            if (value!=null){
                try {
                    JSONObject jsonObject = (JSONObject) value;
                    FSLCompliance data = new FSLCompliance();
                    data.readFromJSONObject(jsonObject);
                    this.compliance = data;
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
        else if (key.equalsIgnoreCase("temperature")){
            if (value!=null){
                try {
                    JSONObject jsonObject = (JSONObject) value;
                    FSLTemperature data = new FSLTemperature();
                    data.readFromJSONObject(jsonObject);
                    this.temperature = data;
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
    }
}
