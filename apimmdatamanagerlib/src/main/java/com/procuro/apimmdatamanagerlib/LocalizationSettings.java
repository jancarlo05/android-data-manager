package com.procuro.apimmdatamanagerlib;

/**
 * Created by jophenmarieweeks on 10/07/2017.
 */

public class LocalizationSettings extends PimmBaseObject {

    public enum UnitSystemEnum {
        UnitSystem_Undefined(-1),
        UnitSystem_USCustomary(0),
        UnitSystem_Metric(1);

        private int value;
        private UnitSystemEnum(int value)
        {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }
        public static UnitSystemEnum setIntValue (int i) {
            for (UnitSystemEnum type : UnitSystemEnum.values()) {
                if (type.value == i) { return type; }
            }
            return UnitSystem_Undefined;
        }
    }



   public enum TemperatureUnitEnum
    {
        TemperatureUnit_Undefined(-1),
        TemperatureUnit_Fahrenheit(0),
        TemperatureUnit_Celsius(1);

        private int value;
        private TemperatureUnitEnum(int value)
        {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }

        public static TemperatureUnitEnum setIntValue (int i) {
            for (TemperatureUnitEnum type : TemperatureUnitEnum.values()) {
                if (type.value == i) { return type; }
            }
            return TemperatureUnit_Undefined;
        }
    }


    public String Id;
    public String ObjectId;
    public String LocalizationId;
    public String Culture;
    public UnitSystemEnum UnitSystem;
    public TemperatureUnitEnum TemperatureUnits;

    @Override
    public void setValueForKey(Object value, String key) {

        if(key.equalsIgnoreCase("UnitSystem")) {
            if (!value.equals(null)) {
                UnitSystemEnum unitSystem = UnitSystemEnum.setIntValue((int) value);
                this.UnitSystem = unitSystem;
            }

        }else if(key.equalsIgnoreCase("TemperatureUnits")){
            if (!value.equals(null)) {
                TemperatureUnitEnum temperatureUnitEnum = TemperatureUnitEnum.setIntValue((int) value);
                this.TemperatureUnits = temperatureUnitEnum;
            }

        } else {
            super.setValueForKey(value, key);
        }
    }




}
