package com.procuro.apimmdatamanagerlib;

import java.util.HashMap;

/**
 * Created by jophenmarieweeks on 05/07/2017.
 */
public class SiteContact extends PimmBaseObject {
    public String Name;
    public String PhoneNumber;
    public String Email;


    public HashMap<String,Object> dictionaryWithValuesForKeys() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();

        dictionary.put("Name", this.Name);
        dictionary.put("PhoneNumber", this.PhoneNumber);
        dictionary.put("Email", this.Email);

        return dictionary;
    }
}
