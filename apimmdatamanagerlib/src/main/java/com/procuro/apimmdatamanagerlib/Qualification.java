package com.procuro.apimmdatamanagerlib;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by jophenmarieweeks on 07/07/2017.
 */

public class Qualification extends PimmBaseObject {

    public enum QualificationStatusEnum
    {
        QualificationStatus_Requested(0),
        QualificationStatus_Submitted(1),
        QualificationStatus_Pending(2),
        QualificationStatus_Accepted(3),
        QualificationStatus_Rejected(4),
        QualificationStatus_Deleted(5),
        QualificationStatus_Mailed(6),
        QualificationStatus_Missing(7),
        QualificationStatus_Expired(8);

        private int value;
        private QualificationStatusEnum(int value)
        {
            this.value = value;
        }
        public int getValue() {
            return this.value;
        }

        public static QualificationStatusEnum setIntValue (int i) {
            for (QualificationStatusEnum type : QualificationStatusEnum.values()) {
                if (type.value == i) { return type; }
            }
            return QualificationStatus_Requested;
        }
    }

    public String qualificationID ;
    public String userID ;
    public String qualificationTypeID ;
    public String adminUserID ;
    public Date creationDate ;
    public Date effectiveDate;
    public Date expirationDate ;
    public QualificationStatusEnum status;
    public ArrayList<QualificationAudit> auditHistory;
    public ArrayList<PimmForm> forms;
    public ArrayList<QualificationAttachment> attachments;


    @Override
    public void setValueForKey(Object value, String key) {

        if(key.equalsIgnoreCase("status")) {
            if (!value.equals(null)) {
                QualificationStatusEnum result = QualificationStatusEnum.setIntValue((int) value);
                this.status = result;

            }
        }else if (key.equalsIgnoreCase("auditHistory")) {
                if (this.auditHistory == null) {
                    this.auditHistory = new ArrayList<QualificationAudit>();
                }

                if (!value.equals(null)) {
                    JSONArray arrData = (JSONArray) value;

                    if (arrData != null) {
                        for (int i = 0; i < arrData.length(); i++) {
                            try {
                                QualificationAudit result = new QualificationAudit();
                                result.readFromJSONObject(arrData.getJSONObject(i));

                                this.auditHistory.add(result);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }

        }else if (key.equalsIgnoreCase("forms")) {
                if (this.forms == null) {
                    this.forms = new ArrayList<PimmForm>();
                }

                if (!value.equals(null)) {
                    JSONArray arrData = (JSONArray) value;

                    if (arrData != null) {
                        for (int i = 0; i < arrData.length(); i++) {
                            try {
                                PimmForm result = new PimmForm();
                                result.readFromJSONObject(arrData.getJSONObject(i));

                                this.forms.add(result);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }

        }else if (key.equalsIgnoreCase("attachments")) {
                if (this.attachments == null) {
                    this.attachments = new ArrayList<QualificationAttachment>();
                }

                if (!value.equals(null)) {
                    JSONArray arrData = (JSONArray) value;

                    if (arrData != null) {
                        for (int i = 0; i < arrData.length(); i++) {
                            try {
                                QualificationAttachment result = new QualificationAttachment();
                                result.readFromJSONObject(arrData.getJSONObject(i));

                                this.attachments.add(result);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }

            } else {
            super.setValueForKey(value, key);
        }
    }


}
