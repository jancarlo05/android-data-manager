package com.procuro.apimmdatamanagerlib;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by jophenmarieweeks on 10/07/2017.
 */

public class SdrRuleSet extends PimmBaseObject {

    public String RuleSetId;
    public String Name;
    public String Description;
    public ArrayList<SdrRule> Rules;
    @Override
    public void setValueForKey(Object value, String key)
    {
        if(key.equalsIgnoreCase("Rules")) {
            if (this.Rules == null) {
                this.Rules = new ArrayList<SdrRule>();
            }

            if (!value.equals(null)) {
                JSONArray arrStops = (JSONArray) value;

                if (arrStops != null) {
                    for (int i = 0; i < arrStops.length(); i++) {
                        try {
                            SdrRule sdrRule = new SdrRule();
                            sdrRule.readFromJSONObject(arrStops.getJSONObject(i));

                            this.Rules.add(sdrRule);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }

            }
        }else {
            super.setValueForKey(value, key);
        }
    }

    public HashMap<String,Object> dictionaryWithValuesForKeys() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();

        dictionary.put("RuleSetId", this.RuleSetId);
        dictionary.put("Name", this.Name);
        dictionary.put("Description", this.Description);

//        if(this.Rules != null  ){
//            ArrayList<HashMap<String, Object>> RulesList = new ArrayList<>();
//
//            for(SdrRule sdrRule : this.Rules) {
//
//                HashMap<String, Object> RulesDict = sdrRule.dictionaryWithValuesForKeys();
//                RulesList.add(RulesDict);
//
//            }
//
//            dictionary.put("Rules", RulesList);
//
//        }else{
//            dictionary.put("Rules", null);
//        }

        return dictionary;
    }
}
