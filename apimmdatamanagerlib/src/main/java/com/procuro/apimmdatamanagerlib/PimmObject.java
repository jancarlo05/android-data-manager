package com.procuro.apimmdatamanagerlib;

import java.util.HashMap;


public class PimmObject extends PimmBaseObject {

    public String objectName;
    public Number severity;
    public Number eventSeverity;
    public Number state;
    public Boolean oos;
    public Number nodeClass;
    public Number nodeState;
    public String label;
    public String category;

    @Override
    public void setValueForKey(Object value, String key) {
        super.setValueForKey(value, key);
    }

    public HashMap<String,Object> dictionaryWithValuesForKeys() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();

        dictionary.put("objectName",this.objectName);
        dictionary.put("severity",this.severity);
        dictionary.put("eventSeverity",this.eventSeverity);
        dictionary.put("state",this.state);
        dictionary.put("oos",this.oos);
        dictionary.put("nodeClass",this.nodeClass);
        dictionary.put("nodeState",this.nodeState);
        dictionary.put("label",this.label);
        dictionary.put("category",this.category);

        return dictionary;
    }



}
