package com.procuro.apimmdatamanagerlib;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jophenmarieweeks on 06/07/2017.
 */

public class BorderCrossingBuilder extends PimmBaseObject {
    public ArrayList<BorderData> BorderEvts;

    @Override
    public void setValueForKey(Object value, String key) {
        if (key.equalsIgnoreCase("BorderEvts")) {
            if (this.BorderEvts == null) {
                this.BorderEvts = new ArrayList<BorderData>();
            }

            if (!value.equals(null)) {
                JSONArray arrData = (JSONArray) value;

                if (arrData != null) {
                    for (int i = 0; i < arrData.length(); i++) {
                        try {
                            BorderData borderEvts = new BorderData();
                            borderEvts.readFromJSONObject(arrData.getJSONObject(i));

                            this.BorderEvts.add(borderEvts);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        } else {
            super.setValueForKey(value, key);
        }
    }

}
