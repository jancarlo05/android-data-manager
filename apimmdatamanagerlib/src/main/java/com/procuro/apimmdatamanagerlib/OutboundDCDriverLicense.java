package com.procuro.apimmdatamanagerlib;

import java.util.Date;

/**
 * Created by jophenmarieweeks on 24/10/2017.
 */

public class OutboundDCDriverLicense extends PimmBaseObject {

    public  enum DriverLicenseStatus
    {
        LicenseStatus_Valid(0),
        LicenseStatus_Expired(1),
        LicenseStatus_Revoked(2),
        LicenseStatus_Suspended(3),
        LicenseStatus_Restricted(4),
        LicenseStatus_Cancelled(5),
        LicenseStatus_Surrendered(6),
        LicenseStatus_Surrendered_mopedOnly(7),
        LicenseStatus_Invalid(8);

        private int value;
        private DriverLicenseStatus(int value)
        {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }

        public static DriverLicenseStatus setIntValue (int i) {
            for (DriverLicenseStatus type : DriverLicenseStatus.values()) {
                if (type.value == i) { return type; }
            }
            return LicenseStatus_Valid;
        }
    }
    public String outboundDCDriverLicenseId;
    public String userId;
    public String pimmFormID;
    public String licenseType;
    public String licenseNumber;
    public String issuer;
    public Date issueDate;
    public Date expirationDate;
    public DriverLicenseStatus status;

    @Override
    public void setValueForKey(Object value, String key) {

        if (key.equalsIgnoreCase("status")) {

            if (!value.equals(null)) {
                DriverLicenseStatus driverLicenseStatus = DriverLicenseStatus.setIntValue((int) value);
                this.status = driverLicenseStatus;
            }
        }else {
            super.setValueForKey(value, key);
        }
    }




}
