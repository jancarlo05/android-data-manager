package com.procuro.apimmdatamanagerlib;

import org.json.JSONObject;

import java.util.Date;
import java.util.HashMap;

import static com.procuro.apimmdatamanagerlib.SdrMetric.MetricClass.MetricClass_Default;
import static com.procuro.apimmdatamanagerlib.SdrMetric.MetricCompartment.MetricCompartment_None;

/**
 * Created by jophenmarieweeks on 10/07/2017.
 */

public class SdrMetric extends PimmBaseObject {

    public enum MetricClass
    {
        MetricClass_Default(0),
        MetricClass_Temperature(9), // Temperature
        MetricClass_AmbientTemperature(10),
        MetricClass_ProductTemperature(11),
        MetricClass_GPS(30), /// Full GPS for time, location, speed, dir.
        MetricClass_LatLon(31), /// Time and location, no speed and dir.
        MetricClass_Speed(32),
        MetricClass_Direction(33),
        MetricClass_Door(40),
        MetricClass_ReeferPower(50),
        MetricClass_StopInfo(60),
        MetricClass_PendingShipment(300);// 3xx: supplier delivery

        public int value;

        private MetricClass(int value)
        {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }

        public static MetricClass setIntValue (int i) {
            for (MetricClass type : MetricClass.values()) {
                if (type.value == i) { return type; }
            }
            return MetricClass_Default;
        }
    }

    public enum MetricCompartment
    {
        MetricCompartment_None(0),
        MetricCompartment_Freezer(1),
        MetricCompartment_Cooler(2),
        MetricCompartment_Tomato(3),
        MetricCompartment_Dry(4);

        private int value;
        private MetricCompartment(int value)
        {
            this.value = value;
        }


        public int getValue() {
            return this.value;
        }

        public static MetricCompartment setIntValue (int i) {
            for (MetricCompartment type : MetricCompartment.values()) {
                if (type.value == i) { return type; }
            }
            return MetricCompartment_None;
        }
    }

    public String SdrMetricId;
    public MetricClass metricClass;
    public MetricCompartment Compartment;
    public String CompartmentName;
    public String RulesName;
    public String Label;
    public Date LastTimestamp;
    public double LastValue;
    public double Precool;
    public double Min;
    public double Max;
    public double Avg;
    public String LastTimestampText;
    public String LastValueText;
    public String MinText;
    public String MaxText;
    public String AvgText;


    public SdrRuleSet Rules;
    public boolean Primary;
    public boolean OOS;
    public double ProductSource;


    @Override
    public void setValueForKey(Object value, String key) {

        if (key.equalsIgnoreCase("metricClass")) {
            if (!value.equals(null)) {
                MetricClass mClass = MetricClass.setIntValue((int) value);
                this.metricClass = mClass;
            }

        } else if (key.equalsIgnoreCase("Compartment")) {
            if (!value.equals(null)) {
                MetricCompartment metricCompartment = MetricCompartment.setIntValue((int) value);
                this.Compartment = metricCompartment;
            }
        }else if (key.equalsIgnoreCase("Rules")) {

                if (!value.equals(null)) {
                    SdrRuleSet sdrRuleSet = new SdrRuleSet();
                    JSONObject jsonObject = (JSONObject) value;
                    sdrRuleSet.readFromJSONObject(jsonObject);
                    this.Rules = sdrRuleSet;
                }

        } else {
            super.setValueForKey(value, key);
        }
    }

    public HashMap<String,Object> dictionaryWithValuesForKeys() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();


        dictionary.put("SdrMetricId", this.SdrMetricId);
        dictionary.put("CompartmentName", this.CompartmentName);
        dictionary.put("RulesName", this.RulesName);
        dictionary.put("Label", this.Label);
        dictionary.put("LastTimestamp", this.LastTimestamp);
        dictionary.put("LastValue", this.LastValue);
        dictionary.put("Precool", this.Precool);
        dictionary.put("Min", this.Min);
        dictionary.put("Max", this.Max);
        dictionary.put("Avg", this.Avg);
        dictionary.put("LastTimestampText", this.LastTimestampText);
        dictionary.put("LastValueText", this.LastValueText);

        dictionary.put("MinText", this.MinText);
        dictionary.put("MaxText", this.MaxText);
        dictionary.put("AvgText", this.AvgText);
        dictionary.put("Primary", this.Primary);
        dictionary.put("ProductSource", this.ProductSource);


        dictionary.put("Rules", (this.Rules == null) ? null : this.Rules.dictionaryWithValuesForKeys());


        dictionary.put("MetricClass", (this.metricClass ==  null) ? MetricClass_Default : this.metricClass);
        dictionary.put("Compartment", (this.Compartment ==  null) ? MetricCompartment_None : this.Compartment);



        return dictionary;

    }

}
