package com.procuro.apimmdatamanagerlib;

import java.util.Date;
import java.util.HashMap;

/**
 * Created by jophenmarieweeks on 12/07/2017.
 */

public class CompartmentDTO extends PimmBaseObject {




    public enum CompartmentTemperatureProfileEnum {
        TemperatureProfile_Default(0),
        TemperatureProfile_Freezer(1),
        TemperatureProfile_Cooler(2),
        TemperatureProfile_Produce(3),
        TemperatureProfile_Dry(4);

        private int value;
        private CompartmentTemperatureProfileEnum(int value) {
            this.value = value;
        }


       public int getValue() {
           return this.value;
       }

       public static CompartmentTemperatureProfileEnum setIntValue (int i) {
           for (CompartmentTemperatureProfileEnum type : CompartmentTemperatureProfileEnum.values()) {
               if (type.value == i) { return type; }
           }
           return TemperatureProfile_Default;
       }
    }


    public String loader;
    public Date loadStartTime;
    public Date loadEndTime;
    public double cases;
    public double cubes;
    public double setPoint;
    public double targetTemp;
    public double actualTemp;
    public Date actualTime;
    public double precoolTemp;
    public Date precoolTime;
    public CompartmentTemperatureProfileEnum profile;

    public String deviceID; //Trailer DeviceID or Tractor Device ID to indicate whether compartment belongs to tractor or trailer

    public String deliveryStopID; //last stop for the compartment

    @Override
    public void setValueForKey(Object value, String key) {

        if (key.equalsIgnoreCase("profile")) {

            if (!value.equals(null)) {
                CompartmentTemperatureProfileEnum compartmentTemperatureProfileEnum = CompartmentTemperatureProfileEnum.setIntValue((int) value);
                this.profile = compartmentTemperatureProfileEnum;
            }

        }else {
            super.setValueForKey(value, key);
        }
    }

    public HashMap<String,Object> dictionaryWithValuesForKeys() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();

        dictionary.put("loader", this.loader);
        dictionary.put("cases", this.cases);
        dictionary.put("cubes", this.cubes);
        dictionary.put("setPoint", this.setPoint);
        dictionary.put("targetTemp", this.targetTemp);
        dictionary.put("actualTemp", this.actualTemp);
        dictionary.put("precoolTemp", this.precoolTemp);
        dictionary.put("profile", this.profile);
        dictionary.put("loadStartTime", (this.loadStartTime == null) ? null :  this.loadStartTime);
        dictionary.put("loadEndTime", this.loadEndTime);
        dictionary.put("actualTime", this.actualTime);
        dictionary.put("precoolTime", this.precoolTime);


        return dictionary;

    }

}
