package com.procuro.apimmdatamanagerlib;

import java.util.Date;

/**
 * Created by jophenmarieweeks on 12/07/2017.
 */

public class SdrDCReturn extends PimmBaseObject {

   public enum DeliveryDCReturnType {
        DeliveryDCReturnType_Undefined(-1),
        DeliveryDCReturnType_Unplanned(0);

        private int value;
        private DeliveryDCReturnType(int value)
        {
            this.value = value;
        }

       public int getValue() {
           return this.value;
       }

       public static DeliveryDCReturnType setIntValue (int i) {
           for (DeliveryDCReturnType type : DeliveryDCReturnType.values()) {
               if (type.value == i) { return type; }
           }
           return DeliveryDCReturnType_Undefined;
       }
    }

    public String SdrDCReturnId;
    public Date DCArrivalTime;
    public Date DCDepartureTime;
    public DeliveryDCReturnType EventType;

    @Override
    public void setValueForKey(Object value, String key) {

        if(key.equalsIgnoreCase("EventType")) {
            if (!value.equals(null)) {
                DeliveryDCReturnType result = DeliveryDCReturnType.setIntValue((int) value);
                this.EventType = result;
            }
        } else if (key.equalsIgnoreCase("DCArrivalTime")){
            if (!value.equals(null)) {
                this.DCArrivalTime = JSONDate.convertJSONDateToNSDate(String.valueOf(value));

            }
        } else if (key.equalsIgnoreCase("DCDepartureTime")){
            if (!value.equals(null)) {
                this.DCDepartureTime = JSONDate.convertJSONDateToNSDate(String.valueOf(value));
            }

        } else {
            super.setValueForKey(value, key);
        }
    }
}
