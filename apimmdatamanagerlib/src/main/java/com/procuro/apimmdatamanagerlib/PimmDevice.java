package com.procuro.apimmdatamanagerlib;

import java.util.HashMap;

public class PimmDevice extends PimmBaseObject {

    public String deviceID;
    public String deviceName;
    public String ipaddress;
    public String os;
    public String description;
    public int isAdmin;
    public int state;
    public int severity;
    public boolean oos;
    public int nodeClass;
    public int nodeState;
    public String type;
    public int siteType;
    public String devClass;
    public int eventSeverity;
    public boolean canAck;
    public boolean canEditEvent;
    public boolean canEditThreshold;
    public boolean canDisableMetric;

    public String siteID;

    @Override
    public void setValueForKey(Object value, String key) {
        super.setValueForKey(value, key);
    }

    public HashMap<String,Object> dictionaryWithValuesForKeys() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();
        dictionary.put("deviceID",this.deviceID);
        dictionary.put("deviceName",this.deviceName);
        dictionary.put("ipaddress",this.ipaddress);
        dictionary.put("os",this.os);
        dictionary.put("description",this.description);
        dictionary.put("isAdmin",this.isAdmin);
        dictionary.put("state",this.state);
        dictionary.put("severity",this.severity);
        dictionary.put("oos",this.oos);
        dictionary.put("nodeClass",this.nodeClass);
        dictionary.put("nodeState",this.nodeState);
        dictionary.put("type",this.type);
        dictionary.put("siteType",this.siteType);
        dictionary.put("devClass",this.devClass);
        dictionary.put("eventSeverity",this.eventSeverity);
        dictionary.put("canAck",this.canAck);
        dictionary.put("canEditEvent",this.canEditEvent);
        dictionary.put("canEditThreshold",this.canEditThreshold);
        dictionary.put("canDisableMetric",this.canDisableMetric);

        return dictionary;
    }




}
