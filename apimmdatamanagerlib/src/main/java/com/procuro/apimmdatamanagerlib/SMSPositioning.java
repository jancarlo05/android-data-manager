package com.procuro.apimmdatamanagerlib;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

public class SMSPositioning extends PimmBaseObject {

    public String siteID;
    public String templateCode;
    public String keyDriverAccuracy;
    public String keyDriverCleanliness;
    public String keyDriverSpeed;
    public String keyDriverTaste;
    public String keyDriverFriendliness;
    public String salesPerformance_Forecast;
    public String salesPerformance_Actual;
    public String customerSatisfaction_VOC;
    public String customerSatisfaction_ZOD;
    public String customerExperience_CEE;
    public String customerExperience_FEE;

    public String likelyReturn;
    public String problemScore;
    public String resolutionScore;
    public String likelyRecommend;

    public Date storeRecord_Timestamp;
    public String storeRecord_NoOfCars;

    public ArrayList<SMSPositioningEmployeeScheduleAssignment>employeeScheduleAssignmentList;
    public ArrayList<SMSPositioningEmployeePositionData>employeesPositionDataList;



    @Override
    public void setValueForKey(Object value, String key) {
        super.setValueForKey(value, key);

        if (key.equalsIgnoreCase("likelyRecommend")){
            if (value!=null){
                if (!value.toString().equalsIgnoreCase("null")){
                    this.likelyRecommend =  value.toString();
                }
            }
        }

        if (key.equalsIgnoreCase("resolutionScore")){
            if (value!=null){
                if (!value.toString().equalsIgnoreCase("null")){
                    this.resolutionScore =  value.toString();
                }
            }
        }

        if (key.equalsIgnoreCase("problemScore")){
            if (value!=null){
                if (!value.toString().equalsIgnoreCase("null")){
                    this.problemScore =  value.toString();
                }
            }
        }

        if (key.equalsIgnoreCase("likelyReturn")){
            if (value!=null){
                if (!value.toString().equalsIgnoreCase("null")){
                    this.likelyReturn =  value.toString();
                }
            }
        }

        if (key.equalsIgnoreCase("storeRecord_Timestamp")){
            if (value!=null){
                if (!value.toString().equalsIgnoreCase("null")){
                    this.storeRecord_Timestamp = JSONDate.convertJSONDateToNSDate(value.toString());
                }
            }
        }
        if (key.equalsIgnoreCase("siteID")){
            if (value!=null){
                if (!value.toString().equalsIgnoreCase("null")){
                    this.siteID =  value.toString();
                }
            }
        }
        if (key.equalsIgnoreCase("templateCode")){
            if (value!=null){
                if (!value.toString().equalsIgnoreCase("null")){
                    this.templateCode =  value.toString();
                }
            }
        }
        if (key.equalsIgnoreCase("storeRecord_NoOfCars")){
            if (value!=null){
                if (!value.toString().equalsIgnoreCase("null")){
                    this.storeRecord_NoOfCars = value.toString();
                }
            }
        }
        if (key.equalsIgnoreCase("keyDriverAccuracy")){
            if (value!=null){
                if (!value.toString().equalsIgnoreCase("null")){
                    this.keyDriverAccuracy = value.toString();
                }
            }
        }
        if (key.equalsIgnoreCase("keyDriverCleanliness")){
            if (value!=null){
                if (!value.toString().equalsIgnoreCase("null")){
                    this.keyDriverCleanliness = value.toString();
                }
            }
        }
        if (key.equalsIgnoreCase("keyDriverTaste")){
            if (value!=null){
                if (!value.toString().equalsIgnoreCase("null")){
                    this.keyDriverTaste = value.toString();
                }
            }
        }
        if (key.equalsIgnoreCase("keyDriverSpeed")){
            if (value!=null){
                if (!value.toString().equalsIgnoreCase("null")){
                    this.keyDriverSpeed = value.toString();
                }
            }
        }
        if (key.equalsIgnoreCase("keyDriverFriendliness")){
            if (value!=null){
                if (!value.toString().equalsIgnoreCase("null")){
                    this.keyDriverFriendliness = value.toString();
                }
            }
        }

        if (key.equalsIgnoreCase("salesPerformance_Forecast")){
            if (value!=null){
                if (!value.toString().equalsIgnoreCase("null")){
                    this.salesPerformance_Forecast = value.toString();
                }
            }
        }
        if (key.equalsIgnoreCase("salesPerformance_Actual")){
            if (value!=null){
                if (!value.toString().equalsIgnoreCase("null")){
                    this.salesPerformance_Actual = value.toString();
                }
            }
        }
        if (key.equalsIgnoreCase("customerSatisfaction_VOC")){
            if (value!=null){
                if (!value.toString().equalsIgnoreCase("null")){
                    this.customerSatisfaction_VOC = value.toString();
                }
            }
        }
        if (key.equalsIgnoreCase("customerSatisfaction_ZOD")){
            if (value!=null){
                if (!value.toString().equalsIgnoreCase("null")){
                    this.customerSatisfaction_ZOD = value.toString();
                }
            }
        }
        if (key.equalsIgnoreCase("customerExperience_CEE")){
            if (value!=null){
                if (!value.toString().equalsIgnoreCase("null")){
                    this.customerExperience_CEE = value.toString();
                }
            }
        }
        if (key.equalsIgnoreCase("customerExperience_FE")){
            if (value!=null){
                if (!value.toString().equalsIgnoreCase("null")){
                    this.customerExperience_FEE = value.toString();
                }
            }
        }




        if (key.equalsIgnoreCase("employeeScheduleAssignmentList")){
            ArrayList<SMSPositioningEmployeeScheduleAssignment>employeeScheduleAssignmentList = new ArrayList<>();
            if (value!=null){
                JSONArray jsonArray = (JSONArray) value;
                for (int i = 0; i <jsonArray.length() ; i++) {
                    SMSPositioningEmployeeScheduleAssignment employeeScheduleAssignment = new SMSPositioningEmployeeScheduleAssignment();
                    try {
                        employeeScheduleAssignment.readFromJSONObject(jsonArray.getJSONObject(i));
                        employeeScheduleAssignmentList.add(employeeScheduleAssignment);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                this.employeeScheduleAssignmentList = employeeScheduleAssignmentList;
            }
        }
        if (key.equalsIgnoreCase("employeesPositionDataList")){
            ArrayList<SMSPositioningEmployeePositionData>employeesPositionDataList = new ArrayList<>();
            if (value!=null){
                JSONArray jsonArray = (JSONArray) value;
                for (int i = 0; i <jsonArray.length() ; i++) {
                    SMSPositioningEmployeePositionData SMSPositioningEmployeePositionData = new SMSPositioningEmployeePositionData();
                    try {
                        SMSPositioningEmployeePositionData.readFromJSONObject(jsonArray.getJSONObject(i));
                        employeesPositionDataList.add(SMSPositioningEmployeePositionData);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                this.employeesPositionDataList = employeesPositionDataList;
            }
        }

    }


    public HashMap<String,Object> dictionaryWithValuesForKeys() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();

        dictionary.put("employeeScheduleAssignmentList", this.employeeScheduleAssignmentList);;
        dictionary.put("employeesPositionDataList", this.employeesPositionDataList);
        dictionary.put("storeRecord_NoOfCars", this.storeRecord_NoOfCars);
        dictionary.put("keyDriverAccuracy", this.keyDriverAccuracy);
        dictionary.put("keyDriverCleanliness", this.keyDriverCleanliness);
        dictionary.put("keyDriverSpeed", this.keyDriverSpeed);
        dictionary.put("keyDriverTaste", this.keyDriverTaste);
        dictionary.put("keyDriverFriendliness", this.keyDriverFriendliness);
        dictionary.put("salesPerformance_Forecast", this.salesPerformance_Forecast);
        dictionary.put("salesPerformance_Actual", this.salesPerformance_Actual);
        dictionary.put("customerSatisfaction_VOC", this.customerSatisfaction_VOC);
        dictionary.put("customerSatisfaction_ZOD", this.customerSatisfaction_ZOD);
        dictionary.put("customerExperience_CEE", this.customerExperience_CEE);
        dictionary.put("customerExperience_FEE", this.customerExperience_FEE);
        dictionary.put("storeRecord_Timestamp", this.storeRecord_Timestamp);
        dictionary.put("siteID", this.siteID);
        dictionary.put("templateCode", this.templateCode);

        return dictionary;
    }



}
