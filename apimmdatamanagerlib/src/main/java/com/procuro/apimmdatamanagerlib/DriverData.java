package com.procuro.apimmdatamanagerlib;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jophenmarieweeks on 06/07/2017.
 */

public class DriverData extends PimmBaseObject {


    public ArrayList<PimmForm> forms;
    public ArrayList<SdrReport> shipments;

    @Override
    public void setValueForKey(Object value, String key) {

        if (key.equalsIgnoreCase("forms")) {
            if (this.forms == null) {
                this.forms = new ArrayList<PimmForm>();
            }

            if (!value.equals(null)) {
                JSONArray arrData = (JSONArray) value;

                if (arrData != null) {
                    for (int i = 0; i < arrData.length(); i++) {
                        try {
                            PimmForm pimmForm = new PimmForm();
                            pimmForm.readFromJSONObject(arrData.getJSONObject(i));

                            this.forms.add(pimmForm);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }else if (key.equalsIgnoreCase("shipments")) {
            if (this.shipments == null) {
                this.shipments = new ArrayList<SdrReport>();
            }

            if (!value.equals(null)) {
                JSONArray arrData = (JSONArray) value;

                if (arrData != null) {
                    for (int i = 0; i < arrData.length(); i++) {
                        try {
                            SdrReport sdrReport = new SdrReport();
                            sdrReport.readFromJSONObject(arrData.getJSONObject(i));

                            this.shipments.add(sdrReport);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        } else {
            super.setValueForKey(value, key);
        }
    }
}
