package com.procuro.apimmdatamanagerlib;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jophenmarieweeks on 10/07/2017.
 */

public class IFTABuilder extends PimmBaseObject {

    public String CustomerName;
    public String Address1;
    public String Address2;
    public String DateRange;
    public String TimeZone;
    public int TimeZoneId;
    public ArrayList<IFTABorderCalculation> ReportData; // Array of IFTABorderCalculation objects

    @Override
    public void setValueForKey(Object value, String key)
    {
        if(key.equalsIgnoreCase("ReportData")) {
            if (this.ReportData == null) {
                this.ReportData = new ArrayList<IFTABorderCalculation>();
            }

            if (!value.equals(null)) {
                JSONArray arrData = (JSONArray) value;

                if (arrData != null) {
                    for (int i = 0; i < arrData.length(); i++) {
                        try {
                            IFTABorderCalculation iftaBorderCalculation = new IFTABorderCalculation();
                            iftaBorderCalculation.readFromJSONObject(arrData.getJSONObject(i));

                            this.ReportData.add(iftaBorderCalculation);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        } else {
            super.setValueForKey(value, key);
        }
    }

}
