package com.procuro.apimmdatamanagerlib;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jophenmarieweeks on 10/07/2017.
 */

public class SdrDeliveryTrailer extends PimmBaseObject {

    public String TrailerId;
    public String RouteId;
    public String RouteName;
    public String DeliveryId;
    public StoreDeliveryEnums.TransportStatus transportStatus;
    public StoreDeliveryEnums.LifecycleState lifecycleState;
    public String DistributorName;
    public String DCId;
    public String DCName;
    public String TrailerName;

    //Array of SdrMetric Objects
    public ArrayList<SdrMetric> Metrics;
//Array of Strings
    public ArrayList<String> Descriptors;
    public SdrGis GIS;
    public GPSValue LastGPS;


//Array of Strings
    //public MutableArray Capabilities;
    public ArrayList<String> Capabilities;

    @Override
    public void setValueForKey(Object value, String key)
    {
        if(key.equalsIgnoreCase("Metrics"))
        {
            if(this.Metrics == null)
            {
                this.Metrics = new ArrayList<SdrMetric>();
            }
            if (!value.equals(null)) {
                if (value instanceof JSONArray) {

                    JSONArray arrData = (JSONArray) value;

                    if (arrData != null) {
                        for (int i = 0; i < arrData.length(); i++) {
                            try {
                                SdrMetric sdrMetric = new SdrMetric();
                                sdrMetric.readFromJSONObject(arrData.getJSONObject(i));

                                this.Metrics.add(sdrMetric);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }
        }
        else if(key.equalsIgnoreCase("Descriptors"))
        {
            if(this.Descriptors == null)
            {
                this.Descriptors = new ArrayList<String>();
            }

            List<String> Descriptors = (List<String>) value;
            for(String Desc : Descriptors)
            {
                this.Descriptors.add(Desc);
            }
        }
        else if(key.equalsIgnoreCase("Capabilities"))
        {
            if(this.Capabilities == null)
            {
                this.Capabilities = new ArrayList<String>();
            }

            List<String> Capabilities = (List<String>) value;
            for(String Capability : Capabilities)
            {
                this.Capabilities.add(Capability);
            }
        }
        else if (key.equalsIgnoreCase("transportStatus"))
        {
            if (!value.equals(null)) {
                StoreDeliveryEnums.TransportStatus transportStatus = StoreDeliveryEnums.TransportStatus.setIntValue((int) value);
                this.transportStatus = transportStatus;
            }
        }
        else if (key.equalsIgnoreCase("lifecycleState"))
        {
            if (!value.equals(null)) {
                StoreDeliveryEnums.LifecycleState lifecycleState = StoreDeliveryEnums.LifecycleState.setIntValue((int) value);
                this.lifecycleState = lifecycleState;
            }
        }
        else if (key.equalsIgnoreCase("GIS"))
        {
            if (value != null) {
                SdrGis sdrGis = new SdrGis();
                JSONObject jsonObject = (JSONObject) value;
                sdrGis.readFromJSONObject(jsonObject);
                this.GIS = sdrGis;
            }
        }
        else if (key.equalsIgnoreCase("LastGPS"))
        {
            if (value != null) {
                GPSValue gpsValue = new GPSValue();
                JSONObject jsonObject = (JSONObject) value;
                gpsValue.readFromJSONObject(jsonObject);
                this.LastGPS = gpsValue;
            }
        }
        else
        {
            super.setValueForKey(value, key);
        }
    }
}
