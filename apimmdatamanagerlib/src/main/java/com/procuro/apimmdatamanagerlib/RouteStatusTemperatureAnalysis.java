package com.procuro.apimmdatamanagerlib;

import java.util.HashMap;

/**
 * Created by jophenmarieweeks on 12/07/2017.
 */

public class RouteStatusTemperatureAnalysis extends PimmBaseObject {

   // NS_ASSUME_NONNULL_BEGIN

    public String RSTA_ID;
    public String ShipmentId;
    public String InstanceId;
    public String Label;
    public String Compartment;
    public int SetPoint;
    public int Departure;
    public double InTempDuration;
    public double TotalDuration;
    public double RouteAvg;
    public double StopAvg;
    public double StagingAvg;
    public double InTempPercent;
    public double PrecoolTemp;
    public String InTempDurationString   ;
    public String TotalDurationString;

//    NS_ASSUME_NONNULL_END



    public HashMap<String, Object> dictionaryWithValuesForKeys() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();


        dictionary.put("RSTA_ID",this.RSTA_ID);
        dictionary.put("ShipmentId",this.ShipmentId);
        dictionary.put("InstanceId",this.InstanceId);
        dictionary.put("Label",this.Label);
        dictionary.put("Compartment",this.Compartment);
        dictionary.put("SetPoint",this.SetPoint);
        dictionary.put("Departure",this.Departure);
        dictionary.put("InTempDuration",this.InTempDuration);
        dictionary.put("TotalDuration",this.TotalDuration);
        dictionary.put("RouteAvg",this.RouteAvg);
        dictionary.put("StopAvg",this.StopAvg);
        dictionary.put("StagingAvg",this.StagingAvg);
        dictionary.put("InTempPercent",this.InTempPercent);
        dictionary.put("PrecoolTemp",this.PrecoolTemp);
        dictionary.put("InTempDurationString",this.InTempDurationString);
        dictionary.put("TotalDurationString",this.TotalDurationString);

        return dictionary;
    }
}
