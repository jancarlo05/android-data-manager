package com.procuro.apimmdatamanagerlib;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

public class SMSDailyOpsPlanCustomerSatisfacation extends  PimmBaseObject {

    public String verifiedBy;
    public Date verifiedDate;
    public ArrayList<SMSDailyOpsPlanCustomerSatisfactionItem>customerSatisfactionItems;


    @Override
    public void setValueForKey(Object value, String key) {
        super.setValueForKey(value, key);

        if (key.equalsIgnoreCase("verifiedBy")){
            this.verifiedBy = (String) value.toString();
        }
        if (key.equalsIgnoreCase("verifiedDate")){
            this.verifiedDate = JSONDate.convertJSONDateToNSDate(value.toString());
        }

        if (key.equalsIgnoreCase("customerSatisfactionItems")){
            ArrayList<SMSDailyOpsPlanCustomerSatisfactionItem>items = new ArrayList<>();
            if (value!=null){
                if (!value.toString().equalsIgnoreCase("null")){
                    JSONArray jsonArray = (JSONArray) value;
                    for (int i = 0; i <jsonArray.length() ; i++) {
                        SMSDailyOpsPlanCustomerSatisfactionItem item =
                                new SMSDailyOpsPlanCustomerSatisfactionItem();
                        JSONObject jsonObject = null;
                        try {
                            jsonObject = jsonArray.getJSONObject(i);
                            item.readFromJSONObject(jsonObject);
                            items.add(item);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            this.customerSatisfactionItems = items;
        }
    }


    public HashMap<String,Object> dictionaryWithValuesForKeys() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();

        dictionary.put("verifiedBy", this.verifiedBy);
        dictionary.put("verifiedDate", this.verifiedDate);
        dictionary.put("customerSatisfactionItems", this.customerSatisfactionItems);

        return dictionary;
    }

}
