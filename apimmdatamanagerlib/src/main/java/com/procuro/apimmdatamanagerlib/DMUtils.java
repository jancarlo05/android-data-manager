package com.procuro.apimmdatamanagerlib;

import android.util.Base64;

import java.nio.charset.StandardCharsets;
import java.util.Date;

import static java.lang.Math.floor;

/**
 * Created by llagundimao on 8/10/17.
 */

public class DMUtils {
    private  static DMUtils context;

    private  DMUtils() {

    }

    public static DMUtils getInstance() {
        return (context == null) ? context = new DMUtils() : context;
    }

    public int getIntValue (int value) {
        int retVal = 0;

        try {
            retVal = value;
        } catch (NumberFormatException e) {
            return 0;
        }

        return retVal;
    }

    public static Date truncateSecondsForDate(Date date){

        if(date == null){
            return null;
        }
        double time =floor(date.getTime() / 60.0) * 60.0;

        Date minute = new Date((long) time);
        return minute;
    }

    public static byte[] ConvertStringToByteArray(Object object)throws Exception{
        if (ValidObject(object)){
            String finaldecode = object.toString().replace("\\r","").replace("\\n","").replace("\\","");
            byte[] bt = Base64.decode(finaldecode, Base64.DEFAULT);
            String strNewEncode = new String(bt, StandardCharsets.UTF_8);
            String s = strNewEncode.replaceAll("[^\\x20-\\x7E]", "");
            return s.getBytes();
        }
        return null;
    }

    public static boolean ValidObject(Object object){
        return object != null && !object.toString().equalsIgnoreCase("null") && !object.toString().equalsIgnoreCase("");
    }

    public static boolean ConvertToBoolean(Object object){
        if (object instanceof Boolean){
            return (boolean) object;
        }
        else if ( object instanceof String){
            return object.toString().equalsIgnoreCase("true");
        }
        else if (object instanceof Integer){
            return object.toString().equalsIgnoreCase("1");
        }
        return false;
    }
}
