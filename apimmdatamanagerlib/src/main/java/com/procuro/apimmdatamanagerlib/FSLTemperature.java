package com.procuro.apimmdatamanagerlib;

public class FSLTemperature extends  PimmBaseObject {


    public double inTempPercent;
    public double monitoredPercent;
    public int score;
    public int numberOfSites;

    @Override
    public void setValueForKey(Object value, String key) {
        super.setValueForKey(value, key);

        if (key.equalsIgnoreCase("inTempPercent")){
            if (value!=null){
                try {
                    this.inTempPercent = Double.parseDouble(value.toString());
                }catch (Exception ignored){
                }
            }
        }

        if (key.equalsIgnoreCase("monitoredPercent")){
            if (value!=null){
                try {
                    this.monitoredPercent = Double.parseDouble(value.toString());
                }catch (Exception ignored){
                }
            }
        }
    }
}
