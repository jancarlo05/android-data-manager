package com.procuro.apimmdatamanagerlib;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

public class SMSDailyOpsPlanCommunicationNotes extends  PimmBaseObject {

    public String verifiedBy;
    public Date verifiedDate;
    public ArrayList<String>communicationNotesItems;


    @Override
    public void setValueForKey(Object value, String key) {
        super.setValueForKey(value, key);

        if (key.equalsIgnoreCase("verifiedBy")){
            if (!value.toString().equalsIgnoreCase("null")){
                this.verifiedBy = (String)value.toString();
            }
        }
        if (key.equalsIgnoreCase("verifiedDate")){
            this.verifiedDate = JSONDate.convertJSONDateToNSDate(value.toString());
        }

        if (key.equalsIgnoreCase("communicationNotesItems")){
            ArrayList<String>strings = new ArrayList<>();

            if (value!=null){
                JSONArray jsonArray = (JSONArray) value;
                for (int i = 0; i <jsonArray.length() ; i++) {
                    try {
                        strings.add(jsonArray.getString(i));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            this.communicationNotesItems = strings;
        }
    }
    public HashMap<String,Object> dictionaryWithValuesForKeys() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();

        dictionary.put("verifiedBy", this.verifiedBy);
        dictionary.put("verifiedDate", this.verifiedDate);
        dictionary.put("communicationNotesItems", this.communicationNotesItems);

        return dictionary;
    }

}
