package com.procuro.apimmdatamanagerlib;

import java.util.Date;

/**
 * Created by jophenmarieweeks on 10/07/2017.
 */

public class StickyNote extends PimmBaseObject {

   public String stickyNoteID;
   public String userid;
   public String instanceid;
   public String instanceName;
   public String deviceid;
   public String deviceName;
   public String instanceObjectName;
   public Date creationtime;
   public Date inserttime;
   public Date timestamp;
   public String message;
   public String snObjectName;




}
