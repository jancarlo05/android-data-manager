package com.procuro.apimmdatamanagerlib;


import java.util.HashMap;

public class SupplierDeliveryPlantScorecardDTO extends PimmBaseObject {

    public int grade;
    public int deliveryCount_A;
    public int deliveryCount_B;
    public int deliveryCount_C;
    public int deliveryCount_D;
    public int deliveryCount_F;
    public int numberShipments;
    public int numberFlaggedShipments;
    public int numberRejectedShipments;

    @Override
    public void setValueForKey(Object value, String key)
    {
        super.setValueForKey(value, key);
    }

    public HashMap<String,Object> dictionaryWithValuesForKeys() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();

        dictionary.put("grade",this.grade);
        dictionary.put("deliveryCount_A",this.deliveryCount_A);
        dictionary.put("deliveryCount_B",this.deliveryCount_B);
        dictionary.put("deliveryCount_C",this.deliveryCount_C);
        dictionary.put("deliveryCount_D",this.deliveryCount_D);
        dictionary.put("deliveryCount_F",this.deliveryCount_F);

        dictionary.put("numberShipments",this.numberShipments);
        dictionary.put("numberFlaggedShipments",this.numberFlaggedShipments);
        dictionary.put("numberRejectedShipments",this.numberRejectedShipments);

        return dictionary;
    }

}
