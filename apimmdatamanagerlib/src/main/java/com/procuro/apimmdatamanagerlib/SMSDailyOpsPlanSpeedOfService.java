package com.procuro.apimmdatamanagerlib;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

public class SMSDailyOpsPlanSpeedOfService extends  PimmBaseObject {

    public String verifiedBy;
    public Date verifiedDate;
    public ArrayList<SMSDailyOpsPlanSpeedOfServiceItem>speedOfServiceItems;


    @Override
    public void setValueForKey(Object value, String key) {
        super.setValueForKey(value, key);

        if (key.equalsIgnoreCase("speedOfServiceItems")){
            ArrayList<SMSDailyOpsPlanSpeedOfServiceItem> items = new ArrayList<>();
            if (value!=null){
                if (!value.toString().equalsIgnoreCase("null")){
                    JSONArray jsonArray = (JSONArray) value;
                    for (int i = 0; i <jsonArray.length() ; i++) {
                        try {
                            SMSDailyOpsPlanSpeedOfServiceItem item = new SMSDailyOpsPlanSpeedOfServiceItem();
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            item.readFromJSONObject(jsonObject);
                            items.add(item);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    this.speedOfServiceItems = items;
                }
            }
        }

        if (key.equalsIgnoreCase("verifiedBy")){
            this.verifiedBy = (String)value.toString();
        }
        if (key.equalsIgnoreCase("verifiedDate")){
            this.verifiedDate = JSONDate.convertJSONDateToNSDate(value.toString());
        }

    }


    public HashMap<String,Object> dictionaryWithValuesForKeys() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();

        dictionary.put("verifiedBy", this.verifiedBy);
        dictionary.put("verifiedDate", this.verifiedDate);


        return dictionary;
    }

}
