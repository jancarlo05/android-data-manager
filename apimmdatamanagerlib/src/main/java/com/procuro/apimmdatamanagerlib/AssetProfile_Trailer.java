package com.procuro.apimmdatamanagerlib;

import java.util.Date;
import java.util.HashMap;

/**
 * Created by jophenmarieweeks on 06/07/2017.
 */

public class AssetProfile_Trailer extends PimmBaseObject {
    public String profileType;
    public String make;
    public String name;
    public String model;
    public String hardwareVersion;
    public String insulation;
    public String generatorMake;
    public String generatorModel;
    public String generatorFuel;
    public String tireMake;
    public String tireModel;
    public String tireSize;
    public String liftgateMake;
    public String liftgateModel;
    public String landingGearMake;
    public String landingGearModel;
    public String lightingType;
    public String additionalFeatures;
    public String notes;


    public int height;
    public int length;
    public int width;
    public int frontWallThickness;
    public int sideWallThickness;
    public int floorThickness;
    public int rearDoorThickness;
    public int generatorCylanders;
    public int generatorFuelCapacity;
    public int tireCount;
    public int tirePSI;
    public int tireWarrantyMiles;
    public int liftgateCapacity;
    public int footWidth;
    public int footLength;


    public Date warrantyStartDate;
    public Date warrantyEndDate;
    public Date leaseStartDate;
    public Date leaseEndDate;
    public Date liftgateWarrantyStartDate;
    public Date liftgateWarrantyEndDate;

    public double generatorSize;
    public int trailerType;

    public boolean hasReefer;
    public boolean hasTelematics;
    public boolean hasLiftgate;
    public boolean hasLandingGear;
    public boolean hasLighting;
    public boolean hasRearAxleProbe;
    public boolean hasHubOdometer;


    public HashMap<String,Object> dictionaryWithValuesForKeys() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();

        dictionary.put("hosEventID", this.profileType);
        dictionary.put("make", this.make);
        dictionary.put("name", this.name);
        dictionary.put("model", this.model);
        dictionary.put("hardwareVersion", this.hardwareVersion);
        dictionary.put("height", this.height);
        dictionary.put("length", this.length);
        dictionary.put("width", this.width);
        dictionary.put("insulation", this.insulation);
        dictionary.put("frontWallThickness", this.frontWallThickness);
        dictionary.put("sideWallThickness", this.sideWallThickness);
        dictionary.put("floorThickness", this.floorThickness);
        dictionary.put("rearDoorThickness", this.rearDoorThickness);
        dictionary.put("generatorMake", this.generatorMake);
        dictionary.put("generatorModel", this.generatorModel);
        dictionary.put("generatorCylanders", this.generatorCylanders);
        dictionary.put("generatorSize", this.generatorSize);
        dictionary.put("generatorFuel", this.generatorFuel);
        dictionary.put("generatorFuelCapacity", this.generatorFuelCapacity);
        dictionary.put("tireCount", this.tireCount);
        dictionary.put("tireMake", this.tireMake);
        dictionary.put("tireModel", this.tireModel);
        dictionary.put("tireSize", this.tireSize);
        dictionary.put("tirePSI", this.tirePSI);
        dictionary.put("tireWarrantyMiles", this.tireWarrantyMiles);
        dictionary.put("liftgateMake", this.liftgateMake);
        dictionary.put("liftgateModel", this.liftgateModel);
        dictionary.put("liftgateCapacity", this.liftgateCapacity);
        dictionary.put("landingGearMake", this.landingGearMake);
        dictionary.put("landingGearModel", this.landingGearModel);
        dictionary.put("footWidth", this.footWidth);
        dictionary.put("footLength", this.footLength);
        dictionary.put("lightingType", this.lightingType);
        dictionary.put("additionalFeatures", this.additionalFeatures);
        dictionary.put("notes", this.notes);
        dictionary.put("hasReefer", this.hasReefer);
        dictionary.put("hasTelematics", this.hasTelematics);
        dictionary.put("hasLiftgate", this.hasLiftgate);
        dictionary.put("hasLandingGear", this.hasLandingGear);
        dictionary.put("hasLighting", this.hasLighting);
        dictionary.put("hasRearAxleProbe", this.hasRearAxleProbe);
        dictionary.put("hasHubOdometer", this.hasHubOdometer);

        dictionary.put("warrantyStartDate", this.warrantyStartDate.toString());
        dictionary.put("warrantyEndDate", this.warrantyEndDate.toString());
        dictionary.put("leaseStartDate", this.leaseStartDate.toString());
        dictionary.put("leaseEndDate", this.leaseEndDate.toString());
        dictionary.put("liftgateWarrantyStartDate", this.liftgateWarrantyStartDate.toString());
        dictionary.put("liftgateWarrantyEndDate", this.liftgateWarrantyEndDate.toString());


        return dictionary;
    }


}


