package com.procuro.apimmdatamanagerlib;

/**
 * Created by jophenmarieweeks on 12/07/2017.
 */

public class DeliveryStatusStopDTO extends PimmBaseObject {

    public int StopId;
    public String SiteId;
    public String SiteName;

}
