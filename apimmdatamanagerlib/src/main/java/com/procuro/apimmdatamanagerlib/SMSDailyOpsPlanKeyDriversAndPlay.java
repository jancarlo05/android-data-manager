package com.procuro.apimmdatamanagerlib;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

public class SMSDailyOpsPlanKeyDriversAndPlay extends  PimmBaseObject {

    public String verifiedBy;
    public Date verifiedDate;

    public String keyDriverPerformance_Accuracy;
    public String keyDriverPerformance_Speed;
    public String customerSatisfaction_ZOD;
    public String keyDriverPerformance_Cleanliness;
    public String customerSatisfaction_VOC;
    public String keyDriverPerformance_Taste;
    public String keyDriverPerformance_Friendliness;
    public String customerExperience_FE;
    public String customerExperience_CEE;


    @Override
    public void setValueForKey(Object value, String key) {
        super.setValueForKey(value, key);

        if (key.equalsIgnoreCase("verifiedBy")){
            if (value!=null){
                this.verifiedBy = (String)value.toString();
            }

        }

        if (key.equalsIgnoreCase("verifiedDate")){
            if (value!=null){
                this.verifiedDate = JSONDate.convertJSONDateToNSDate(value.toString());
            }

        }

        if (key.equalsIgnoreCase("keyDriverPerformance_Accuracy")){
            if (value!=null){
                this.keyDriverPerformance_Accuracy = (String)value.toString();
            }

        }

        if (key.equalsIgnoreCase("keyDriverPerformance_Speed")){
            if (value!=null){
                this.keyDriverPerformance_Speed = (String)value.toString();
            }

        }

        if (key.equalsIgnoreCase("customerSatisfaction_ZOD")){
            if (value!=null){
                this.customerSatisfaction_ZOD = (String)value.toString();
            }

        }

        if (key.equalsIgnoreCase("keyDriverPerformance_Cleanliness")){
            if (value!=null){
                this.keyDriverPerformance_Cleanliness = (String)value.toString();
            }

        }

        if (key.equalsIgnoreCase("customerSatisfaction_VOC")){
            if (value!=null){
                this.customerSatisfaction_VOC = (String)value.toString();
            }

        }

        if (key.equalsIgnoreCase("keyDriverPerformance_Taste")){
            if (value!=null){
                this.keyDriverPerformance_Taste = (String)value.toString();
            }

        }

        if (key.equalsIgnoreCase("keyDriverPerformance_Friendliness")){
            if (value!=null){
                this.keyDriverPerformance_Friendliness = (String)value.toString();
            }

        }

        if (key.equalsIgnoreCase("customerExperience_FE")){
            if (value!=null){
                this.customerExperience_FE = (String)value.toString();
            }

        }

        if (key.equalsIgnoreCase("customerExperience_CEE")){
            if (value!=null){
                this.customerExperience_CEE = (String)value.toString();
            }
        }


    }


    public HashMap<String,Object> dictionaryWithValuesForKeys() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();

        dictionary.put("verifiedBy", this.verifiedBy);
        dictionary.put("keyDriverPerformance_Accuracy", this.keyDriverPerformance_Accuracy);
        dictionary.put("keyDriverPerformance_Speed", this.keyDriverPerformance_Speed);
        dictionary.put("customerSatisfaction_ZOD", this.customerSatisfaction_ZOD);
        dictionary.put("keyDriverPerformance_Cleanliness", this.keyDriverPerformance_Cleanliness);
        dictionary.put("customerSatisfaction_VOC", this.customerSatisfaction_VOC);
        dictionary.put("keyDriverPerformance_Taste", this.keyDriverPerformance_Taste);
        dictionary.put("keyDriverPerformance_Friendliness", this.keyDriverPerformance_Friendliness);
        dictionary.put("customerExperience_FE", this.customerExperience_FE);
        dictionary.put("customerExperience_CEE", this.customerExperience_CEE);


        return dictionary;
    }

}
