package com.procuro.apimmdatamanagerlib;

import java.util.HashMap;

/**
 * Created by jophenmarieweeks on 07/07/2017.
 */

public class PropertyValue extends PimmBaseObject {

    public String property;
    public String value;

    @Override
    public void setValueForKey(Object value, String key) {
        super.setValueForKey(value, key);
        if (key.equalsIgnoreCase("Key")){
            if (value!=null){
                if (!value.toString().equalsIgnoreCase("null")){
                    this.property = value.toString();
                }
            }
        }
        if (key.equalsIgnoreCase("value")){
            if (value!=null){
                if (!value.toString().equalsIgnoreCase("null")){
                    this.value = value.toString();
                }
            }
        }
    }

    public HashMap<String,Object> dictionaryWithValuesForKeys() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();


        dictionary.put("property", this.property);
        dictionary.put("value", this.value);

        return dictionary;
    }
}
