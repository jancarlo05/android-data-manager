package com.procuro.apimmdatamanagerlib;


import java.util.HashMap;

public class StoreDeliveryScorecardDTO extends PimmBaseObject {

    public int ctmOverallGrade;
    public int ctmCoolerGrade;
    public int ctmFreezerGrade;
    public double temperatureGrade;
    public double otdGrade;
    public double inTempStopPercent;
    public double monitoredRoutePercent;
    public double integrationScore;
    public double ctmInputScore;
    public double trailerCommunicationScore;
    public double driverPerformanceScore;

    @Override
    public void setValueForKey(Object value, String key)
    {
        super.setValueForKey(value, key);
    }

    public HashMap<String,Object> dictionaryWithValuesForKeys() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();

        dictionary.put("ctmOverallGrade",this.ctmOverallGrade);
        dictionary.put("ctmCoolerGrade",this.ctmCoolerGrade);
        dictionary.put("ctmFreezerGrade",this.ctmFreezerGrade);
        dictionary.put("temperatureGrade",this.temperatureGrade);
        dictionary.put("otdGrade",this.otdGrade);
        dictionary.put("inTempStopPercent",this.inTempStopPercent);

        dictionary.put("monitoredRoutePercent",this.monitoredRoutePercent);
        dictionary.put("integrationScore",this.integrationScore);
        dictionary.put("ctmInputScore",this.ctmInputScore);
        dictionary.put("trailerCommunicationScore",this.trailerCommunicationScore);
        dictionary.put("driverPerformanceScore",this.driverPerformanceScore);

        return dictionary;
    }

}
