package com.procuro.apimmdatamanagerlib;

import java.util.Date;

/**
 * Created by jophenmarieweeks on 01/08/2017.
 */

public class ShipmentLot extends PimmBaseObject {

    public String shipmentLotId;
    public String shipmentId;
    public String lotId;
    public String lotDetailId;
    public Date timestamp;
    public String userId;
    public  int binCount; //int
    public  double caseCount;
    public  double minorDefectCount;
    public  double majorDefectCount;
    public  Date codeDate;



}
