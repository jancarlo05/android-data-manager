package com.procuro.apimmdatamanagerlib;

import java.util.Date;

/**
 * Created by jophenmarieweeks on 01/08/2017.
 */

public class TagDataUploadDTO extends PimmBaseObject {
    public String deviceType;
    public String serialNumber;
    public String shipmentId;
    public Date timestamp;
}
