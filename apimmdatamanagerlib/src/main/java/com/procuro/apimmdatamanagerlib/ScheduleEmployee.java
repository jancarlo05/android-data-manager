package com.procuro.apimmdatamanagerlib;

import java.util.Date;

/**
 * Created by jophenmarieweeks on 12/07/2017.
 */

public class ScheduleEmployee extends PimmBaseObject {

    public enum EmployeeAvailability {
        EmployeeAvailability_Available(0),
        EmployeeAvailability_Vacation(1),
        EmployeeAvailability_Sick(2),
        EmployeeAvailability_Leave(3);

        private int value;
        private EmployeeAvailability(int value)
        {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }

        public static EmployeeAvailability setIntValue (int i) {
            for (EmployeeAvailability type : EmployeeAvailability.values()) {
                if (type.value == i) { return type; }
            }
            return EmployeeAvailability_Available;
        }
    }

    public String siteId;
    public String userId;
    public Date timestamp;
    public int scheduled;
    public int actual;
    public EmployeeAvailability availabilty;

    @Override
    public void setValueForKey(Object value, String key) {

        if(key.equalsIgnoreCase("availabilty")) {
            if (!value.equals(null)) {
                EmployeeAvailability employeeAvailability = EmployeeAvailability.setIntValue((int) value);
                this.availabilty = employeeAvailability;
            }

        } else {
            super.setValueForKey(value, key);
        }
    }


}
