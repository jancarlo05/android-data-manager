package com.procuro.apimmdatamanagerlib;

import android.util.Log;

import org.json.JSONObject;

import java.util.Date;

/**
 * Created by jophenmarieweeks on 10/07/2017.
 */

public class DeliveryEndPointActionResult extends PimmBaseObject {

    public boolean StartPointAction ;
    public boolean EndPointAction ;

    /// <summary>
    /// POI that represents a newly assigned startpoint.
    /// </summary>
    /// <remarks>
    /// On client StartPointPOIId needs to be set or cleared depending on
    /// this value.
    /// </remarks>
    public SdrPointOfInterest StartPoint;

    /// <summary>
    /// Previously set manual startPoint that was unset do to a new
    /// startPoint being set.
    /// </summary>
    public  SdrPointOfInterest UnsetStartPoint;

    /// <summary>
    /// Previously set start point was swapped to the end point.
    /// </summary>
    public boolean StartPointSwappedToEnd;

    /// <summary>
    /// Previously set manual startPoint that was unset do to a new
    /// startPoint being set.
    /// This POI is not longer valid and was deleted.
    /// </summary>
    public String DeletedStartPoint;

    /// <summary>
    /// POI that represents a newly assigned endpoint.
    /// </summary>
    /// On client EndPointPOIId needs to be set or cleared depending on this
    /// value.
    /// </remarks>
    public SdrPointOfInterest EndPoint;

    /// <summary>
    /// Previously set manual endpoint that was unset do to a new endpoint
    /// being set.
    /// </summary>
    public  SdrPointOfInterest UnsetEndPoint;

    /// <summary>
    /// Previously set manual endpoint that was unset do to a new endpoint
    /// being set.
    /// This POI is not longer valid and was deleted.
    /// </summary>
    public String DeletedEndPoint;

    /// <summary>
    /// Previously set end point was swapped to the start point.
    /// </summary>
    public boolean EndPointSwappedToStart ;

    /// <summary>
    /// POI is deleted because converted to permanent stop.
    /// </summary>
    public boolean POIDeleted ;

    /// <summary>
    /// Updated start time. Only valid when StartPointAction is true.
    /// </summary>
    public Date StartTime ;

    /// <summary>
    /// Updated departure time. Only valid when StartPointAction is true.
    /// </summary>
    public Date DepartureTime ;

    /// <summary>
    /// Updated end time. Only valid when EndPointAction is true.
    /// </summary>
    public Date EndTime ;

    /// <summary>
    /// Updated arrival time. Only valid when EndPointAction is true.
    /// </summary>
    public Date ArrivalTime ;

    /// <summary>
    /// Updated start time. Only valid when StartPointAction is true.
    /// </summary>
    public String StartTimeText ;

    /// <summary>
    /// Updated departure time. Only valid when StartPointAction is true.
    /// </summary>
    public String DepartureTimeText ;

    /// <summary>
    /// Updated end time. Only valid when EndPointAction is true.
    /// </summary>
    public String EndTimeText ;

    /// <summary>
    /// Updated arrival time. Only valid when EndPointAction is true.
    /// </summary>
    public String ArrivalTimeText ;

    public StoreDeliveryEnums.DeliveryStatus Status ;
    public StoreDeliveryEnums.TransportStatus TransportStatus;


    public int GIS_UnscheduledCount ;
    public int GIS_UnauthorizedCount ;
    public int GIS_UnscheduledTotal ;

    public String DeliveryId ;
    public boolean Rebuild ;
    public boolean Changed ;


    @Override
    public void setValueForKey(Object value, String key) {

        if (key.equalsIgnoreCase("StartPoint")) {

            SdrPointOfInterest sdrPointOfInterest = new SdrPointOfInterest();
            JSONObject jsonObject = (JSONObject) value;
            sdrPointOfInterest.readFromJSONObject(jsonObject);
            this.StartPoint = sdrPointOfInterest;

        } else if (key.equalsIgnoreCase("UnsetStartPoint")) {

            SdrPointOfInterest sdrPointOfInterest = new SdrPointOfInterest();
            JSONObject jsonObject = (JSONObject) value;
            sdrPointOfInterest.readFromJSONObject(jsonObject);
            this.UnsetStartPoint = sdrPointOfInterest;

        } else if (key.equalsIgnoreCase("EndPoint")) {

            SdrPointOfInterest sdrPointOfInterest = new SdrPointOfInterest();
            JSONObject jsonObject = (JSONObject) value;
            sdrPointOfInterest.readFromJSONObject(jsonObject);
            this.EndPoint = sdrPointOfInterest;

        } else if (key.equalsIgnoreCase("UnsetEndPoint")) {

            SdrPointOfInterest sdrPointOfInterest = new SdrPointOfInterest();
            JSONObject jsonObject = (JSONObject) value;
            sdrPointOfInterest.readFromJSONObject(jsonObject);
            this.UnsetEndPoint = sdrPointOfInterest;

        } else if(key.equalsIgnoreCase("Status")) {

            StoreDeliveryEnums.DeliveryStatus deliveryStatus = StoreDeliveryEnums.DeliveryStatus.setIntValue((int) value);
            this.Status = deliveryStatus;

        } else if(key.equalsIgnoreCase("TransportStatus")) {

            StoreDeliveryEnums.TransportStatus mTransport = StoreDeliveryEnums.TransportStatus.setIntValue((int) value);
            this.TransportStatus = mTransport;

        } else {
            super.setValueForKey(value, key);
        }

    }


}
