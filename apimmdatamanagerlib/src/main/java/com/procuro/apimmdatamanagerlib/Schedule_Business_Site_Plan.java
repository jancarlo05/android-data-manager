package com.procuro.apimmdatamanagerlib;

import java.util.Date;

public class Schedule_Business_Site_Plan extends PimmBaseObject {

    public String siteID;
    public String userID;
    public Date date;
    public Date startTime;
    public Date endTime;
    public Date createdAt;
    public Date modifiedAt;
    public int position;

    @Override
    public void setValueForKey(Object value, String key) {
        super.setValueForKey(value, key);

        if (key.equalsIgnoreCase("date")){
            if (value!=null){
                this.date = DateTimeFormat.convertJSONDateToDate(value.toString());
            }
        }
        else if (key.equalsIgnoreCase("startTime")){
            if (value!=null){
                this.startTime = DateTimeFormat.convertJSONDateToDate(value.toString());
            }
        }
        else if (key.equalsIgnoreCase("endTime")){
            if (value!=null){
                this.endTime = DateTimeFormat.convertJSONDateToDate(value.toString());
            }
        }
       else if (key.equalsIgnoreCase("createdAt")){
            if (value!=null){
                this.createdAt = DateTimeFormat.convertJSONDateToDate(value.toString());
            }
        }
       else if (key.equalsIgnoreCase("modifiedAt")){
            if (value!=null){
                this.modifiedAt = DateTimeFormat.convertJSONDateToDate(value.toString());
            }
        }
    }
}
