package com.procuro.apimmdatamanagerlib;

import java.util.Date;

/**
 * Created by jophenmarieweeks on 07/07/2017.
 */

public class QualificationAudit extends PimmBaseObject {

     public String qualificationID;
     public String userID;
     public Date timestamp;
     public int operation;
     public int status;


}
