package com.procuro.apimmdatamanagerlib;

import java.util.Date;
import java.util.HashMap;

/**
 * Created by jophenmarieweeks on 12/07/2017.
 */

public class AuditTrailEntry extends PimmBaseObject {

    public String AuditBy;
    public Date AuditDate;
    public int TimeLogEntryType;
    public int DeliveryPOIType;
    public String TimeLogEntryId;

    public HashMap<String,Object> dictionaryWithValuesForKeys() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();

        dictionary.put("AuditBy", this.AuditBy);
        dictionary.put("AuditDate", this.AuditDate);
        dictionary.put("TimeLogEntryType", this.TimeLogEntryType);
        dictionary.put("DeliveryPOIType", this.DeliveryPOIType);
        dictionary.put("TimeLogEntryId", this.TimeLogEntryId);

        return dictionary;
    }


}
