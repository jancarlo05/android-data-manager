package com.procuro.apimmdatamanagerlib;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jophenmarieweeks on 06/07/2017.
 */

public class DashboardEventBuilder extends PimmBaseObject {


    public ArrayList<String> SpeedEvts;
    public ArrayList<String> IdleEvts;
    public ArrayList<String> RPMEvts;
    public ArrayList<String> AccEvts;
    public ArrayList<String> BrakeEvts;

//    @Override
//    public void setValueForKey(Object value, String key)
//    {
//        if(key.equalsIgnoreCase("SpeedEvts"))
//        {
//            if(this.SpeedEvts == null)
//            {
//                this.SpeedEvts = new ArrayList<String>();
//            }
//
//            List<String> SpeedEvts = (List<String>) value;
//            for(String SpeedEvt : SpeedEvts)
//            {
//                this.SpeedEvts.add(SpeedEvt);
//            }
//        }
//        else if(key.equalsIgnoreCase("IdleEvts"))
//        {
//            if(this.IdleEvts == null)
//            {
//                this.IdleEvts = new ArrayList<String>();
//            }
//
//            List<String> IdleEvts = (List<String>) value;
//            for(String IdleEvt : IdleEvts)
//            {
//                this.IdleEvts.add(IdleEvt);
//            }
//        }
//        else if(key.equalsIgnoreCase("RPMEvts"))
//        {
//            if(this.RPMEvts == null)
//            {
//                this.RPMEvts = new ArrayList<String>();
//            }
//
//            List<String> RPMEvts = (List<String>) value;
//            for(String RPMEvt : RPMEvts)
//            {
//                this.RPMEvts.add(RPMEvt);
//            }
//        }
//        else if(key.equalsIgnoreCase("AccEvts"))
//        {
//            if(this.AccEvts == null)
//            {
//                this.AccEvts = new ArrayList<String>();
//            }
//
//            List<String> AccEvts = (List<String>) value;
//            for(String AccEvt : AccEvts)
//            {
//                this.AccEvts.add(AccEvt);
//            }
//        }
//        else if(key.equalsIgnoreCase("BrakeEvts"))
//        {
//            if(this.BrakeEvts == null)
//            {
//                this.BrakeEvts = new ArrayList<String>();
//            }
//
//            List<String> BrakeEvts = (List<String>) value;
//            for(String BrakeEvt : BrakeEvts)
//            {
//                this.BrakeEvts.add(BrakeEvt);
//            }
//        }
//        else
//        {
//            super.setValueForKey(value, key);
//        }
//    }
}
