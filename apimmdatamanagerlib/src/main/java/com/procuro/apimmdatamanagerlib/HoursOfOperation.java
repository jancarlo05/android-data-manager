package com.procuro.apimmdatamanagerlib;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;

public class HoursOfOperation extends PimmBaseObject {

    public Date openTime;
    public Date closeTime;
    public boolean isBreakfast;
    public boolean isOpen24Hours;
    public boolean isOpen7Days;
    public boolean isRestaurant;
    public boolean isTestKitchen;
    public String pimmFormID;
    public String siteID;
    public StorePrepSchedule storePrepSchedule;
    public ArrayList<DayOperationSchedule>dayOperationSchedules;
    public ArrayList<HolidaySchedules>holidaySchedules;


    @Override
    public void setValueForKey(Object value, String key) {
        super.setValueForKey(value, key);

        if (key.equalsIgnoreCase("storePrepSchedule")){
            if (value!=null){
                if (!value.toString().equalsIgnoreCase("null")){
                    try {
                        JSONObject jsonObject = (JSONObject) value;
                        StorePrepSchedule data = new StorePrepSchedule();
                        data.readFromJSONObject(jsonObject);
                        this.storePrepSchedule = data;
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }

        }

        if (key.equalsIgnoreCase("dayOperationSchedules")){
            if (value!=null){
                if (!value.toString().equalsIgnoreCase("null")){
                    ArrayList<DayOperationSchedule>list = new ArrayList<>();
                    try {
                        JSONArray array = (JSONArray) value;
                        for (int i = 0; i <array.length() ; i++) {
                            DayOperationSchedule data = new DayOperationSchedule();
                            data.readFromJSONObject(array.getJSONObject(i));
                            list.add(data);
                        }
                        this.dayOperationSchedules = list;
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }

        }

        if (key.equalsIgnoreCase("holidaySchedules")){
            if (value!=null){
                if (!value.toString().equalsIgnoreCase("null")){
                    ArrayList<HolidaySchedules>list = new ArrayList<>();
                    try {
                        JSONArray array = (JSONArray) value;
                        for (int i = 0; i <array.length() ; i++) {
                            HolidaySchedules data = new HolidaySchedules();
                            data.readFromJSONObject(array.getJSONObject(i));
                            list.add(data);
                        }
                        this.holidaySchedules = list;
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }

        }

        if (key.equalsIgnoreCase("closeTime")){
            if (value!=null){
                if (!value.toString().equalsIgnoreCase("null")){
                    this.closeTime = JSONDate.convertJSONDateToNSDate(value.toString());
                }
            }
        }

        if (key.equalsIgnoreCase("openTime")){
            if (value!=null){
                if (!value.toString().equalsIgnoreCase("null")){
                    this.openTime = JSONDate.convertJSONDateToNSDate(value.toString());
                }
            }
        }
    }

}
