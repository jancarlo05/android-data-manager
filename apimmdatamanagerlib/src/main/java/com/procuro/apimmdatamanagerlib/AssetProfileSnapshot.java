package com.procuro.apimmdatamanagerlib;

import java.util.Date;

/**
 * Created by jophenmarieweeks on 06/07/2017.
 */

public class AssetProfileSnapshot extends PimmBaseObject {

    public enum AssetProfileTypeEnum {
        AssetProfile_Undefined(-1),
        AssetProfileType_Trailer(0),
        AssetProfileType_Tractor(1),
        AssetProfileType_Telematics(2),
        AssetProfileType_TelematicsWirelessSensor(3),
        AssetProfileType_Reefer(4);

        private int value;
        private AssetProfileTypeEnum(int value) {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }

        public static AssetProfileTypeEnum setIntValue (int i) {
            for (AssetProfileTypeEnum type : AssetProfileTypeEnum.values()) {
                if (type.value == i) { return type; }
            }
            return AssetProfile_Undefined;
        }
    }

//   public Integer AssetProfileTypeEnum;

    public String assetProfileSnapshotID;
    public String assetProfileID;
    public String userID;
    public String body;

    public Date startTime;
    public Date endTime;

    public AssetProfileTypeEnum assetProfileTypeEnum;

    @Override
    public void setValueForKey(Object value, String key) {

        if(key.equalsIgnoreCase("assetProfileTypeEnum")) {
            if (!value.equals(null)) {
                AssetProfileTypeEnum assetProfileType = AssetProfileTypeEnum.setIntValue((int) value);
                this.assetProfileTypeEnum = assetProfileType;
            }

        } else {
            super.setValueForKey(value, key);
        }
    }

}


