package com.procuro.apimmdatamanagerlib;

import android.util.Log;

/**
 * Created by jophenmarieweeks on 06/07/2017.
 */

public class GPSEventTypeEnum extends PimmBaseObject {



    //public enum GPSEventTypeEnum { //duplicate class error
    public enum GPSEventTypesEnum
        {
        GPSEventType_None(0),
        GPSEventType_EnterGeofence(1),
        GPSEventType_ExitGeofence(2);

        private int value;
        private GPSEventTypesEnum(int value)
        {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }

        public static GPSEventTypesEnum setIntValue (int i) {
            for (GPSEventTypesEnum type : GPSEventTypesEnum.values()) {
                if (type.value == i) { return type; }
            }
            return GPSEventType_None;
        }
    }

    public GPSEventTypesEnum gpsEventTypeEnum;

    @Override
    public void setValueForKey(Object value, String key) {

        if(key.equalsIgnoreCase("gpsEventTypeEnum")) {
            if (!value.equals(null)) {
                GPSEventTypesEnum gpsEventTypeEnum = GPSEventTypesEnum.setIntValue((int) value);
                this.gpsEventTypeEnum = gpsEventTypeEnum;

            }
         } else {
            super.setValueForKey(value, key);
        }
    }


}
