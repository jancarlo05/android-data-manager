package com.procuro.apimmdatamanagerlib;

import java.util.ArrayList;

/**
 * Created by jophenmarieweeks on 01/08/2017.
 */

public class TracRoute extends PimmBaseObject {

    public String tracRouteId;
    public  TracRecorder recorder;
    public  TracDepot plant;
    public  TracCustomer customer;
    public  TracProduct product;
    public String codeDate;
    public String sealNumber;
    public boolean  cpu;
    public boolean  backhaul;
    public boolean  supplierFleet;
    public  TracCustomer broker;
    public String carrierName;
    public String trailerName;
    public ArrayList<TracStop> stops;
}
