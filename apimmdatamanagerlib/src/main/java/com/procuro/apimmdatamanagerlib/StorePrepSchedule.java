package com.procuro.apimmdatamanagerlib;

import org.json.JSONArray;

import java.util.ArrayList;

public class StorePrepSchedule extends PimmBaseObject {

    public ArrayList<StorePrepData> lunch;
    public ArrayList<StorePrepData>breakfast;

    @Override
    public void setValueForKey(Object value, String key) {
        super.setValueForKey(value, key);

        if (key.equalsIgnoreCase("lunch")){
            if (value!=null){
                if (!value.toString().equalsIgnoreCase("null")){
                    ArrayList<StorePrepData>list = new ArrayList<>();
                    try {
                        JSONArray array = (JSONArray) value;
                        for (int i = 0; i <array.length() ; i++) {
                            StorePrepData storePrepData = new StorePrepData();
                            storePrepData.readFromJSONObject(array.getJSONObject(i));
                            list.add(storePrepData);
                        }
                        this.lunch = list;
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }

        }

        if (key.equalsIgnoreCase("breakfast")){
            if (value!=null){
                if (!value.toString().equalsIgnoreCase("null")){
                    ArrayList<StorePrepData>list = new ArrayList<>();
                    try {
                        JSONArray array = (JSONArray) value;
                        for (int i = 0; i <array.length() ; i++) {
                            StorePrepData storePrepData = new StorePrepData();
                            storePrepData.readFromJSONObject(array.getJSONObject(i));
                            list.add(storePrepData);
                        }
                        this.breakfast = list;
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }

        }

    }
}
