package com.procuro.apimmdatamanagerlib;

/**
 * Created by jophenmarieweeks on 12/07/2017.
 */

public class SdDriverQualificationSummary extends PimmBaseObject {

    public String  UserId;
    public String  Username;
    public String  FirstName;
    public String  LastName;

    public int  MandatoryRequested;
    public int  MandatorySubmitted;
    public int  MandatoryPending;
    public int  MandatoryAccepted;
    public int  MandatoryRejected;
    //public int  MandatoryDeleted;
    public int  MandatoryMailed;
    public int  MandatoryMissing;
    public int  MandatoryExpired;
    public int  OptionalRequested;
    public int  OptionalSubmitted;
    public int  OptionalPending;
    public int  OptionalAccepted;
    public int  OptionalRejected;
    //public int  OptionalDeleted;
    public int  OptionalMailed;
    public int  OptionalMissing;
    public int  OptionalExpired;


}
