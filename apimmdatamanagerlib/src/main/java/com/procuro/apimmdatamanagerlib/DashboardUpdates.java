package com.procuro.apimmdatamanagerlib;

import org.json.JSONObject;

import java.util.Date;

/**
 * Created by jophenmarieweeks on 06/07/2017.
 */

public class DashboardUpdates extends PimmBaseObject {

    public Date Timestamp;
    public int TimezoneId;
    public DashboardUpdateBuilder UpdateData;

    @Override
    public void setValueForKey(Object value, String key) {

        if(key.equalsIgnoreCase("UpdateData")) {
            DashboardUpdateBuilder dashboardUpdateBuilder = new DashboardUpdateBuilder();
            JSONObject jsonObject = (JSONObject) value;
            dashboardUpdateBuilder.readFromJSONObject(jsonObject);
            this.UpdateData = dashboardUpdateBuilder;

        } else {
            super.setValueForKey(value, key);
        }
    }
}
