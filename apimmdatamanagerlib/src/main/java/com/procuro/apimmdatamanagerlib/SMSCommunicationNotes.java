package com.procuro.apimmdatamanagerlib;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

public class SMSCommunicationNotes extends  PimmBaseObject {

   public String id;
   public String siteID;
   public String userID;
   public String userName;
   public Date timestamp;
   public String message;
   public ArrayList<String>attachments;


    @Override
    public void setValueForKey(Object value, String key) {
        super.setValueForKey(value, key);

        if (key.equalsIgnoreCase("timestamp")){
            if (value!=null){
                if (!value.toString().equalsIgnoreCase("null")){
                    this.timestamp = JSONDate.convertJSONDateToNSDate(value.toString());
                }
            }

        }

        if (key.equalsIgnoreCase("id")){
            if (value!=null){
                if (!value.toString().equalsIgnoreCase("null")){
                    this.id = (String)value.toString();
                }
            }

        }

        if (key.equalsIgnoreCase("siteID")){
            if (value!=null){
                if (!value.toString().equalsIgnoreCase("null")){
                    this.siteID = (String)value.toString();
                }
            }

        }

        if (key.equalsIgnoreCase("userID")){
            if (value!=null){
                if (!value.toString().equalsIgnoreCase("null")){
                    this.userID = (String)value.toString();
                }
            }

        }

        if (key.equalsIgnoreCase("userName")){
            if (value!=null){
                if (!value.toString().equalsIgnoreCase("null")){
                    this.userName = (String)value.toString();
                }
            }

        }

        if (key.equalsIgnoreCase("message")){
            if (value!=null){
                if (!value.toString().equalsIgnoreCase("null")){
                    this.message = (String)value.toString();
                }
            }

        }

        if (key.equalsIgnoreCase("attachments")){
            ArrayList<String>strings = new ArrayList<>();
            if (value!=null){
                JSONArray jsonArray = (JSONArray) value;
                for (int i = 0; i <jsonArray.length() ; i++) {
                    try {
                        strings.add(jsonArray.getString(i));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            }
            this.attachments = strings;
        }

    }


    public HashMap<String,Object> dictionaryWithValuesForKeys() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();

        dictionary.put("id", this.id);
        dictionary.put("siteID", this.siteID);
        dictionary.put("userID", this.userID);
        dictionary.put("userName", this.userName);
        dictionary.put("timestamp", this.timestamp);
        dictionary.put("message", this.message);
        dictionary.put("attachments", this.attachments);

        return dictionary;
    }
}
