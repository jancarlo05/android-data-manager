package com.procuro.apimmdatamanagerlib;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by jophenmarieweeks on 01/08/2017.
 */

public class RouteRegistrationRequest extends PimmBaseObject {

    public enum PickupType {
        PickupType_None(0),
        PickupType_CustomerPickup(1),
        PickupType_Backhaul(2),
        PickupType_ThirdParty(3),
        PickupType_Supplier(4);

        private int value;
        private PickupType(int value)
        {
            this.value = value;
        }


        public int getValue() {
            return this.value;
        }

        public static PickupType setIntValue (int i) {
            for (PickupType type : PickupType.values()) {
                if (type.value == i) { return type; }
            }
            return PickupType_None;
        }
    }
    public String recorderId;
    public String customerId;
    public String productId;
    public String codeDate;
    public String sealNumber;
    public PickupType pickupType;
    public String brokerId;
    public String carrierName;
    public String trailerName;
    public Date loadTime;

    public String driverUserId;
    public String driverName;
    public String driverMobileNumber;

   public ArrayList<RouteRegistrationRequestStop> stops;


    public void setValueForKey(Object value, String key) {

        if (key.equalsIgnoreCase("stops")) {
            if (this.stops == null) {
                this.stops = new ArrayList<RouteRegistrationRequestStop>();
            }

            if (!value.equals(null)) {
                JSONArray arrData = (JSONArray) value;

                if (arrData != null) {
                    for (int i = 0; i < arrData.length(); i++) {
                        try {
                            RouteRegistrationRequestStop result = new RouteRegistrationRequestStop();
                            result.readFromJSONObject(arrData.getJSONObject(i));

                            this.stops.add(result);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
//        }else if(){
        } else {
            super.setValueForKey(value, key);
        }

    }



    public class RouteRegistrationRequestLot extends PimmBaseObject{

        public String routeRegistrationRequestLotId;
        public String itemNumber;
        public String lotNumber;
        public String productDescription;
        public double binCount;
        public double caseCount;
        public Date harvestDate;
        public Date useByDate;

        public HashMap<String,Object> dictionaryWithValuesForKeys() {
            HashMap<String, Object> dictionary = new HashMap<String, Object>();

            dictionary.put("routeRegistrationRequestLotId", this.routeRegistrationRequestLotId);
            dictionary.put("itemNumber", this.itemNumber);
            dictionary.put("lotNumber", this.lotNumber);
            dictionary.put("productDescription", this.productDescription);
            dictionary.put("binCount", this.binCount);
            dictionary.put("caseCount", this.caseCount);
            dictionary.put("harvestDate", (this.harvestDate == null) ? null : DateTimeFormat.convertToJsonDateTime(this.harvestDate));
            dictionary.put("useByDate", (this.useByDate == null) ? null : DateTimeFormat.convertToJsonDateTime(this.useByDate));

            return dictionary;

        }
//- (NSString *)JSONString;

    }

    public class RouteRegistrationRequestStop extends PimmBaseObject{

        public  int routeRegistrationRequestStopId;
        public String trackKey;
        public int number;
        public String dcDepotId;
        public String deliveryOrder;
        public String po;
        public String bolNumber;
        public Date scheduledArrival;

        public ArrayList<RouteRegistrationRequestLot> lots;

        public void setValueForKey(Object value, String key) {

           if (key.equalsIgnoreCase("lots")) {
                if (this.lots == null) {
                    this.lots = new ArrayList<RouteRegistrationRequestLot>();
                }

                if (!value.equals(null)) {
                    JSONArray arrData = (JSONArray) value;

                    if (arrData != null) {
                        for (int i = 0; i < arrData.length(); i++) {
                            try {
                                RouteRegistrationRequestLot result = new RouteRegistrationRequestLot();
                                result.readFromJSONObject(arrData.getJSONObject(i));

                                this.lots.add(result);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            } else {
                super.setValueForKey(value, key);
            }

        }
        public HashMap<String,Object> dictionaryWithValuesForKeys() {
            HashMap<String, Object> dictionary = new HashMap<String, Object>();

            dictionary.put("id", this.routeRegistrationRequestStopId);
            dictionary.put("trackKey", this.trackKey);
            dictionary.put("number", this.number);
            dictionary.put("dcDepotId", this.dcDepotId);
            dictionary.put("deliveryOrder", this.deliveryOrder);
            dictionary.put("po", this.po);
            dictionary.put("bolNumber", this.bolNumber);

            dictionary.put("scheduledArrival", (this.scheduledArrival == null) ? null : DateTimeFormat.convertToJsonDateTime(this.scheduledArrival));

            if (this.lots != null) {

                ArrayList<HashMap<String, Object>> lotsDictionary = new ArrayList<>();

                for(RouteRegistrationRequestLot propertyValue : this.lots) {

                    HashMap<String, Object> propertyValueDict = propertyValue.dictionaryWithValuesForKeys();
                    lotsDictionary.add(propertyValueDict);

                }

                dictionary.put("lots", lotsDictionary);

            }else{
                dictionary.put("lots", null);
            }

            return dictionary;
        }


    }

    public class Optional extends PimmBaseObject{
        public boolean isEmpty;
        public Date value;

        public void initWithValue(Date value){

            this.isEmpty = false;
            this.value = value;
        }
        public HashMap<String,Object> dictionaryWithValuesForKeys() {
            HashMap<String, Object> dictionary = new HashMap<String, Object>();

            dictionary.put("isEmpty", this.isEmpty);
            dictionary.put("value", (this.value == null) ? null : DateTimeFormat.convertToJsonDateTime(this.value) );

            return dictionary;
        }

//- (id)initWithValue:(id) value;
//- (NSDictionary* )dictionaryWithValuesForKeys;
//- (NSString *)JSONString;

    }

    public class TracArrivalUpdate extends PimmBaseObject{
        public Optional deliveryStartTime;
        public Optional deliveryEndTime;

        public HashMap<String,Object> dictionaryWithValuesForKeys() {
            HashMap<String, Object> dictionary = new HashMap<String, Object>();

            if(this.deliveryStartTime != null){
                dictionary.put("deliveryStartTime", this.deliveryStartTime.dictionaryWithValuesForKeys());
            }

            if(this.deliveryEndTime != null){
                dictionary.put("deliveryEndTime", this.deliveryEndTime.dictionaryWithValuesForKeys());
            }



            return dictionary;
        }


//- (NSDictionary* )dictionaryWithValuesForKeys;
//- (NSString *)JSONString;
//

    }




}
