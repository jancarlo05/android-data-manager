package com.procuro.apimmdatamanagerlib;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.util.Calendar;
import java.util.Date;

import static java.lang.Math.floor;

/**
 * Created by jophenmarieweeks on 27/09/2017.
 */

public class DateTimeFormat extends PimmBaseObject {

    public static Date ConvertISOToDate(String ISO){


        Date date = null;
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        if (ISO!=null){
         try {
             date = format.parse(ISO);
         }catch (Exception e){
             e.printStackTrace();
         }
        }
        return date;
    }


    public static String TimeOnlyStringForDateTime(Date dateTime){
        SimpleDateFormat printFormat = new SimpleDateFormat("HH:mm:ss");
        String date = printFormat.format(dateTime);
        return date;
    }

    public static Date truncateSecondsForDate(Date date){

        if(date == null){
            return null;
        }
        double time =floor(date.getTime() / 60.0) * 60.0;

        Date minute = new Date((long) time);
        return minute;
    }



    public static BigDecimal truncateDecimal(double x, int numberofDecimals)
    {
        if ( x > 0) {
            return new BigDecimal(String.valueOf(x)).setScale(numberofDecimals, BigDecimal.ROUND_FLOOR);
        } else {
            return new BigDecimal(String.valueOf(x)).setScale(numberofDecimals, BigDecimal.ROUND_CEILING);
        }
    }

    public static Date convertJSONDateToDate(String jsonDateString){
        Date date = new Date();
        if(jsonDateString != null) {
            Calendar calendar = Calendar.getInstance();
            String jsondate = jsonDateString.replace("/Date(", "").replace(")/", "");
            Long timeInMillis = Long.valueOf(jsondate);
            calendar.setTimeInMillis(timeInMillis);
            date = new Date(timeInMillis);
        }

        return date;
    }
    public static String convertToJsonDateTime(Date dateToConvert) {
        String JsonDate = null;
        if(dateToConvert == null) {
            JsonDate = null;
        }else if(dateToConvert.equals("/Date(nan000)/")){
            JsonDate = null;
        }else if(dateToConvert.equals("<null>")){
            JsonDate = null;
        } else {

            long time = dateToConvert.getTime();
            JsonDate = "/Date(" + time + "+0000)/";
        }

        return  JsonDate;
    }

    public static String ShortFormatStringForDateTime(Date date){

        String stringDate = DateFormat.getDateTimeInstance().format(date);
        return stringDate;

    }

    public static boolean isBetweenDate(Date dateStopStart, Date dateStopEnd) {
        Date now = new Date();

        if (now.after(dateStopStart) && now.before(dateStopEnd) )
        {
            return true;
        }

        return false ;
    }


}
