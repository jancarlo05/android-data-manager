package com.procuro.apimmdatamanagerlib;

import org.json.JSONObject;

import java.util.Date;

/**
 * Created by jophenmarieweeks on 06/07/2017.
 */

public class AssetProfile extends PimmBaseObject {

    public AssetProfileSnapshot assetProfileSnapshot;
    public String assetProfileID;
    public String deviceID;
    public String anchorProfileID;


    @Override
    public void setValueForKey(Object value, String key) {

        if (key.equalsIgnoreCase("assetProfileSnapshot")) {
            if (!value.equals(null)) {
                AssetProfileSnapshot AssetProfile = new AssetProfileSnapshot();
                JSONObject jsonObject = (JSONObject) value;
                AssetProfile.readFromJSONObject(jsonObject);

                this.assetProfileSnapshot = AssetProfile;
            }
        } else {
            super.setValueForKey(value, key);
        }
    }



}
