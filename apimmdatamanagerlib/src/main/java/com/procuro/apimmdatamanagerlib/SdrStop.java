package com.procuro.apimmdatamanagerlib;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by jophenmarieweeks on 10/07/2017.
 */

public class SdrStop extends PimmBaseObject {

    public enum SdrStopRecordType {
        SdrStopRecordType_Stop(0),
        SdrStopRecordType_Violation(1),
        SdrStopRecordType_POI(2);

        private int value;
        SdrStopRecordType(int value)
        {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }

        public static SdrStopRecordType setIntValue (int i) {
            for (SdrStopRecordType type : SdrStopRecordType.values()) {
                if (type.value == i) { return type; }
            }
            return SdrStopRecordType_Stop;
        }
    }

    public enum SiteType {
        SiteType_Standard(0),
        SiteType_Restaurant(50),
        SiteType_NonDeliverableStop(51),
        SiteType_OnTheRoad(200),
        SiteType_OutOfService(300),
        SiteType_SupplierDeliveryTrailers(1000),
        SiteType_SupplierDeliveryTagsLegacy(1001),
        SiteType_SupplierDeliveryTags(1002),
        SiteType_SupplierDeliveryPlantOutGen2(1003),
        SiteType_SupplierDeliveryDCInGen2(1004),
        SiteType_SupplierDeliveryCustomerOutGen2(1005),
        SiteType_CarrierTransportSite(1006),
        SiteType_PlantActiveShipments(1007),
        SiteType_DCActiveShipments(1008),
        SiteType_QSRActiveShipments(1009),
        SiteType_CarrierActiveShipments(1010),
        SiteType_StoreTrailers(1020),
        SiteType_StoreRoutes(1021),
        SiteType_StoreCustomerRoutes(1022),
        SiteType_SUTrailers(1025),
        SiteType_SupplierDeliveryPlantOut(1030),
        SiteType_upplierDeliveryDCIn(1031),
        SiteType_SupplierDeliveryCustomerOut(1032),
        SiteType_SupplierDeliveryCustomerIn(1033),
        SiteType_Test1(32701),
        SiteType_Test2(32702),
        SiteType_Test3(32703),
        SiteType_SysTrash(32710);

        private int value;
        private SiteType(int value)
        {
            this.value=value;
        }

        public int getValue() {
            return this.value;
        }

        public static SiteType setIntValue (int i) {
            for (SiteType type : SiteType.values()) {
                if (type.value == i) { return type; }
            }
            return SiteType_Standard;
        }
    }

   public enum DeliveryStopStatus {
        DeliveryStopStatus_NoStop(0),
        DeliveryStopStatus_InStop(1),
        DeliveryStopStatus_StopMade(2),
        DeliveryStopStatus_Skipped(3);

        private int value;
        private DeliveryStopStatus(int value)
        {
            this.value =value;
        }
        public int getValue() {
    return this.value;
}

       public static DeliveryStopStatus setIntValue (int i) {
           for (DeliveryStopStatus type : DeliveryStopStatus.values()) {
               if (type.value == i) { return type; }
           }
           return DeliveryStopStatus_NoStop;
       }
    }

    public enum OnTimeStatus {
        OnTimeStatus_None(0),
        OnTimeStatus_OnTime(1),
        OnTimeStatus_Late(2),
        OnTimeStatus_Early(3);

        private int value;
        private OnTimeStatus(int value)
        {
            this.value =value;
        }

        public int getValue() {
            return this.value;
        }

        public static OnTimeStatus setIntValue (int i) {
            for (OnTimeStatus type : OnTimeStatus.values()) {
                if (type.value == i) { return type; }
            }
            return OnTimeStatus_None;
        }
    }

    public enum PaymentTerms {
        PaymentTerms_Net(0),
        PaymentTerms_Cash(1),
        PaymentTerms_Check(2),
        PaymentTerms_CreditCard(3);

        private int value;
        private PaymentTerms(int value)
        {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }

        public static PaymentTerms setIntValue (int i) {
            for (PaymentTerms type : PaymentTerms.values()) {
                if (type.value == i) { return type; }
            }
            return PaymentTerms_Net;
        }
    }

    public enum DietaryRestrictions {
        DietaryRestrictions_None(0),
        DietaryRestrictions_Halal(1),
        DietaryRestrictions_Kosher(2),
        DietaryRestrictions_FoodAllergy(4),
        DietaryRestrictions_Vegan(8);

        private int value;

        DietaryRestrictions(int value)
        {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }

        public static DietaryRestrictions setIntValue (int i) {
            for (DietaryRestrictions type : DietaryRestrictions.values()) {
                if (type.value == i) { return type; }
            }
            return DietaryRestrictions_None;
        }
    }

    public class SdrOnTimeRule {
        public String RuleId;
        public String Name;
        public int LateThreshold;
        public int EarlyThreshold;

        public HashMap<String,Object> dictionaryWithValuesForKeys() {
            HashMap<String, Object> dictionary = new HashMap<String, Object>();

            dictionary.put("RuleId", this.RuleId);
            dictionary.put("Name", this.Name);
            dictionary.put("LateThreshold", this.LateThreshold);
            dictionary.put("EarlyThreshold", this.EarlyThreshold);

            return dictionary;
        }

    }


    public SdrStopRecordType RecordType;
    public String SdrStopId;
    public String Address;
    public String CustomerName;
    public String SiteId;
    public String SiteName;
    public double FlagStatus;
    public SiteType Type;
    public String siteType;
    public LatLon LatLon;
    public double DeliveryOrder;
    public double OriginalDeliveryOrder;
    public String StopText;
    public String POText;
    public String PO;
    public String PO1;
    public String PO2;
    public String PO3;
    public int GeoFenceRadius;
    public DeliveryStopStatus Status;
    public Boolean TemperatureOK;
    public String TemperatureComplianceText;
    public Boolean StopMade;
    public Boolean StopMissed;
    public String StatusText;
    public Date ArrivalTime;
    public String ArrivalTimeText;
    public String ArrivalTimeISO;
    public Date DepartureTime;
    public String DepartureTimeText;
    public String DepartureTimeISO;
    public Date ExpectedDeliveryTime;
    public String ExpectedDeliveryTimeText;
    public String ExpectedDeliveryTimeISO;
    public Date DataEndDate;
    public int MinutesLate;
    public String MinutesLateText;
    public double Duration;
    public String DurationText;

    public double DriveTime;

    public String DriveTimeText;

    public double DriveTimeFromDC;
    public String DriveTimeFromDCText;
    public String DriveTimeFrom;
    public double FreezerCases;
    public double CoolerCases;
    public double DryCases;
    public double TotalCases;
    public double FreezerCubes;
    public double CoolerCubes;
    public double DryCubes;
    public double TotalCubes;
    public double Weight;
    public String IconSrc;
//    @property (nonatomic,retain) SdrOnTimeRule *OnTimeRule;
    public SdrOnTimeRule OnTimeRule;
    public OnTimeStatus OnTime;
    public double UnauthorizedCount;
    public double UnscheduledCount;
    public double UnscheduledTotal;
    public String BackhaulId;


//Array of SdrStopMetric
    public List<SdrStopMetric> Metrics;

//Array of SdrStopCompliance
    public ArrayList<SdrStopCompliance> Compliance;

    public AddressComponents AddressComponents;

    public ArrayList<SdrStopProduct>  StopProduct;

    public ArrayList<RouteShipmentEquipmentDTO> StopEquipment;

    public double DeliveryDuration;
    public String DeliveryDurationText;
    public Date AnticipatedDeliveryTime;
    public String AnticipatedDeliveryTimeText;
    public Date ExpectedArrivalTime;
    public String ExpectedArrivalTimeText;
    public Date DeliveryStartTime;
    public String DeliveryStartTimeText;
    public Date DeliveryEndTime;
    public String DeliveryEndTimeText;
    public Date ManualArrive;
    public String ManualArriveText;
    public Date ManualDeliveryEnd;
    public String ManualDeliveryEndText;
    public Date ManualDepart;
    public String ManualDepartText;
    public Date ManualDeliveryStart;
    public String ManualDeliveryStartText;
    public double POD_Status;
    public double ProductSource;
    public String CustomerId;
    public int DeliveryNumber;
    public int UnadjustedDeliveryOrder;

    public String DeliveryInstructions;
    public SiteContact Contact;
    public Fuel Fuel;
    public String StopID;

    public Date DeliveryConfirmationTime;
    public String DeliveryConfirmationText;
    public double ExpectedDeliveryDuration;

    public ArrayList<RouteStatusInvoiceItem> StopInvoiceItem;

    public String Owner;
    public String BillTo;

    public String ArmCode;
    public String DisarmCode;
    public String KeyNumber;
    public PaymentTerms paymentTerms;
    public int PaymentTermsDays;
    public boolean BarcodeEnabled;
    public DietaryRestrictions dietaryRestrictions;
    public boolean keyStop;;


    @Override
    public void setValueForKey(Object value, String key) {

        if (key.equalsIgnoreCase("Id")) {
            this.SdrStopId = String.valueOf(value);

        }
        else if (key.equalsIgnoreCase("keyStop")) {
            if (DMUtils.ValidObject(value)) {
                try {
                    this.BarcodeEnabled = DMUtils.ConvertToBoolean(value);
                } catch (Exception ignored) {
                }
            }
        }
        else if (key.equalsIgnoreCase("BarcodeEnabled")) {
            if (DMUtils.ValidObject(value)) {
                try {
                    this.BarcodeEnabled = DMUtils.ConvertToBoolean(value);
                } catch (Exception ignored) {
                }
            }
        }
        else if (key.equalsIgnoreCase("dietaryRestrictions")) {
            if (value!=null) {
                try {
                    this.dietaryRestrictions = DietaryRestrictions.setIntValue(Integer.parseInt(value.toString()));
                }catch (Exception ignored){
                }
            }
        }
        else if (key.equalsIgnoreCase("RecordType")) {

            if (!value.equals(null)) {
                SdrStopRecordType sdrStopRecordType = SdrStopRecordType.setIntValue((int) value);
                this.RecordType = sdrStopRecordType;
            }
        }

        else if (key.equalsIgnoreCase("Type")) {

            if (!value.equals(null)) {
                SiteType siteType1 = SiteType.setIntValue((int) value);
                this.Type = siteType1;
            }

        }

        else if (key.equalsIgnoreCase("Status")) {

            if (!value.equals(null)) {
                DeliveryStopStatus deliveryStopStatus = DeliveryStopStatus.setIntValue((int) value);
                this.Status = deliveryStopStatus;
            }

        }

        else if (key.equalsIgnoreCase("OnTime")) {

            if (value != null) {
                try {
                    this.OnTime = OnTimeStatus.setIntValue((int) value);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

        }

        else if (key.equalsIgnoreCase("paymentTerms")) {

            if (!value.equals(null)) {
                PaymentTerms paymentTerm = PaymentTerms.setIntValue((int) value);
                this.paymentTerms = paymentTerm;
            }
        }

        else if (key.equalsIgnoreCase("AddressComponents")) {

            if (!value.equals(null)) {
                AddressComponents addressComponents = new AddressComponents();
                JSONObject jsonObject = (JSONObject) value;
                addressComponents.readFromJSONObject(jsonObject);
                this.AddressComponents = addressComponents;
            }

        }

        else if (key.equalsIgnoreCase("Contact")) {

            if (!value.equals(null)) {
                SiteContact siteContact = new SiteContact();
                JSONObject jsonObject = (JSONObject) value;
                siteContact.readFromJSONObject(jsonObject);

                this.Contact = siteContact;
            }

//            Log.v(">>> Contact", "Contact");

        }

        else if (key.equalsIgnoreCase("LatLon")) {

            if (!value.equals(null)) {
                JSONObject jsonObject = (JSONObject) value;

                LatLon latLon = new LatLon();

                try {
                    latLon.Lat = jsonObject.getDouble("Lat");
                    latLon.Lon = jsonObject.getDouble("Lon");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                this.LatLon = latLon;
            }

        }

        else if (key.equalsIgnoreCase("Fuel")) {

            if (!value.equals(null)) {
                Fuel fuel = new Fuel();
                JSONObject jsonObject = (JSONObject) value;
                fuel.readFromJSONObject(jsonObject);

                this.Fuel = fuel;
            }

//            Log.v(">>> Fuel", "Fuel");

        }

        else if (key.equalsIgnoreCase("Metrics")) {
            if (this.Metrics == null)
                this.Metrics = new ArrayList<SdrStopMetric>();

            if(!value.equals(null)) {
                if (value instanceof JSONArray) {
                    JSONArray arrData = (JSONArray) value;

                    if (arrData != null) {

                        for (int i = 0; i < arrData.length(); i++) {
                            try {
                                SdrStopMetric sdrStopMetric = new SdrStopMetric();
                                sdrStopMetric.readFromJSONObject(arrData.getJSONObject(i));

                                this.Metrics.add(sdrStopMetric);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }
        }

        else if (key.equalsIgnoreCase("Compliance")) {

            if (this.Compliance == null) {
                this.Compliance = new ArrayList<SdrStopCompliance>();
            }

            if (!value.equals(null)) {
                if (value instanceof JSONArray) {

                    JSONArray arrStops = (JSONArray) value;

                    if (arrStops != null) {
                        for (int i = 0; i < arrStops.length(); i++) {
                            try {
                                SdrStopCompliance sdrStopCompliance = new SdrStopCompliance();
                                sdrStopCompliance.readFromJSONObject(arrStops.getJSONObject(i));

                                this.Compliance.add(sdrStopCompliance);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }

            }
        }

        else if (key.equalsIgnoreCase("StopProduct")) {
            this.StopProduct = new ArrayList<>();
            if (DMUtils.ValidObject(value)) {
                if (value instanceof JSONArray) {
                    JSONArray arrStops = (JSONArray) value;
                    for (int i = 0; i < arrStops.length(); i++) {
                        try {
                            SdrStopProduct sdrStopProduct = new SdrStopProduct();
                            sdrStopProduct.readFromJSONObject(arrStops.getJSONObject(i));
                            this.StopProduct.add(sdrStopProduct);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }else if (value instanceof JSONObject){
                    JSONObject jsonObject = (JSONObject) value;
                    try {
                        SdrStopProduct sdrStopProduct = new SdrStopProduct();
                        sdrStopProduct.readFromJSONObject(jsonObject);
                        this.StopProduct.add(sdrStopProduct);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        else if (key.equalsIgnoreCase("StopEquipment")) {

            if (this.StopEquipment == null) {
                this.StopEquipment = new ArrayList<RouteShipmentEquipmentDTO>();
            }

            if (!value.equals(null)) {
                if (value instanceof JSONArray) {

                    JSONArray arrStops = (JSONArray) value;

                    if (arrStops != null) {
                        for (int i = 0; i < arrStops.length(); i++) {
                            try {
                                RouteShipmentEquipmentDTO routeShipmentEquipmentDTO = new RouteShipmentEquipmentDTO();
                                routeShipmentEquipmentDTO.readFromJSONObject(arrStops.getJSONObject(i));

                                this.StopEquipment.add(routeShipmentEquipmentDTO);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                }
            }

        }

        else if (key.equalsIgnoreCase("StopInvoiceItem")) {

            if (this.StopInvoiceItem == null) {
                this.StopInvoiceItem = new ArrayList<RouteStatusInvoiceItem>();
            }

            if (!value.equals(null)) {
                if (value instanceof JSONArray) {

                    JSONArray arrStops = (JSONArray) value;

                    if (arrStops != null) {
                        for (int i = 0; i < arrStops.length(); i++) {
                            try {
                                RouteStatusInvoiceItem routeStatusInvoiceItem = new RouteStatusInvoiceItem();
                                routeStatusInvoiceItem.readFromJSONObject(arrStops.getJSONObject(i));

                                this.StopInvoiceItem.add(routeStatusInvoiceItem);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                }
            }
        }

        else if(key.equalsIgnoreCase("DietaryRestrictions")) {

        }
        else if(key.equalsIgnoreCase("ArrivalTime")) {
            this.ArrivalTime = JSONDate.convertJSONDateToNSDate(value.toString());
        }

        else if(key.equalsIgnoreCase("DepartureTime")) {
            this.DepartureTime = JSONDate.convertJSONDateToNSDate(value.toString());
        }

        else if(key.equalsIgnoreCase("ExpectedArrivalTime")) {
            this.ExpectedArrivalTime = JSONDate.convertJSONDateToNSDate(value.toString());
        }

        else if(key.equalsIgnoreCase("ExpectedDeliveryTime")) {
            if (value!=null){
                this.ExpectedDeliveryTime = JSONDate.convertJSONDateToNSDate(value.toString());
            }
        }


        else {
            if(value != null) {
                super.setValueForKey(value, key);
            }
        }
    }




    public HashMap<String,Object> dictionaryWithValuesForKeys() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();

        dictionary.put("RecordType", this.RecordType);

        dictionary.put("SdrStopId", this.SdrStopId);
        dictionary.put("Address", this.Address);
        dictionary.put("CustomerName", this.CustomerName);
        dictionary.put("SiteId", this.SiteId);
        dictionary.put("SiteName", this.SiteName);
        dictionary.put("FlagStatus", this.FlagStatus);
        dictionary.put("Type", this.Type);
        dictionary.put("siteType", this.siteType);
        dictionary.put("LatLon", (this.LatLon == null) ? null : this.LatLon.dictionaryWithValuesForKeys());

        dictionary.put("DeliveryOrder", this.DeliveryOrder);
        dictionary.put("OriginalDeliveryOrder", this.OriginalDeliveryOrder);
        dictionary.put("StopText", this.StopText);
        dictionary.put("POText", this.POText);
        dictionary.put("PO", this.PO);
        dictionary.put("PO1", this.PO1);
        dictionary.put("PO2", this.PO2);
        dictionary.put("PO3", this.PO3);
        dictionary.put("GeoFenceRadius", this.GeoFenceRadius);
        dictionary.put("Status", this.Status);
        dictionary.put("TemperatureOK", this.TemperatureOK);
        dictionary.put("TemperatureComplianceText", this.TemperatureComplianceText);
        dictionary.put("StopMade", this.StopMade);
        dictionary.put("StopMissed", this.StopMissed);
        dictionary.put("StatusText", this.StatusText);
        dictionary.put("ArrivalTime", this.ArrivalTime);
        dictionary.put("ArrivalTimeText", this.ArrivalTimeText);
        dictionary.put("ArrivalTimeISO", this.ArrivalTimeISO);
        dictionary.put("DepartureTime", this.DepartureTime);
        dictionary.put("DepartureTimeText", this.DepartureTimeText);
        dictionary.put("DepartureTimeISO", this.DepartureTimeISO);
        dictionary.put("ExpectedDeliveryTime", this.ExpectedDeliveryTime);
        dictionary.put("ExpectedDeliveryTimeText", this.ExpectedArrivalTimeText);
        dictionary.put("ExpectedDeliveryTimeISO", this.ExpectedDeliveryTimeISO);
        dictionary.put("DataEndDate", this.DataEndDate);
        dictionary.put("MinutesLate", this.MinutesLate);
        dictionary.put("MinutesLateText", this.MinutesLateText);
        dictionary.put("Duration", this.Duration);
        dictionary.put("DurationText", this.DurationText);
        dictionary.put("DriveTime", this.DriveTime);
        dictionary.put("DriveTimeText", this.DriveTimeText);
        dictionary.put("DriveTimeFromDC", this.DriveTimeFromDC);
        dictionary.put("DriveTimeFromDCText", this.DriveTimeFromDCText);
        dictionary.put("DriveTimeFrom", this.DriveTimeFrom);
        dictionary.put("FreezerCases", this.FreezerCases);
        dictionary.put("CoolerCases", this.CoolerCases);
        dictionary.put("DryCases", this.DryCases);
        dictionary.put("TotalCases", this.TotalCases);
        dictionary.put("FreezerCubes", this.FreezerCubes);
        dictionary.put("CoolerCubes", this.CoolerCubes);
        dictionary.put("DryCubes", this.DryCubes);
        dictionary.put("TotalCubes", this.TotalCubes);
        dictionary.put("Weight", this.Weight);
        dictionary.put("IconSrc", this.IconSrc);
        dictionary.put("OnTimeRule", (this.OnTimeRule == null) ? null : this.OnTimeRule.dictionaryWithValuesForKeys());
        dictionary.put("AddressComponents", (this.AddressComponents == null) ? null : this.AddressComponents.dictionaryWithValuesForKeys());
        dictionary.put("UnauthorizedCount", this.UnauthorizedCount);
        dictionary.put("UnscheduledCount", this.UnscheduledCount);
        dictionary.put("UnscheduledTotal", this.UnscheduledTotal);
        dictionary.put("BackhaulId", this.BackhaulId);
        dictionary.put("BarcodeEnabled", this.BarcodeEnabled);
        dictionary.put("DietaryRestrictions", this.dietaryRestrictions);

        if (this.Metrics != null) {

            ArrayList<HashMap<String, Object>> MetricList = new ArrayList<>();

            for(SdrStopMetric metric : this.Metrics) {

                HashMap<String, Object> metricDic = metric.dictionaryWithValuesForKeys();
                MetricList.add(metricDic);

            }

            dictionary.put("Metrics", MetricList);

        }else{
            dictionary.put("Metrics", null);
        }

        if (this.StopProduct != null) {

            ArrayList<HashMap<String, Object>> StopProductList = new ArrayList<>();

            for(SdrStopProduct product : this.StopProduct) {

                HashMap<String, Object> productDic = product.dictionaryWithValuesForKeys();
                StopProductList.add(productDic);

            }

            dictionary.put("StopProduct", StopProductList);

        }else{
            dictionary.put("StopProduct", null);
        }


        if (this.StopEquipment != null) {

            ArrayList<HashMap<String, Object>> stopEquipmentDicArray = new ArrayList<>();

            for(RouteShipmentEquipmentDTO equipment : this.StopEquipment) {

                HashMap<String, Object> equipmentDic = equipment.dictionaryWithValuesForKeys();
                stopEquipmentDicArray.add(equipmentDic);

            }

            dictionary.put("StopEquipment", stopEquipmentDicArray);

        }else{
            dictionary.put("StopEquipment", null);
        }



        return dictionary;
    }

}
