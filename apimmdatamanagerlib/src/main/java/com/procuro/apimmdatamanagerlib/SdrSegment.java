package com.procuro.apimmdatamanagerlib;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by jophenmarieweeks on 12/07/2017.
 */

public class SdrSegment extends PimmBaseObject {

    public String RouteSegmentId;
    public String ShipmentId;
    public int SupplierDeliveryStopId;
    public int ReceiverDeliveryStopId;
    public String ViolationAlgorithm;
    public Date SuBookingTime;
    public Date ReBookingTime;
    public String CarrierOrder;
    public String attributes;
    public int SuDetentionTime; //Can be null
    public int ReDetentionTime; //Can be null

    public ArrayList<SdrStopMetric> Metrics; //List of SdrStopMetric Objects (active metrics) for this segment
    public ArrayList<SdrStopCompliance> Compliance; //List of SdrSopCompliance Objects (computed compliance values for this segment



    @Override
    public void setValueForKey(Object value, String key)
    {
        if(key.equalsIgnoreCase("Compliance")) {
            if (this.Compliance == null) {
                this.Compliance = new ArrayList<SdrStopCompliance>();
            }

            if (value instanceof JSONArray) {
                JSONArray arrData = (JSONArray) value;

                if (arrData != null) {
                    for (int i = 0; i < arrData.length(); i++) {
                        try {
                            SdrStopCompliance result = new SdrStopCompliance();
                            result.readFromJSONObject(arrData.getJSONObject(i));

                            this.Compliance.add(result);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

        }else if(key.equalsIgnoreCase("Metrics"))
            {
                if (this.Metrics == null) {
                    this.Metrics = new ArrayList<SdrStopMetric>();
                }

                if (value instanceof JSONArray) {
                    JSONArray arrData = (JSONArray) value;

                    if (arrData != null) {
                        for (int i = 0; i < arrData.length(); i++) {
                            try {
                                SdrStopMetric result = new SdrStopMetric();
                                result.readFromJSONObject(arrData.getJSONObject(i));

                                this.Metrics.add(result);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
        } else {
         super.setValueForKey(value, key);
        }
    }

}
