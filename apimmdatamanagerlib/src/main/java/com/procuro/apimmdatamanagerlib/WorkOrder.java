package com.procuro.apimmdatamanagerlib;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by jophenmarieweeks on 10/07/2017.
 */

public class WorkOrder extends PimmBaseObject {

    public enum WorkOrderStatusEnum
    {
        WorkOrderStatusEnum_Undefined(-1),
        WorkOrderStatusEnum_Unscheduled(0),
        WorkOrderStatusEnum_Scheduled(1),
        WorkOrderStatusEnum_InProgress(2),
        WorkOrderStatusEnum_Completed(3),
        WorkOrderStatusEnum_Verified(4),
        WorkOrderStatusEnum_Cancelled(5);

        private int value;
        private WorkOrderStatusEnum(int value)
        {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }

        public static WorkOrderStatusEnum setIntValue (int i) {
            for (WorkOrderStatusEnum type : WorkOrderStatusEnum.values()) {
                if (type.value == i) { return type; }
            }
            return WorkOrderStatusEnum_Undefined;
        }

    };

    public String workOrderID;
    public String workOrderNumber;
    public String userID;
    public String deviceID;
    public Date creationDate;
    public WorkOrderStatusEnum status;
    public ArrayList<WorkOrderService> serviceList;

    @Override
    public void setValueForKey(Object value, String key) {

        if(key.equalsIgnoreCase("status")) {
            if (!value.equals(null)) {
                WorkOrderStatusEnum workOrderStatusEnum = WorkOrderStatusEnum.setIntValue((int) value);
                this.status = workOrderStatusEnum;
            }
        } else if (key.equalsIgnoreCase("serviceList")) {

            if (this.serviceList == null) {
                this.serviceList = new ArrayList<WorkOrderService>();
            }

            if (!value.equals(null)) {
                if (value instanceof JSONArray) {

                    JSONArray arrStops = (JSONArray) value;

                    if (arrStops != null) {
                        for (int i = 0; i < arrStops.length(); i++) {
                            try {
                                WorkOrderService workOrderService = new WorkOrderService();
                                workOrderService.readFromJSONObject(arrStops.getJSONObject(i));

                                this.serviceList.add(workOrderService);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                }
            }

        } else {
            super.setValueForKey(value, key);
        }
    }


}
