package com.procuro.apimmdatamanagerlib;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by jophenmarieweeks on 10/07/2017.
 */

public class SdrFleetReport extends PimmBaseObject {

    public Date Timestamp;
    public String  TimestampText;
    public String Title;
    public int TimezoneId;

//Array of SdrDeliveryTrailer objects
    public ArrayList<SdrDeliveryTrailer> OnRoadFleetReport;
//Array of SdrDeliveryTrailer objects
    public ArrayList<SdrDeliveryTrailer> OnSiteFleetReport;
    public String IconSet;

    @Override
    public void setValueForKey(Object value, String key)
    {
        if(key.equalsIgnoreCase("OnRoadFleetReport")) {
            if (this.OnRoadFleetReport == null) {
                this.OnRoadFleetReport = new ArrayList<SdrDeliveryTrailer>();
            }

            if (!value.equals(null)) {
                JSONArray arrStops = (JSONArray) value;

                if (arrStops != null) {
                    for (int i = 0; i < arrStops.length(); i++) {
                        try {
                            SdrDeliveryTrailer result = new SdrDeliveryTrailer();
                            result.readFromJSONObject(arrStops.getJSONObject(i));

                            this.OnRoadFleetReport.add(result);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }

            }
        }else if(key.equalsIgnoreCase("OnSiteFleetReport")) {
            if (this.OnSiteFleetReport == null) {
                this.OnSiteFleetReport = new ArrayList<SdrDeliveryTrailer>();
            }

            if (!value.equals(null)) {
                JSONArray arrStops = (JSONArray) value;

                if (arrStops != null) {
                    for (int i = 0; i < arrStops.length(); i++) {
                        try {
                            SdrDeliveryTrailer result = new SdrDeliveryTrailer();
                            result.readFromJSONObject(arrStops.getJSONObject(i));

                            this.OnSiteFleetReport.add(result);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }

            }
        }else {
            super.setValueForKey(value, key);
        }
    }

}
