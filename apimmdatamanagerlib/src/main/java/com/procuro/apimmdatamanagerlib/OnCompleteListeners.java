package com.procuro.apimmdatamanagerlib;

import com.procuro.apimmdatamanagerlib.DashboardDTOs.FM_FacilityDashboardDTO;
import com.procuro.apimmdatamanagerlib.DashboardDTOs.StD_CustomerDashboardDTO;
import com.procuro.apimmdatamanagerlib.DashboardDTOs.StD_DistributorDashboardDTO;
import com.procuro.apimmdatamanagerlib.DashboardDTOs.SuD_CustomerDashboard;
import com.procuro.apimmdatamanagerlib.DashboardDTOs.SuD_DistributorDashboard;
import com.procuro.apimmdatamanagerlib.DashboardDTOs.SuD_SupplierDashboard;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by EmmanKusumi on 7/4/17.
 */

public class OnCompleteListeners
{
    @Deprecated
    public interface OnError {
        void onError(String error);
    }

    @Deprecated
    public interface OnTaskCompleteListener
    {
        void onTaskComplete(JSONObject object);
    }

    public interface OnRestConnectionCompleteListener
    {
        void onRestConnectionComplete(Object object, Error error);
    }

    public interface OnLoginCompleteListener
    {
        void onLoginComplete(User user, Error error);
    }
    public interface OnLoginPODRecoveryCompleteListener
    {
        void onLoginCompletePODRecovery( Error error);
    }

    public interface OnGetUserRoleCompleteListener {
        void onGetUserRoleComplete(ArrayList<User> userList, Error error);
    }
    public interface getRouteShipmentProductListForSiteIdListener {
        void getRouteShipmentProductListForSiteId(ArrayList<RouteShipmentProduct> routeShipmentProductList, Error error);
    }
    public interface OnGetSiteWithSiteIdListener {
        void onGetWithSiteId(Site site, Error error);
    }
    public interface OnGetTrailerReportForTrailerIdListener {
        void onGetTrailerReportForTrailerId(SdrReport sdrReport, Error error);
    }
    public interface OnCreateRouteShipmentListener {
        void OnCreateRouteShipment(RouteShipmentResult routeShipmentResult, Error error);
    }
    public interface OnUpdateRouteShipmentListener {
        void onnupdateRouteShipment(RouteShipmentResult routeShipmentResult, Error error);
    }
    public interface getRouteShipmentListener {
        void getRouteShipment(RouteShipment routeShipment, Error error);
    }
    public interface getCustomerSettingsListener {
        void getCustomerSettings(CustomerSettings customerSettings, Error error);
    }
    public interface getTrailerSettingsForSiteIdListener {
        void getTrailerSettingsForSiteId(TrailerSettings trailerSettings, Error error);
    }
    public interface getTractorSettingsForSiteIdListener {
        void getTractorSettingsForSiteId(TrailerSettings tractorSettings, Error error);
    }
    public interface getStoreProviderSiteSettingsForSiteIdListener {
        void getStoreProviderSiteSettingsForSiteId(StoreProviderSiteSettings storeProviderSiteSettings, Error error);
    }
    public interface getDriverDataForSiteIdListener {
        void getDriverDataForSiteId(DriverData driverData, Error error);
    }
    public interface getRouteShipmentProductForProductIdListener {
        void getRouteShipmentProductForProductId(RouteShipmentProduct routeShipmentProduct);
    }
    public interface OnCreateBorderCrossingEventForDeviceIdListener {
        void createBorderCrossingEventForDeviceId(Error error);
    }
    public interface getBorderCrossingDataForShipmentIdListener {
        void getBorderCrossingDataForShipmentId(LocationDTO borderCrossingData);
    }
    public interface getBorderCrossingDataForDeviceIdListener {
        void getBorderCrossingDataForDeviceId(LocationDTO borderCrossingData, Error error);
    }
    public interface getModifyPOIStatusForPOIIdListener {
        void modifyPOIStatusForPOIId(DeliveryPOIActionResult result, Error error);
    }
    public interface modifyPOIStatusWithFuelListener {
        void modifyPOIStatusWithFuel(DeliveryPOIActionResult result, Error error);
    }
    public interface OnCreatePermanentStopForDeliveryIdListener {
        void createPermanentStopForDeliveryId(RouteShipmentCreateStopResult RouteShipmentCreateStopResult, Error error);
    }
    public interface OnCreatePermanentStopForDeliveryId2Listener {
        void createPermanentStopForDeliveryId2(DeliveryPOIActionResult result, Error error);
    }
    public interface OnCreatePermanentStopWithFuelForDeliveryId {
        void createPermanentStopWithFuelForDeliveryId(DeliveryPOIActionResult result, Error error);
    }
    public interface OncreatePOIForDeliveryIdListener {
        void createPOIWithFuelForDeliveryId(DeliveryPOIActionResult result, Error error);
    }
    public interface OnCreatePOIWithFuelForDeliveryIdListener {
        void createPOIWithFuelForDeliveryId(DeliveryPOIActionResult result, Error error);
    }
    public interface OnDeleteManualPOIForDeliveryIdListener {
        void deleteManualPOIForDeliveryId(DeliveryPOIActionResult result, Error error);
    }
    public interface OnChangePOIToStartForDeliveryIdListener {
        void changePOIToStartForDeliveryId(DeliveryEndPointActionResult deliveryEndpointActionResult, Error error);
    }
    public interface OnchangePOIToEndForDeliveryIdListener {
        void changePOIToEndForDeliveryId(DeliveryEndPointActionResult deliveryEndpointActionResult, Error error);
    }
    public interface OnEndRouteHereForDeliveryIdListener {
        void endRouteHereForDeliveryId(DeliveryEndPointActionResult deliveryEndpointActionResult);
    }
    public interface OnUnsetPOIAsEndPointForDeliveryIdListener {
        void unsetPOIAsEndPointForDeliveryId(DeliveryEndPointActionResult deliveryEndpointActionResult, Error error);
    }
     public interface SuggestStopNameForLatitudeListener {
        void suggestStopNameForLatitude(SuggestStopNameResult SuggestedName, Error error);
    }
     public interface EndOfRouteForDeliveryIdListener {
        void endOfRouteForDeliveryId(DeliveryEndPointActionResult deliveryEndpointActionResult);
    }
    public interface StartOfRouteForDeliveryIdListener {
        void startOfRouteForDeliveryId(DeliveryEndPointActionResult deliveryEndpointActionResult);
    }
    public interface setManualStopTimesForDeliveryIdListener {
        void setManualStopTimesForDeliveryId(DeliveryStopActionResult result);
    }
    public interface setManualArriveAndDepartForDeliveryIdListener {
        void setManualArriveAndDepartForDeliveryId(DeliveryStopActionResult result);
    }
    public interface setManualDeliveryTimesForDeliveryIdListener {
        void setManualDeliveryTimesForDeliveryId(DeliveryStopActionResult result);
    }
    public interface setDeliveryConfirmationTimeForDeliveryIdListener {
        void setDeliveryConfirmationTimeForDeliveryId(DeliveryStopActionResult result, Error error);
    }

     public interface setStopOdometerforDeliveryIdListener {
        void setStopOdometerforDeliveryId(DeliveryStopActionResult result, Error error);
    }
     public interface setManualDeliveryStartForDeliveryIdListener {
        void setManualDeliveryStartForDeliveryId(DeliveryStopActionResult result, Error error);
    }
    public interface setManualDeliveryEndForDeliveryIdListener {
        void setManualDeliveryEndForDeliveryId(DeliveryStopActionResult result, Error error);
    }
    public interface setManualStartAndEndForDeliveryIdListener {
        void setManualStartAndEndForDeliveryId(DeliveryActionResult result);
    }
    public interface createDCReturnForDeliveryIDListener {
        void createDCReturnForDeliveryID(DCReturnActionResult result, Error error);
    }
    public interface OnUpdateDCReturnForDeliveryIDListener {
        void updateDCReturnForDeliveryID(DCReturnActionResult result, Error error);
    }
    public interface OnPushGPSEventForStopWithDeliveryIdListener {
        void pushGPSEventForStopWithDeliveryId(DeliveryStopActionResult result, Error error);
    }
    public interface OnPushGPSEventForSiteWithDeliveryIdListener {
        void pushGPSEventForSiteWithDeliveryId(DeliveryReportActionResult result, Error error);
    }
    public interface OnPushGPSEventForStopsWithDeliveryIdListener {
        void pushGPSEventForStopsWithDeliveryId(DeliveryStopActionResult result, Error error);
    }
    public interface OnPushGPSEventForPOIWithDeliveryIdListener {
        void pushGPSEventForPOIWithDeliveryId(DeliveryStopActionResult result, Error error);
    }
    public interface getRouteShipmentEquipmentForEquipmentIdListener {
        void getRouteShipmentEquipmentForEquipmentId(RouteShipmentEquipmentDTO equipment);
    }
    public interface getRouteShipmentUnitForRouteShipmentUnitIdListener {
        void getRouteShipmentUnitForRouteShipmentUnitId(RouteShipmentUnit routeShipmentUnit);
    }
    public interface getRouteShipmentProductClassForProductClassIdListener {
        void getRouteShipmentProductClassForProductClassId(RouteShipmentProductClass routeShipmentProductClass);
    }
    public interface onSaveTripForDeliveryIdListener {
        void SaveTripForDeliveryId(TripDataResult tripDataResult, Error error);
    }
    public interface onSaveCoreForDeliveryIdListener {
        void SaveCoreForDeliveryId(TripDataResult tripDataResult);
    }
    public interface onSaveTripAndStreamDataForDeliveryIdListener {
        void saveTripAndStreamDataForDeliveryId(TripDataResult tripDataResult);
    }
    public interface onSavePlannedRoutesForDeliveryIdListener {
        void savePlannedRoutesForDeliveryId(TripDataResult tripDataResult);
    }
    public interface onSaveAdjustedRoutesForDeliveryIdListener {
        void saveAdjustedRoutesForDeliveryId(TripDataResult tripDataResult);
    }
    public interface onSaveStreamDataForDeliveryIdListener {
        void saveStreamDataForDeliveryId(TripDataResult tripDataResult);
    }
    public interface onSaveStreamUpdateForDeliveryIdListener {
        void saveStreamUpdateForDeliveryId(TripDataResult tripDataResult);
    }
    public interface getPreTripVerificationResultForSiteIdListener {
        void GetPreTripVerificationResultForSiteId(PreTripVerifierResult preTripVerifierResult, Error error);
    }
    public interface SetDeliveryStatusForDeliveryIdListener {
        void setDeliveryStatusForDeliveryId(Error error);
    }
    public interface escalateHOSAlertWithIdListener {
        void escalateHOSAlertWithId(Error error);
    }
    public interface cancelHOSAlertWithIdListener {
        void cancelHOSAlertWithId(Error error);
    }
    public interface deleteHOSAlertWithIdListener {
        void deleteHOSAlertWithId(Error error);
    }
    public interface SetProductSourceForDeliveryIdListener {
        void setProductSourceForDeliveryId(Error error);
    }
    public interface getRouteStatusDNDBEventForEventIdListener {
        void GetRouteStatusDNDBEventForEventId(RouteShipmentStopDNDBEvent dndbEvent);
    }
    public interface onCreateOrUpdateDNDBEventWithEventIdListener {
        void createOrUpdateDNDBEventWithEventId(RouteShipmentStopDNDBEvent dndbEvent, Error error);
    }

    public interface getCombinedStatusListsForDCIdListener {
        void GetCombinedStatusListsForDCId(RMSCombinedStatusLists combinedStatusList);
    }
    public interface getDeliveryStatusWithSelectedForDCIdListener {
        void GetDeliveryStatusWithSelectedForDCId(DeliveryStatusWithSelectedDTO deliveryStatusWithSelectedDTO);
    }
    public interface getSdrReportForDeliveryIdListener {
        void GetSdrReportForDeliveryId(SdrReport sdrReport, Error error);
    }
    public interface getTrailerReportForTrailerIdListener {
        void getTrailerReportForTrailerId(SdrReport sdrReport, Error error);
    }
    public interface onSendPODEmailForDeliveryIdListener {
        void sendPODEmailForDeliveryId(NotifyConfirmation notifyConfirmation, Error error);
    }
    public interface RebuildRouteWithDeliveryIdListener {
        void rebuildRouteWithDeliveryId(DeliveryActionResult result);
    }
    public interface terminateRouteWithDeliveryIdListener {
        void terminateRouteWithDeliveryId(DeliveryReportActionResult result);
    }
    public interface moveStopForDeliveryIdListener {
        void moveStopForDeliveryId(DeliveryStopActionResult result);
    }
    public interface moveStopToPOIForDeliveryIdListener {
        void moveStopForDeliveryId(DeliveryStopActionResult result);
    }
    public interface onChangeStopAddressForDeliveryIdListener {
        void changeStopAddressForDeliveryId(DeliveryStopActionResult result);
    }
    public interface onChangeDeliveryInstructionsForDeliveryIdListener {
        void changeDeliveryInstructionsForDeliveryId(DeliveryStopActionResult result);
    }
     public interface setPOIAddressFromClientForPOIIdListener {
        void setPOIAddressFromClientForPOIId(DeliveryPOIActionResult result);
    }
    public interface onMoveSiteWithSiteIdListener {
        void moveSiteWithSiteId(SiteResult result);
    }
    public interface onChangeSiteAddressForSiteIdListener {
        void changeSiteAddressForSiteId(SiteResult result);
    }
    public interface onChangeDeliveryInstructionsForSiteIdListener {
        void changeDeliveryInstructionsForSiteId(SiteResult result);
    }
    public interface onConfirmRouteWithDeliveryIdListener {
        void confirmRouteWithDeliveryId(DeliveryActionResult result, Error error);
    }
    public interface getTractorFleetForDCListener {
        void GetTractorFleetForDC(SdFleet tractorFleet);
    }
    public interface getTrailerFleetForDCListener {
        void GetTrailerFleetForDC(SdFleet trailerFleet);
    }
    public interface getMostRecentDriverTimeLogForUserIdListener {
        void getMostRecentDriverTimeLogForUserId(DriverData driverdata);
    }
    public interface getPreviousDriverTimeLogForUserIdListener {
        void getPreviousDriverTimeLogForUserId(DriverData driverdata);
    }
    public interface getNextDriverTimeLogForUserIdListener {
        void getNextDriverTimeLogForUserId(DriverData driverdata);
    }
    public interface getAllDriverTimeLogsByWorkDayForUserIdListener {
        void getAllDriverTimeLogsByWorkDayForUserId(DriverData driverdata);
    }
    public interface addEmployeeDailyScheduleForUserIdListener {
        void addEmployeeDailyScheduleForUserId();
    }
    public interface onCompleteTMSWithDeliveryIDListener {
        void completeTMSWithDeliveryID(DeliveryActionResult result, Error error);
    }
    public interface adjustDriverAssignmentForUserIDListener {
        void adjustDriverAssignmentForUserID(Error error);
    }
    public interface getLoadInfoForDeliveryIdListener {
        void getLoadInfoForDeliveryId(LoadInfoDTO loadInfo);
    }
    public interface onUpdateLoadInfoForDeliveryListener {
        void updateLoadInfoForDelivery(RouteShipmentResult routeShipmentResult);
    }
    public interface getAssetTrackingDataForSiteListener {
        void getAssetTrackingDataForSite(SdrFleetReport sdrFleetReport, Error error);
    }
    public interface getCurrentAssetTrackingStatusForDeviceListener {
        void getCurrentAssetTrackingStatusForDevice(AssetTrackingDTO currentStatus);
    }
    public interface getTrailersInSiteForSiteIdListener {
        void getTrailersInSiteForSiteId(SdrFleetReport sdrFleetReport);
    }
    public interface getTrailersInGroupWithGroupIdListener {
        void getTrailersInGroupWithGroupId(SdrFleetReport sdrFleetReport);
    }
    public interface setAdditionalDeliveryTimeForDeliveryIdListener {
        void setAdditionalDeliveryTimeForDeliveryId(Error error);
    }
    public interface removeAdditionalDeliveryTimeForDeliveryIdListener {
        void removeAdditionalDeliveryTimeForDeliveryId();
    }
    public interface getRouteShipmentInvoiceItemListener {
        void getRouteShipmentInvoiceItem(RouteShipmentInvoiceItem routeShipmentInvoiceItem);
    }
    public interface getLoadInfo2ForDeliveryIdListener {
        void getLoadInfo2ForDeliveryId(LoadInfo2DTO loadInfo, Error error);
    }
    public interface OnUpdateLoadInfo2ForDeliveryIdListener {
        void updateLoadInfo2ForDeliveryId(RouteShipmentResult result, Error error);
    }
    public interface OnSetEORValuesWithShipmentIdListener {
        void setEORValuesWithShipmentId(DeliveryStopActionResult result, Error error);
    }

    /******************************************      iPimmBaseDataManager   *******************************************/

    public interface getMessageWithMessageIdListener {
        void getMessageWithMessageId(PimmMessage pimmMessage);
    }
    public interface onCreateMessageForRecipientUserEmailListener {
        void createMessageForRecipientUserEmail(PimmMessage pimmMessage, Error error);
    }
    public interface onCreateMessageForRecipientUserIDListener {
        void createMessageForRecipientUserID(PimmMessage pimmMessage, Error error);
    }
    public interface onCreateMessageForPimmObjectWithIDListener {
        void createMessageForPimmObjectWithID(PimmMessage pimmMessage, Error error);
    }
    public interface onUpdateMessageWithMessageIdListener {
        void updateMessageWithMessageId(PimmMessage pimmMessage, Error error);
    }
    public interface onDeleteMessageWithMessageIdListener {
        void deleteMessageWithMessageId(Error error);
    }
    public interface onCreateFormWithAppIdListener {
        void createFormWithAppId(PimmForm pimmForm, Error error);
    }
    public interface onCreateFormWithAppIdrefIdListener {
        void createFormWithAppIdrefID(PimmForm pimmForm, Error error);
    }
    public interface onCreateFormWithAppIdpimmformIdListener {
        void createFormWithAppIdrefIDpimmform(PimmForm pimmForm, Error error);
    }
    public interface oncreateFormWithFormIdListener {
        void createFormWithFormId(PimmForm pimmForm, Error error);
    }
    public interface oncreateFormWithFormIdRefListener {
        void createFormWithFormIdref(PimmForm pimmForm, Error error);
    }
    public interface onUpdateReferenceForPimmFormWithFormIdListener {
        void updateReferenceForPimmFormWithFormId(PimmForm pimmForm, Error error);
    }
    public interface getFormWithFormIdListener {
        void getFormWithFormId(PimmForm pimmForm, Error error);
    }
    public interface onDeleteFormWithFormIdListener {
        void deleteFormWithFormId();
    }
    public interface oncreateFormAttachmentWithFormIdListener {
        void createFormAttachmentWithFormId(PimmFormAttachment pimmFormAttachment, Error error);
    }

    public interface onCreateBinaryFormAttachmentWithAttachmentIdListener {
        void createBinaryFormAttachmentWithAttachmentId(PimmFormAttachment pimmFormAttachment, Error error);
    }
    public interface getFormAttachmentWithFormIdListener {
        void getFormAttachmentWithFormId(PimmFormAttachment pimmFormAttachment, Error error);
    }
    public interface onDeleteFormAttachmentWithFormIdListener {
        void deleteFormAttachmentWithFormId(Error error);
    }
    public interface getStickyNoteWithIdListener {
        void getStickyNoteWithId(StickyNote stickyNote);
    }
    public interface getAssetProfileForDeviceIDListener {
        void getAssetProfileForDeviceID(AssetProfile assetProfile);
    }

    public interface ondeleteAssetProfileWithAssetProfileIDListener {
        void deleteAssetProfileWithAssetProfileID();
    }
    public interface onDeleteAssetProfilesForDeviceIDListener {
        void deleteAssetProfilesForDeviceID();
    }

    public interface getAssetProfileListForDeviceIdListener {
        void getAssetProfileListForDeviceId(ArrayList<AssetProfile> assetProfileList, Error error);
    }
    public interface getAssetProfileForAssetProfileIdListener {
        void getAssetProfileForAssetProfileId(AssetProfile assetProfile);
    }
    public interface onenableInstanceWithIdListener {
        void enableInstanceWithId();
    }
    public interface ondisableInstanceWithIdListener {
        void disableInstanceWithId();
    }
    public interface getShipmentFormWithFormIdListener {
        void getShipmentFormWithFormId(PimmForm pimmForm);
    }
    public interface getDeviceFormWithFormIdListener {
        void getDeviceFormWithFormId(PimmForm pimmForm);
    }
    public interface getSiteFormWithFormIdListener {
        void getSiteFormWithFormId(PimmForm pimmForm);
    }
    public interface ackAlarmForDeviceIdListener {
        void ackAlarmForDeviceId();
    }
    public interface ackAlarmForObjectNameListener {
        void ackAlarmForObjectName();
    }
    public interface onEnableDeviceWithIdListener {
        void enableDeviceWithId();
    }
    public interface ondisableDeviceWithIdListener {
        void disableDeviceWithId();
    }
    public interface onEnableObjectWithNameListener {
        void enableObjectWithName();
    }
    public interface onDisableObjectWithNameListener {
        void disableObjectWithName();
    }
    public interface getELDSequenceNumberForELDDeviceListener {
        void getELDSequenceNumberForELDDevice(ELDSequenceNum eldSequenceNum, Error error);
    }
    public interface onPostHOSViolationListener {
        void postHOSViolation(Error error);
    }
    public interface onCreateHOSViolationForDriverIdListener {
        void createHOSViolationForDriverId(HOSViolation hosViolation, Error error);
    }
    public interface getHOSViolationListForDriverIdListener {
        void getHOSViolationListForDriverId(ArrayList<HOSViolation> violationList , Error error);
    }
    public interface onDeleteHOSViolationForDriverIdListener {
        void deleteHOSViolationForDriverId(Error error);
    }
    public interface getHOSAlertForDriverIdListener {
        void getHOSAlertForDriverId(HOSAlert hosAlert);
    }
    public interface onDeleteHOSAlertsForDriverIdListener {
        void deleteHOSAlertsForDriverId();
    }
    public interface onCreateHOSAlertForDriverIdListener {
        void createHOSAlertForDriverId(HOSAlert hosAlert, Error error);
    }
    public interface createHOSViolationWithIdListener {
        void createHOSViolationWithId(HOSViolation hosViolation, Error error);
    }
    public interface updateHOSViolationWithIdListener {
        void updateHOSViolationWithId(HOSViolation hosViolation, Error error);
    }
    public interface updateHOSAlertWithIdListener {
        void updateHOSAlertWithId(HOSAlert hosAlert, Error error);
    }
    public interface onCancelHOSAlertForDriverIdListener {
        void cancelHOSAlertForDriverId(Error error);
    }
    public interface onEscalateHOSAlertForDriverIdListener {
        void escalateHOSAlertForDriverId();
    }
    public interface onPostHOSSummaryListener {
        void postHOSSummary(Error error);
    }
    public interface onDeleteHOSSummaryForUserIdListener {
        void deleteHOSSummaryForUserId();
    }
    public interface getUserRecordForUserIdListener {
        void getUserRecordForUserId(User user, Error error);
    }
    public interface isUserNameAvailableForDcIdListener {
        void isUserNameAvailableForDcId(UsernameAvailableResult result);
    }
    public interface getDriverScorecardForShipmentIdListener {
        void getDriverScorecardForShipmentId(VehicleDTO vehicleDTO, Error error);
    }
    public interface getDriverDashboardEventsForShipmentIdListener {
        void getDriverDashboardEventsForShipmentId(VehicleDTO vehicleDTO);
    }
    public interface getDriverDashboardUpdatesForShipmentIdListener {
        void getDriverDashboardUpdatesForShipmentId(VehicleDTO vehicleDTO);
    }
    public interface getDriverRollUpForDriverIdListener {
        void getDriverRollUpForDriverId(VehicleDTO vehicleDTO);
    }
    public interface getDriverLeaderBoardForCustomerIdListener {
        void getDriverLeaderBoardForCustomerId(VehicleDTO vehicleDTO, Error error);
    }
    public interface postHOSActivityForUserIdListener {
        void postHOSActivityForUserId(Error error);
    }
    public interface onCreateHOSActivityForUserIdListener {
        void createHOSActivityForUserId(HOSActivity hosActivity, Error error);
    }
    public interface onUpdateHOSActivityWithActivityIDListener {
        void updateHOSActivityWithActivityID(HOSActivity hosActivity, Error error);
    }
    public interface ondeleteHOSActivityForUserIdListener {
        void deleteHOSActivityForUserId(Error error);
    }
    public interface oncreateDocumentQualificationListener {
        void createDocumentQualification(Qualification qualification);
    }
    public interface onupdateDocumentQualificationWithQualificationIdListener {
        void updateDocumentQualificationWithQualificationId(Qualification qualification);
    }
    public interface oncreateDocumentQualificationFormForQualificationIdListener {
        void createDocumentQualificationFormForQualificationId(PimmForm pimmForm);
    }
    public interface onupdateDocumentQualificationFormForQualificationIdListener {
        void updateDocumentQualificationFormForQualificationId(PimmForm pimmForm);
    }
    public interface getQualificationWithQualificationIdListener {
        void getQualificationWithQualificationId(Qualification qualification);
    }
    public interface oncreateMobileDeviceWithSerialNumberListener {
        void createMobileDeviceWithSerialNumber(Device device);
    }
    public interface onupdateMobileDeviceWithDeviceIdNumberListener {
        void updateMobileDeviceWithDeviceId(Device device);
    }
    public interface getMobileDeviceDetailsForDeviceIdListener {
        void getMobileDeviceDetailsForDeviceId(Device device);
    }
    public interface ondeleteMobileDeviceWithDeviceIdListener {
        void deleteMobileDeviceWithDeviceId();
    }
    public interface getFacilityProductForFacilityProductIdListener {
        void getFacilityProductForFacilityProductId(FacilityProductDTO facilityProductDTO, Error error);
    }
    public interface getFacilityProductSiteEntryForFacilityProductSiteIdListener {
        void getFacilityProductSiteEntryForFacilityProductSiteId(FacilityProductSiteDTO facilityProductSiteDTO, Error error);
    }
    public interface getBuisnessScheduleForScheduleIdListener {
        void getBuisnessScheduleForScheduleId(Schedule_Buisness buisnessSchedule, Error error);
    }
    public interface getSiteBuisnessScheduleForScheduleIdListener {
        void getSiteBuisnessScheduleForScheduleId(Schedule_Buisness_Site siteBuisnessSchedule, Error error);
    }
    public interface getIFTAReportDataForCustomerIdListener {
        void getIFTAReportDataForCustomerId(IFTA_ECM iftaECMReportData, Error error);
    }
    public interface getMobileDeviceAlertForAlertIdListener {
        void getMobileDeviceAlertForAlertId(MobileDeviceAlert alert);
    }
    public interface oncreateMobileDeviceAlertForDeviceIdListener {
        void createMobileDeviceAlertForDeviceId(MobileDeviceAlert alert);
    }
    public interface ondeleteMobileDeviceAlertWithAlertIdListener {
        void deleteMobileDeviceAlertWithAlertId();
    }
    public interface ondeleteMobileDeviceAlertsWithAlertIdListener {
        void deleteMobileDeviceAlertsForDeviceId();
    }
    public interface getOdometerReadingForCustomerIdListener {
        void getOdometerReadingForCustomerId(TractorPOI tractorPOI, Error error);
    }
    public interface onCreateReminderForSiteIdListener {
        void createReminderForSiteId(PimmReminder reminder);
    }
    public interface getReminderWithReminderIdListener {
        void getReminderWithReminderId(PimmReminder reminder);
    }
    public interface onDeleteReminderWithReminderId {
        void deleteReminderWithReminderId();
    }
    public interface getMobileDeviceBySerialListener {
        void getMobileDeviceBySerial(Device device);
    }
    public interface oncreateEmbeddedDeviceForSiteIdListener {
        void createEmbeddedDeviceForSiteId(Device device);
    }
    public interface getEmbeddedDeviceWithSerialNumberListener {
        void getEmbeddedDeviceWithSerialNumber(Device device);
    }
    public interface getEmbeddedDeviceWithDeviceIdListener {
        void getEmbeddedDeviceWithDeviceId(Device device);
    }
    public interface deleteEmbeddedDeviceWithDeviceIdListener {
        void deleteEmbeddedDeviceWithDeviceId();
    }
    public interface saveSiteInfoForSiteIdListener {
        void saveSiteInfoForSiteId(Site site);
    }
    public interface getReeferAlarmWithIdListener {
        void getReeferAlarmWithId(ReeferAlarm reeferAlarm);
    }
    public interface clearAllReeferAlarmsForDeviceIdListener {
        void clearAllReeferAlarmsForDeviceId();
    }
    public interface getReeferCommandWithIdListener {
        void getReeferCommandWithId(ReeferCommand reeferCommand);
    }
    public interface oncancelReeferCommandWithIdListener {
        void cancelReeferCommandWithId();
    }
    public interface getSiteSettingsForSiteIdListener {
        void getSiteSettingsForSiteId(SiteSettings siteSettings, Error error);
    }
    public interface onchangeReeferSetPointForDeviceIdListener {
        void changeReeferSetPointForDeviceId();
    }
    public interface onchangeReeferModeToContinuousForDeviceIdListener {
        void changeReeferModeToContinuousForDeviceId();
    }
    public interface onchangeReeferModeToCycleForDeviceIdListener {
        void changeReeferModeToCycleForDeviceId();
    }
    public interface onchangeReeferModeToSleepForDeviceIdListener {
        void changeReeferModeToSleepForDeviceId();
    }
    public interface onchangeReeferPowerToOnForDeviceIdListener {
        void changeReeferPowerToOnForDeviceId();
    }
    public interface ondefrostReeferWithDeviceIdListener {
        void defrostReeferWithDeviceId();
    }
    public interface runPreTripOnDeviceIdListener {
        void runPreTripOnDeviceId();
    }
    public interface getDocumentMetaDataForDocumentListener {
        void getDocumentMetaDataForDocument(DocumentDTO document, Error error);
    }
    public interface onaddDocumentWithDocumentIdListener {
        void addDocumentWithDocumentId(DocumentDTO document, Error error);
    }
    public interface ondeleteDocumentWithIdListener {
        void deleteDocumentWithId();
    }
    public interface getSuD_SupplierDashboardForStartDateListener {
        void getSuD_SupplierDashboardForStartDate(SuD_SupplierDashboard supplierDashboard);
    }
    public interface getSuD_CustomerDashboardForStartDateListener {
        void getSuD_CustomerDashboardForStartDate(SuD_CustomerDashboard customerDashboard);
    }
    public interface getSuD_DistributorDashboardForStartDateListener {
        void getSuD_DistributorDashboardForStartDate(SuD_DistributorDashboard SuD_DistributorDashboard);
    }
    public interface getStD_DistributorDashboardForStartDateListener {
        void getStD_DistributorDashboardForStartDate(StD_DistributorDashboardDTO distributorDashboard);
    }
    public interface getStD_CustomerDashboardForStartDateListener {
        void getStD_CustomerDashboardForStartDate(StD_CustomerDashboardDTO StD_CustomerDashboardDTO);
    }
    public interface getFM_FacilityDashboardForStartDateListener {
        void getFM_FacilityDashboardForStartDate(FM_FacilityDashboardDTO facilityDashboard);
    }
    public interface getPOISiteListForDCSiteListener {
        void getPOISiteListForDCSite(POISiteListDTO poiSiteListDTO, Error error);
    }
    public interface onaddHOSEventListener {
        void addHOSEvent(HOSEventDTO HOSEventDTO, Error error);
    }
    public interface onUploadTagDataForDeviceTypeListener {
        void uploadTagDataForDeviceType(TagDataUploadDTO tagDataUpload);
    }
    public interface getTRACRegistrationDetailsForSiteIDListener {
        void getTRACRegistrationDetailsForSiteID(RegistrationDetails registrationDetails);
    }
    public interface getTRACConfirmationForTracRouteIDListener {
        void getTRACConfirmationForTracRouteID(TracRoute tracRoute);
    }

    public interface getFormDefinitionListForAppIdListener {
        void getFormDefinitionListForAppId(ArrayList<PimmForm> pimmFormList, Error error);
    }
     public interface getRouteShipmentEquipmentForSiteIdListener {
        void getRouteShipmentEquipmentForSiteId(ArrayList<RouteShipmentEquipmentDTO> equipmentList, Error error);
    }
     public interface getRouteShipmentUnitListForDCSiteIdListener {
        void getRouteShipmentUnitListForDCSiteId(ArrayList<RouteShipmentUnit> routeShipmentUnitList, Error error);
    }
     public interface getRouteShipmentProductListForCustomerIdListener {
        void getRouteShipmentProductListForCustomerId(ArrayList<RouteShipmentProduct> routeShipmentProductList, Error error);
    }
     public interface getDCSitesForUserIdListener {
        void getDCSitesForUserId(ArrayList<Site> siteList, Error error);
    }
     public interface routeShipmentInvoiceItemListListener {
        void routeShipmentInvoiceItemList(ArrayList<RouteShipmentInvoiceItem> routeShipmentInvoiceItemList, Error error);
    }
    public interface getFormListForShipmentIdListener {
        void getFormListForShipmentId(ArrayList<PimmForm> pimmFormList, Error error);
    }
    public interface getHOSSummaryForDriverIdListener {
        void getHOSSummaryForDriverId(ArrayList<HOSSummary> hosSummaryList, Error error);
    }
    public interface getHOSActivityForUserIdListener {
        void getHOSActivityForUserId(ArrayList<HOSActivity> hosActivityList, Error error);
    }
    public interface getAssignmentForDCSiteListener {
        void getAssignmentForDCSite(ArrayList<AssignmentInfo> assignmentInfoList, Error error);
    }
    public interface getStopListForDCSiteListener {
        void getStopListForDCSite(ArrayList<Site> stopList, Error error);
    }
    public interface getDriverListForDCSiteListener {
        void getDriverListForDCSite(ArrayList<User> deliveryTrailerList, Error error);
    }
    public interface getTrailerReportForDCSiteListener {
        void getTrailerReportForDCSite(ArrayList<DeliveryTrailerDTO> driverList, Error error);
    }
    public interface getTractorReportForDCSiteListener {
        void getTractorReportForDCSite(ArrayList<DeliveryTrailerDTO> deliveryTractorList, Error error);
    }
    public interface getTrailerListForDCSiteListener {
        void getTrailerListForDCSite(ArrayList<Device> trailerList, Error error);
    }
    public interface getTractorListForDCSiteListener {
        void getTractorListForDCSite(ArrayList<Device> tractorList, Error error);
    }
    public interface getRouteScheduleForDCSiteListener {
        void getRouteScheduleForDCSite(ArrayList<RouteShipment> routeShipmentList, Error error);
    }
    public interface getRouteScheduleSummaryDataForDCSiteListener {
        void getRouteScheduleSummaryDataForDCSite(ArrayList<RouteShipmentSummary> routeList, Error error);
    }
    public interface getSiteListForUserIdListener {
        void getSiteListForUserId(ArrayList<Site> siteList, Error error);
    }
    public interface getHOSEventsForUserListener {
        void getHOSEventsForUser(List<HOSEventDTO> hosEventList, Error error);
    }
    public interface calculate34HourResetTimeForUserIdListener {
        void calculate34HourResetTimeForUserId(List<HOSActivity> hosActivityList, Error error);
    }
    public interface getAllHOSEventsWithDateRangeStartListener {
        void getAllHOSEventsWithDateRangeStart(List<HOSEventDTO> hosEventList, Error error);
    }
    public interface getMessageListForRecipientUserListener {
        void getMessageListForRecipientUser(ArrayList<PimmMessage> pimmMessageList, Error error);
    }
    public interface getMessageListForPimmObjectIDListener {
        void getMessageListForPimmObjectID(ArrayList<PimmMessage> pimmMessageList, Error error);
    }
    public interface getRouteSiteListForDCSiteIdListener {
        void getRouteSiteListForDCSiteId(ArrayList<RouteSite> routeSiteList, Error error);
    }
     public interface postGPSDataForDeviceSerialNumberListener {
        void postGPSDataForDeviceSerialNumber( Error error);
    }
     public interface startRouteFromHereForDeliveryIdListener {
        void startRouteFromHereForDeliveryId(DeliveryEndPointActionResult deliveryEndpointActionResult,  Error error);
    }
     public interface updateFormWithFormIdListener {
        void updateFormWithFormId(PimmForm pimmForm,  Error error);
    }

    public interface getRouteShuttleWithIDListener {
        void getRouteShuttleWithID(RouteShuttleDTO routeShuttle,  Error error);
    }
    public interface addHOSEventDueToEditByDriverListener {
        void addHOSEventDueToEditByDriver(HOSEventDTO hosEvent,  Error error);
    }
    public interface addHOSEventDueToEditByManagerListener {
        void addHOSEventDueToEditByManager(HOSEventDTO hosEvent,  Error error);
    }

    public interface deleteHOSEventByDriverWithHOSEventIdListener {
        void deleteHOSEventByDriverWithHOSEventId(HOSEventDTO hosEvent,  Error error);
    }
    public interface deleteHOSEventByManagerWithHOSEventIdListener {
        void deleteHOSEventByManagerWithHOSEventId(HOSEventDTO hosEvent,  Error error);
    }
    public interface updateHOSEventDueToEditApprovedByDriverListener {
        void updateHOSEventDueToEditApprovedByDriver(HOSEventDTO hosEvent,  Error error);
    }
    public interface updateHOSEventDueToEditRejectedByDriverListener {
        void updateHOSEventDueToEditRejectedByDriver(HOSEventDTO hosEvent,  Error error);
    }
    public interface addDriverPerformanceEntryForUserListener {
        void addDriverPerformanceEntryForUser(DriverPerformanceDTO driverPerformanceEntry,  Error error);
    }
    public interface getHOSViolationWithIdListener {
        void getHOSViolationWithId(HOSViolation hosViolation,  Error error);
    }
    public interface getHOSAlertWithIdListener {
        void getHOSAlertWithId(HOSAlert hosAlert,  Error error);
    }
    public interface getRouteShuttleListForShipmentIDListener {
        void getRouteShuttleListForShipmentID(ArrayList<RouteShuttleDTO> routeShuttleList, Error error);
    }

    public interface getPendingRequestsforDriverListener {
        void getPendingRequestsforDriver(ArrayList<HOSEventDTO> hosEventList, Error error);
    }
    public interface getPendingRequestsforAdminListener {
        void getPendingRequestsforAdmin(ArrayList<HOSEventDTO> hosEventList, Error error);
    }
    public interface updateOutboundDCDriverPermissionListener {
        void updateOutboundDCDriverPermission(OutboundDCDriverPermission driverPermission, Error error);
    }
    public interface updateOutboundDCDriverLicenseListener {
        void updateOutboundDCDriverLicense(OutboundDCDriverLicense driverLicense, Error error);
    }
    public interface addHOSShippingDocumentWithIdListener {
        void addHOSShippingDocumentWithId(HOSShippingDocumentDTO driverLicense, Error error);
    }
    public interface addHOSDriverVehicleWithIdListener {
        void addHOSDriverVehicleWithId(HOSShippingDocumentDTO.HOSDriverVehicleDTO hosDriverVehicleDTO, Error error);
    }
    public interface getHOSSummaryForUserListener {
        void getHOSSummaryForUser(HOSSummary hosSummary, Error error);
    }
    public interface addCoDriverWithHosIdListener {
        void addCoDriverWithHosId(HOSShippingDocumentDTO.HOSCoDriverDTO hosCoDriver, Error error);
    }
    public interface getHOSSummaryForSiteListener {
        void getHOSSummaryForSite(ArrayList<HOSSummary> hosSummary, Error error);
    }
    public interface updateHOSCertificationForDriverListener {
        void updateHOSCertificationForDriver( Error error);
    }
    public interface updateHOSShippingDocumentWithIdListener {
        void updateHOSShippingDocumentWithId( Error error);
    }
    public interface updateHOSDriverVehicleWithIdListener {
        void updateHOSDriverVehicleWithId( Error error);
    }
    public interface updateCoDriverWithHosIdListener {
        void updateCoDriverWithHosId( Error error);
    }
    public interface getDriverPerformanceListForUserListener {
        void getDriverPerformanceListForUser(ArrayList<DriverPerformanceDTO> driverPerformanceList, Error error);
    }
    public interface addMultipleHOSEventsDueToEditByDriverListener {
        void addMultipleHOSEventsDueToEditByDriver(ArrayList<HOSEventDTO> originalHOSEventIdList, Error error);
    }
    public interface getUnassignedHOSEventsForSiteListener {
        void getUnassignedHOSEventsForSite(ArrayList<HOSEventDTO> hosEventList, Error error);
    }
    public interface getPOIListForShipmentIDListener {
        void getPOIListForShipmentID(ArrayList<SdrPointOfInterest> poiList, Error error);
    }
    public interface getDriverPerformanceListForSiteListener {
        void getDriverPerformanceListForSite(ArrayList<DriverPerformanceDTO> driverPerformanceList, Error error);
    }
    public interface getRouteShuttleListForDriverIDListener {
        void getRouteShuttleListForDriverID(ArrayList<RouteShuttleDTO> routeShuttleList, Error error);
    }
    public interface getHOSViolationsForDriverIdListener {
        void getHOSViolationsForDriverId(ArrayList<HOSViolation> violationList, Error error);
    }
    public interface getHOSAlertsForDriverIdListener {
        void getHOSAlertsForDriverId(ArrayList<HOSAlert> alertArrayList, Error error);
    }
    public interface getHOSAlertsForSiteIdListener {
        void getHOSAlertsForSiteId(ArrayList<HOSAlert> alertArrayList, Error error);
    }
    public interface getHOSViolationsForSiteIdListener {
        void getHOSViolationsForSiteId(ArrayList<HOSViolation> violationList, Error error);
    }
    public interface getFacilityProductListForCustomerIdListener {
        void getFacilityProductListForCustomerId(ArrayList<FacilityProductDTO> facilityProductList, Error error);
    }
    public interface getFacilityProductSiteListForSiteIdListener {
        void getFacilityProductSiteListForSiteId(ArrayList<FacilityProductSiteDTO> facilityProductSiteList, Error error);
    }
    public interface getFacilityDataForSiteIdListener {
        void getFacilityProductSiteListForSiteId(ArrayList<Instance_Performance> facilityDataList, Error error);
    }
    public interface getBuisnessScheduleForCustomerIdListener {
        void getBuisnessScheduleForCustomerId(ArrayList<Schedule_Buisness> buisnessScheduleList, Error error);
    }
    public interface getBuisnessScheduleForSiteIdListener {
        void getBuisnessScheduleForSiteId(ArrayList<Schedule_Buisness_Site> buisnessScheduleList, Error error);
    }
    public interface getFacilityStatusForSiteIdListener {
        void getFacilityStatusForSiteId(ArrayList<PimmDevice> pimmDeviceList, Error error);
    }
    public interface getFacilityStatusForSiteClassListener {
        void getFacilityStatusForSiteClass(ArrayList<PimmDevice> pimmDeviceList, Error error);
    }


    /******************************************      Stop POD Data   *******************************************/

    public interface completionCallbackListener {
        void completionCallback( Error error);
    }
    public interface podSaveImageOnServerListener {
        void podSaveImageOnServer( Error error, String attachmentID);
    }
    public interface initWithAppIdListener {
        void initWithAppId( Error error);
    }
    public interface initDeliveryInfoWithStopDataListener {
        void initDeliveryInfoWithStopData( Error error);
    }
    public interface getDeliveryFormFromServerWithCallbackListener {
        void getDeliveryFormFromServerWithCallback( boolean formFoundOnServer);
    }
    public interface createDeliveryFormWithCompletionCallbackListener {
        void createDeliveryFormWithCompletionCallback( Error error , String formID);
    }
    public interface getAddressComponentsWithCallbackListener {
        void getAddressComponentsWithCallback( AddressComponents addressComponents,Error error);
    }
    public interface getShuttleSiteListforLoggedInUserWithCallbackListener {
        void getShuttleSiteListforLoggedInUserWithCallback( ArrayList<ShuttleSite> siteList ,Error error);
    }

    /*****************************************SMS Dashboard*******************************************/
    public interface getSiteListForLoggedInUserWithCallbackListener{
        void getSiteListForLoggedInUserWithCallback(ArrayList<Site> siteList, Error error);
    }

    public interface getSMSDashboardWithCallbackListener{
        void getSMSDashboardWithCallback(SMSDashboard smsDashboard, Error error);
    }

    public interface getSMSDashboardCustomerCommentsWithCallbackListener{
        void getSMSDashboardCustomerCommentsWithCallback(SMSDashboardCustomerComments smsDashboardCustomerComments, Error error);
    }

    public interface getLatestFormForReferenceIdCallbackListener {
        void getLatestFormForReferenceId(PimmForm pimmForm, Error error);
    }

    public interface getFormDefinitionListForAppIdCallbackListener{
        void getFormDefinitionListForAppIdCallback(ArrayList<PimmForm> pimmFormArrayList, Error error);
    }

    public interface getDocumentListForReferenceIdCallbackListener{
        void  getDocumentListForReferenceIdCallback(ArrayList<DocumentDTO> documentDTOArrayList, Error error);
    }

    public interface getDocumentContentForDocumentIdCallbackListener{
        void getDocumentContentForDocumentIdCallback(Object Object, Error error);
    }

    /*****************************************APIs(20191114)*******************************************/
    public interface getInstanceListForSiteIdCallbackListener{
        void getInstanceListForSiteIdCallback(ArrayList<PimmInstance> pimmInstanceArrayList, Error error);
    }

    public interface getScorecardForSiteIdCallbackListener {
        void getScorecardForSiteIdCallback(ScorecardDTO scorecardDTO, Error error);
    }

    public interface getObjectListForDeviceIdCallbackListener {
        void getObjectListForDeviceIdCallback(PimmObject pimmObject, Error error);
    }

    public interface getThresholdForInstanceIdCallbackListener {
        void getThresholdForInstanceIdCallback(PimmThreshold pimmThreshold, Error error);
    }

    public interface getReportListforLoggedInUserCallbackListener {
        void getReportListforLoggedInUserCallback( ArrayList<ApplicationReport>applicationReports,Error error);
    }

    public interface getDaySummaryForSiteIDCallbackListener {
        void getDaySummaryForSiteIDrCallback(FSLDaySummary fslDaySummary,Error error);
    }

    public interface getCertificationDefinitionsWithCallbackListener {
        void getCertificationDefinitionsWithCallback(ArrayList<CertificationDefinition> certificationDefinitions, Error error);
    }

    public interface getWithStructureCallbackListener {
        void getWithStructureWithCallback(ArrayList<CorpStructure>corpStructures, JSONArray siteObject, Error error);
    }

    public interface getSMSCommunicationNotesForSiteCallbackListener {
        void getSMSCommunicationNotesForSiteWithCallback(ArrayList<SMSCommunicationNotes>smsCommunicationNotes,Error error);
    }

    public interface getStoreUsersCallbackListener {
        void getStoreUsersCallback(ArrayList<User> storeUsers,Error error);
    }

    public interface updateUserRecordWithUserIdListener {
        void updateUserRecordWithUserIdCallback(Boolean status, Error error);
    }

    public interface getLatestFormpackListener {
        void getLatestFormpackCallback(byte [] zip,Error error);
    }

      public interface listAllFormpackWithNameListener {
        void listAllFormpackWithNameCallback(ArrayList<FormPack>formPackslist,Error error);
    }

    public interface getFMSAuditListForSiteID {
        void getFMSAuditListForSiteIDCallback(Error error);
    }

    public interface getSiteSalesDefaultListener {
        void getSiteSalesDefaultCallback(SiteSalesDefault salesDefault ,Error error);
    }

    public interface getSiteSalesByDateListener {
        void getSiteSalesByDateCallback(SiteSales siteSales ,Error error);
    }
    public interface getDaypartSummaryForSiteIDListener {
        void getDaypartSummaryForSiteIDCallback(FSLDaypartSummaryDTO daypartSummary ,Error error);
    }
    public interface getEmployeeScheduleListener {
        void getEmployeeScheduleCallback(ArrayList<Schedule_Business_Site_Plan> schedules ,Error error);
    }

    public interface getBinaryFormAttachmentWithFormIdListener {
        void getBinaryFormAttachmentWithFormIdCallback(Object object ,Error error);
    }



}
