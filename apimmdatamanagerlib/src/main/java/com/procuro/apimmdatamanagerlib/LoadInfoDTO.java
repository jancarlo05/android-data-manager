package com.procuro.apimmdatamanagerlib;

import java.util.Date;
import java.util.HashMap;

/**
 * Created by jophenmarieweeks on 19/07/2017.
 */

public class LoadInfoDTO extends PimmBaseObject {

    public String deliveryId;
    public User freezerLoader;
    public Date freezerLoadStartTime;
    public Date freezerLoadEndTime;
    public int freezerCases; //int
    public int freezerCubes; //int
    public double freezerSetPoint; //double
    public double freezerTargetTemp; //double
    public double freezerActualTemp; //double
    public Date freezerActualTime;
    public double freezerPrecoolTemp; //double
    public Date freezerPrecoolTime;

    //public String coolerLoader;
    public User coolerLoader;
    public Date coolerLoadStartTime;
    public Date coolerLoadEndTime;
    public double coolerCases;
    public double coolerCubes;
    public double coolerSetPoint;
    public double coolerTargetTemp;
    public double coolerActualTemp;
    public Date coolerActualTime;
    public double coolerPrecoolTemp;
    public Date coolerPrecoolTime;

    public double dryCases;
    public double dryCubes;


    public HashMap<String,Object> dictionaryWithValuesForKeys() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();

        if(this.deliveryId != null) {
            dictionary.put("deliveryId", this.deliveryId);
        }
        if(this.freezerLoader != null) {
            dictionary.put("freezerLoader", this.freezerLoader.dictionaryWithValuesForKeys());
        }
        if(this.coolerLoader != null) {
            dictionary.put("coolerLoader", this.coolerLoader.dictionaryWithValuesForKeys());
        }

        if(this.freezerLoadStartTime != null) {
            dictionary.put("freezerLoadStartTime",  DateTimeFormat.convertToJsonDateTime(this.freezerLoadStartTime));
        }

        if(this.freezerLoadEndTime != null) {
            dictionary.put("freezerLoadEndTime",  DateTimeFormat.convertToJsonDateTime(this.freezerLoadEndTime));
        }

        dictionary.put("freezerCases",  this.freezerCases);
        dictionary.put("freezerCubes",  this.freezerCubes);
        dictionary.put("freezerSetPoint",  this.freezerSetPoint);
        dictionary.put("FreezerTargetTemp",  this.freezerTargetTemp);

        if(this.freezerActualTime != null) {
            dictionary.put("freezerActualTime",  DateTimeFormat.convertToJsonDateTime(this.freezerActualTime));
        }

            dictionary.put("freezerPrecoolTemp", this.freezerPrecoolTemp);

        if(this.freezerPrecoolTime != null) {
            dictionary.put("freezerPrecoolTime",  DateTimeFormat.convertToJsonDateTime(this.freezerPrecoolTime));
        }

        dictionary.put("coolerLoader", this.coolerLoader);

        if(this.coolerLoadStartTime != null) {
            dictionary.put("coolerLoadStartTime",  DateTimeFormat.convertToJsonDateTime(this.coolerLoadStartTime));
        }

        if(this.coolerLoadEndTime != null) {
            dictionary.put("coolerLoadEndTime",  DateTimeFormat.convertToJsonDateTime(this.coolerLoadEndTime));
        }

        dictionary.put("coolerCases", this.coolerCases);
        dictionary.put("coolerCases", this.coolerCubes);
        dictionary.put("coolerSetPoint", this.coolerSetPoint);
        dictionary.put("coolerTargetTemp", this.coolerTargetTemp);
        dictionary.put("coolerActualTemp", this.coolerActualTemp);

        if(this.coolerActualTime != null) {
            dictionary.put("coolerActualTime",  DateTimeFormat.convertToJsonDateTime(this.coolerActualTime));
        }
        dictionary.put("coolerPrecoolTemp", this.coolerPrecoolTemp);

        if(this.coolerPrecoolTime != null) {
            dictionary.put("coolerPrecoolTime",  DateTimeFormat.convertToJsonDateTime(this.coolerPrecoolTime));
        }

        dictionary.put("dryCases", this.dryCases);
        dictionary.put("dryCubes", this.dryCubes);

        return dictionary;
    }

}
