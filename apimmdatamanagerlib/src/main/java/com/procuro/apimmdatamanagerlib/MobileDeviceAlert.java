package com.procuro.apimmdatamanagerlib;

import java.util.Date;

/**
 * Created by jophenmarieweeks on 10/07/2017.
 */

public class MobileDeviceAlert extends PimmBaseObject {


    public enum MobileDeviceAlertType
    {
        MobileDeviceAlert_Undefined(-1),
        MobileDeviceAlert_Battery(1),
        MobileDeviceAlert_Proximity(2);
        private int value;
        private MobileDeviceAlertType(int value)
        {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }

        public static MobileDeviceAlertType setIntValue (int i) {
            for (MobileDeviceAlertType type : MobileDeviceAlertType.values()) {
                if (type.value == i) { return type; }
            }
            return MobileDeviceAlert_Undefined;
        }
    }

    public String mobileDeviceAlertID;
    public String deviceID;
    public Date timestamp;
    public MobileDeviceAlertType alertType;

    @Override
    public void setValueForKey(Object value, String key) {

        if(key.equalsIgnoreCase("alertType")) {
            if (!value.equals(null)) {
                MobileDeviceAlertType mobileDeviceAlertType = MobileDeviceAlertType.setIntValue((int) value);
                this.alertType = mobileDeviceAlertType;
            }

        } else {
            super.setValueForKey(value, key);
        }
    }
}
