package com.procuro.apimmdatamanagerlib;

/**
 * Created by jophenmarieweeks on 12/07/2017.
 */

public class DeliveryStatusDriverDTO extends PimmBaseObject {

    public String deliveryStatusDriverId;
    public String UserId;
    public String Username;
    public String FirstName;
    public String LastName;
    public int Rank; //Need to be replaced with DeliveryDriverRank


}
