package com.procuro.apimmdatamanagerlib;

import java.util.HashMap;

/**
 * Created by jophenmarieweeks on 12/07/2017.
 */

public class GS1BarcodeComponents extends PimmBaseObject {
    public String barcode;
    public boolean valid;
    public String GTIN14;
    public String lotNumber; //Alphanumeric String of upto 20 characters
    public String productionDateStr; //Date String as decoded from Barcode
    public String packagingDateStr;
    public String sellByDateStr;
    public String expirationDateStr;
    public String bestBeforeDateStr;
    public double catchWeight;

    public HashMap<String,Object> dictionaryWithValuesForKeys() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();

        dictionary.put("barcode", this.barcode);
        dictionary.put("valid", this.valid);
        dictionary.put("GTIN14", this.GTIN14);
        dictionary.put("lotNumber", this.lotNumber);
        dictionary.put("productionDateStr", this.productionDateStr);
        dictionary.put("packagingDateStr", this.packagingDateStr);
        dictionary.put("expirationDateStr", this.expirationDateStr);
        dictionary.put("bestBeforeDateStr", this.bestBeforeDateStr);
        dictionary.put("catchWeight", this.catchWeight);

        return dictionary;

    }
}
