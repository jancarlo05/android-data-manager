package com.procuro.apimmdatamanagerlib;

import org.json.JSONObject;

import java.util.Date;
import java.util.HashMap;

public class FacilityScorecardInspectionDTO extends PimmBaseObject {

    public Date timestamp;
    public Date lastComplete;
    public Date lastIncomplete;
    public int onTimeCount;
    public int lateCount;
    public int incompleteCount;
    public int grade;

    @Override
    public void setValueForKey(Object value, String key)
    {
        super.setValueForKey(value, key);
    }

    public HashMap<String,Object> dictionaryWithValuesForKeys() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();


        dictionary.put("timestamp", (this.timestamp == null) ? null : DateTimeFormat.convertToJsonDateTime(this.timestamp));
        dictionary.put("lastComplete", (this.lastComplete == null) ? null : DateTimeFormat.convertToJsonDateTime(this.lastComplete));
        dictionary.put("lastIncomplete", (this.lastIncomplete == null) ? null : DateTimeFormat.convertToJsonDateTime(this.lastIncomplete));


        dictionary.put("onTimeCount",this.onTimeCount);
        dictionary.put("lateCount",this.lateCount);
        dictionary.put("incompleteCount",this.incompleteCount);
        dictionary.put("grade",this.grade);

        return dictionary;
    }

}
