package com.procuro.apimmdatamanagerlib;

import java.util.Date;

public class FSLSummary extends PimmBaseObject {

    public String siteID;
    public String userID;

    public Date date;
    public Integer onTime;
    public int reviewedByUserID;

    public int status;
    public int reviewed;
    public int daypart;

    @Override
    public void setValueForKey(Object value, String key) {
        super.setValueForKey(value, key);

        if (key.equalsIgnoreCase("onTime")){
            try {
                if (value!=null){
                    this.onTime = Integer.parseInt(value.toString());
                }
            }catch (Exception ignored){

            }
        }

        if (key.equalsIgnoreCase("date")){
            try {
                if (value!=null){
                    this.date = DateTimeFormat.convertJSONDateToDate(value.toString());
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }

    }
}
