package com.procuro.apimmdatamanagerlib;

import org.json.JSONObject;

/**
 * Created by jophenmarieweeks on 07/07/2017.
 */

public class RouteShipmentCreateStopResult extends PimmBaseObject {

    public String ShipmentID;
    public boolean Rebuild;
    public boolean Created;
    public Site Stop;


    @Override
    public void setValueForKey(Object value, String key)
    {

        if (key.equalsIgnoreCase("Stop")) {
            if (value != null) {
                if (value instanceof JSONObject) {
                    Site result = new Site();
                    JSONObject jsonObject = (JSONObject) value;
                    result.readFromJSONObject(jsonObject);
                    this.Stop = result;
                }
            }
    } else {
        super.setValueForKey(value, key);
    }
    }

}
