package com.procuro.apimmdatamanagerlib;

import android.util.Log;

/**
 * Created by jophenmarieweeks on 01/08/2017.
 */

public class RegistrationDetails extends PimmBaseObject {

   public enum AssetType {
        AssetType_None(0),
        AssetType_Trailer(1),
        AssetType_Tractor(2),
        AssetType_StraightTruck(3),
        AssetType_AirTag(50),
        AssetType_LogTag(51);

       private int value;
       private AssetType(int value)
       {
           this.value = value;
       }

       public int getValue() {
           return this.value;
       }

       public static AssetType setIntValue (int i) {
           for (AssetType type : AssetType.values()) {
               if (type.value == i) { return type; }
           }
           return AssetType_None;
       }
    }


    public RegistrationRoute route;
    public Connections connections;
    public AssetType assetType;

    @Override
    public void setValueForKey(Object value, String key) {

        if(key.equalsIgnoreCase("assetType")) {
            if (!value.equals(null)) {
                AssetType AssetTypes = AssetType.setIntValue((int) value);
                this.assetType = AssetTypes;

            }
        } else {
            super.setValueForKey(value, key);
        }
    }


}
