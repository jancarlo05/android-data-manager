package com.procuro.apimmdatamanagerlib;

/**
 * Created by jophenmarieweeks on 10/07/2017.
 */

public class RouteShipmentProductClass extends PimmBaseObject {

    public String RouteShipmentProductClassID;
    public String DCID;
    public String Description;
    public int CustomerProductClassID;
    public ProductCompartmentEnum.ProductCompartmentsEnum compartment;
    public ProductCategoryEnum.ProductCategorysEnum  category;
    public int productClass;
    public String shortDescription;
    public String longDescription;
    public boolean valid;
    public String customerProductAltID; // 14 digit GTIN number (barcode) associated with the item

    @Override
    public void setValueForKey(Object value, String key) {

        if(key.equalsIgnoreCase("compartment")) {
            if (!value.equals(null)) {
                ProductCompartmentEnum.ProductCompartmentsEnum productCompartments = ProductCompartmentEnum.ProductCompartmentsEnum.setIntValue((int) value);
                this.compartment = productCompartments;

            }else if (!value.equals(category)) {
                ProductCategoryEnum.ProductCategorysEnum productCategorysEnum = ProductCategoryEnum.ProductCategorysEnum.setIntValue((int) value);
                this.category = productCategorysEnum;
            }

        } else {
            super.setValueForKey(value, key);
        }
    }

}
