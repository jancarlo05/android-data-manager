package com.procuro.apimmdatamanagerlib;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by jophenmarieweeks on 10/07/2017.
 */

public class SdrDelivery extends PimmBaseObject {



    public enum TransportConfigType
    {
        TractorOnly(0),
        Tractor_Trailer(1),                    // default
        Tractor_Trailer_Trailer(2),
        StraightTruck(3),
        StraightTruck_Trailer(4),              // meyer-style combo route
        StraightTruck_Trailer_Trailer(5);

        private int value;
        private TransportConfigType(int value)
        {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }

        public static TransportConfigType setIntValue (int i) {
            for (TransportConfigType type : TransportConfigType.values()) {
                if (type.value == i) { return type; }
            }
            return TractorOnly;
        }

    }

    public String DeliveryId;
    public StoreDeliveryEnums.DeliveryStatus Status;
    public StoreDeliveryEnums.TransportStatus transportStatus;

    public boolean Terminated;
    public String Title;
    public boolean HasError;
    public String RouteId;
    public String RouteName;
    public String RouteSummary;
    public String RouteStatus;
    public boolean TemperatureOK;
    public String TemperatureComplianceText;
    public String DistributorName;
    public String DCId;
    public String DCName;
    public String StartSiteAccountName;
    public String StartSiteId;
    public String StartSiteName;
    public String EndSiteAccountName;
    public String EndSiteId;
    public String EndSiteName;
    public String TrailerId;
    public String TrailerName;
    public String GPSLink;

//Array of Strings
    public List<String> Descriptors;
    public String DeliveryRouteType;
    public Date ArrivalTime;
    public String ArrivalTimeText;
    public String ArrivalMonthDayText;
    public Date DepartureTime;
    public String DepartureTimeText;
    public String DepartureMonthDayText;
    public Date StartTime;
    public String StartTimeText;
    public Date EndTime;
    public String EndTimeText;
    public Date LastSampleTime;
    public String LastSampleTimeText;
    public Date LoadTime;
    public String LoadTimeText;
    public Date DispatchTime;
    public String DispatchTimeText;
    public double TotalTime;
    public String TotalTimeText;
    public double TransitTime;
    public String TransitTimeText;
    public double LoadDuration;
    public String LoadDurationText;
    public double StopsTotal;
    public double PlannedStops;
    public double DynamicStops;
    public double ScheduledStops;
    public double StopsMade;
    public double ScheduledStopsMade;
    public double StopsMissed;
    public double StopsOnTime;
    public double StopsNotOnTime;
    public double LateStops;
    public double EarlyStops;
    public double TotalMinutesLate;
    public Date DataStartDate;
    public Date DataEndDate;
    public List<SdrMetric> Metrics; //Array of SdrMetric
    public SdrOnTimeComplianceRule OnTimeComplianceRule;
    public Date FirstOutOfSpecTemperatureTime;
    public String FirstOutOfSpecTemperatureMetricId;
    public String FirstOutOfSpecTemperatureMetricLabel;
    public List<SdrChartGroup> Charts; //Array of SdrChartGroup
    public SdrGis GIS;
    public SdrTrailer Trailer;

//public String* Driver;
//public String* Driver2;
    public String DistanceUnits;
    public String Tractor;
    public String TractorId;
    public String DeliveryCustomerId;

//Array of Strings
    public List<String> Capabilities;
    public List<PimmEvent> EventList;
    public String TripDistance;
    public String TripDistanceText;
    public double FlagStatus;
    public HashMap<String, Object> ShipmentResults;
    public Date PrecoolStart;
    public String PrecoolStartText;
    public String BackhaulPlantId;
    public String BackhaulId;
    public boolean HasBackhaul;

//public String *DriverId;
//public String *Driver2Id;
//public MutableArray *Drivers;

    public SdrDriver Driver;
    public SdrDriver CoDriver;

    //public MutableArray *DCMetrics;
    public List<String> DCMetrics;
    public boolean IsSynchronizing;

    public String StartPointPoiId;
    public String EndPointPoiId;
    public String DCAddress;

    public String TMSDeviceId;

    public double TrailerOdometerStart;
    public double TrailerOdometerEnd;
    public double TractorOdometerStart;
    public double TractorOdometerEnd;






    public TransportConfigType TransportConfig;

    //@property (nonatomic,strong) NSMutableArray* TripDistanceByHour;
    public ArrayList<NumericValue> TripDistanceByHour; // List of NumericValue objects. There is one NumericValue per hour with the total distance for that hour

    @Override
    public void setValueForKey(Object value, String key)
    {
        if(key.equalsIgnoreCase("Descriptors"))
        {
            if(this.Descriptors == null) {
                this.Descriptors = new ArrayList<String>();
            }
            if (!value.equals(null)) {
                if (value instanceof JSONArray) {

                    JSONArray arrData = (JSONArray) value;

                    for (int i = 0; i < arrData.length(); i++) {
                        try {
                            this.Descriptors.add(arrData.get(i).toString());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
        else if(key.equalsIgnoreCase("Metrics"))
        {
            if(this.Metrics == null)
            {
                this.Metrics = new ArrayList<SdrMetric>();
            }

            if (!value.equals(null)) {
                if (value instanceof JSONArray) {

                    JSONArray arrData = (JSONArray) value;

                    if (arrData != null) {
                        for (int i = 0; i < arrData.length(); i++) {
                            try {
                                SdrMetric sdrMetric = new SdrMetric();
                                sdrMetric.readFromJSONObject(arrData.getJSONObject(i));

                                this.Metrics.add(sdrMetric);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }
        }
        else if(key.equalsIgnoreCase("Charts"))
        {
            if(this.Charts == null) {
                this.Charts = new ArrayList<SdrChartGroup>();
            }
            if (!value.equals(null)) {
                if (value instanceof JSONArray) {

                    JSONArray arrData = (JSONArray) value;

                    if (arrData != null) {
                        for (int i = 0; i < arrData.length(); i++) {
                            try {
                                SdrChartGroup sdrChartGroup = new SdrChartGroup();
                                sdrChartGroup.readFromJSONObject(arrData.getJSONObject(i));

                                this.Charts.add(sdrChartGroup);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }
        }
        else if (key.equalsIgnoreCase("GIS"))
        {

            if (!value.equals(null)) {
                SdrGis sdrGis = new SdrGis();
                JSONObject jsonObject = (JSONObject) value;
                sdrGis.readFromJSONObject(jsonObject);
                this.GIS = sdrGis;
            }
        }
        else if (key.equalsIgnoreCase("OnTimeComplianceRule"))
        {
            if (!value.equals(null)) {
                SdrOnTimeComplianceRule sdrOnTimeComplianceRule = new SdrOnTimeComplianceRule();
                JSONObject jsonObject = (JSONObject) value;
                sdrOnTimeComplianceRule.readFromJSONObject(jsonObject);
                this.OnTimeComplianceRule = sdrOnTimeComplianceRule;
            }
        }
        else if (key.equalsIgnoreCase("TransportConfig"))
        {
            if (!value.equals(null)) {
                TransportConfigType transportConfigType = TransportConfigType.setIntValue((int) value);
                this.TransportConfig = transportConfigType;
            }
        }
        else if (key.equalsIgnoreCase("Status"))
        {
            if (!value.equals(null)) {
                StoreDeliveryEnums.DeliveryStatus deliveryStatus = StoreDeliveryEnums.DeliveryStatus.setIntValue((int) value);
                this.Status = deliveryStatus;
            }
        }
        else if (key.equalsIgnoreCase("transportStatus"))
        {
            if (!value.equals(null)) {
                StoreDeliveryEnums.TransportStatus transportStatus = StoreDeliveryEnums.TransportStatus.setIntValue((int) value);
                this.transportStatus = transportStatus;
            }
        }
        else if(key.equalsIgnoreCase("EventList"))
        {
            if(this.EventList == null) {
                this.EventList = new ArrayList<PimmEvent>();
            }
            if (!value.equals(null)) {
                if (value instanceof JSONArray) {

                    JSONArray arrData = (JSONArray) value;

                    if (arrData != null) {
                        for (int i = 0; i < arrData.length(); i++) {
                            try {
                                PimmEvent pimmEvent = new PimmEvent();
                                pimmEvent.readFromJSONObject(arrData.getJSONObject(i));

                                this.EventList.add(pimmEvent);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }
        }
        else if (key.equalsIgnoreCase("Trailer"))
        {
            if (value != null) {
                SdrTrailer sdrTrailer = new SdrTrailer();
                JSONObject jsonObject = (JSONObject) value;
                sdrTrailer.readFromJSONObject(jsonObject);
                this.Trailer = sdrTrailer;
            }
        }
        else if (key.equalsIgnoreCase("Driver"))
        {
            if (DMUtils.ValidObject(value)) {
                SdrDriver sdrDriver = new SdrDriver();
                JSONObject jsonObject = (JSONObject) value;
                sdrDriver.readFromJSONObject(jsonObject);
                this.Driver = sdrDriver;
            }
        }
        else if (key.equalsIgnoreCase("CoDriver"))
        {
            if (value != null) {
                if (value instanceof JSONObject) {
                    SdrDriver sdrDriver = new SdrDriver();
                    JSONObject jsonObject = (JSONObject) value;
                    sdrDriver.readFromJSONObject(jsonObject);
                    this.CoDriver = sdrDriver;
                }
            }
        }
        else if(key.equalsIgnoreCase("Capabilities"))
        {
            if(this.Capabilities == null)
            {
                this.Capabilities = new ArrayList<String>();
            }
            if (!value.equals(null)) {
                if (value instanceof JSONArray) {

                    JSONArray capabiliites = (JSONArray) value;

                    if (capabiliites != null) {
                        for (int i = 0; i < capabiliites.length(); i++) {
                            try {
                                this.Capabilities.add(capabiliites.getString(i));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }
        }
        else if(key.equalsIgnoreCase("TripDistanceByHour"))
        {
            if(this.TripDistanceByHour == null) {
                this.TripDistanceByHour = new ArrayList<NumericValue>();
            }
            if (!value.equals(null)) {
                if (value instanceof JSONArray) {

                    JSONArray arrData = (JSONArray) value;

                    if (arrData != null) {
                        for (int i = 0; i < arrData.length(); i++) {
                            try {
                                NumericValue numericValue = new NumericValue();
                                numericValue.readFromJSONObject(arrData.getJSONObject(i));

                                this.TripDistanceByHour.add(numericValue);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }
        } else if(key.equalsIgnoreCase("ArrivalTime")) {
            this.ArrivalTime = JSONDate.convertJSONDateToNSDate(value.toString());
        }
        else if(key.equalsIgnoreCase("DepartureTime")) {
            if (value!=null){
                this.DepartureTime = JSONDate.convertJSONDateToNSDate(value.toString());
            }
        } else {
            super.setValueForKey(value, key);
        }
    }



    public HashMap<String,Object> dictionaryWithValuesForKeys() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();


        dictionary.put("DeliveryId", this.DeliveryId);
        dictionary.put("StartSiteId", this.StartSiteId);
        dictionary.put("EndSiteId", this.EndSiteId);
        dictionary.put("EndSiteName", this.EndSiteName);
        dictionary.put("StartPointPoiId", this.StartPointPoiId);
        dictionary.put("EndPointPoiId", this.EndPointPoiId);
        dictionary.put("DCAddress", this.DCAddress);
        dictionary.put("TMSDeviceId", this.TMSDeviceId);
        dictionary.put("GIS", (this.GIS == null) ? null : this.GIS.dictionaryWithValuesForKeys());

        return dictionary;
    }
}
