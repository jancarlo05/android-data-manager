package com.procuro.apimmdatamanagerlib;

import android.util.Log;

import java.util.HashMap;

/**
 * Created by jophenmarieweeks on 10/07/2017.
 */

public class SdrStopProduct extends PimmBaseObject {


    public String RouteShipmentProductID;
    public int QuantityOrdered;
    public int QuantityShipped;
    public String CodeDate;
    public String Location;
    public UnitEnum Unit;
    public double UnitPrice;
    public double UnitWeight;
    public int DeliveryType;
    public String DeliveryNote;
    public String PO;
    public String AltPO;
    public int Sort;

    public String ProductionDate; // YYMMDD format date from GS1-128 Barcode
    public String PackagingDate;
    public String SellByDate;
    public String ExpirationDate;
    public String LotID;
    public Double CatchWeight ;// double

    @Override
    public void setValueForKey(Object value, String key) {


        if (key.equalsIgnoreCase("Unit")) {
            if (value != null) {
                this.Unit = UnitEnum.setIntValue((int) value);
            }
        }  else {
            super.setValueForKey(value, key);
        }
    }
    public HashMap<String,Object> dictionaryWithValuesForKeys() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();

        dictionary.put("QuantityOrdered", this.QuantityOrdered);
        dictionary.put("QuantityShipped", this.QuantityShipped);
        dictionary.put("Unit", this.Unit);
        dictionary.put("UnitPrice", this.UnitPrice);
        dictionary.put("UnitWeight", this.UnitWeight);
        dictionary.put("DeliveryType", this.DeliveryType);
        dictionary.put("CatchWeight", this.CatchWeight);

        dictionary.put("RouteShipmentProductID", this.RouteShipmentProductID);
        dictionary.put("CodeDate", this.CodeDate);
        dictionary.put("Location", this.Location);
        dictionary.put("PO", this.PO);
        dictionary.put("AltPO", this.AltPO);
        dictionary.put("Sort", this.Sort);
        dictionary.put("ProductionDate", this.ProductionDate);
        dictionary.put("PackagingDate", this.PackagingDate);
        dictionary.put("SellByDate", this.SellByDate);
        dictionary.put("ExpirationDate", this.ExpirationDate);
        dictionary.put("LotID", this.LotID);


        return dictionary;
    }

    public enum UnitEnum {
        Unit_None(0),
        Unit_Bag_in_Box(1),
        Unit_Bread_Box(2),
        Unit_Canister(3),
        Unit_Carton(4),
        Unit_Pallet(5),
        Unit_Bundle(6),
        Unit_Roller_Cage(7),
        Unit_Packet(8),
        Unit_Piece(9),
        Unit_Case(10);

        private int value;
        private UnitEnum(int value)
        {
            this.value=value;
        }

        public int getValue() {
            return this.value;
        }

        public static UnitEnum setIntValue (int i) {
            for (UnitEnum type : UnitEnum.values()) {
                if (type.value == i) { return type; }
            }
            return Unit_None;
        }

    }

    public enum DeliveryTypeEnum{
        POD(0),
        Credit_Memo(1);

        private int value;
        DeliveryTypeEnum(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }

        public static DeliveryTypeEnum setValue(int value) {
            for (DeliveryTypeEnum type : DeliveryTypeEnum.values()) {
                if (type.value == value) {
                    return type; }
            }
            return POD;
        }
    }

}
