package com.procuro.apimmdatamanagerlib;

import java.util.Date;
import java.util.HashMap;

/**
 * Created by jophenmarieweeks on 12/07/2017.
 */

public class ELDEventDTO extends PimmBaseObject {

     public String eldEventID;
     public HOSEnums.HOSEventTypeEnum eventType;
     public int eventCode;
     public Date creationTimeUTC;
     public Date eventTimeUTC;
     public int eldSequenceNumber;
     public String eldDeviceID;
     public HOSEnums.ELDEventProviderEnum eldEventProvider;
     public int odometer; //int
     public double engineHours; //double
     public String locationString;
     public double lat;
     public double lon;

    @Override
    public void setValueForKey(Object value, String key) {

        if (key.equalsIgnoreCase("eventType")) {

            if (!value.equals(null)) {
                HOSEnums.HOSEventTypeEnum hosEventTypeEnum = HOSEnums.HOSEventTypeEnum.setIntValue((int) value);
                this.eventType = hosEventTypeEnum;
            }
        }else if (key.equalsIgnoreCase("eldEventProvider")) {

            if (!value.equals(null)) {
                HOSEnums.ELDEventProviderEnum providerEnum = HOSEnums.ELDEventProviderEnum.setIntValue((int) value);
                this.eldEventProvider = providerEnum;
            }

        }else {
            super.setValueForKey(value, key);
        }
    }

    public HashMap<String,Object> dictionaryWithValuesForKeys() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();

        dictionary.put("eldEventID", this.eldEventID);
        dictionary.put("eldSequenceNumber", this.eldSequenceNumber);
        dictionary.put("eldDeviceID", this.eldDeviceID);
        dictionary.put("eventCode", this.eventCode);
        dictionary.put("eventType", this.eventType);
        dictionary.put("eldEventProvider", this.eldEventProvider);
        dictionary.put("odometer", this.odometer);
        dictionary.put("engineHours", this.engineHours);
        dictionary.put("lat", this.lat);
        dictionary.put("lon", this.lon);

        return dictionary;
    }
}
