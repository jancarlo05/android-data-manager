package com.procuro.apimmdatamanagerlib;

/**
 * Created by jophenmarieweeks on 10/07/2017.
 */

public class SdrStopCompliance extends PimmBaseObject {


    public SdrMetric.MetricClass MetricClass;
    public double Value;
    public String ValueText;
    public String Caption;

    @Override
    public void setValueForKey(Object value, String key) {

        if (key.equalsIgnoreCase("MetricClass")) {
            if (!value.equals(null)) {
                SdrMetric.MetricClass mClass = SdrMetric.MetricClass.setIntValue((int) value);
                this.MetricClass = mClass;
            }
        } else {
            super.setValueForKey(value, key);
        }
    }



}
