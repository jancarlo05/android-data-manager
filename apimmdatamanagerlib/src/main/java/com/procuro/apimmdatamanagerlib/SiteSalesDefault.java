package com.procuro.apimmdatamanagerlib;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class SiteSalesDefault extends  PimmBaseObject {

   public String customerId;
   public int dayOfWeek;
   public int storeType;
   public ArrayList<SiteSalesDefaultData> data;

   @Override
   public void setValueForKey(Object value, String key) {
      super.setValueForKey(value, key);


      if (key.equalsIgnoreCase("data")){
         ArrayList<SiteSalesDefaultData> list = new ArrayList<>();
         if (value!=null){
            JSONArray array = (JSONArray) value;
            for (int i = 0; i <array.length() ; i++) {
               try {
                  JSONObject jsonObject = array.getJSONObject(i);
                  SiteSalesDefaultData item = new SiteSalesDefaultData();
                  item.readFromJSONObject(jsonObject);
                  list.add(item);
               }catch (Exception e){
                  e.printStackTrace();
               }
            }
         }
         this.data = list;
      }
   }


   public Enum getDayOfWeek() {
      for (DayOfWeek type : DayOfWeek.values()){
         if (type.getValue() == dayOfWeek){
            return type;
         }
      }
      return DayOfWeek.MONDAY;
   }

   public Enum getStoreType() {
      for (StoreType type : StoreType.values()){
         if (type.getValue() == storeType){
            return type;
         }
      }
      return StoreType.UNDEFINED;
   }

   public enum StoreType {
      UNDEFINED(-2),
      FULL(0),
      BREAKFAST(1),
      LUNCH(2);

      private final int mValue;

      StoreType(int value) {
         mValue = value;
      }

      public int getValue() {
         return mValue;
      }

   }

   public enum DayOfWeek {
      MONDAY(0),
      TUESDAY(1),
      WEDNESDAY(2),
      THURSDAY(3),
      FRIDAY(4),
      SATURDAY(5),
      SUNDAY(6);

      private final int mValue;

      DayOfWeek(int value) {
         mValue = value;
      }

      public int getValue() {
         return mValue;
      }

   }
}
