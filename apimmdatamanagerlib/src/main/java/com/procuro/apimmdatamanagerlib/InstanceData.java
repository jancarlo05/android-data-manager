package com.procuro.apimmdatamanagerlib;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jophenmarieweeks on 12/07/2017.
 */

public class InstanceData extends PimmBaseObject {

    public PimmInstance pimmInstance;
    public ArrayList<PimmHistory> historyData; // List of PimmHistory objects

    @Override
    public void setValueForKey(Object value, String key)
    {
        if(key.equalsIgnoreCase("historyData")) {
            if (this.historyData == null) {
                this.historyData = new ArrayList<PimmHistory>();
            }

            if (!value.equals(null)) {
                JSONArray arrData = (JSONArray) value;

                if (arrData != null) {
                    for (int i = 0; i < arrData.length(); i++) {
                        try {
                            PimmHistory result = new PimmHistory();
                            result.readFromJSONObject(arrData.getJSONObject(i));

                            this.historyData.add(result);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }else if (key.equalsIgnoreCase("pimmInstance")) {
            if (!value.equals(null)) {
                PimmInstance result = new PimmInstance();
                JSONObject jsonObject = (JSONObject) value;
                result.readFromJSONObject(jsonObject);
                this.pimmInstance = result;
            }

        } else {
            super.setValueForKey(value, key);
        }
    }
}
