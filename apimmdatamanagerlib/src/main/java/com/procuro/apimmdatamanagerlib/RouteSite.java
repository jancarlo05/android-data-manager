package com.procuro.apimmdatamanagerlib;

import java.util.Date;

/**
 * Created by jophenmarieweeks on 10/07/2017.
 */

public class RouteSite extends PimmBaseObject {

    public Date creationDate;
    public String  providerSiteId ;            // the DC
    public String  siteId ;                    // the store
    public String  deliveriesDeviceId ;        // the pimm device where delivery data is posted
    public String shipmentId;                // if not null, the most recent shipment for the site
    public int shipmentStatus;                 // status of last shipment.  default to "unknown"
}
