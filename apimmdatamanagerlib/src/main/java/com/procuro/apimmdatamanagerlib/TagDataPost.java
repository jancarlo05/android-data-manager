package com.procuro.apimmdatamanagerlib;

import java.util.Date;

/**
 * Created by jophenmarieweeks on 01/08/2017.
 */

public class TagDataPost extends PimmBaseObject {

    public Date timestamp;
    public Number temp1;
    public Number humidity;
    public Number signal;
    public Number battery;
    public Number lat;
    public Number lon;
    public Number speed;
    public Number direction;


}
