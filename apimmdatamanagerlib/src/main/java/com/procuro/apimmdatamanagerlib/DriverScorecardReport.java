package com.procuro.apimmdatamanagerlib;

import org.json.JSONObject;

import java.util.Date;

/**
 * Created by jophenmarieweeks on 06/07/2017.
 */

public class DriverScorecardReport extends PimmBaseObject {

    public Date Timestamp;
    public String TimestampText;
    public String Title;
    public int TimezoneId;
    public DriverScorecardBuilder ScorecardData;

    @Override
    public void setValueForKey(Object value, String key) {

        if(key.equalsIgnoreCase("ScorecardData")) {
            DriverScorecardBuilder driverScorecardBuilder = new DriverScorecardBuilder();
            JSONObject jsonObject = (JSONObject) value;
            driverScorecardBuilder.readFromJSONObject(jsonObject);
            this.ScorecardData = driverScorecardBuilder;

         } else {
            super.setValueForKey(value, key);
        }
    }
}
