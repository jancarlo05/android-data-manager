package com.procuro.apimmdatamanagerlib;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import static com.procuro.apimmdatamanagerlib.POISiteDTO.RouteStopTypeEnum.RouteStopType_Undefined;

/**
 * Created by jophenmarieweeks on 12/07/2017.
 */

public class POISiteDTO extends PimmBaseObject {

    public enum RouteStopTypeEnum
    {
        RouteStopType_Undefined(-1),
        RouteStopType_Store(0),
        RouteStopType_Fuel(2),
        RouteStopType_Plant(3),
        RouteStopType_Rest(5),
        RouteStopType_Inspection(6),
        RouteStopType_Service(7),
        RouteStopType_Layover(8),
        RouteStopType_CoffeeStop(9),
        RouteStoptype_Sleeper(10),
        RouteStopType_Traffic(11),
        RouteStopType_TruckWash(13),
        RouteStopType_Shuttle(20),
        RouteStopType_Depot(21),
        RouteStopType_Unauthorized(101);

        private int value;
        private RouteStopTypeEnum(int value)
        {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }

        public static RouteStopTypeEnum setIntValue (int i) {
            for (RouteStopTypeEnum type : RouteStopTypeEnum.values()) {
                if (type.value == i) { return type; }
            }
            return RouteStopType_Undefined;
        }

    }

    public String SiteId;
    public String CustomerId;
    public String CustomerName;
    public String SiteName;
    public AddressComponents Address;
    public String SiteClass;
    public int SiteType; //integer
    public int TimezoneId;
    public LatLon LatLon;
    public RouteStopTypeEnum RouteStopType;
    public boolean dst;
    public int effectiveUTCOffset;



    @Override
    public void setValueForKey(Object value, String key) {

        if(key.equalsIgnoreCase("RouteStopType"))
        {
            if (!value.equals(null)) {
                RouteStopTypeEnum routeStopTypeEnum = RouteStopTypeEnum.setIntValue((int) value);
                this.RouteStopType = routeStopTypeEnum;

                Log.v("DM ", ">>>>RouteStopTypee Value: " + this.RouteStopType);
            }
        }
        else if (key.equalsIgnoreCase("Address"))
        {
            if (!value.equals(null)) {
                AddressComponents addressComponents = new AddressComponents();
                JSONObject jsonObject = (JSONObject) value;
                addressComponents.readFromJSONObject(jsonObject);
                this.Address = addressComponents;
            }
        }
        else if (key.equalsIgnoreCase("LatLon"))
        {
            if (!value.equals(null)) {
                LatLon latLon = new LatLon();
                JSONObject jsonObject = (JSONObject) value;
                latLon.readFromJSONObject(jsonObject);
                this.LatLon = latLon;
            }
        }
        else
        {
            super.setValueForKey(value, key);
        }
    }

    public HashMap<String,Object> dictionaryWithValuesForKeys() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();


        dictionary.put("Id", this.SiteId);
        dictionary.put("CustomerId", this.CustomerId);
        dictionary.put("CustomerName", this.CustomerName);
        dictionary.put("SiteName", this.SiteName);
        dictionary.put("SiteClass", this.SiteClass);
        dictionary.put("TimezoneId", this.TimezoneId);
        dictionary.put("SiteType", this.SiteType);
        dictionary.put("dst", this.dst);
        dictionary.put("effectiveUTCOffset", this.effectiveUTCOffset);

        dictionary.put("Address", (this.Address == null) ? null : this.Address.dictionaryWithValuesForKeys());
        dictionary.put("Address", (this.LatLon == null) ? null : this.LatLon.dictionaryWithValuesForKeys());
        dictionary.put("Address", (this.RouteStopType == null) ? RouteStopType_Undefined : this.RouteStopType);

        return dictionary;
    }

}
