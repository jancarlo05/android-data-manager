package com.procuro.apimmdatamanagerlib;

import java.util.Date;
import java.util.HashMap;

/**
 * Created by jophenmarieweeks on 06/07/2017.
 */

public class AssetProfile_Tractor extends PimmBaseObject {

    public String profileType;
    public String make;
    public String model;
    public String name;
    public String color;
    public String serial;
    public String vin;
    public String engineMake;
    public String engineModel;
    public String engineType;
    public String engineFuelType;
    public String transmission;
    public String brakes;
    public String tireMake;
    public String tireModel;
    public String tireSize;
    public String ecmMake;
    public String ecmModel;
    public String additionalFeatures;
    public String notes;

    public int year;
    public int engineHorsePower;
    public int engineTorque;
    public int tireCount;
    public int tirePSI;
    public int tireWarrantyMiles;
    public int autoAirPressureSystemSetting;

    public Date purchaseDate;
    public Date warrantyStartDate;
    public Date warrantyEndDate;
    public Date leaseStartDate;
    public Date leaseEndDate;
    public Date engineWarrantyStartDate;
    public Date engineWarrantyEndDate;

    public double engineSize;
    public double engineFuelEconomyRating;
    public int tractorType;

    public boolean hasABS;
    public boolean hasTelematics;
    public boolean hasDriveCam;
    public boolean hasAutoAirPressureSystem;
    public boolean hasSleeper;

    public HashMap<String,Object> dictionaryWithValuesForKeys() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();

        dictionary.put("hosEventID", this.profileType);
        dictionary.put("make", this.make);
        dictionary.put("name", this.name);
        dictionary.put("model", this.model);
        dictionary.put("year", this.year);
        dictionary.put("color", this.color);
        dictionary.put("serial", this.serial);
        dictionary.put("vin", this.vin);
        dictionary.put("engineMake", this.engineMake);
        dictionary.put("engineModel", this.engineModel);
        dictionary.put("engineSize", this.engineSize);
        dictionary.put("engineType", this.engineType);
        dictionary.put("engineHorsePower", this.engineHorsePower);
        dictionary.put("engineTorque", this.engineTorque);
        dictionary.put("engineFuelType", this.engineFuelType);
        dictionary.put("engineFuelEconomyRating", this.engineFuelEconomyRating);
        dictionary.put("transmission", this.transmission);
        dictionary.put("brakes", this.brakes);
        dictionary.put("engineFuelType", this.engineFuelType);
        dictionary.put("hasABS", this.hasABS);
        dictionary.put("tireCount", this.tireCount);
        dictionary.put("tireMake", this.tireMake);
        dictionary.put("tireModel", this.tireModel);
        dictionary.put("tireSize", this.tireSize);
        dictionary.put("tirePSI", this.tirePSI);
        dictionary.put("tireWarrantyMiles", this.tireWarrantyMiles);
        dictionary.put("ecmMake", this.ecmMake);
        dictionary.put("ecmModel", this.ecmModel);
        dictionary.put("hasTelematics", this.hasTelematics);
        dictionary.put("hasDriveCam", this.hasDriveCam);
        dictionary.put("hasAutoAirPressureSystem", this.hasAutoAirPressureSystem);
        dictionary.put("autoAirPressureSystemSetting", this.autoAirPressureSystemSetting);
        dictionary.put("hasSleeper", this.hasSleeper);
        dictionary.put("additionalFeatures", this.additionalFeatures);
        dictionary.put("notes", this.notes);
        dictionary.put("purchaseDate", this.purchaseDate.toString());

        dictionary.put("warrantyStartDate", this.warrantyStartDate.toString());
        dictionary.put("warrantyEndDate", this.warrantyEndDate.toString());
        dictionary.put("leaseStartDate", this.leaseStartDate.toString());
        dictionary.put("leaseEndDate", this.leaseEndDate.toString());
        dictionary.put("engineWarrantyStartDate", this.engineWarrantyStartDate.toString());
        dictionary.put("engineWarrantyEndDate", this.engineWarrantyEndDate.toString());


        return dictionary;
    }

}
