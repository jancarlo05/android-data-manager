package com.procuro.apimmdatamanagerlib;

import org.json.JSONObject;

import java.util.Date;

/**
 * Created by jophenmarieweeks on 11/07/2017.
 */

public class SdVehicleStatus extends PimmBaseObject {

    public String SiteId;
    public Date StartTime;
    public Date EndTime;
    public int status;
    public  double FuelIn;
    public  double FuelOut;
    public  double ReeferHoursIn;
    public  double ReeferHoursOut;
    public String AssetTrackingRegionId;


    public String VehicleId; // trailer/tractor deviceId
    public String VehicleName; // trailer/tractor device name
    public String DeliveryId; // current active route
    public Date Timestamp; // timestamp when status information was updated, can be null if unkown

    public Boolean OnTheRoad;  // is the vehicle on-the-road, so not at a stop
    public  DeliveryPOITypeEnum AtLocation; // the type of location the vehicle is current at. Undefined (-1) means not at a stop

    public StoreDeliveryEnums.GenericOnOffEnum ReeferPower;
    public StoreDeliveryEnums.DoorAccessEnum DoorStatus;
    public boolean OOS; // out of service
    public String RouteName;
    public StoreDeliveryEnums.DeliveryStatus DeliveryStatus;
    public StoreDeliveryEnums.TransportStatus DeliveryTransportStatus;
    public int DeliveryFlagStatus; // Need to replace NSInteger with EventPrioroty Enum
    public boolean DeliveryTemperatureOK;

    public String VendorId;

    public SdOOSInfo DoorOOSInfo;
    public SdOOSInfo ReeferOOSInfo;

    public SdVehicleAssetTracking Tracking;
    @Override
    public void setValueForKey(Object value, String key) {

        if (key.equalsIgnoreCase("Tracking")) {
            if (!value.equals(null)) {
                SdVehicleAssetTracking result = new SdVehicleAssetTracking();
                JSONObject jsonObject = (JSONObject) value;
                result.readFromJSONObject(jsonObject);
                this.Tracking = result;
            }

        }else if (key.equalsIgnoreCase("ReeferOOSInfo")) {
            if (!value.equals(null)) {
                SdOOSInfo result = new SdOOSInfo();
                JSONObject jsonObject = (JSONObject) value;
                result.readFromJSONObject(jsonObject);
                this.ReeferOOSInfo = result;
            }

        }else if (key.equalsIgnoreCase("DoorOOSInfo")) {
            if (!value.equals(null)) {
                SdOOSInfo result = new SdOOSInfo();
                JSONObject jsonObject = (JSONObject) value;
                result.readFromJSONObject(jsonObject);
                this.DoorOOSInfo = result;
            }
        }else if(key.equalsIgnoreCase("DoorStatus")) {
            if (!value.equals(null)) {
                StoreDeliveryEnums.DoorAccessEnum DoorAccess = StoreDeliveryEnums.DoorAccessEnum.setIntValue((int) value);
                this.DoorStatus = DoorAccess;
            }
        }else if(key.equalsIgnoreCase("ReeferPower")) {
            if (!value.equals(null)) {
                StoreDeliveryEnums.GenericOnOffEnum genericOnOffEnum = StoreDeliveryEnums.GenericOnOffEnum.setIntValue((int) value);
                this.ReeferPower = genericOnOffEnum;
            }

        }else if(key.equalsIgnoreCase("DeliveryStatus")) {
            if (!value.equals(null)) {
                StoreDeliveryEnums.DeliveryStatus delivery = StoreDeliveryEnums.DeliveryStatus.setIntValue((int) value);
                this.DeliveryStatus = delivery;
            }
        }else   if(key.equalsIgnoreCase("DeliveryTransportStatus")) {
            if (!value.equals(null)) {
                StoreDeliveryEnums.TransportStatus trans = StoreDeliveryEnums.TransportStatus.setIntValue((int) value);
                this.DeliveryTransportStatus = trans;
            }


        }else{
            super.setValueForKey(value, key);
        }
    }



    }
