package com.procuro.apimmdatamanagerlib;

import org.json.JSONObject;

/**
 * Created by jophenmarieweeks on 10/07/2017.
 */

public class VehicleDTO extends PimmBaseObject {
    public DriverScorecardReport Scorecard;
    public DriverScorecardReport DriverRollup;

     public DashboardEvents Events;
     public DashboardUpdates Updates;
     public DriverLeaderboard Leaderboard;




    @Override
    public void setValueForKey(Object value, String key) {

        if(key.equalsIgnoreCase("Scorecard")) {
            DriverScorecardReport driverScorecardReport = new DriverScorecardReport();
            JSONObject jsonObject = (JSONObject) value;
            driverScorecardReport.readFromJSONObject(jsonObject);
            this.Scorecard = driverScorecardReport;

        }else if(key.equalsIgnoreCase("DriverRollup")){

            DriverScorecardReport driverScorecardReport = new DriverScorecardReport();
            JSONObject jsonObject = (JSONObject) value;
            driverScorecardReport.readFromJSONObject(jsonObject);
            this.DriverRollup = driverScorecardReport;

        }else if(key.equalsIgnoreCase("Events")){

            DashboardEvents dashboardEvents = new DashboardEvents();
            JSONObject jsonObject = (JSONObject) value;
            dashboardEvents.readFromJSONObject(jsonObject);
            this.Events = dashboardEvents;
        }else if(key.equalsIgnoreCase("Updates")){

            DashboardUpdates dashboardUpdates = new DashboardUpdates();
            JSONObject jsonObject = (JSONObject) value;
            dashboardUpdates.readFromJSONObject(jsonObject);
            this.Updates = dashboardUpdates;

        }else if(key.equalsIgnoreCase("Leaderboard")){

            DriverLeaderboard driverLeaderboard = new DriverLeaderboard();
            JSONObject jsonObject = (JSONObject) value;
            driverLeaderboard.readFromJSONObject(jsonObject);
            this.Leaderboard = driverLeaderboard;

        } else {
            super.setValueForKey(value, key);
        }
    }
}
