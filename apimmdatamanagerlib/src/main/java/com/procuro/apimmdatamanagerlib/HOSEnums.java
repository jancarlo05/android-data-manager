package com.procuro.apimmdatamanagerlib;

/**
 * Created by jophenmarieweeks on 27/07/2017.
 */

public class HOSEnums extends PimmBaseObject {

    public enum HOSEventTypeEnum {
        DRIVER_STATUS_CHANGE(1),
        INTERMEDIATE_LOG(2),
        USAGE(3),
        DRIVER_CERTIFICATION(4),
        SIGN_IN(5),
        ENGINE_POWER(6),
        ELD_ERROR(7),
        EXCEPTION(8);   // non-standard for ELD, these are the HOS exceptions such as "weather/adverse driving conditions", "16 hour short-haul", "34 hour reset", etc.

        private int value;
        private HOSEventTypeEnum(int value)
        {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }

        public static HOSEventTypeEnum setIntValue (int i) {
            for (HOSEventTypeEnum type : HOSEventTypeEnum.values()) {
                if (type.value == i) { return type; }
            }
            return DRIVER_STATUS_CHANGE;
        }

    }

   public enum DriverStatusChangeEnum
    {
        OFF_DUTY(1),
        SLEEPER_BERTH(2),
        DRIVING(3),
        ON_DUTY_NON_DRIVING(4),
        PUNCH_IN(5),
        PUNCH_OUT_OFFDUTY(6),
        BREAK(7),
        LAYOVER(8),
        DELIVERY(9),
        PUNCH_OUT_SLEEPER(10),
        USAGE_PERSONAL_USE(101),
        USAGE_YARD_MOVE(102);

        private int value;
        private DriverStatusChangeEnum(int value)
        {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }

        public static DriverStatusChangeEnum setIntValue (int i) {
            for (DriverStatusChangeEnum type : DriverStatusChangeEnum.values()) {
                if (type.value == i) { return type; }
            }
            return OFF_DUTY;
        }
    }


   public enum EngineStatusCodeEnum
    {
        ENGINE_POWER_UP(1),
        ENGINE_POWER_UP_REDUCED_LOCATION_PRECISION(2),
        ENGINE_SHUTDOWN(3),
        ENGINE_SHUTDOWN_REDUCED_LOCATION_PRECISION(4);

        private int value;
        private EngineStatusCodeEnum(int value)
        {
            this.value = value;
        }
        public int getValue() {
            return this.value;
        }

        public static EngineStatusCodeEnum setIntValue (int i) {
            for (EngineStatusCodeEnum type : EngineStatusCodeEnum.values()) {
                if (type.value == i) { return type; }
            }
            return ENGINE_POWER_UP;
        }

    }

   public enum ELDLoginEventCodeEnum
    {
        ELD_LOGIN(1),
        ELD_LOGOUT(2);

        private int value;
        private ELDLoginEventCodeEnum(int value)
        {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }

        public static ELDLoginEventCodeEnum setIntValue (int i) {
            for (ELDLoginEventCodeEnum type : ELDLoginEventCodeEnum.values()) {
                if (type.value == i) { return type; }
            }
            return ELD_LOGIN;
        }

    }

   public enum DriverVehicleSpecialUsageEventCodeEnum
    {
        AUTHORIZED_PERSONAL_USE(1),
        YARD_MOVES(2),
        CLEAR(0);

        private int value;
        private DriverVehicleSpecialUsageEventCodeEnum(int value)
        {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }

        public static DriverVehicleSpecialUsageEventCodeEnum setIntValue (int i) {
            for (DriverVehicleSpecialUsageEventCodeEnum type : DriverVehicleSpecialUsageEventCodeEnum.values()) {
                if (type.value == i) { return type; }
            }
            return CLEAR;
        }
    }

   public enum IntermediateLogEventCodeEnum
    {
        INTERMEDIATE_LOG_CONVENTIONAL(1),
        INTERMEDIATE_LOG_REDUCED_LOCATION_PRECISION(2);

        private int value;
        private IntermediateLogEventCodeEnum(int value)
        {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }

        public static IntermediateLogEventCodeEnum setIntValue (int i) {
            for (IntermediateLogEventCodeEnum type : IntermediateLogEventCodeEnum.values()) {
                if (type.value == i) { return type; }
            }
            return INTERMEDIATE_LOG_CONVENTIONAL;
        }
    }

    public enum ELDDiagnosticEventCodeEnum
    {
        ELD_MALFUNCTION_LOGGED(1),
        ELD_MALFUNCTION_CLEARED(2),
        ELD_DATA_DIAGNOSTIC_EVENT_LOGGED(3),
        ELD_DATA_DIAGNOSTIC_EVENT_CLEARED(4);

        private int value;
        private ELDDiagnosticEventCodeEnum(int value)
        {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }

        public static ELDDiagnosticEventCodeEnum setIntValue (int i) {
            for (ELDDiagnosticEventCodeEnum type : ELDDiagnosticEventCodeEnum.values()) {
                if (type.value == i) { return type; }
            }
            return ELD_MALFUNCTION_LOGGED;
        }
    }

   public enum HOSExceptionTypeEnum
    {
        HOS_EXCEPTION_BADWEATHER(1),
        HOS_EXCEPTION_BIGDAY(3),
        HOS_EXCEPTION_34HOUR_RESET(3),
        HOS_EXCEPTION_14HOUR_START(4);

        private int value;
        private HOSExceptionTypeEnum(int value)
        {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }

        public static HOSExceptionTypeEnum setIntValue (int i) {
            for (HOSExceptionTypeEnum type : HOSExceptionTypeEnum.values()) {
                if (type.value == i) { return type; }
            }
            return HOS_EXCEPTION_BADWEATHER;
        }

    }

    public enum HOSEventStatusEnum
    {
        ACTIVE(1),
        INACTIVE_CHANGED(2),
        INACTIVE_CHANGE_REQUESTED(3),
        INACTIVE_CHANGE_REJECTED(4);

        private int value;
        private HOSEventStatusEnum(int value)
        {
            this.value = value;
        }


        public int getValue() {
            return this.value;
        }

        public static HOSEventStatusEnum setIntValue (int i) {
            for (HOSEventStatusEnum type : HOSEventStatusEnum.values()) {
                if (type.value == i) { return type; }
            }
            return ACTIVE;
        }

    }

    public enum HOSEventSourceEnum
    {
        AUTOMATIC(1),
        DRIVER(2),
        OTHER_USER(3),
        ASSUMED_FROM_UNASSIGNED(4);

        private int value;
        private HOSEventSourceEnum(int value)
        {
            this.value = value;
        }


        public int getValue() {
            return this.value;
        }

        public static HOSEventSourceEnum setIntValue (int i) {
            for (HOSEventSourceEnum type : HOSEventSourceEnum.values()) {
                if (type.value == i) { return type; }
            }
            return AUTOMATIC;
        }

    }

   public enum ELDEventProviderEnum
    {   // this is internal to PIMM, not part of the DoT spec
        ELD_DEVICE(1),
        MOBILE_DEVICE(2);

        private int value;
        private ELDEventProviderEnum(int value)
        {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }

        public static ELDEventProviderEnum setIntValue (int i) {
            for (ELDEventProviderEnum type : ELDEventProviderEnum.values()) {
                if (type.value == i) { return type; }
            }
            return ELD_DEVICE;
        }

    }


    public enum HOSExemptionStatusEnum
    {
        NONEXEMPT(1),
        EXEMPT(2);

        private int value;
        private HOSExemptionStatusEnum(int value)
        {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }

        public static HOSExemptionStatusEnum setIntValue (int i) {
            for (HOSExemptionStatusEnum type : HOSExemptionStatusEnum.values()) {
                if (type.value == i) { return type; }
            }
            return NONEXEMPT;
        }

    }

    public enum HOSUsageEnum
    {
        PERSONAL_USE(1),
        YARD_MOVE(2);

        private int value;
        private HOSUsageEnum(int value)
        {
            this.value = value;
        }


        public int getValue() {
            return this.value;
        }

        public static HOSUsageEnum setIntValue (int i) {
            for (HOSUsageEnum type : HOSUsageEnum.values()) {
                if (type.value == i) { return type; }
            }
            return PERSONAL_USE;
        }

    }


    public enum DriverHOSStatusEnum
    {
        DriverHOSStatus_Undefined(0),
        DriverHOSStatus_PunchIn(1),
        DriverHOSStatus_OnDuty(2),
        DriverHOSStatus_Delivery(3),
        DriverHOSStatus_OnBreak(4),
        DriverHOSStatus_Driving(5),
        DriverHOSStatus_Sleeper(6),
        DriverHOSStatus_OffDuty(7),
        DriverHOSStatus_PunchOut_OffDuty(8),
        DriverHOSStatus_Layover(9),
        DriverHOSStatus_YardMoves(10),
        DriverHOSStatus_PersonalUse(11),
        DriverHOSStatus_PunchOut_Sleeper(12);

        private int value;
        private DriverHOSStatusEnum(int value)
        {
            this.value = value;
        }


        public int getValue() {
            return this.value;
        }

        public static DriverHOSStatusEnum setIntValue (int i) {
            for (DriverHOSStatusEnum type : DriverHOSStatusEnum.values()) {
                if (type.value == i) { return type; }
            }
            return DriverHOSStatus_Undefined;
        }
    }

    public enum VehicleMotionStatusEnum
    {
        VEHICLE_MOTION_STATE_UNKNOWN(0),
        VEHICLE_IN_MOTION(1),
        VEHICLE_NOT_IN_MOTION(2),
        VEHICLE_ENGINE_ON(3),
        VEHICLE_ENGINE_OFF(4);

        private int value;
        private VehicleMotionStatusEnum(int value)
        {
            this.value = value;
        }


        public int getValue() {
            return this.value;
        }

        public static VehicleMotionStatusEnum setIntValue (int i) {
            for (VehicleMotionStatusEnum type : VehicleMotionStatusEnum.values()) {
                if (type.value == i) { return type; }
            }
            return VEHICLE_MOTION_STATE_UNKNOWN;
        }
    }


    public HOSEventTypeEnum hosEventTypeEnum;
    public DriverStatusChangeEnum driverStatusChangeEnum;
    public VehicleMotionStatusEnum vehicleMotionStatusEnum;
    public HOSEventStatusEnum hosEventStatusEnum;
    public HOSEventSourceEnum hosEventSourceEnum;
    public ELDEventProviderEnum eldEventProviderEnum;
    public HOSExemptionStatusEnum hosExemptionStatusEnum;
    public HOSUsageEnum hosUsageEnum;
    public EngineStatusCodeEnum engineStatusCodeEnum;
    public ELDLoginEventCodeEnum eldLoginEventCodeEnum;
    public DriverVehicleSpecialUsageEventCodeEnum driverVehicleSpecialUsageEventCodeEnum;
    public IntermediateLogEventCodeEnum intermediateLogEventCodeEnum;
    public ELDDiagnosticEventCodeEnum eldDiagnosticEventCodeEnum;
    public HOSExceptionTypeEnum hosExceptionTypeEnum;
    public DriverHOSStatusEnum driverHOSStatusEnum;


    @Override
    public void setValueForKey(Object value, String key) {

        if(key.equalsIgnoreCase("hosEventTypeEnum")) {

            if (!value.equals(null)) {
                HOSEventTypeEnum hosEventType = HOSEventTypeEnum.setIntValue((int) value);
                this.hosEventTypeEnum = hosEventType;

            }
        }else if(key.equalsIgnoreCase("driverStatusChangeEnum")){
            if (!value.equals(null)) {
                DriverStatusChangeEnum driverStatusChange = DriverStatusChangeEnum.setIntValue((int) value);
                this.driverStatusChangeEnum = driverStatusChange;
            }
        }else if(key.equalsIgnoreCase("vehicleMotionStatusEnum")){
            if (!value.equals(null)) {
                VehicleMotionStatusEnum vehicleMotionStatus = VehicleMotionStatusEnum.setIntValue((int) value);
                this.vehicleMotionStatusEnum = vehicleMotionStatus;
            }
        }else if(key.equalsIgnoreCase("hosEventStatusEnum")){
            if (!value.equals(null)) {
                HOSEventStatusEnum hosEventStatus = HOSEventStatusEnum.setIntValue((int) value);
                this.hosEventStatusEnum = hosEventStatus;
            }
        }else if(key.equalsIgnoreCase("hosEventSourceEnum")){
            if (!value.equals(null)) {
                HOSEventSourceEnum hosEventSource = HOSEventSourceEnum.setIntValue((int) value);
                this.hosEventSourceEnum = hosEventSource;
            }
        }else if(key.equalsIgnoreCase("eldEventProviderEnum")){
            if (!value.equals(null)) {
                ELDEventProviderEnum eldEventProvider = ELDEventProviderEnum.setIntValue((int) value);
                this.eldEventProviderEnum = eldEventProvider;
            }
        }else if(key.equalsIgnoreCase("hosExemptionStatusEnum")){
            if (!value.equals(null)) {
                HOSExemptionStatusEnum hosExemptionStatus = HOSExemptionStatusEnum.setIntValue((int) value);
                this.hosExemptionStatusEnum = hosExemptionStatus;
            }
        }else if(key.equalsIgnoreCase("hosUsageEnum")){
            if (!value.equals(null)) {
                HOSUsageEnum hosUsage = HOSUsageEnum.setIntValue((int) value);
                this.hosUsageEnum = hosUsage;
            }
        }else if(key.equalsIgnoreCase("engineStatusCodeEnum")){
            if (!value.equals(null)) {
                EngineStatusCodeEnum engineStatusCode = EngineStatusCodeEnum.setIntValue((int) value);
                this.engineStatusCodeEnum = engineStatusCode;
            }
        }else if(key.equalsIgnoreCase("eldLoginEventCodeEnum")){
            if (!value.equals(null)) {
                ELDLoginEventCodeEnum eldLoginEventCode = ELDLoginEventCodeEnum.setIntValue((int) value);
                this.eldLoginEventCodeEnum = eldLoginEventCode;
            }
        }else if(key.equalsIgnoreCase("driverVehicleSpecialUsageEventCodeEnum")){
            if (!value.equals(null)) {
                DriverVehicleSpecialUsageEventCodeEnum driverVehicleSpecialUsageEventCode = DriverVehicleSpecialUsageEventCodeEnum.setIntValue((int) value);
                this.driverVehicleSpecialUsageEventCodeEnum = driverVehicleSpecialUsageEventCode;
            }
        }else if(key.equalsIgnoreCase("intermediateLogEventCodeEnum")){
            if (!value.equals(null)) {
                IntermediateLogEventCodeEnum intermediateLogEventCode = IntermediateLogEventCodeEnum.setIntValue((int) value);
                this.intermediateLogEventCodeEnum = intermediateLogEventCode;
            }
        }else if(key.equalsIgnoreCase("eldDiagnosticEventCodeEnum")){
            if (!value.equals(null)) {
                ELDDiagnosticEventCodeEnum eldDiagnosticEventCodeEnum = ELDDiagnosticEventCodeEnum.setIntValue((int) value);
                this.eldDiagnosticEventCodeEnum = eldDiagnosticEventCodeEnum;
            }
        }else if(key.equalsIgnoreCase("hosExceptionTypeEnum")){
            if (!value.equals(null)) {
                HOSExceptionTypeEnum hosExceptionType = HOSExceptionTypeEnum.setIntValue((int) value);
                this.hosExceptionTypeEnum = hosExceptionType;
            }
        }else if(key.equalsIgnoreCase("driverHOSStatusEnum")){
            if (!value.equals(null)) {
                DriverHOSStatusEnum driverHOSStatusEnum = DriverHOSStatusEnum.setIntValue((int) value);
                this.driverHOSStatusEnum = driverHOSStatusEnum;
            }
        } else {
            super.setValueForKey(value, key);
        }
    }

}
