package com.procuro.apimmdatamanagerlib;

import java.math.BigDecimal;
import java.util.Date;

public class SiteSalesForecast extends  PimmBaseObject {

   public Date startTime;
   public Date endTime;
   public BigDecimal percentage;

   @Override
   public void setValueForKey(Object value, String key) {
      super.setValueForKey(value, key);

      if (key.equalsIgnoreCase("percentage")){
         if (value!=null){
            try {
               percentage = new BigDecimal(value.toString());
            }catch (Exception e){
               e.printStackTrace();
            }
         }
      }

      if (key.equalsIgnoreCase("startTime")){
         if (value!=null){
            try {
               startTime = DateTimeFormat.ConvertISOToDate(value.toString());
            }catch (Exception e){
               e.printStackTrace();
            }
         }
      }

      if (key.equalsIgnoreCase("endTime")){
         if (value!=null){
            try {
               endTime = DateTimeFormat.ConvertISOToDate(value.toString());
            }catch (Exception e){
               e.printStackTrace();
            }
         }
      }

   }
}
