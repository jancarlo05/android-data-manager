package com.procuro.apimmdatamanagerlib;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class FSLDaySummary extends PimmBaseObject {
    public ArrayList<CorpStructure>corpStructure;
    public ArrayList<FSLSite>siteList;
    public ArrayList<FSLAggregateData>aggregateDataList;
    public ArrayList<FSLSummary>summaryList;
    public ArrayList<FSLAggregateSiteData>aggregateSiteDataList;

    @Override
    public void setValueForKey(Object value, String key) {
        super.setValueForKey(value, key);

        if (key.equalsIgnoreCase("corpStructure")) {
            ArrayList<CorpStructure> corpStructures = new ArrayList<>();
            if (value!=null){
                JSONArray jsonArray = (JSONArray) value;
                for (int i = 0; i < jsonArray.length(); i++) {
                    try {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        CorpStructure corpStructure = new CorpStructure();
                        corpStructure.readFromJSONObject(jsonObject);
                        corpStructures.add(corpStructure);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            this.corpStructure = corpStructures;
        }

        else if (key.equalsIgnoreCase("siteList")) {
            ArrayList<FSLSite> list = new ArrayList<>();
            if (value != null) {
                JSONArray jsonArray = (JSONArray) value;
                for (int i = 0; i < jsonArray.length(); i++) {
                    try {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        FSLSite item = new FSLSite();
                        item.readFromJSONObject(jsonObject);
                        list.add(item);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            this.siteList = list;
        }

        else if (key.equalsIgnoreCase("aggregateDataList")) {
            ArrayList<FSLAggregateData> list = new ArrayList<>();
            if (value != null) {
                JSONArray jsonArray = (JSONArray) value;
                for (int i = 0; i < jsonArray.length(); i++) {
                    try {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        FSLAggregateData item = new FSLAggregateData();
                        item.readFromJSONObject(jsonObject);
                        list.add(item);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            this.aggregateDataList = list;
        }

        else if (key.equalsIgnoreCase("summaryList")) {
            ArrayList<FSLSummary> list = new ArrayList<>();
            if (value != null) {
                JSONArray jsonArray = (JSONArray) value;
                for (int i = 0; i < jsonArray.length(); i++) {
                    try {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        FSLSummary item = new FSLSummary();
                        item.readFromJSONObject(jsonObject);
                        list.add(item);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            this.summaryList = list;
        }

        else if (key.equalsIgnoreCase("aggregateSiteDataList")) {
            ArrayList<FSLAggregateSiteData> list = new ArrayList<>();
            if (value != null) {
                JSONArray jsonArray = (JSONArray) value;
                for (int i = 0; i < jsonArray.length(); i++) {
                    try {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        FSLAggregateSiteData item = new FSLAggregateSiteData();
                        item.readFromJSONObject(jsonObject);
                        list.add(item);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            this.aggregateSiteDataList = list;
        }

    }
    public HashMap<String,Object> dictionaryWithValuesForKeys() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();
        dictionary.put("corpStructure",this.corpStructure);
        return dictionary;
    }

}
