package com.procuro.apimmdatamanagerlib;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by jophenmarieweeks on 15/08/2017.
 */

public class AssetTrackingRegionDTO extends PimmBaseObject {
    public String assetTrackingRegionId;
    public String name;
    public ArrayList<AssetTrackingRegionMarkerDTO> markers;

    @Override
    public void setValueForKey(Object value, String key) {

        if (key.equalsIgnoreCase("markers")) {
            if (this.markers == null) {
                this.markers = new ArrayList<AssetTrackingRegionMarkerDTO>();
            }

            if (!value.equals(null)) {
                JSONArray arrData = (JSONArray) value;

                if (arrData != null) {
                    for (int i = 0; i < arrData.length(); i++) {
                        try {
                            AssetTrackingRegionMarkerDTO assetTrackingRegion= new AssetTrackingRegionMarkerDTO();
                            assetTrackingRegion.readFromJSONObject(arrData.getJSONObject(i));

                            this.markers.add(assetTrackingRegion);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
    }


    public HashMap<String,Object> dictionaryWithValuesForKeys() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();

        dictionary.put("assetTrackingRegionId" , this.assetTrackingRegionId);
        dictionary.put("name" , this.name);

        if (this.markers != null) {

            ArrayList<HashMap<String, Object>> AssetArray = new ArrayList<>();

            for(AssetTrackingRegionMarkerDTO assetValue : this.markers) {

                HashMap<String, Object> propertyValueDict = assetValue.dictionaryWithValuesForKeys();
                AssetArray.add(propertyValueDict);

            }

            dictionary.put("siteProperties", AssetArray);

        }else{
            dictionary.put("siteProperties", null);
        }

        return  dictionary;
    }
}
