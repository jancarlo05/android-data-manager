package com.procuro.apimmdatamanagerlib;

/**
 * Created by jophenmarieweeks on 06/07/2017.
 */

public class ProductCategoryEnum extends PimmBaseObject {

    //public enum ProductCategoryEnum {  //duplicate class error
    public enum ProductCategorysEnum {
        Category_None(0),
        Category_Food(1),
        Category_Unknown2(2),
        Category_Paper(3),
        Category_Cleaning(4),
        Category_Uniform(5),
        Category_Replacement(6),
        Category_Other(7),
        Category_Taxes(8);

        private int value;
        private ProductCategorysEnum(int value)
        {
            this.value = value;
        }


        public int getValue() {
            return this.value;
        }

        public static ProductCategorysEnum setIntValue (int i) {
            for (ProductCategorysEnum type : ProductCategorysEnum.values()) {
                if (type.value == i) { return type; }
            }
            return Category_None;
        }
    }

    public ProductCategorysEnum productCategorysEnum;

    @Override
    public void setValueForKey(Object value, String key) {

        if(key.equalsIgnoreCase("productCategorysEnum")) {
            if (!value.equals(null)) {
                ProductCategorysEnum productCategorys = ProductCategorysEnum.setIntValue((int) value);
                this.productCategorysEnum = productCategorys;

            }

        } else {
            super.setValueForKey(value, key);
        }
    }


}
