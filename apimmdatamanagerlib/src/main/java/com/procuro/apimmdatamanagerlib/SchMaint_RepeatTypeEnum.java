package com.procuro.apimmdatamanagerlib;

/**
 * Created by jophenmarieweeks on 07/07/2017.
 */

public class SchMaint_RepeatTypeEnum extends PimmBaseObject {
    public enum SchMaint_RepeatType
    {
        Day(0),
        Week(1),
        Month(2),
        Year(3),
        DayOfWeek(4),
        DayOfMonth(5),
        Distance(6),
        Usage(7);

        private int value;
        private SchMaint_RepeatType(int value)
        {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }

        public static SchMaint_RepeatType setIntValue (int i) {
            for (SchMaint_RepeatType type : SchMaint_RepeatType.values()) {
                if (type.value == i) { return type; }
            }
            return Day;
        }
    }

    public SchMaint_RepeatType schMaint_repeatTypeEnum;

    @Override
    public void setValueForKey(Object value, String key) {

        if (key.equalsIgnoreCase("schMaint_repeatTypeEnum")) {

            if (!value.equals(null)) {
                SchMaint_RepeatType repeatTypeEnum = SchMaint_RepeatType.setIntValue((int) value);
                this.schMaint_repeatTypeEnum = repeatTypeEnum;
            }

        }else {
            super.setValueForKey(value, key);
        }
    }


}
