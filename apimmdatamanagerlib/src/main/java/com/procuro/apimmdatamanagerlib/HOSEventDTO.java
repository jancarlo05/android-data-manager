package com.procuro.apimmdatamanagerlib;

import java.text.DateFormat;
import java.util.Date;
import java.util.HashMap;

import static com.procuro.apimmdatamanagerlib.HOSEnums.HOSEventSourceEnum.AUTOMATIC;
import static com.procuro.apimmdatamanagerlib.HOSEnums.HOSEventStatusEnum.ACTIVE;
import static com.procuro.apimmdatamanagerlib.HOSEnums.HOSEventTypeEnum.DRIVER_STATUS_CHANGE;

/**
 * Created by jophenmarieweeks on 01/08/2017.
 */

public class HOSEventDTO extends PimmBaseObject {
    public String hosEventID;
    public HOSEnums.HOSEventTypeEnum eventType;
    public int eventCode;

    public Date creationTimeUTC;
    public Date eventTimeUTC;

//Note: Following two fields are not part of Server Side object model.

    public String eldUserName; //generally Logged-in User Name
    public Date creationTime;
//    public Date eventTime;
    public String eventTimeStr; ///<HHMMSS> Format <HH> between 00-23, <MM> and <SS> between 00-59

    public String eventDateStr; ///<MMDDYY> Format  where <MM>  between 01 -12, <DD> between 01-31, <YY> between 00-99

///////////////////////////

    public  int eldSequenceNumber;
    public HOSEnums.HOSEventSourceEnum eventSource;
    public HOSEnums.HOSEventStatusEnum eventStatus;
    public String eldDeviceID;
    public String eldEventID;
    public String driverUserID;
    public String eventSourceUserID;
    public int odometer; //int
    public double engineHours; //double
    public String locationString;
    public double lat;
    public double lon;

    public String annotation;
    public String checksum;
    public String refHOSEventID;
    public Date updatedTimeUTC;
//    public Date  rejectedTimeUTC;

    public String truckName;
    public String truckDeviceID;

    public String trailer1Name;
    public String trailer1DeviceID;


    public String trailer2Name;
    public String trailer2DeviceID;

    public String shippingDocumentName;
    public String shippingDocumentID;

    public boolean exemptionStatus;

    public String auditID; //links all events that are edited as part of a single logical op
    public String coDriverFirstName;
    public String coDriverLastName;
    public String coDriverUserID;



//-(NSDictionary*) printHOSEvent;

    @Override
    public void setValueForKey(Object value, String key) {

        if(key.equalsIgnoreCase("eventType")) {
            if (!value.equals(null)) {
                if (!value.equals(null)) {
                    HOSEnums.HOSEventTypeEnum hosEventTypeEnum = HOSEnums.HOSEventTypeEnum.setIntValue((int) value);
                    this.eventType = hosEventTypeEnum;
                }
            }
        }else if(key.equalsIgnoreCase("eventSource")) {
            if (!value.equals(null)) {
                if (!value.equals(null)) {
                    HOSEnums.HOSEventSourceEnum hosEventSourceEnum = HOSEnums.HOSEventSourceEnum.setIntValue((int) value);
                    this.eventSource = hosEventSourceEnum;
                }
            }

        }else if(key.equalsIgnoreCase("eventStatus")) {
            if (!value.equals(null)) {
                if (!value.equals(null)) {
                    HOSEnums.HOSEventStatusEnum hosEventSourceEnum = HOSEnums.HOSEventStatusEnum.setIntValue((int) value);
                    this.eventStatus = hosEventSourceEnum;
                }
            }

        } else {
            super.setValueForKey(value, key);
        }
    }

    public HashMap<String,Object> dictionaryWithValuesForKeys() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();

        dictionary.put("hosEventID", this.hosEventID);
        dictionary.put("eldSequenceNumber", this.eldSequenceNumber);
        dictionary.put("eldUserName", this.eldUserName);
        dictionary.put("driverUserID", this.driverUserID);
        dictionary.put("eldDeviceID", this.eldDeviceID);
        dictionary.put("eventCode", this.eventCode);
        dictionary.put("eventType", (this.eventType == null) ? DRIVER_STATUS_CHANGE : this.eventType.getValue());
        dictionary.put("eventSource", (this.eventSource== null) ? AUTOMATIC : this.eventSource.getValue());
        dictionary.put("eventStatus", (this.eventStatus== null) ? ACTIVE : this.eventStatus.getValue());
        dictionary.put("eventDateStr", this.eventDateStr);
        dictionary.put("eventTimeStr", this.eventTimeStr);
        dictionary.put("eventTimeUTC", this.eventTimeUTC);
        dictionary.put("odometer", this.odometer);
        dictionary.put("engineHours", this.engineHours);
        dictionary.put("lat", this.lat);
        dictionary.put("lon", this.lon);
        dictionary.put("checksum", this.checksum);
        dictionary.put("eventSourceUserID", this.eventSourceUserID);

        dictionary.put("coDriverFirstName", this.coDriverFirstName);
        dictionary.put("coDriverLastName", this.coDriverLastName);
        dictionary.put("coDriverUserID", this.coDriverUserID);
        if(this.locationString != null) {
            dictionary.put("locationString", this.locationString);
        }
        if(this.auditID != null) {
            dictionary.put("auditID", this.auditID);
        }

        if(this.updatedTimeUTC != null) {
            dictionary.put("updatedTimeUTC", this.updatedTimeUTC);
        }
        if(this.shippingDocumentName != null) {
            dictionary.put("shippingDocumentName", this.shippingDocumentName);
        }
        if(this.truckName != null) {
            dictionary.put("truckName", this.truckName);
        }

        return dictionary;
    }

    public HashMap<String,Object> printHOSEvent() {

        HashMap<String, Object> dictionary = new HashMap<String, Object>();

        DateFormat dateShort = DateFormat.getDateInstance(DateFormat.SHORT);
        String eventDateStr = dateShort.format(this.eventTimeUTC);
        String eventTimeStr = dateShort.format(DateTimeFormat.TimeOnlyStringForDateTime(this.eventTimeUTC));

        String eventType = "";

        switch (this.eventType) {
            case DRIVER_STATUS_CHANGE:
                eventType = "DRIVER_STATUS_CHANGE";
                break;
            case INTERMEDIATE_LOG:
                eventType = "INTERMEDIATE_LOG";
                break;
            case USAGE:
                eventType = "SPECIAL_USAGE";
                break;
            case DRIVER_CERTIFICATION:
                eventType = "DRIVER_CERTIFICATION";
                break;
            case SIGN_IN:
                eventType = "SIGN_IN";
                break;
            case ENGINE_POWER:
                eventType = "ENGINE_POWER";
                break;
            case ELD_ERROR:
                eventType = "ELD_ERROR";
                break;
            case EXCEPTION:
                eventType = "EXCEPTION";
                break;
            default:
                break;
        }

        dictionary.put("hosEventID", this.hosEventID);
        dictionary.put("eldSequenceNumber", this.eldSequenceNumber);
        dictionary.put("eldUserName", this.eldUserName);
        dictionary.put("driverUserID", this.driverUserID);
        dictionary.put("eldDeviceID", this.eldDeviceID);
        dictionary.put("eventCode", this.eventCode);
        dictionary.put("eventType", (this.eventType == null) ? DRIVER_STATUS_CHANGE : this.eventType.getValue());
        dictionary.put("eventSource", (this.eventSource== null) ? AUTOMATIC : this.eventSource.getValue());
        dictionary.put("eventStatus", (this.eventStatus== null) ? ACTIVE : this.eventStatus.getValue());
        dictionary.put("eventDateStr", this.eventDateStr);
        dictionary.put("eventTimeStr", this.eventTimeStr);
        dictionary.put("eventTimeUTC", this.eventTimeUTC);
        dictionary.put("odometer", this.odometer);
        dictionary.put("engineHours", this.engineHours);
        dictionary.put("lat", this.lat);
        dictionary.put("lon", this.lon);
        dictionary.put("checksum", this.checksum);
        dictionary.put("eventSourceUserID", this.eventSourceUserID);
        if(this.locationString != null) {
            dictionary.put("locationString", this.locationString);
        }
        if(this.auditID != null) {
            dictionary.put("auditID", this.auditID);
        }

        if(this.updatedTimeUTC != null) {
            dictionary.put("updatedTimeUTC", this.updatedTimeUTC);
        }

        if(this.creationTime != null){
            dictionary.put("creationTimeUTC", this.creationTimeUTC);
        }
        return dictionary;
    }
}
