package com.procuro.apimmdatamanagerlib;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by jophenmarieweeks on 11/08/2017.
 */

public class DataSeries extends PimmBaseObject {



   public class GPSLocation extends PimmBaseObject{

       public int time;
       public double lat;
       public double lon;
       public double speed;
       public double heading;

//(id)initWithTime:(int) time
//       lat:(float) lat
//       lon:(float) lon
//       speed:(float) speed
//       heading:(float) heading;

        public void initWithTime(int time, double lat, double lon, double speed, double heading){
            this.time = time;
            this.lat = lat;
            this.speed = speed;
            this.heading = heading;
        }
   }




    public class DataSeriesQueryItem extends  PimmBaseObject{

        public String metricId;
        public int samplingMethod;
        public Date start;
        public Date end;

        public HashMap<String,Object> dictionaryWithValuesForKeys() {
            HashMap<String, Object> dictionary = new HashMap<String, Object>();

            dictionary.put("metricId", this.metricId);
            dictionary.put("samplingMethod", this.samplingMethod);
            dictionary.put("start", (this.start == null) ? null : DateTimeFormat.convertToJsonDateTime(this.start));
            dictionary.put("end", (this.end == null) ? null : DateTimeFormat.convertToJsonDateTime(this.end));



            return dictionary;
        }


        public void initWithShipmentID(String shipmentID, Date startDate, Date endDate){
            this.metricId = String.format("%@$gps", shipmentID);
            this.samplingMethod = 8;
            this.start = startDate;
            this.end = endDate;
        }

        public void initWithTracKey(String tracKey, Date startDate, Date endDate){
            this.metricId = String.format("%@$gps", tracKey);
            this.samplingMethod = 8;
            this.start = startDate;
            this.end = endDate;
        }

    }

    public class DataSeriesQuery extends PimmBaseObject {

        public ArrayList<DataSeriesQueryItem> queries;
        public String siteId;


        @Override
        public void setValueForKey(Object value, String key) {

            if (key.equalsIgnoreCase("queries")) {
                if (this.queries == null) {
                    this.queries = new ArrayList<DataSeriesQueryItem>();
                }

                if (!value.equals(null)) {
                    JSONArray arrData = (JSONArray) value;

                    if (arrData != null) {
                        for (int i = 0; i < arrData.length(); i++) {
                            try {
                                DataSeriesQueryItem result = new DataSeriesQueryItem();
                                result.readFromJSONObject(arrData.getJSONObject(i));

                                this.queries.add(result);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            } else {
            super.setValueForKey(value, key);
            }
        }
        public HashMap<String,Object> dictionaryWithValuesForKeys() {
            HashMap<String, Object> dictionary = new HashMap<String, Object>();

            dictionary.put("siteId", this.siteId);

            if (this.queries != null) {

                ArrayList<HashMap<String, Object>> queryDic = new ArrayList<>();

                for(DataSeriesQueryItem propertyValue : this.queries) {

                    HashMap<String, Object> propertyValueDict = propertyValue.dictionaryWithValuesForKeys();
                    queryDic.add(propertyValueDict);

                }

                dictionary.put("queries", queryDic);

            }else{
                dictionary.put("queries", null);
            }

            return dictionary;
        }

    }



    public String type;
    public String metricId;
    public int interval;
    public double startOffset;
    public Date start;
    public Date end;
    public Date preSampleTime;
    public int preSampleValue;
    public ArrayList<GPSLocation> data;


    public void setValueForKey(Object value, String key) {

        if (key.equalsIgnoreCase("start")) {
            this.start = JSONDate.convertJSONDateToNSDate(String.valueOf(value));
            Log.v("start", "Value: " + this.start);

        } else if (key.equalsIgnoreCase("end")) {
            this.end = JSONDate.convertJSONDateToNSDate(String.valueOf(value));
            Log.v("start", "Value: " + this.end);
        }else if (key.equalsIgnoreCase("data")) {
                if (this.data == null) {
                    this.data = new ArrayList<GPSLocation>();
                }
            double GPS_FIELDS = 5.0;
            double GPS_MULTIPLER = 1000000.0;
            double TIME_RESOLUTION = 60000.0;


            if (!value.equals(null)) {
                    JSONArray arrData = (JSONArray) value;

                    if (arrData != null) {
                        for (int i = 0; i < arrData.length(); i++) {
                            try {
                                GPSLocation result = new GPSLocation();
                                result.readFromJSONObject(arrData.getJSONObject(i));

                                this.data.add(result);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
        } else {
            super.setValueForKey(value, key);
        }
    }

    public HashMap<String,Object> dictionaryWithValuesForKeys() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();

        dictionary.put("data", this.data);
        dictionary.put("start", this.start);
        dictionary.put("end", this.end);
        dictionary.put("preSampleTime", this.preSampleTime);
        dictionary.put("type", this.type);
        dictionary.put("metricId", this.metricId);
        dictionary.put("interval", this.interval);

        return dictionary;
    }



}
