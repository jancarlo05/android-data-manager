package com.procuro.apimmdatamanagerlib;

import org.json.JSONObject;

/**
 * Created by jophenmarieweeks on 10/07/2017.
 */

public class IFTA_ECM extends PimmBaseObject {

    public   IFTABuilder IFTAResults;
    public   IFTABuilder IFTAReportResults;

    public void setValueForKey(Object value, String key) {

        if (key.equalsIgnoreCase("IFTAResults")) {
            if (!value.equals(null)) {
                IFTABuilder iftaBuilder = new IFTABuilder();
                JSONObject jsonObject = (JSONObject) value;
                iftaBuilder.readFromJSONObject(jsonObject);
                this.IFTAResults = iftaBuilder;
            }

        }else if (key.equalsIgnoreCase("IFTAReportResults")) {
            if (!value.equals(null)) {
                IFTABuilder iftaBuilder = new IFTABuilder();
                JSONObject jsonObject = (JSONObject) value;
                iftaBuilder.readFromJSONObject(jsonObject);
                this.IFTAReportResults = iftaBuilder;
            }

        } else {
            super.setValueForKey(value, key);
        }

    }




}
