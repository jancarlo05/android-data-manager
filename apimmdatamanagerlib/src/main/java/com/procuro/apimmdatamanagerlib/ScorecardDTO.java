package com.procuro.apimmdatamanagerlib;

import org.json.JSONObject;

import java.util.HashMap;

public class ScorecardDTO extends PimmBaseObject {

    public FacilityScorecardDTO facilityScorecard;
    public SupplierDeliveryPlantScorecardDTO supplierDeliveryPlantScorecard;
    public SupplierDeliveryDCScorecardDTO supplierDeliveryDCScorecard;
    public StoreDeliveryScorecardDTO storeDeliveryScorecard;

    @Override
    public void setValueForKey(Object value, String key)
    {

        if (key.equalsIgnoreCase("facilityScorecard")) {
            if (value != null) {
                FacilityScorecardDTO result = new FacilityScorecardDTO();
                JSONObject jsonObject = (JSONObject) value;
                result.readFromJSONObject(jsonObject);
                this.facilityScorecard = result;
            }
        } else if (key.equalsIgnoreCase("supplierDeliveryDCScorecard")) {
            if (value != null) {
                if (value instanceof JSONObject) {
                    SupplierDeliveryDCScorecardDTO result = new SupplierDeliveryDCScorecardDTO();
                    JSONObject jsonObject = (JSONObject) value;
                    result.readFromJSONObject(jsonObject);
                    this.supplierDeliveryDCScorecard = result;
                }
            }
        } else if (key.equalsIgnoreCase("supplierDeliveryPlantScorecard")) {
            if (value != null) {
                if (value instanceof JSONObject) {
                    SupplierDeliveryPlantScorecardDTO result = new SupplierDeliveryPlantScorecardDTO();
                    JSONObject jsonObject = (JSONObject) value;
                    result.readFromJSONObject(jsonObject);
                    this.supplierDeliveryPlantScorecard = result;
                }
            }
        } else if (key.equalsIgnoreCase("storeDeliveryScorecard")) {
            if (value != null) {
                if (value instanceof JSONObject) {
                    StoreDeliveryScorecardDTO result = new StoreDeliveryScorecardDTO();
                    JSONObject jsonObject = (JSONObject) value;
                    result.readFromJSONObject(jsonObject);
                    this.storeDeliveryScorecard = result;
                }
            }
        }else {
            super.setValueForKey(value, key);
        }
     }

    public HashMap<String,Object> dictionaryWithValuesForKeys() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();

        dictionary.put("facilityScorecard",this.facilityScorecard.dictionaryWithValuesForKeys());
        dictionary.put("supplierDeliveryDCScorecard",this.supplierDeliveryDCScorecard.dictionaryWithValuesForKeys());
        dictionary.put("supplierDeliveryPlantScorecard",this.supplierDeliveryPlantScorecard.dictionaryWithValuesForKeys());
        dictionary.put("storeDeliveryScorecard",this.storeDeliveryScorecard.dictionaryWithValuesForKeys());

        return dictionary;
    }







}
