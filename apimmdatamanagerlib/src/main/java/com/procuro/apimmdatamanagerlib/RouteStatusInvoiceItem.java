package com.procuro.apimmdatamanagerlib;

/**
 * Created by jophenmarieweeks on 12/07/2017.
 */

public class RouteStatusInvoiceItem extends PimmBaseObject {

    public String RouteShipmentInvoiceItemID;
    public double Amount;
    public String PO;

}
