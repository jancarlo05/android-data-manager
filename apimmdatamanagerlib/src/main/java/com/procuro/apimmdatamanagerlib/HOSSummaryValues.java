package com.procuro.apimmdatamanagerlib;

import java.util.HashMap;

/**
 * Created by jophenmarieweeks on 12/07/2017.
 */

public class HOSSummaryValues extends PimmBaseObject {

    public int driveTime;
    public int onDutyTime;
    public int prepTime;
    public int restTime;
    public int deliveryTime;
    public int postTripTime;
    public int sleeperTime;


    public HashMap<String,Object> dictionaryWithValuesForKeys() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();

        dictionary.put("driveTime", this.driveTime);
        dictionary.put("onDutyTime", this.onDutyTime);
        dictionary.put("prepTime", this.prepTime);
        dictionary.put("restTime", this.restTime);
        dictionary.put("deliveryTime", this.deliveryTime);
        dictionary.put("postTripTime", this.postTripTime);
        dictionary.put("sleeperTime", this.sleeperTime);

        return dictionary;
    }

}
