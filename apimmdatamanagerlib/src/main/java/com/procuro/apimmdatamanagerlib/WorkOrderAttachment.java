package com.procuro.apimmdatamanagerlib;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by jophenmarieweeks on 10/07/2017.
 */

public class WorkOrderAttachment extends PimmBaseObject {


    public String AttachmentID;
    public String WorkOrderServiceID;
    public String AttachmentType;
    public String AttachmentName;
    public Date CreationDate;
    public String oid;
    public ArrayList<String> body;
    //@property (nonatomic,retain) NSData* AttachmentContent;

}
