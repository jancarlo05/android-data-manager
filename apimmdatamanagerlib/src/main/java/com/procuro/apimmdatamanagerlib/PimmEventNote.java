package com.procuro.apimmdatamanagerlib;

import java.util.Date;

/**
 * Created by jophenmarieweeks on 06/07/2017.
 */

public class PimmEventNote extends PimmBaseObject {

    public int note_id;
    public int event_id;
    public Date creationTime;
    public String userId;
    public String msg;
    public String username;

}
