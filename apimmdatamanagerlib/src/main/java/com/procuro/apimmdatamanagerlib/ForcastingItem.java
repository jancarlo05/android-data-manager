package com.procuro.apimmdatamanagerlib;

import java.util.HashMap;

public class ForcastingItem extends  PimmBaseObject {

    public String actual;
    public String difference;
    public String projected;
    public String item;


    @Override
    public void setValueForKey(Object value, String key) {
            super.setValueForKey(value, key);

            if (key.equalsIgnoreCase("actual")){
                String string = "";
                if (value!=null){
                    string = (String) value.toString();
                }
                this.actual = string;
            }

        if (key.equalsIgnoreCase("difference")){
            String string = "";
            if (value!=null){
                string = (String) value.toString();
            }
            this.difference = string;
        }

        if (key.equalsIgnoreCase("item")){
            String string = "";
            if (value!=null){
                string = (String) value.toString();
            }
            this.item = string;
        }

        if (key.equalsIgnoreCase("projected")){
            String string = "";
            if (value!=null){
                string = (String) value.toString();
            }
            this.projected = string;
        }

    }


    public HashMap<String,Object> dictionaryWithValuesForKeys() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();

        dictionary.put("actual", this.actual);
        dictionary.put("difference", this.difference);
        dictionary.put("projected", this.projected);
        dictionary.put("item", this.item);

        return dictionary;
    }
}
