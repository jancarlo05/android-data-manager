package com.procuro.apimmdatamanagerlib;

import java.util.ArrayList;

/**
 * Created by jophenmarieweeks on 01/08/2017.
 */

public class Connections extends PimmBaseObject {

    public CustomerConnection supplier;
    public DepotConnection plant;

    public ArrayList<AssetConnection> trackingRecorders;
    public ArrayList<CustomerConnection> customers;
    public ArrayList<ProductConnection> products;
    public ArrayList<CarrierConnection> carriers;
    public ArrayList<CustomerConnection> brokers;

//public ArrayList<AssetConnection *> *trackingRecorders;
//    public ArrayList<CustomerConnection *> *customers;
//    public ArrayList<ProductConnection *> *products;
//    public ArrayList<CarrierConnection *> *carriers;
//    public ArrayList<CustomerConnection *> *brokers;

//    @property (nonatomic, strong) NSMutableDictionary<NSString *, CustomerConnection *> *dc;
//    @property (nonatomic, strong) NSMutableDictionary<NSString *, NSMutableArray<NSString *> *> *customerDC;
//    @property (nonatomic, strong) NSMutableDictionary<NSString *, NSMutableArray<DepotConnection *> *> *dcDepots;


}
