package com.procuro.apimmdatamanagerlib;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

public class SMSDailyOpsPlanSpeedOfServiceItem extends  PimmBaseObject {

    public String item;
    public ArrayList<SpeedOfServiceItemDaypartItems>daypartItems;



    @Override
    public void setValueForKey(Object value, String key) {
        super.setValueForKey(value, key);

        if (key.equalsIgnoreCase("item")){
            this.item = (String)value;
        }

        if (key.equalsIgnoreCase("daypartItems")){

            if (value!=null){
                ArrayList<SpeedOfServiceItemDaypartItems>items = new ArrayList<>();
                JSONArray jsonArray = (JSONArray) value;
                for (int i = 0; i <jsonArray.length() ; i++) {
                    SpeedOfServiceItemDaypartItems item = new SpeedOfServiceItemDaypartItems();
                    try {
                        item.readFromJSONObject(jsonArray.getJSONObject(i));
                        items.add(item);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                this.daypartItems = items;
            }

        }

    }

    public HashMap<String,Object> dictionaryWithValuesForKeys() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();

        dictionary.put("item", this.item);
        dictionary.put("daypartItems", this.daypartItems);



        return dictionary;
    }

}
