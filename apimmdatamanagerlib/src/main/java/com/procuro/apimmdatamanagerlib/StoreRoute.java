package com.procuro.apimmdatamanagerlib;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

/**
 * Created by jophenmarieweeks on 10/07/2017.
 */

public class StoreRoute extends PimmBaseObject {

   public enum StoreRouteTypeEnum
    {
        StoreRouteType_All(-1),
        StoreRouteType_StoreDelivery(0),
        StoreRouteType_Shuttle(1),
        StoreRouteType_AdHoc(10);

        private int value;
        private StoreRouteTypeEnum(int value)
        {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }

        public static StoreRouteTypeEnum setIntValue (int i) {
            for (StoreRouteTypeEnum type : StoreRouteTypeEnum.values()) {
                if (type.value == i) { return type; }
            }
            return StoreRouteType_All;
        }
    }
    public String routeID ;
    public String routeName ;
    public int dayOfWeek ;
    public ArrayList<Site> regularStops;
    public String shipmentID ;
    public String container ;
    public String driver ;
    public StoreRouteTypeEnum routeType ;

    @Override
    public void setValueForKey(Object value, String key) {

        if (key.equalsIgnoreCase("routeType")) {
            if (!value.equals(null)) {
                StoreRouteTypeEnum result = StoreRouteTypeEnum.setIntValue((int) value);
                this.routeType = result;
            }
        }else if (key.equalsIgnoreCase("regularStops")) {

                if (this.regularStops == null) {
                    this.regularStops = new ArrayList<Site>();
                }

                if (!value.equals(null)) {
                    if (value instanceof JSONArray) {

                        JSONArray arrVals = (JSONArray) value;

                        if (arrVals != null) {
                            for (int i = 0; i < arrVals.length(); i++) {
                                try {
                                    Site results = new Site();
                                    results.readFromJSONObject(arrVals.getJSONObject(i));

                                    this.regularStops.add(results);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                    }
                }
        } else {
            super.setValueForKey(value, key);
        }
    }


}
