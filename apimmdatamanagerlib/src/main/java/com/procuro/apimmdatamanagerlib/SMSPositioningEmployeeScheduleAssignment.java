package com.procuro.apimmdatamanagerlib;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

public class SMSPositioningEmployeeScheduleAssignment extends PimmBaseObject {

    public Date startDateISO;
    public Date endDateISO;
    public Date endDate;
    public Date startDate;
    public ArrayList<SMSPositioningEmployeeScheduleAssignmentPositions>positions;


    @Override
    public void setValueForKey(Object value, String key) {
        super.setValueForKey(value, key);

        if (key.equalsIgnoreCase("startDateISO")){
            if (value!=null){
                this.startDateISO = JSONDate.convertJSONDateToNSDate(value.toString());
            }
        }
        if (key.equalsIgnoreCase("endDateISO")){
            if (value!=null){
                this.endDateISO = JSONDate.convertJSONDateToNSDate(value.toString());
            }
        }
        if (key.equalsIgnoreCase("endDate")){
            if (value!=null){
                this.endDate = JSONDate.convertJSONDateToNSDate(value.toString());
            }
        }
        if (key.equalsIgnoreCase("startDate")){
            if (value!=null){
                this.startDate = JSONDate.convertJSONDateToNSDate(value.toString());
            }
        }
        if (key.equalsIgnoreCase("positions")){
            ArrayList<SMSPositioningEmployeeScheduleAssignmentPositions>positions = new ArrayList<>();
            if (value!=null){
                JSONArray jsonArray = (JSONArray) value;
                for (int i = 0; i <jsonArray.length() ; i++) {
                    SMSPositioningEmployeeScheduleAssignmentPositions position = new SMSPositioningEmployeeScheduleAssignmentPositions();
                    try {
                        position.readFromJSONObject(jsonArray.getJSONObject(i));
                        positions.add(position);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                this.positions = positions;
            }
        }

    }


    public HashMap<String,Object> dictionaryWithValuesForKeys() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();

        dictionary.put("startDateISO", this.startDateISO);
        dictionary.put("endDateISO", this.endDateISO);
        dictionary.put("endDate", this.endDate);
        dictionary.put("startDate", this.startDate);
        dictionary.put("positions", this.positions);

        return dictionary;
    }

    
}
