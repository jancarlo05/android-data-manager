package com.procuro.apimmdatamanagerlib;

/**
 * Created by jophenmarieweeks on 10/07/2017.
 */

public class SdrChartSeries extends PimmBaseObject {

   public String MetricId;
   public String Title;
   public String Description;
}
