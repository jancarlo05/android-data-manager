package com.procuro.apimmdatamanagerlib;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jophenmarieweeks on 06/07/2017.
 */

public class DeliveryStopActionResult extends PimmBaseObject {

    public String DeliveryId;
    public String DeletedPOIId;

    public boolean Rebuild;
    public boolean Changed;

    //  Updated state for the SdrDelivery object that reflect changes applied
    //      to the stop affected in this call.
    public int Delivery_StopsTotal;
    public int Delivery_DynamicStops;
    public int Delivery_PlannedStops;
    public int Delivery_StopsMade;
    public int Delivery_StopsMissed;
    public int Delivery_ScheduledStops;
    public int Delivery_ScheduledStopsMade;
    public int Delivery_StopsOnTime;
    public int Delivery_StopsNotOnTime;
    public int Delivery_EarlyStops;
    public int Delivery_LateStops;
    public int Delivery_TotalMinutesLate;

    //classes
    //The token to be associated with the route for use in refresh calls.
    public int RebuildToken;

    // The DTO containing the updated state for the stop.

    public SdrStop Stop;
    public ArrayList<SdrStop> Stops;


    @Override
    public void setValueForKey(Object value, String key) {

        if (key.equalsIgnoreCase("Stop")) {

            SdrStop sdrStop = new SdrStop();
            JSONObject jsonObject = (JSONObject) value;
            sdrStop.readFromJSONObject(jsonObject);
            this.Stop = sdrStop;

        } else if (key.equalsIgnoreCase("Stops")) {
            if (this.Stops == null) {
                this.Stops = new ArrayList<SdrStop>();
            }

            if (!value.equals(null)) {
                JSONArray arrStops  = (JSONArray) value;

                if (arrStops != null) {
                    for (int i = 0; i < arrStops.length(); i++) {
                        try {
                            SdrStop sdrStop = new SdrStop();
                            sdrStop.readFromJSONObject(arrStops.getJSONObject(i));

                            this.Stops.add(sdrStop);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }

                Log.v("Data","Value: "+this.Stops);
            }
        } else {
            super.setValueForKey(value, key);
        }
    }
}
