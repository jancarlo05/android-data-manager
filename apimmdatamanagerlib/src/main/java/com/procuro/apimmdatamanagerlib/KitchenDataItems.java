package com.procuro.apimmdatamanagerlib;

import java.util.HashMap;

public class KitchenDataItems extends PimmBaseObject {

    public int height;
    public int width;
    public int ycoordinate;
    public int xcoordinate;
    public String buildingType;
    public String imageType;
    public String text;
    public String kitchenType;


    @Override
    public void setValueForKey(Object value, String key) {
        super.setValueForKey(value, key);

        if (key.equalsIgnoreCase("height")){
            this.height = (int) value;
        }

        if (key.equalsIgnoreCase("width")){
            this.width = (int) value;
        }

        if (key.equalsIgnoreCase("ycoordinate")){
            this.ycoordinate = (int) value;
        }

        if (key.equalsIgnoreCase("xcoordinate")){
            this.xcoordinate = (int) value;
        }

        if (key.equalsIgnoreCase("buildingType")){
            this.buildingType = (String) value;
        }

        if (key.equalsIgnoreCase("imageType")){
            this.imageType = (String) value;
        }

        if (key.equalsIgnoreCase("text")){
            this.text = (String) value;
        }

        if (key.equalsIgnoreCase("kitchenType")){
            this.kitchenType = (String) value;
        }
    }

    public HashMap<String,Object> dictionaryWithValuesForKeys() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();

        dictionary.put("height",this.height);
        dictionary.put("width",this.width);
        dictionary.put("ycoordinate",this.ycoordinate);
        dictionary.put("xcoordinate",this.xcoordinate);
        dictionary.put("buildingType",this.buildingType);
        dictionary.put("imageType",this.imageType);
        dictionary.put("text",this.text);
        dictionary.put("kitchenType",this.kitchenType);

        return dictionary;
    }




}
