package com.procuro.apimmdatamanagerlib;

import java.util.Date;
import java.util.HashMap;

/**
 * Created by jophenmarieweeks on 30/08/2017.
 */

public class RouteShuttleDTO extends PimmBaseObject {

   public enum ShuttleContainerType
    {
        ShuttleContainerType_None(0),
        ShuttleContainerType_Trailer(1),
        ShuttleContainerType_ShippingContainer(2);

        private int value;
        private ShuttleContainerType(int value)
        {
            this.value =value;
        }

        public int getValue() {
            return this.value;
        }

        public static ShuttleContainerType setIntValue (int i) {
            for (ShuttleContainerType type : ShuttleContainerType.values()) {
                if (type.value == i) { return type; }
            }
            return ShuttleContainerType_None;
        }
    }


    public enum HazMatType
    {
        HazMatType_NotHazMat(0),
        HazMatType_HazMat(1);

        private int value;
        private HazMatType(int value)
        {
            this.value =value;
        }

        public int getValue() {
            return this.value;
        }

        public static HazMatType setIntValue (int i) {
            for (HazMatType type : HazMatType.values()) {
                if (type.value == i) { return type; }
            }
            return HazMatType_NotHazMat;
        }
    }

    public String routeShuttleId;
    public String shipmentId;

    public String dcSiteId;
    public Date routeDay;
    public Date creationDate;
    public String originSiteId;
    public String driverUserId;
    public String tractorDeviceId;
    public String destinationSiteId;
    public String destinationYardLocation;
    public String container;
    public ShuttleContainerType containerType;
    public int containerSize;

    public int hazMatContainer;
    public int hazMatTractor;
    public int hazMatTrailer;
    public HazMatType hazMat;

    public String commodity;
    public String notes;
    public String po;
    public String bol;

    public Date originPlanArrivalTime;
    public Date originPlanDepartTime;
    public Date originArrivalTime;
    public Date originDepartTime;
    public int originOdometer;
    public double originTemperature;
    public String originNotes;

    public Date destinationPlanArrivalTime;
    public Date destinationPlanDepartTime;
    public Date destinationArrivalTime;
    public Date destinationDepartTime;

    public Date availableDate;
    public Date lastDetentionDate;
    public Date lastStorageDate;

    public int destinationOdometer;
    public double destinationTemperature;
    public String destinationNotes;

    public int deliveryOrder;

    @Override
    public void setValueForKey(Object value, String key) {

        if (key.equalsIgnoreCase("containerType")) {

            if (!value.equals(null)) {
                ShuttleContainerType shuttleContainerType = ShuttleContainerType.setIntValue((int) value);
                this.containerType = shuttleContainerType;
            }
        }else if (key.equalsIgnoreCase("hazMat")) {

            if (!value.equals(null)) {
                HazMatType hazMatType = HazMatType.setIntValue((int) value);
                this.hazMat = hazMatType;
            }

        }else {
            super.setValueForKey(value, key);
        }
    }


    public HashMap<String,Object> dictionaryWithValuesForKeys() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();

        dictionary.put("routeShuttleId", this.routeShuttleId);
        dictionary.put("shipmentId", this.shipmentId);
        dictionary.put("dcSiteId", this.dcSiteId);
        dictionary.put("driverUserId", this.driverUserId);
        dictionary.put("tractorDeviceId", this.tractorDeviceId);
        dictionary.put("originSiteId", this.originSiteId);
        dictionary.put("destinationSiteId", this.destinationSiteId);
        dictionary.put("destinationYardLocation", this.destinationYardLocation);
        dictionary.put("container", this.container);
        dictionary.put("commodity", this.commodity);
        dictionary.put("po", this.po);
        dictionary.put("bol", this.bol);
        dictionary.put("originNotes", this.originNotes);
        dictionary.put("destinationNotes", this.destinationNotes);

        dictionary.put("routeDay", (this.routeDay == null) ? null : DateTimeFormat.convertToJsonDateTime(this.routeDay));
        dictionary.put("creationDate", (this.creationDate == null) ? null : DateTimeFormat.convertToJsonDateTime(this.creationDate));
        dictionary.put("originPlanArrivalTime", (this.originPlanArrivalTime == null) ? null : DateTimeFormat.convertToJsonDateTime(this.originPlanArrivalTime));
        dictionary.put("originPlanDepartTime", (this.originPlanDepartTime == null) ? null : DateTimeFormat.convertToJsonDateTime(this.originPlanDepartTime));
        dictionary.put("originArrivalTime", (this.originArrivalTime == null) ? null : DateTimeFormat.convertToJsonDateTime(this.originArrivalTime));
        dictionary.put("originDepartTime", (this.originDepartTime == null) ? null : DateTimeFormat.convertToJsonDateTime(this.originDepartTime));
        dictionary.put("destinationPlanArrivalTime", (this.destinationPlanArrivalTime == null) ? null : DateTimeFormat.convertToJsonDateTime(this.destinationPlanArrivalTime));
        dictionary.put("destinationPlanDepartTime", (this.destinationPlanDepartTime == null) ? null : DateTimeFormat.convertToJsonDateTime(this.destinationPlanDepartTime));

        dictionary.put("destinationArrivalTime", (this.destinationArrivalTime == null) ? null : DateTimeFormat.convertToJsonDateTime(this.destinationArrivalTime));
        dictionary.put("destinationDepartTime", (this.destinationDepartTime == null) ? null : DateTimeFormat.convertToJsonDateTime(this.destinationDepartTime));
        dictionary.put("availableDate", (this.availableDate == null) ? null : DateTimeFormat.convertToJsonDateTime(this.availableDate));
        dictionary.put("lastStorageDate", (this.lastStorageDate == null) ? null : DateTimeFormat.convertToJsonDateTime(this.lastStorageDate));

        dictionary.put("containerType", this.containerType.getValue());
        dictionary.put("hazMat", this.hazMat.getValue());
        dictionary.put("containerSize",this.containerSize);
        dictionary.put("hazMatContainer",this.hazMatContainer);
        dictionary.put("hazMatTractor",this.hazMatTractor);
        dictionary.put("hazMatTrailer",this.hazMatTrailer);
        dictionary.put("originOdometer",this.originOdometer);
        dictionary.put("originTemperature",this.originTemperature);
        dictionary.put("destinationOdometer",this.destinationOdometer);
        dictionary.put("destinationTemperature",this.destinationTemperature);
        dictionary.put("deliveryOrder",this.deliveryOrder);

        return dictionary;
    }


}
