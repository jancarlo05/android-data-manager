package com.procuro.apimmdatamanagerlib;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by jophenmarieweeks on 12/07/2017.
 */

public class DeliveryStatusDTO extends PimmBaseObject {

    public String DeliveryId ;
    public Date Timestamp ;
    public String RouteId ;
    public String RouteName ;

    public StoreDeliveryEnums.DeliveryStatus Status;
    public StoreDeliveryEnums.TransportStatus TransportStatus;
    public int FlagStatus ;
    public boolean Terminated ;
    public boolean Active ;
    public boolean TemperatureOK ;

    public String TrailerId ;
    public String TrailerName ;
    public String  Tractor ;
    public String TractorId ;

    public DeliveryStatusVehicleDTO TrailerVehicleStatus;
    public DeliveryStatusVehicleDTO TractorVehicleStatus;
    public DeliveryStatusDriverDTO Driver;
    public DeliveryStatusDriverDTO CoDriver;

    public Date DispatchTime;
    public ArrayList<DeliveryStatusStopDTO> Stops; //List of DeliveryStatusStopDTO objects

    @Override
    public void setValueForKey(Object value, String key) {

        if(key.equalsIgnoreCase("TransportStatus")) {

            StoreDeliveryEnums.TransportStatus trans = StoreDeliveryEnums.TransportStatus.setIntValue((int) value);
            this.TransportStatus = trans;

        } else if(key.equalsIgnoreCase("Status")) {

            StoreDeliveryEnums.DeliveryStatus delStat = StoreDeliveryEnums.DeliveryStatus.setIntValue((int) value);
            this.Status = delStat;

        } else if (key.equalsIgnoreCase("TrailerVehicleStatus")) {

            DeliveryStatusVehicleDTO delStatus = new DeliveryStatusVehicleDTO();
            JSONObject jsonObject = (JSONObject) value;
            delStatus.readFromJSONObject(jsonObject);
            this.TrailerVehicleStatus = delStatus;

        } else if (key.equalsIgnoreCase("TractorVehicleStatus")) {

            DeliveryStatusVehicleDTO delStatus = new DeliveryStatusVehicleDTO();
            JSONObject jsonObject = (JSONObject) value;
            delStatus.readFromJSONObject(jsonObject);
            this.TractorVehicleStatus = delStatus;

        } else if (key.equalsIgnoreCase("Driver")) {

            DeliveryStatusDriverDTO delStatusDriver = new DeliveryStatusDriverDTO();
            JSONObject jsonObject = (JSONObject) value;
            delStatusDriver.readFromJSONObject(jsonObject);
            this.Driver = delStatusDriver;

         } else if (key.equalsIgnoreCase("CoDriver")) {

            DeliveryStatusDriverDTO delStatusDriver = new DeliveryStatusDriverDTO();
            JSONObject jsonObject = (JSONObject) value;
            delStatusDriver.readFromJSONObject(jsonObject);
            this.CoDriver = delStatusDriver;

        } else if (key.equalsIgnoreCase("Stops")) {
            if (this.Stops == null) {
                this.Stops = new ArrayList<DeliveryStatusStopDTO>();
            }

            if (!value.equals(null)) {
                JSONArray arrStops  = (JSONArray) value;

                if (arrStops != null) {
                    for (int i = 0; i < arrStops.length(); i++) {
                        try {
                            DeliveryStatusStopDTO deliveryStatusStopDTO = new DeliveryStatusStopDTO();
                            deliveryStatusStopDTO.readFromJSONObject(arrStops.getJSONObject(i));

                            this.Stops.add(deliveryStatusStopDTO);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }

                Log.v("Data","Value: "+this.Stops);
            }
        } else if (key.equalsIgnoreCase("Timestamp")){
            this.Timestamp = JSONDate.convertJSONDateToNSDate(String.valueOf(value));

        } else if (key.equalsIgnoreCase("DispatchTime")){
            this.DispatchTime = JSONDate.convertJSONDateToNSDate(String.valueOf(value));

        } else {
            super.setValueForKey(value, key);
        }
    }




}
