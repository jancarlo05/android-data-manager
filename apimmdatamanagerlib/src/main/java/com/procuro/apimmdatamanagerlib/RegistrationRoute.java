package com.procuro.apimmdatamanagerlib;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by jophenmarieweeks on 01/08/2017.
 */

public class RegistrationRoute extends PimmBaseObject {

    public String registrationRouteId;
    public String recorderId;
    public RegistrationDetails.AssetType assetType;
    public String supplierId;
    public String plantId;
    public String customerId;
    public String productId;
    public String codeDate;
    public String sealNumber;
    public Date loadTime;
    public boolean  cpu;
    public boolean  backhaul;
    public boolean  supplierFleet;
    public String brokerId;
    public String carrierName;
    public String trailerName;

    public String driverUserId;
    public String driverName;
    public String driverMobileNumber;

    public ArrayList<RegistrationStop> stops;
// @property (nonatomic, strong) NSMutableArray<RegistrationStop *> *stops;


}
