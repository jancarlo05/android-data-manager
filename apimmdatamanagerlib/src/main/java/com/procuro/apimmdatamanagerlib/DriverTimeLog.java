package com.procuro.apimmdatamanagerlib;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Objects;

/**
 * Created by jophenmarieweeks on 11/07/2017.
 */

public class DriverTimeLog extends PimmBaseObject {

    public enum OnDutyTypeEnum
    {
        OnDutyType_Undefined(-1),
        OnDutyType_Route(0),
        OnDutyType_Shuttle(1),
        OnDutyType_Linhaul(2),
        OnDutyType_Adhoc(3),
        OnDutyType_Service(4),
        OnDutyType_Training(5);

        private int value;
        private OnDutyTypeEnum(int value)
        {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }

        public static OnDutyTypeEnum setIntValue (int i) {
            for (OnDutyTypeEnum type : OnDutyTypeEnum.values()) {
                if (type.value == i) { return type; }
            }
            return OnDutyType_Undefined;
        }


    }

    public double Version;
    public String Address;
    public double BeginningTractorFuel;
    public double BeginningTractorOdometer;
    public double BeginningTrailerFuel;
    public double BeginningTrailerOdometer;
    public double BeginningTrailerHUBometer;
    public double EndTractorFuel;
    public double EndTractorOdometer;
    public double EndTrailerFuel;
    public double EndTrailerOdometer;
    public double EndTrailerHUBometer;
    public String CoDriver;
    public String CoDriverID;
    public String Comments;
    public String DC;
    public Date DateCreated;
    public Date DateUpdated;
    public String DeliveryID;
    public String Driver;
    public String DriverID;

    public String FormID;
    public String GuardSignatureID;
    public String RouteName;
    public String SignatureID;
    public String TerminalKey;
    public DriverLogTimeInfoDetails TimeInfoDetails;
    public DriverLogTimeInfoDetails CoDriverTimeInfoDetails;
    public String Tractor;
    public String Trailer;

    public OnDutyTypeEnum onDutyTypeSelected;
    public boolean isMainDriverDriving;



    @Override
    public void setValueForKey(Object value, String key) {

        if (key.equalsIgnoreCase("TimeInfoDetails")) {
            if (!value.equals(null)) {
                DriverLogTimeInfoDetails driverLogTimeInfoDetails = new DriverLogTimeInfoDetails();
                JSONObject jsonObject = (JSONObject) value;
                driverLogTimeInfoDetails.readFromJSONObject(jsonObject);

                this.TimeInfoDetails = driverLogTimeInfoDetails;
            }
        }else if (key.equalsIgnoreCase("CoDriverTimeInfoDetails")) {
            if (!value.equals(null)) {
                DriverLogTimeInfoDetails driverLogTimeInfoDetails = new DriverLogTimeInfoDetails();
                JSONObject jsonObject = (JSONObject) value;
                driverLogTimeInfoDetails.readFromJSONObject(jsonObject);

                this.CoDriverTimeInfoDetails = driverLogTimeInfoDetails;
            }

        }else if (key.equalsIgnoreCase("onDutyTypeSelected")) {

                if (!value.equals(null)) {
                    OnDutyTypeEnum onDutyTypeEnum = OnDutyTypeEnum.setIntValue((int) value);
                    this.onDutyTypeSelected = onDutyTypeEnum;
                }

        }else {
            super.setValueForKey(value, key);
        }
    }
    public double driverLogVersion = 1.1;

    public  void init(){
        this.TimeInfoDetails = new DriverLogTimeInfoDetails();
        this.CoDriverTimeInfoDetails = new DriverLogTimeInfoDetails();
        this.onDutyTypeSelected = OnDutyTypeEnum.OnDutyType_Route;
        this.isMainDriverDriving = true;
    }

    public HashMap<String,Object> dictionaryWithValuesForKeys() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();

        dictionary.put("Version", this.Version);
        dictionary.put("Address", this.Address);
        dictionary.put("BeginningTractorFuel", this.BeginningTractorFuel);
        dictionary.put("BeginningTractorOdometer", this.BeginningTractorOdometer);
        dictionary.put("BeginningTrailerFuel", this.BeginningTrailerFuel);
        dictionary.put("BeginningTrailerOdometer", this.BeginningTrailerOdometer);
        dictionary.put("BeginningTrailerHUBometer", this.BeginningTrailerHUBometer);
        dictionary.put("EndTractorFuel", this.EndTractorFuel);
        dictionary.put("EndTractorOdometer", this.EndTractorOdometer);
        dictionary.put("EndTrailerFuel", this.EndTrailerFuel);
        dictionary.put("EndTrailerOdometer", this.EndTrailerOdometer);
        dictionary.put("EndTrailerHUBometer", this.EndTrailerHUBometer);
        dictionary.put("CoDriver", this.CoDriver);
        dictionary.put("CoDriverID", this.CoDriverID);
        dictionary.put("Comments", this.Comments);
        dictionary.put("DC", this.DC);
        dictionary.put("DeliveryID", this.DeliveryID);
        dictionary.put("Driver", this.Driver);
        dictionary.put("DriverID", this.DriverID);
        dictionary.put("FormID", this.FormID);
        dictionary.put("GuardSignatureID", this.GuardSignatureID);
        dictionary.put("RouteName", this.RouteName);
        dictionary.put("SignatureID", this.SignatureID);
        dictionary.put("TerminalKey", this.TerminalKey);
        dictionary.put("Tractor", this.Tractor);
        dictionary.put("Trailer", this.Trailer);
        dictionary.put("onDutyTypeSelected", this.onDutyTypeSelected);
        dictionary.put("isMainDriverDriving", this.isMainDriverDriving);
        dictionary.put("TimeInfoDetails", this.TimeInfoDetails.dictionaryWithValuesForKeys());
        dictionary.put("CoDriverTimeInfoDetails", this.CoDriverTimeInfoDetails.dictionaryWithValuesForKeys());
        dictionary.put("DateCreated", this.DateCreated);
        dictionary.put("DateUpdated", this.DateUpdated);


        return dictionary;
    }

    public double getTotalBreakTimeForDriver (){
        return this.TimeInfoDetails.getTotalBreakTime();
    }
    public double getTotalSleeperTimeForDriver (){
        return this.TimeInfoDetails.getTotalSleeperTime();
    }
    public double getTotalDrivingTimeForDriver (){
        return this.TimeInfoDetails.getTotalDrivingTime();
    }
    public double getTotalBreakTimeForCoDriver (){
        return this.CoDriverTimeInfoDetails != null ? this.CoDriverTimeInfoDetails.getTotalBreakTime() : -1;
    }
    public double getTotalSleeperTimeForCoDriver (){
        return this.CoDriverTimeInfoDetails != null ? this.CoDriverTimeInfoDetails.getTotalSleeperTime() : -1;
    }
    public double getTotalDrivingTimeForCoDriver (){
        return this.CoDriverTimeInfoDetails != null ? this.CoDriverTimeInfoDetails.getTotalDrivingTime() : -1;
    }
    public Date getLastOpenBreakStartTimeForDriver(){
        return this.TimeInfoDetails.getLastOpenBreakStartTime();
    }
    public Date getLastOpenBreakStartTimeForCoDriver(){
        return this.CoDriverTimeInfoDetails.getLastOpenBreakStartTime();
    }
    public DriverLogRestBreakInfoEntry getLastOpenRestBreakEntryForDriver(){
        return this.TimeInfoDetails.getLastOpenRestBreakEntry();
    }
    public DriverLogRestBreakInfoEntry getLastOpenRestBreakEntryForCoDriver(){
        return this.CoDriverTimeInfoDetails.getLastOpenRestBreakEntry();
    }


    public HashMap<String,Object> dictionaryWithValuesForKeysForPrint() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();

        dictionary.put("Version", this.Version);
        dictionary.put("Address", this.Address);
        dictionary.put("BeginningTractorFuel", this.BeginningTractorFuel);
        dictionary.put("BeginningTractorOdometer", this.BeginningTractorOdometer);
        dictionary.put("BeginningTrailerFuel", this.BeginningTrailerFuel);
        dictionary.put("BeginningTrailerOdometer", this.BeginningTrailerOdometer);
        dictionary.put("BeginningTrailerHUBometer", this.BeginningTrailerHUBometer);
        dictionary.put("EndTractorFuel", this.EndTractorFuel);
        dictionary.put("EndTractorOdometer", this.EndTractorOdometer);
        dictionary.put("EndTrailerFuel", this.EndTrailerFuel);
        dictionary.put("EndTrailerOdometer", this.EndTrailerOdometer);
        dictionary.put("EndTrailerHUBometer", this.EndTrailerHUBometer);
        dictionary.put("CoDriver", this.CoDriver);
        dictionary.put("CoDriverID", this.CoDriverID);
        dictionary.put("Comments", this.Comments);
        dictionary.put("DC", this.DC);
        dictionary.put("DeliveryID", this.DeliveryID);
        dictionary.put("Driver", this.Driver);
        dictionary.put("DriverID", this.DriverID);
        dictionary.put("FormID", this.FormID);
        dictionary.put("GuardSignatureID", this.GuardSignatureID);
        dictionary.put("RouteName", this.RouteName);
        dictionary.put("SignatureID", this.SignatureID);
        dictionary.put("TerminalKey", this.TerminalKey);
        dictionary.put("Tractor", this.Tractor);
        dictionary.put("Trailer", this.Trailer);
        dictionary.put("onDutyTypeSelected", this.onDutyTypeSelected);
        dictionary.put("isMainDriverDriving", this.isMainDriverDriving);
        dictionary.put("TimeInfoDetails", this.TimeInfoDetails.dictionaryWithValuesForKeys());
        dictionary.put("CoDriverTimeInfoDetails", this.CoDriverTimeInfoDetails.dictionaryWithValuesForKeys());
        dictionary.put("DateCreated", this.DateCreated);
        dictionary.put("DateUpdated", this.DateUpdated);


        return dictionary;
    }

    public HashMap<String, Object> getEventsFromTimeInfoDetails(DriverLogTimeInfoDetails timeInfoDetails){
        HashMap<String, Object> dictionary = new HashMap<String, Object>();

        Date punchin = timeInfoDetails.Punch.Start;
        Date punchout = timeInfoDetails.Punch.End;
        Date pretripStart = timeInfoDetails.PreTrip.Start;
        Date pretripEnd = timeInfoDetails.PreTrip.End;
        Date posttripStart = timeInfoDetails.PostTrip.Start;
        Date posttripEnd = timeInfoDetails.PostTrip.End;

        dictionary.put("Punch-in", punchin);
        dictionary.put("PreTripStart", pretripStart);
        dictionary.put("PreTripEnd", pretripEnd);

        if(timeInfoDetails.DriveSegments != null && timeInfoDetails.DriveSegments.size() > 0){
            ArrayList<HashMap<String, Object>> driveSegmentsArray = new ArrayList<>();

            for(DriverLogTimeInfoEntry entry : timeInfoDetails.DriveSegments) {
                HashMap<String, Object> entryDic = new HashMap<>();

                    entryDic.put("Start", (entry.Start == null )? null : entry.Start);
                    entryDic.put("End", (entry.End == null )? null : entry.End);

                    if(entry.StartDetail != null && entry.StartDetail.Label != null){
                        entryDic.put("StartLabel", entry.StartDetail.Label);
                    }
                    if(entry.EndDetail != null && entry.EndDetail.Label != null){
                        entryDic.put("EndLabel", entry.EndDetail.Label);
                    }

                driveSegmentsArray.add(entryDic);
            }


            dictionary.put("DriveSegments", driveSegmentsArray);

        }else{
            dictionary.put("DriveSegments", null);
        }

        if(timeInfoDetails.Sleeper != null && timeInfoDetails.Sleeper.size() > 0) {

            ArrayList<HashMap<String, Object>> SleeperArray = new ArrayList<>();
            for(DriverLogTimeInfoEntry entry : timeInfoDetails.Sleeper) {
                HashMap<String, Object> entryDic = new HashMap<>();

                entryDic.put("Start", (entry.Start == null )? null : entry.Start);
                entryDic.put("End", (entry.End == null )? null : entry.End);

                SleeperArray.add(entryDic);
            }


            dictionary.put("Sleeper", SleeperArray);

        }else{
            dictionary.put("Sleeper", null);

        }

        if(timeInfoDetails.RestStops != null && timeInfoDetails.RestStops.size() > 0) {

            ArrayList<HashMap<String, Object>> restStopsArray = new ArrayList<>();
            for(DriverLogRestBreakInfoEntry entry : timeInfoDetails.RestStops) {
                HashMap<String, Object> entryDic = new HashMap<>();

                entryDic.put("Start", (entry.Start == null )? null : entry.Start);
                entryDic.put("End", (entry.End == null )? null : entry.End);

                restStopsArray.add(entryDic);
            }


            dictionary.put("RestStops", restStopsArray);

        }else{
            dictionary.put("RestStops", null);

        }

        dictionary.put("PostTripStart", posttripStart);
        dictionary.put("PostTripEnd", posttripEnd);
        dictionary.put("Punch-out", punchout);

        switch (timeInfoDetails.LastStatus){

            case DriverLastStatusEnum_Undefined:
                dictionary.put("LastStatus","Undefined");
                break;
            case DriverLastStatusEnum_GoHome:
                dictionary.put("LastStatus","Punchout");
                break;
            case DriverLastStatusEnum_GoOnBreak:
                dictionary.put("LastStatus","OnBreak");
                break;
            case DriverLastStatusEnum_StayOnDuty:
                dictionary.put("LastStatus","StayOnDuty");
                break;
            case DriverLastStatusEnum_Sleeper:
                dictionary.put("LastStatus","Sleeper");
                break;
            default:
                dictionary.put("LastStatus","Undefined");
                break;
        }

            return dictionary;
    }

}
