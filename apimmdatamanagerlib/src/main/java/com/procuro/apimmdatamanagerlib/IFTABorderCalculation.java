package com.procuro.apimmdatamanagerlib;

import java.util.Date;

/**
 * Created by jophenmarieweeks on 11/07/2017.
 */

public class IFTABorderCalculation extends PimmBaseObject {

    public String State;
    public String Country;
//(won't be used for display, included anyway)
    public Date Enter;
    public Date  Exit;
    public double IFTATaxableMiles;
    public double  TollMiles;
    public double  OffRoadMiles;
    public double  GallonsUsed;
    public double  MPG;
    public double  GallonsPurchased;
    public double  FuelCost;
    public String Edited;
    public double TotalMiles;


}
