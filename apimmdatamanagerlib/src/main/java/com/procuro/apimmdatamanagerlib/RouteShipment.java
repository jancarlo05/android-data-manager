package com.procuro.apimmdatamanagerlib;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by jophenmarieweeks on 07/07/2017.
 */

public class RouteShipment extends PimmBaseObject {

     public String  shipmentID;

    public RouteShipmentDriverDTO2 driver;
    public RouteShipmentDriverDTO2 coDriver;
     public String driverID;
     public String coDriverID;

     public String tractor;
     public String tractorID;
     public String trailer;
     public String trailerID;

     public Date loadTime;            // scheduled load time
     public Date dispatchTime;        // scheduled departure time
     public Date returnTime;          // scheduled return time
     public Date departureTime;
     public Date arrivalTime;

     public String loadTimeText;           // scheduled load time
     public String dispatchTimeText;       // scheduled departure time
     public String returnTimeText;         // scheduled return time
     public String departureTimeText;
     public String arrivalTimeText;

     public double truckOdometer;
     public double truckFuelGallons;
     public double truckFuelPercent;
     public double trailerOdometer;
     public double trailerFuelGallons;
     public double trailerFuelPercent;


     public Date overrideDepartureTime;
     public Date overrideArrivalTime;


     public String routeID;                // GUID for route
     public String routeName;              // name of route
     public double dayOfWeek;                   // day of week, 0 = Sun

     public double totalWeight;                 // sum of freezer/cooler/dry weight
     public double totalCases;                  // sum of freezer/cooler/dry cases
     public double totalCubes;                  // sum of freezer/cooler/dry cubes
     public double totalCount;
     public double coolerCases;
     public double coolerCubes;
     public double coolerCount;

     public double dryCases;
     public double dryCubes;
     public double dryCount;

     public double freezerCases;
     public double freezerCubes;
     public double freezerCount;

     public ArrayList<Site> regularStops; // permanently assigned stops
     public ArrayList<Site> additionalStops; // stops added just for this shipment
     public ArrayList<Site> shipmentStops;


     public String coolerLoader;

     public Date coolerLoadStart;
     public Date coolerLoadEnd;

     public String freezerLoader;

     public Date freezerLoadStart;
     public Date freezerLoadEnd;


    public boolean hasReturn;
     public double status;

/*

status
 0 = pending
 6 = pending
 7 = pending, red flag
 8 = in-progress
 9 = in-progress, red flag
 10 = in-progress
 11 = in-progress, red flag
 12 = in-progress
 13 = in-progress, red flag
 14 = complete
 15 = complete, red flag
 16 = terminated
 18 = terminated
 */

     public String  cellPhone;
     public String   jack;
     public String  epass;
     public String helper;

     public Date routeDay;

// public MutableArray *drivers;
// public String *helperID;



//Currently Supported Values for this field are:
//StoreDelivery = 0,
//Shuttle = 1,
//AdHoc = 10

     public int routeType;

//-(id) init;

    @Override
    public void setValueForKey(Object value, String key) {
        if (key.equalsIgnoreCase("regularStops")) {

            if (this.regularStops == null) {
                this.regularStops = new ArrayList<Site>();
            }

            if (!value.equals(null)) {
                JSONArray arrData = (JSONArray) value;

                if (arrData != null) {
                    for (int i = 0; i < arrData.length(); i++) {
                        try {
                            Site site = new Site();
                            site.readFromJSONObject(arrData.getJSONObject(i));

                            this.regularStops.add(site);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        } else if (key.equalsIgnoreCase("additionalStops")) {

            if (this.additionalStops == null) {
                this.additionalStops = new ArrayList<Site>();
            }

            if (!value.equals(null)) {
                JSONArray arrData = (JSONArray) value;

                if (arrData != null) {
                    for (int i = 0; i < arrData.length(); i++) {
                        try {
                            Site site = new Site();
                            site.readFromJSONObject(arrData.getJSONObject(i));

                            this.additionalStops.add(site);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        } else if (key.equalsIgnoreCase("shipmentStops")) {
            if (this.shipmentStops == null) {
                this.shipmentStops = new ArrayList<Site>();
            }

            if (!value.equals(null)) {
                JSONArray arrData = (JSONArray) value;

                if (arrData != null) {
                    for (int i = 0; i < arrData.length(); i++) {
                        try {
                            Site site = new Site();
                            site.readFromJSONObject(arrData.getJSONObject(i));

                            this.shipmentStops.add(site);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } else if (key.equalsIgnoreCase("driver")) {
                RouteShipmentDriverDTO2 routeShipmentDriverDTO2 = new RouteShipmentDriverDTO2();
                JSONObject jsonObject = (JSONObject) value;
                routeShipmentDriverDTO2.readFromJSONObject(jsonObject);
                this.driver = routeShipmentDriverDTO2;

            } else if (key.equalsIgnoreCase("coDriver")) {
                RouteShipmentDriverDTO2 routeShipmentDriverDTO2 = new RouteShipmentDriverDTO2();
                JSONObject jsonObject = (JSONObject) value;
                routeShipmentDriverDTO2.readFromJSONObject(jsonObject);
                this.coDriver = routeShipmentDriverDTO2;

            } else {
                super.setValueForKey(value, key);
            }
        }
    }


    public HashMap<String,Object> dictionaryWithValuesForKeys() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();


        dictionary.put("shipmentID ", this.shipmentID);
        dictionary.put("driverID ", this.driverID);
        dictionary.put("coDriverID ", this.coDriverID);
        dictionary.put("tractor ", this.tractor);
        dictionary.put("tractorID ", this.tractorID);
        dictionary.put("trailer ", this.trailer);
        dictionary.put("trailerID ", this.trailerID);
        dictionary.put("loadTimeText ", this.loadTimeText);
        dictionary.put("dispatchTimeText ", this.dispatchTimeText);
        dictionary.put("returnTimeText ", this.returnTimeText);
        dictionary.put("departureTimeText ", this.departureTimeText);
        dictionary.put("arrivalTimeText ", this.arrivalTimeText);
        dictionary.put("truckOdometer ", this.truckOdometer);
        dictionary.put("truckFuelGallons ", this.truckFuelGallons);
        dictionary.put("truckFuelPercent ", this.truckFuelPercent);
        dictionary.put("trailerOdometer ", this.trailerOdometer);
        dictionary.put("trailerFuelGallons ", this.trailerFuelGallons);
        dictionary.put("trailerFuelPercent ", this.trailerFuelPercent);

        dictionary.put("routeID ", this.routeID);
        dictionary.put("routeName ", this.routeName);
        dictionary.put("dayOfWeek ", this.shipmentID);
        dictionary.put("totalWeight ", this.dayOfWeek);
        dictionary.put("totalCases ", this.totalCases);
        dictionary.put("totalCubes ", this.totalCubes);
        dictionary.put("totalCount ", this.totalCount);
        dictionary.put("coolerCases ", this.coolerCases);
        dictionary.put("coolerCubes ", this.coolerCubes);
        dictionary.put("coolerCount ", this.coolerCount);
        dictionary.put("dryCases ", this.dryCases);
        dictionary.put("dryCubes ", this.dryCubes);
        dictionary.put("dryCount ", this.dryCount);
        dictionary.put("freezerCases ", this.freezerCases);
        dictionary.put("freezerCubes ", this.freezerCubes);
        dictionary.put("freezerCount ", this.freezerCount);
        dictionary.put("coolerLoader ", this.coolerLoader);
        dictionary.put("freezerLoader ", this.freezerLoader);
        dictionary.put("status ", this.status);
        dictionary.put("routeType ", this.routeType);

        if (this.additionalStops != null) {

            ArrayList<HashMap<String, Object>> additionalStopsArray = new ArrayList<>();

            for(Site res : this.additionalStops) {

                HashMap<String, Object> propertyValueDict = res.dictionaryWithValuesForKeys();
                additionalStopsArray.add(propertyValueDict);

            }

            dictionary.put("additionalStops", additionalStopsArray);

        }else{
            dictionary.put("additionalStops", null);
        }

        if (this.regularStops != null) {

            ArrayList<HashMap<String, Object>> regularStopsArray = new ArrayList<>();

            for(Site res : this.regularStops) {

                HashMap<String, Object> propertyValueDict = res.dictionaryWithValuesForKeys();
                regularStopsArray.add(propertyValueDict);

            }

            dictionary.put("regularStops", regularStopsArray);

        }else{
            dictionary.put("regularStops", null);
        }

        if (this.shipmentStops != null) {

            ArrayList<HashMap<String, Object>> shipmentStopsArray = new ArrayList<>();

            for(Site res : this.shipmentStops) {

                HashMap<String, Object> propertyValueDict = res.dictionaryWithValuesForKeys();
                shipmentStopsArray.add(propertyValueDict);

            }

            dictionary.put("shipmentStops", shipmentStopsArray);

        }else{
            dictionary.put("shipmentStops", null);
        }

        if(this.loadTime != null){
            dictionary.put("loadTime" , DateTimeFormat.convertToJsonDateTime(this.loadTime));
        }else{
            dictionary.put("loadTime", null);
        }

        if(this.dispatchTime != null){
            dictionary.put("dispatchTime" , DateTimeFormat.convertToJsonDateTime(this.dispatchTime));
        }else{
            dictionary.put("loadTime", null);
        }

        if(this.returnTime != null){
            dictionary.put("returnTime" , DateTimeFormat.convertToJsonDateTime(this.returnTime));
        }else{
            dictionary.put("returnTime", null);
        }


        if(this.departureTime != null){
            dictionary.put("departureTime" , DateTimeFormat.convertToJsonDateTime(this.departureTime));
        }else{
            dictionary.put("departureTime", null);
        }

        if(this.arrivalTime != null){
            dictionary.put("arrivalTime" , DateTimeFormat.convertToJsonDateTime(this.arrivalTime));
        }else{
            dictionary.put("arrivalTime", null);
        }

        if(this.overrideArrivalTime != null){
            dictionary.put("overrideArrivalTime" , DateTimeFormat.convertToJsonDateTime(this.overrideArrivalTime));
        }else{
            dictionary.put("overrideArrivalTime", null);
        }

        if(this.overrideDepartureTime != null){
            dictionary.put("overrideDepartureTime" , DateTimeFormat.convertToJsonDateTime(this.overrideDepartureTime));
        }else{
            dictionary.put("overrideDepartureTime", null);
        }

        if(this.coolerLoadStart != null){
            dictionary.put("coolerLoadStart" , DateTimeFormat.convertToJsonDateTime(this.coolerLoadStart));
        }else{
            dictionary.put("coolerLoadStart", null);
        }

        if(this.coolerLoadEnd != null){
            dictionary.put("coolerLoadEnd" , DateTimeFormat.convertToJsonDateTime(this.coolerLoadEnd));
        }else{
            dictionary.put("coolerLoadEnd", null);
        }

        if(this.freezerLoadStart != null){
            dictionary.put("freezerLoadStart" , DateTimeFormat.convertToJsonDateTime(this.freezerLoadStart));
        }else{
            dictionary.put("freezerLoadStart", null);
        }

        if(this.freezerLoadEnd != null){
            dictionary.put("freezerLoadEnd" , DateTimeFormat.convertToJsonDateTime(this.freezerLoadEnd));
        }else{
            dictionary.put("freezerLoadEnd", null);
        }
        if(this.routeDay != null){
            dictionary.put("routeDay" , DateTimeFormat.convertToJsonDateTime(this.routeDay));
        }else{
            dictionary.put("routeDay", null);
        }

        dictionary.put("hasReturn", (this.hasReturn == false) ? 0 : 1);

        dictionary.put("driver", this.driver.dictionaryWithValuesForKeys());
        dictionary.put("coDriver", this.coDriver.dictionaryWithValuesForKeys());
        return dictionary;
    }
}
