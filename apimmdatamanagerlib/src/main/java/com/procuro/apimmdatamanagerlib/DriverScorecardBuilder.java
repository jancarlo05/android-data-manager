package com.procuro.apimmdatamanagerlib;

/**
  Created by jophenmarieweeks on 06/07/2017.
 **/

public class DriverScorecardBuilder extends PimmBaseObject {


    public String Date;
    public String Departure;
    public String Return;
    public String CarrierOrDC;
    public String RouteName;
    public String DriverName;
    public String AddressLine;
    public String TractorName;
    public String TrailerName;
    public String DriverID;
    public String Terminal;
    public double TotalDistance;
    public String TotalTime;
    public String EngHours;
    public String DriveTime;
    public double DTPerEngHr;
    public String DeliveryTime;
    public int DeliveryTimeSeconds;

    public double TotalFuel;
    public double MPG;
    public double TotalCO2;

    public double TargetMPG;
    public double MPGSavings;
    public double CostSavings;


    public double GreenBandPerDT;
    public String GreenBandTime;
    public double GreenBandDist;
    public double CruisePerDT;
    public String CruiseTime;
    public double CruiseDist;
    public double CruiseAvgSpeed;
    public int NumCruiseEvt;
    public int NumRPMVEvt;
    public double AvgRPM;
    public String TimeAboveRPMTh;
    public int DistAboveRPMTh;
    public String TimeTorqueAbove90;
    public double AvgEngTorque;
    public double SpeedingPerDT;
    public double EcoSpeedPerDT;


    public String SpeedingTime;
    public double AvgSpeed;
    public int NumSpeedingEvt;
    public int NumHBrakeEvt;
    public String TimeAboveHBrake;
    public int NumBrakeEvt;
    public double BrakeEvtPerMin;
    public double CoastPerDT;
    public String CoastTime;
    public double CoastDist;
    public int NumHAccelerationEvt;
    public String TimeAboveHAccTh;
    public String TimeAccPedalAbove90;
    public double AvgAccPedalPos;
    public double IdlePerEngHours;
    public int NumExcessIdleEvt;
    public String TimeExcessIdle;
    public double IdleFuel;
    public int PTOEvt;
    public int PTOPerDelTime;
    public String PTOTime;
    public int FuelPTO;
    public int NumStops;
    public int NumOnTimeStops;
    public int NumDelayedStops;
    public int OnTimePer;
    public int NumCasesDelivered;
    public double CasesPerHr;
    public double CasesPerMin;
    public int EcoGrade;
    public int GreenBandScore;
    public int CruiseScore;
    public int RPMGrade;
    public int RPMViolationScore;
    public int SpeedingGrade;
    public int SpeedingScore;
    public int SpeedingViolationScore;
    public int AnticipationGrade;
    public int HBrakeViolationScore;
    public int CoastScore;
    public int AccelerationGrade;
    public int HAccViolationScore;
    public int IdleGrade;
    public int IdleScore;
    public int OverallScore;
    public String OverallGrade;
    public String PerformanceGrade;
    public String OnTimeGrade;
    public String DeliveryGrade;


}
