package com.procuro.apimmdatamanagerlib;

import android.util.Log;

/**
 * Created by jophenmarieweeks on 14/09/2017.
 */

public class GS1Barcode {
    public GS1Barcode() {

    }

//    public String decode(String barcode) {
//        // do something
//        return "";
//    }
    public GS1BarcodeComponents decodeAsGS1Barcode(String barcode) {
            GS1BarcodeComponents barcodeComponents = new GS1BarcodeComponents();
            int currentIndex = 0;
            String applicationIdentifier = null;


            while (currentIndex >= 0 && currentIndex < barcode.length()) {
                currentIndex = getApplicationIdentifierStartingAtIndex(currentIndex, applicationIdentifier, barcode);

                if(currentIndex == -1){
                    barcodeComponents.valid = false;
                    break;
                }


//                if(![self isNumericApplicationIdentifier:applicationIdentifier])
//                {
//                    //Not a valid GS1-128 Identifier
//                    DDLogError(@"Unknown Application Identifier starting at index:%ld in barcode:%@", (long)currentIndex, applicationIdentifier);
//
//                [barcodeComponents setValid:false];
//
//                    break;
//                }

                currentIndex = decodeValueForApplicationIdentifier(barcode, Integer.parseInt(applicationIdentifier), currentIndex, barcodeComponents);
            }


            return barcodeComponents;
        }



//        public boolean isNumericApplicationIdentifier(String str) {
//    NSCharacterSet* notDigits = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
//    if ([str rangeOfCharacterFromSet:notDigits].location == NSNotFound)
//    {
//        // newString consists only of the digits 0 through 9
//        return true;
//    }
//    else
//            return false;
//
//        }

    public int getApplicationIdentifierStartingAtIndex(int index, String applicationIdentifier, String barcode) {

            String AI = "";
            String str = barcode.substring(index, 1);
            int retVal = -1;
        if( (str.equalsIgnoreCase("0")) || (str.equalsIgnoreCase("1")) ){
                //2 digit application identifier
            AI = barcode.substring(index, 2);
            applicationIdentifier = AI;
            Log.v("DM","Application Identifier: " + AI);

            retVal = index + 2;

        }else if(str.equalsIgnoreCase("3")) {

            AI = barcode.substring(index, 4);
            applicationIdentifier = AI;
            Log.v("DM","Application Identifier: " + AI);
            retVal = index + 2;
        }else{
            Log.e("DM","Unknown Application Identifier starting at index: "+(long) index+" in barcode: " + barcode);
            retVal = -1;
        }
            return retVal;
    }

    public int decodeValueForApplicationIdentifier(String barcode, int applicationIdentifier, int index, GS1BarcodeComponents barcodeComponents) {

        try {

            switch (applicationIdentifier) {

                case 1:
                {
                    //its a GS1-128 Barcode encoding a GTIN-14
                    //Get 14 digits after "01"

                    String encodedGTIN = barcode.substring(index, 14);
                    Log.v("DM","GTIN-14 Identifier: " +encodedGTIN+ " from  GS1-128 Barcode:%@" + barcode);

                    barcodeComponents.GTIN14 = encodedGTIN;
                    barcodeComponents.valid = true;
                    return index + 14;

                }
                case 10:
                {
                    //Batch or lot number

                    //We are assuming that it should be the last application identifier because
                    //it is a variable length field

                    int len = barcode.length() - index;
                    if (len >20)
                    {
                        Log.e("DM","Batch or lot number is not the last component in barcode, cannot determine its length");
                        return -1;
                    }

                    String lotNumber = barcode.substring(index, len);
                    Log.v("DM","Batch or Lot number: " + lotNumber);
                    barcodeComponents.lotNumber = lotNumber;
                    return -1;
                }
                case 11:
                {

                    //Production Date (YYMMDD)
                    String dateStr = barcode.substring(index, 6);
                    Log.v("DM","Production Date: " + dateStr);
                    barcodeComponents.productionDateStr = dateStr;
                    return index+6;


                }

                case 12:
                {
                    //Due Date its not used

                    return index+6;

                }
                case 13:
                {
                    //Packaging Date (YYMMDD)
                    String dateStr = barcode.substring(index, 6);
                    Log.v("Dm","Packaging Date: " + dateStr);
                    barcodeComponents.packagingDateStr = dateStr;
                    return index+6;

                }
                case 15:
                {
                    //Best Before Date (YYMMDD)
                    String dateStr = barcode.substring(index, 6);
                    Log.v("DM","Best Before Date: "+ dateStr);
                    barcodeComponents.bestBeforeDateStr = dateStr;
                    return index+6;

                }
                case 16:
                {
                    //Sell By Date (YYMMDD)
                    String dateStr = barcode.substring(index, 6);
                    Log.v("DM","Sell By Date: " + dateStr);
                    barcodeComponents.sellByDateStr = dateStr;
                    return index+6;
                }
                case 17:
                {
                    //Expiration Date(YYMMDD)
                    String dateStr = barcode.substring(index, 6);
                    Log.v("Dm","Expiration Date: " + dateStr);
                    barcodeComponents.expirationDateStr = dateStr;
                    return index+6;
                }
                case 3202:
                {
                    //Catch Weight in pounds

                    String str1 = barcode.substring(index, 4);

                    index = index+4;
                    String str2 = barcode.substring(index, 2);
                    String weightStr = String.format("%@.%@", str1, str2);
                    double catchWeight = Double.parseDouble(weightStr);

                    Log.v("DM","Catch Weight value:%f" + catchWeight);
                    barcodeComponents.catchWeight = catchWeight;
                    return index+2;

                }

                default:
                {
                    Log.e("DM","Unknown Application Identifier: " + (long)applicationIdentifier);
                    return -1;
                }

            }
        }catch(Exception e){
            Log.e("DM","Caught Exception while trying to decode Barcode: " + e.getMessage());
            return -1;
        }


    }



}
