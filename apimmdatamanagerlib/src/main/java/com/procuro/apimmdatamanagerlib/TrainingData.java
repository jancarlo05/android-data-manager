package com.procuro.apimmdatamanagerlib;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;

public class TrainingData extends  PimmBaseObject{

    public String version;
    public ArrayList<TrainingDataItems>trainingDataItems;
    public ArrayList<String>categories;



    @Override
    public void setValueForKey(Object value, String key) {
        super.setValueForKey(value, key);
        if (key.equalsIgnoreCase("trainingDataItems")){
            ArrayList<TrainingDataItems>items = new ArrayList<>();

            if (value!=null){
                if (!value.toString().equalsIgnoreCase("null")){
                    JSONArray jsonArray = (JSONArray) value;
                    for (int i = 0; i <jsonArray.length() ; i++) {
                        TrainingDataItems item = new TrainingDataItems();
                        try {
                            item.readFromJSONObject(jsonArray.getJSONObject(i));
                            items.add(item);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            this.trainingDataItems = items;
        }

        if (key.equalsIgnoreCase("categories")){
            ArrayList<String>items = new ArrayList<>();

            if (value!=null){
                if (!value.toString().equalsIgnoreCase("null")){
                    JSONArray jsonArray = (JSONArray) value;
                    for (int i = 0; i <jsonArray.length() ; i++) {
                        try {
                            items.add(jsonArray.getString(i));

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            this.categories = items;
        }
    }

    public HashMap<String,Object> dictionaryWithValuesForKeys() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();

        dictionary.put("version", this.version);
        dictionary.put("trainingDataItems", this.trainingDataItems);
        dictionary.put("categories", this.categories);

        return dictionary;
    }

}
