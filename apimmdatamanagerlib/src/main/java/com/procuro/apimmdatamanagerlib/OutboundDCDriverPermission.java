package com.procuro.apimmdatamanagerlib;

/**
 * Created by jophenmarieweeks on 11/08/2017.
 */

public class OutboundDCDriverPermission extends PimmBaseObject {




    public enum OnDutySchedule{
        OnDutySchedule_Undefined(0),
        OnDutySchedule_7Day(1),
        OnDutySchedule_8Day(2);

        private int value;
        private OnDutySchedule(int value)
        {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }

        public static OnDutySchedule setIntValue (int i) {
            for (OnDutySchedule type : OnDutySchedule.values()) {
                if (type.value == i) { return type; }
            }
            return OnDutySchedule_Undefined;
        }


    }


    public enum ExemptionStatus
    {
        ExemptionStatus_Undefined(0),
        ExemptionStatus_NonExempt(1),
        ExemptionStatus_Exempt(2);

        private int value;
        private ExemptionStatus(int value)
        {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }

        public static ExemptionStatus setIntValue (int i) {
            for (ExemptionStatus type : ExemptionStatus.values()) {
                if (type.value == i) { return type; }
            }
            return ExemptionStatus_Undefined;
        }

    }


    public String userID;
    public boolean storeRoutes;
    public boolean shuttleTrailer;
    public boolean shuttleContainer;
    public boolean adHoc;
    public boolean personal;
    public boolean yardMove;
    public OnDutySchedule onDutySchedule;
    public int shiftStart;
    public  ExemptionStatus hosExemptionStatus;

    @Override
    public void setValueForKey(Object value, String key) {

        if (key.equalsIgnoreCase("onDutySchedule")) {

            if (!value.equals(null)) {
                OnDutySchedule onDutySchedule = OnDutySchedule.setIntValue((int) value);
                this.onDutySchedule = onDutySchedule;
            }
        }else if (key.equalsIgnoreCase("hosExemptionStatus")) {

            if (!value.equals(null)) {
                ExemptionStatus exemptionStatus = ExemptionStatus.setIntValue((int) value);
                this.hosExemptionStatus = exemptionStatus;
            }
        }else {
            super.setValueForKey(value, key);
        }
    }


    }
