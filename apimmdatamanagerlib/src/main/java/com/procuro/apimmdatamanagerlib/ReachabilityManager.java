package com.procuro.apimmdatamanagerlib;

/**
 * Created by jophenmarieweeks on 12/07/2017.
 */

public class ReachabilityManager extends PimmBaseObject {
    public String serverName;
    public Boolean isServerReachable;
    public Boolean isNetworkReachable;
    public Boolean isServerReachableViaWifi;
    public Boolean isServerReachableViaWWAN;


//
//+(ReachabilityManager*) sharedReachabilityManager;
//-(BOOL) initializeWithServerName:(NSString*) serverName;
//-(void) startTimerToCheckNetworkStatus;
//-(void) checkNetworkStatus;
//
//
//-(void) dealloc;

}
