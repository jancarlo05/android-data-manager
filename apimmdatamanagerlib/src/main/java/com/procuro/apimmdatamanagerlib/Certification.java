package com.procuro.apimmdatamanagerlib;

public class Certification extends PimmBaseObject {

   public String Certification;
   public String StarRating;

    @Override
    public void setValueForKey(Object value, String key) {
        super.setValueForKey(value, key);

        if (key.equalsIgnoreCase("Certification")){
            if (value!=null){
                if (!value.toString().equalsIgnoreCase("null")){
                    Certification = value.toString();
                }
            }
        }

        if (key.equalsIgnoreCase("StarRating")){
            if (value!=null){
                if (!value.toString().equalsIgnoreCase("null")){
                    StarRating = value.toString();
                }
            }
        }

    }
}
