package com.procuro.apimmdatamanagerlib;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jophenmarieweeks on 12/07/2017.
 */

public class RMSCombinedStatusLists extends PimmBaseObject {

    public ArrayList<SdVehicleStatus> Trailers; //Array of SdVehicleStatus
    public ArrayList<SdVehicleStatus> Tractors; //Array of SdVehicleStatus
    public ArrayList<SdDriverStatus> Drivers; //Array of SdDriverStatus

    @Override
    public void setValueForKey(Object value, String key) {
        if (key.equalsIgnoreCase("Trailers")) {

            if (this.Trailers == null) {
                this.Trailers = new ArrayList<SdVehicleStatus>();
            }

            if (!value.equals(null)) {
                JSONArray arrData = (JSONArray) value;

                if (arrData != null) {
                    for (int i = 0; i < arrData.length(); i++) {
                        try {
                            SdVehicleStatus result = new SdVehicleStatus();
                            result.readFromJSONObject(arrData.getJSONObject(i));

                            this.Trailers.add(result);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        } else if (key.equalsIgnoreCase("Tractors")) {

            if (this.Tractors == null) {
                this.Tractors = new ArrayList<SdVehicleStatus>();
            }

            if (!value.equals(null)) {
                JSONArray arrData = (JSONArray) value;

                if (arrData != null) {
                    for (int i = 0; i < arrData.length(); i++) {
                        try {
                            SdVehicleStatus result = new SdVehicleStatus();
                            result.readFromJSONObject(arrData.getJSONObject(i));

                            this.Tractors.add(result);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        } else if (key.equalsIgnoreCase("Drivers")) {
            if (this.Drivers == null) {
                this.Drivers = new ArrayList<SdDriverStatus>();
            }

            if (!value.equals(null)) {
                JSONArray arrData = (JSONArray) value;

                if (arrData != null) {
                    for (int i = 0; i < arrData.length(); i++) {
                        try {
                            SdDriverStatus result = new SdDriverStatus();
                            result.readFromJSONObject(arrData.getJSONObject(i));

                            this.Drivers.add(result);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

        } else {
                super.setValueForKey(value, key);
        }
    }


}
