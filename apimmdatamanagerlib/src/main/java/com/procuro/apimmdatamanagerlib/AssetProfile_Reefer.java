package com.procuro.apimmdatamanagerlib;

import java.util.Date;

/**
 * Created by jophenmarieweeks on 06/07/2017.
 */

public class AssetProfile_Reefer extends PimmBaseObject {


    public String profileType;
    public String make;
    public String model;
    public String refrigerant;
    public String compressorMake;
    public String compressorModel;
    public String serial;
    public Integer year;
    public Integer controlCircuit;
    public Date warrantyStartDate;
    public Date warrantyEndDate;
    public Date installationDate;




}
