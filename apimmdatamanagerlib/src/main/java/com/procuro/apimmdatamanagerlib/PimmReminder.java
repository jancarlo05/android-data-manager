package com.procuro.apimmdatamanagerlib;

import java.util.Date;

/**
 * Created by jophenmarieweeks on 11/07/2017.
 */

public class PimmReminder extends PimmBaseObject {

   public enum RouteTypeEnum
    {
        RouteTypeEnum_AllRoutes(0),
        RouteTypeEnum_Outbound(1),
        RouteTypeEnum_LineHaul(2),
        RouteTypeEnum_Shuttle(3);

        private int value;
        private RouteTypeEnum(int value)
        {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }

        public static RouteTypeEnum setIntValue (int i) {
            for (RouteTypeEnum type : RouteTypeEnum.values()) {
                if (type.value == i) { return type; }
            }
            return RouteTypeEnum_AllRoutes;
        }
    }

    public enum ReminderConditionEnum
    {
        ReminderConditionEnum_PreLogin(1),
        ReminderConditionEnum_PostLogin(2),
        ReminderConditionEnum_DeliveryStart(3),
        ReminderConditionEnum_DeliveryEnd(4),
        ReminderConditionEnum_EORStart(5),
        ReminderConditionEnum_Logout(6);
        private int value;
        private ReminderConditionEnum(int value)
        {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }

        public static ReminderConditionEnum setIntValue (int i) {
            for (ReminderConditionEnum type : ReminderConditionEnum.values()) {
                if (type.value == i) { return type; }
            }
            return ReminderConditionEnum_PreLogin;
        }

    }

    public String pimmReminderID;            // GUID PK
    public String userId;                    // user who created this reminder
    public String siteId;                    // all reminders are tied to a site
    public  RouteTypeEnum routeType;                        // 0 = all route types, 1 = outbound, 2 = line haul, 3 = shuttle
    public int value;                            // 0 = use custom message.  any other value means use that pre-canned message (app's responsibility)
    public  ReminderConditionEnum condition;

    //   1.   Pre Login – as soon as the app starts up, before the driver enters his user name and password.  Only supported for “all route types” because we don’t know who is going to log in for what route at this point.
    //   2.   Post Login – right after login authentication
    //   3.   Delivery Start – driver clicks on the stop in the delivery tab
    //   4.   Delivery End – store manager signs off on the delivery
    //   5.   EOR Start– user clicks “end of route”
    //   6.   Logout – user signs off from app

    public Date startDate;  // start Date
    public Date endDate ;                // if null, reminder never expires.  otherwise, the expiration date.
    public String customMessage ;             // if value=0, use this string as the reminder message


    @Override
    public void setValueForKey(Object value, String key) {

        if(key.equalsIgnoreCase("routeType")) {
            if (!value.equals(null)) {
                RouteTypeEnum routeTypeEnum = RouteTypeEnum.setIntValue((int) value);
                this.routeType = routeTypeEnum;
            }
        }else if(key.equalsIgnoreCase("condition")){
            if (!value.equals(null)) {
                ReminderConditionEnum reminderConditionEnum = ReminderConditionEnum.setIntValue((int) value);
                this.condition = reminderConditionEnum;

            }

        } else {
            super.setValueForKey(value, key);
        }
    }
}
