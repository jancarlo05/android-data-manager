package com.procuro.apimmdatamanagerlib;

import org.json.JSONObject;

import java.util.Date;

/**
 * Created by jophenmarieweeks on 06/07/2017.
 */

public class DriverLeaderboard extends PimmBaseObject {

    public Date Timestamp;
    public int TimezoneId;
    public DriverLeaderboardBuilder LeaderboardData;

    @Override
    public void setValueForKey(Object value, String key) {

        if(key.equalsIgnoreCase("LeaderboardData")) {
            if (!value.equals(null)) {

                DriverLeaderboardBuilder driverLeaderboardBuilder = new DriverLeaderboardBuilder();
                JSONObject jsonObject = (JSONObject) value;
                driverLeaderboardBuilder.readFromJSONObject(jsonObject);
                this.LeaderboardData = driverLeaderboardBuilder;
            }
        } else {
            super.setValueForKey(value, key);
        }
    }

}
