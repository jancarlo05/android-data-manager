package com.procuro.apimmdatamanagerlib;

import java.util.Date;

public class HolidaySchedules extends PimmBaseObject {

    public String holiday;
    public Date closeTime;
    public Date openTime;
    public Date timestamp;
    public Date date;
    public boolean isAllDay;


    @Override
    public void setValueForKey(Object value, String key) {
        super.setValueForKey(value, key);

        if (key.equalsIgnoreCase("closeTime")){
            if (value!=null){
                if (!value.toString().equalsIgnoreCase("null")){
                    this.closeTime = JSONDate.convertJSONDateToNSDate(value.toString());
                }
            }
        }

        if (key.equalsIgnoreCase("openTime")){
            if (value!=null){
                if (!value.toString().equalsIgnoreCase("null")){
                    this.openTime = JSONDate.convertJSONDateToNSDate(value.toString());
                }
            }
        }

        if (key.equalsIgnoreCase("timestamp")){
            if (value!=null){
                if (!value.toString().equalsIgnoreCase("null")){
                    this.timestamp = JSONDate.convertJSONDateToNSDate(value.toString());
                }
            }
        }

        if (key.equalsIgnoreCase("date")){
            if (value!=null){
                if (!value.toString().equalsIgnoreCase("null")){
                    this.date = JSONDate.convertJSONDateToNSDate(value.toString());
                }
            }
        }
    }
}
