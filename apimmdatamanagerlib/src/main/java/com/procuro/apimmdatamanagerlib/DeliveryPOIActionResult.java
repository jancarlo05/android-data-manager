package com.procuro.apimmdatamanagerlib;

import org.json.JSONObject;

/**
 * Created by jophenmarieweeks on 06/07/2017.
 */

public class DeliveryPOIActionResult extends PimmBaseObject {

    public String DeliveryId;
    // The ID of the point of interest that was deleted (i.e. converted to a
    //                                                  permanent stop).
    public String DeletedPOIId;

    public boolean Rebuild;
    public boolean Changed;



    // POI is deleted because converted to permanent stop. The POI/unscheduled
    //object should be removed and replaced by stop provided in the
    //PermanentStop field below.
    public boolean POIDeleted;



    // Updated state for the SdrDelivery.GIS object that reflect changes applied
    //    to the POI affected in this call.
    public int GIS_UnscheduledCount;
    public int GIS_UnauthorizedCount;
    public int GIS_UnscheduledTotal;

    // If the action converts the POI to a permanent stop, then a stop action
    //result is bundled in the response. This gives incrementanl information
    //for the new stop.

    public DeliveryStopActionResult PermanentStop;

    //classes
    // The token to be associated with the route for use in refresh calls.
   public int RebuildToken;

    //The DTO containing the updated state for the POI.
    public SdrPointOfInterest POI;

    @Override
    public void setValueForKey(Object value, String key) {

        if (key.equalsIgnoreCase("PermanentStop")) {

            DeliveryStopActionResult deliveryStopActionResult = new DeliveryStopActionResult();
            JSONObject jsonObject = (JSONObject) value;
            deliveryStopActionResult.readFromJSONObject(jsonObject);
            this.PermanentStop = deliveryStopActionResult;
        } else if (key.equalsIgnoreCase("POI")) {

            SdrPointOfInterest sdrPointOfInterest = new SdrPointOfInterest();
            JSONObject jsonObject = (JSONObject) value;
            sdrPointOfInterest.readFromJSONObject(jsonObject);
            this.POI = sdrPointOfInterest;

        } else {
            super.setValueForKey(value, key);
        }

    }

}
