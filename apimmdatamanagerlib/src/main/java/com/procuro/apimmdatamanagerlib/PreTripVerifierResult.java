package com.procuro.apimmdatamanagerlib;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jophenmarieweeks on 11/07/2017.
 */

public class PreTripVerifierResult extends PimmBaseObject {

   public enum PreTripVerifierResultEnum
    {
        PreTripVerifierResultEnum_Undefined(0),
        PreTripVerifierResultEnum_Allow(1),
        PreTripVerifierResultEnum_DriverUnassigned(2),
        PreTripVerifierResultEnum_TractorUnassigned(3),
        PreTripVerifierResultEnum_QualificationIssue(4),
        PreTripVerifierResultEnum_HOSIssue(5),
        PreTripVerifierResultEnum_TractorAssignmentMismatch(6);

        private int value;
        private PreTripVerifierResultEnum(int value)
        {
            this.value = value;
        }
        public int getValue() {
            return this.value;
        }

        public static PreTripVerifierResultEnum setIntValue (int i) {
            for (PreTripVerifierResultEnum type : PreTripVerifierResultEnum.values()) {
                if (type.value == i) { return type; }
            }
            return PreTripVerifierResultEnum_Undefined;
        }
    }

//    public static int PRETRIPVERIFIERRESULT_UNDEFINED                   = 0;
//    public static int PRETRIPVERIFIERRESULT_ALLOW                       = 1;
//    public static int PRETRIPVERIFIERRESULT_DRIVERUNASSIGNED            = 2;
//    public static int PRETRIPVERIFIERRESULT_TRACTORUNASSIGNED           = 3;
//    public static int PRETRIPVERIFIERRESULT_QUALIFICATIONISSUE          = 4;
//    public static int PRETRIPVERIFIERRESULT_HOSISSUE                    = 5;
//    public static int PRETRIPVERIFIERRESULT_TRACTORASSIGNMENTMISMATCH   = 6;

    public boolean driverAssigned;
    public boolean tractorAssigned;
    public boolean provisioned;
    public PreTripVerifierResultEnum resultCode;
//    public int resultCode;
    public int resultCodeDetail;
    public String resultCodeDescription;
    public List<AssignmentInfo> assignments;

    public String currentDriverRouteName;
    public String currentDriverShipmentID;
    public String currentTractorRouteName;
    public String currentTractorShipmentID;
    public String tractorName;

    @Override
    public void setValueForKey(Object value, String key) {
        if (key.equalsIgnoreCase("assignments")) {
            if (this.assignments == null) {
                this.assignments = new ArrayList<AssignmentInfo>();
            }

            JSONArray arrAssigments = (JSONArray) value;

            if (arrAssigments != null) {
                for (int i = 0; i < arrAssigments.length(); i++) {
                    try {
                        AssignmentInfo assignmentInfo = new AssignmentInfo();
                        assignmentInfo.readFromJSONObject(arrAssigments.getJSONObject(i));

                        this.assignments.add(assignmentInfo);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

     } else if(key.equalsIgnoreCase("resultCode")) {
            if (!value.equals(null)) {
                PreTripVerifierResultEnum routeStatusDNDBEventTypeEnum = PreTripVerifierResultEnum.setIntValue((int) value);
                this.resultCode = routeStatusDNDBEventTypeEnum;

            }
        } else
        {
            super.setValueForKey(value, key);
        }
    }
}
