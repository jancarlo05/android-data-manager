package com.procuro.apimmdatamanagerlib;

/**
 * Created by jophenmarieweeks on 10/07/2017.
 */

public class SiteTypeEnum extends PimmBaseObject {

   // public enum SiteTypeEnum   //Duplicate class error
    public enum SiteTypesEnum
    {
        SiteType_Undefined(-1),
        SiteType_Store(0),
        SiteType_Fuel(2),
        SiteType_Plant(3),
        SiteType_Rest(5),
        SiteType_Inspection(6),
        SiteType_Service(7),                     // SiteType_Truck is changed to Service
        SiteType_Layover(8),
        SiteType_CoffeeStop(9),
        SiteType_Shuttle(20),
        SiteType_Depot(21),
        SiteType_Unauthorized(101);

        private int value;
        private SiteTypesEnum(int value)
        {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }

        public static SiteTypesEnum setIntValue (int i) {
            for (SiteTypesEnum type : SiteTypesEnum.values()) {
                if (type.value == i) { return type; }
            }
            return SiteType_Undefined;
        }
    }

    public SiteTypesEnum siteTypesEnum;

    @Override
    public void setValueForKey(Object value, String key) {

        if(key.equalsIgnoreCase("siteTypesEnum")) {
            if (!value.equals(null)) {
                SiteTypesEnum siteType = SiteTypesEnum.setIntValue((int) value);
                this.siteTypesEnum = siteType;
            }

        } else {
            super.setValueForKey(value, key);
        }
    }

}
