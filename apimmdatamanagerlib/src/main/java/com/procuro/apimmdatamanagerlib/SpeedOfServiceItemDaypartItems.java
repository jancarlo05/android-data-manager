package com.procuro.apimmdatamanagerlib;

import java.util.Date;
import java.util.HashMap;

public class SpeedOfServiceItemDaypartItems extends  PimmBaseObject {

    public String actual;
    public Date actualTimestamp;
    public int daypart;
    public Date goalTimestamp;
    public String goal;


    @Override
    public void setValueForKey(Object value, String key) {
        super.setValueForKey(value, key);

        if (key.equalsIgnoreCase("actual")){
            if (value!=null){
                if (!value.toString().equalsIgnoreCase("null")){
                    this.actual = value.toString();
                }
            }
        }
        if (key.equalsIgnoreCase("actualTimestamp")){
            if (value!=null){
                this.actualTimestamp = JSONDate.convertJSONDateToNSDate(value.toString());
            }
        }
        if (key.equalsIgnoreCase("goalTimestamp")){
            if (value!=null){
                this.goalTimestamp = JSONDate.convertJSONDateToNSDate(value.toString());
            }
        }
        if (key.equalsIgnoreCase("daypart")){
            if (value!=null){
                if (value.toString().equalsIgnoreCase("null")){
                    this.daypart = (int) value;
                }
            }
        }
        if (key.equalsIgnoreCase("goal")){
            if (value!=null){
                this.actual = (String)goal;
            }
        }
    }


    public HashMap<String,Object> dictionaryWithValuesForKeys() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();

        dictionary.put("actual", this.actual);
        dictionary.put("actualTimestamp", this.actualTimestamp);
        dictionary.put("daypart", this.daypart);
        dictionary.put("goalTimestamp", this.goalTimestamp);
        dictionary.put("goal", this.goal);



        return dictionary;
    }

}
