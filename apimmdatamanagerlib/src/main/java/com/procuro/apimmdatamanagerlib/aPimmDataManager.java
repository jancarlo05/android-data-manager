package com.procuro.apimmdatamanagerlib;

/**
 * Created by EmmanKusumi on 7/3/17.
 */

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.Uri;
import android.os.Environment;
import android.util.Base64;
import android.util.Log;

import com.procuro.apimmdatamanagerlib.DashboardDTOs.FM_FacilityDashboardDTO;
import com.procuro.apimmdatamanagerlib.DashboardDTOs.StD_CustomerDashboardDTO;
import com.procuro.apimmdatamanagerlib.DashboardDTOs.StD_DistributorDashboardDTO;
import com.procuro.apimmdatamanagerlib.DashboardDTOs.SuD_CustomerDashboard;
import com.procuro.apimmdatamanagerlib.DashboardDTOs.SuD_DistributorDashboard;
import com.procuro.apimmdatamanagerlib.DashboardDTOs.SuD_SupplierDashboard;
import com.procuro.apimmdatamanagerlib.OnCompleteListeners.OnLoginCompleteListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.URI;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.Dictionary;
import java.util.Objects;
import java.util.TimeZone;
import java.util.UUID;

import static com.procuro.apimmdatamanagerlib.MobileDeviceAlert.MobileDeviceAlertType.MobileDeviceAlert_Undefined;
import static com.procuro.apimmdatamanagerlib.PimmRefTypeEnum.PimmRefTypesEnum.PimmRefType_Undefined;

//import static com.procuro.apimmdatamanagerlib.PimmRefTypeEnum.PimmRefType_Undefined;

public class aPimmDataManager {
    public static String ApplicationID = "c89d8b58-e89b-4aa5-ab78-84524df56898";

    private static final aPimmDataManager ourInstance = new aPimmDataManager();

    private String spid;
    private String username;
    private String password;

    private String appname;
    private String version;
    private RestConnection restConnection;

    private String authHeaderValue;

    private StringBuilder baseUrl;
    public Context mContext;

    public void setContext(Context ctx) {
        this.mContext = ctx;
    }

    public static aPimmDataManager getInstance() {
        return ourInstance;
    }

    private aPimmDataManager() {

    }

    public String getAuthHeaderValue() {
        return authHeaderValue;
    }

    public StringBuilder getBaseUrl() {
        return baseUrl;
    }

    //    private  void resConnInit() {
//        restConnection = RestConnection.getInstance();
//    }

    private void setSpid(String spid) {
        this.spid = spid;
    }

    private void setUsernameAndPassword(String username, String password) {
        this.username = username;
        this.password = password;

        String authStr = username + ":" + password;
        String authValue = "Basic " + Base64Converter.toBase64(authStr);
        this.setAuthHeaderValue(authValue);
        Log.v("DM", "Auth Header: " + authStr);
    }

    private void setAuthHeaderValue(String value) {
        Log.v("DM", "Auth Header Value: " + value);
        this.authHeaderValue = value;
    }

    public void initializeWithSPID(String spid) {

        this.setSpid(spid);

        this.baseUrl = new StringBuilder();
        this.baseUrl.append("https://");
        this.baseUrl.append(spid);
        this.baseUrl.append(".pimm.us/WebPimm5/Rest/");

        Log.v("DM", "BaseURLString: " + this.baseUrl);
    }

    public boolean isValidParameterString(String string) {
        if (string == null || string.isEmpty()) {
            return false;
        } else {
            return true;
        }

    }

    public void wrtieFileOnInternalStorage(String title,String sBody){

        String root = Environment.getExternalStorageDirectory().toString();
        File file = new File(root);
        if(!file.exists()){
            file.mkdir();
        }else {
            file.delete();
        }
        try{
            File gpxfile = new File(file, title);
            FileWriter writer = new FileWriter(gpxfile);
            writer.append(sBody);
            writer.flush();
            writer.close();

            BufferedReader br = new BufferedReader(new FileReader(gpxfile));
            StringBuilder stringBuilder= new StringBuilder();
            String st;
            while ((st = br.readLine()) != null){
                stringBuilder.append(st);
            }

//            String s = stringBuilder.toString().replaceAll("[^\\x20-\\x7E]", "");
//            System.out.println(s);
//            JSONObject jsonObject = new JSONObject(s);
//            System.out.println(jsonObject);



        }catch (Exception e){
            e.printStackTrace();

        }
    }

    public static String getUTCFormatDate(Date date) {
        final Date currentTime = date;
        //final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm'Z'");
        final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));

        String dateString = sdf.format(currentTime);
        return dateString;
    }

    private byte[] convertToBytes(Object object) throws IOException {

        try (ByteArrayOutputStream bos = new ByteArrayOutputStream();
             ObjectOutputStream out = new ObjectOutputStream(bos)) {
            out.writeObject(object);
            return bos.toByteArray();
        }
    }

    public void initializeWithAppName(String appName) {
       this.appname = appName;
    }

    public void initializeWithAppName(String app, String versionNum, boolean isNetworkClient, String spid, boolean enabled) {

        initializeWithAppName(app, versionNum, isNetworkClient, spid, enabled);
    }


    public void loginWithUsernameAndPassword(String username, String password, final OnLoginCompleteListener listener) {
        Log.v("DM", "Enter Function: loginWithUsernameAndPassword");

        this.setUsernameAndPassword(username, password);

        final OnLoginCompleteListener loginCompleteListener = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Pimm/User");
        if (appname!=null){
            urlString.append("?app=");
            urlString.append(appname);
        }
        urlString.append("&deviceType=Android");


        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.setAppContext(mContext);
        restConnection.isOnline();

        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (error == null) {
                        Log.v("DM", "Received JSON Object: " + object.toString());
                        User user = new User();
                        JSONObject jsonObject = (JSONObject) object;
                        user.readFromJSONObject(jsonObject);
                        listener.onLoginComplete(user, null);

                    } else {
                        Log.e("DM", "Null object ");
                        listener.onLoginComplete(null, error);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for onLoginComplete:" + e.getMessage());
                    listener.onLoginComplete(null, error);
                }
            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.i("DM", "connection path: loginWithUsernameAndPassword " + String.valueOf(urlString));
        Log.v("DM", "Exit Function: loginWithUsernameAndPassword");
    }

    public void loginWithUsernameAndPassword(String username, String password, String serialNumber, String deviceType, String app, String versionNum, final OnLoginCompleteListener listener) {
        Log.v("DM", "Enter Function: loginWithUsernameAndPassword");

        this.setUsernameAndPassword(username, password);

        final OnLoginCompleteListener loginCompleteListener = listener;


        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Pimm/User");

        urlString.append("?serialNumber=");
        urlString.append(serialNumber);
        urlString.append("&deviceType=");
        urlString.append(deviceType);
        urlString.append("&app=");
        urlString.append(app);
        urlString.append("&version=");
        urlString.append(versionNum);

        Log.v("DM", "Request URL: " + urlString);
        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object: " + object.toString());
                        User user = new User();
                        JSONObject jsonObject = (JSONObject) object;
                        user.readFromJSONObject(jsonObject);
                        listener.onLoginComplete(user, null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.onLoginComplete(null, error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for loginWithUsernameAndPassword" + e.getMessage());

                }

            }

        });

        restConnection.execute(String.valueOf(urlString));
        Log.i("DM", "connection path: loginWithUsernameAndPassword " + String.valueOf(urlString));
        Log.v("DM", "Exit Function: loginWithUsernameAndPassword");
    }

//


    public void getUsersByRole(String role, final OnCompleteListeners.OnGetUserRoleCompleteListener listener) {

        Log.v("DM", "Enter Function: getUsersByRole");

//        final OnCompleteListeners.OnGetUserRoleCompleteListener userroleCompleteListener = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Pimm/User");
        urlString.append("?role=");
        urlString.append(role);

        Log.v("DM", "Request URL with Role: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                if (error == null) {
                    try {
                        JSONArray jsonArray = (JSONArray) object;
                        ArrayList<User> resultArrayList = new ArrayList<User>();

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);

                            User result = new User();
                            result.readFromJSONObject(jsonObject);
                            resultArrayList.add(result);
                        }

                        listener.onGetUserRoleComplete(resultArrayList, null);
                    } catch (JSONException e) {
                        listener.onGetUserRoleComplete(null, new Error(e.getMessage()));
                    }

                } else {
                    listener.onGetUserRoleComplete(null, error);
                }
            }
        });


        restConnection.execute(String.valueOf(urlString));


        Log.v("DM", "Exit Function: getUsersByRole");

    }

    public void getSiteWithSiteId(String siteId, String userId, final OnCompleteListeners.OnGetSiteWithSiteIdListener listener) {

        Log.v("DM", "Enter Function: getSiteWithSiteId");
        final OnCompleteListeners.OnGetSiteWithSiteIdListener getSiteWithSiteIdListener = listener;


        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/Site");
        urlString.append("?siteId=");
        urlString.append(siteId);
        urlString.append("&userId=");
        urlString.append(userId);

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for site: " + object.toString());
                        Site site = new Site();
                        JSONObject jsonObject = (JSONObject) object;
                        site.readFromJSONObject(jsonObject);
                        listener.onGetWithSiteId(site, null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.onGetWithSiteId(null, error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for onGetWithSiteId: " + e.getMessage());
                    listener.onGetWithSiteId(null, error);

                }
            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getSiteWithSiteId");
    }

    public void getTrailerReportForTrailerId(String trailerId, String deliveryId, final OnCompleteListeners.OnGetTrailerReportForTrailerIdListener listener) {

        Log.v("DM", "Enter Function: getTrailerReportForTrailerId");
        final OnCompleteListeners.OnGetTrailerReportForTrailerIdListener GetTrailerReportForTrailerId = listener;


        StringBuilder urlString = new StringBuilder();
//        URI urlString = new URIBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/TrailerReport");

        if (isValidParameterString(trailerId)) {
            urlString.append("?trailerId=");
            urlString.append(trailerId);
            //urlString.addParameter("trailerId",trailerId);

        } else {
            Log.e("DM", "getTrailerReportForTrailerId: Parameter trailerId is not set");
        }

        if (isValidParameterString(deliveryId)) {
            urlString.append("&deliveryId=");
            urlString.append(deliveryId);
        } else {
            Log.e("DM", "getTrailerReportForTrailerId: Parameter deliveryId is not set");
        }


        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for trailer report: " + object.toString());
                        SdrReport sdrRep = new SdrReport();
                        JSONObject jsonObject = (JSONObject) object;
                        sdrRep.readFromJSONObject(jsonObject);
                        listener.onGetTrailerReportForTrailerId(sdrRep, null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.onGetTrailerReportForTrailerId(null, error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException TrailerReport >> " + e.getMessage());
                    listener.onGetTrailerReportForTrailerId(null, error);
                }
            }
        });

        restConnection.execute(String.valueOf(urlString));

        Log.v("DM", "ExitFunction: getTrailerReportForTrailerId");
    }

    public void createRouteShipment(RouteShipment shipment, String siteId, final OnCompleteListeners.OnCreateRouteShipmentListener listener) {

        Log.v("DM", "Enter Function: createRouteShipment");
        final OnCompleteListeners.OnCreateRouteShipmentListener CreateRouteShipment = listener;


        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/RouteShipment");

//        String input = [jsonData base64EncodedString];
//        HashMap<String, Object> data = shipment.dictionaryWithValuesForKeys();
        JSONObject jsonObjFB = new JSONObject(shipment.dictionaryWithValuesForKeys());
        String jsonString = jsonObjFB.toString();
        String input = Base64Converter.toBase64(jsonString);

        StringBuilder parameterString = new StringBuilder();

        parameterString.append("?siteId=");
        parameterString.append(siteId);
        parameterString.append("&input=");
        parameterString.append(input);


        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initializePOSTRequest(this.authHeaderValue, parameterString.toString(), new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for createRouteShipment: " + object.toString());
                        RouteShipmentResult routeShipmentResult = new RouteShipmentResult();
                        JSONObject jsonObject = (JSONObject) object;
                        routeShipmentResult.readFromJSONObject(jsonObject);
                        listener.OnCreateRouteShipment(routeShipmentResult, null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.OnCreateRouteShipment(null, error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException loginerror >> " + e.getMessage());
                    listener.OnCreateRouteShipment(null, error);

                }
            }
        });

        restConnection.execute(String.valueOf(urlString));

        Log.v("DM", "ExitFunction: createRouteShipment");
    }

    public void updateRouteShipment(RouteShipment shipment, String siteId, final OnCompleteListeners.OnUpdateRouteShipmentListener listener) {

        Log.v("DM", "Enter Function: updateRouteShipment");
        final OnCompleteListeners.OnUpdateRouteShipmentListener UpdateRouteShipment = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/RouteShipment2");
        JSONObject jsonObjFB = new JSONObject(shipment.dictionaryWithValuesForKeys());
        String jsonString = jsonObjFB.toString();
        String input = Base64Converter.toBase64(jsonString);

        StringBuilder parameterString = new StringBuilder();

        if (isValidParameterString(shipment.shipmentID)) {
            parameterString.append("?shipmentId=");
            parameterString.append(shipment.shipmentID);
        } else {
            Log.e("DM", "updateRouteShipment: Parameter shipmentID is not set");
        }

        if (isValidParameterString(siteId)) {
            parameterString.append("&siteId=");
            parameterString.append(siteId);
        } else {
            Log.e("DM", "updateRouteShipment: Parameter shipmentID is not set");
        }

        if (isValidParameterString(siteId)) {
            urlString.append("&input=");
            urlString.append(siteId);
        } else {
            Log.e("DM", "updateRouteShipment: Parameter shipmentID is not set");
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initializePOSTRequest(this.authHeaderValue, parameterString.toString(), new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for updateRouteShipment: " + object.toString());
                        RouteShipmentResult routeShipmentResult = new RouteShipmentResult();
                        JSONObject jsonObject = (JSONObject) object;
                        routeShipmentResult.readFromJSONObject(jsonObject);
                        listener.onnupdateRouteShipment(routeShipmentResult, null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.onnupdateRouteShipment(null, error);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException updateRouteShipment >> " + e.getMessage());
                    listener.onnupdateRouteShipment(null, error);
                }
            }
        });

        restConnection.execute(String.valueOf(urlString));

        Log.v("DM", "ExitFunction: updateRouteShipment");
    }

    public void getRouteShipment(String shipmentId, String siteId, final OnCompleteListeners.getRouteShipmentListener listener) {

        Log.v("DM", "Enter Function: getRouteShipment");
        final OnCompleteListeners.getRouteShipmentListener GetRouteShipmentListener = listener;


        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/RouteShipment");


        //try
        urlString.append("?shipmentId=");
        urlString.append(shipmentId);
        urlString.append("&siteId=");
        urlString.append(siteId);


        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for getRouteShipment: " + object.toString());
                        RouteShipment routeShipment = new RouteShipment();
                        JSONObject jsonObject = (JSONObject) object;
                        routeShipment.readFromJSONObject(jsonObject);
                        listener.getRouteShipment(routeShipment, null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.getRouteShipment(null, error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException getRouteShipment >> " + e.getMessage());
                    listener.getRouteShipment(null, error);

                }
            }
        });

        restConnection.execute(String.valueOf(urlString));

        Log.v("DM", "ExitFunction: getRouteShipment");
    }

    public void getRouteShipmentProductListForSiteId(String siteId, boolean valid, final OnCompleteListeners.getRouteShipmentProductListForSiteIdListener listener) {

        Log.v("DM", "Enter Function: getRouteShipmentProductListForSiteId");

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/RouteShipmentProduct");

        if (isValidParameterString(siteId)) {
            urlString.append("?siteId=");
            urlString.append(siteId);

            urlString.append("&valid=");
            urlString.append((valid) ? true : false);
        } else {
            Log.e("DM", "getRouteShipmentProductListForSiteId: Parameter site id is not set");
        }
        Log.v("DM", "Request URL with Role: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                if (error == null) {
                    try {
                        JSONArray jsonArray = (JSONArray) object;
                        ArrayList<RouteShipmentProduct> resultArrayList = new ArrayList<RouteShipmentProduct>();

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);

                            RouteShipmentProduct result = new RouteShipmentProduct();
                            result.readFromJSONObject(jsonObject);
                            resultArrayList.add(result);
                        }

                        listener.getRouteShipmentProductListForSiteId(resultArrayList, null);
                    } catch (JSONException e) {
                        listener.getRouteShipmentProductListForSiteId(null, new Error(e.getMessage()));
                    }

                } else {
                    listener.getRouteShipmentProductListForSiteId(null, error);
                }
            }
        });


        restConnection.execute(String.valueOf(urlString));


        Log.v("DM", "Exit Function: getRouteShipmentProductListForSiteId");

    }
    public void getRouteShipmentProductListForSiteId(String siteId, String deliveryId,boolean valid, final OnCompleteListeners.getRouteShipmentProductListForSiteIdListener listener) {

        Log.v("DM", "Enter Function: getRouteShipmentProductListForSiteId");

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/RouteShipmentProduct");

        if (isValidParameterString(siteId)) {
            urlString.append("?siteId=");
            urlString.append(siteId);

            urlString.append("&valid=");
            urlString.append(true);

            urlString.append("&deliveryId=");
            urlString.append(deliveryId);
        } else {
            Log.e("DM", "getRouteShipmentProductListForSiteId: Parameter site id is not set");
        }
        Log.v("DM", "Request URL with Role: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                if (error == null) {
                    try {
                        JSONArray jsonArray = (JSONArray) object;
                        ArrayList<RouteShipmentProduct> resultArrayList = new ArrayList<RouteShipmentProduct>();

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);

                            RouteShipmentProduct result = new RouteShipmentProduct();
                            result.readFromJSONObject(jsonObject);
                            resultArrayList.add(result);
                        }

                        listener.getRouteShipmentProductListForSiteId(resultArrayList, null);
                    } catch (JSONException e) {
                        listener.getRouteShipmentProductListForSiteId(null, new Error(e.getMessage()));
                    }

                } else {
                    listener.getRouteShipmentProductListForSiteId(null, error);
                }
            }
        });


        restConnection.execute(String.valueOf(urlString));


        Log.v("DM", "Exit Function: getRouteShipmentProductListForSiteId");

    }

    public void getCustomerSettings(final OnCompleteListeners.getCustomerSettingsListener listener) {

        Log.v("DM", "Enter Function: getCustomerSettings");
        final OnCompleteListeners.getCustomerSettingsListener GetCustomerSettings = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/Settings");

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue,  new OnCompleteListeners.OnRestConnectionCompleteListener(){
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for getCustomerSettings: " + object.toString());
                        CustomerSettings result = new CustomerSettings();
                        JSONObject jsonObject = (JSONObject) object;
                        result.readFromJSONObject(jsonObject);
                        listener.getCustomerSettings(result, null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.getCustomerSettings(null, error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for getCustomerSettings" + e.getMessage());
                    listener.getCustomerSettings(null, error);

                }
            }

        });

        restConnection.execute(String.valueOf(urlString));

        Log.v("DM", "ExitFunction: getCustomerSettings");
    }

    public void getTrailerSettingsForSiteId(String siteId, String trailerId, final OnCompleteListeners.getTrailerSettingsForSiteIdListener listener) {

        Log.v("DM", "Enter Function: getTrailerSettingsForSiteId");
        final OnCompleteListeners.getTrailerSettingsForSiteIdListener TrailerSettingsForSiteId = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/Settings");

        //try
        urlString.append("?siteId=");
        urlString.append(siteId);
        urlString.append("&trailerId=");
        urlString.append(trailerId);


        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue,  new OnCompleteListeners.OnRestConnectionCompleteListener(){
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for getTrailerSettingsForSiteId: " + object.toString());
                        TrailerSettings result = new TrailerSettings();
                        JSONObject jsonObject = (JSONObject) object;
                        result.readFromJSONObject(jsonObject);
                        listener.getTrailerSettingsForSiteId(result, null);
                        Log.e("DM", "Null object ");
                        listener.getTrailerSettingsForSiteId(null, error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for getTrailerSettingsForSiteId" + e.getMessage());
                    listener.getTrailerSettingsForSiteId(null, error);
                }
            }
        });

        restConnection.execute(String.valueOf(urlString));

        Log.v("DM", "ExitFunction: getTrailerSettingsForSiteId");
    }

    public void getTractorSettingsForSiteId(String siteId, String tractorId, final OnCompleteListeners.getTractorSettingsForSiteIdListener listener) {

        Log.v("DM", "Enter Function: getTractorSettingsForSiteId");
        final OnCompleteListeners.getTractorSettingsForSiteIdListener TractorSettingsForSiteId = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/Settings/TractorSettings");

        //try
        urlString.append("?siteId=");
        urlString.append(siteId);
        urlString.append("&tractorId=");
        urlString.append(tractorId);

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue,  new OnCompleteListeners.OnRestConnectionCompleteListener(){
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for getTractorSettingsForSiteId: " + object.toString());
                        TrailerSettings result = new TrailerSettings();
                        JSONObject jsonObject = (JSONObject) object;
                        result.readFromJSONObject(jsonObject);
                        listener.getTractorSettingsForSiteId(result, null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.getTractorSettingsForSiteId(null, error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for getTractorSettingsForSiteId" + e.getMessage());
                    listener.getTractorSettingsForSiteId(null, error);
                }
            }

        });

        restConnection.execute(String.valueOf(urlString));

        Log.v("DM", "ExitFunction: getTractorSettingsForSiteId");
    }

    public void getStoreProviderSiteSettingsForSiteId(String siteId, final OnCompleteListeners.getStoreProviderSiteSettingsForSiteIdListener listener) {

        Log.v("DM", "Enter Function: getStoreProviderSiteSettingsForSiteId");
        final OnCompleteListeners.getStoreProviderSiteSettingsForSiteIdListener StoreProviderSiteSettingsForSiteId = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/Settings");
        //try
        urlString.append("?siteId=");
        urlString.append(siteId);

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue,  new OnCompleteListeners.OnRestConnectionCompleteListener(){
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for getStoreProviderSiteSettingsForSiteId: " + object.toString());
                        StoreProviderSiteSettings result = new StoreProviderSiteSettings();
                        JSONObject jsonObject = (JSONObject) object;
                        result.readFromJSONObject(jsonObject);
                        listener.getStoreProviderSiteSettingsForSiteId(result, null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.getStoreProviderSiteSettingsForSiteId(null, error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for getStoreProviderSiteSettingsForSiteId" + e.getMessage());
                    listener.getStoreProviderSiteSettingsForSiteId(null, error);

                }
            }
        });

        restConnection.execute(String.valueOf(urlString));

        Log.v("DM", "ExitFunction: getStoreProviderSiteSettingsForSiteId");
    }

    public void getDriverDataForSiteId(String siteId, String userId, Date timestamp, int span, final OnCompleteListeners.getDriverDataForSiteIdListener listener) {

        Log.v("DM", "Enter Function: getDriverDataForSiteId");
        final OnCompleteListeners.getDriverDataForSiteIdListener DriverDataForSiteId = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/Driver");

        String timestampStr = getUTCFormatDate(timestamp);
        String span_ = String.format("%d", span);
        //try
        urlString.append("?siteId=");
        urlString.append(siteId);
        urlString.append("&userid=");
        urlString.append(userId);
        urlString.append("&timestamp=");
        urlString.append(timestampStr);
        urlString.append("&span=");
        urlString.append(span_);

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for getDriverDataForSiteId: " + object.toString());
                        DriverData driverData = new DriverData();
                        JSONObject jsonObject = (JSONObject) object;
                        driverData.readFromJSONObject(jsonObject);
                        listener.getDriverDataForSiteId(driverData, null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.getDriverDataForSiteId(null, error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for getDriverDataForSiteId" + e.getMessage());
                    listener.getDriverDataForSiteId(null, error);

                }
            }
        });

        restConnection.execute(String.valueOf(urlString));

        Log.v("DM", "ExitFunction: getDriverDataForSiteId");
    }

    public void getRouteShipmentProductForProductId(String productId, String siteId, boolean valid, final OnCompleteListeners.getRouteShipmentProductForProductIdListener listener) {

        Log.v("DM", "Enter Function: getRouteShipmentProductForProductId");
        final OnCompleteListeners.getRouteShipmentProductForProductIdListener RouteShipmentProduct = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/RouteShipmentProduct");

        //try
        if (isValidParameterString(siteId)) {
            urlString.append("?siteId=");
            urlString.append(siteId);
            urlString.append("&routeShipmentProductId=");
            urlString.append(productId);
            urlString.append("&valid=");
            urlString.append(valid ? "1" : "0");
        } else {
            Log.e("DM", "getRouteShipmentProductForProductId: siteId parameter is invalid/missing,returning");
            //return -1;
        }


        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "ShipmentProduct", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for getRouteShipmentProductForProductId: " + object.toString());
                    RouteShipmentProduct routeShipmentProduct = new RouteShipmentProduct();
                    routeShipmentProduct.readFromJSONObject(object);
                    listener.getRouteShipmentProductForProductId(routeShipmentProduct);
                } else {
                    Log.d("DM", "Null object response for getRouteShipmentProductForProductId");
                }
            }
        });

        restConnection.execute(String.valueOf(urlString));

        Log.v("DM", "ExitFunction: getRouteShipmentProductForProductId");
    }

    public void createBorderCrossingEventForDeviceId(String deviceId, String routeId, String driverId, String shipmentId, Date timestamp,
                                                     int odometerReading, String enteringState, String leavingState,
                                                     String enteringCountry, String leavingCountry, final OnCompleteListeners.OnCreateBorderCrossingEventForDeviceIdListener listener) {

        Log.v("DM", "Enter Function: createBorderCrossingEventForDeviceId");
        OnCompleteListeners.OnCreateBorderCrossingEventForDeviceIdListener CreateBorderCrossingEvent = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Location/Location/InsertBorderCrossing");


        //try
        if (isValidParameterString(deviceId)) {
            urlString.append("?deviceId=");
            urlString.append(deviceId);
        }

        if (isValidParameterString(shipmentId)) {
            urlString.append("&shipmentId=");
            urlString.append(shipmentId);
        }

        if (isValidParameterString(routeId)) {
            urlString.append("&routeId=");
            urlString.append(routeId);
        }

        if (isValidParameterString(driverId)) {
            urlString.append("&driverId=");
            urlString.append(driverId);
        }
        if (isValidParameterString(enteringState)) {
            urlString.append("&entering=");
            urlString.append(enteringState);
        }

        if (isValidParameterString(leavingState)) {
            urlString.append("&leaving=");
            urlString.append(leavingState);
        }

        if (isValidParameterString(enteringCountry)) {
            urlString.append("&enteringCountryCode=");
            urlString.append(enteringCountry);
        }
        if (isValidParameterString(leavingState)) {
            urlString.append("&leavingCountryCode=");
            urlString.append(leavingCountry);
        }

        if (odometerReading > 0) {
            urlString.append("&odometer=");
            urlString.append(String.format("%@", odometerReading));
        } else {
            Log.e("DM", "createBorderCrossingEventForDeviceId: Odometer  parameter cannot be null");
        }
        if (timestamp != null) {
            urlString.append("&timestamp=");
            urlString.append(getUTCFormatDate(timestamp));
            //SimpleTimeZone.UTC_TxIME(timestamp);
//            DateFormat formatter = new SimpleDateFormat("dd MMM yyyy HH:mm:ss z");
//            formatter.setTimeZone(TimeZone.getTimeZone("UTC"));
//            formatter.format(timestamp);
            // [parameterString appendString:[NSDate getISOFormatDate:timestamp]];
        } else {
            Log.e("DM", "createBorderCrossingEventForDeviceId: Timestamp  parameter cannot be null");
        }


        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        listener.createBorderCrossingEventForDeviceId(null);
                        Log.v("DM", "Received JSON Object for createBorderCrossingEventForDeviceId: " + object.toString());
                    } else {
                        Log.e("DM", "Null object ");
                        listener.createBorderCrossingEventForDeviceId(error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException createBorderCrossingEventForDeviceId >> " + e.getMessage());
                    listener.createBorderCrossingEventForDeviceId(null);

                }

            }
        });

        restConnection.execute(String.valueOf(urlString));

        Log.v("DM", "ExitFunction: createBorderCrossingEventForDeviceId");
    }

    public void getBorderCrossingDataForShipmentId(String shipmentId, final OnCompleteListeners.getBorderCrossingDataForShipmentIdListener listener) {

        Log.v("DM", "Enter Function: getBorderCrossingDataForShipmentId");
        final OnCompleteListeners.getBorderCrossingDataForShipmentIdListener BorderCrossingData = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Location/Location");

        //try
        urlString.append("?shipmentId=");
        urlString.append(shipmentId);

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "BorderCrossingData", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for getBorderCrossingDataForShipmentId: " + object.toString());
                    LocationDTO borderCrossingData = new LocationDTO();
                    borderCrossingData.readFromJSONObject(object);
                    listener.getBorderCrossingDataForShipmentId(borderCrossingData);
                } else {
                    Log.d("DM", "Null object response for getBorderCrossingDataForShipmentId");
                }
            }
        });

        restConnection.execute(String.valueOf(urlString));

        Log.v("DM", "ExitFunction: getBorderCrossingDataForShipmentId");
    }

    public void getBorderCrossingDataForDeviceId(String deviceId, final OnCompleteListeners.getBorderCrossingDataForDeviceIdListener listener) {

        Log.v("DM", "Enter Function: getBorderCrossingDataForDeviceId");
        final OnCompleteListeners.getBorderCrossingDataForDeviceIdListener borderCrossingDataForDevice = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Location/Location");

        //try
        urlString.append("?deviceId=");
        urlString.append(deviceId);

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for getBorderCrossingDataForDeviceId: " + object.toString());
                        LocationDTO borderCrossingData = new LocationDTO();
                        JSONObject jsonObject = (JSONObject) object;
                        borderCrossingData.readFromJSONObject(jsonObject);
                        listener.getBorderCrossingDataForDeviceId(borderCrossingData, null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.getBorderCrossingDataForDeviceId(null, error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for getBorderCrossingDataForDeviceId" + e.getMessage());
                    listener.getBorderCrossingDataForDeviceId(null, error);

                }
            }
        });

        restConnection.execute(String.valueOf(urlString));

        Log.v("DM", "ExitFunction: getBorderCrossingDataForDeviceId");
    }


    public void modifyPOIStatusForPOIId(String poiId, String deliveryId, DeliveryPOITypeEnum.DeliveryPOITypesEnum typeOverride, String name, boolean hasAddress, AddressComponents address,
                                        boolean hasManualTimes, String arrivalTime, String departureTime, boolean hasLocation, Double latitude, Double longitude,
                                        boolean hasComment, String comment, final OnCompleteListeners.getModifyPOIStatusForPOIIdListener listener) {

        Log.v("DM", "Enter Function: modifyPOIStatusForPOIId");
        final OnCompleteListeners.getModifyPOIStatusForPOIIdListener DeliveryPOIStatusForPOIId = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/RouteShipment2/ModifyPOIStatus");
        StringBuilder parameterString = new StringBuilder();

        //Integer typePOI= Integer.parseInt(typeOverride.toString());
        //try
        if (isValidParameterString(poiId)) {
            parameterString.append("poiId=");
            parameterString.append(poiId);
        } else {
            Log.e("DM", "modifyPOIStatusForPOIId: POIId is nil, Cannot send request to Server");
        }

        if (isValidParameterString(deliveryId)) {
            parameterString.append("&deliveryId=");
            parameterString.append(deliveryId);
        } else {
            Log.e("DM", "modifyPOIStatusForPOIId: deliveryId is nil, Cannot send request to Server");
        }

        if (isValidParameterString(name)) {
            parameterString.append("&name=");
            parameterString.append(name);
        }
        if (typeOverride.getValue() >= 0) {
            String typeOverrideStr = String.format("%d", typeOverride);
            parameterString.append("&typeOverride=");
            parameterString.append(typeOverrideStr);
        }

        if (hasAddress == Boolean.TRUE) {
            if (address != null) {
                parameterString.append("&hasAddress=");
                parameterString.append(true);
                //urlString.append("1");


                if (address.Address1 != null) {
                    parameterString.append("&address1=");
                    parameterString.append(address.Address1);
                }
                if (address.Address2 != null) {
                    parameterString.append("&address2=");
                    parameterString.append(address.Address2);
                }
                if (address.City != null) {
                    parameterString.append("&city=");
                    parameterString.append(address.City);
                }
                if (address.Province != null) {
                    parameterString.append("&province=");
                    parameterString.append(address.Province);
                }
                if (address.Postal != null) {
                    parameterString.append("&postal=");
                    parameterString.append(address.Postal);
                }
                if (address.Country != null) {
                    parameterString.append("&country=");
                    parameterString.append(address.Country);
                }
                if (address.StreetName != null) {
                    parameterString.append("&streetName=");
                    parameterString.append(address.StreetName);
                }
                if (address.StreetNumber != null) {
                    parameterString.append("&streetNumber=");
                    parameterString.append(address.StreetNumber);
                }
            } else {
                Log.d("DM", "modifyPOIStatusForPOIId: HasAddress parameter is set to TRUE But 'address' parameter is nil");
            }
        }

        if (hasLocation == true) {
            parameterString.append("&hasLocation=");
            parameterString.append("1");

            String latitudeStr = String.format("%f", latitude);
            String longitudeStr = String.format("%f", longitude);

            parameterString.append("&latitude=");
            parameterString.append(latitudeStr);
            parameterString.append("&longitude=");
            parameterString.append(longitudeStr);

        }

        if (hasManualTimes == true) {
            parameterString.append("&hasManualTimes=");
            parameterString.append("1");

            if (isValidParameterString(arrivalTime)) {
                parameterString.append("&arrivalTime=");
                parameterString.append(arrivalTime);
            }

            if (isValidParameterString(departureTime)) {
                parameterString.append("&departureTime=");
                parameterString.append(departureTime);
            }
        }

        if (hasComment == true) {
            parameterString.append("&hasComment=");
            parameterString.append("1");

            if (isValidParameterString(comment)) {
                parameterString.append("&comment=");
                parameterString.append(comment);
            } else {
                Log.d("DM", "modifyPOIStatusForPOIId: HasComment parameter is set to TRUE But 'comment' parameter is nil");
            }
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initializePOSTRequest(this.authHeaderValue, parameterString.toString(), new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for modifyPOIStatusForPOIId: " + object.toString());
                        DeliveryPOIActionResult result = new DeliveryPOIActionResult();
                        JSONObject jsonObject = (JSONObject) object;
                        result.readFromJSONObject(jsonObject);
                        listener.modifyPOIStatusForPOIId(result, null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.modifyPOIStatusForPOIId(null, error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for modifyPOIStatusForPOIId" + e.getMessage());
                    listener.modifyPOIStatusForPOIId(null, error);

                }
            }
        });

        restConnection.execute(String.valueOf(urlString));

        Log.v("DM", "ExitFunction: modifyPOIStatusForPOIId");
    }


    public void modifyPOIStatusWithFuel(String poiId, String deliveryId, DeliveryPOITypeEnum typeOverride, String name, boolean hasAddress, AddressComponents address,
                                        boolean hasManualTimes, String arrivalTime, String departureTime, boolean hasLocation, Double latitude, Double longitude,
                                        boolean hasComment, String comment, Double tractorFuelQuantity, Double tractorFuelCost, Integer tractorOdometer, Double trailerFuelQuantity,
                                        Double trailerFuelCost, Integer trailerOdometer, double defQuantity, double defCost, final OnCompleteListeners.modifyPOIStatusWithFuelListener listener) {

        Log.v("DM", "Enter Function: modifyPOIStatusWithFuel");
        final OnCompleteListeners.modifyPOIStatusWithFuelListener modifyStatusWithFuel = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/RouteShipment2/ModifyPOIStatusWithFuel");

        //try
        Integer typePOI = Integer.parseInt(typeOverride.toString());
        if (isValidParameterString(poiId)) {
            urlString.append("poiId=");
            urlString.append(poiId);
        } else {
            Log.e("DM", "modifyPOIStatusWithFuel: POIId is nil, Cannot send request to Server");
        }

        if (isValidParameterString(deliveryId)) {
            urlString.append("&deliveryId=");
            urlString.append(deliveryId);
        } else {
            Log.e("DM", "modifyPOIStatusWithFuel: deliveryId is nil, Cannot send request to Server");
        }

        if (isValidParameterString(name)) {
            urlString.append("&name=");
            urlString.append(name);
        } else {
            Log.v("DM", "modifyPOIStatusWithFuel: Name is  Nil");
        }
        if (typePOI >= 0) {
            String typeOverrideStr = String.format("%d", Integer.parseInt(typeOverride.toString()));
            urlString.append("&typeOverride=");
            urlString.append(typeOverrideStr);
        }
        if (tractorFuelQuantity >= 0) {
            String str = String.format("%f", tractorFuelQuantity);
            urlString.append("&tractorFuelQuantity=");
            urlString.append(str);
        }
        if (tractorFuelCost >= 0) {
            String str = String.format("%f", tractorFuelCost);
            urlString.append("&tractorFuelCost=");
            urlString.append(str);
        }

        if (tractorOdometer >= 0) {
            String str = String.format("%f", tractorOdometer);
            urlString.append("&tractorOdometer=");
            urlString.append(str);

        }
        if (trailerFuelQuantity >= 0) {
            String str = String.format("%f", trailerFuelQuantity);
            urlString.append("&trailerFuelQuantity=");
            urlString.append(str);

        }
        if (trailerFuelCost >= 0) {
            String str = String.format("%f", trailerFuelCost);
            urlString.append("&trailerFuelCost=");
            urlString.append(str);
        }

        if (trailerOdometer >= 0) {
            String str = String.format("%f", trailerOdometer);
            urlString.append("&trailerOdometer=");
            urlString.append(str);
        }
        if (hasAddress == Boolean.TRUE) {
            if (address != null) {
                urlString.append("&hasAddress=");
                urlString.append("1");


                if (address.Address1 != null) {
                    urlString.append("&address1=");
                    urlString.append(address.Address1);
                }
                if (address.Address2 != null) {
                    urlString.append("&address2=");
                    urlString.append(address.Address2);
                }
                if (address.City != null) {
                    urlString.append("&city=");
                    urlString.append(address.City);
                }
                if (address.Province != null) {
                    urlString.append("&province=");
                    urlString.append(address.Province);
                }
                if (address.Postal != null) {
                    urlString.append("&postal=");
                    urlString.append(address.Postal);
                }
                if (address.Country != null) {
                    urlString.append("&country=");
                    urlString.append(address.Country);
                }
                if (address.StreetName != null) {
                    urlString.append("&streetName=");
                    urlString.append(address.StreetName);
                }
                if (address.StreetNumber != null) {
                    urlString.append("&streetNumber=");
                    urlString.append(address.StreetNumber);
                }
            } else {
                Log.d("DM", "modifyPOIStatusWithFuel: HasAddress parameter is set to TRUE But 'address' parameter is nil");
            }
        }

        if (hasLocation == true) {
            urlString.append("&hasLocation=");
            urlString.append("1");

            String latitudeStr = String.format("%f", latitude);
            String longitudeStr = String.format("%f", longitude);

            urlString.append("&latitude=");
            urlString.append(latitudeStr);
            urlString.append("&longitude=");
            urlString.append(longitudeStr);

        }

        if (hasManualTimes == true) {
            urlString.append("&hasManualTimes=");
            urlString.append("1");

            if (isValidParameterString(arrivalTime)) {
                urlString.append("&arrivalTime=");
                urlString.append(arrivalTime);
            }

            if (isValidParameterString(departureTime)) {
                urlString.append("&departureTime=");
                urlString.append(departureTime);
            }
        }

        if (hasComment == true) {
            urlString.append("&hasComment=");
            urlString.append("1");

            if (isValidParameterString(comment)) {
                urlString.append("&comment=");
                urlString.append(comment);
            } else {
                Log.d("DM", "modifyPOIStatusForPOIId: HasComment parameter is set to TRUE But 'comment' parameter is nil");
            }
        }

        if (defCost >= 0) {
            String str = String.format("%f", defCost);
            urlString.append("&defCost=");
            urlString.append(str);
        }

        if (defQuantity >= 0) {
            String str = String.format("%f", defQuantity);
            urlString.append("&defQuantity=");
            urlString.append(str);

        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for modifyPOIStatusWithFuel: " + object.toString());
                        DeliveryPOIActionResult result = new DeliveryPOIActionResult();
                        JSONObject jsonObject = (JSONObject) object;
                        result.readFromJSONObject(jsonObject);
                        listener.modifyPOIStatusWithFuel(result, error);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.modifyPOIStatusWithFuel(null, error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for modifyPOIStatusWithFuel" + e.getMessage());
                    listener.modifyPOIStatusWithFuel(null, error);

                }

            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: modifyPOIStatusWithFuel");
    }

    public void createPermanentStopForDeliveryId(String deliveryId, String sitename, SiteTypeEnum.SiteTypesEnum siteType, double latitude,
                                                 double longitude, final OnCompleteListeners.OnCreatePermanentStopForDeliveryIdListener listener) {

        Log.v("DM", "Enter Function: createPermanentStopForDeliveryId");
        final OnCompleteListeners.OnCreatePermanentStopForDeliveryIdListener CreatePermanentStop = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/RouteShipment2/CreatePermanentStop_old");

        StringBuilder parameterString = new StringBuilder();


        if (isValidParameterString(deliveryId)) {
            parameterString.append("deliveryId=");
            parameterString.append(deliveryId);
        } else {
            Log.e("DM", "createPermanentStopForDeliveryId: deliveryId is nil, Cannot send request to Server");
        }

        if (isValidParameterString(sitename)) {
            parameterString.append("&sitename=");
            parameterString.append(sitename);
        } else {
            Log.e("DM", "createPermanentStopForDeliveryId: Sitename parameter is Nil");
        }
        switch (siteType) {
            case SiteType_Inspection:
                parameterString.append("&siteType=");
                parameterString.append("inspection");
                break;
            case SiteType_Plant:
                parameterString.append("&siteType=");
                parameterString.append("plant");
            case SiteType_Shuttle:
                parameterString.append("&siteType=");
                parameterString.append("shuttle");
            case SiteType_Fuel:
                parameterString.append("&siteType=");
                parameterString.append("fuel");
            case SiteType_Service:
                parameterString.append("&siteType=");
                parameterString.append("service");
            case SiteType_Rest:
                parameterString.append("&siteType=");
                parameterString.append("rest");
            case SiteType_Depot:
                parameterString.append("&siteType=");
                parameterString.append("depot");
            case SiteType_Store:
                parameterString.append("&siteType=");
                parameterString.append("store");
            case SiteType_Unauthorized:
                parameterString.append("&siteType=");
                parameterString.append("unauthorized");
            default:
                Log.v("DM", "Undefined SiteType");
                break;
        }
        String latitudeStr = String.format("%f", latitude);
        String longitudeStr = String.format("%f", longitude);

        parameterString.append("&lat=");
        parameterString.append(latitudeStr);
        parameterString.append("&lon=");
        parameterString.append(longitudeStr);


        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initializePOSTRequest(this.authHeaderValue, parameterString.toString(), new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for createPermanentStopForDeliveryId: " + object.toString());
                        RouteShipmentCreateStopResult result = new RouteShipmentCreateStopResult();
                        JSONObject jsonObject = (JSONObject) object;
                        result.readFromJSONObject(jsonObject);
                        listener.createPermanentStopForDeliveryId(result, null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.createPermanentStopForDeliveryId(null, error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for createPermanentStopForDeliveryId" + e.getMessage());
                    listener.createPermanentStopForDeliveryId(null, error);

                }
            }
        });

        restConnection.execute(String.valueOf(urlString));

        Log.v("DM", "ExitFunction: createPermanentStopForDeliveryId");
    }


    public void createPermanentStopForDeliveryId(String deliveryId, String sitename, SiteTypeEnum.SiteTypesEnum siteType, final OnCompleteListeners.OnCreatePermanentStopForDeliveryId2Listener listener) {

        Log.v("DM", "Enter Function: createPermanentStopForDeliveryId");
        final OnCompleteListeners.OnCreatePermanentStopForDeliveryId2Listener CreatePermanentStopForDeliveryId2 = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/RouteShipment2/CreatePermanentStop");

        Log.v("DM", "Request URL: " + urlString);

        StringBuilder parameterString = new StringBuilder();

        if (isValidParameterString(deliveryId)) {
            parameterString.append("deliveryId=");
            parameterString.append(deliveryId);
        } else {
            Log.e("DM", "createPermanentStopForDeliveryId: deliveryId is nil, Cannot send request to Server");
        }

        if (isValidParameterString(sitename)) {
            parameterString.append("&sitename=");
            parameterString.append(sitename);
        } else {
            Log.e("DM", "createPermanentStopForDeliveryId: Sitename parameter is Nil");
        }

        if (siteType.getValue() >= 0) {
            parameterString.append("&sitename=");
            parameterString.append(String.format("%d", siteType.getValue()));

        }

        RestConnection restConnection = new RestConnection();
        restConnection.initializePOSTRequest(this.authHeaderValue, parameterString.toString(), new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for createPermanentStopForDeliveryId: " + object.toString());
                        DeliveryPOIActionResult result = new DeliveryPOIActionResult();
                        JSONObject jsonObject = (JSONObject) object;
                        result.readFromJSONObject(jsonObject);
                        listener.createPermanentStopForDeliveryId2(result, null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.createPermanentStopForDeliveryId2(null, error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for setDeliveryConfirmationTimeForDeliveryId" + e.getMessage());
                    listener.createPermanentStopForDeliveryId2(null, error);

                }
            }
        });

        restConnection.execute(String.valueOf(urlString));

        Log.v("DM", "ExitFunction: createPermanentStopForDeliveryId");
    }

    public void createPermanentStopWithFuelForDeliveryId(String deliveryId, String poiId, String siteName, SiteTypeEnum.SiteTypesEnum siteType,
                                                         double tractorFuelQuantity, double tractorFuelCost, Integer tractorOdometer, double trailerFuelQuantity, double trailerFuelCost, Integer trailerOdometer, final OnCompleteListeners.OnCreatePermanentStopWithFuelForDeliveryId listener) {

        Log.v("DM", "Enter Function: createPermanentStopWithFuelForDeliveryId");
        final OnCompleteListeners.OnCreatePermanentStopWithFuelForDeliveryId CreatePermanentStopWithFuel = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/RouteShipment2/CreatePermanentStopWithFuel");

        StringBuilder parameterString = new StringBuilder();

        if (isValidParameterString(deliveryId)) {
            parameterString.append("deliveryId=");
            parameterString.append(deliveryId);
        } else {
            Log.e("DM", "createPermanentStopWithFuelForDeliveryId: DeliveryId is nil, Cannot send request to Server");
        }

        if (isValidParameterString(siteName)) {
            parameterString.append("&siteName=");
            parameterString.append(siteName);
        } else {
            Log.d("DM", "createPermanentStopWithFuelForDeliveryId: Sitename is Nil");
        }

        if (siteType.getValue() >= 0) {
            String siteTypeStr = String.format("%d", siteType.getValue());
            parameterString.append("&siteType=");
            parameterString.append(siteTypeStr);
        }
        if (tractorFuelQuantity >= 0) {
            String str = String.format("%f", tractorFuelQuantity);
            urlString.append("&tractorFuelQuantity=");
            urlString.append(str);
        }
        if (tractorFuelCost >= 0) {
            String str = String.format("%f", tractorFuelCost);
            urlString.append("&tractorFuelCost=");
            urlString.append(str);
        }

        if (tractorOdometer >= 0) {
            String str = String.format("%f", tractorOdometer);
            urlString.append("&tractorOdometer=");
            urlString.append(str);
        }
        if (trailerFuelQuantity >= 0) {
            String str = String.format("%f", trailerFuelQuantity);
            urlString.append("&trailerFuelQuantity=");
            urlString.append(str);
        }
        if (trailerFuelCost >= 0) {
            String str = String.format("%f", trailerFuelCost);
            urlString.append("&trailerFuelCost=");
            urlString.append(str);
        }

        if (trailerOdometer >= 0) {
            String str = String.format("%f", trailerOdometer);
            urlString.append("&trailerOdometer=");
            urlString.append(str);
        }


        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initializePOSTRequest(this.authHeaderValue, parameterString.toString(), new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for createPermanentStopWithFuelForDeliveryId: " + object.toString());
                        DeliveryPOIActionResult result = new DeliveryPOIActionResult();
                        JSONObject jsonObject = (JSONObject) object;
                        result.readFromJSONObject(jsonObject);
                        listener.createPermanentStopWithFuelForDeliveryId(result, null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.createPermanentStopWithFuelForDeliveryId(null, error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for createPermanentStopWithFuelForDeliveryId" + e.getMessage());
                    listener.createPermanentStopWithFuelForDeliveryId(null, error);

                }
            }
        });

        restConnection.execute(String.valueOf(urlString));

        Log.v("DM", "ExitFunction: createPermanentStopWithFuelForDeliveryId");
    }


    public void createPOIForDeliveryId(String deliveryId, String poiId, DeliveryPOITypeEnum.DeliveryPOITypesEnum type, String name, String timestamp,
                                       String arrivalTime, String departureTime, double latitude, double longitude, AddressComponents address,
                                       boolean hasComment, String comment, final OnCompleteListeners.OncreatePOIForDeliveryIdListener listener) {

        Log.v("DM", "Enter Function: createPOIForDeliveryId");
        final OnCompleteListeners.OncreatePOIForDeliveryIdListener CreatePOIForDeliveryId = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/RouteShipment2/CreatePOI");


        // try
        StringBuilder parameterString = new StringBuilder();


        if (isValidParameterString(deliveryId)) {
            parameterString.append("deliveryId=");
            parameterString.append(deliveryId);
        } else {
            Log.e("DM", "createPOIForDeliveryId: DeliveryId is nil, Cannot send request to Server");
        }

        if (isValidParameterString(poiId)) {
            parameterString.append("&poiId=");
            parameterString.append(poiId);
        }
        if (isValidParameterString(name)) {
            parameterString.append("&name=");
            parameterString.append(name);
        } else {
            Log.e("DM", "createPOIForDeliveryId: Name is  Nil");
        }
        if (type.getValue() >= 0) {
            String TypeStr = String.format("%d", type.getValue());
            parameterString.append("&type=");
            parameterString.append(TypeStr);
        }

        if (address != null) {
            parameterString.append("&hasAddress=");
            parameterString.append("1");

            if (address.Address1 != null) {
                parameterString.append("&address1=");
                parameterString.append(address.Address1);
            }
            if (address.Address2 != null) {
                parameterString.append("&address2=");
                parameterString.append(address.Address2);
            }
            if (address.City != null) {
                parameterString.append("&city=");
                parameterString.append(address.City);
            }
            if (address.Province != null) {
                parameterString.append("&province=");
                parameterString.append(address.Province);
            }
            if (address.Postal != null) {
                parameterString.append("&postal=");
                parameterString.append(address.Postal);
            }
            if (address.Country != null) {
                parameterString.append("&country=");
                parameterString.append(address.Country);
            }
            if (address.StreetName != null) {
                parameterString.append("&streetName=");
                parameterString.append(address.StreetName);
            }
            if (address.StreetNumber != null) {
                parameterString.append("&streetNumber=");
                parameterString.append(address.StreetNumber);
            }
        }

        String latitudeStr = String.format("%f", latitude);
        String longitudeStr = String.format("%f", longitude);

        parameterString.append("&latitude=");
        parameterString.append(latitudeStr);
        parameterString.append("&longitude=");
        parameterString.append(longitudeStr);

        if (isValidParameterString(timestamp)) {
            parameterString.append("&timestamp=");
            parameterString.append(timestamp);
        }

        if (isValidParameterString(arrivalTime)) {
            parameterString.append("&arrivalTime=");
            parameterString.append(arrivalTime);
        }
        if (isValidParameterString(departureTime)) {
            parameterString.append("&departureTime=");
            parameterString.append(departureTime);
        }
        if (hasComment == true) {
            parameterString.append("&hasComment=");
            parameterString.append("1");

            if (isValidParameterString(comment)) {
                parameterString.append("&comment=");
                parameterString.append(comment);
            } else {
                Log.e("DM", "HasComment parameter is set to TRUE But 'comment' parameter is nil");
            }
        }
        Log.v("DM", "Create Form  API Parameters: " + parameterString);

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initializePOSTRequest(this.authHeaderValue, parameterString.toString(), new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for createPOIForDeliveryId: " + object.toString());
                        DeliveryPOIActionResult result = new DeliveryPOIActionResult();
                        JSONObject jsonObject = (JSONObject) object;
                        result.readFromJSONObject(jsonObject);
                        listener.createPOIWithFuelForDeliveryId(result, null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.createPOIWithFuelForDeliveryId(null, error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for createPOIWithFuelForDeliveryId" + e.getMessage());

                }
            }
        });

        restConnection.execute(String.valueOf(urlString));

        Log.v("DM", "ExitFunction: createPOIForDeliveryId");
    }

    public void createPOIWithFuelForDeliveryId(String deliveryId, String poiId, DeliveryPOITypeEnum.DeliveryPOITypesEnum type, String name, String timestamp,
                                               String arrivalTime, String departureTime, double latitude, double longitude, AddressComponents address,
                                               boolean hasComment, String comment, double tractorFuelQuantity, double tractorFuelCost, Integer tractorOdometer,
                                               double trailerFuelQuantity, double trailerFuelCost, Integer trailerOdometer, final OnCompleteListeners.OnCreatePOIWithFuelForDeliveryIdListener listener) {

        Log.v("DM", "Enter Function: createPOIWithFuelForDeliveryId");
        final OnCompleteListeners.OnCreatePOIWithFuelForDeliveryIdListener CreatePOIWithFuelForDeliveryId = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/RouteShipment2/CreatePOIWithFuel");

        StringBuilder parameterString = new StringBuilder();


        if (isValidParameterString(deliveryId)) {
            parameterString.append("deliveryId=");
            parameterString.append(deliveryId);
        } else {
            Log.e("DM", "createPOIWithFuelForDeliveryId: DeliveryId is nil, Cannot send request to Server");
        }

        if (isValidParameterString(poiId)) {
            parameterString.append("&poiId=");
            parameterString.append(poiId);
        }
        if (isValidParameterString(name)) {
            parameterString.append("&name=");
            parameterString.append(name);
        }

        if (type.getValue() >= 0) {
            String TypeStr = String.format("%d", type.getValue());
            parameterString.append("&siteType=");
            parameterString.append(TypeStr);
        }
        if (address != null) {
            parameterString.append("&hasAddress=");
            parameterString.append("1");

            if (address.Address1 != null) {
                parameterString.append("&address1=");
                parameterString.append(address.Address1);
            }
            if (address.Address2 != null) {
                parameterString.append("&address2=");
                parameterString.append(address.Address2);
            }
            if (address.City != null) {
                parameterString.append("&city=");
                parameterString.append(address.City);
            }
            if (address.Province != null) {
                parameterString.append("&province=");
                parameterString.append(address.Province);
            }
            if (address.Postal != null) {
                parameterString.append("&postal=");
                parameterString.append(address.Postal);
            }
            if (address.Country != null) {
                parameterString.append("&country=");
                parameterString.append(address.Country);
            }
            if (address.StreetName != null) {
                parameterString.append("&streetName=");
                parameterString.append(address.StreetName);
            }
            if (address.StreetNumber != null) {
                parameterString.append("&streetNumber=");
                parameterString.append(address.StreetNumber);
            }
        }

        String latitudeStr = String.format("%f", latitude);
        String longitudeStr = String.format("%f", longitude);

        parameterString.append("&latitude=");
        parameterString.append(latitudeStr);
        parameterString.append("&longitude=");
        parameterString.append(longitudeStr);

        if (timestamp != null) {
            parameterString.append("&timestamp=");
            // [parameterString appendString:[NSDate getISOFormatDate:timestamp]]
            parameterString.append(timestamp);
        }

        if (arrivalTime != null) {
            parameterString.append("&arrivalTime=");
            parameterString.append(arrivalTime);
        }
        if (departureTime != null) {
            parameterString.append("&departureTime=");
            parameterString.append(departureTime);
        }


        if (tractorFuelQuantity >= 0) {
            String str = String.format("%f", tractorFuelQuantity);
            parameterString.append("&tractorFuelQuantity=");
            parameterString.append(str);
        }
        if (tractorFuelCost >= 0) {
            String str = String.format("%f", tractorFuelCost);
            parameterString.append("&tractorFuelCost=");
            parameterString.append(str);
        }

        if (tractorOdometer >= 0) {
            String str = String.format("%f", tractorOdometer);
            parameterString.append("&tractorOdometer=");
            parameterString.append(str);
        }
        if (trailerFuelCost >= 0) {
            String str = String.format("%f", trailerFuelCost);
            parameterString.append("&trailerFuelCost=");
            parameterString.append(str);
        }
        if (tractorFuelCost >= 0) {
            String str = String.format("%f", tractorFuelCost);
            parameterString.append("&tractorFuelCost=");
            parameterString.append(str);
        }

        if (trailerOdometer >= 0) {
            String str = String.format("%f", trailerOdometer);
            parameterString.append("&trailerOdometer=");
            parameterString.append(str);
        }

        if (hasComment == true) {
            parameterString.append("&hasComment=");
            parameterString.append("1");

            if (isValidParameterString(comment)) {
                parameterString.append("&comment=");
                parameterString.append(comment);
            } else {
                Log.e("DM", "createPOIWithFuelForDeliveryId: HasComment parameter is set to TRUE But 'comment' parameter is nil");
            }
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initializePOSTRequest(this.authHeaderValue, parameterString.toString(), new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for createPOIForDeliveryId: " + object.toString());
                        DeliveryPOIActionResult result = new DeliveryPOIActionResult();
                        JSONObject jsonObject = (JSONObject) object;
                        result.readFromJSONObject(jsonObject);
                        listener.createPOIWithFuelForDeliveryId(result, null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.createPOIWithFuelForDeliveryId(null, error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for createPOIWithFuelForDeliveryId" + e.getMessage());
                    listener.createPOIWithFuelForDeliveryId(null, error);

                }
            }
        });

        restConnection.execute(String.valueOf(urlString));

        Log.v("DM", "ExitFunction: createPOIForDeliveryId");
    }

    public void deleteManualPOIForDeliveryId(String deliveryId, String poiId, final OnCompleteListeners.OnDeleteManualPOIForDeliveryIdListener listener) {

        Log.v("DM", "Enter Function: deleteManualPOIForDeliveryId");
        final OnCompleteListeners.OnDeleteManualPOIForDeliveryIdListener deleteManualPOI = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/RouteShipment2/DeleteManualPOI");

        StringBuilder parameterString = new StringBuilder();

        ///try
        if (isValidParameterString(deliveryId)) {
            parameterString.append("&deliveryId=");
            parameterString.append(deliveryId);
        } else {
            Log.d("DM", "deleteManualPOIForDeliveryId: DeliveryId is nil, Cannot send request to Server");
        }

        if (isValidParameterString(poiId)) {
            parameterString.append("&poiId=");
            parameterString.append(poiId);
        } else {
            Log.d("DM", "deleteManualPOIForDeliveryId: POIId is nil, Cannot send request to Server");
        }


        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initializePOSTRequest(this.authHeaderValue, parameterString.toString(), new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for deleteManualPOIForDeliveryId: " + object.toString());
                        DeliveryPOIActionResult result = new DeliveryPOIActionResult();
                        JSONObject jsonObject = (JSONObject) object;
                        result.readFromJSONObject(jsonObject);
                        listener.deleteManualPOIForDeliveryId(result, null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.deleteManualPOIForDeliveryId(null, error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for deleteManualPOIForDeliveryId" + e.getMessage());
                    listener.deleteManualPOIForDeliveryId(null, error);

                }
            }
        });

        restConnection.execute(String.valueOf(urlString));

        Log.v("DM", "ExitFunction: deleteManualPOIForDeliveryId");
    }

    public void changePOIToStartForDeliveryId(String deliveryId, String poiId, String locationName, boolean hasAddress,
                                              AddressComponents address, boolean hasComment, String comment, boolean hasManualTimes,
                                              String startTime, String departureTime, final OnCompleteListeners.OnChangePOIToStartForDeliveryIdListener listener) {

        Log.v("DM", "Enter Function: changePOIToStartForDeliveryId");
        final OnCompleteListeners.OnChangePOIToStartForDeliveryIdListener ChangePOIToStart = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/RouteShipment2/ChangePOIToStart");


        ///try
        if (isValidParameterString(deliveryId)) {
            urlString.append("&deliveryId=");
            urlString.append(deliveryId);
        } else {
            Log.d("DM", "changePOIToStartForDeliveryId: DeliveryId is nil, Cannot send request to Server");
        }

        if (isValidParameterString(poiId)) {
            urlString.append("&poiId=");
            urlString.append(poiId);
        } else {
            Log.d("DM", "changePOIToStartForDeliveryId: POIId is nil, Cannot send request to Server");
        }
        if (isValidParameterString(locationName)) {
            urlString.append("&locationName=");
            urlString.append(locationName);
        } else {
            Log.d("DM", "changePOIToStartForDeliveryId: locationName is nil/empty");
        }
        if (hasAddress == true) {
            if (address != null) {
                urlString.append("&hasAddress=");
                urlString.append("1");

                if (address.Address1 != null) {
                    urlString.append("&address1=");
                    urlString.append(address.Address1);
                }
                if (address.Address2 != null) {
                    urlString.append("&address2=");
                    urlString.append(address.Address2);
                }
                if (address.City != null) {
                    urlString.append("&city=");
                    urlString.append(address.City);
                }
                if (address.Province != null) {
                    urlString.append("&province=");
                    urlString.append(address.Province);
                }
                if (address.Postal != null) {
                    urlString.append("&postal=");
                    urlString.append(address.Postal);
                }
                if (address.Country != null) {
                    urlString.append("&country=");
                    urlString.append(address.Country);
                }
                if (address.StreetName != null) {
                    urlString.append("&streetName=");
                    urlString.append(address.StreetName);
                }
                if (address.StreetNumber != null) {
                    urlString.append("&streetNumber=");
                    urlString.append(address.StreetNumber);
                }
            } else {
                Log.d("DM", "changePOIToStartForDeliveryId: HasAddress parameter is set to TRUE But 'address' parameter is nil");
            }

        }

        if (hasManualTimes == true) {
            urlString.append("&hasManualTimes=");
            urlString.append("1");

            if (isValidParameterString(startTime)) {
                urlString.append("&startTime=");
                urlString.append(startTime);
            }

            if (isValidParameterString(departureTime)) {
                urlString.append("&departureTime=");
                urlString.append(departureTime);
            }
        }

        if (hasComment == true) {
            urlString.append("&hasComment=");
            urlString.append("1");

            if (isValidParameterString(comment)) {
                urlString.append("&comment=");
                urlString.append(comment);
            } else {
                Log.d("DM", "changePOIToStartForDeliveryId: HasComment parameter is set to TRUE But 'comment' parameter is nil");
            }
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for changePOIToStartForDeliveryId: " + object.toString());
                        DeliveryEndPointActionResult result = new DeliveryEndPointActionResult();
                        JSONObject jsonObject = (JSONObject) object;
                        result.readFromJSONObject(jsonObject);
                        listener.changePOIToStartForDeliveryId(result, null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.changePOIToStartForDeliveryId(null, error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for changePOIToStartForDeliveryId" + e.getMessage());
                    listener.changePOIToStartForDeliveryId(null, error);

                }

            }
        });

        restConnection.execute(String.valueOf(urlString));

        Log.v("DM", "ExitFunction: changePOIToStartForDeliveryId");
    }

    public void changePOIToEndForDeliveryId(String deliveryId, String poiId, String locationName, boolean hasAddress,
                                            AddressComponents address, boolean hasComment, String comment, boolean hasManualTimes,
                                            String arrivalTime, String endTime, final OnCompleteListeners.OnchangePOIToEndForDeliveryIdListener listener) {

        Log.v("DM", "Enter Function: changePOIToEndForDeliveryId");
        final OnCompleteListeners.OnchangePOIToEndForDeliveryIdListener ChangePOIToEnd = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/RouteShipment2/ChangePOIToEnd");

        ///try
        if (isValidParameterString(deliveryId)) {
            urlString.append("&deliveryId=");
            urlString.append(deliveryId);
        } else {
            Log.d("DM", "changePOIToEndForDeliveryId: DeliveryId is nil, Cannot send request to Server");
        }

        if (isValidParameterString(poiId)) {
            urlString.append("&poiId=");
            urlString.append(poiId);
        } else {
            Log.d("DM", "changePOIToEndForDeliveryId: POIId is nil/empty");
        }
        if (isValidParameterString(locationName)) {
            urlString.append("&locationName=");
            urlString.append(locationName);
        } else {
            Log.d("DM", "changePOIToEndForDeliveryId: locationName is nil/empty");
        }
        if (hasAddress == true) {
            if (address != null) {
                urlString.append("&hasAddress=");
                urlString.append("1");

                if (address.Address1 != null) {
                    urlString.append("&address1=");
                    urlString.append(address.Address1);
                }
                if (address.Address2 != null) {
                    urlString.append("&address2=");
                    urlString.append(address.Address2);
                }
                if (address.City != null) {
                    urlString.append("&city=");
                    urlString.append(address.City);
                }
                if (address.Province != null) {
                    urlString.append("&province=");
                    urlString.append(address.Province);
                }
                if (address.Postal != null) {
                    urlString.append("&postal=");
                    urlString.append(address.Postal);
                }
                if (address.Country != null) {
                    urlString.append("&country=");
                    urlString.append(address.Country);
                }
                if (address.StreetName != null) {
                    urlString.append("&streetName=");
                    urlString.append(address.StreetName);
                }
                if (address.StreetNumber != null) {
                    urlString.append("&streetNumber=");
                    urlString.append(address.StreetNumber);
                }
            } else {
                Log.d("DM", "changePOIToEndForDeliveryId: HasAddress parameter is set to TRUE But 'address' parameter is nil");
            }

        }

        if (hasManualTimes == true) {
            urlString.append("&hasManualTimes=");
            urlString.append("1");

            if (isValidParameterString(arrivalTime)) {
                urlString.append("&arrivalTime=");
                urlString.append(arrivalTime);
            }

            if (isValidParameterString(endTime)) {
                urlString.append("&endTime=");
                urlString.append(endTime);
            }
        }

        if (hasComment == true) {
            urlString.append("&hasComment=");
            urlString.append("1");

            if (isValidParameterString(comment)) {
                urlString.append("&comment=");
                urlString.append(comment);
            } else {
                Log.e("DM", "changePOIToEndForDeliveryId: HasComment parameter is set to TRUE But 'comment' parameter is nil");
            }
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for changePOIToEndForDeliveryId: " + object.toString());
                        DeliveryEndPointActionResult result = new DeliveryEndPointActionResult();
                        JSONObject jsonObject = (JSONObject) object;
                        result.readFromJSONObject(jsonObject);
                        listener.changePOIToEndForDeliveryId(result, null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.changePOIToEndForDeliveryId(null, error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for changePOIToEndForDeliveryId" + e.getMessage());
                    listener.changePOIToEndForDeliveryId(null, error);

                }
            }
        });

        restConnection.execute(String.valueOf(urlString));

        Log.v("DM", "ExitFunction: changePOIToEndForDeliveryId");
    }

    public void endRouteHereForDeliveryId(String deliveryId, String poiId, double latitude, double longitude, String locationName,
                                          boolean hasAddress, AddressComponents address, boolean hasComment, String comment, boolean hasManualTimes,
                                          String arrivalTime, String endTime, final OnCompleteListeners.OnEndRouteHereForDeliveryIdListener listener) {

        Log.v("DM", "Enter Function: endRouteHereForDeliveryId");
        final OnCompleteListeners.OnEndRouteHereForDeliveryIdListener EndRouteHere = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/RouteShipment2/EndRouteHere");


        ///try
        if (isValidParameterString(deliveryId)) {
            urlString.append("&deliveryId=");
            urlString.append(deliveryId);
        } else {
            Log.d("DM", "endRouteHereForDeliveryId: DeliveryId is nil, Cannot send request to Server");
        }

        if (isValidParameterString(poiId)) {
            urlString.append("&poiId=");
            urlString.append(poiId);
        } else {
            Log.d("DM", "endRouteHereForDeliveryId: POIId is nil/empty");
        }
        String latitudeStr = String.format("%f", latitude);
        String longitudeStr = String.format("%f", longitude);

        urlString.append("&latitude=");
        urlString.append(latitudeStr);
        urlString.append("&longitude=");
        urlString.append(longitudeStr);

        if (isValidParameterString(locationName)) {
            urlString.append("&locationName=");
            urlString.append(locationName);
        } else {
            Log.d("DM", "endRouteHereForDeliveryId: locationName is nil/empty");
        }
        if (hasAddress == true) {
            if (address != null) {
                urlString.append("&hasAddress=");
                urlString.append("1");

                if (address.Address1 != null) {
                    urlString.append("&address1=");
                    urlString.append(address.Address1);
                }
                if (address.Address2 != null) {
                    urlString.append("&address2=");
                    urlString.append(address.Address2);
                }
                if (address.City != null) {
                    urlString.append("&city=");
                    urlString.append(address.City);
                }
                if (address.Province != null) {
                    urlString.append("&province=");
                    urlString.append(address.Province);
                }
                if (address.Postal != null) {
                    urlString.append("&postal=");
                    urlString.append(address.Postal);
                }
                if (address.Country != null) {
                    urlString.append("&country=");
                    urlString.append(address.Country);
                }
                if (address.StreetName != null) {
                    urlString.append("&streetName=");
                    urlString.append(address.StreetName);
                }
                if (address.StreetNumber != null) {
                    urlString.append("&streetNumber=");
                    urlString.append(address.StreetNumber);
                }
            } else {
                Log.d("DM", "endRouteHereForDeliveryId: HasAddress parameter is set to TRUE But 'address' parameter is nil");
            }

        }
        if (hasManualTimes == true) {
            urlString.append("&hasManualTimes=");
            urlString.append("1");

            if (isValidParameterString(arrivalTime)) {
                urlString.append("&arrivalTime=");
                urlString.append(arrivalTime);
            }

            if (isValidParameterString(endTime)) {
                urlString.append("&endTime=");
                urlString.append(endTime);
            }
        }

        if (hasComment == true) {
            urlString.append("&hasComment=");
            urlString.append("1");

            if (isValidParameterString(comment)) {
                urlString.append("&comment=");
                urlString.append(comment);
            } else {
                Log.d("DM", "endRouteHereForDeliveryId: HasComment parameter is set to TRUE But 'comment' parameter is nil");
            }
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "EndRouteHere", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for endRouteHereForDeliveryId: " + object.toString());
                    DeliveryEndPointActionResult deliveryEndpoIntegerActionResult = new DeliveryEndPointActionResult();
                    deliveryEndpoIntegerActionResult.readFromJSONObject(object);
                    listener.endRouteHereForDeliveryId(deliveryEndpoIntegerActionResult);
                } else {
                    Log.d("DM", "Null object response for endRouteHereForDeliveryId");
                }
            }
        });

        restConnection.execute(String.valueOf(urlString));

        Log.v("DM", "ExitFunction: endRouteHereForDeliveryId");
    }

    public void unsetPOIAsEndPointForDeliveryId(String deliveryId, String poiId, DeliveryPOITypeEnum.DeliveryPOITypesEnum type, boolean atDC, final OnCompleteListeners.OnUnsetPOIAsEndPointForDeliveryIdListener listener) {

        Log.v("DM", "Enter Function: unsetPOIAsEndPointForDeliveryId");
        final OnCompleteListeners.OnUnsetPOIAsEndPointForDeliveryIdListener unsetPOIAsEndPoInteger = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/RouteShipment2/UnsetPOIAsEndpoInteger");

        //try

        if (isValidParameterString(deliveryId)) {
            urlString.append("&deliveryId=");
            urlString.append(deliveryId);
        } else {
            Log.d("DM", "unsetPOIAsEndPointForDeliveryId: DeliveryId is nil, Cannot send request to Server");
        }

        if (isValidParameterString(poiId)) {
            urlString.append("&poiId=");
            urlString.append(poiId);
        } else {
            Log.d("DM", "unsetPOIAsEndPointForDeliveryId: poiId is nil/empty");
        }

        if (Integer.parseInt(type.toString()) >= 0) {
            String TypeStr = String.format("%ld", Long.parseLong(type.toString()));
            urlString.append("&siteType=");
            urlString.append(TypeStr);
        }
        urlString.append("&atDC=");
        urlString.append(atDC ? "true" : "false");


        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for unsetPOIAsEndPointForDeliveryId: " + object.toString());
                        DeliveryEndPointActionResult deliveryEndpoIntegerActionResult = new DeliveryEndPointActionResult();
                        JSONObject jsonObject = (JSONObject) object;
                        deliveryEndpoIntegerActionResult.readFromJSONObject(jsonObject);
                        listener.unsetPOIAsEndPointForDeliveryId(deliveryEndpoIntegerActionResult, null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.unsetPOIAsEndPointForDeliveryId(null, error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for unsetPOIAsEndPointForDeliveryId" + e.getMessage());
                    listener.unsetPOIAsEndPointForDeliveryId(null, error);

                }
            }
        });

        restConnection.execute(String.valueOf(urlString));

        Log.v("DM", "ExitFunction: unsetPOIAsEndPointForDeliveryId");
    }

    public void suggestStopNameForLatitude(double latitude, double longitude, SiteTypeEnum.SiteTypesEnum stopType, final OnCompleteListeners.SuggestStopNameForLatitudeListener listener) {

        Log.v("DM", "Enter Function: suggestStopNameForLatitude");
        final OnCompleteListeners.SuggestStopNameForLatitudeListener suggestStopNameForLatitude = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/RouteShipment2/SuggestStopName");

        StringBuilder parameterString = new StringBuilder();
        //try
        String latitudeStr = String.format("%f", latitude);
        String longitudeStr = String.format("%f", longitude);

        parameterString.append("lat=");
        parameterString.append(latitudeStr);
        parameterString.append("&lon=");
        parameterString.append(longitudeStr);
        switch (stopType) {
            case SiteType_Inspection:
                parameterString.append("&stopType=");
                parameterString.append("inspection");
                break;
            case SiteType_Fuel:
                parameterString.append("&stopType=");
                parameterString.append("fuel");
            case SiteType_Service:
                parameterString.append("&stopType=");
                parameterString.append("service");
            case SiteType_Rest:
                parameterString.append("&stopType=");
                parameterString.append("rest");

            default:
                Log.d("DM", "suggestStopNameForLatitude:Invalid StopType");
                break;
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initializePOSTRequest(this.authHeaderValue, parameterString.toString(), new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for suggestStopNameForLatitude: " + object.toString());
                        SuggestStopNameResult result = new SuggestStopNameResult();
                        JSONObject jsonObject = (JSONObject) object;
                        result.readFromJSONObject(jsonObject);
                        listener.suggestStopNameForLatitude(result, null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.suggestStopNameForLatitude(null, error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for setDeliveryConfirmationTimeForDeliveryId" + e.getMessage());
                    listener.suggestStopNameForLatitude(null, error);

                }
            }
        });

        restConnection.execute(String.valueOf(urlString));

        Log.v("DM", "ExitFunction: suggestStopNameForLatitude");
    }


    public void endOfRouteForDeliveryId(String deliveryId, double latitude, double longitude, String locationName, boolean hasAddress,
                                        AddressComponents address, boolean hasComment, String comment, boolean hasManualTimes,
                                        String arrivalTime, String endTime, final OnCompleteListeners.EndOfRouteForDeliveryIdListener listener) {

        Log.v("DM", "Enter Function: endOfRouteForDeliveryId");
        final OnCompleteListeners.EndOfRouteForDeliveryIdListener EndOfRoute = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/RouteShipment2/EndOfRoute");

        /* //try */

        if (isValidParameterString(deliveryId)) {
            urlString.append("deliveryId=");
            urlString.append(deliveryId);
        } else {
            Log.v("DM", "endOfRouteForDeliveryId: DeliveryId is nil, Cannot send request to Server");
        }

        String latitudeStr = String.format("%f", latitude);
        String longitudeStr = String.format("%f", longitude);
        urlString.append("&latitude=");
        urlString.append(latitudeStr);
        urlString.append("&longitude=");
        urlString.append(longitudeStr);

        if (isValidParameterString(locationName)) {
            urlString.append("&locationName=");
            urlString.append(locationName);
        } else {
            Log.d("DM", "endOfRouteForDeliveryId: locationName is nil/empty");
        }
        if (hasAddress == true) {
            if (address != null) {
                urlString.append("&hasAddress=");
                urlString.append("1");

                if (address.Address1 != null) {
                    urlString.append("&address1=");
                    urlString.append(address.Address1);
                }
                if (address.Address2 != null) {
                    urlString.append("&address2=");
                    urlString.append(address.Address2);
                }
                if (address.City != null) {
                    urlString.append("&city=");
                    urlString.append(address.City);
                }
                if (address.Province != null) {
                    urlString.append("&province=");
                    urlString.append(address.Province);
                }
                if (address.Postal != null) {
                    urlString.append("&postal=");
                    urlString.append(address.Postal);
                }
                if (address.Country != null) {
                    urlString.append("&country=");
                    urlString.append(address.Country);
                }
                if (address.StreetName != null) {
                    urlString.append("&streetName=");
                    urlString.append(address.StreetName);
                }
                if (address.StreetNumber != null) {
                    urlString.append("&streetNumber=");
                    urlString.append(address.StreetNumber);
                }
            } else {
                Log.d("DM", "endOfRouteForDeliveryId: HasAddress parameter is set to TRUE But 'address' parameter is nil");
            }

        }
        if (hasManualTimes == true) {
            urlString.append("&hasManualTimes=");
            urlString.append("1");

            if (isValidParameterString(arrivalTime)) {
                urlString.append("&arrivalTime=");
                urlString.append(arrivalTime);
            }

            if (isValidParameterString(endTime)) {
                urlString.append("&endTime=");
                urlString.append(endTime);
            }
        }

        if (hasComment == true) {
            urlString.append("&hasComment=");
            urlString.append("1");

            if (isValidParameterString(comment)) {
                urlString.append("&comment=");
                urlString.append(comment);
            } else {
                Log.d("DM", "endOfRouteForDeliveryId: HasComment parameter is set to TRUE But 'comment' parameter is nil");
            }
        }


        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "EndOfRoute", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for endOfRouteForDeliveryId: " + object.toString());
                    DeliveryEndPointActionResult deliveryEndpoIntegerActionResult = new DeliveryEndPointActionResult();
                    deliveryEndpoIntegerActionResult.readFromJSONObject(object);
                    listener.endOfRouteForDeliveryId(deliveryEndpoIntegerActionResult);
                } else {
                    Log.d("DM", "Null object response for endOfRouteForDeliveryId");
                }
            }
        });

        restConnection.execute(String.valueOf(urlString));

        Log.v("DM", "ExitFunction: endOfRouteForDeliveryId");
    }


    public void startOfRouteForDeliveryId(String deliveryId, double latitude, double longitude, String locationName, boolean hasAddress,
                                          AddressComponents address, boolean hasComment, String comment, boolean hasManualTimes,
                                          String arrivalTime, String endTime, final OnCompleteListeners.StartOfRouteForDeliveryIdListener listener) {

        Log.v("DM", "Enter Function: startOfRouteForDeliveryId");
        final OnCompleteListeners.StartOfRouteForDeliveryIdListener StartOfRoute = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/RouteShipment2/StartOfRoute");

        /* //try */

        if (isValidParameterString(deliveryId)) {
            urlString.append("?deliveryId=");
            urlString.append(deliveryId);
        } else {
            Log.v("DM", "startOfRouteForDeliveryId: DeliveryId is nil, Cannot send request to Server");
        }

        String latitudeStr = String.format("%f", latitude);
        String longitudeStr = String.format("%f", longitude);
        urlString.append("&latitude=");
        urlString.append(latitudeStr);
        urlString.append("&longitude=");
        urlString.append(longitudeStr);

        if (isValidParameterString(locationName)) {
            urlString.append("&locationName=");
            urlString.append(locationName);
        } else {
            Log.d("DM", "startOfRouteForDeliveryId: locationName is nil/empty");
        }
        if (hasAddress == true) {
            if (address != null) {
                urlString.append("&hasAddress=");
                urlString.append("1");

                if (address.Address1 != null) {
                    urlString.append("&address1=");
                    urlString.append(address.Address1);
                }
                if (address.Address2 != null) {
                    urlString.append("&address2=");
                    urlString.append(address.Address2);
                }
                if (address.City != null) {
                    urlString.append("&city=");
                    urlString.append(address.City);
                }
                if (address.Province != null) {
                    urlString.append("&province=");
                    urlString.append(address.Province);
                }
                if (address.Postal != null) {
                    urlString.append("&postal=");
                    urlString.append(address.Postal);
                }
                if (address.Country != null) {
                    urlString.append("&country=");
                    urlString.append(address.Country);
                }
                if (address.StreetName != null) {
                    urlString.append("&streetName=");
                    urlString.append(address.StreetName);
                }
                if (address.StreetNumber != null) {
                    urlString.append("&streetNumber=");
                    urlString.append(address.StreetNumber);
                }
            } else {
                Log.d("DM", "startOfRouteForDeliveryId: HasAddress parameter is set to TRUE But 'address' parameter is nil");
            }

        }
        if (hasManualTimes == true) {
            urlString.append("&hasManualTimes=");
            urlString.append("1");

            if (isValidParameterString(arrivalTime)) {
                urlString.append("&arrivalTime=");
                urlString.append(arrivalTime);
            }

            if (isValidParameterString(endTime)) {
                urlString.append("&endTime=");
                urlString.append(endTime);
            }
        }

        if (hasComment == true) {
            urlString.append("&hasComment=");
            urlString.append("1");

            if (isValidParameterString(comment)) {
                urlString.append("&comment=");
                urlString.append(comment);
            } else {
                Log.d("DM", "startOfRouteForDeliveryId: HasComment parameter is set to TRUE But 'comment' parameter is nil");
            }
        }


        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "StartOfRoute", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for startOfRouteForDeliveryId: " + object.toString());
                    DeliveryEndPointActionResult deliveryEndpoIntegerActionResult = new DeliveryEndPointActionResult();
                    deliveryEndpoIntegerActionResult.readFromJSONObject(object);
                    listener.startOfRouteForDeliveryId(deliveryEndpoIntegerActionResult);
                } else {
                    Log.d("DM", "Null object response for startOfRouteForDeliveryId");
                }
            }
        });

        restConnection.execute(String.valueOf(urlString));

        Log.v("DM", "ExitFunction: startOfRouteForDeliveryId");
    }

    public void setManualStopTimesForDeliveryId(String deliveryId, String deliveryStopId, String arrivalTime,
                                                String departureTime, String deliveryStartTime, String deliveryEndTime, final OnCompleteListeners.setManualStopTimesForDeliveryIdListener listener) {

        Log.v("DM", "Enter Function: setManualStopTimesForDeliveryId");
        final OnCompleteListeners.setManualStopTimesForDeliveryIdListener setManualStopTime = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/RouteShipment2/SetManualStopTimes");

        if (isValidParameterString(deliveryId)) {
            urlString.append("?deliveryId=");
            urlString.append(deliveryId);
        } else {
            Log.e("DM", "setManualStopTimesForDeliveryId: DeliveryId is nil, Cannot send request to Server");
        }

        if (isValidParameterString(deliveryStopId)) {
            urlString.append("&deliveryStopId=");
            urlString.append(deliveryStopId);
        } else {
            Log.e("DM", "setManualStopTimesForDeliveryId: deliveryStopId is nil, Cannot send request to Server");
        }

        if (arrivalTime != null) {
            //NSString* arrivalTimeStr = [NSDate getISOFormatDate:arrivalTime];
            urlString.append("&arrivalTime=");
            urlString.append(arrivalTime);
        } else {
            Log.e("DM", "setManualStopTimesForDeliveryId: ArrivalTime is nil, Cannot send request to Server\"");
        }
        if (departureTime != null) {
            //  NSString* departureTimeStr = [NSDate getISOFormatDate:departureTime];
            urlString.append("&departureTime=");
            urlString.append(departureTime);
        } else {
            Log.e("DM", "setManualStopTimesForDeliveryId: departureTime is nil, Cannot send request to Server");
        }
        if (deliveryStartTime != null) {
            //NSString* deliveryStartTimeStr = [NSDate getISOFormatDate:deliveryStartTime];
            urlString.append("&deliveryStart=");
            urlString.append(deliveryStartTime);
        } else {
            Log.e("DM", "setManualStopTimesForDeliveryId: deliveryStartTime is nil, Cannot send request to Server");
        }
        if (deliveryEndTime != null) {
            //  NSString* deliveryEndTimeStr = [NSDate getISOFormatDate:deliveryEndTime];
            urlString.append("&deliveryEnd=");
            urlString.append(deliveryEndTime);
        } else {
            Log.e("DM", "setManualStopTimesForDeliveryId: deliveryEndTime is nil, Cannot send request to Server");
        }


        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "setManualStopTime", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for setManualStopTimesForDeliveryId: " + object.toString());
                    DeliveryStopActionResult result = new DeliveryStopActionResult();
                    result.readFromJSONObject(object);
                    listener.setManualStopTimesForDeliveryId(result);
                } else {
                    Log.d("DM", "Null object response for setManualStopTimesForDeliveryId");
                }
            }
        });

        restConnection.execute(String.valueOf(urlString));

        Log.v("DM", "ExitFunction: setManualStopTimesForDeliveryId");
    }


    public void setManualArriveAndDepartForDeliveryId(String deliveryId, String deliveryStopId, String arrivalTime,
                                                      String departureTime, final OnCompleteListeners.setManualArriveAndDepartForDeliveryIdListener listener) {

        Log.v("DM", "Enter Function: setManualArriveAndDepartForDeliveryId");
        final OnCompleteListeners.setManualArriveAndDepartForDeliveryIdListener setManualArriveAndDepart = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/RouteShipment2/SetManualArriveAndDepart");

        if (isValidParameterString(deliveryId)) {
            urlString.append("?deliveryId=");
            urlString.append(deliveryId);
        } else {
            Log.e("DM", "setManualArriveAndDepartForDeliveryId: DeliveryId is nil, Cannot send request to Server");
        }

        if (isValidParameterString(deliveryStopId)) {
            urlString.append("&deliveryStopId=");
            urlString.append(deliveryStopId);
        } else {
            Log.e("DM", "setManualArriveAndDepartForDeliveryId: deliveryStopId is nil, Cannot send request to Server");
        }

        if (arrivalTime != null) {
            //NSString* arrivalTimeStr = [NSDate getISOFormatDate:arrivalTime];
            urlString.append("&arrivalTime=");
            urlString.append(arrivalTime);
        } else {
            Log.e("DM", "setManualArriveAndDepartForDeliveryId: ArrivalTime is nil, Cannot send request to Server\"");
        }
        if (departureTime != null) {
            //  NSString* departureTimeStr = [NSDate getISOFormatDate:departureTime];
            urlString.append("&departureTime=");
            urlString.append(departureTime);
        } else {
            Log.e("DM", "setManualArriveAndDepartForDeliveryId: departureTime is nil, Cannot send request to Server");
        }


        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "setManualArriveAndDepart", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for setManualArriveAndDepartForDeliveryId: " + object.toString());
                    DeliveryStopActionResult result = new DeliveryStopActionResult();
                    result.readFromJSONObject(object);
                    listener.setManualArriveAndDepartForDeliveryId(result);
                } else {
                    Log.d("DM", "Null object response for setManualArriveAndDepartForDeliveryId");
                }
            }
        });

        restConnection.execute(String.valueOf(urlString));

        Log.v("DM", "ExitFunction: setManualArriveAndDepartForDeliveryId");
    }

    public void setManualDeliveryTimesForDeliveryId(String deliveryId, String deliveryStopId, String deliveryStartTime,
                                                    String deliveryEndTime, String deliveryConfirmationTime, String deliveryOdometer, final OnCompleteListeners.setManualDeliveryTimesForDeliveryIdListener listener) {

        Log.v("DM", "Enter Function: setManualDeliveryTimesForDeliveryId");
        final OnCompleteListeners.setManualDeliveryTimesForDeliveryIdListener setManualDeliveryTimes = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/RouteShipment2/SetManualDeliveryTimes");

        if (isValidParameterString(deliveryId)) {
            urlString.append("?deliveryId=");
            urlString.append(deliveryId);
        } else {
            Log.e("DM", "setManualDeliveryTimesForDeliveryId: DeliveryId is nil, Cannot send request to Server");
        }

        if (isValidParameterString(deliveryStopId)) {
            urlString.append("&deliveryStopId=");
            urlString.append(deliveryStopId);
        } else {
            Log.e("DM", "setManualDeliveryTimesForDeliveryId: deliveryStopId is nil, Cannot send request to Server");
        }

        if (deliveryStartTime != null) {
            //NSString* deliveryStartTimeStr = [NSDate getISOFormatDate:deliveryStartTime];
            urlString.append("&deliveryStart=");
            urlString.append(deliveryStartTime);
        } else {
            Log.e("DM", "setManualDeliveryTimesForDeliveryId: deliveryStartTime is nil, Cannot send request to Server\"");
        }
        if (deliveryEndTime != null) {
            //  NSString* deliveryEndTimeStr = [NSDate getISOFormatDate:deliveryEndTime];
            urlString.append("&deliveryEnd=");
            urlString.append(deliveryEndTime);
        } else {
            Log.e("DM", "setManualDeliveryTimesForDeliveryId: deliveryEndTime is nil, Cannot send request to Server");
        }
        if (deliveryConfirmationTime != null) {
            //  String deliveryConfirmationTimeStr = [NSDate getISOFormatDate:deliveryConfirmationTime];
            urlString.append("&deliveryConfirmationTime=");
            urlString.append(deliveryConfirmationTime);
        } else {
            Log.e("DM", "setManualDeliveryTimesForDeliveryId: deliveryConfirmationTime is nil, Cannot send request to Server");
        }

        if (deliveryOdometer != null) {
            urlString.append("&deliveryOdometer=");
            urlString.append(String.format("%.2f", deliveryOdometer));
        }


        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "setManualDeliveryTimes", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for setManualDeliveryTimesForDeliveryId: " + object.toString());
                    DeliveryStopActionResult result = new DeliveryStopActionResult();
                    result.readFromJSONObject(object);
                    listener.setManualDeliveryTimesForDeliveryId(result);
                } else {
                    Log.d("DM", "Null object response for setManualDeliveryTimesForDeliveryId");
                }
            }
        });

        restConnection.execute(String.valueOf(urlString));

        Log.v("DM", "ExitFunction: setManualDeliveryTimesForDeliveryId");
    }

    public void setDeliveryConfirmationTimeForDeliveryId(String deliveryId, String deliveryStopId, Date deliveryConfirmationTime,
                                                         boolean isRecovery, final OnCompleteListeners.setDeliveryConfirmationTimeForDeliveryIdListener listener) {

        Log.v("DM", "Enter Function: setDeliveryConfirmationTimeForDeliveryId");
        final OnCompleteListeners.setDeliveryConfirmationTimeForDeliveryIdListener setDeliveryConfirmationTime = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/RouteShipment2/SetDeliveryConfirmationTime");

        StringBuilder parameterString = new StringBuilder();

        if (isValidParameterString(deliveryId)) {
            parameterString.append("?deliveryId=");
            parameterString.append(deliveryId);
        } else {
            Log.e("DM", "setDeliveryConfirmationTimeForDeliveryId: DeliveryId is nil, Cannot send request to Server");
        }

        if (isValidParameterString(deliveryStopId)) {
            parameterString.append("&deliveryStopId=");
            parameterString.append(deliveryStopId);
        } else {
            Log.e("DM", "setDeliveryConfirmationTimeForDeliveryId: deliveryStopId is nil, Cannot send request to Server");
        }


        if (deliveryConfirmationTime != null) {
            //  String deliveryConfirmationTimeStr = [NSDate getISOFormatDate:deliveryConfirmationTime];
            parameterString.append("&deliveryConfirmationTime=");
            parameterString.append(getUTCFormatDate(deliveryConfirmationTime));
        } else {
            Log.e("DM", "setDeliveryConfirmationTimeForDeliveryId: deliveryConfirmationTime is nil, Cannot send request to Server");
        }

        if (isRecovery) {
            parameterString.append("&isRecovery=");
            //urlString.append("1");
            parameterString.append(true);

        } else {
            parameterString.append("&isRecovery=");
            //urlString.append("0");
            parameterString.append(false);
        }
        Log.v("DM", "Create Form  API Parameters:: " + parameterString);


        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initializePOSTRequest(this.authHeaderValue, parameterString.toString(), new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for setDeliveryConfirmationTimeForDeliveryId: " + object.toString());
                        DeliveryStopActionResult result = new DeliveryStopActionResult();
                        JSONObject jsonObject = (JSONObject) object;
                        result.readFromJSONObject(jsonObject);
                        listener.setDeliveryConfirmationTimeForDeliveryId(result, null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.setDeliveryConfirmationTimeForDeliveryId(null, error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for setDeliveryConfirmationTimeForDeliveryId" + e.getMessage());
                    listener.setDeliveryConfirmationTimeForDeliveryId(null, error);

                }
            }
        });

        restConnection.execute(String.valueOf(urlString));

        Log.v("DM", "ExitFunction: setDeliveryConfirmationTimeForDeliveryId");
    }

//    public void setStopOdometerforDeliveryId(String deliveryId, String deliveryStopId, String deliveryOdometer, final OnCompleteListeners.setStopOdometerforDeliveryIdListener listener ) {
//
//        Log.v("DM", "Enter Function: setStopOdometerforDeliveryId");
//        final OnCompleteListeners.setStopOdometerforDeliveryIdListener setStopOdometer = listener;
//
//        StringBuilder urlString = new StringBuilder();
//        urlString.append(this.baseUrl);
//        urlString.append("StoreDelivery/RouteShipment2/SetStopOdometer");
//
//        if(isValidParameterString(deliveryId)){
//            urlString.append("?deliveryId=");
//            urlString.append(deliveryId);
//        }else{
//            Log.e("DM", "setStopOdometerforDeliveryId: DeliveryId is nil, Cannot send request to Server");
//        }
//
//        if(isValidParameterString(deliveryStopId)){
//            urlString.append("&deliveryStopId=");
//            urlString.append(deliveryStopId);
//        }else{
//            Log.e("DM", "setStopOdometerforDeliveryId: deliveryStopId is nil, Cannot send request to Server");
//        }
//
//        if(deliveryOdometer != null ) {
//            urlString.append("&deliveryOdometer=");
//            urlString.append(String.format("%.2f", deliveryOdometer));
//        }
//
//
//        Log.v("DM", "Request URL: " + urlString);
//
//        RestConnection restConnection = new RestConnection();
//        restConnection.initialize(this.authHeaderValue, "SetStopOdometer", new OnCompleteListeners.OnTaskCompleteListener() {
//            @Override
//            public void onTaskComplete(JSONObject object) {
//                if (object != null){
//                    Log.v("DM", "Received JSON Object for setStopOdometerforDeliveryId: " + object.toString());
//                    DeliveryStopActionResult result = new DeliveryStopActionResult();
//                    result.readFromJSONObject(object);
//                    listener.setStopOdometerforDeliveryId(result);
//                } else {
//                    Log.d("DM","Null object response for setStopOdometerforDeliveryId");
//                }
//            }
//        });
//
//        restConnection.execute(String.valueOf(urlString));
//
//        Log.v("DM", "ExitFunction: setStopOdometerforDeliveryId");
//    }

    public void setManualDeliveryStartForDeliveryId(String deliveryId, String deliveryStopId, Date deliveryStart, final OnCompleteListeners.setManualDeliveryStartForDeliveryIdListener listener) {

        Log.v("DM", "Enter Function: setManualDeliveryStartForDeliveryId");
        final OnCompleteListeners.setManualDeliveryStartForDeliveryIdListener setManualDeliveryStart = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/RouteShipment2/SetManualDeliveryStart");

        StringBuilder parameterString = new StringBuilder();

        if (isValidParameterString(deliveryId)) {
            parameterString.append("?deliveryId=");
            parameterString.append(deliveryId);
        } else {
            Log.e("DM", "setManualDeliveryStartForDeliveryId: DeliveryId is nil, Cannot send request to Server");
        }

        if (isValidParameterString(deliveryStopId)) {
            parameterString.append("&deliveryStopId=");
            parameterString.append(deliveryStopId);
        } else {
            Log.e("DM", "setManualDeliveryStartForDeliveryId: deliveryStopId is nil, Cannot send request to Server");
        }

        if (deliveryStart != null) {
            // NSString* deliveryStartTimeStr = [NSDate getISOFormatDate:deliveryStart];
            parameterString.append("?deliveryStart=");
            parameterString.append(getUTCFormatDate(deliveryStart));
        } else {
            Log.e("DM", "setManualDeliveryStartForDeliveryId: deliveryStart is nil, Cannot send request to Server");
        }
        Log.v("DM", "Create Form  API Parameters: " + parameterString);

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initializePOSTRequest(this.authHeaderValue, parameterString.toString(), new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for setManualDeliveryStartForDeliveryId: " + object.toString());
                        DeliveryStopActionResult result = new DeliveryStopActionResult();
                        JSONObject jsonObject = (JSONObject) object;
                        result.readFromJSONObject(jsonObject);
                        listener.setManualDeliveryStartForDeliveryId(result, null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.setManualDeliveryStartForDeliveryId(null, error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for setManualDeliveryStartForDeliveryId" + e.getMessage());
                    listener.setManualDeliveryStartForDeliveryId(null, error);

                }
            }

        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: setManualDeliveryStartForDeliveryId");
    }

    public void setManualDeliveryEndForDeliveryId(String deliveryId, String deliveryStopId, Date deliveryEnd, final OnCompleteListeners.setManualDeliveryEndForDeliveryIdListener listener) {

        Log.v("DM", "Enter Function: setManualDeliveryEndForDeliveryId");
        final OnCompleteListeners.setManualDeliveryEndForDeliveryIdListener setManualDeliveryEnd = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/RouteShipment2/SetManualDeliveryEnd");

        StringBuilder parameterString = new StringBuilder();

        if (isValidParameterString(deliveryId)) {
            parameterString.append("deliveryId=");
            parameterString.append(deliveryId);
        } else {
            Log.e("DM", "setManualDeliveryEndForDeliveryId: DeliveryId is nil, Cannot send request to Server");
        }

        if (isValidParameterString(deliveryStopId)) {
            parameterString.append("&deliveryStopId=");
            parameterString.append(deliveryStopId);
        } else {
            Log.e("DM", "setManualDeliveryEndForDeliveryId: deliveryStopId is nil, Cannot send request to Server");
        }

        if (deliveryEnd != null) {
            // NSString* deliveryEndTimeStr = [NSDate getISOFormatDate:deliveryEnd];
            parameterString.append("?deliveryEnd=");
            parameterString.append(getUTCFormatDate(deliveryEnd));
        } else {
            Log.e("DM", "setManualDeliveryEndForDeliveryId: deliveryEnd is nil, Cannot send request to Server");
        }
        Log.v("DM", "Create Form  API Parameters: " + parameterString);

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initializePOSTRequest(this.authHeaderValue, parameterString.toString(), new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for setManualDeliveryEndForDeliveryId: " + object.toString());
                        DeliveryStopActionResult result = new DeliveryStopActionResult();
                        JSONObject jsonObject = (JSONObject) object;
                        result.readFromJSONObject(jsonObject);
                        listener.setManualDeliveryEndForDeliveryId(result, null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.setManualDeliveryEndForDeliveryId(null, error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for setManualDeliveryEndForDeliveryId" + e.getMessage());
                    listener.setManualDeliveryEndForDeliveryId(null, error);

                }
            }
        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: setManualDeliveryEndForDeliveryId");
    }

    public void setStopOdometerforDeliveryId(String deliveryId, String deliveryStopId, double deliveryOdometer, final OnCompleteListeners.setStopOdometerforDeliveryIdListener listener) {
        Log.v("DM", "Enter Function: setStopOdometerforDeliveryId");

        final OnCompleteListeners.setStopOdometerforDeliveryIdListener setStopOdometer = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/RouteShipment2/SetStopOdometer");

        StringBuilder parameterString = new StringBuilder();

        if (isValidParameterString(deliveryId)) {
            parameterString.append("?deliveryId=");
            parameterString.append(deliveryId);
        } else {
            Log.e("DM", "setStopOdometerforDeliveryId: DeliveryId is nil, Cannot send request to Server");
        }

        if (isValidParameterString(deliveryStopId)) {
            parameterString.append("&deliveryStopId=");
            parameterString.append(deliveryStopId);
        } else {
            Log.e("DM", "setStopOdometerforDeliveryId: deliveryStopId is nil, Cannot send request to Server");
        }

        if (deliveryOdometer > 0) {
            parameterString.append("&deliveryOdometer=");
            parameterString.append(String.format("%.2f", deliveryOdometer));
        } else {
            Log.e("DM", "setStopOdometerforDeliveryId: deliveryOdometer is nil, Cannot send request to Server");
        }

        Log.v("DM", "Create Form  API Parameters: " + parameterString);

        Log.v("DM", "Request URL: " + urlString);
        Log.v("DM", "Create Form  API Parameters: " + parameterString);

        RestConnection restConnection = new RestConnection();
        restConnection.initializePOSTRequest(this.authHeaderValue, parameterString.toString(), new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for setManualDeliveryEndForDeliveryId: " + object.toString());
                        DeliveryStopActionResult result = new DeliveryStopActionResult();
                        JSONObject jsonObject = (JSONObject) object;
                        result.readFromJSONObject(jsonObject);
                        listener.setStopOdometerforDeliveryId(result, null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.setStopOdometerforDeliveryId(null, error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for setStopOdometerforDeliveryId" + e.getMessage());
                    listener.setStopOdometerforDeliveryId(null, error);

                }
            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: setStopOdometerforDeliveryId");

    }

    public void setManualStartAndEndForDeliveryId(String deliveryId, boolean hasStartTime, Date manualStartTime,
                                                  boolean hasDepartureTime, Date manualDepartureTime, boolean hasEndTime, Date manualEndTime, final OnCompleteListeners.setManualStartAndEndForDeliveryIdListener listener) {

        Log.v("DM", "Enter Function: setManualStartAndEndForDeliveryId");
        final OnCompleteListeners.setManualStartAndEndForDeliveryIdListener ManualStartAndEnd = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/RouteShipment2/ManualStartAndEnd");

        if (isValidParameterString(deliveryId)) {
            urlString.append("?deliveryId=");
            urlString.append(deliveryId);
        } else {
            Log.e("DM", "setManualStartAndEndForDeliveryId: DeliveryId is nil, Cannot send request to Server");
        }

        if (hasStartTime) {
            urlString.append("&hasStartTime=true");
            //  NSString* manualStartTimeStr = [NSDate getISOFormatDate:manualStartTime];
            urlString.append("&manualStartTime=");
            urlString.append(manualStartTime);
        } else {
            urlString.append("&hasStartTime=false");
        }
        if (hasEndTime) {
            urlString.append("&hasEndTime=true");
            //  NSString* manualEndTimeStr = [NSDate getISOFormatDate:manualEndTime];
            urlString.append("&manualEndTime=");
            urlString.append(manualEndTime);
        } else {
            urlString.append("&hasEndTime=false");
        }
        if (hasDepartureTime) {
            urlString.append("&hasDepartureTime=true");
            //  NSString* manualEndTimeStr = [NSDate getISOFormatDate:manualEndTime];
            urlString.append("&manualDepartureTime=");
            urlString.append(manualDepartureTime);
        } else {
            urlString.append("&hasDepartureTime=false");
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "SetManualDeliveryEnd", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for setManualDeliveryEndForDeliveryId: " + object.toString());
                    DeliveryActionResult result = new DeliveryActionResult();
                    result.readFromJSONObject(object);
                    listener.setManualStartAndEndForDeliveryId(result);
                } else {
                    Log.d("DM", "Null object response for setManualDeliveryEndForDeliveryId");
                }
            }
        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: setManualDeliveryEndForDeliveryId");
    }

    public void createDCReturnForDeliveryID(String deliveryId, Date dcArrivalTime, Date dcDepartureTime, SdrDCReturn.DeliveryDCReturnType deliveryDCReturnType, final OnCompleteListeners.createDCReturnForDeliveryIDListener listener) {

        Log.v("DM", "Enter Function: createDCReturnForDeliveryID");
        final OnCompleteListeners.createDCReturnForDeliveryIDListener createDCReturn = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/DCReturn/Create");

        if (isValidParameterString(deliveryId)) {
            urlString.append("?deliveryId=");
            urlString.append(deliveryId);
        } else {
            Log.e("DM", "createDCReturnForDeliveryID: DeliveryId is nil, Cannot send request to Server");
        }

        if (dcArrivalTime != null) {
            //  NSString* dcArrivalTimeStr = [NSDate getISOFormatDate:dcArrivalTime];
            urlString.append("&dcArrivalTime=");
            urlString.append(dcArrivalTime);
        } else {
            Log.e("DM", "createDCReturnForDeliveryID: dcArrivalTime is nil, Cannot send request to Server");
        }
        if (dcDepartureTime != null) {
            //  NSString* dcDepartureTimeStr = [NSDate getISOFormatDate:dcDepartureTime];
            urlString.append("&dcDepartureTime=");
            urlString.append(dcDepartureTime);
        } else {
            Log.e("DM", "createDCReturnForDeliveryID: dcDepartureTime is nil, Cannot send request to Server");
        }

        String typeStr = String.format("%ld", Long.parseLong(deliveryDCReturnType.toString()));
        urlString.append("&eventType=");
        urlString.append(typeStr);

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for createDCReturnForDeliveryID: " + object.toString());
                        DCReturnActionResult result = new DCReturnActionResult();
                        JSONObject jsonObject = (JSONObject) object;
                        result.readFromJSONObject(jsonObject);
                        listener.createDCReturnForDeliveryID(result, null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.createDCReturnForDeliveryID(null, error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for createDCReturnForDeliveryID" + e.getMessage());
                    listener.createDCReturnForDeliveryID(null, error);

                }

            }
        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: createDCReturnForDeliveryID");
    }

    public void updateDCReturnForDeliveryID(String deliveryId, String dcReturnId, Date dcDepartureTime, final OnCompleteListeners.OnUpdateDCReturnForDeliveryIDListener listener) {

        Log.v("DM", "Enter Function: updateDCReturnForDeliveryID");
        final OnCompleteListeners.OnUpdateDCReturnForDeliveryIDListener updateDCReturn = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/DCReturn/Update");

        if (isValidParameterString(deliveryId)) {
            urlString.append("?deliveryId=");
            urlString.append(deliveryId);
        } else {
            Log.e("DM", "updateDCReturnForDeliveryID: DeliveryId is nil, Cannot send request to Server");
        }

        if (isValidParameterString(dcReturnId)) {
            urlString.append("&dcReturnId=");
            urlString.append(dcReturnId);
        } else {
            Log.e("DM", "updateDCReturnForDeliveryID: dcReturnId is nil, Cannot send request to Server");
        }
        if (dcDepartureTime != null) {
            //  NSString* dcDepartureTimeStr = [NSDate getISOFormatDate:dcDepartureTime];
            urlString.append("&dcDepartureTime=");
            urlString.append(getUTCFormatDate(dcDepartureTime));
        } else {
            Log.e("DM", "updateDCReturnForDeliveryID: dcDepartureTime is nil, Cannot send request to Server");
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for updateDCReturnForDeliveryID: " + object.toString());
                        DCReturnActionResult result = new DCReturnActionResult();
                        JSONObject jsonObject = (JSONObject) object;
                        result.readFromJSONObject(jsonObject);
                        listener.updateDCReturnForDeliveryID(result, null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.updateDCReturnForDeliveryID(null, error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for updateDCReturnForDeliveryID" + e.getMessage());
                    listener.updateDCReturnForDeliveryID(null, error);

                }
            }
        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: updateDCReturnForDeliveryID");
    }

    public void pushGPSEventForStopWithDeliveryId(String deliveryId, String deliveryStopId, String serialNumber, String deviceType,
                                                  Date timestamp, double latitude, double longitude, GPSEventTypeEnum.GPSEventTypesEnum type, final OnCompleteListeners.OnPushGPSEventForStopWithDeliveryIdListener listener) {

        Log.v("DM", "Enter Function: pushGPSEventForStopWithDeliveryId");
        final OnCompleteListeners.OnPushGPSEventForStopWithDeliveryIdListener pushGPSEventForStop = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/RouteShipment2/PushGPSEventForStop");

        if (isValidParameterString(deliveryId)) {
            urlString.append("?deliveryId=");
            urlString.append(deliveryId);
        } else {
            Log.e("DM", "pushGPSEventForStopWithDeliveryId: DeliveryId is nil, Cannot send request to Server");
        }

        if (isValidParameterString(serialNumber)) {
            urlString.append("&serialNumber=");
            urlString.append(serialNumber);
        } else {
            Log.e("DM", "pushGPSEventForStopWithDeliveryId: serialNumber is nil, Cannot send request to Server");
        }
        if (isValidParameterString(deliveryStopId)) {
            urlString.append("&deliveryStopId=");
            urlString.append(deliveryStopId);
        } else {
            Log.e("DM", "pushGPSEventForStopWithDeliveryId: deliveryStopId is nil, Cannot send request to Server");
        }
        if (isValidParameterString(deviceType)) {
            urlString.append("&deviceType=");
            urlString.append(deviceType);
        } else {
            Log.e("DM", "pushGPSEventForStopWithDeliveryId: deviceType is nil, Cannot send request to Server");
        }
        if (timestamp != null) {
            //  NSString* timestampStr = [NSDate getISOFormatDate:timestamp];
            urlString.append("&timestamp=");
            urlString.append(timestamp);
        } else {
            Log.e("DM", "updateDCReturnForDeliveryID: timestamp is nil, Cannot send request to Server");
        }
        String latitudeStr = String.format("%f", latitude);
        String longitudeStr = String.format("%f", longitude);

        urlString.append("&lat=");
        urlString.append(latitudeStr);
        urlString.append("&lon=");
        urlString.append(longitudeStr);

        if (Integer.parseInt(type.toString()) >= 0) {
            urlString.append("&type=");
            urlString.append(String.format("%d", Integer.parseInt(type.toString())));
        } else {
            Log.e("DM", "pushGPSEventForStopWithDeliveryId: Invalid GPSEventType value, Cannot send request to Server");
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for pushGPSEventForStopWithDeliveryId: " + object.toString());
                        DeliveryStopActionResult result = new DeliveryStopActionResult();
                        JSONObject jsonObject = (JSONObject) object;
                        result.readFromJSONObject(jsonObject);
                        listener.pushGPSEventForStopWithDeliveryId(result, null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.pushGPSEventForStopWithDeliveryId(null, error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for pushGPSEventForStopsWithDeliveryId" + e.getMessage());
                    listener.pushGPSEventForStopWithDeliveryId(null, error);

                }

            }
        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: pushGPSEventForStopWithDeliveryId");
    }

    public void pushGPSEventForSiteWithDeliveryId(String deliveryId, String siteId, String serialNumber, String deviceType,
                                                  Date timestamp, double latitude, double longitude, GPSEventTypeEnum.GPSEventTypesEnum type, final OnCompleteListeners.OnPushGPSEventForSiteWithDeliveryIdListener listener) {

        Log.v("DM", "Enter Function: pushGPSEventForSiteWithDeliveryId");
        final OnCompleteListeners.OnPushGPSEventForSiteWithDeliveryIdListener pushGPSEventForSite = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/RouteShipment2/PushGPSEventForSite");


        StringBuilder parameterString = new StringBuilder();

        if (isValidParameterString(deliveryId)) {
            parameterString.append("deliveryId=");
            parameterString.append(deliveryId);
        } else {
            Log.e("DM", "pushGPSEventForSiteWithDeliveryId: DeliveryId is nil, Cannot send request to Server");
        }

        if (isValidParameterString(siteId)) {
            parameterString.append("&siteId=");
            parameterString.append(siteId);
        } else {
            Log.e("DM", "pushGPSEventForSiteWithDeliveryId: siteId is nil, Cannot send request to Server");
        }

        if (isValidParameterString(serialNumber)) {
            parameterString.append("&serialNumber=");
            parameterString.append(serialNumber);
        } else {
            Log.e("DM", "pushGPSEventForSiteWithDeliveryId: serialNumber is nil, Cannot send request to Server");
        }


        if (isValidParameterString(deviceType)) {
            parameterString.append("&deviceType=");
            parameterString.append(deviceType);
        } else {
            Log.e("DM", "pushGPSEventForSiteWithDeliveryId: deviceType is nil, Cannot send request to Server");
        }

        if (timestamp != null) {
            //  NSString* timestampStr = [NSDate getISOFormatDate:timestamp];
            parameterString.append("&timestamp=");
            parameterString.append(getUTCFormatDate(timestamp));
        } else {
            Log.e("DM", "pushGPSEventForSiteWithDeliveryId: timestamp is nil, Cannot send request to Server");
        }

        String latitudeStr = String.format("%f", latitude);
        String longitudeStr = String.format("%f", longitude);

        parameterString.append("&lat=");
        parameterString.append(latitudeStr);
        parameterString.append("&lon=");
        parameterString.append(longitudeStr);

        if (type.getValue() >= 0) {
            parameterString.append("&type=");
            parameterString.append(String.format("%d", type.getValue()));
        } else {
            Log.e("DM", "pushGPSEventForSiteWithDeliveryId: Invalid GPSEventType value, Cannot send request to Server");
        }

        Log.v("DM", "Create Form  API Parameters: " + parameterString);


        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initializePOSTRequest(this.authHeaderValue, parameterString.toString(), new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for pushGPSEventForSiteWithDeliveryId: " + object.toString());
                        DeliveryReportActionResult result = new DeliveryReportActionResult();
                        JSONObject jsonObject = (JSONObject) object;
                        result.readFromJSONObject(jsonObject);
                        listener.pushGPSEventForSiteWithDeliveryId(result, null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.pushGPSEventForSiteWithDeliveryId(null, error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for pushGPSEventForSiteWithDeliveryId" + e.getMessage());
                    listener.pushGPSEventForSiteWithDeliveryId(null, error);

                }
            }

        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: pushGPSEventForSiteWithDeliveryId");
    }

    public void pushGPSEventForStopsWithDeliveryId(String deliveryId, ArrayList deliveryStopIds, String serialNumber, String deviceType,
                                                   Date timestamp, double latitude, double longitude, GPSEventTypeEnum type, final OnCompleteListeners.OnPushGPSEventForStopsWithDeliveryIdListener listener) {

        Log.v("DM", "Enter Function: pushGPSEventForStopsWithDeliveryId");
        final OnCompleteListeners.OnPushGPSEventForStopsWithDeliveryIdListener pushGPSEventForStops = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/RouteShipment2/PushGPSEventForStop");

        if (isValidParameterString(deliveryId)) {
            urlString.append("?deliveryId=");
            urlString.append(deliveryId);
        } else {
            Log.e("DM", "pushGPSEventForStopsWithDeliveryId: DeliveryId is nil, Cannot send request to Server");
        }

        if (isValidParameterString(serialNumber)) {
            urlString.append("&serialNumber=");
            urlString.append(serialNumber);
        } else {
            Log.e("DM", "pushGPSEventForStopsWithDeliveryId: serialNumber is nil, Cannot send request to Server");
        }

        for (Object str : deliveryStopIds) {
            urlString.append("&deliveryStopId=");
            urlString.append(str.toString());
        }


        if (isValidParameterString(deviceType)) {
            urlString.append("&deviceType=");
            urlString.append(deviceType);
        } else {
            Log.e("DM", "pushGPSEventForStopsWithDeliveryId: deviceType is nil, Cannot send request to Server");
        }
        if (timestamp != null) {
            //  NSString* timestampStr = [NSDate getISOFormatDate:timestamp];
            urlString.append("&timestamp=");
            urlString.append(timestamp);
        } else {
            Log.e("DM", "pushGPSEventForStopsWithDeliveryId: timestamp is nil, Cannot send request to Server");
        }

        String latitudeStr = String.format("%f", latitude);
        String longitudeStr = String.format("%f", longitude);

        urlString.append("&lat=");
        urlString.append(latitudeStr);
        urlString.append("&lon=");
        urlString.append(longitudeStr);

        if (Integer.parseInt(type.toString()) >= 0) {
            urlString.append("&type=");
            urlString.append(String.format("%d", Integer.parseInt(type.toString())));
        } else {
            Log.e("DM", "pushGPSEventForStopsWithDeliveryId: Invalid GPSEventType value, Cannot send request to Server");
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for pushGPSEventForStopsWithDeliveryId: " + object.toString());
                        DeliveryStopActionResult result = new DeliveryStopActionResult();
                        JSONObject jsonObject = (JSONObject) object;
                        result.readFromJSONObject(jsonObject);
                        listener.pushGPSEventForStopsWithDeliveryId(result, null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.pushGPSEventForStopsWithDeliveryId(null, error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for pushGPSEventForStopsWithDeliveryId" + e.getMessage());
                    listener.pushGPSEventForStopsWithDeliveryId(null, error);

                }

            }
        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: pushGPSEventForStopsWithDeliveryId");
    }

    public void pushGPSEventForPOIWithDeliveryId(String deliveryId, String poiId, String serialNumber, String deviceType,
                                                 Date timestamp, double latitude, double longitude, GPSEventTypeEnum.GPSEventTypesEnum type, final OnCompleteListeners.OnPushGPSEventForPOIWithDeliveryIdListener listener) {

        Log.v("DM", "Enter Function: pushGPSEventForPOIWithDeliveryId");
        final OnCompleteListeners.OnPushGPSEventForPOIWithDeliveryIdListener pushGPSEventForPOI = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/RouteShipment2/PushGPSEventForPOI");

        StringBuilder parameterString = new StringBuilder();


        if (isValidParameterString(deliveryId)) {
            parameterString.append("deliveryId=");
            parameterString.append(deliveryId);
        } else {
            Log.e("DM", "pushGPSEventForPOIWithDeliveryId: DeliveryId is nil, Cannot send request to Server");
        }
        if (isValidParameterString(poiId)) {
            parameterString.append("&poiId=");
            parameterString.append(poiId);
        } else {
            Log.e("DM", "pushGPSEventForPOIWithDeliveryId: poiId is nil, Cannot send request to Server");
        }

        if (isValidParameterString(serialNumber)) {
            parameterString.append("&serialNumber=");
            parameterString.append(serialNumber);
        } else {
            Log.e("DM", "pushGPSEventForPOIWithDeliveryId: serialNumber is nil, Cannot send request to Server");
        }


        if (isValidParameterString(deviceType)) {
            parameterString.append("&deviceType=");
            parameterString.append(deviceType);
        } else {
            Log.e("DM", "pushGPSEventForPOIWithDeliveryId: deviceType is nil, Cannot send request to Server");
        }
        if (timestamp != null) {
            //  NSString* timestampStr = [NSDate getISOFormatDate:timestamp];
            parameterString.append("&timestamp=");
            parameterString.append(getUTCFormatDate(timestamp));
        } else {
            Log.e("DM", "pushGPSEventForPOIWithDeliveryId: timestamp is nil, Cannot send request to Server");
        }

        String latitudeStr = String.format("%f", latitude);
        String longitudeStr = String.format("%f", longitude);

        parameterString.append("&lat=");
        parameterString.append(latitudeStr);
        parameterString.append("&lon=");
        parameterString.append(longitudeStr);

        if (type.getValue() >= 0) {
            parameterString.append("&type=");
            parameterString.append(String.format("%d", (type.getValue())));
        } else {
            Log.e("DM", "pushGPSEventForPOIWithDeliveryId: Invalid GPSEventType value");
        }
        Log.v("DM", "Create Form  API Parameters: " + parameterString);


        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initializePOSTRequest(this.authHeaderValue, parameterString.toString(), new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for pushGPSEventForPOIWithDeliveryId: " + object.toString());
                        DeliveryStopActionResult result = new DeliveryStopActionResult();
                        JSONObject jsonObject = (JSONObject) object;
                        result.readFromJSONObject(jsonObject);
                        listener.pushGPSEventForPOIWithDeliveryId(result, null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.pushGPSEventForPOIWithDeliveryId(null, error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for pushGPSEventForPOIWithDeliveryId" + e.getMessage());
                    listener.pushGPSEventForPOIWithDeliveryId(null, error);

                }
            }
        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: pushGPSEventForPOIWithDeliveryId");
    }

    public void getRouteShipmentEquipmentForEquipmentId(String routeShipmentEquipmentId, final OnCompleteListeners.getRouteShipmentEquipmentForEquipmentIdListener listener) {

        Log.v("DM", "Enter Function: getRouteShipmentEquipmentForEquipmentId");
        final OnCompleteListeners.getRouteShipmentEquipmentForEquipmentIdListener getRouteShipmentEquipment = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/DCSite");

        if (isValidParameterString(routeShipmentEquipmentId)) {
            urlString.append("?RouteShipmentEquipmentId=");
            urlString.append(routeShipmentEquipmentId);
        } else {
            Log.e("DM", "getRouteShipmentEquipmentForEquipmentId: routeShipmentEquipmentId is nil, Cannot send request to Server");
        }


        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "getRouteShipmentEquipment", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for getRouteShipmentEquipmentForEquipmentId: " + object.toString());
                    RouteShipmentEquipmentDTO equipment = new RouteShipmentEquipmentDTO();
                    equipment.readFromJSONObject(object);
                    listener.getRouteShipmentEquipmentForEquipmentId(equipment);
                } else {
                    Log.d("DM", "Null object response for getRouteShipmentEquipmentForEquipmentId");
                }
            }
        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getRouteShipmentEquipmentForEquipmentId");
    }

    public void getRouteShipmentUnitForRouteShipmentUnitId(String routeShipmentUnitId, final OnCompleteListeners.getRouteShipmentUnitForRouteShipmentUnitIdListener listener) {

        Log.v("DM", "Enter Function: getRouteShipmentUnitForRouteShipmentUnitId");
        final OnCompleteListeners.getRouteShipmentUnitForRouteShipmentUnitIdListener getRouteShipmentUnit = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/RouteShipmentUnit");

        if (isValidParameterString(routeShipmentUnitId)) {
            urlString.append("?routeShipmentUnitId=");
            urlString.append(routeShipmentUnitId);
        } else {
            Log.e("DM", "getRouteShipmentUnitForRouteShipmentUnitId: routeShipmentEquipmentId Invalid parameter routeShipmentUnitId");
        }


        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "RouteShipmentUnit", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for getRouteShipmentUnitForRouteShipmentUnitId: " + object.toString());
                    RouteShipmentUnit routeShipmentUnit = new RouteShipmentUnit();
                    routeShipmentUnit.readFromJSONObject(object);
                    listener.getRouteShipmentUnitForRouteShipmentUnitId(routeShipmentUnit);
                } else {
                    Log.d("DM", "Null object response for getRouteShipmentUnitForRouteShipmentUnitId");
                }
            }
        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getRouteShipmentUnitForRouteShipmentUnitId");
    }


    public void getRouteShipmentProductClassForProductClassId(String routeShipmentProductClassId, final OnCompleteListeners.getRouteShipmentProductClassForProductClassIdListener listener) {

        Log.v("DM", "Enter Function: getRouteShipmentProductClassForProductClassId");
        final OnCompleteListeners.getRouteShipmentProductClassForProductClassIdListener getRouteShipmentProduct = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/RouteShipmentProductClass");

        if (isValidParameterString(routeShipmentProductClassId)) {
            urlString.append("?routeShipmentProductClassId=");
            urlString.append(routeShipmentProductClassId);
        } else {
            Log.e("DM", "getRouteShipmentProductClassForProductClassId: routeShipmentProductClassId Invalid parameter routeShipmentUnitId");
        }


        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "RouteShipmentProductClass", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for getRouteShipmentProductClassForProductClassId: " + object.toString());
                    RouteShipmentProductClass routeShipmentProductClass = new RouteShipmentProductClass();
                    routeShipmentProductClass.readFromJSONObject(object);
                    listener.getRouteShipmentProductClassForProductClassId(routeShipmentProductClass);
                } else {
                    Log.d("DM", "Null object response for getRouteShipmentProductClassForProductClassId");
                }
            }
        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getRouteShipmentProductClassForProductClassId");
    }

    public void SaveTripForDeliveryId(String deliveryId, String data, final OnCompleteListeners.onSaveTripForDeliveryIdListener listener) {

        Log.v("DM", "Enter Function: SaveTripForDeliveryId");
        final OnCompleteListeners.onSaveTripForDeliveryIdListener SaveTrip = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/RouteShipment2/SaveTrip");


        if (isValidParameterString(deliveryId)) {
            urlString.append("?deliveryId=");
            urlString.append(deliveryId);
        } else {
            Log.e("DM", "SaveTripForDeliveryId: DeliveryId is Nil/Empty");
        }

        if (isValidParameterString(data)) {
            urlString.append("&data=");
            urlString.append(data);
        } else {
            Log.e("DM", "SaveTripForDeliveryId: Data parameter is Nil/Empty ");
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for SaveTripForDeliveryId: " + object.toString());
                        TripDataResult tripDataResult = new TripDataResult();
                        JSONObject jsonObject = (JSONObject) object;
                        tripDataResult.readFromJSONObject(jsonObject);
                        listener.SaveTripForDeliveryId(tripDataResult, null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.SaveTripForDeliveryId(null, error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for SaveTripForDeliveryId" + e.getMessage());
                    listener.SaveTripForDeliveryId(null, error);

                }
            }

        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: SaveTripForDeliveryId");
    }

    public void SaveCoreForDeliveryId(String deliveryId, String data, final OnCompleteListeners.onSaveCoreForDeliveryIdListener listener) {

        Log.v("DM", "Enter Function: SaveCoreForDeliveryId");
        final OnCompleteListeners.onSaveCoreForDeliveryIdListener SaveCore = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/RouteShipment2/SaveCore");


        if (isValidParameterString(deliveryId)) {
            urlString.append("?deliveryId=");
            urlString.append(deliveryId);
        } else {
            Log.e("DM", "SaveCoreForDeliveryId: DeliveryId is Nil/Empty");
        }

        if (isValidParameterString(data)) {
            urlString.append("&data=");
            urlString.append(data);
        } else {
            Log.e("DM", "SaveCoreForDeliveryId: Data parameter is Nil/Empty ");
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "SaveCore", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for SaveCoreForDeliveryId: " + object.toString());
                    TripDataResult tripDataResult = new TripDataResult();
                    tripDataResult.readFromJSONObject(object);
                    listener.SaveCoreForDeliveryId(tripDataResult);
                } else {
                    Log.d("DM", "Null object response for SaveCoreForDeliveryId");
                }
            }
        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: SaveCoreForDeliveryId");
    }

    public void SaveTripAndStreamDataForDeliveryId(String deliveryId, String data, final OnCompleteListeners.onSaveTripAndStreamDataForDeliveryIdListener listener) {

        Log.v("DM", "Enter Function: SaveTripAndStreamData");
        final OnCompleteListeners.onSaveTripAndStreamDataForDeliveryIdListener SaveTripAndStreamData = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/RouteShipment2/SaveTripAndStreamData");


        if (isValidParameterString(deliveryId)) {
            urlString.append("?deliveryId=");
            urlString.append(deliveryId);
        } else {
            Log.e("DM", "SaveTripAndStreamData: DeliveryId is Nil/Empty");
        }

        if (isValidParameterString(data)) {
            urlString.append("&data=");
            urlString.append(data);
        } else {
            Log.e("DM", "SaveTripAndStreamData: Data parameter is Nil/Empty ");
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "SaveTripAndStreamData", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for SaveTripAndStreamData: " + object.toString());
                    TripDataResult tripDataResult = new TripDataResult();
                    tripDataResult.readFromJSONObject(object);
                    listener.saveTripAndStreamDataForDeliveryId(tripDataResult);
                } else {
                    Log.d("DM", "Null object response for SaveTripAndStreamData");
                }
            }
        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: SaveTripAndStreamData");
    }

    public void SavePlannedRoutesForDeliveryId(String deliveryId, String data, final OnCompleteListeners.onSavePlannedRoutesForDeliveryIdListener listener) {

        Log.v("DM", "Enter Function: SavePlannedRoutes");
        final OnCompleteListeners.onSavePlannedRoutesForDeliveryIdListener SavePlannedRoutes = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/RouteShipment2/SavePlannedRoutes");


        if (isValidParameterString(deliveryId)) {
            urlString.append("?deliveryId=");
            urlString.append(deliveryId);
        } else {
            Log.e("DM", "SavePlannedRoutes: DeliveryId is Nil/Empty");
        }

        if (isValidParameterString(data)) {
            urlString.append("&data=");
            urlString.append(data);
        } else {
            Log.e("DM", "SavePlannedRoutes: Data parameter is Nil/Empty ");
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "SavePlannedRoutes", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for SavePlannedRoutes: " + object.toString());
                    TripDataResult tripDataResult = new TripDataResult();
                    tripDataResult.readFromJSONObject(object);
                    listener.savePlannedRoutesForDeliveryId(tripDataResult);
                } else {
                    Log.d("DM", "Null object response for SavePlannedRoutes");
                }
            }
        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: SavePlannedRoutes");
    }


    public void SaveAdjustedRoutesForDeliveryId(String deliveryId, String data, final OnCompleteListeners.onSaveAdjustedRoutesForDeliveryIdListener listener) {

        Log.v("DM", "Enter Function: SaveAdjustedRoutes");
        final OnCompleteListeners.onSaveAdjustedRoutesForDeliveryIdListener SaveAdjustedRoutes = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/RouteShipment2/SaveAdjustedRoutes");


        if (isValidParameterString(deliveryId)) {
            urlString.append("?deliveryId=");
            urlString.append(deliveryId);
        } else {
            Log.e("DM", "SaveAdjustedRoutes: DeliveryId is Nil/Empty");
        }

        if (isValidParameterString(data)) {
            urlString.append("&data=");
            urlString.append(data);
        } else {
            Log.e("DM", "SaveAdjustedRoutes: Data parameter is Nil/Empty ");
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "SaveAdjustedRoutes", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for SaveAdjustedRoutes: " + object.toString());
                    TripDataResult tripDataResult = new TripDataResult();
                    tripDataResult.readFromJSONObject(object);
                    listener.saveAdjustedRoutesForDeliveryId(tripDataResult);
                } else {
                    Log.d("DM", "Null object response for SaveAdjustedRoutes");
                }
            }
        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: SaveAdjustedRoutes");
    }

    public void SaveStreamDataForDeliveryId(String deliveryId, String data, final OnCompleteListeners.onSaveStreamDataForDeliveryIdListener listener) {

        Log.v("DM", "Enter Function: SaveStreamData");
        final OnCompleteListeners.onSaveStreamDataForDeliveryIdListener SaveStreamData = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/RouteShipment2/SaveStreamData");


        if (isValidParameterString(deliveryId)) {
            urlString.append("?deliveryId=");
            urlString.append(deliveryId);
        } else {
            Log.e("DM", "SaveStreamData: DeliveryId is Nil/Empty");
        }

        if (isValidParameterString(data)) {
            urlString.append("&data=");
            urlString.append(data);
        } else {
            Log.e("DM", "SaveStreamData: Data parameter is Nil/Empty ");
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "SaveStreamData", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for SaveStreamData: " + object.toString());
                    TripDataResult tripDataResult = new TripDataResult();
                    tripDataResult.readFromJSONObject(object);
                    listener.saveStreamDataForDeliveryId(tripDataResult);
                } else {
                    Log.d("DM", "Null object response for SaveStreamData");
                }
            }
        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: SaveStreamData");
    }


    public void SaveStreamUpdateForDeliveryId(String deliveryId, String data, final OnCompleteListeners.onSaveStreamUpdateForDeliveryIdListener listener) {

        Log.v("DM", "Enter Function: SaveStreamUpdate");
        final OnCompleteListeners.onSaveStreamUpdateForDeliveryIdListener SaveStreamUpdate = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/RouteShipment2/SaveStreamUpdate");


        if (isValidParameterString(deliveryId)) {
            urlString.append("?deliveryId=");
            urlString.append(deliveryId);
        } else {
            Log.e("DM", "SaveStreamUpdate: DeliveryId is Nil/Empty");
        }

        if (isValidParameterString(data)) {
            urlString.append("&data=");
            urlString.append(data);
        } else {
            Log.e("DM", "SaveStreamUpdate: Data parameter is Nil/Empty ");
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "SaveStreamUpdate", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for SaveStreamUpdate: " + object.toString());
                    TripDataResult tripDataResult = new TripDataResult();
                    tripDataResult.readFromJSONObject(object);
                    listener.saveStreamUpdateForDeliveryId(tripDataResult);
                } else {
                    Log.d("DM", "Null object response for SaveStreamUpdate");
                }
            }
        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: SaveStreamUpdate");
    }

       /* #pragma Mark - PreTrip Verification


        /// <summary>
        /// Verify that the driver and the tractor as assigned to a route.
        /// Side effects:  may assign a driver or a tractor to a route
        /// </summary>
        /// <param name="siteId">dc site</param>
        /// <param name="deviceType">mobile device deviceType (ie, IPAD)</param>
        /// <param name="serialNumber">mobile device serial number</param>
        /// <returns>PreTripVerificationResult, which indicates whether the login should be allowed or not.</returns> */

    public void getPreTripVerificationResultForSiteId(String siteId, String deviceType, String serialNumber, final OnCompleteListeners.getPreTripVerificationResultForSiteIdListener listener) {

        Log.v("DM", "Enter Function: SaveStreamUpdate");
        final OnCompleteListeners.getPreTripVerificationResultForSiteIdListener PreTripVerification = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/DCSite/PreTripVerification");


        if (isValidParameterString(siteId)) {
            urlString.append("?siteId=");
            urlString.append(siteId);
        } else {
            Log.e("DM", "PreTripVerification: siteId is Nil/Empty");
        }

        if (isValidParameterString(deviceType)) {
            urlString.append("&deviceType=");
            urlString.append(deviceType);
        } else {
            Log.e("DM", "PreTripVerification: deviceType parameter is Nil/Empty ");
        }
        if (isValidParameterString(serialNumber)) {
            urlString.append("&serialNumber=");
            urlString.append(serialNumber);
        } else {
            Log.e("DM", "PreTripVerification: serialNumber parameter is Nil/Empty ");
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for PreTripVerification: " + object.toString());
                        PreTripVerifierResult preTripVerifierResult = new PreTripVerifierResult();
                        JSONObject jsonObject = (JSONObject) object;
                        preTripVerifierResult.readFromJSONObject(jsonObject);
                        listener.GetPreTripVerificationResultForSiteId(preTripVerifierResult, null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.GetPreTripVerificationResultForSiteId(null, error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for GetPreTripVerificationResultForSiteId: " + e.getMessage());
                    listener.GetPreTripVerificationResultForSiteId(null, error);

                }

            }
        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: PreTripVerification");
    }
        /*#pragma Mark POD Discrepancy Report APIs

        /// <summary>
        /// Set the deliveryStatus for a stop on a route.  note: will get switched over to RPS commands
        /// </summary>
        /// <param name="shipmentId"></param>
        /// <param name="stopId"></param>
        /// <param name="deliveryStatus">null = no POD information available
        ///                              1    = delivery ok
        ///                              2    = product discrepancy
        /// </param>
        /// <returns>SuccessResult</returns> */

    public void SetDeliveryStatusForDeliveryId(String deliveryId, String stopId, StoreDeliveryEnums.PODDeliveryStatusEnum deliveryStatus, final OnCompleteListeners.SetDeliveryStatusForDeliveryIdListener listener) {

        Log.v("DM", "Enter Function: SetDeliveryStatus");
        final OnCompleteListeners.SetDeliveryStatusForDeliveryIdListener SetDeliveryStatus = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/RouteShipment2/SetDeliveryStatus");

        StringBuilder parameterString = new StringBuilder();


        if (isValidParameterString(deliveryId)) {
            parameterString.append("?shipmentId=");
            parameterString.append(deliveryId);
        } else {
            Log.e("DM", "SetDeliveryStatus: deliveryId is Nil/Empty");
        }

        if (isValidParameterString(stopId)) {
            parameterString.append("&stopId=");
            parameterString.append(stopId);
        } else {
            Log.e("DM", "SetDeliveryStatus: stopId parameter is Nil/Empty ");
        }

        if (deliveryStatus.getValue() != 0) {
            parameterString.append("&deliveryStatus=");
            parameterString.append(String.format("%d", (int) deliveryStatus.getValue()));
        }
        Log.v("DM", "Create Form  API Parameters:: " + parameterString);

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initializePOSTRequest(this.authHeaderValue, parameterString.toString(), new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for SetDeliveryStatus: " + object.toString());
                        listener.setDeliveryStatusForDeliveryId(null);
                    } else if (object == null && error == null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for SetDeliveryStatus: " + object.toString());
                        listener.setDeliveryStatusForDeliveryId(null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.setDeliveryStatusForDeliveryId(error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for setDeliveryStatusForDeliveryId" + e.getMessage());
                    listener.setDeliveryStatusForDeliveryId(error);

                }

            }

        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: SetDeliveryStatus");
    }

    public void setProductSourceForDeliveryId(String deliveryId, ArrayList<ProductSource> productSourceList, final OnCompleteListeners.SetProductSourceForDeliveryIdListener listener) {

        Log.v("DM", "Enter Function: SetDeliveryStatus");
        final OnCompleteListeners.SetProductSourceForDeliveryIdListener SetProductSource = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/RouteShipment2/SetProductSource");


        if (isValidParameterString(deliveryId)) {
            urlString.append("?shipmentId=");
            urlString.append(deliveryId);
        } else {
            Log.e("DM", "SetProductSource: deliveryId is Nil/Empty");
        }

//           if (productSourceList != null){
//               for(ProductSource productSource : productSourceList){
//                   urlString.append("&input=");
//                   urlString.append(input);
//
//                   NSMutableArray* tmpArray = [[NSMutableArray alloc] init];
//                   for(ProductSource* productSource in productSourceList)
//                   {
//                       NSDictionary* dictionary = [productSource dictionaryWithValuesForKeys];
//           addHOSEventDueToEditByDriver//                   }
//
//                   NSError *jsonError = nil;
//
//                   NSData *jsonData = [NSJSONSerialization dataWithJSONObject:tmpArray options:0 error:&jsonError];
//
//                   NSString* jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
//
//                   DDLogVerbose(@"JSONRepresentation:%@",jsonString);
//                   NSString *input = [jsonData base64EncodedString];
//
//        [parameterString appendString:@"&input="];
//        [parameterString appendString:input];
//
//
//               }
//           }


        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        listener.setProductSourceForDeliveryId(null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.setProductSourceForDeliveryId(error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for setProductSourceForDeliveryId" + e.getMessage());
                    listener.setProductSourceForDeliveryId(error);

                }
            }
        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: SetProductSource");
    }

    public void getRouteStatusDNDBEventForEventId(String routeStatusDNDBEventId, final OnCompleteListeners.getRouteStatusDNDBEventForEventIdListener listener) {

        Log.v("DM", "Enter Function: RouteStatusDNDBEvent");
        final OnCompleteListeners.getRouteStatusDNDBEventForEventIdListener RouteStatusDNDBEvent = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/RouteStatusDNDBEvent");

        if (isValidParameterString(routeStatusDNDBEventId)) {
            urlString.append("?routeStatusDNDBEventId=");
            urlString.append(routeStatusDNDBEventId);
        } else {
            Log.e("DM", "RouteStatusDNDBEvent: routeStatusDNDBEventId is Nil/Empty");
        }


        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "RouteStatusDNDBEvent", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for RouteStatusDNDBEvent: " + object.toString());
                    RouteShipmentStopDNDBEvent dndbEvent = new RouteShipmentStopDNDBEvent();
                    dndbEvent.readFromJSONObject(object);
                    listener.GetRouteStatusDNDBEventForEventId(dndbEvent);
                } else {
                    Log.d("DM", "Null object response for RouteStatusDNDBEvent");
                }
            }
        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: RouteStatusDNDBEvent");
    }

    public void createOrUpdateDNDBEventWithEventId(String routeStatusDNDBEventId, String shipmentId, Integer deliveryStopId, Date arrivalDate,
                                                   Date departureDate, RouteShipmentStopDNDBEvent.RouteStatusDNDBEventTypeEnum eventType, final OnCompleteListeners.onCreateOrUpdateDNDBEventWithEventIdListener listener) {

        Log.v("DM", "Enter Function: RouteStatusDNDBEvent");
        final OnCompleteListeners.onCreateOrUpdateDNDBEventWithEventIdListener RouteStatusDNDBEvent = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/RouteStatusDNDBEvent");

        StringBuilder parameterString = new StringBuilder();


        if (isValidParameterString(routeStatusDNDBEventId)) {
            parameterString.append("?routeStatusDNDBEventId=");
            parameterString.append(routeStatusDNDBEventId);
        } else {
            Log.e("DM", "RouteStatusDNDBEvent: routeStatusDNDBEventId is Nil/Empty");
        }

        if (isValidParameterString(shipmentId)) {
            parameterString.append("&shipmentId=");
            parameterString.append(shipmentId);
        } else {
            Log.e("DM", "RouteStatusDNDBEvent: shipmentId is Nil/Empty");
        }
        if (deliveryStopId > 0) {
            parameterString.append("&deliveryStopId=");
            parameterString.append(String.format("%d", deliveryStopId));
        } else {
            Log.e("DM", "RouteStatusDNDBEvent: DeliveryStopId is not set");
        }
        if (arrivalDate != null) {
            parameterString.append("&arrivalTime=");
            //[parameterString appendString:[NSDate getISOFormatDate:arrivalDate]];
            parameterString.append(getUTCFormatDate(arrivalDate));
        } else {
            Log.e("DM", "RouteStatusDNDBEvent: Arrival Time is nil, cannot send request to Server");
        }
        if (departureDate != null) {
            parameterString.append("&departureTime=");
            //[parameterString appendString:[NSDate getISOFormatDate:arrivalDate]];
            parameterString.append(getUTCFormatDate(departureDate));
        } else {
            Log.e("DM", "RouteStatusDNDBEvent: Departure Time is nil, cannot send request to Server");
        }

        if (eventType != null && eventType.getValue() > -1) {
            urlString.append("&eventType=");
            urlString.append(String.format("%d", (int) eventType.getValue()));

        }
        Log.v("DM", "Create Form  API Parameters:: " + parameterString);

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initializePOSTRequest(this.authHeaderValue, parameterString.toString(), new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for RouteStatusDNDBEvent: " + object.toString());
                        RouteShipmentStopDNDBEvent dndbEvent = new RouteShipmentStopDNDBEvent();
                        JSONObject jsonObject = (JSONObject) object;
                        dndbEvent.readFromJSONObject(jsonObject);
                        listener.createOrUpdateDNDBEventWithEventId(dndbEvent, null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.createOrUpdateDNDBEventWithEventId(null, error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for createOrUpdateDNDBEventWithEventId" + e.getMessage());
                    listener.createOrUpdateDNDBEventWithEventId(null, error);

                }
            }
        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: RouteStatusDNDBEvent");
    }

    public void getCombinedStatusListsForDCId(String dcId, boolean tractors, boolean trailers, boolean drivers, final OnCompleteListeners.getCombinedStatusListsForDCIdListener listener) {

        Log.v("DM", "Enter Function: getCombinedStatusListsForDCId");
        final OnCompleteListeners.getCombinedStatusListsForDCIdListener CombinedStatusLists = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/DCSite/CombinedStatusLists");

        if (isValidParameterString(dcId)) {
            urlString.append("?dcId=");
            urlString.append(dcId);
        } else {
            Log.e("DM", "RouteStatusDNDBEvent: dcId is Nil/Empty");
        }


        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "CombinedStatusLists", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for getCombinedStatusListsForDCId: " + object.toString());
                    RMSCombinedStatusLists combinedStatusList = new RMSCombinedStatusLists();
                    combinedStatusList.readFromJSONObject(object);
                    listener.GetCombinedStatusListsForDCId(combinedStatusList);
                } else {
                    Log.d("DM", "Null object response for getCombinedStatusListsForDCId");
                }
            }
        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getCombinedStatusListsForDCId");
    }


    public void getDeliveryStatusWithSelectedForDCId(String dcId, Date date, String deliveryId, boolean autoSelectFirst, boolean includeForms, String formsToInclude, final OnCompleteListeners.getDeliveryStatusWithSelectedForDCIdListener listener) {

        Log.v("DM", "Enter Function: DeliveryStatusWithSelected");
        final OnCompleteListeners.getDeliveryStatusWithSelectedForDCIdListener DeliveryStatusWithSelected = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/DCSite/DeliveryStatusWithSelected");

        if (isValidParameterString(dcId)) {
            urlString.append("?dcId=");
            urlString.append(dcId);
        } else {
            Log.e("DM", "DeliveryStatusWithSelected: dcId is Nil/Empty");
        }
        if (date != null) {
            urlString.append("&date=");
            //getISOFormatDate
            urlString.append(date);
        }
        if (autoSelectFirst) {
            urlString.append("&autoSelectFirst=");
            urlString.append(autoSelectFirst ? "1" : "0");
        }
        if (includeForms) {
            urlString.append("&includeForms=");
            urlString.append(includeForms ? "1" : "0");
        }
        if (isValidParameterString(formsToInclude)) {
            urlString.append("&formsToInclude=");
            urlString.append(formsToInclude);
        }
        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "DeliveryStatusWithSelected", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for DeliveryStatusWithSelected: " + object.toString());
                    DeliveryStatusWithSelectedDTO deliveryStatusWithSelectedDTO = new DeliveryStatusWithSelectedDTO();
                    deliveryStatusWithSelectedDTO.readFromJSONObject(object);
                    listener.GetDeliveryStatusWithSelectedForDCId(deliveryStatusWithSelectedDTO);
                } else {
                    Log.d("DM", "Null object response for DeliveryStatusWithSelected");
                }
            }
        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: DeliveryStatusWithSelected");
    }

    public void getSdrReportForDeliveryId(String deliveryId, boolean includeForms, String formsToInclude, final OnCompleteListeners.getSdrReportForDeliveryIdListener listener) {

        Log.v("DM", "Enter Function: getSdrReportForDeliveryId");
        final OnCompleteListeners.getSdrReportForDeliveryIdListener Report = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/RouteShipment2/Report");

        if (isValidParameterString(deliveryId)) {
            urlString.append("?deliveryId=");
            urlString.append(deliveryId);
        } else {
            Log.e("DM", "getSdrReportForDeliveryId: deliveryId is Nil/Empty");
        }


        urlString.append("&includeForms=");
        // urlString.append(includeForms? "1":"0");
        urlString.append(includeForms);

        if (isValidParameterString(formsToInclude)) {
            urlString.append("&formsToInclude=");
            urlString.append(formsToInclude);
        }
        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for getSdrReportForDeliveryId: " + object.toString());
                        SdrReport sdrReport = new SdrReport();
                        JSONObject jsonObject = (JSONObject) object;
                        sdrReport.readFromJSONObject(jsonObject);
                        listener.GetSdrReportForDeliveryId(sdrReport, null);
                    } else {
                        Log.e("DM", "Null object for GetSdrReportForDeliveryId");
                        listener.GetSdrReportForDeliveryId(null, error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for getSdrReportForDeliveryId: " + e.getMessage());
                    listener.GetSdrReportForDeliveryId(null, error);
                }
            }
        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getSdrReportForDeliveryId");
    }

    public void getTrailerReportForTrailerId(String trailerId, String deliveryId, String siteId, boolean includeForms, String formsToInclude, final OnCompleteListeners.getTrailerReportForTrailerIdListener listener) {

        Log.v("DM", "Enter Function: getTrailerReportForTrailerId");
        final OnCompleteListeners.getTrailerReportForTrailerIdListener TrailerReport = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/TrailerReport");

        if (isValidParameterString(trailerId)) {
            urlString.append("?trailerId=");
            urlString.append(trailerId);
        } else {
            Log.e("DM", "getTrailerReportForTrailerId: trailerId is Nil/Empty");
        }
        if (isValidParameterString(deliveryId)) {
            urlString.append("&deliveryId=");
            urlString.append(deliveryId);
        } else {
            Log.e("DM", "getTrailerReportForTrailerId: deliveryId is Nil/Empty");
        }

        if (includeForms) {
            urlString.append("&includeForms=");
            urlString.append(includeForms ? "true" : "false");
//            urlString.append(includeForms? "1":"0");
        }
        if (isValidParameterString(formsToInclude)) {
            urlString.append("&formsToInclude=");
            urlString.append(formsToInclude);
        }
        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for getTrailerReportForTrailerId: " + object.toString());
                        SdrReport sdrReport = new SdrReport();
                        JSONObject jsonObject = (JSONObject) object;
                        sdrReport.readFromJSONObject(jsonObject);
                        listener.getTrailerReportForTrailerId(sdrReport, null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.getTrailerReportForTrailerId(null, error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for getTrailerReportForTrailerId" + e.getMessage());
                    listener.getTrailerReportForTrailerId(null, error);
                }
            }
        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getTrailerReportForTrailerId");
    }

    public void sendPODEmailForDeliveryId(String deliveryId, Integer stopId, String subject, String text, String html, Dictionary attachmentsDic, final OnCompleteListeners.onSendPODEmailForDeliveryIdListener listener) {

        Log.v("DM", "Enter Function: sendPODEmailForDeliveryId");
        final OnCompleteListeners.onSendPODEmailForDeliveryIdListener TrailesendPODEmailrReport = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/Notify/tms/pod/pre/");


        if (stopId != null) {
            urlString.append("?/stop/=");
            urlString.append(String.format("%ld", Long.parseLong(stopId.toString())));
        }

        /*
        Integer ticks = [[NSDate date] timeIntegerervalSince1970];
        NSString* boundary = [NSString stringWithFormat:@"%d",ticks];

        NSString* contentTypeValue = [NSString stringWithFormat:@"multipart/form-data; boundary=------------%@", boundary];



        NSMutableData *body = [NSMutableData data];

        NSMutableString* contentDisposition1 = [[NSMutableString alloc] init];
    [contentDisposition1 appendString:@"Content-Disposition: form-data; name=\"subject\"\r\n\r\n"];
    [body appendData:[[NSString stringWithFormat:@"\r\n--------------%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithString:contentDisposition1] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[subject dataUsingEncoding:NSUTF8StringEncoding]];


        NSMutableString* contentDisposition2 = [[NSMutableString alloc] init];
     [contentDisposition2 appendString:@"Content-Disposition: form-data; name=\"text\"\r\n\r\n"];
     [body appendData:[[NSString stringWithFormat:@"\r\n--------------%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
     [body appendData:[[NSString stringWithString:contentDisposition2] dataUsingEncoding:NSUTF8StringEncoding]];
     [body appendData:[text dataUsingEncoding:NSUTF8StringEncoding]];


        if([self isValidParameterString:html])
        {
            NSMutableString* contentDisposition3 = [[NSMutableString alloc] init];
        [contentDisposition3 appendString:@"Content-Disposition: form-data; name=\"html\"\r\n\r\n"];
        [body appendData:[[NSString stringWithFormat:@"\r\n--------------%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];


        [body appendData:[[NSString stringWithString:contentDisposition3] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[html dataUsingEncoding:NSUTF8StringEncoding]];
        }
    else
        {
            DDLogDebug(@"HTML Parameter in SendPODEmail API is nil/empty");
        }


        if(attachmentsDic != nil && attachmentsDic.count > 0)
        {
            Integer i = 0;
            for(NSString* key in attachmentsDic)
            {
                NSString* filename = key;
                // NSString* filePath = [attachmentsDic objectForKey:key];

                DDLogDebug(@"Attaching PODReport File With Name:%@", filename);

                NSMutableString* contentDisposition = [[NSMutableString alloc] init];
            [contentDisposition appendString:[NSString  stringWithFormat:@"Content-Disposition: form-data; name=\"file%d\"; filename=\"%@\"\r\n",i, filename]];


            [body appendData:[[NSString stringWithFormat:@"\r\n--------------%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithString:contentDisposition] dataUsingEncoding:NSUTF8StringEncoding]];

                NSString* contentType = @"Content-Type: application/pdf\r\n\r\n";
            [body appendData:[contentType dataUsingEncoding:NSUTF8StringEncoding]];
              /*  NSData* attachmentContent = [[NSData alloc] initWithContentsOfFile:filePath];*/
/*
                NSData* attachmentContent = [attachmentsDic objectForKey:key];

                //  DDLogDebug(@"AttachmentContent:%@", attachmentContent);

            [body appendData:[NSData dataWithData:attachmentContent]];

            }

        }
     [body appendData:[[NSString stringWithFormat:@"\r\n--------------%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];







        Integer reqIdentifier = [self generateRequestIdentifier];

        NotifyConfirmation* response = [[NotifyConfirmation alloc] init];
        NSString* objectClassName = @"NotifyConfirmation";
        BOOL isJSONResponse = true;

        NSMutableDictionary* context = [[NSMutableDictionary alloc] init];

    [context setValue:[[NSNumber alloc] initWithInteger:SEND_POD_EMAIL] forKey:@"MessageType"];
    [context setValue:deliveryId forKey:@"DeliveryId"];
    [context setValue:[[NSNumber alloc] initWithInteger:stopId]  forKey:@"StopId"];

**/
        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for sendPODEmailForDeliveryId: " + object.toString());
                        NotifyConfirmation notifyConfirmation = new NotifyConfirmation();
                        JSONObject jsonObject = (JSONObject) object;
                        notifyConfirmation.readFromJSONObject(jsonObject);
                        listener.sendPODEmailForDeliveryId(notifyConfirmation, null);

                    } else {
                        Log.e("DM", "Null object ");
                        listener.sendPODEmailForDeliveryId(null, error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for sendPODEmailForDeliveryId" + e.getMessage());
                    listener.sendPODEmailForDeliveryId(null, error);

                }

            }
        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: sendPODEmailForDeliveryId");
    }


    public void rebuildRouteWithDeliveryId(String deliveryId, final OnCompleteListeners.RebuildRouteWithDeliveryIdListener listener) {

        Log.v("DM", "Enter Function: rebuildRoute");
        final OnCompleteListeners.RebuildRouteWithDeliveryIdListener rebuildRoute = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/RouteShipment2/Rebuild");

        if (isValidParameterString(deliveryId)) {
            urlString.append("?deliveryId=");
            urlString.append(deliveryId);
        } else {
            Log.e("DM", "rebuildRoute: deliveryId is Nil/Empty");
        }


        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "rebuildRoute", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for rebuildRoute: " + object.toString());
                    DeliveryActionResult result = new DeliveryActionResult();
                    result.readFromJSONObject(object);
                    listener.rebuildRouteWithDeliveryId(result);
                } else {
                    Log.d("DM", "Null object response for rebuildRoute");
                }
            }
        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: rebuildRoute");
    }

    public void terminateRouteWithDeliveryId(String deliveryId, final OnCompleteListeners.terminateRouteWithDeliveryIdListener listener) {

        Log.v("DM", "Enter Function: TerminateRoute");
        final OnCompleteListeners.terminateRouteWithDeliveryIdListener TerminateRoute = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/RouteShipment2/Terminate");

        if (isValidParameterString(deliveryId)) {
            urlString.append("?deliveryId=");
            urlString.append(deliveryId);
        } else {
            Log.e("DM", "TerminateRoute: deliveryId is Nil/Empty");
        }


        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "TerminateRoute", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for TerminateRoute: " + object.toString());
                    DeliveryReportActionResult result = new DeliveryReportActionResult();
                    result.readFromJSONObject(object);
                    listener.terminateRouteWithDeliveryId(result);
                } else {
                    Log.d("DM", "Null object response for TerminateRoute");
                }

            }
        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: TerminateRoute");
    }


    public void moveStopForDeliveryId(String deliveryId, Integer deliveryStopId, double latitude, double longitude, Integer radius,
                                      boolean hasAddress, AddressComponents address, boolean hasContact, String contactName,
                                      String contactPhoneNumber, String contactEmail, final OnCompleteListeners.moveStopForDeliveryIdListener listener) {


        Log.v("DM", "Enter Function: MoveStopToPOI");
        final OnCompleteListeners.moveStopForDeliveryIdListener MoveStop = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/RouteShipment2/MoveStop");

        if (isValidParameterString(deliveryId)) {
            urlString.append("?deliveryId=");
            urlString.append(deliveryId);
        } else {
            Log.e("DM", "MoveStop: deliveryId is Nil/Empty");
        }

        urlString.append("&deliveryStopId=");
        urlString.append(String.format("%d", deliveryStopId));

        urlString.append("&latitude=");
        urlString.append(String.format("%f", latitude));
        urlString.append("&longitude=");
        urlString.append(String.format("%f", longitude));
        urlString.append("&radius=");
        urlString.append(String.format("%d", radius));


        if (hasAddress == Boolean.TRUE) {
            if (address != null) {
                urlString.append("&hasAddress=");
                urlString.append("1");


                if (address.Address1 != null) {
                    urlString.append("&address1=");
                    urlString.append(address.Address1);
                }
                if (address.Address2 != null) {
                    urlString.append("&address2=");
                    urlString.append(address.Address2);
                }
                if (address.City != null) {
                    urlString.append("&city=");
                    urlString.append(address.City);
                }
                if (address.Province != null) {
                    urlString.append("&province=");
                    urlString.append(address.Province);
                }
                if (address.Postal != null) {
                    urlString.append("&postal=");
                    urlString.append(address.Postal);
                }
                if (address.Country != null) {
                    urlString.append("&country=");
                    urlString.append(address.Country);
                }
                if (address.StreetName != null) {
                    urlString.append("&streetName=");
                    urlString.append(address.StreetName);
                }
                if (address.StreetNumber != null) {
                    urlString.append("&streetNumber=");
                    urlString.append(address.StreetNumber);
                }
            } else {
                Log.d("DM", "MoveStop: HasAddress parameter is set to TRUE But 'address' parameter is nil");
            }
        }

        if (hasContact == true) {
            urlString.append("&hasContact=");
            urlString.append("1");

            if (isValidParameterString(contactName)) {
                urlString.append("&contactName=");
                urlString.append(contactName);
            } else {
                Log.d("DM", "MoveStop: contactName parameter is set to TRUE But 'comment' parameter is nil");
            }
        }

        if (isValidParameterString(contactPhoneNumber)) {
            urlString.append("&contactPhoneNumber=");
            urlString.append(contactPhoneNumber);
        } else {
            Log.e("DM", "MoveStop: contactPhoneNumber is Nil/Empty");
        }
        if (isValidParameterString(contactEmail)) {
            urlString.append("&contactEmail=");
            urlString.append(contactEmail);
        } else {
            Log.e("DM", "MoveStopToPOI: contactEmail is Nil/Empty");
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "MoveStopToPOI", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for MoveStopToPOI: " + object.toString());
                    DeliveryStopActionResult result = new DeliveryStopActionResult();
                    result.readFromJSONObject(object);
                    listener.moveStopForDeliveryId(result);
                } else {
                    Log.d("DM", "Null object response for MoveStopToPOI");
                }
            }
        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: MoveStopToPOI");
    }

    public void moveStopToPOIForDeliveryId(String deliveryId, Integer deliveryStopId, String poiId, Integer radius,
                                           boolean hasAddress, AddressComponents address, boolean hasContact, String contactName,
                                           String contactPhoneNumber, String contactEmail, final OnCompleteListeners.moveStopToPOIForDeliveryIdListener listener) {


        Log.v("DM", "Enter Function: MoveStopToPOI");
        final OnCompleteListeners.moveStopToPOIForDeliveryIdListener MoveStopToPOI = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/RouteShipment2/MoveStopToPOI");

        if (isValidParameterString(deliveryId)) {
            urlString.append("?deliveryId=");
            urlString.append(deliveryId);
        } else {
            Log.e("DM", "MoveStopToPOI: deliveryId is Nil/Empty");
        }

        urlString.append("&deliveryStopId=");
        urlString.append(String.format("%d", deliveryStopId));

        if (isValidParameterString(poiId)) {
            urlString.append("&poiId=");
            urlString.append(poiId);
        } else {
            Log.e("DM", "MoveStopToPOI: deliveryId is Nil/Empty");
        }

        if (radius > 0) {
            urlString.append("&radius=");
            urlString.append(String.format("%d", radius));

        }

        if (hasAddress == Boolean.TRUE) {
            if (address != null) {
                urlString.append("&hasAddress=");
                urlString.append("1");


                if (address.Address1 != null) {
                    urlString.append("&address1=");
                    urlString.append(address.Address1);
                }
                if (address.Address2 != null) {
                    urlString.append("&address2=");
                    urlString.append(address.Address2);
                }
                if (address.City != null) {
                    urlString.append("&city=");
                    urlString.append(address.City);
                }
                if (address.Province != null) {
                    urlString.append("&province=");
                    urlString.append(address.Province);
                }
                if (address.Postal != null) {
                    urlString.append("&postal=");
                    urlString.append(address.Postal);
                }
                if (address.Country != null) {
                    urlString.append("&country=");
                    urlString.append(address.Country);
                }
                if (address.StreetName != null) {
                    urlString.append("&streetName=");
                    urlString.append(address.StreetName);
                }
                if (address.StreetNumber != null) {
                    urlString.append("&streetNumber=");
                    urlString.append(address.StreetNumber);
                }
            } else {
                Log.d("DM", "MoveStopToPOI: HasAddress parameter is set to TRUE But 'address' parameter is nil");
            }
        }

        if (hasContact == true) {
            urlString.append("&hasContact=");
            urlString.append("1");

            if (isValidParameterString(contactName)) {
                urlString.append("&contactName=");
                urlString.append(contactName);
            } else {
                Log.d("DM", "MoveStopToPOI: contactName parameter is set to TRUE But 'comment' parameter is nil");
            }
        }

        if (isValidParameterString(contactPhoneNumber)) {
            urlString.append("&contactPhoneNumber=");
            urlString.append(contactPhoneNumber);
        } else {
            Log.e("DM", "MoveStopToPOI: contactPhoneNumber is Nil/Empty");
        }
        if (isValidParameterString(contactEmail)) {
            urlString.append("&contactEmail=");
            urlString.append(contactEmail);
        } else {
            Log.e("DM", "MoveStopToPOI: contactEmail is Nil/Empty");
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "MoveStopToPOI", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for MoveStopToPOI: " + object.toString());
                    DeliveryStopActionResult result = new DeliveryStopActionResult();
                    result.readFromJSONObject(object);
                    listener.moveStopForDeliveryId(result);
                } else {
                    Log.d("DM", "Null object response for MoveStopToPOI");
                }
            }
        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: MoveStopToPOI");
    }


    public void changeStopAddressForDeliveryId(String deliveryId, Integer deliveryStopId, boolean hasAddress, AddressComponents address,
                                               boolean hasContact, String contactName, String contactPhoneNumber, String contactEmail, final OnCompleteListeners.onChangeStopAddressForDeliveryIdListener listener) {


        Log.v("DM", "Enter Function: changeStopAddressForDeliveryId");
        final OnCompleteListeners.onChangeStopAddressForDeliveryIdListener ChangeStopAddress = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/RouteShipment2/ChangeStopAddress");

        if (isValidParameterString(deliveryId)) {
            urlString.append("?deliveryId=");
            urlString.append(deliveryId);
        } else {
            Log.e("DM", "changeStopAddressForDeliveryId: deliveryId is Nil/Empty");
        }

        urlString.append("&deliveryStopId=");
        urlString.append(String.format("%d", deliveryStopId));


        if (hasAddress == Boolean.TRUE) {
            if (address != null) {
                urlString.append("&hasAddress=");
                urlString.append("1");


                if (address.Address1 != null) {
                    urlString.append("&address1=");
                    urlString.append(address.Address1);
                }
                if (address.Address2 != null) {
                    urlString.append("&address2=");
                    urlString.append(address.Address2);
                }
                if (address.City != null) {
                    urlString.append("&city=");
                    urlString.append(address.City);
                }
                if (address.Province != null) {
                    urlString.append("&province=");
                    urlString.append(address.Province);
                }
                if (address.Postal != null) {
                    urlString.append("&postal=");
                    urlString.append(address.Postal);
                }
                if (address.Country != null) {
                    urlString.append("&country=");
                    urlString.append(address.Country);
                }
                if (address.StreetName != null) {
                    urlString.append("&streetName=");
                    urlString.append(address.StreetName);
                }
                if (address.StreetNumber != null) {
                    urlString.append("&streetNumber=");
                    urlString.append(address.StreetNumber);
                }
            } else {
                Log.d("DM", "changeStopAddressForDeliveryId: HasAddress parameter is set to TRUE But 'address' parameter is nil");
            }
        }

        if (hasContact == true) {
            urlString.append("&hasContact=");
            urlString.append("1");

            if (isValidParameterString(contactName)) {
                urlString.append("&contactName=");
                urlString.append(contactName);
            } else {
                Log.d("DM", "changeStopAddressForDeliveryId: contactName parameter is set to TRUE But 'comment' parameter is nil");
            }
        }

        if (isValidParameterString(contactPhoneNumber)) {
            urlString.append("&contactPhoneNumber=");
            urlString.append(contactPhoneNumber);
        } else {
            Log.e("DM", "changeStopAddressForDeliveryId: contactPhoneNumber is Nil/Empty");
        }
        if (isValidParameterString(contactEmail)) {
            urlString.append("&contactEmail=");
            urlString.append(contactEmail);
        } else {
            Log.e("DM", "changeStopAddressForDeliveryId: contactEmail is Nil/Empty");
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "ChangeStopAddress", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for changeStopAddressForDeliveryId: " + object.toString());
                    DeliveryStopActionResult result = new DeliveryStopActionResult();
                    result.readFromJSONObject(object);
                    listener.changeStopAddressForDeliveryId(result);
                } else {
                    Log.d("DM", "Null object response for changeStopAddressForDeliveryId");
                }
            }
        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: changeStopAddressForDeliveryId");
    }

    public void changeDeliveryInstructionsForDeliveryId(String deliveryId, Integer deliveryStopId, final OnCompleteListeners.onChangeDeliveryInstructionsForDeliveryIdListener listener) {


        Log.v("DM", "Enter Function: changeDeliveryInstructionsForDeliveryId");
        final OnCompleteListeners.onChangeDeliveryInstructionsForDeliveryIdListener ChangeDeliveryInstructions = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/RouteShipment2/ChangeDeliveryInstructions");

        if (isValidParameterString(deliveryId)) {
            urlString.append("?deliveryId=");
            urlString.append(deliveryId);
        } else {
            Log.e("DM", "changeDeliveryInstructionsForDeliveryId: deliveryId is Nil/Empty");
        }

        urlString.append("&deliveryStopId=");
        urlString.append(String.format("%d", deliveryStopId));


        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "ChangeDeliveryInstructions", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for changeDeliveryInstructionsForDeliveryId: " + object.toString());
                    DeliveryStopActionResult result = new DeliveryStopActionResult();
                    result.readFromJSONObject(object);
                    listener.changeDeliveryInstructionsForDeliveryId(result);
                } else {
                    Log.d("DM", "Null object response for changeDeliveryInstructionsForDeliveryId");
                }
            }
        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: changeDeliveryInstructionsForDeliveryId");
    }

    public void setPOIAddressFromClientForPOIId(String poiId, String deliveryId, AddressComponents address, final OnCompleteListeners.setPOIAddressFromClientForPOIIdListener listener) {


        Log.v("DM", "Enter Function: setPOIAddressFromClientForPOIId");
        final OnCompleteListeners.setPOIAddressFromClientForPOIIdListener SetPOIAddressFromClient = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/RouteShipment2/SetPOIAddressFromClient");

        if (isValidParameterString(deliveryId)) {
            urlString.append("?deliveryId=");
            urlString.append(deliveryId);
        } else {
            Log.e("DM", "setPOIAddressFromClientForPOIId: deliveryId is Nil/Empty");
        }
        if (isValidParameterString(poiId)) {
            urlString.append("&poiId=");
            urlString.append(poiId);
        } else {
            Log.e("DM", "setPOIAddressFromClientForPOIId: poiId is Nil/Empty");
        }

        if (address != null) {

            if (address.Address1 != null) {
                urlString.append("&address1=");
                urlString.append(address.Address1);
            }
            if (address.Address2 != null) {
                urlString.append("&address2=");
                urlString.append(address.Address2);
            }
            if (address.City != null) {
                urlString.append("&city=");
                urlString.append(address.City);
            }
            if (address.Province != null) {
                urlString.append("&province=");
                urlString.append(address.Province);
            }
            if (address.Postal != null) {
                urlString.append("&postal=");
                urlString.append(address.Postal);
            }
            if (address.Country != null) {
                urlString.append("&country=");
                urlString.append(address.Country);
            }

        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "SetPOIAddressFromClient", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for setPOIAddressFromClientForPOIId: " + object.toString());
                    DeliveryPOIActionResult result = new DeliveryPOIActionResult();
                    result.readFromJSONObject(object);
                    listener.setPOIAddressFromClientForPOIId(result);
                } else {
                    Log.d("DM", "Null object response for setPOIAddressFromClientForPOIId");
                }
            }
        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: setPOIAddressFromClientForPOIId");
    }

    public void moveSiteWithSiteId(String siteId, Double latitude, Double longitude, Integer radius, boolean hasAddress,
                                   AddressComponents address, boolean hasContact, String contactName, String contactPhoneNumber, String contactEmail, final OnCompleteListeners.onMoveSiteWithSiteIdListener listener) {


        Log.v("DM", "Enter Function: setPOIAddressFromClientForPOIId");
        final OnCompleteListeners.onMoveSiteWithSiteIdListener moveSite = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/Site/Move");

        if (isValidParameterString(siteId)) {
            urlString.append("?siteId=");
            urlString.append(siteId);
        } else {
            Log.e("DM", "moveSiteWithSiteId: siteId is Nil/Empty");
        }

        urlString.append("&latitude=");
        urlString.append(String.format("%f", latitude));
        urlString.append("&longitude=");
        urlString.append(String.format("%f", longitude));
        urlString.append("&radius=");
        urlString.append(String.format("%d", radius));


        if (hasAddress == Boolean.TRUE) {
            if (address != null) {
                urlString.append("&hasAddress=");
                urlString.append("1");


                if (address.Address1 != null) {
                    urlString.append("&address1=");
                    urlString.append(address.Address1);
                }
                if (address.Address2 != null) {
                    urlString.append("&address2=");
                    urlString.append(address.Address2);
                }
                if (address.City != null) {
                    urlString.append("&city=");
                    urlString.append(address.City);
                }
                if (address.Province != null) {
                    urlString.append("&province=");
                    urlString.append(address.Province);
                }
                if (address.Postal != null) {
                    urlString.append("&postal=");
                    urlString.append(address.Postal);
                }
                if (address.Country != null) {
                    urlString.append("&country=");
                    urlString.append(address.Country);
                }

            } else {
                Log.d("DM", "moveSiteWithSiteId: HasAddress parameter is set to TRUE But 'address' parameter is nil");
            }
        }

        if (hasContact == true) {
            urlString.append("&hasContact=");
            urlString.append("1");

            if (isValidParameterString(contactName)) {
                urlString.append("&contactName=");
                urlString.append(contactName);
            } else {
                Log.d("DM", "moveSiteWithSiteId: contactName parameter is set to TRUE But 'comment' parameter is nil");
            }
        }

        if (isValidParameterString(contactPhoneNumber)) {
            urlString.append("&contactPhoneNumber=");
            urlString.append(contactPhoneNumber);
        } else {
            Log.e("DM", "moveSiteWithSiteId: contactPhoneNumber is Nil/Empty");
        }
        if (isValidParameterString(contactEmail)) {
            urlString.append("&contactEmail=");
            urlString.append(contactEmail);
        } else {
            Log.e("DM", "moveSiteWithSiteId: contactEmail is Nil/Empty");
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "moveSite", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for moveSiteWithSiteId: " + object.toString());
                    SiteResult result = new SiteResult();
                    result.readFromJSONObject(object);
                    listener.moveSiteWithSiteId(result);
                } else {
                    Log.d("DM", "Null object response for moveSiteWithSiteId");
                }
            }
        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: moveSiteWithSiteId");
    }

    public void changeSiteAddressForSiteId(String siteId, boolean hasAddress, AddressComponents address, boolean hasContact, String contactName,
                                           String contactPhoneNumber, String contactEmail, final OnCompleteListeners.onChangeSiteAddressForSiteIdListener listener) {


        Log.v("DM", "Enter Function: setPOIAddressFromClientForPOIId");
        final OnCompleteListeners.onChangeSiteAddressForSiteIdListener ChangeAddress = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/Site/ChangeAddress");

        if (isValidParameterString(siteId)) {
            urlString.append("?siteId=");
            urlString.append(siteId);
        } else {
            Log.e("DM", "changeSiteAddressForSiteId: siteId is Nil/Empty");
        }


        if (hasAddress == Boolean.TRUE) {
            if (address != null) {
                urlString.append("&hasAddress=");
                urlString.append("1");


                if (address.Address1 != null) {
                    urlString.append("&address1=");
                    urlString.append(address.Address1);
                }
                if (address.Address2 != null) {
                    urlString.append("&address2=");
                    urlString.append(address.Address2);
                }
                if (address.City != null) {
                    urlString.append("&city=");
                    urlString.append(address.City);
                }
                if (address.Province != null) {
                    urlString.append("&province=");
                    urlString.append(address.Province);
                }
                if (address.Postal != null) {
                    urlString.append("&postal=");
                    urlString.append(address.Postal);
                }
                if (address.Country != null) {
                    urlString.append("&country=");
                    urlString.append(address.Country);
                }

            } else {
                Log.d("DM", "changeSiteAddressForSiteId: HasAddress parameter is set to TRUE But 'address' parameter is nil");
            }
        }

        if (hasContact == true) {
            urlString.append("&hasContact=");
            urlString.append("1");

            if (isValidParameterString(contactName)) {
                urlString.append("&contactName=");
                urlString.append(contactName);
            } else {
                Log.d("DM", "changeSiteAddressForSiteId: contactName parameter is set to TRUE But 'comment' parameter is nil");
            }
        }

        if (isValidParameterString(contactPhoneNumber)) {
            urlString.append("&contactPhoneNumber=");
            urlString.append(contactPhoneNumber);
        } else {
            Log.e("DM", "changeSiteAddressForSiteId: contactPhoneNumber is Nil/Empty");
        }
        if (isValidParameterString(contactEmail)) {
            urlString.append("&contactEmail=");
            urlString.append(contactEmail);
        } else {
            Log.e("DM", "changeSiteAddressForSiteId: contactEmail is Nil/Empty");
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "ChangeAddress", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for changeSiteAddressForSiteId: " + object.toString());
                    SiteResult result = new SiteResult();
                    result.readFromJSONObject(object);
                    listener.changeSiteAddressForSiteId(result);
                } else {
                    Log.d("DM", "Null object response for changeSiteAddressForSiteId");
                }
            }
        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: changeSiteAddressForSiteId");
    }

    public void changeDeliveryInstructionsForSiteId(String siteId, String deliveryInstructions, final OnCompleteListeners.onChangeDeliveryInstructionsForSiteIdListener listener) {


        Log.v("DM", "Enter Function: changeDeliveryInstructionsForSiteId");
        final OnCompleteListeners.onChangeDeliveryInstructionsForSiteIdListener ChangeDeliveryInstructions = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/Site/ChangeDeliveryInstructions");

        if (isValidParameterString(siteId)) {
            urlString.append("?siteId=");
            urlString.append(siteId);
        } else {
            Log.e("DM", "changeDeliveryInstructionsForSiteId: siteId is Nil/Empty");
        }
        if (isValidParameterString(deliveryInstructions)) {
            urlString.append("&deliveryInstructions=");
            urlString.append(deliveryInstructions);
        } else {
            Log.e("DM", "changeDeliveryInstructionsForSiteId: deliveryInstructions is Nil/Empty");
        }


        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "ChangeDeliveryInstructions", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for changeDeliveryInstructionsForSiteId: " + object.toString());
                    SiteResult result = new SiteResult();
                    result.readFromJSONObject(object);
                    listener.changeDeliveryInstructionsForSiteId(result);
                } else {
                    Log.d("DM", "Null object response for changeDeliveryInstructionsForSiteId");
                }
            }
        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: changeDeliveryInstructionsForSiteId");
    }

    public void confirmRouteWithDeliveryId(String deliveryId, String tractorId, String trailerId, String driverId, boolean includesCoDriver,
                                           String coDriverId, Date loadTime, Date dispatchTime, int tractorOdometer, double tractorFuelQuantity, double tractorFuelCost,
                                           int trailerOdometer, double trailerFuelQuantity, double trailerFuelCost, String deviceType, String serialNumber, final OnCompleteListeners.onConfirmRouteWithDeliveryIdListener listener) {


        Log.v("DM", "Enter Function: ConfirmRoute");
        final OnCompleteListeners.onConfirmRouteWithDeliveryIdListener ConfirmRoute = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/RouteShipment2/ConfirmRoute");

        if (isValidParameterString(deliveryId)) {
            urlString.append("?deliveryId=");
            urlString.append(deliveryId);
        } else {
            Log.e("DM", "confirmRouteWithDeliveryId: deliveryId is Nil/Empty");
        }
        if (isValidParameterString(tractorId)) {
            urlString.append("&tractorId=");
            urlString.append(tractorId);
        } else {
            Log.e("DM", "confirmRouteWithDeliveryId: tractorId is Nil/Empty");
        }
        if (isValidParameterString(trailerId)) {
            urlString.append("&trailerId=");
            urlString.append(trailerId);
        } else {
            Log.e("DM", "confirmRouteWithDeliveryId: trailerId is Nil/Empty");
        }
        if (isValidParameterString(driverId)) {
            urlString.append("&driverId=");
            urlString.append(driverId);
        } else {
            Log.e("DM", "confirmRouteWithDeliveryId: driverId is Nil/Empty");
        }
        if (includesCoDriver) {
            urlString.append("&includesCoDriver=");
            urlString.append("true");
            urlString.append("&coDriverId=");
            urlString.append(coDriverId);
        } else {
            urlString.append("&includesCoDriver=");
            urlString.append("false");
        }
        if (loadTime != null) {
            urlString.append("&loadTime=");
            //  [parameterString appendString:[NSDate getISOFormatDate:loadTime]];
            urlString.append(loadTime);
        }
        if (dispatchTime != null) {
            urlString.append("&dispatchTime=");
            //  [parameterString appendString:[NSDate getISOFormatDate:dispatchTime]];
            urlString.append(getUTCFormatDate(dispatchTime));
        }
        if (tractorOdometer >= 0) {
            urlString.append("&tractorOdometer=");
            urlString.append(String.format("%d", tractorOdometer));

        }
        if (trailerOdometer >= 0) {
            urlString.append("&trailerOdometer=");
            urlString.append(String.format("%d", trailerOdometer));
        }
        if (tractorFuelQuantity >= 0) {
            urlString.append("&tractorFuelQuantity=");
            urlString.append(String.format("%f", tractorFuelQuantity));
        }
        if (tractorFuelCost >= 0) {
            urlString.append("&tractorFuelCost=");
            urlString.append(String.format("%f", tractorFuelCost));
        }
        if (trailerFuelQuantity >= 0) {
            urlString.append("&trailerFuelQuantity=");
            urlString.append(String.format("%f", trailerFuelQuantity));
        }
        if (trailerFuelCost >= 0) {
            urlString.append("&trailerFuelCost=");
            urlString.append(String.format("%f", trailerFuelCost));
        }

        if (deviceType != null) {
            urlString.append("&deviceType=");
            urlString.append(deviceType);
        }
        if (serialNumber != null) {
            urlString.append("&serialNumber=");
            urlString.append(serialNumber);
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));

                        Log.v("DM", "Received JSON Object for ConfirmRoute: " + object.toString());
                        DeliveryActionResult result = new DeliveryActionResult();
                        JSONObject jsonObject = (JSONObject) object;
                        result.readFromJSONObject(jsonObject);
                        listener.confirmRouteWithDeliveryId(result, null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.confirmRouteWithDeliveryId(null, error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException ConfirmRoute >> " + e.getMessage());
                    listener.confirmRouteWithDeliveryId(null, error);

                }
            }
        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: ConfirmRoute");
    }

    public void getTractorFleetForDC(String dcId, final OnCompleteListeners.getTractorFleetForDCListener listener) {


        Log.v("DM", "Enter Function: TractorFleetForDC");
        final OnCompleteListeners.getTractorFleetForDCListener TractorFleetForDC = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/DCSite/TractorFleetForDC");

        if (isValidParameterString(dcId)) {
            urlString.append("?dcId=");
            urlString.append(dcId);
        } else {
            Log.e("DM", "getTractorFleetForDC: Parameter DCId is not set");
        }
        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "TractorFleetForDC", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for getTractorFleetForDC: " + object.toString());
                    SdFleet result = new SdFleet();
                    result.readFromJSONObject(object);
                    listener.GetTractorFleetForDC(result);
                } else {
                    Log.d("DM", "Null object response for getTractorFleetForDC");
                }
            }
        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getTractorFleetForDC");
    }

    public void getTrailerFleetForDC(String dcId, final OnCompleteListeners.getTrailerFleetForDCListener listener) {


        Log.v("DM", "Enter Function: TrailerFleetForDC");
        final OnCompleteListeners.getTrailerFleetForDCListener TrailerFleetForDC = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/DCSite/TrailerFleetForDC");

        if (isValidParameterString(dcId)) {
            urlString.append("?dcId=");
            urlString.append(dcId);
        } else {
            Log.e("DM", "TrailerFleetForDC: Parameter DCId is not set");
        }
        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "TrailerFleetForDC", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for TrailerFleetForDC: " + object.toString());
                    SdFleet result = new SdFleet();
                    result.readFromJSONObject(object);
                    listener.GetTrailerFleetForDC(result);
                } else {
                    Log.d("DM", "Null object response for TrailerFleetForDC");
                }
            }
        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: TrailerFleetForDC");
    }

    public void getMostRecentDriverTimeLogForUserId(String userId, boolean includeActiveDelivery, final OnCompleteListeners.getMostRecentDriverTimeLogForUserIdListener listener) {


        Log.v("DM", "Enter Function: getMostRecentDriverTimeLogForUserId");
        final OnCompleteListeners.getMostRecentDriverTimeLogForUserIdListener MostRecentDTL = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/Driver/MostRecentDTL");

        if (isValidParameterString(userId)) {
            urlString.append("?userId=");
            urlString.append(userId);

            urlString.append("&includeActiveDelivery=");
            urlString.append(includeActiveDelivery ? "1" : "0");


        } else {
            Log.e("DM", "getMostRecentDriverTimeLogForUserId: userId DCId is nil, Cannot send request to Server");
        }
        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "MostRecentDTL", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for getMostRecentDriverTimeLogForUserId: " + object.toString());
                    DriverData result = new DriverData();
                    result.readFromJSONObject(object);
                    listener.getMostRecentDriverTimeLogForUserId(result);
                } else {
                    Log.d("DM", "Null object response for getMostRecentDriverTimeLogForUserId");
                }
            }
        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getMostRecentDriverTimeLogForUserId");
    }

    public void getPreviousDriverTimeLogForUserId(String userId, String shipmentId, boolean includeShipments, final OnCompleteListeners.getPreviousDriverTimeLogForUserIdListener listener) {


        Log.v("DM", "Enter Function: getPreviousDriverTimeLogForUserId");
        final OnCompleteListeners.getPreviousDriverTimeLogForUserIdListener HOSDataPrevious = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Driver/HOS/HOSDataPrevious");

        if (isValidParameterString(userId)) {
            urlString.append("/");
            urlString.append(userId);

            if (isValidParameterString(shipmentId)) {
                urlString.append("?shipmentId=");
                urlString.append(shipmentId);
            }

            urlString.append("&includeShipments=");
            urlString.append(includeShipments ? "true" : "false");


        } else {
            Log.e("DM", "getPreviousDriverTimeLogForUserId: userId is nil, Cannot send request to Server");
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "HOSDataPrevious", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for getPreviousDriverTimeLogForUserId: " + object.toString());
                    DriverData result = new DriverData();
                    result.readFromJSONObject(object);
                    listener.getPreviousDriverTimeLogForUserId(result);
                } else {
                    Log.d("DM", "Null object response for getPreviousDriverTimeLogForUserId");
                }
            }
        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getPreviousDriverTimeLogForUserId");
    }

    public void getNextDriverTimeLogForUserId(String userId, String shipmentId, boolean includeShipments, final OnCompleteListeners.getNextDriverTimeLogForUserIdListener listener) {


        Log.v("DM", "Enter Function: getNextDriverTimeLogForUserId");
        final OnCompleteListeners.getNextDriverTimeLogForUserIdListener HOSDataNext = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Driver/HOS/HOSDataNext");

        if (isValidParameterString(userId)) {
            urlString.append("/");
            urlString.append(userId);

            if (isValidParameterString(shipmentId)) {
                urlString.append("?shipmentId=");
                urlString.append(shipmentId);
            }

            urlString.append("&includeShipments=");
            urlString.append(includeShipments ? "true" : "false");


        } else {
            Log.e("DM", "getNextDriverTimeLogForUserId: userId is nil, Cannot send request to Server");
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "HOSDataNext", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for getNextDriverTimeLogForUserId: " + object.toString());
                    DriverData result = new DriverData();
                    result.readFromJSONObject(object);
                    listener.getNextDriverTimeLogForUserId(result);
                } else {
                    Log.d("DM", "Null object response for getNextDriverTimeLogForUserId");
                }
            }
        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getNextDriverTimeLogForUserId");
    }

    public void getAllDriverTimeLogsByWorkDayForUserId(String userId, Date timestamp, Integer span, boolean includeShipments, final OnCompleteListeners.getAllDriverTimeLogsByWorkDayForUserIdListener listener) {


        Log.v("DM", "Enter Function: getAllDriverTimeLogsByWorkDayForUserId");
        final OnCompleteListeners.getAllDriverTimeLogsByWorkDayForUserIdListener HOSDataByWorkday = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/Driver/HOS/HOSDataByWorkday");

        if (isValidParameterString(userId)) {
            urlString.append("?userid=");
            urlString.append(userId);

            urlString.append("&includeShipments=");
            urlString.append(includeShipments ? "true" : "false");

            urlString.append("&timestamp=");
            //[urlString appendString:[NSDate getISOFormatDate:timestamp]];
            urlString.append(timestamp);

            urlString.append("&span=");
            urlString.append(String.format("%d", span));
        } else {
            Log.e("DM", "getAllDriverTimeLogsByWorkDayForUserId: userId is nil, Cannot send request to Server");
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "HOSDataByWorkday", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for getAllDriverTimeLogsByWorkDayForUserId: " + object.toString());
                    DriverData result = new DriverData();
                    result.readFromJSONObject(object);
                    listener.getAllDriverTimeLogsByWorkDayForUserId(result);
                } else {
                    Log.d("DM", "Null object response for getAllDriverTimeLogsByWorkDayForUserId");
                }
            }
        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getAllDriverTimeLogsByWorkDayForUserId");
    }

    public void addEmployeeDailyScheduleForUserId(String userId, String siteId, Date timestamp, Integer scheduled, ScheduleEmployee.EmployeeAvailability availability, final OnCompleteListeners.addEmployeeDailyScheduleForUserIdListener listener) {


        Log.v("DM", "Enter Function: addEmployeeDailySchedule");
        final OnCompleteListeners.addEmployeeDailyScheduleForUserIdListener addEmployeeDailySchedule = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Driver/Schedule/Daily");

        if (isValidParameterString(userId)) {
            urlString.append("/");
            urlString.append(userId);
        } else {
            Log.e("DM", "addEmployeeDailyScheduleForUserId: userId is nil, Cannot send request to Server");
        }
        if (isValidParameterString(siteId)) {
            urlString.append("?siteId=");
            urlString.append(siteId);
        } else {
            Log.e("DM", "addEmployeeDailyScheduleForUserId: siteId is nil, Cannot send request to Server");
        }
        if (timestamp != null) {
            // NSString *timestampStr = [NSDate getISOFormatShortDate:timestamp];
            urlString.append("&timestamp=");
            urlString.append(timestamp);
        } else {
            Log.e("DM", "addEmployeeDailyScheduleForUserId: Timestamp is Nil or Empty");
        }

        urlString.append("&scheduled=");
        urlString.append(String.format("%ld", Long.parseLong(scheduled.toString())));

        urlString.append("&availability=");
        urlString.append(String.format("%ld", Long.parseLong(availability.toString())));


        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "addEmployeeDailySchedule", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override

            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for addEmployeeDailySchedule: " + object.toString());
                    listener.addEmployeeDailyScheduleForUserId();
                } else {
                    Log.d("DM", "Null object response for addEmployeeDailySchedule");
                }
            }
        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: addEmployeeDailySchedule");
    }

    public void completeTMSWithDeliveryID(String deliveryId, Date timestamp, final OnCompleteListeners.onCompleteTMSWithDeliveryIDListener listener) {


        Log.v("DM", "Enter Function: completeTMSWithDeliveryID");
        final OnCompleteListeners.onCompleteTMSWithDeliveryIDListener TMSComplete = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/RouteShipment2/TMSComplete");

        StringBuilder parameterString = new StringBuilder();

        if (isValidParameterString(deliveryId)) {
            parameterString.append("?deliveryId=");
            parameterString.append(deliveryId);

        } else {
            Log.e("DM", "completeTMSWithDeliveryID: userId is nil, Cannot send request to Server");
        }

        // NSString* tmsCompletedStr = [NSDate getISOFormatDate:timestamp];
        String tmsCompletedStr = getUTCFormatDate(timestamp);
        if (isValidParameterString(tmsCompletedStr)) {
            parameterString.append("&tmsCompleted=");
            parameterString.append(tmsCompletedStr);

        } else {
            Log.e("DM", "completeTMSWithDeliveryID: userId is nil, Cannot send request to Server");
        }

        urlString.append(parameterString);
        Log.v("DM", "Create Form  API Parameters: " + parameterString);

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initializePOSTRequest(this.authHeaderValue, parameterString.toString(), new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for completeTMSWithDeliveryID: " + object.toString());
                        DeliveryActionResult result = new DeliveryActionResult();
                        JSONObject jsonObject = (JSONObject) object;
                        result.readFromJSONObject(jsonObject);
                        listener.completeTMSWithDeliveryID(result, null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.completeTMSWithDeliveryID(null, error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException completeTMSWithDeliveryID >> " + e.getMessage());
                    listener.completeTMSWithDeliveryID(null, error);

                }
            }
        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: completeTMSWithDeliveryID");
    }

    public void adjustDriverAssignmentForUserID(String userId, String assignTodeliveryID, String unassignFromDeliveryID, final OnCompleteListeners.adjustDriverAssignmentForUserIDListener listener) {


        Log.v("DM", "Enter Function: adjustDriverAssignmentForUserID");
        final OnCompleteListeners.adjustDriverAssignmentForUserIDListener AdjustDriverAssignment = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/RouteShipment2/AdjustDriverAssignment");

        if (isValidParameterString(userId)) {
            urlString.append("?userId=");
            urlString.append(userId);
        } else {
            Log.e("DM", "adjustDriverAssignmentForUserID: userId is nil, Cannot send request to Server");
        }

        if (isValidParameterString(assignTodeliveryID)) {
            urlString.append("&deliveryId=");
            urlString.append(assignTodeliveryID);
        } else {
            Log.e("DM", "adjustDriverAssignmentForUserID: deliveryId is nil, Cannot send request to Server");
        }

        if (isValidParameterString(unassignFromDeliveryID)) {
            urlString.append("&unassignDeliveryId=");
            urlString.append(unassignFromDeliveryID);
        } else {
            Log.e("DM", "adjustDriverAssignmentForUserID: unassignFromDeliveryID is nil, Cannot send request to Server");
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for adjustDriverAssignmentForUserID: " + object.toString());
                        listener.adjustDriverAssignmentForUserID(null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.adjustDriverAssignmentForUserID(error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for adjustDriverAssignmentForUserID" + e.getMessage());
                    listener.adjustDriverAssignmentForUserID(error);

                }
            }
        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: adjustDriverAssignmentForUserID");
    }

    public void getLoadInfoForDeliveryId(String deliveryId, final OnCompleteListeners.getLoadInfoForDeliveryIdListener listener) {

        Log.v("DM", "Enter Function: getLoadInfoForDeliveryId");
        final OnCompleteListeners.getLoadInfoForDeliveryIdListener GetLoadInfo = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/LoadInfo/GetLoadInfo");

        if (isValidParameterString(deliveryId)) {
            urlString.append("?deliveryId=");
            urlString.append(deliveryId);

        } else {
            Log.e("DM", "getLoadInfoForDeliveryId: deliveryId is nil, Cannot send request to Server");
        }


        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "GetLoadInfo", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for getLoadInfoForDeliveryId: " + object.toString());
                    LoadInfoDTO result = new LoadInfoDTO();
                    result.readFromJSONObject(object);
                    listener.getLoadInfoForDeliveryId(result);
                } else {
                    Log.d("DM", "Null object response for getLoadInfoForDeliveryId");
                }
            }
        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getLoadInfoForDeliveryId");
    }

    public void updateLoadInfoForDelivery(LoadInfoDTO updatedLoadInfo, final OnCompleteListeners.onUpdateLoadInfoForDeliveryListener listener) {

        Log.v("DM", "Enter Function: updateLoadInfoForDelivery");
        final OnCompleteListeners.onUpdateLoadInfoForDeliveryListener ModifyLoadInfo = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/LoadInfo/ModifyLoadInfo");

        if (updatedLoadInfo != null) {
//            NSDictionary *dictionary = [updatedLoadInfo dictionaryWithValuesForKeys];
//
//
//            //  NSString *parameterString = [super addQueryStringToUrlString:@"" withDictionary:dictionary];
//            NSString* parameterString = [super getPOSTRequestParameterStringForDictionary:dictionary];
//
//
//            DDLogVerbose(@"POSTData String:%@", parameterString);
//
//            NSData* bytes = [parameterString dataUsingEncoding:NSASCIIStringEncoding];
//
//
//            NSMutableURLRequest *request = [super makeRequestWithUrl:urlString];


        } else {
            Log.e("DM", "Null Argument:UpdatedLoadInfo, Cannot send request to Server");
        }


        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "ModifyLoadInfo", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for updateLoadInfoForDelivery: " + object.toString());
                    RouteShipmentResult result = new RouteShipmentResult();
                    result.readFromJSONObject(object);
                    listener.updateLoadInfoForDelivery(result);
                } else {
                    Log.d("DM", "Null object response for updateLoadInfoForDelivery");
                }
            }
        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: updateLoadInfoForDelivery");
    }


    public void getAssetTrackingDataForSite(String siteId, final OnCompleteListeners.getAssetTrackingDataForSiteListener listener) {

        Log.v("DM", "Enter Function: getAssetTrackingDataForSite");
        final OnCompleteListeners.getAssetTrackingDataForSiteListener AssetTracking = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/AssetTracking");

        if (isValidParameterString(siteId)) {
            urlString.append("?siteId=");
            urlString.append(siteId);
        } else {
            Log.e("DM", "getLoadInfoForDeliveryId: siteId is nil, Cannot send request to Server");
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue,  new OnCompleteListeners.OnRestConnectionCompleteListener(){
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for getAssetTrackingDataForSite: " + object.toString());
                        SdrFleetReport result = new SdrFleetReport();
                        JSONObject jsonObject = (JSONObject) object;
                        result.readFromJSONObject(jsonObject);
                        listener.getAssetTrackingDataForSite(result, null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.getAssetTrackingDataForSite(null, error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for getAssetTrackingDataForSite" + e.getMessage());
                    listener.getAssetTrackingDataForSite(null, error);

                }
            }
        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getAssetTrackingDataForSite");
    }

    public void getCurrentAssetTrackingStatusForDevice(String deviceId, final OnCompleteListeners.getCurrentAssetTrackingStatusForDeviceListener listener) {

        Log.v("DM", "Enter Function: getCurrentAssetTrackingStatusForDevice");
        final OnCompleteListeners.getCurrentAssetTrackingStatusForDeviceListener GetCurrentForDevice = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/AssetTracking/GetCurrentForDevice");

        if (isValidParameterString(deviceId)) {
            urlString.append("?deviceId=");
            urlString.append(deviceId);
        } else {
            Log.e("DM", "getCurrentAssetTrackingStatusForDevice: deviceId is nil, Cannot send request to Server");
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "GetCurrentForDevice", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for getCurrentAssetTrackingStatusForDevice: " + object.toString());
                    AssetTrackingDTO result = new AssetTrackingDTO();
                    result.readFromJSONObject(object);
                    listener.getCurrentAssetTrackingStatusForDevice(result);
                } else {
                    Log.d("DM", "Null object response for getCurrentAssetTrackingStatusForDevice");
                }
            }
        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getCurrentAssetTrackingStatusForDevice");
    }

    public void getTrailersInSiteForSiteId(String siteId, final OnCompleteListeners.getTrailersInSiteForSiteIdListener listener) {

        Log.v("DM", "Enter Function: getTrailersInSiteForSiteId");
        final OnCompleteListeners.getTrailersInSiteForSiteIdListener GetCurrentForDevice = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/AssetTracking/GetTrailersInSite");

        if (isValidParameterString(siteId)) {
            urlString.append("?siteId=");
            urlString.append(siteId);
        } else {
            Log.e("DM", "getTrailersInSiteForSiteId: siteId is nil, Cannot send request to Server");
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "GetCurrentForDevice", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                Log.v("DM", "Received JSON Object for getTrailersInSiteForSiteId: " + object.toString());
                SdrFleetReport result = new SdrFleetReport();
                result.readFromJSONObject(object);
                listener.getTrailersInSiteForSiteId(result);
            }
        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getTrailersInSiteForSiteId");
    }

    public void getTrailersInGroupWithGroupId(String groupId, final OnCompleteListeners.getTrailersInGroupWithGroupIdListener listener) {

        Log.v("DM", "Enter Function: getTrailersInGroupWithGroupId");
        final OnCompleteListeners.getTrailersInGroupWithGroupIdListener GetTrailersInGroup = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/AssetTracking/GetTrailersInGroup");

        if (isValidParameterString(groupId)) {
            urlString.append("?groupId=");
            urlString.append(groupId);
        } else {
            Log.e("DM", "GetTrailersInGroup: groupId is nil, Cannot send request to Server");
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "GetTrailersInGroup", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                Log.v("DM", "Received JSON Object for getTrailersInGroupWithGroupId: " + object.toString());
                SdrFleetReport result = new SdrFleetReport();
                result.readFromJSONObject(object);
                listener.getTrailersInGroupWithGroupId(result);
            }
        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getTrailersInGroupWithGroupId");
    }

    public void setAdditionalDeliveryTimeForDeliveryId(String deliveryId, int deliveryStopId, Date arrivalTime, Date departureTime, Integer reasonCode, final OnCompleteListeners.setAdditionalDeliveryTimeForDeliveryIdListener listener) {

        Log.v("DM", "Enter Function: SetAdditionalDeliveryTime");
        final OnCompleteListeners.setAdditionalDeliveryTimeForDeliveryIdListener SetAdditionalDeliveryTime = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/RouteShipment2/SetAdditionalDeliveryTime");

        StringBuilder parameterString = new StringBuilder();

        if (isValidParameterString(deliveryId)) {
            parameterString.append("?deliveryId=");
            parameterString.append(deliveryId);
        } else {
            Log.e("DM", "SetAdditionalDeliveryTime: deliveryId is nil, Cannot send request to Server");
        }
        if (deliveryStopId > 0) {
            parameterString.append("&deliveryStopId=");
            parameterString.append(String.format("%d", deliveryStopId));
        } else {
            Log.e("DM", "SetAdditionalDeliveryTime: deliveryStopId is nil, Cannot send request to Server");
        }

        if (arrivalTime != null) {
            parameterString.append("&arrival=");
            parameterString.append(getUTCFormatDate(arrivalTime));
        } else {
            Log.e("DM", "SetAdditionalDeliveryTime: Arrival is not set");
        }

        if (departureTime != null) {
            parameterString.append("&departure=");
            parameterString.append(getUTCFormatDate(departureTime));
        } else {
            Log.e("DM", "SetAdditionalDeliveryTime: departure is not set");
        }

        if (deliveryStopId > 0) {
            parameterString.append("&reasonCode=");
            parameterString.append(String.format("%d", reasonCode));
        }
        Log.v("DM", "Create Form  API Parameters:: " + parameterString);

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initializePOSTRequest(this.authHeaderValue, parameterString.toString(), new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for SetAdditionalDeliveryTime: " + object.toString());
                        listener.setAdditionalDeliveryTimeForDeliveryId(null);
                    } else if (object == null && error == null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for SetAdditionalDeliveryTime: " + object.toString());
                        listener.setAdditionalDeliveryTimeForDeliveryId(null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.setAdditionalDeliveryTimeForDeliveryId(error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for setAdditionalDeliveryTimeForDeliveryId" + e.getMessage());
                    listener.setAdditionalDeliveryTimeForDeliveryId(error);

                }
            }

        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: SetAdditionalDeliveryTime");
    }

    public void removeAdditionalDeliveryTimeForDeliveryId(String deliveryId, Integer deliveryStopId, final OnCompleteListeners.removeAdditionalDeliveryTimeForDeliveryIdListener listener) {

        Log.v("DM", "Enter Function: removeAdditionalDeliveryTimeForDeliveryId");
        final OnCompleteListeners.removeAdditionalDeliveryTimeForDeliveryIdListener removeAdditionalDeliveryTime = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/RouteShipment2/SetAdditionalDeliveryTime");

        if (isValidParameterString(deliveryId)) {
            urlString.append("?deliveryId=");
            urlString.append(deliveryId);
        } else {
            Log.e("DM", "removeAdditionalDeliveryTimeForDeliveryId: DeliveryId is Nil/Empty");
        }
        if (deliveryStopId > 0) {
            urlString.append("&deliveryStopId=");
            urlString.append(String.format("%d", deliveryStopId));
        } else {
            Log.e("DM", "removeAdditionalDeliveryTimeForDeliveryId: DeliveryStopId is not set");
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "removeAdditionalDeliveryTime", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for removeAdditionalDeliveryTimeForDeliveryId: " + object.toString());
                    listener.removeAdditionalDeliveryTimeForDeliveryId();
                } else {
                    Log.d("DM", "Null object response for removeAdditionalDeliveryTimeForDeliveryId");
                }
            }
        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: removeAdditionalDeliveryTimeForDeliveryId");
    }

    public void getRouteShipmentInvoiceItem(String routeShipmentInvoiceItemId, final OnCompleteListeners.getRouteShipmentInvoiceItemListener listener) {

        Log.v("DM", "Enter Function: getRouteShipmentInvoiceItem");
        final OnCompleteListeners.getRouteShipmentInvoiceItemListener RouteShipmentInvoiceItem = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/RouteShipmentInvoiceItem");

        if (isValidParameterString(routeShipmentInvoiceItemId)) {
            urlString.append("?RouteShipmentInvoiceItemId=");
            urlString.append(routeShipmentInvoiceItemId);
        } else {
            Log.e("DM", "getRouteShipmentInvoiceItem: RouteShipmentInvoiceItemId is Nil/Empty");
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "RouteShipmentInvoiceItem", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for getRouteShipmentInvoiceItem: " + object.toString());
                    RouteShipmentInvoiceItem result = new RouteShipmentInvoiceItem();
                    result.readFromJSONObject(object);
                    listener.getRouteShipmentInvoiceItem(result);
                } else {
                    Log.d("DM", "Null object response for getRouteShipmentInvoiceItem");
                }
            }
        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getRouteShipmentInvoiceItem");
    }

    public void getLoadInfo2ForDeliveryId(String deliveryId, final OnCompleteListeners.getLoadInfo2ForDeliveryIdListener listener, Error error) {

        Log.v("DM", "Enter Function: getLoadInfo2ForDeliveryId");
        final OnCompleteListeners.getLoadInfo2ForDeliveryIdListener GetLoadInfo = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/LoadInfo2/GetLoadInfo");

        if (isValidParameterString(deliveryId)) {
            urlString.append("?deliveryId=");
            urlString.append(deliveryId);
        } else {
            Log.e("DM", "getLoadInfo2ForDeliveryId: DeliveryId is nil, Cannot send request to Server");
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for getLoadInfo2ForDeliveryId: " + object.toString());
                        LoadInfo2DTO result = new LoadInfo2DTO();
                        JSONObject jsonObject = (JSONObject) object;
                        result.readFromJSONObject(jsonObject);
                        listener.getLoadInfo2ForDeliveryId(result, null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.getLoadInfo2ForDeliveryId(null, error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException getLoadInfo2ForDeliveryId >> " + e.getMessage());
                    listener.getLoadInfo2ForDeliveryId(null, error);

                }
            }
        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getLoadInfo2ForDeliveryId");
    }

    public void updateLoadInfo2ForDeliveryId(String deliveryId, LoadInfo2DTO updatedLoadInfo, final OnCompleteListeners.OnUpdateLoadInfo2ForDeliveryIdListener listener) {

        Log.v("DM", "Enter Function: updateLoadInfo2ForDeliveryId");
        final OnCompleteListeners.OnUpdateLoadInfo2ForDeliveryIdListener ModifyLoadInfo = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/LoadInfo2/ModifyLoadInfo");

        StringBuilder parameterString = new StringBuilder();


        if (updatedLoadInfo != null) {
            JSONObject jsonObjFB = new JSONObject(updatedLoadInfo.dictionaryWithValuesForKeys());
            String formBody = jsonObjFB.toString();
            String input = Base64Converter.toBase64(formBody);
            parameterString.append("&vehicleData=");
            parameterString.append(input);
        }


        Log.v("DM", "Create Form  API Parameters: " + parameterString);

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initializePOSTRequest(this.authHeaderValue, parameterString.toString(), new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for updateLoadInfo2ForDeliveryId: " + object.toString());
                        RouteShipmentResult result = new RouteShipmentResult();
                        JSONObject jsonObject = (JSONObject) object;
                        result.readFromJSONObject(jsonObject);
                        listener.updateLoadInfo2ForDeliveryId(result, null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.updateLoadInfo2ForDeliveryId(null, error);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException updateLoadInfo2ForDeliveryId >> " + e.getMessage());
                    listener.updateLoadInfo2ForDeliveryId(null, error);

                }
            }
        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: updateLoadInfo2ForDeliveryId");
    }


    public void setEORValuesWithShipmentId(String shipmentId, Integer truckOdometer, Integer trailerOdometer, final OnCompleteListeners.OnSetEORValuesWithShipmentIdListener listener) {

        Log.v("DM", "Enter Function: setEORValuesWithShipmentId");
        final OnCompleteListeners.OnSetEORValuesWithShipmentIdListener SetEORValues = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/RouteShipment2/SetEORValues");

        if (isValidParameterString(shipmentId)) {
            urlString.append("?shipmentId=");
            urlString.append(shipmentId);
        } else {
            Log.e("DM", "setEORValuesWithShipmentId: shipmentId is nil, Cannot send request to Server");
        }
        urlString.append("&truckOdometer=");
        urlString.append(truckOdometer);
        urlString.append("&trailerOdometer=");
        urlString.append(trailerOdometer);

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for setEORValuesWithShipmentId: " + object.toString());
                        DeliveryStopActionResult result = new DeliveryStopActionResult();
                        JSONObject jsonObject = (JSONObject) object;
                        result.readFromJSONObject(jsonObject);
                        listener.setEORValuesWithShipmentId(result, null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.setEORValuesWithShipmentId(null, error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException setEORValuesWithShipmentId >> " + e.getMessage());
                    listener.setEORValuesWithShipmentId(null, error);
                }

            }
        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: setEORValuesWithShipmentId");
    }

    /******************************************      iPimmBaseDataManager   *******************************************/

    public void getMessageWithMessageId(String messageId, final OnCompleteListeners.getMessageWithMessageIdListener listener) {

        Log.v("DM", "Enter Function: getMessageWithMessageId");
        final OnCompleteListeners.getMessageWithMessageIdListener getMessage = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Pimm/Message");

        //        if([self isValidParameterString:messageId] == NO)
        //        {
        //            DDLogVerbose(@"MessageId argument is Invalid. Throwing Exception");
        //            NSException* exception = [[NSException alloc]initWithName:NSInvalidArgumentException reason:@"MessageId Argument is Invalid/Empty" userInfo:Nil];
        //            @throw exception;
        //
        //        }
        //
        //        NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys: messageId, @"messageId", @"-1", @"status", @"-1", @"priority", nil];
        //
        //
        //        NSString *urlWithQueryString = [self addQueryStringToUrlString:urlString withDictionary:params];
        //
        //        DDLogDebug(@"URLString:%@", urlWithQueryString);
        //
        //
        //        NSMutableURLRequest *request = [self makeRequestWithUrl:urlWithQueryString];
        //

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "getMessage", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                Log.v("DM", "Received JSON Object for getMessageWithMessageId: " + object.toString());
                PimmMessage result = new PimmMessage();
                result.readFromJSONObject(object);
                listener.getMessageWithMessageId(result);
            }
        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getMessageWithMessageId");
    }


    public void createMessageForRecipientUserEmail(String recipientUserEmailAddress, String messageBody, Integer priority, PimmRefTypeEnum referenceType, String referenceId, final OnCompleteListeners.onCreateMessageForRecipientUserEmailListener listener) {

        Log.v("DM", "Enter Function: createMessageForRecipientUserEmail");
        final OnCompleteListeners.onCreateMessageForRecipientUserEmailListener createMessageUserEmail = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Pimm/Message");

//        NSData* data = [messageBody dataUsingEncoding:NSUTF16LittleEndianStringEncoding];
//        NSString* encodedMessageBody = [data base64EncodedString];
        String encodedMessageBody = Base64Converter.toBase64(messageBody);

        StringBuilder parameterString = new StringBuilder();

        if (isValidParameterString(recipientUserEmailAddress)) {
            parameterString.append("?recipient=");
            parameterString.append(recipientUserEmailAddress);
        } else {
            Log.e("DM", "createMessageForRecipientUserEmail: Missing/Empty/Invalid Parameter messageBody");
        }

        if (priority <= -1 || priority > 100) {
            priority = 0; //default priority

        }
        parameterString.append("&priority=");
        parameterString.append(String.format("%d", priority));
        parameterString.append("&status=-1");
        parameterString.append("&recipientType=0");

        Integer refType = Integer.parseInt(referenceType.toString());
        if (refType != -1) {
            parameterString.append("&refType=");
            parameterString.append(String.format("%ld", Long.parseLong(refType.toString())));

            parameterString.append("&refx=");
            parameterString.append(referenceId);
        } else {
            Log.e("DM", "createMessageForRecipientUserEmail: ReferenceId parameter is invalid/empty");
        }

        if (isValidParameterString(messageBody)) {
            parameterString.append("&body=");
            parameterString.append(encodedMessageBody);
        } else {
            Log.e("DM", "createMessageForRecipientUserEmail: Missing/Empty/Invalid Parameter messageBody");
        }
        Log.v("DM", "Create Form  API Parameters:: " + parameterString);


        RestConnection restConnection = new RestConnection();
        restConnection.initializePOSTRequest(this.authHeaderValue, parameterString.toString(), new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for createMessageForRecipientUserEmail: " + object.toString());
                        PimmMessage result = new PimmMessage();
                        JSONObject jsonObject = (JSONObject) object;
                        result.readFromJSONObject(jsonObject);
                        listener.createMessageForRecipientUserEmail(result, null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.createMessageForRecipientUserEmail(null, error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for createMessageForRecipientUserEmail" + e.getMessage());
                    listener.createMessageForRecipientUserEmail(null, error);

                }

            }
        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: createMessageForRecipientUserEmail");
    }

    public void createMessageForRecipientUserID(String recipientUserId, String messageBody, Integer priority, PimmRefTypeEnum referenceType, String referenceId, final OnCompleteListeners.onCreateMessageForRecipientUserIDListener listener) {

        Log.v("DM", "Enter Function: createMessageForRecipientUserID");
        final OnCompleteListeners.onCreateMessageForRecipientUserIDListener createMessageUserID = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Pimm/Message");

        String encodedMessageBody = Base64Converter.toBase64(messageBody);
        StringBuilder parameterString = new StringBuilder();

        if (isValidParameterString(recipientUserId)) {
            parameterString.append("?recipientID=");
            parameterString.append(recipientUserId);
        } else {
            Log.e("DM", "createMessageForRecipientUserID: RecipientUserId parameter is invalid/empty");
        }

        if (priority <= -1 || priority > 100) {
            priority = 0; //default priority

        }
        parameterString.append("&priority=");
        parameterString.append(String.format("%d", priority));
        parameterString.append("&status=-1");
        parameterString.append("&recipientType=0");

        Integer refType = Integer.parseInt(referenceType.toString());
        if (refType != -1) {
            parameterString.append("&refType=");
            parameterString.append(String.format("%ld", Long.parseLong(refType.toString())));

            parameterString.append("&refx=");
            parameterString.append(referenceId);
        } else {
            Log.e("DM", "createMessageForRecipientUserID: ReferenceId parameter is invalid/empty");
        }

        if (isValidParameterString(messageBody)) {
            parameterString.append("&body=");
            parameterString.append(encodedMessageBody);
        } else {
            Log.e("DM", "createMessageForRecipientUserID: Missing/Empty/Invalid Parameter messageBody");
        }

        Log.v("DM", "Create Form  API Parameters:: " + parameterString);

        RestConnection restConnection = new RestConnection();
        restConnection.initializePOSTRequest(this.authHeaderValue, parameterString.toString(), new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for createMessageForRecipientUserID: " + object.toString());
                        PimmMessage result = new PimmMessage();
                        JSONObject jsonObject = (JSONObject) object;
                        result.readFromJSONObject(jsonObject);
                        listener.createMessageForRecipientUserID(result, null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.createMessageForRecipientUserID(null, error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for createMessageForRecipientUserID" + e.getMessage());
                    listener.createMessageForRecipientUserID(null, error);

                }

            }
        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: createMessageForRecipientUserID");
    }

    public void createMessageForPimmObjectWithID(String pimmObjectId, PimmRefTypeEnum recipientType, String role, String messageBody, Integer priority,
                                                 PimmRefTypeEnum referenceType, String referenceId, final OnCompleteListeners.onCreateMessageForPimmObjectWithIDListener listener) {

        Log.v("DM", "Enter Function: createMessageForPimmObjectWithID");
        final OnCompleteListeners.onCreateMessageForPimmObjectWithIDListener createMessagePimmObjectWithID = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Pimm/Message");

        String encodedMessageBody = Base64Converter.toBase64(messageBody);
        if (priority <= -1 || priority > 100) {
            priority = 0; //default priority

        }
        Integer refType = null;
        if (referenceType != null) {
            refType = Integer.parseInt(referenceType.toString());
        }

        StringBuilder parameterString = new StringBuilder();

        parameterString.append("?priority=");
        parameterString.append(String.format("%d", priority));
        parameterString.append("&status=-1");

        if (isValidParameterString(pimmObjectId)) {
            parameterString.append("&recipientID=");
            parameterString.append(pimmObjectId);
        } else {
            Log.e("DM", "createMessageForPimmObjectWithID: RecipientUserId parameter is invalid/empty");
        }
        parameterString.append("&recipientType=");
        parameterString.append(String.format("%ld", Long.parseLong(recipientType.toString())));

        if (isValidParameterString(role)) {
            parameterString.append("&role=");
            parameterString.append(role);
        } else {
            Log.e("DM", "createMessageForPimmObjectWithID: role parameter is invalid/empty");
        }
        parameterString.append("&refType=");
        parameterString.append(String.format("%d", Integer.parseInt(refType.toString())));

        if (refType != -1) {

            parameterString.append("&refx=");
            parameterString.append(referenceId);
        } else {
            Log.e("DM", "createMessageForPimmObjectWithID: RefType is set but ReferenceId parameter is  not set");
        }

        if (isValidParameterString(messageBody)) {
            parameterString.append("&body=");
            parameterString.append(encodedMessageBody);
        } else {
            Log.e("DM", "createMessageForPimmObjectWithID: Missing/Empty/Invalid Parameter messageBody");
        }
        Log.v("DM", "Create Form  API Parameters:: " + parameterString);

        RestConnection restConnection = new RestConnection();
        restConnection.initializePOSTRequest(this.authHeaderValue, parameterString.toString(), new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for createMessageForPimmObjectWithID: " + object.toString());
                        PimmMessage result = new PimmMessage();
                        JSONObject jsonObject = (JSONObject) object;
                        result.readFromJSONObject(jsonObject);
                        listener.createMessageForPimmObjectWithID(result, null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.createMessageForPimmObjectWithID(null, error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for createMessageForPimmObjectWithID" + e.getMessage());
                    listener.createMessageForPimmObjectWithID(null, error);

                }
            }
        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: createMessageForPimmObjectWithID");
    }

    public void updateMessageWithMessageId(String messageId, PimmMessageStatusEnum.PimmMessagesStatusEnum status, final OnCompleteListeners.onUpdateMessageWithMessageIdListener listener) {

        Log.v("DM", "Enter Function: updateMessageWithMessageId");
        final OnCompleteListeners.onUpdateMessageWithMessageIdListener updateMessage = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Pimm/Message");

        StringBuilder parameterString = new StringBuilder();

        if (isValidParameterString(messageId)) {
            parameterString.append("?messageId=");
            parameterString.append(messageId);
        } else {
            Log.e("DM", "updateMessageWithMessageId: MessageId parameter is Invalid/Empty");
        }
        parameterString.append("&priority=-1");
        parameterString.append("&status=");
        parameterString.append(String.format("%ld", Long.parseLong(status.toString())));
        Log.v("DM", "Create Form  API Parameters:: " + parameterString);

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initializePOSTRequest(this.authHeaderValue, parameterString.toString(), new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for updateMessageWithMessageId: " + object.toString());
                        PimmMessage result = new PimmMessage();
                        JSONObject jsonObject = (JSONObject) object;
                        result.readFromJSONObject(jsonObject);
                        listener.updateMessageWithMessageId(result, null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.updateMessageWithMessageId(null, error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for updateMessageWithMessageId" + e.getMessage());
                    listener.updateMessageWithMessageId(null, error);

                }
            }
        });

       /* PimmMessage *message = [[PimmMessage alloc] init];

        [connection setJsonRootObject:message];

        [connection setObjectClassName:@"PimmMessage"];

        [connection setReqCompletionCallbackBlock:block];
        [self addRequestToQueue:connection];*/

        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: updateMessageWithMessageId");
    }

    public void deleteMessageWithMessageId(String messageId, final OnCompleteListeners.onDeleteMessageWithMessageIdListener listener) {

        Log.v("DM", "Enter Function: deleteMessageWithMessageId");
        final OnCompleteListeners.onDeleteMessageWithMessageIdListener deleteMessage = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Pimm/Message");

        StringBuilder parameterString = new StringBuilder();

        if (isValidParameterString(messageId)) {
            parameterString.append("?messageId=");
            parameterString.append(messageId);
            parameterString.append("&priority=-1");
            parameterString.append("&status=4");
        } else {
            Log.e("DM", "deleteMessageWithMessageId: MessageId parameter is Invalid/Empty");
        }
        Log.v("DM", "Create Form  API Parameters:: " + parameterString);


        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initializePOSTRequest(this.authHeaderValue, parameterString.toString(), new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for deleteMessageWithMessageId: " + object.toString());
                        listener.deleteMessageWithMessageId(null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.deleteMessageWithMessageId(error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for pushGPSEventForPOIWithDeliveryId" + e.getMessage());
                    listener.deleteMessageWithMessageId(error);

                }
            }
        });


        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: deleteMessageWithMessageId");
    }


    ////////////******* //Form APIs


    public void updateReferenceForPimmFormWithFormId(String formId, String referenceId, PimmRefTypeEnum refType, final OnCompleteListeners.onUpdateReferenceForPimmFormWithFormIdListener listener) {

        Log.v("DM", "Enter Function: updateReferenceForPimmFormWithFormId");
        final OnCompleteListeners.onUpdateReferenceForPimmFormWithFormIdListener updateReference = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("PimmForm/Form/SetReference");
        StringBuilder parameterString = new StringBuilder();

        if (isValidParameterString(formId)) {
            parameterString.append("?formId=");
            parameterString.append(formId);
            parameterString.append("&refx=");
            parameterString.append(referenceId);
            parameterString.append("&refType=");
            parameterString.append(String.format("%ld", Long.parseLong(refType.toString())));
        } else {
            Log.e("DM", "updateReferenceForPimmFormWithFormId: Missing/Empty/Invalid Parameter formId");
        }

        Log.v("DM", "Request URL: " + urlString);
        Log.v("DM", "Create Form  API Parameters:: " + parameterString);

        RestConnection restConnection = new RestConnection();
        restConnection.initializePOSTRequest(this.authHeaderValue, parameterString.toString(), new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for updateReferenceForPimmFormWithFormId: " + object.toString());
                        PimmForm result = new PimmForm();
                        JSONObject jsonObject = (JSONObject) object;
                        result.readFromJSONObject(jsonObject);
                        listener.updateReferenceForPimmFormWithFormId(result, null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.updateReferenceForPimmFormWithFormId(null, error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for updateReferenceForPimmFormWithFormId" + e.getMessage());
                    listener.updateReferenceForPimmFormWithFormId(null, error);

                }
            }
        });


        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: updateReferenceForPimmFormWithFormId");
    }

    public void getFormWithFormId(String formId, String appId, final OnCompleteListeners.getFormWithFormIdListener listener) {

        Log.v("DM", "Enter Function: getFormWithFormId");
        final OnCompleteListeners.getFormWithFormIdListener getForm = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Pimm/Forms");

        if (isValidParameterString(formId)) {
            urlString.append("?formId=");
            urlString.append(formId);
        } else {
            Log.e("DM", "getFormWithFormId: Missing/Empty/Invalid Parameter formId");
        }
        if (isValidParameterString(appId)) {
            urlString.append("&appId=");
            urlString.append(appId);

        } else {
            Log.e("DM", "getFormWithFormId: Missing/Empty/Invalid Parameter appId");
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for getFormWithFormId: " + object.toString());
                        PimmForm result = new PimmForm();
                        JSONObject jsonObject = (JSONObject) object;
                        result.readFromJSONObject(jsonObject);
                        listener.getFormWithFormId(result, null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.getFormWithFormId(null, error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for getFormWithFormId" + e.getMessage());
                    listener.getFormWithFormId(null, error);
                }
            }
        });


        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getFormWithFormId");
    }

    public void deleteFormWithFormId(String formId, String appId, final OnCompleteListeners.onDeleteFormWithFormIdListener listener) {

        Log.v("DM", "Enter Function: deleteFormWithFormId");
        final OnCompleteListeners.onDeleteFormWithFormIdListener deleteForm = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Pimm/Forms");

        if (isValidParameterString(formId)) {
            urlString.append("?formId=");
            urlString.append(formId);
        } else {
            Log.e("DM", "deleteFormWithFormId: Missing/Empty/Invalid Parameter formId");
        }
        if (isValidParameterString(appId)) {
            urlString.append("&appId=");
            urlString.append(appId);

        } else {
            Log.e("DM", "deleteFormWithFormId: Missing/Empty/Invalid Parameter appId");
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "getForm", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for deleteFormWithFormId: " + object.toString());
                    listener.deleteFormWithFormId();
                } else {
                    Log.d("DM", "Null object response for deleteFormWithFormId");
                }
            }
        });


        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: deleteFormWithFormId");
    }


    //public void createFormAttachmentWithFormId(String formId, byte[] attachmentContent,  String attachmentName, String attachmentType,  final OnCompleteListeners.oncreateFormAttachmentWithFormIdListener listener ) {
    public void createFormAttachmentWithFormId(String formId, String attachmentContent, String attachmentName, String attachmentType, final OnCompleteListeners.oncreateFormAttachmentWithFormIdListener listener) {

        Log.v("DM", "Enter Function: createFormAttachmentWithFormId");
        final OnCompleteListeners.oncreateFormAttachmentWithFormIdListener createFormAttachmentWithFormId = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Pimm/FormsAttachment");
        // NSString *input = [attachmentContent base64EncodedString];

        String input = Base64Converter.toBase64(attachmentContent);

        StringBuilder parameterString = new StringBuilder();

        if (isValidParameterString(formId)) {
            parameterString.append("?formId=");
            parameterString.append(formId);
            parameterString.append("&body=");
            parameterString.append(input);
        } else {
            Log.e("DM", "createFormAttachmentWithFormId: Missing/Empty/Invalid Parameter formId");
        }

        if (isValidParameterString(attachmentName)) {
            parameterString.append("&name=");
            parameterString.append(attachmentName);
        }
        if (isValidParameterString(attachmentType)) {
            parameterString.append("&type=");
            parameterString.append(attachmentType);
        }
        Log.v("DM", "Create Form  API Parameters:: " + parameterString);

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initializePOSTRequest(this.authHeaderValue, parameterString.toString(), new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for createFormAttachmentWithFormId: " + object.toString());
                        PimmFormAttachment result = new PimmFormAttachment();
                        JSONObject jsonObject = (JSONObject) object;
                        result.readFromJSONObject(jsonObject);
                        listener.createFormAttachmentWithFormId(result, null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.createFormAttachmentWithFormId(null, error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for createFormAttachmentWithFormId" + e.getMessage());
                    listener.createFormAttachmentWithFormId(null, error);

                }
            }
        });

//        Integer reqIdentifier = [self generateRequestIdentifier];
//
//        PimmFormAttachment *response = [[PimmFormAttachment alloc] init];
//
//        NSString* objectClassName = @"PimmFormAttachment";
//        BOOL isJSONResponse = true;
//
//        NSString* attachmentTypeStr = [[NSString alloc] initWithFormat:@"application/%@",attachmentType];
//
//        NSDictionary* filePathDic = [[NSDictionary alloc] initWithObjectsAndKeys:filePath,attachmentName, nil];
//        NSDictionary* attachmentTypeDic = [[NSDictionary alloc] initWithObjectsAndKeys:attachmentTypeStr,attachmentName, nil];


        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: createFormAttachmentWithFormId");
    }

    public void createBinaryFormAttachmentWithAttachmentId(String attachmentId, String formId, String filePath, String attachmentName, String attachmentType, final OnCompleteListeners.onCreateBinaryFormAttachmentWithAttachmentIdListener listener) {

        Log.v("DM", "Enter Function: createBinaryFormAttachmentWithAttachmentId");
        final OnCompleteListeners.onCreateBinaryFormAttachmentWithAttachmentIdListener createBinaryFormAttachment = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("PimmForm/FormAttachment/Binary");

        urlString.append("/");
        urlString.append(attachmentType);

        if (isValidParameterString(attachmentId)) {
            urlString.append("?attachmentId=");
            urlString.append(attachmentId);

        } else {
            Log.e("DM", "createBinaryFormAttachmentWithAttachmentId: Missing/Empty/Invalid Parameter formId");
        }


//        urlString.append("&formId=");
//        urlString.append(formId);
//        if(filePath != null){
//            urlString.append("&filePath=");
//            urlString.append(filePath);
//
//        }else{
//            Log.e("DM", "Invalid Parameter attachmentContent");
//        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for pushGPSEventForPOIWithDeliveryId: " + object.toString());
                        PimmFormAttachment result = new PimmFormAttachment();
                        JSONObject jsonObject = (JSONObject) object;
                        result.readFromJSONObject(jsonObject);
                        listener.createBinaryFormAttachmentWithAttachmentId(result, null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.createBinaryFormAttachmentWithAttachmentId(null, error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for createBinaryFormAttachmentWithAttachmentId" + e.getMessage());
                    listener.createBinaryFormAttachmentWithAttachmentId(null, error);

                }
            }
        });

//        int reqIdentifier = [self generateRequestIdentifier];
//
//        PimmFormAttachment *response = [[PimmFormAttachment alloc] init];
//
//        NSString* objectClassName = @"PimmFormAttachment";
//        BOOL isJSONResponse = true;
//
//        NSString* attachmentTypeStr = [[NSString alloc] initWithFormat:@"application/%@",attachmentType];
//
//        NSDictionary* filePathDic = [[NSDictionary alloc] initWithObjectsAndKeys:filePath,attachmentName, nil];
//        NSDictionary* attachmentTypeDic = [[NSDictionary alloc] initWithObjectsAndKeys:attachmentTypeStr,attachmentName, nil];
//
//    [self createFileUploadRequestWithUrlString:urlString RequestType:HTTP_REQUEST_TYPE_POST IsJSONResponse:isJSONResponse ResponseObject:response ResponseObjectArray:nil ResponseObjectClassName:objectClassName CallBack:block RequestName:[NSString stringWithFormat:@"%s", __FUNCTION__] RequestIdentifier:reqIdentifier Context:nil FileUploadList:filePathDic AttachmentTypeList:attachmentTypeDic ContentDispositionList:nil];
//
//
//
//        DDLogVerbose(@"Exit Function:%s",__FUNCTION__);
//        return reqIdentifier;

        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: createBinaryFormAttachmentWithAttachmentId");
    }

    public void getFormAttachmentWithFormId(String formId, String attachmentId, final OnCompleteListeners.getFormAttachmentWithFormIdListener listener) {

        Log.v("DM", "Enter Function: getFormAttachmentWithFormId");
        final OnCompleteListeners.getFormAttachmentWithFormIdListener getFormAttachment = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Pimm/FormsAttachment");

        if (isValidParameterString(formId)) {
            urlString.append("?formId=");
            urlString.append(formId);
        } else {
            Log.e("DM", "getFormAttachmentWithFormId: Missing/Empty/Invalid Parameter formId");
        }
        if (isValidParameterString(attachmentId)) {
            urlString.append("&attachmentId=");
            urlString.append(attachmentId);

        } else {
            Log.e("DM", "getFormAttachmentWithFormId: Missing/Empty/Invalid Parameter attachmentId");
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        Log.v("DM", "Received JSON Object for getFormAttachmentWithFormId: " + object.toString());
                        PimmFormAttachment result = new PimmFormAttachment();
                        JSONObject jsonObject = (JSONObject) object;
                        result.readFromJSONObject(jsonObject);
                        listener.getFormAttachmentWithFormId(result, null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.getFormAttachmentWithFormId(null, error);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for getFormAttachmentWithFormId" + e.getMessage());
                    listener.getFormAttachmentWithFormId(null, error);
                }
            }
        });


        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getFormAttachmentWithFormId");
    }

    public void deleteFormAttachmentWithFormId(String formId, String attachmentId, final OnCompleteListeners.onDeleteFormAttachmentWithFormIdListener listener) {

        Log.v("DM", "Enter Function: deleteFormAttachmentWithFormId");
        final OnCompleteListeners.onDeleteFormAttachmentWithFormIdListener deleteFormAttachment = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Pimm/FormsAttachment");

        if (isValidParameterString(formId)) {
            urlString.append("?formId=");
            urlString.append(formId);
        } else {
            Log.e("DM", "deleteFormAttachmentWithFormId: Missing/Empty/Invalid Parameter formId");
        }
        if (isValidParameterString(attachmentId)) {
            urlString.append("&attachmentId=");
            urlString.append(attachmentId);

        } else {
            Log.e("DM", "deleteFormAttachmentWithFormId: Missing/Empty/Invalid Parameter attachmentId");
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for deleteFormAttachmentWithFormId: " + object.toString());
                        listener.deleteFormAttachmentWithFormId(null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.deleteFormAttachmentWithFormId(error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for deleteFormAttachmentWithFormId" + e.getMessage());
                    listener.deleteFormAttachmentWithFormId(error);

                }
            }
        });


        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: deleteFormAttachmentWithFormId");
    }

    public void getStickyNoteWithId(String stickyNoteId, final OnCompleteListeners.getStickyNoteWithIdListener listener) {

        Log.v("DM", "Enter Function: getStickyNoteWithId");
        final OnCompleteListeners.getStickyNoteWithIdListener getStickyNote = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Pimm/StickyNote");

        if (isValidParameterString(stickyNoteId)) {
            urlString.append("?stickyNoteId=");
            urlString.append(stickyNoteId);
        } else {
            Log.e("DM", "getStickyNoteWithId: Missing/Empty/Invalid Parameter stickyNoteId");
        }


        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "getStickyNote", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for getStickyNoteWithId: " + object.toString());
                    StickyNote result = new StickyNote();
                    result.readFromJSONObject(object);
                    listener.getStickyNoteWithId(result);
                } else {
                    Log.d("DM", "Null object response for getStickyNoteWithId");
                }
            }
        });


        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getStickyNoteWithId");
    }

    public void getAssetProfileForDeviceID(String deviceID, String assetProfileID, final OnCompleteListeners.getAssetProfileForDeviceIDListener listener) {

        Log.v("DM", "Enter Function: getAssetProfileForDeviceID");
        final OnCompleteListeners.getAssetProfileForDeviceIDListener getAssetProfile = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Pimm/AssetProfile");

        if (isValidParameterString(deviceID)) {
            urlString.append("?deviceID=");
            urlString.append(deviceID);
        } else {
            Log.e("DM", "getAssetProfileForDeviceID: Missing/Empty/Invalid Parameter deviceID");
        }
        if (isValidParameterString(assetProfileID)) {
            urlString.append("&assetProfileID=");
            urlString.append(assetProfileID);
        } else {
            Log.e("DM", "getAssetProfileForDeviceID: Missing/Empty/Invalid Parameter assetProfileID");
        }


        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "getAssetProfile", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for getAssetProfileForDeviceID: " + object.toString());
                    AssetProfile result = new AssetProfile();
                    result.readFromJSONObject(object);
                    listener.getAssetProfileForDeviceID(result);
                } else {
                    Log.d("DM", "Null object response for getAssetProfileForDeviceID");
                }
            }
        });


        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getAssetProfileForDeviceID");
    }

    public void deleteAssetProfileWithAssetProfileID(String assetProfileID, boolean currentOnly, final OnCompleteListeners.ondeleteAssetProfileWithAssetProfileIDListener listener) {

        Log.v("DM", "Enter Function: deleteAssetProfileWithAssetProfileID");
        final OnCompleteListeners.ondeleteAssetProfileWithAssetProfileIDListener deleteAssetProfile = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/AssetProfile/Delete");

        if (isValidParameterString(assetProfileID)) {
            urlString.append("?assetProfileID=");
            urlString.append(assetProfileID);
            urlString.append("&currentOnly=");
            urlString.append(currentOnly ? "1" : "0");
        } else {
            Log.e("DM", "deleteAssetProfileWithAssetProfileID: Missing/Empty/Invalid Parameter assetProfileID");
        }


        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "deleteAssetProfile", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for deleteAssetProfileWithAssetProfileID: " + object.toString());
                    listener.deleteAssetProfileWithAssetProfileID();
                } else {
                    Log.d("DM", "Null object response for deleteAssetProfileWithAssetProfileID");
                }
            }
        });


        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: deleteAssetProfileWithAssetProfileID");
    }

    public void deleteAssetProfilesForDeviceID(String deviceID, final OnCompleteListeners.onDeleteAssetProfilesForDeviceIDListener listener) {

        Log.v("DM", "Enter Function: deleteAssetProfilesForDeviceID");
        final OnCompleteListeners.onDeleteAssetProfilesForDeviceIDListener deleteAssetProfiles = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/AssetProfile/DeleteForDevice");

        if (isValidParameterString(deviceID)) {
            urlString.append("?deviceId=");
            urlString.append(deviceID);

        } else {
            Log.e("DM", "deleteAssetProfilesForDeviceID: Missing/Empty/Invalid Parameter deviceID");
        }


        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "deleteAssetProfiles", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for deleteAssetProfilesForDeviceID: " + object.toString());
                    listener.deleteAssetProfilesForDeviceID();
                } else {
                    Log.d("DM", "Null object response for deleteAssetProfilesForDeviceID");
                }
            }
        });


        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: deleteAssetProfilesForDeviceID");
    }

    public void getAssetProfileListForDeviceId(String deviceId, Date timestamp, final OnCompleteListeners.getAssetProfileListForDeviceIdListener listener) {

        Log.v("DM", "Enter Function: getHOSEventsForUser");

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Pimm/AssetProfile/GetForDevice");

        if (isValidParameterString(deviceId)) {
            urlString.append("?deviceId=");
            urlString.append(deviceId);

        } else {
            Log.e("DM", "getAssetProfileListForDeviceId error :Missing/Empty/Invalid argument deviceId ");
        }


        if (timestamp != null) {
            urlString.append("&timestamp=");
            urlString.append(getUTCFormatDate(timestamp));

        } else {
            Log.e("DM", "getAssetProfileListForDeviceId error :Missing/Empty/Invalid argument timestamp ");
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();

        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                if (error == null) {
                    try {
                        JSONArray jsonArray = (JSONArray) object;
                        ArrayList<AssetProfile> assetProfiles = new ArrayList<AssetProfile>();

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);

                            AssetProfile assetProfile = new AssetProfile();
                            assetProfile.readFromJSONObject(jsonObject);
                            assetProfiles.add(assetProfile);
                        }

                        listener.getAssetProfileListForDeviceId(assetProfiles, null);
                    } catch (JSONException e) {
                        listener.getAssetProfileListForDeviceId(null, new Error(e.getMessage()));
                    }

                } else {
                    listener.getAssetProfileListForDeviceId(null, error);
                }
            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getAssetProfileListForDeviceId");
    }


    public void getAssetProfileForAssetProfileId(String assetProfileId, Date timestamp, final OnCompleteListeners.getAssetProfileForAssetProfileIdListener listener) {

        Log.v("DM", "Enter Function: getScorecardForSiteId");
        final OnCompleteListeners.getAssetProfileForAssetProfileIdListener getAssetProfile = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/AssetProfile/Get");

        if (isValidParameterString(assetProfileId)) {
            urlString.append("?assetProfileId=");
            urlString.append(assetProfileId);

        } else {
            Log.e("DM", "getScorecardForSiteId: Missing/Empty/Invalid Parameter deviceID");
        }

        if (timestamp != null) {
            String timeUTCSTR = getUTCFormatDate(timestamp);
            urlString.append("&timestamp=");
            urlString.append(timeUTCSTR);
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "getAssetProfile", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for getScorecardForSiteId: " + object.toString());
                    AssetProfile results = new AssetProfile();
                    results.readFromJSONObject(object);
                    listener.getAssetProfileForAssetProfileId(results);
                } else {
                    Log.d("DM", "Null object response for getScorecardForSiteId");
                }
            }
        });


        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getScorecardForSiteId");
    }

    public void enableInstanceWithId(String instanceId, final OnCompleteListeners.onenableInstanceWithIdListener listener) {

        Log.v("DM", "Enter Function: enableInstanceWithId");
        final OnCompleteListeners.onenableInstanceWithIdListener enableInstance = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("PimmTree/Tree/Instance/");

        if (isValidParameterString(instanceId)) {
            urlString.append(instanceId);
            urlString.append("/Enable");

        } else {
            Log.e("DM", "enableInstanceWithId: InstanceID parameter is invalid/missing,throwing exception");
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "enableInstance", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for enableInstanceWithId: " + object.toString());
                    listener.enableInstanceWithId();
                } else {
                    Log.d("DM", "Null object response for enableInstanceWithId");
                }
            }
        });


        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: enableInstanceWithId");
    }

    public void disableInstanceWithId(String instanceId, final OnCompleteListeners.ondisableInstanceWithIdListener listener) {

        Log.v("DM", "Enter Function: disableInstanceWithId");
        final OnCompleteListeners.ondisableInstanceWithIdListener disableInstance = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("PimmTree/Tree/Instance/");

        if (isValidParameterString(instanceId)) {
            urlString.append(instanceId);
            urlString.append("/Disable");

        } else {
            Log.e("DM", "disableInstanceWithId: InstanceID parameter is invalid/missing,throwing exception");
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "enableInstance", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for disableInstanceWithId: " + object.toString());
                    listener.disableInstanceWithId();
                } else {
                    Log.d("DM", "Null object response for disableInstanceWithId");
                }
            }
        });


        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: disableInstanceWithId");
    }

    public void getShipmentFormWithFormId(String formId, String appId, final OnCompleteListeners.getShipmentFormWithFormIdListener listener) {

        Log.v("DM", "Enter Function: getShipmentFormWithFormId");
        final OnCompleteListeners.getShipmentFormWithFormIdListener getShipmentForm = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("/update/Shipment");

        if (isValidParameterString(formId)) {
            urlString.append("?appId=");
            urlString.append(appId);
            urlString.append("&formId=");
            urlString.append(formId);

        } else {
            Log.e("DM", "getShipmentFormWithFormId: formId parameter is invalid/missing,throwing exception");
        }


        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "getShipmentForm", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for getShipmentFormWithFormId: " + object.toString());
                    PimmForm results = new PimmForm();
                    results.readFromJSONObject(object);
                    listener.getShipmentFormWithFormId(results);
                } else {
                    Log.d("DM", "Null object response for getShipmentFormWithFormId");
                }
            }
        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getShipmentFormWithFormId");
    }

    public void getDeviceFormWithFormId(String formId, String appId, final OnCompleteListeners.getDeviceFormWithFormIdListener listener) {

        Log.v("DM", "Enter Function: getDeviceFormWithFormId");
        final OnCompleteListeners.getDeviceFormWithFormIdListener getDeviceFormW = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("PimmForm/Form/Device");

        if (isValidParameterString(formId)) {
            urlString.append("?appId=");
            urlString.append(appId);
            urlString.append("&formId=");
            urlString.append(formId);

        } else {
            Log.e("DM", "getDeviceFormWithFormId: formId parameter is invalid/missing,throwing exception");
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "getShipmentForm", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for getDeviceFormWithFormId: " + object.toString());
                    PimmForm results = new PimmForm();
                    results.readFromJSONObject(object);
                    listener.getDeviceFormWithFormId(results);
                } else {
                    Log.d("DM", "Null object response for getDeviceFormWithFormId");
                }
            }
        });


        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getDeviceFormWithFormId");
    }


    public void getSiteFormWithFormId(String formId, String appId, final OnCompleteListeners.getSiteFormWithFormIdListener listener) {

        Log.v("DM", "Enter Function: getSiteFormWithFormId");
        final OnCompleteListeners.getSiteFormWithFormIdListener getSiteForm = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("PimmForm/Form/Site");

        if (isValidParameterString(formId)) {
            urlString.append("?appId=");
            urlString.append(appId);
            urlString.append("&formId=");
            urlString.append(formId);

        } else {
            Log.e("DM", "getSiteFormWithFormId: formId parameter is invalid/missing,throwing exception");
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "getSiteForm", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for getSiteFormWithFormId: " + object.toString());
                    PimmForm results = new PimmForm();
                    results.readFromJSONObject(object);
                    listener.getSiteFormWithFormId(results);
                } else {
                    Log.d("DM", "Null object response for getSiteFormWithFormId");
                }
            }
        });


        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getDeviceFormWithFormId");
    }

    public void ackAlarmForDeviceId(String deviceId, final OnCompleteListeners.ackAlarmForDeviceIdListener listener) {

        Log.v("DM", "Enter Function: ackAlarmForDeviceId");
        final OnCompleteListeners.ackAlarmForDeviceIdListener ackAlarm = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("PimmTree/Tree/Device/");

        if (isValidParameterString(deviceId)) {
            urlString.append(deviceId);
            urlString.append("/AckAlarms");
        } else {
            Log.e("DM", "ackAlarmForDeviceId: deviceId parameter is invalid/missing,throwing exception");
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "ackAlarm", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for ackAlarmForDeviceId: " + object.toString());
                    listener.ackAlarmForDeviceId();
                } else {
                    Log.d("DM", "Null object response for ackAlarmForDeviceId");
                }
            }
        });


        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: ackAlarmForDeviceId");
    }

    public void ackAlarmForObjectName(String objectName, String deviceId, final OnCompleteListeners.ackAlarmForObjectNameListener listener) {

        Log.v("DM", "Enter Function: ackAlarmForObjectName");
        final OnCompleteListeners.ackAlarmForObjectNameListener ackAlarmForObjectName = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("PimmTree/Tree/Device/");

        if (isValidParameterString(deviceId)) {
            urlString.append(deviceId);
            urlString.append("/");
            urlString.append(objectName);
            urlString.append("/AckAlarms");
        } else {
            Log.e("DM", "ackAlarmForObjectName: deviceId parameter is invalid/missing,throwing exception");
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "ackAlarmForObjectName", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for ackAlarmForObjectName: " + object.toString());
                    listener.ackAlarmForObjectName();
                } else {
                    Log.d("DM", "Null object response for ackAlarmForObjectName");
                }
            }
        });


        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: ackAlarmForObjectName");
    }

    public void enableDeviceWithId(String deviceId, final OnCompleteListeners.onEnableDeviceWithIdListener listener) {

        Log.v("DM", "Enter Function: enableDeviceWithId");
        final OnCompleteListeners.onEnableDeviceWithIdListener enableDevice = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("PimmTree/Tree/Device/");

        if (isValidParameterString(deviceId)) {
            urlString.append(deviceId);
            urlString.append("/Disable");

        } else {
            Log.e("DM", "enableDeviceWithId: deviceId parameter is invalid/missing,throwing exception");
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "enableDevice", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for enableDeviceWithId: " + object.toString());
                    listener.enableDeviceWithId();
                } else {
                    Log.d("DM", "Null object response for enableDeviceWithId");
                }
            }
        });


        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: enableDeviceWithId");
    }

    public void disableDeviceWithId(String deviceId, final OnCompleteListeners.ondisableDeviceWithIdListener listener) {

        Log.v("DM", "Enter Function: disableDeviceWithId");
        final OnCompleteListeners.ondisableDeviceWithIdListener disableDeviceWithId = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("PimmTree/Tree/Device/");

        if (isValidParameterString(deviceId)) {
            urlString.append(deviceId);
            urlString.append("/Enable");

        } else {
            Log.e("DM", "disableDeviceWithId: deviceId parameter is invalid/missing,throwing exception");
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "disableDeviceWithId", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for disableDeviceWithId: " + object.toString());
                    listener.disableDeviceWithId();
                } else {
                    Log.d("DM", "Null object response for disableDeviceWithId");
                }
            }
        });


        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: disableDeviceWithId");
    }

    public void enableObjectWithName(String objectName, String deviceId, final OnCompleteListeners.onEnableObjectWithNameListener listener) {

        Log.v("DM", "Enter Function: enableObjectWithName");
        final OnCompleteListeners.onEnableObjectWithNameListener enableObjectWithName = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("PimmTree/Tree/Device/");

        if (isValidParameterString(deviceId)) {
            urlString.append(deviceId);
            urlString.append("/");
            urlString.append(objectName);
            urlString.append("/Enable");

        } else {
            Log.e("DM", "enableObjectWithName: deviceId parameter is invalid/missing,throwing exception");
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "enableObjectWithName", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for enableObjectWithName: " + object.toString());
                    listener.enableObjectWithName();
                } else {
                    Log.d("DM", "Null object response for enableObjectWithName");
                }
            }
        });


        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: enableObjectWithName");
    }

    public void disableObjectWithName(String objectName, String deviceId, final OnCompleteListeners.onDisableObjectWithNameListener listener) {

        Log.v("DM", "Enter Function: disableObjectWithName");
        final OnCompleteListeners.onDisableObjectWithNameListener disableObjectWithName = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("PimmTree/Tree/Device/");

        if (isValidParameterString(deviceId)) {
            urlString.append(deviceId);
            urlString.append("/");
            urlString.append(objectName);
            urlString.append("/Disable");

        } else {
            Log.e("DM", "disableObjectWithName: deviceId parameter is invalid/missing,throwing exception");
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "disableObjectWithName", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for disableObjectWithName: " + object.toString());
                    listener.disableObjectWithName();
                } else {
                    Log.d("DM", "Null object response for disableObjectWithName");
                }
            }
        });


        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: disableObjectWithName");
    }


    public void getELDSequenceNumberForELDDevice(String truckDeviceId, final OnCompleteListeners.getELDSequenceNumberForELDDeviceListener listener) {

        Log.v("DM", "Enter Function: getELDSequenceNumberForELDDevice");
        final OnCompleteListeners.getELDSequenceNumberForELDDeviceListener getELDSequenceNumber = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Driver/HOS2/ELDSequenceNumber_Get");

        if (isValidParameterString(truckDeviceId)) {
            urlString.append("?truckDeviceId=");
            urlString.append(truckDeviceId);

        } else {
            Log.e("DM", "getELDSequenceNumberForELDDevice: truckDeviceId parameter is invalid/missing,throwing exception");
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for getELDSequenceNumberForELDDevice: " + object.toString());
                        ELDSequenceNum results = new ELDSequenceNum();
                        JSONObject jsonObject = (JSONObject) object;
                        results.readFromJSONObject(jsonObject);
                        listener.getELDSequenceNumberForELDDevice(results, null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.getELDSequenceNumberForELDDevice(null, error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for pushGPSEventForPOIWithDeliveryId" + e.getMessage());
                    listener.getELDSequenceNumberForELDDevice(null, error);

                }
            }
        });


        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getELDSequenceNumberForELDDevice");
    }

    public void postHOSViolation(HOSViolation hosViolation, final OnCompleteListeners.onPostHOSViolationListener listener) {

        Log.v("DM", "Enter Function: postHOSViolation");
        final OnCompleteListeners.onPostHOSViolationListener postHOSViolation = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Driver/HOS/HOSViolation/");

        if (isValidParameterString(hosViolation.userID)) {
            urlString.append(hosViolation.userID);
        } else {
            Log.e("DM", "postHOSViolation: driverID parameter is invalid/missing,throwing exception");
        }

        StringBuilder parameterString = new StringBuilder();


        if (isValidParameterString(hosViolation.deliveryId)) {
            parameterString.append("deliveryId=");
            parameterString.append(hosViolation.deliveryId);
        } else {
            Log.e("DM", "postHOSViolation: deliveryId parameter is invalid/missing,throwing exception");
        }

        int violationType = hosViolation.violationType.getValue();
        String violationTypeString = String.format("%d", violationType);
        parameterString.append("&violationType=");
        parameterString.append(violationTypeString);

        String violationStartTimeStr = getUTCFormatDate(hosViolation.violationStart);
        parameterString.append("&violationStart=");
        parameterString.append(violationStartTimeStr);

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initializePOSTRequest(this.authHeaderValue, parameterString.toString(), new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for postHOSViolation: " + object.toString());
                        listener.postHOSViolation(null);
                    } else if (object == null && error == null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for postHOSViolation: " + object.toString());
                        listener.postHOSViolation(null);

                    } else {
                        Log.e("DM", "Null object ");
                        listener.postHOSViolation(error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for postHOSViolation" + e.getMessage());
                    listener.postHOSViolation(error);

                }
            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: postHOSViolation");
    }

//    public void createHOSViolationWithId(String hosViolationId, String driverUserId, String deliveryId, HOSViolationTypeEnum.HOSViolationTypesEnum violationType, Date violationStartTime, final OnCompleteListeners.createHOSViolationWithIdListener listener) {
//
//        Log.v("DM", "Enter Function: createHOSViolationWithId");
//        final OnCompleteListeners.createHOSViolationWithIdListener createHOSViolationWithId = listener;
//
//        StringBuilder urlString = new StringBuilder();
//        urlString.append(this.baseUrl);
//        urlString.append("Driver/HOSViolation");
//
//        StringBuilder parameterString = new StringBuilder();
//
//
//        if (isValidParameterString(driverUserId)) {
//            parameterString.append("driverUserId=");
//            parameterString.append(driverUserId);
//        } else {
//            Log.e("DM", "createHOSViolationWithId: driverUserId parameter is invalid/missing,throwing exception");
//        }
//        if (isValidParameterString(deliveryId)) {
//            parameterString.append("&deliveryId=");
//            parameterString.append(deliveryId);
//        } else {
//            Log.e("DM", "createHOSViolationWithId: deliveryId parameter is invalid/missing,throwing exception");
//        }
//        if (isValidParameterString(hosViolationId)) {
//            parameterString.append("&hosViolationId=");
//            parameterString.append(hosViolationId);
//        } else {
//            Log.e("DM", "createHOSViolationWithId: deliveryId parameter is invalid/missing,throwing exception");
//        }
//
//        parameterString.append("&violationType=");
//        parameterString.append(String.format("%ld", Long.parseLong(String.valueOf(violationType.getValue()))));
//
//        if (violationStartTime != null){
//            parameterString.append("&violationTimeUTC=");
//            parameterString.append(getUTCFormatDate(violationStartTime));
//
//        }
//        Log.v("DM", "Create Form  API Parameters:: " + parameterString);
//
//        Log.v("DM", "Request URL: " + urlString);
//
//        RestConnection restConnection = new RestConnection();
//        restConnection.initializePOSTRequest(this.authHeaderValue, parameterString.toString(), new OnCompleteListeners.OnRestConnectionCompleteListener() {
//            @Override
//            public void onRestConnectionComplete(Object object, Error error) {
//                try {
//                    if (object != null) {
//                        new JSONObject(String.valueOf(object));
//                        Log.v("DM", "Received JSON Object for createHOSViolationForDriverId: " + object.toString());
//                        HOSViolation results = new HOSViolation();
//                        JSONObject jsonObject = (JSONObject) object;
//                        results.readFromJSONObject(jsonObject);
//                        listener.createHOSViolationWithId(results, null);
//                    } else {
//                        Log.e("DM", "Null object ");
//                        listener.createHOSViolationWithId(null, error);
//                    }
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                    Log.e("DM", "JSONException for createHOSViolationForDriverId" + e.getMessage());
//                    listener.createHOSViolationWithId(null, error);
//
//                }
//            }
//        });
//
//
//        restConnection.execute(String.valueOf(urlString));
//        Log.v("DM", "ExitFunction: createHOSViolationForDriverId");
//    }

    public void createHOSViolationForDriverId(String userId, String deliveryId, HOSViolationTypeEnum violationType, String violationStartTime, final OnCompleteListeners.onCreateHOSViolationForDriverIdListener listener) {

        Log.v("DM", "Enter Function: createHOSViolationForDriverId");
        final OnCompleteListeners.onCreateHOSViolationForDriverIdListener createHOSViolation = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Driver/HOS/HOSViolation/");

        if (isValidParameterString(userId)) {
            urlString.append(userId);
        } else {
            Log.e("DM", "createHOSViolationForDriverId: driverID parameter is invalid/missing,throwing exception");
        }
        StringBuilder parameterString = new StringBuilder();


        if (isValidParameterString(deliveryId)) {
            parameterString.append("?deliveryId=");
            parameterString.append(deliveryId);
        } else {
            Log.e("DM", "postHOSViolation: deliveryId parameter is invalid/missing,throwing exception");
        }

        parameterString.append("&violationType=");
        parameterString.append(String.format("%ld", Long.parseLong(violationType.toString())));

        parameterString.append("&violationStart=");
        parameterString.append(violationStartTime);
        Log.v("DM", "Create Form  API Parameters:: " + parameterString);

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initializePOSTRequest(this.authHeaderValue, parameterString.toString(), new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for createHOSViolationForDriverId: " + object.toString());
                        HOSViolation results = new HOSViolation();
                        JSONObject jsonObject = (JSONObject) object;
                        results.readFromJSONObject(jsonObject);
                        listener.createHOSViolationForDriverId(results, null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.createHOSViolationForDriverId(null, error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for createHOSViolationForDriverId" + e.getMessage());
                    listener.createHOSViolationForDriverId(null, error);

                }
            }
        });


        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: createHOSViolationForDriverId");
    }


    public void getHOSViolationListForDriverId(String driverId, String deliveryId, String start, String end, final OnCompleteListeners.getHOSViolationListForDriverIdListener listener) {

        Log.v("DM", "Enter Function: getHOSViolationListForDriverId");

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Driver/HOS/HOSViolation/");

        if (isValidParameterString(driverId)) {
            urlString.append(driverId);
        } else {
            Log.e("DM", "getHOSViolationListForDriverId: driverId parameter is invalid/missing,throwing exception");
        }

        if (isValidParameterString(deliveryId)) {
            urlString.append("?deliveryId=");
            urlString.append(deliveryId);
        } else {
            Log.e("DM", "getHOSViolationListForDriverId: deliveryId parameter is invalid/missing,throwing exception");
        }

        if (isValidParameterString(start) && isValidParameterString(end)) {
            urlString.append("&start=");
            urlString.append(start);

            urlString.append("&end=");
            urlString.append(end);
        } else {
            Log.e("DM", "getHOSViolationListForDriverId: deliveryId parameter is invalid/missing,throwing exception");
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();

        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                if (error == null) {
                    try {
                        JSONArray jsonArray = (JSONArray) object;
                        ArrayList<HOSViolation> hosViolationArrayList = new ArrayList<HOSViolation>();

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);

                            HOSViolation result = new HOSViolation();
                            result.readFromJSONObject(jsonObject);
                            hosViolationArrayList.add(result);
                        }

                        listener.getHOSViolationListForDriverId(hosViolationArrayList, null);
                    } catch (JSONException e) {
                        listener.getHOSViolationListForDriverId(null, new Error(e.getMessage()));
                    }

                }
            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getHOSViolationListForDriverId");
    }

    public void deleteHOSViolationForDriverId(String userId, String deliveryId, String hosViolationId, HOSViolationTypeEnum violationType, final OnCompleteListeners.onDeleteHOSViolationForDriverIdListener listener) {

        Log.v("DM", "Enter Function: deleteHOSViolationForDriverId");
        final OnCompleteListeners.onDeleteHOSViolationForDriverIdListener deleteHOSViolation = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Driver/HOS/HOSViolation/");

        if (isValidParameterString(userId)) {
            urlString.append(userId);
        } else {
            Log.e("DM", "deleteHOSViolationForDriverId: driverID parameter is invalid/missing,throwing exception");
        }


        if (isValidParameterString(deliveryId)) {
            urlString.append("?deliveryId=");
            urlString.append(deliveryId);
        } else {
            Log.e("DM", "deleteHOSViolationForDriverId: deliveryId parameter is invalid/missing,throwing exception");
        }

        if (isValidParameterString(hosViolationId)) {
            urlString.append("&hosViolationId=");
            urlString.append(hosViolationId);
        } else {
            Log.e("DM", "deleteHOSViolationForDriverId: hosViolationId parameter is invalid/missing,throwing exception");
        }

        if (Integer.parseInt(violationType.toString()) > 0) {
            urlString.append("&violationType=");
            urlString.append(String.format("%d", Integer.parseInt(violationType.toString())));
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for deleteHOSViolationForDriverId: " + object.toString());
                        listener.deleteHOSViolationForDriverId(null);
                    } else if (object == null && error == null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for deleteHOSViolationForDriverId: " + object.toString());
                        listener.deleteHOSViolationForDriverId(null);

                    } else {
                        Log.e("DM", "Null object ");
                        listener.deleteHOSViolationForDriverId(error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for deleteHOSViolationForDriverId" + e.getMessage());
                    listener.deleteHOSViolationForDriverId(error);

                }
            }

        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: deleteHOSViolationForDriverId");
    }

    public void getHOSAlertForDriverId(String userId, String hosAlertId, final OnCompleteListeners.getHOSAlertForDriverIdListener listener) {

        Log.v("DM", "Enter Function: getHOSAlertForDriverId");
        final OnCompleteListeners.getHOSAlertForDriverIdListener getHOSAlert = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Driver/HOS/HOSAlert/");

        if (isValidParameterString(userId)) {
            urlString.append(userId);
        } else {
            Log.e("DM", "getHOSAlertForDriverId: driverID parameter is invalid/missing,throwing exception");
        }


        if (isValidParameterString(hosAlertId)) {
            urlString.append("?hosAlertId=");
            urlString.append(hosAlertId);
        } else {
            Log.e("DM", "getHOSAlertForDriverId: hosAlertId parameter is invalid/missing,throwing exception");
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "getHOSAlert", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for getHOSAlertForDriverId: " + object.toString());
                    HOSAlert results = new HOSAlert();
                    results.readFromJSONObject(object);
                    listener.getHOSAlertForDriverId(results);
                } else {
                    Log.d("DM", "Null object response for getHOSAlertForDriverId");
                }
            }
        });


        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getHOSAlertForDriverId");
    }

    public void deleteHOSAlertsForDriverId(String userId, String hosAlertId, String deliveryId, HOSViolationTypeEnum violationType, final OnCompleteListeners.onDeleteHOSAlertsForDriverIdListener listener) {

        Log.v("DM", "Enter Function: deleteHOSAlertsForDriverId");
        final OnCompleteListeners.onDeleteHOSAlertsForDriverIdListener deleteHOSAlerts = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Driver/HOS/HOSAlert/");

        if (isValidParameterString(userId)) {
            urlString.append(userId);
        } else {
            Log.e("DM", "deleteHOSAlertsForDriverId: userId parameter is invalid/missing,throwing exception");
        }


        if (isValidParameterString(hosAlertId)) {
            urlString.append("?hosAlertId=");
            urlString.append(hosAlertId);
        } else {
            Log.e("DM", "deleteHOSAlertsForDriverId: hosAlertId parameter is invalid/missing,throwing exception");
        }

        if (isValidParameterString(deliveryId)) {
            urlString.append("&deliveryId=");
            urlString.append(deliveryId);
        } else {
            Log.e("DM", "deleteHOSAlertsForDriverId: deliveryId parameter is invalid/missing,throwing exception");
        }

        if (Integer.parseInt(violationType.toString()) > 0) {
            urlString.append("&violationType=");
            urlString.append(String.format("%d", Integer.parseInt(violationType.toString())));
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "deleteHOSAlerts", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for deleteHOSAlertsForDriverId: " + object.toString());
                    listener.deleteHOSAlertsForDriverId();
                } else {
                    Log.d("DM", "Null object response for deleteHOSAlertsForDriverId");
                }
            }
        });


        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: deleteHOSAlertsForDriverId");
    }

    public void createHOSAlertForDriverId(String userId, String hosAlertId, String deliveryId, HOSViolationTypeEnum.HOSViolationTypesEnum violationType, int severity, String alertStartTime, final OnCompleteListeners.onCreateHOSAlertForDriverIdListener listener) {

        Log.v("DM", "Enter Function: createHOSAlertForDriverId");
        final OnCompleteListeners.onCreateHOSAlertForDriverIdListener createHOSAlert = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Driver/HOSAlert/Create");

//        if(isValidParameterString(userId)){
//            urlString.append(userId);
//        }else{
//            Log.e("DM", "createHOSAlertForDriverId: userId parameter is invalid/missing,throwing exception");
//        }
        StringBuilder parameterString = new StringBuilder();

        if (isValidParameterString(deliveryId)) {
            parameterString.append("deliveryId=");
            parameterString.append(deliveryId);
        } else {
            Log.e("DM", "createHOSAlertForDriverId: deliveryId parameter is invalid/missing,throwing exception");
        }
        if (isValidParameterString(userId)) {
            parameterString.append("&driverUserId=");
            parameterString.append(userId);
        } else {
            Log.e("DM", "createHOSAlertForDriverId: driverUserId parameter is invalid/missing,throwing exception");
        }
        if (isValidParameterString(hosAlertId)) {
            parameterString.append("&hosAlertId=");
            parameterString.append(hosAlertId);
        } else {
            Log.e("DM", "createHOSAlertForDriverId: hosAlertId parameter is invalid/missing,throwing exception");
        }

        if (violationType != null) {
            parameterString.append("&violationType=");
            parameterString.append(String.format("%d", violationType.getValue()));
        }

        parameterString.append("&alertStartTime=");
        parameterString.append(alertStartTime);

        parameterString.append("&severity=");
        parameterString.append(Long.parseLong("%ld", severity));

        Log.v("DM", "Create Form  API Parameters:: " + parameterString);

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initializePOSTRequest(this.authHeaderValue, parameterString.toString(), new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for createHOSAlertForDriverId: " + object.toString());
                        HOSAlert results = new HOSAlert();
                        JSONObject jsonObject = (JSONObject) object;
                        results.readFromJSONObject(jsonObject);
                        listener.createHOSAlertForDriverId(results, null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.createHOSAlertForDriverId(null, error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for createHOSAlertForDriverId" + e.getMessage());
                    listener.createHOSAlertForDriverId(null, error);

                }
            }
        });


        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: createHOSAlertForDriverId");
    }


    public void cancelHOSAlertForDriverId(String userId, String hosAlertId, String alertEndTime, final OnCompleteListeners.onCancelHOSAlertForDriverIdListener listener) {

        Log.v("DM", "Enter Function: cancelHOSAlertForDriverId");
        final OnCompleteListeners.onCancelHOSAlertForDriverIdListener cancelHOSAlert = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Driver/HOS/HOSAlertCancel/");

        if (isValidParameterString(userId)) {
            urlString.append(userId);
        } else {
            Log.e("DM", "cancelHOSAlertForDriverId: userId parameter is invalid/missing,throwing exception");
        }

        StringBuilder parameterString = new StringBuilder();

        if (isValidParameterString(hosAlertId)) {
            parameterString.append("?hosAlertId=");
            parameterString.append(hosAlertId);
        } else {
            Log.e("DM", "cancelHOSAlertForDriverId: hosAlertId parameter is invalid/missing,throwing exception");
        }

        parameterString.append("&time=");
        parameterString.append(alertEndTime);

        Log.v("DM", "Create Form  API Parameters:: " + parameterString);

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initializePOSTRequest(this.authHeaderValue, parameterString.toString(), new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {

                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for cancelHOSAlertForDriverId: " + object.toString());
                        listener.cancelHOSAlertForDriverId(null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.cancelHOSAlertForDriverId(error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for cancelHOSAlertForDriverId" + e.getMessage());
                    listener.cancelHOSAlertForDriverId(error);

                }
            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: cancelHOSAlertForDriverId");
    }

    public void escalateHOSAlertForDriverId(String userId, String hosAlertId, Integer severity, String escalationTime, final OnCompleteListeners.onEscalateHOSAlertForDriverIdListener listener) {

        Log.v("DM", "Enter Function: escalateHOSAlertForDriverId");
        final OnCompleteListeners.onEscalateHOSAlertForDriverIdListener escalateHOSAlert = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Driver/HOS/HOSAlertEscalate/");

        if (isValidParameterString(userId)) {
            urlString.append(userId);
        } else {
            Log.e("DM", "escalateHOSAlertForDriverId: userId parameter is invalid/missing,throwing exception");
        }

        if (isValidParameterString(hosAlertId)) {
            urlString.append("?hosAlertId=");
            urlString.append(hosAlertId);
        } else {
            Log.e("DM", "escalateHOSAlertForDriverId: hosAlertId parameter is invalid/missing,throwing exception");
        }

        urlString.append("&time=");
        urlString.append(escalationTime);

        urlString.append("&severity=");
        urlString.append(String.format("%ld", Long.parseLong(severity.toString())));


        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "escalateHOSAlert", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for escalateHOSAlertForDriverId: " + object.toString());
                    listener.escalateHOSAlertForDriverId();
                } else {
                    Log.d("DM", "Null object response for escalateHOSAlertForDriverId");
                }
            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: escalateHOSAlertForDriverId");
    }

    public void postHOSSummary(HOSSummary hosSummary, final OnCompleteListeners.onPostHOSSummaryListener listener) {

        Log.v("DM", "Enter Function: postHOSSummary");
        final OnCompleteListeners.onPostHOSSummaryListener postHOSSummary = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Driver/HOS/HOSSummary/");

        if (isValidParameterString(hosSummary.userID)) {
            urlString.append(hosSummary.userID);
        } else {
            Log.e("DM", "postHOSSummary: driverID parameter is invalid/missing,throwing exception");
        }
        StringBuilder parameterString = new StringBuilder();


        if (isValidParameterString(hosSummary.deliveryId)) {
            parameterString.append("deliveryId=");
            parameterString.append(hosSummary.deliveryId);
        } else {
            Log.e("DM", "postHOSSummary: deliveryId parameter is invalid/missing,throwing exception");
        }


        if (hosSummary.driveTime >= 0) {
            String driveTimeString = String.format("%ld", Long.parseLong(String.valueOf(hosSummary.driveTime)));
            parameterString.append("&driveTime=");
            parameterString.append(driveTimeString);
        } else {
            urlString.append("&driveTime=0");
        }

        if (hosSummary.onDutyTime >= 0) {
            String onDutyTimeString = String.format("%ld", Long.parseLong(String.valueOf(hosSummary.onDutyTime)));
            parameterString.append("&onDutyTime=");
            parameterString.append(onDutyTimeString);
        } else {
            parameterString.append("&onDutyTime=0");
        }

        if (hosSummary.prepTime >= 0) {
            String prepTimeString = String.format("%ld", Long.parseLong(String.valueOf(hosSummary.prepTime)));
            parameterString.append("&prepTime=");
            parameterString.append(prepTimeString);
        } else {
            parameterString.append("&prepTime=0");
        }

        if (hosSummary.restTime >= 0) {
            String restTimeString = String.format("%ld", Long.parseLong(String.valueOf(hosSummary.restTime)));
            parameterString.append("&restTime=");
            parameterString.append(restTimeString);
        } else {
            urlString.append("&restTime=0");
        }

        if (hosSummary.deliveryTime >= 0) {
            String deliveryTimeString = String.format("%ld", Long.parseLong(String.valueOf(hosSummary.deliveryTime)));
            parameterString.append("&deliveryTime=");
            parameterString.append(deliveryTimeString);
        } else {
            parameterString.append("&deliveryTime=0");
        }
        Log.v("DM", "Create Form  API Parameters: " + parameterString);

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initializePOSTRequest(this.authHeaderValue, parameterString.toString(), new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {

                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for postHOSSummary: " + object.toString());
                        listener.postHOSSummary(null);
                    } else if (object == null && error == null) {
                        Log.v("DM", "Received JSON Object for postHOSSummary: ");
                        listener.postHOSSummary(null);

                    } else {
                        Log.d("DM", "Null object response for postHOSSummary");
                        listener.postHOSSummary(error);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for postHOSSummary" + e.getMessage());
                    listener.postHOSSummary(error);

                }
            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: postHOSSummary");
    }

    public void deleteHOSSummaryForUserId(String userId, String deliveryId, final OnCompleteListeners.onDeleteHOSSummaryForUserIdListener listener) {

        Log.v("DM", "Enter Function: deleteHOSSummaryForUserId");
        final OnCompleteListeners.onDeleteHOSSummaryForUserIdListener deleteHOSSummary = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Driver/HOS/HOSSummary/");

        if (isValidParameterString(userId)) {
            urlString.append(userId);
        } else {
            Log.e("DM", "deleteHOSSummaryForUserId: userId parameter is invalid/missing,throwing exception");
        }

        if (isValidParameterString(deliveryId)) {
            urlString.append("?deliveryId=");
            urlString.append(deliveryId);
        } else {
            Log.e("DM", "deleteHOSSummaryForUserId: deliveryId parameter is invalid/missing,throwing exception");
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "deleteHOSSummary", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for deleteHOSSummaryForUserId: " + object.toString());
                    listener.deleteHOSSummaryForUserId();
                } else {
                    Log.d("DM", "Null object response for deleteHOSSummaryForUserId");
                }
            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: deleteHOSSummaryForUserId");
    }

    public void isUserNameAvailableForDcId(String dcId, String usernameArg, final OnCompleteListeners.isUserNameAvailableForDcIdListener listener) {

        Log.v("DM", "Enter Function: isUserNameAvailableForDcId");
        final OnCompleteListeners.isUserNameAvailableForDcIdListener isUserNameAvailable = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/Driver/IsUsernameAvailable");

        if (isValidParameterString(dcId)) {
            urlString.append("?dcId=");
            urlString.append(dcId);
        } else {
            Log.e("DM", "isUserNameAvailableForDcId: dcId parameter is invalid/missing,throwing exception");
        }

        if (isValidParameterString(usernameArg)) {
            urlString.append("&username=");
            urlString.append(usernameArg);
        } else {
            Log.e("DM", "isUserNameAvailableForDcId: usernameArg parameter is invalid/missing,throwing exception");
        }


        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "isUserNameAvailable", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for isUserNameAvailableForDcId: " + object.toString());
                    UsernameAvailableResult results = new UsernameAvailableResult();
                    results.readFromJSONObject(object);
                    listener.isUserNameAvailableForDcId(results);
                } else {
                    Log.d("DM", "Null object response for isUserNameAvailableForDcId");
                }
            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: isUserNameAvailableForDcId");
    }

    public void getDriverScorecardForShipmentId(String shipmentId, final OnCompleteListeners.getDriverScorecardForShipmentIdListener listener) {

        Log.v("DM", "Enter Function: getDriverScorecardForShipmentId");
        final OnCompleteListeners.getDriverScorecardForShipmentIdListener getDriverScorecard = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Vehicle/Vehicle");

        if (isValidParameterString(shipmentId)) {
            urlString.append("?shipmentId=");
            urlString.append(shipmentId);
        } else {
            Log.e("DM", "getDriverScorecardForShipmentId: shipmentId parameter is invalid/missing,throwing exception");
        }

        urlString.append("&application=");
        urlString.append("scorecard");


        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for getDriverScorecardForShipmentId: " + object.toString());
                        VehicleDTO results = new VehicleDTO();
                        JSONObject jsonObject = (JSONObject) object;
                        results.readFromJSONObject(jsonObject);
                        listener.getDriverScorecardForShipmentId(results, null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.getDriverScorecardForShipmentId(null, error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for getDriverScorecardForShipmentId" + e.getMessage());
                    listener.getDriverScorecardForShipmentId(null, error);

                }
            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getDriverScorecardForShipmentId");
    }

    public void getDriverScorecardForShipmentId(String shipmentId, String driverId, String routeId, String deviceId, Date startDate, Date endDate, final OnCompleteListeners.getDriverScorecardForShipmentIdListener listener) {

        Log.v("DM", "Enter Function: getDriverScorecardForShipmentId");
        final OnCompleteListeners.getDriverScorecardForShipmentIdListener getDriverScorecard = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Vehicle/Vehicle");

        if (isValidParameterString(shipmentId)) {
            urlString.append("?shipmentId=");
            urlString.append(shipmentId);
        } else {
            Log.e("DM", "getDriverScorecardForShipmentId: shipmentId parameter is invalid/missing,throwing exception");
        }

        if (isValidParameterString(routeId)) {
            urlString.append("&routeId=");
            urlString.append(routeId);
        }

        if (isValidParameterString(deviceId)) {
            urlString.append("&deviceId=");
            urlString.append(deviceId);
        }
        if (isValidParameterString(driverId)) {
            urlString.append("&driverId=");
            urlString.append(driverId);
        }

        if (startDate != null && endDate != null) {
            String startDateStr = getUTCFormatDate(startDate);
            String endDateStr = getUTCFormatDate(endDate);

            urlString.append("&start=");
            urlString.append(startDateStr);
            urlString.append("&end=");
            urlString.append(endDateStr);
        }

        urlString.append("&application=");
        urlString.append("scorecard");


        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for getDriverScorecardForShipmentId: " + object.toString());
                        VehicleDTO results = new VehicleDTO();
                        JSONObject jsonObject = (JSONObject) object;
                        results.readFromJSONObject(jsonObject);
                        listener.getDriverScorecardForShipmentId(results, null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.getDriverScorecardForShipmentId(null, error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for getDriverScorecardForShipmentId" + e.getMessage());
                    listener.getDriverScorecardForShipmentId(null, error);

                }

            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getDriverScorecardForShipmentId");
    }

    public void getDriverDashboardEventsForShipmentId(String shipmentId, String routeId, String deviceId, Date startDate, Date endDate, final OnCompleteListeners.getDriverDashboardEventsForShipmentIdListener listener) {

        Log.v("DM", "Enter Function: getDriverDashboardEventsForShipmentId");
        final OnCompleteListeners.getDriverDashboardEventsForShipmentIdListener getDriverDashboardEvents = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Vehicle/Vehicle");

        if (isValidParameterString(shipmentId)) {
            urlString.append("?shipmentId=");
            urlString.append(shipmentId);
        } else {
            Log.e("DM", "getDriverDashboardEventsForShipmentId: shipmentId parameter is invalid/missing,throwing exception");
        }

        if (isValidParameterString(routeId)) {
            urlString.append("&routeId=");
            urlString.append(routeId);
        }

        if (isValidParameterString(deviceId)) {
            urlString.append("&deviceId=");
            urlString.append(deviceId);
        }

        if (startDate != null && endDate != null) {
            String startDateStr = getUTCFormatDate(startDate);
            String endDateStr = getUTCFormatDate(endDate);

            urlString.append("&start=");
            urlString.append(startDateStr);
            urlString.append("&end=");
            urlString.append(endDateStr);
        }

        urlString.append("&application=");
        urlString.append("events");


        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "getDriverDashboardEvents", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for getDriverDashboardEventsForShipmentId: " + object.toString());
                    VehicleDTO results = new VehicleDTO();
                    results.readFromJSONObject(object);
                    listener.getDriverDashboardEventsForShipmentId(results);
                } else {
                    Log.d("DM", "Null object response for getDriverDashboardEventsForShipmentId");
                }
            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getDriverDashboardEventsForShipmentId");
    }

    public void getDriverDashboardUpdatesForShipmentId(String shipmentId, String routeId, String deviceId, Date startDate, Date endDate, final OnCompleteListeners.getDriverDashboardUpdatesForShipmentIdListener listener) {

        Log.v("DM", "Enter Function: getDriverDashboardUpdatesForShipmentId");
        final OnCompleteListeners.getDriverDashboardUpdatesForShipmentIdListener getDriverDashboardUpdates = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Vehicle/Vehicle");

        if (isValidParameterString(shipmentId)) {
            urlString.append("?shipmentId=");
            urlString.append(shipmentId);
        } else {
            Log.e("DM", "getDriverDashboardUpdatesForShipmentId: shipmentId parameter is invalid/missing,throwing exception");
        }

        if (isValidParameterString(routeId)) {
            urlString.append("&routeId=");
            urlString.append(routeId);
        }

        if (isValidParameterString(deviceId)) {
            urlString.append("&deviceId=");
            urlString.append(deviceId);
        }

        if (startDate != null && endDate != null) {
            String startDateStr = getUTCFormatDate(startDate);
            String endDateStr = getUTCFormatDate(endDate);

            urlString.append("&start=");
            urlString.append(startDateStr);
            urlString.append("&end=");
            urlString.append(endDateStr);
        }

        urlString.append("&application=");
        urlString.append("dash");


        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "getDriverDashboardUpdates", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for getDriverDashboardUpdatesForShipmentId: " + object.toString());
                    VehicleDTO results = new VehicleDTO();
                    results.readFromJSONObject(object);
                    listener.getDriverDashboardUpdatesForShipmentId(results);
                } else {
                    Log.d("DM", "Null object response for getDriverDashboardUpdatesForShipmentId");
                }
            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getDriverDashboardUpdatesForShipmentId");
    }

    public void getDriverRollUpForDriverId(String driverId, Date startDate, Date endDate, final OnCompleteListeners.getDriverRollUpForDriverIdListener listener) {

        Log.v("DM", "Enter Function: getDriverRollUpForDriverId");
        final OnCompleteListeners.getDriverRollUpForDriverIdListener getDriverRollUp = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Vehicle/Vehicle");

        if (isValidParameterString(driverId)) {
            urlString.append("?driverId=");
            urlString.append(driverId);
        } else {
            Log.e("DM", "getDriverRollUpForDriverId: driverId parameter is invalid/missing,throwing exception");
        }


        if (startDate != null && endDate != null) {
            String startDateStr = getUTCFormatDate(startDate);
            String endDateStr = getUTCFormatDate(endDate);

            urlString.append("&start=");
            urlString.append(startDateStr);
            urlString.append("&end=");
            urlString.append(endDateStr);
        }

        urlString.append("&application=");
        urlString.append("driverRollup");


        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "getDriverRollUp", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for getDriverRollUpForDriverId: " + object.toString());
                    VehicleDTO results = new VehicleDTO();
                    results.readFromJSONObject(object);
                    listener.getDriverRollUpForDriverId(results);
                } else {
                    Log.d("DM", "Null object response for getDriverRollUpForDriverId");
                }
            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getDriverRollUpForDriverId");
    }

    public void getDriverLeaderBoardForCustomerId(String customerId, String siteId, Date startDate, Date endDate, final OnCompleteListeners.getDriverLeaderBoardForCustomerIdListener listener) {

        Log.v("DM", "Enter Function: getDriverRollUpForDriverId");
        final OnCompleteListeners.getDriverLeaderBoardForCustomerIdListener getDriverLeaderBoard = listener;


        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Vehicle/Vehicle");

        if (isValidParameterString(customerId)) {
            urlString.append("?customerId=");
            urlString.append(customerId);
        } else {
            Log.e("DM", "getDriverRollUpForDriverId: driverId parameter is invalid/missing,throwing exception");
        }
        if (isValidParameterString(siteId)) {
            urlString.append("&siteId=");
            urlString.append(siteId);
        }

        if (startDate != null && endDate != null) {
            String startDateStr = getUTCFormatDate(startDate);
            String endDateStr = getUTCFormatDate(endDate);

            urlString.append("&start=");
            urlString.append(startDateStr);
            urlString.append("&end=");
            urlString.append(endDateStr);
        }

        urlString.append("&application=");
        urlString.append("leaderboard");


        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for getDriverLeaderBoardForCustomerId: " + object.toString());
                        VehicleDTO results = new VehicleDTO();
                        JSONObject jsonObject = (JSONObject) object;
                        results.readFromJSONObject(jsonObject);
                        listener.getDriverLeaderBoardForCustomerId(results, null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.getDriverLeaderBoardForCustomerId(null, error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException getDriverLeaderBoardForCustomerId >> " + e.getMessage());
                    listener.getDriverLeaderBoardForCustomerId(null, error);

                }
            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getDriverLeaderBoardForCustomerId");
    }

    public void postHOSActivityForUserId(String userId, Date timestamp, HOSActivity.HOSActivityTypeEnum activityType, final OnCompleteListeners.postHOSActivityForUserIdListener listener) {

        Log.v("DM", "Enter Function: postHOSActivityForUserId");
        final OnCompleteListeners.postHOSActivityForUserIdListener postHOSActivity = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Driver/HOS/HOSActivity/");
        StringBuilder parameterString = new StringBuilder();

        if (isValidParameterString(userId)) {
            urlString.append(userId);
        } else {
            Log.e("DM", "postHOSActivityForUserId: userId parameter is invalid/missing,throwing exception");
        }


        if (timestamp != null) {
            String timestampString = getUTCFormatDate(timestamp);
            parameterString.append("?timestamp=");
            parameterString.append(timestampString);
        }

        if (activityType.getValue() > 0) {
            String activityTypeString = String.format("%d", Integer.parseInt(activityType.toString()));
            parameterString.append("&activityType=");
            parameterString.append(activityTypeString);
        }
        Log.v("DM", "Create Form  API Parameters: " + parameterString);


        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initializePOSTRequest(this.authHeaderValue, parameterString.toString(), new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for postHOSActivityForUserId: " + object.toString());
                        listener.postHOSActivityForUserId(null);
                    } else if (object == null && error == null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for postHOSActivityForUserId: " + object.toString());
                        listener.postHOSActivityForUserId(null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.postHOSActivityForUserId(error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for postHOSActivityForUserId" + e.getMessage());
                    listener.postHOSActivityForUserId(error);

                }
            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: postHOSActivityForUserId");
    }

    public void postHOSActivityForUserId(String userId, String timestampStr, HOSActivity.HOSActivityTypeEnum activityType, final OnCompleteListeners.postHOSActivityForUserIdListener listener) {

        Log.v("DM", "Enter Function: postHOSActivityForUserId");
        final OnCompleteListeners.postHOSActivityForUserIdListener postHOSActivity = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Driver/HOS/HOSActivity/");

        if (isValidParameterString(userId)) {
            urlString.append(userId);
        } else {
            Log.e("DM", "postHOSActivityForUserId: userId parameter is invalid/missing,throwing exception");
        }
        if (isValidParameterString(timestampStr)) {
            urlString.append("?timestamp=");
            urlString.append(timestampStr);
        } else {
            Log.e("DM", "postHOSActivityForUserId: Timestamp parameter is invalid/missing,throwing exception");
        }


        if (Integer.parseInt(activityType.toString()) > 0) {
            String activityTypeString = String.format("%d", Integer.parseInt(activityType.toString()));
            urlString.append("&activityType=");
            urlString.append(activityTypeString);
        }


        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for postHOSActivityForUserId: " + object.toString());
                        listener.postHOSActivityForUserId(null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.postHOSActivityForUserId(error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for postHOSActivityForUserId" + e.getMessage());
                    listener.postHOSActivityForUserId(error);

                }
            }

        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: postHOSActivityForUserId");
    }

    public void createHOSActivityForUserId(String userId, Date timestamp, HOSActivity.HOSActivityTypeEnum activityType, String authorizedBy, Date authorizedDate, Date ackDate,
                                           String cancelledBy, Date cancelledDate, HOSActivity.HOSActivityCancelledReasonEnum cancelledReason, final OnCompleteListeners.onCreateHOSActivityForUserIdListener listener) {

        Log.v("DM", "Enter Function: createHOSActivityForUserId");
        final OnCompleteListeners.onCreateHOSActivityForUserIdListener createHOSActivity = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Driver/HOS/HOSActivity/");

        if (isValidParameterString(userId)) {
            urlString.append(userId);
        } else {
            Log.e("DM", "createHOSActivityForUserId: userId parameter is invalid/missing,throwing exception");
        }

        if (timestamp != null) {
            urlString.append("?timestamp=");
            urlString.append(getUTCFormatDate(timestamp));
        } else {
            Log.e("DM", "createHOSActivityForUserId: Timestamp parameter is invalid/missing,throwing exception");
        }


        if (Integer.parseInt(activityType.toString()) > 0) {
            String activityTypeString = String.format("%d", Integer.parseInt(activityType.toString()));
            urlString.append("&activityType=");
            urlString.append(activityTypeString);
        }

        if (ackDate != null) {
            urlString.append("&ackDate=");
            urlString.append(getUTCFormatDate(ackDate));
        }

        if (isValidParameterString(authorizedBy)) {
            urlString.append("&authorizedBy=");
            urlString.append(authorizedBy);
        }

        if (authorizedDate != null) {
            urlString.append("&authorizedDate=");
            urlString.append(getUTCFormatDate(authorizedDate));
        }

        if (isValidParameterString(cancelledBy)) {
            urlString.append("&cancelledBy=");
            urlString.append(cancelledBy);

            urlString.append("&cancelledReason=");
            urlString.append(String.format("%d", Integer.parseInt(cancelledReason.toString())));
        }

        if (cancelledDate != null) {
            urlString.append("&cancelledDate=");
            urlString.append(getUTCFormatDate(cancelledDate));
        }


        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for createHOSActivityForUserId: " + object.toString());
                        HOSActivity results = new HOSActivity();
                        JSONObject jsonObject = (JSONObject) object;
                        results.readFromJSONObject(jsonObject);
                        listener.createHOSActivityForUserId(results, null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.createHOSActivityForUserId(null, error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for createHOSActivityForUserId" + e.getMessage());
                    listener.createHOSActivityForUserId(null, error);

                }

            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: createHOSActivityForUserId");
    }

    public void updateHOSActivityWithActivityID(String hosActivityId, String userId, Date timestamp, HOSActivity.HOSActivityTypeEnum activityType, String authorizedBy, Date authorizedDate, String ackBy, Date ackDate,
                                                String cancelledBy, Date cancelledDate, HOSActivity.HOSActivityCancelledReasonEnum cancelledReason, String cancelledAckBy, Date cancelledAckDate, final OnCompleteListeners.onUpdateHOSActivityWithActivityIDListener listener) {

        Log.v("DM", "Enter Function: updateHOSActivityWithActivityID");
        final OnCompleteListeners.onUpdateHOSActivityWithActivityIDListener updateHOSActivity = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Driver/HOS/HOSActivity/");

        if (isValidParameterString(userId)) {
            urlString.append(userId);
        } else {
            Log.e("DM", "updateHOSActivityWithActivityID: userId parameter is invalid/missing,throwing exception");
        }
        if (isValidParameterString(hosActivityId)) {
            urlString.append("?hosActivityId=");
            urlString.append(hosActivityId);
        }
        if (timestamp != null) {
            urlString.append("&timestamp=");
            urlString.append(getUTCFormatDate(timestamp));
        } else {
            Log.e("DM", "updateHOSActivityWithActivityID: hosActivityId parameter is invalid/missing,throwing exception");
        }


        if (Integer.parseInt(activityType.toString()) > 0) {
            String activityTypeString = String.format("%d", Integer.parseInt(activityType.toString()));
            urlString.append("&activityType=");
            urlString.append(activityTypeString);
        }

        if (isValidParameterString(ackBy)) {
            urlString.append("&ackBy=");
            urlString.append(ackBy);
        }

        if (ackDate != null) {
            urlString.append("&ackDate=");
            urlString.append(getUTCFormatDate(ackDate));
        }

        if (isValidParameterString(authorizedBy)) {
            urlString.append("&authorizedBy=");
            urlString.append(authorizedBy);
        }

        if (authorizedDate != null) {
            urlString.append("&authorizedDate=");
            urlString.append(getUTCFormatDate(authorizedDate));
        }

        if (isValidParameterString(cancelledBy)) {
            urlString.append("&cancelledBy=");
            urlString.append(cancelledBy);

            urlString.append("&cancelledReason=");
            urlString.append(String.format("%d", Integer.parseInt(cancelledReason.toString())));
        }

        if (cancelledDate != null) {
            urlString.append("&cancelledDate=");
            urlString.append(getUTCFormatDate(cancelledDate));
        }

        if (isValidParameterString(cancelledAckBy)) {
            urlString.append("&cancelledAckBy=");
            urlString.append(cancelledAckBy);
        }

        if (cancelledAckDate != null) {
            urlString.append("&cancelledAckDate=");
            urlString.append(getUTCFormatDate(cancelledAckDate));
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for updateHOSActivityWithActivityID: " + object.toString());
                        HOSActivity results = new HOSActivity();
                        JSONObject jsonObject = (JSONObject) object;
                        results.readFromJSONObject(jsonObject);
                        listener.updateHOSActivityWithActivityID(results, null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.updateHOSActivityWithActivityID(null, error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for updateHOSActivityWithActivityID" + e.getMessage());
                    listener.updateHOSActivityWithActivityID(null, error);

                }
            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: updateHOSActivityWithActivityID");
    }

    public void deleteHOSActivityForUserId(String userId, HOSActivity.HOSActivityTypeEnum activityType, String startTime, String endTime, final OnCompleteListeners.ondeleteHOSActivityForUserIdListener listener) {

        Log.v("DM", "Enter Function: deleteHOSActivityForUserId");
        final OnCompleteListeners.ondeleteHOSActivityForUserIdListener deleteHOSActivity = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Driver/HOS/HOSActivity/");

        if (isValidParameterString(userId)) {
            urlString.append(userId);
        } else {
            Log.e("DM", "deleteHOSActivityForUserId: userId parameter is invalid/missing,throwing exception");
        }

        if (Integer.parseInt(activityType.toString()) > 0) {
            String activityTypeString = String.format("%d", Integer.parseInt(activityType.toString()));
            urlString.append("?activityType=");
            urlString.append(activityTypeString);
        }
        if (startTime != null) {
            urlString.append("&start=");
            urlString.append(startTime);
        }
        if (endTime != null) {
            urlString.append("&end=");
            urlString.append(endTime);
        }


        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for deleteHOSActivityForUserId: " + object.toString());
                        listener.deleteHOSActivityForUserId(null);
                    } else if (object == null && error == null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for deleteHOSActivityForUserId: " + object.toString());
                        listener.deleteHOSActivityForUserId(null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.deleteHOSActivityForUserId(error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for deleteHOSActivityForUserId" + e.getMessage());
                    listener.deleteHOSActivityForUserId(error);

                }

            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: deleteHOSActivityForUserId");
    }

    public void createDocumentQualification(Qualification documentQualification, final OnCompleteListeners.oncreateDocumentQualificationListener listener) {

        Log.v("DM", "Enter Function: createDocumentQualification");
        final OnCompleteListeners.oncreateDocumentQualificationListener createDocumentQualification = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Document/Qualification");

        if (isValidParameterString(documentQualification.qualificationTypeID)) {
            urlString.append("?qualificationTypeId=");
            urlString.append(documentQualification.qualificationTypeID);
        } else {
            Log.e("DM", "createDocumentQualification: qualificationTypeId parameter is invalid/missing,throwing exception");
        }

        if (isValidParameterString(documentQualification.userID)) {
            urlString.append("&userId=");
            urlString.append(documentQualification.userID);
        }
        if (documentQualification.effectiveDate != null) {
            urlString.append("&effectiveDate=");
            urlString.append(getUTCFormatDate(documentQualification.effectiveDate));
        }
        if (documentQualification.expirationDate != null) {
            urlString.append("&expirationDate=");
            urlString.append(getUTCFormatDate(documentQualification.expirationDate));
        }
        if (Integer.parseInt(documentQualification.status.toString()) >= 0) {
            urlString.append("&status=");
            urlString.append(String.format("%d", Integer.parseInt(documentQualification.status.toString())));
        }


        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "createDocumentQualification", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for createDocumentQualification: " + object.toString());
                    Qualification results = new Qualification();
                    results.readFromJSONObject(object);
                    listener.createDocumentQualification(results);
                } else {
                    Log.d("DM", "Null object response for createDocumentQualification");
                }
            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: createDocumentQualification");
    }

    public void updateDocumentQualificationWithQualificationId(String qualificationId, Qualification documentQualification, final OnCompleteListeners.onupdateDocumentQualificationWithQualificationIdListener listener) {

        Log.v("DM", "Enter Function: updateDocumentQualificationWithQualificationId");
        final OnCompleteListeners.onupdateDocumentQualificationWithQualificationIdListener updateDocumentQualification = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Document/Qualification");

        if (isValidParameterString(qualificationId)) {
            urlString.append("?id=");
            urlString.append(qualificationId);
        } else {
            Log.e("DM", "updateDocumentQualificationWithQualificationId: qualificationId parameter is invalid/missing,throwing exception");
        }
        if (isValidParameterString(documentQualification.qualificationTypeID)) {
            urlString.append("&qualificationTypeId=");
            urlString.append(documentQualification.qualificationTypeID);
        } else {
            Log.e("DM", "updateDocumentQualificationWithQualificationId: qualificationTypeId parameter is invalid/missing,throwing exception");
        }

        if (isValidParameterString(documentQualification.userID)) {
            urlString.append("&userId=");
            urlString.append(documentQualification.userID);
        }
        if (documentQualification.effectiveDate != null) {
            urlString.append("&effectiveDate=");
            urlString.append(getUTCFormatDate(documentQualification.effectiveDate));
        }
        if (documentQualification.expirationDate != null) {
            urlString.append("&expirationDate=");
            urlString.append(getUTCFormatDate(documentQualification.expirationDate));
        }
        if (Integer.parseInt(documentQualification.status.toString()) >= 0) {
            urlString.append("&status=");
            urlString.append(String.format("%d", Integer.parseInt(documentQualification.status.toString())));
        }


        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "updateDocumentQualification", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for updateDocumentQualificationWithQualificationId: " + object.toString());
                    Qualification results = new Qualification();
                    results.readFromJSONObject(object);
                    listener.updateDocumentQualificationWithQualificationId(results);
                } else {
                    Log.d("DM", "Null object response for updateDocumentQualificationWithQualificationId");
                }
            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: updateDocumentQualificationWithQualificationId");
    }

    public void createDocumentQualificationFormForQualificationId(String qualificationId, String appId, String formDefinitionId, String formBody, final OnCompleteListeners.oncreateDocumentQualificationFormForQualificationIdListener listener) {

        Log.v("DM", "Enter Function: createDocumentQualificationFormForQualificationId");
        final OnCompleteListeners.oncreateDocumentQualificationFormForQualificationIdListener reateDocumentQualificationForm = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("PimmForm/Form/Qualification");

        if (isValidParameterString(appId)) {
            urlString.append("?appId=");
            urlString.append(appId);
        } else {
            Log.e("DM", "createDocumentQualificationFormForQualificationId: appId parameter is invalid/missing,throwing exception");
        }
        if (isValidParameterString(formDefinitionId)) {
            urlString.append("&formDefinitionId=");
            urlString.append(formDefinitionId);
        } else {
            Log.e("DM", "createDocumentQualificationFormForQualificationId: formDefinitionId parameter is invalid/missing,throwing exception");
        }

        if (isValidParameterString(qualificationId)) {
            urlString.append("&qualificationId=");
            urlString.append(qualificationId);
        } else {
            Log.e("DM", "createDocumentQualificationFormForQualificationId: qualificationId parameter is invalid/missing,throwing exception");
        }

        //try
        String input = Base64Converter.toBase64(formBody);
        if (isValidParameterString(formBody)) {
            urlString.append("&body=");
            urlString.append(input);
        } else {
            Log.e("DM", "createDocumentQualificationFormForQualificationId: Form Body parameter is invalid/missing,throwing exception");
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "creatDocumentQualificationForm", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for createDocumentQualificationFormForQualificationId: " + object.toString());
                    PimmForm results = new PimmForm();
                    results.readFromJSONObject(object);
                    listener.createDocumentQualificationFormForQualificationId(results);
                } else {
                    Log.d("DM", "Null object response for createDocumentQualificationFormForQualificationId");
                }
            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: createDocumentQualificationFormForQualificationId");
    }

    public void updateDocumentQualificationFormForQualificationId(String qualificationId, String formId, String formBody, final OnCompleteListeners.onupdateDocumentQualificationFormForQualificationIdListener listener) {

        Log.v("DM", "Enter Function: updateDocumentQualificationFormForQualificationId");
        final OnCompleteListeners.onupdateDocumentQualificationFormForQualificationIdListener updateDocumentQualificationForm = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("PimmForm/Form/Qualification");

        if (isValidParameterString(formId)) {
            urlString.append("?formId=");
            urlString.append(formId);
        } else {
            Log.e("DM", "updateDocumentQualificationFormForQualificationId: formId parameter is invalid/missing,throwing exception");
        }

        if (isValidParameterString(qualificationId)) {
            urlString.append("&qualificationId=");
            urlString.append(qualificationId);
        } else {
            Log.e("DM", "updateDocumentQualificationFormForQualificationId: qualificationId parameter is invalid/missing,throwing exception");
        }

        //try
        String input = Base64Converter.toBase64(formBody);
        if (isValidParameterString(formBody)) {
            urlString.append("&body=");
            urlString.append(input);
        } else {
            Log.e("DM", "updateDocumentQualificationFormForQualificationId: Form Body parameter is invalid/missing,throwing exception");
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "updateDocumentQualificationForm", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for updateDocumentQualificationFormForQualificationId: " + object.toString());
                    PimmForm results = new PimmForm();
                    results.readFromJSONObject(object);
                    listener.updateDocumentQualificationFormForQualificationId(results);
                } else {
                    Log.d("DM", "Null object response for updateDocumentQualificationFormForQualificationId");
                }
            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: updateDocumentQualificationFormForQualificationId");
    }

    public void getQualificationWithQualificationId(String qualificationId, final OnCompleteListeners.getQualificationWithQualificationIdListener listener) {

        Log.v("DM", "Enter Function: getQualificationWithQualificationId");
        final OnCompleteListeners.getQualificationWithQualificationIdListener getQualification = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Document/Qualification");


        if (isValidParameterString(qualificationId)) {
            urlString.append("?id=");
            urlString.append(qualificationId);
        } else {
            Log.e("DM", "getQualificationWithQualificationId: qualificationId parameter is invalid/missing,throwing exception");
        }


        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "getQualification", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for getQualificationWithQualificationId: " + object.toString());
                    Qualification results = new Qualification();
                    listener.getQualificationWithQualificationId(results);
                } else {
                    Log.d("DM", "Null object response for getQualificationWithQualificationId");
                }
            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getQualificationWithQualificationId");
    }

    public void createMobileDeviceWithSerialNumber(String serialNumber, String siteId, String deviceType, String deviceName, String groupName,
                                                   String referenceId, Integer refType, final OnCompleteListeners.oncreateMobileDeviceWithSerialNumberListener listener) {

        Log.v("DM", "Enter Function: createMobileDeviceWithSerialNumber");
        final OnCompleteListeners.oncreateMobileDeviceWithSerialNumberListener createMobileDevice = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Device/Mobile");

        if (isValidParameterString(serialNumber)) {
            urlString.append("?serialNumber=");
            urlString.append(serialNumber);
        } else {
            Log.e("DM", "createMobileDeviceWithSerialNumber: serialNumber parameter is invalid/missing,throwing exception");
        }

        if (isValidParameterString(siteId)) {
            urlString.append("&siteId=");
            urlString.append(siteId);
        } else {
            Log.e("DM", "createMobileDeviceWithSerialNumber: siteId parameter is invalid/missing,throwing exception");
        }

        if (isValidParameterString(deviceType)) {
            urlString.append("&deviceType=");
            urlString.append(deviceType);
        } else {
            Log.e("DM", "createMobileDeviceWithSerialNumber: deviceType parameter is invalid/missing,throwing exception");
        }

        if (isValidParameterString(deviceName)) {
            urlString.append("&deviceName=");
            urlString.append(deviceName);
        } else {
            Log.e("DM", "createMobileDeviceWithSerialNumber: deviceName parameter is invalid/missing,throwing exception");
        }

        if (isValidParameterString(groupName)) {
            urlString.append("&groupName=");
            urlString.append(groupName);
        } else {
            Log.e("DM", "createMobileDeviceWithSerialNumber: groupName parameter is invalid/missing,throwing exception");
        }

        if (isValidParameterString(referenceId)) {
            urlString.append("&refx=");
            urlString.append(referenceId);

            if (refType >= 0) {
                urlString.append("&refType=");
                urlString.append(String.format("%d", Integer.parseInt(refType.toString())));
            } else {
                Log.e("DM", "ReferenceType parameter cannot be null if ReferenceId is not null");
            }
        } else {
            Log.e("DM", "createMobileDeviceWithSerialNumber: referenceId parameter is invalid/missing,throwing exception");
        }


        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "createMobileDevice", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for createMobileDeviceWithSerialNumber: " + object.toString());
                    Device results = new Device();
                    results.readFromJSONObject(object);
                    listener.createMobileDeviceWithSerialNumber(results);
                } else {
                    Log.d("DM", "Null object response for createMobileDeviceWithSerialNumber");
                }
            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: createMobileDeviceWithSerialNumber");
    }

    public void updateMobileDeviceWithDeviceId(String deviceId, String serialNumber, String siteId, String deviceType, String deviceName, String groupName,
                                               String referenceId, PimmRefTypeEnum refType, final OnCompleteListeners.onupdateMobileDeviceWithDeviceIdNumberListener listener) {

        Log.v("DM", "Enter Function: updateMobileDeviceWithDeviceId");
        final OnCompleteListeners.onupdateMobileDeviceWithDeviceIdNumberListener updateMobileDevice = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Device/Mobile");

        if (isValidParameterString(deviceId)) {
            urlString.append("?deviceId=");
            urlString.append(deviceId);
        } else {
            Log.e("DM", "updateMobileDeviceWithDeviceId: deviceId parameter is invalid/missing,throwing exception");
        }

        if (isValidParameterString(serialNumber)) {
            urlString.append("&serialNumber=");
            urlString.append(serialNumber);
        } else {
            Log.e("DM", "updateMobileDeviceWithDeviceId: serialNumber parameter is invalid/missing,throwing exception");
        }

        if (isValidParameterString(siteId)) {
            urlString.append("&siteId=");
            urlString.append(siteId);
        } else {
            Log.e("DM", "updateMobileDeviceWithDeviceId: siteId parameter is invalid/missing,throwing exception");
        }

        if (isValidParameterString(deviceType)) {
            urlString.append("&deviceType=");
            urlString.append(deviceType);
        } else {
            Log.e("DM", "updateMobileDeviceWithDeviceId: deviceType parameter is invalid/missing,throwing exception");
        }

        if (isValidParameterString(deviceName)) {
            urlString.append("&deviceName=");
            urlString.append(deviceName);
        } else {
            Log.e("DM", "updateMobileDeviceWithDeviceId: deviceName parameter is invalid/missing,throwing exception");
        }

        if (isValidParameterString(groupName)) {
            urlString.append("&groupName=");
            urlString.append(groupName);
        } else {
            Log.e("DM", "updateMobileDeviceWithDeviceId: groupName parameter is invalid/missing,throwing exception");
        }

        if (isValidParameterString(referenceId)) {
            urlString.append("&refx=");
            urlString.append(referenceId);

            if (Integer.parseInt(refType.toString()) >= 0) {
                urlString.append("&refType=");
                urlString.append(String.format("%d", Integer.parseInt(refType.toString())));
            } else {
                Log.e("DM", "updateMobileDeviceWithDeviceId: ReferenceType parameter cannot be null if ReferenceId is not null");
            }
        } else {
            Log.e("DM", "updateMobileDeviceWithDeviceId: referenceId parameter is invalid/missing,throwing exception");
        }


        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "updateMobileDevice", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for updateMobileDeviceWithDeviceId: " + object.toString());
                    Device results = new Device();
                    results.readFromJSONObject(object);
                    listener.updateMobileDeviceWithDeviceId(results);
                } else {
                    Log.d("DM", "Null object response for updateMobileDeviceWithDeviceId");
                }
            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: updateMobileDeviceWithDeviceId");
    }
//the first element of the deviceList array will contain the device object corresponding to the deviceId


    public void getMobileDeviceDetailsForDeviceId(String deviceId, final OnCompleteListeners.getMobileDeviceDetailsForDeviceIdListener listener) {

        Log.v("DM", "Enter Function: updateMobileDeviceWithDeviceId");
        final OnCompleteListeners.getMobileDeviceDetailsForDeviceIdListener getMobileDeviceDetails = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Device/Mobile");

        if (isValidParameterString(deviceId)) {
            urlString.append("?deviceId=");
            urlString.append(deviceId);
        } else {
            Log.e("DM", "getMobileDeviceDetails: deviceId parameter is invalid/missing,throwing exception");
        }


        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "getMobileDeviceDetails", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for getMobileDeviceDetails: " + object.toString());
                    Device results = new Device();
                    results.readFromJSONObject(object);
                    listener.getMobileDeviceDetailsForDeviceId(results);
                } else {
                    Log.d("DM", "Null object response for getMobileDeviceDetails");
                }
            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getMobileDeviceDetails");
    }

    public void deleteMobileDeviceWithDeviceId(String deviceId, final OnCompleteListeners.ondeleteMobileDeviceWithDeviceIdListener listener) {

        Log.v("DM", "Enter Function: deleteMobileDeviceWithDeviceId");
        final OnCompleteListeners.ondeleteMobileDeviceWithDeviceIdListener deleteMobileDevice = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Device/Mobile");

        if (isValidParameterString(deviceId)) {
            urlString.append("?deviceId=");
            urlString.append(deviceId);
        } else {
            Log.e("DM", "deleteMobileDeviceWithDeviceId: deviceId parameter is invalid/missing,throwing exception");
        }


        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "deleteMobileDeviceWithDeviceId", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for deleteMobileDeviceWithDeviceId: " + object.toString());

                    listener.deleteMobileDeviceWithDeviceId();
                } else {
                    Log.d("DM", "Null object response for deleteMobileDeviceWithDeviceId");
                }
            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: deleteMobileDeviceWithDeviceId");
    }


    ///*** iPimmFacilityDataManager ***////


    public void getFacilityProductForFacilityProductId(String facilityProductId, final OnCompleteListeners.getFacilityProductForFacilityProductIdListener listener) {

        Log.v("DM", "Enter Function: getFacilityProductForFacilityProductId");
        final OnCompleteListeners.getFacilityProductForFacilityProductIdListener getFacilityProduct = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/FacilityProduct");

        if (isValidParameterString(facilityProductId)) {
            urlString.append("?facilityProductId=");
            urlString.append(facilityProductId);
        } else {
            Log.e("DM", "getFacilityProductForFacilityProductId: facilityProductId parameter is invalid/missing,throwing exception");
        }


        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for getFacilityProductForFacilityProductId: " + object.toString());
                        FacilityProductDTO results = new FacilityProductDTO();
                        JSONObject jsonObject = (JSONObject) object;
                        results.readFromJSONObject(jsonObject);
                        listener.getFacilityProductForFacilityProductId(results, null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.getFacilityProductForFacilityProductId(null, error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for pushGPSEventForPOIWithDeliveryId" + e.getMessage());
                    listener.getFacilityProductForFacilityProductId(null, error);

                }
            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getFacilityProductForFacilityProductId");
    }


    public void getFacilityProductListForCustomerId(String customerId, boolean valid, final OnCompleteListeners.getFacilityProductListForCustomerIdListener listener) {
        Log.v("DM", "Enter Function: getFacilityProductListForCustomerId");

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/FacilityProduct");

        if (isValidParameterString(customerId)) {
            urlString.append("?customerId=");
            urlString.append(customerId);
        }
        urlString.append("&valid=");
        if (valid) {
            urlString.append("true");
        } else {
            urlString.append("false");
        }


        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                if (error == null) {
                    try {
                        JSONArray jsonArray = (JSONArray) object;
                        ArrayList<FacilityProductDTO> resultArrayList = new ArrayList<FacilityProductDTO>();

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);

                            FacilityProductDTO result = new FacilityProductDTO();
                            result.readFromJSONObject(jsonObject);
                            resultArrayList.add(result);
                        }

                        listener.getFacilityProductListForCustomerId(resultArrayList, null);
                    } catch (JSONException e) {
                        listener.getFacilityProductListForCustomerId(null, new Error(e.getMessage()));
                    }

                } else {
                    listener.getFacilityProductListForCustomerId(null, error);
                }
            }
        });


        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getFacilityProductListForCustomerId");

    }

    public void getFacilityProductSiteEntryForFacilityProductSiteId(String facilityProductSiteId, final OnCompleteListeners.getFacilityProductSiteEntryForFacilityProductSiteIdListener listener) {

        Log.v("DM", "Enter Function: getFacilityProductSiteEntryForFacilityProductSiteId");
        final OnCompleteListeners.getFacilityProductSiteEntryForFacilityProductSiteIdListener getFacilityProductSiteEntry = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/FacilityProductSite");

        if (isValidParameterString(facilityProductSiteId)) {
            urlString.append("?facilityProductSiteId=");
            urlString.append(facilityProductSiteId);
        } else {
            Log.e("DM", "getFacilityProductSiteEntryForFacilityProductSiteId: facilityProductSiteId parameter is invalid/missing,throwing exception");
        }


        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for getFacilityProductSiteEntryForFacilityProductSiteId: " + object.toString());
                        FacilityProductSiteDTO results = new FacilityProductSiteDTO();
                        JSONObject jsonObject = (JSONObject) object;
                        results.readFromJSONObject(jsonObject);
                        listener.getFacilityProductSiteEntryForFacilityProductSiteId(results, null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.getFacilityProductSiteEntryForFacilityProductSiteId(null, error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for pushGPSEventForPOIWithDeliveryId" + e.getMessage());
                    listener.getFacilityProductSiteEntryForFacilityProductSiteId(null, error);

                }
            }

        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getFacilityProductSiteEntryForFacilityProductSiteId");
    }


    public void getFacilityProductSiteListForSiteId(String siteId, boolean valid, final OnCompleteListeners.getFacilityProductSiteListForSiteIdListener listener) {
        Log.v("DM", "Enter Function: getFacilityProductListForCustomerId");

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/FacilityProductSite");

        if (isValidParameterString(siteId)) {
            urlString.append("?siteId=");
            urlString.append(siteId);
        }
        urlString.append("&valid=");
        if (valid) {
            urlString.append("true");
        } else {
            urlString.append("false");
        }


        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                if (error == null) {
                    try {
                        JSONArray jsonArray = (JSONArray) object;
                        ArrayList<FacilityProductSiteDTO> resultArrayList = new ArrayList<FacilityProductSiteDTO>();

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);

                            FacilityProductSiteDTO result = new FacilityProductSiteDTO();
                            result.readFromJSONObject(jsonObject);
                            resultArrayList.add(result);
                        }

                        listener.getFacilityProductSiteListForSiteId(resultArrayList, null);
                    } catch (JSONException e) {
                        listener.getFacilityProductSiteListForSiteId(null, new Error(e.getMessage()));
                    }

                } else {
                    listener.getFacilityProductSiteListForSiteId(null, error);
                }
            }
        });


        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getFacilityProductSiteListForSiteId");

    }


    public void getFacilityDataForSiteId(String siteId, String deviceId, Date startDate, Date endDate,  final OnCompleteListeners.getFacilityDataForSiteIdListener listener){
        Log.v("DM", "Enter Function: getFacilityDataForSiteId");

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Facility/Data");

        if(isValidParameterString(siteId)) {
            urlString.append("?siteId=");
            urlString.append(siteId);
        }
        if(isValidParameterString(deviceId)) {
            urlString.append("&deviceId=");
            urlString.append(deviceId);
        }

        if(startDate != null && endDate != null){
            urlString.append("&startDate=");
            urlString.append(getUTCFormatDate(startDate));

            urlString.append("&endDate=");
            urlString.append(getUTCFormatDate(endDate));
        }


        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue,  new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                if(error == null)
                {
                    try
                    {
                        JSONArray jsonArray = (JSONArray)object;
                        ArrayList<Instance_Performance> resultArrayList = new ArrayList<Instance_Performance>();

                        for(int i = 0; i < jsonArray.length(); i ++)
                        {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);

                            Instance_Performance result = new Instance_Performance();
                            result.readFromJSONObject(jsonObject);
                            resultArrayList.add(result);
                        }

                        listener.getFacilityProductSiteListForSiteId(resultArrayList, null);
                    }
                    catch (JSONException e)
                    {
                        listener.getFacilityProductSiteListForSiteId(null, new Error(e.getMessage()));
                    }

                }
                else
                {
                    listener.getFacilityProductSiteListForSiteId(null, error);
                }
            }
        });


        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getFacilityProductSiteListForSiteId");

    }

    public void getBuisnessScheduleForCustomerId(String customerId,  final OnCompleteListeners.getBuisnessScheduleForCustomerIdListener listener){
        Log.v("DM", "Enter Function: getBuisnessScheduleForCustomerId");

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Facility/Schedule");

        if(isValidParameterString(customerId)) {
            urlString.append("?customerId=");
            urlString.append(customerId);
        }else{
            Log.e("DM", "CustomerId should not be null/empty");
        }


        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue,  new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                if(error == null)
                {
                    try
                    {
                        JSONArray jsonArray = (JSONArray)object;
                        ArrayList<Schedule_Buisness> resultArrayList = new ArrayList<Schedule_Buisness>();

                        for(int i = 0; i < jsonArray.length(); i ++)
                        {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);

                            Schedule_Buisness result = new Schedule_Buisness();
                            result.readFromJSONObject(jsonObject);
                            resultArrayList.add(result);
                        }

                        listener.getBuisnessScheduleForCustomerId(resultArrayList, null);
                    }
                    catch (JSONException e)
                    {
                        listener.getBuisnessScheduleForCustomerId(null, new Error(e.getMessage()));
                    }

                }
                else
                {
                    listener.getBuisnessScheduleForCustomerId(null, error);
                }
            }
        });


        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getFacilityProductSiteListForSiteId");

    }


    public void getBuisnessScheduleForScheduleId(String scheduleId, final OnCompleteListeners.getBuisnessScheduleForScheduleIdListener listener) {

        Log.v("DM", "Enter Function: getBuisnessScheduleForScheduleId");
        final OnCompleteListeners.getBuisnessScheduleForScheduleIdListener BuisnessScheduleForScheduleId = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Facility/Schedule");

        if (isValidParameterString(scheduleId)) {
            urlString.append("?schedule_BusinessId=");
            urlString.append(scheduleId);
        } else {
            Log.e("DM", "getBuisnessScheduleForScheduleId: facilityProductSiteId parameter is invalid/missing,throwing exception");
        }


        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for getBuisnessScheduleForScheduleId: " + object.toString());
                        Schedule_Buisness results = new Schedule_Buisness();
                        JSONObject jsonObject = (JSONObject) object;
                        results.readFromJSONObject(jsonObject);
                        listener.getBuisnessScheduleForScheduleId(results, null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.getBuisnessScheduleForScheduleId(null, error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for pushGPSEventForPOIWithDeliveryId" + e.getMessage());
                    listener.getBuisnessScheduleForScheduleId(null, error);

                }
            }

        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getFacilityProductSiteEntryForFacilityProductSiteId");
    }


    public void getBuisnessScheduleForSiteId(String siteId,  final OnCompleteListeners.getBuisnessScheduleForSiteIdListener listener){
        Log.v("DM", "Enter Function: getBuisnessScheduleForSiteId");

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Facility/ScheduleSite");

        if(isValidParameterString(siteId)) {
            urlString.append("?siteId=");
            urlString.append(siteId);
        }else{
            Log.e("DM", "siteId should not be null/empty");
        }


        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue,  new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                if(error == null)
                {
                    try
                    {
                        JSONArray jsonArray = (JSONArray)object;
                        ArrayList<Schedule_Buisness_Site> resultArrayList = new ArrayList<Schedule_Buisness_Site>();

                        for(int i = 0; i < jsonArray.length(); i ++)
                        {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);

                            Schedule_Buisness_Site result = new Schedule_Buisness_Site();
                            result.readFromJSONObject(jsonObject);
                            resultArrayList.add(result);
                        }

                        listener.getBuisnessScheduleForSiteId(resultArrayList, null);
                    }
                    catch (JSONException e)
                    {
                        listener.getBuisnessScheduleForSiteId(null, new Error(e.getMessage()));
                    }

                }
                else
                {
                    listener.getBuisnessScheduleForSiteId(null, error);
                }
            }
        });


        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getBuisnessScheduleForSiteId");

    }


    public void getSiteBuisnessScheduleForScheduleId(String scheduleId, final OnCompleteListeners.getSiteBuisnessScheduleForScheduleIdListener listener) {

        Log.v("DM", "Enter Function: getSiteBuisnessScheduleForScheduleId");
        final OnCompleteListeners.getSiteBuisnessScheduleForScheduleIdListener SiteBuisnessScheduleForScheduleId = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Facility/ScheduleSite");

        if (isValidParameterString(scheduleId)) {
            urlString.append("?schedule_Business_SiteId=");
            urlString.append(scheduleId);
        } else {
            Log.e("DM", "getSiteBuisnessScheduleForScheduleId: ScheduleId should not be null/empty");
        }


        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for getBuisnessScheduleForScheduleId: " + object.toString());
                        Schedule_Buisness_Site results = new Schedule_Buisness_Site();
                        JSONObject jsonObject = (JSONObject) object;
                        results.readFromJSONObject(jsonObject);
                        listener.getSiteBuisnessScheduleForScheduleId(results, null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.getSiteBuisnessScheduleForScheduleId(null, error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for pushGPSEventForPOIWithDeliveryId" + e.getMessage());
                    listener.getSiteBuisnessScheduleForScheduleId(null, error);

                }
            }

        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getSiteBuisnessScheduleForScheduleId");
    }


    public void getFacilityStatusForSiteId(String siteId,  final OnCompleteListeners.getFacilityStatusForSiteIdListener listener){
        Log.v("DM", "Enter Function: getFacilityStatusForSiteId");

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Facility/Status");

        if(isValidParameterString(siteId)) {
            urlString.append("?siteId=");
            urlString.append(siteId);
        }else{
            Log.e("DM", "siteId should not be null/empty");
        }


        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue,  new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                if(error == null)
                {
                    try
                    {
                        JSONArray jsonArray = (JSONArray)object;
                        ArrayList<PimmDevice> resultArrayList = new ArrayList<PimmDevice>();

                        for(int i = 0; i < jsonArray.length(); i ++)
                        {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);

                            PimmDevice result = new PimmDevice();
                            result.readFromJSONObject(jsonObject);
                            resultArrayList.add(result);
                        }

                        listener.getFacilityStatusForSiteId(resultArrayList, null);
                    }
                    catch (JSONException e)
                    {
                        listener.getFacilityStatusForSiteId(null, new Error(e.getMessage()));
                    }

                }
                else
                {
                    listener.getFacilityStatusForSiteId(null, error);
                }
            }
        });


        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getBuisnessScheduleForSiteId");

    }

    public void getFacilityStatusForSiteClass(String siteClass, int siteType, String deviceClass ,  final OnCompleteListeners.getFacilityStatusForSiteClassListener listener){
        Log.v("SiteClass", "Enter Function: getFacilityStatusForSiteClass");

//        https://wendys.pimm.us/WebPimm5/Rest/Facility/Status?siteType=50
        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Facility/Status");

        if(isValidParameterString(siteClass)) {
            urlString.append("?siteClass=");
            urlString.append(siteClass);
        }

        if (siteType >= 0) {
            urlString.append("?siteType=");
            urlString.append(siteType);
        }

        if(isValidParameterString(deviceClass)) {
            urlString.append("&deviceClass=");
            urlString.append(deviceClass);
        }

        Log.v("SiteClass", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue,  new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                if(error == null)
                {
                    try
                    {
                        JSONArray jsonArray = (JSONArray) object;
                        Log.v("SiteClass", "Recieved JSON Object: " + jsonArray.toString());

                        ArrayList<PimmDevice> resultArrayList = new ArrayList<PimmDevice>();

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);

                            PimmDevice result = new PimmDevice();
                            result.readFromJSONObject(jsonObject);
                            resultArrayList.add(result);


                            System.out.println("Severity: " + result.severity);
                        }

                        listener.getFacilityStatusForSiteClass(resultArrayList, null);

                    }
                    catch (JSONException e)
                    {
                        listener.getFacilityStatusForSiteClass(null, new Error(e.getMessage()));
                    }

                }
                else
                {
                    Log.e("SiteClass", "JSONException for getFacilityStatusForSiteClass: " + error.getMessage());
                    listener.getFacilityStatusForSiteClass(null, error);
                }
            }
        });


        restConnection.execute(String.valueOf(urlString));
        Log.v("SiteClass", "ExitFunction: getFacilityStatusForSiteClass");

    }



//    public void getSiteSettingsForSiteId(String siteId, final OnCompleteListeners.getSiteSettingsForSiteIdListener listener) {
//
//        Log.v("DM", "Enter Function: getSiteSettingsForSiteId");
//        final OnCompleteListeners.getSiteSettingsForSiteIdListener SiteSettingsForSiteId = listener;
//
//        StringBuilder urlString = new StringBuilder();
//        urlString.append(this.baseUrl);
//        urlString.append("StoreDelivery/Settings");
//
//        if (isValidParameterString(siteId)) {
//            urlString.append("?siteId=");
//            urlString.append(siteId);
//        } else {
//            Log.e("DM", "getSiteSettingsForSiteId: siteId should not be null/empty");
//        }
//
//
//        Log.v("DM", "Request URL: " + urlString);
//
//        RestConnection restConnection = new RestConnection();
//        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
//            @Override
//            public void onRestConnectionComplete(Object object, Error error) {
//                try {
//                    if (object != null) {
//                        new JSONObject(String.valueOf(object));
//                        Log.v("DM", "Received JSON Object for getSiteSettingsForSiteId: " + object.toString());
//                        SiteSettings results = new SiteSettings();
//                        JSONObject jsonObject = (JSONObject) object;
//                        results.readFromJSONObject(jsonObject);
//                        listener.getSiteSettingsForSiteId(results, null);
//                    } else {
//                        Log.e("DM", "Null object ");
//                        listener.getSiteSettingsForSiteId(null, error);
//                    }
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                    Log.e("DM", "JSONException for getSiteSettingsForSiteId" + e.getMessage());
//                    listener.getSiteSettingsForSiteId(null, error);
//
//                }
//            }
//
//        });
//
//        restConnection.execute(String.valueOf(urlString));
//        Log.v("DM", "ExitFunction: getSiteSettingsForSiteId");
//    }

    public void getIFTAReportDataForCustomerId(String customerId, String deviceId, String startTime, String endTime,final OnCompleteListeners.getIFTAReportDataForCustomerIdListener listener ) {

        Log.v("DM", "Enter Function: getIFTAReportDataForCustomerId");
        final OnCompleteListeners.getIFTAReportDataForCustomerIdListener getIFTAReportData = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Vehicle/IFTA");

        if(isValidParameterString(customerId)){
            urlString.append("?customerId=");
            urlString.append(customerId);
        }else{
            Log.e("DM", "getIFTAReportDataForCustomerId: customerId parameter is invalid/missing,throwing exception");
        }

        if(isValidParameterString(deviceId)){
            urlString.append("&deviceId=");
            urlString.append(deviceId);
        }else{
            Log.e("DM", "getIFTAReportDataForCustomerId: deviceId parameter is invalid/missing,throwing exception");
        }

        if(startTime != null && endTime!=null){
            urlString.append("&start=");
            urlString.append(startTime);
            urlString.append("&end=");
            urlString.append(endTime);
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue,   new OnCompleteListeners.OnRestConnectionCompleteListener(){
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for getIFTAReportDataForCustomerId: " + object.toString());
                         IFTA_ECM results = new IFTA_ECM();
                        JSONObject jsonObject = (JSONObject) object;
                        results.readFromJSONObject(jsonObject);
                        listener.getIFTAReportDataForCustomerId(results, null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.getIFTAReportDataForCustomerId(null, error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for getIFTAReportDataForCustomerId" + e.getMessage());
                    listener.getIFTAReportDataForCustomerId(null, error);

                }
            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getIFTAReportDataForCustomerId");
    }

    public void getMobileDeviceAlertForAlertId(String alertId, String endTime,final OnCompleteListeners.getMobileDeviceAlertForAlertIdListener listener ) {

        Log.v("DM", "Enter Function: getMobileDeviceAlertForAlertId");
        final OnCompleteListeners.getMobileDeviceAlertForAlertIdListener getMobileDeviceAlert = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Device/MobileAlert");

        if(isValidParameterString(alertId)){
            urlString.append("?alertId=");
            urlString.append(alertId);
        }else{
            Log.e("DM", "getMobileDeviceAlertForAlertId: alertId parameter is invalid/missing,throwing exception");
        }


        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "getMobileDeviceAlertForAlertId", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for getMobileDeviceAlertForAlertId: " + object.toString());
                    MobileDeviceAlert results = new MobileDeviceAlert();
                    results.readFromJSONObject(object);
                    listener.getMobileDeviceAlertForAlertId(results);
                } else {
                    Log.d("DM","Null object response for getMobileDeviceAlertForAlertId");
                }
            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getMobileDeviceAlertForAlertId");
    }


    public void createMobileDeviceAlertForDeviceId(String deviceId, MobileDeviceAlert.MobileDeviceAlertType alertType, String timestamp, final OnCompleteListeners.oncreateMobileDeviceAlertForDeviceIdListener listener ) {

        Log.v("DM", "Enter Function: createMobileDeviceAlertForDeviceId");
        final OnCompleteListeners.oncreateMobileDeviceAlertForDeviceIdListener createMobileDeviceAlert = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Device/MobileAlert");

        if(isValidParameterString(deviceId)){
            urlString.append("?deviceId=");
            urlString.append(deviceId);
        }else{
            Log.e("DM", "createMobileDeviceAlertForDeviceId: deviceId parameter is invalid/missing,throwing exception");
        }

        if(isValidParameterString(deviceId)){
            urlString.append("&alertType=");
            urlString.append(String.format("%d", Integer.parseInt(alertType.toString())));
        }
        urlString.append("&timestamp=");
        urlString.append(timestamp);



        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "createMobileDeviceAlertForDeviceId", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for createMobileDeviceAlertForDeviceId: " + object.toString());
                    MobileDeviceAlert results = new MobileDeviceAlert();
                    results.readFromJSONObject(object);
                    listener.createMobileDeviceAlertForDeviceId(results);
                } else {
                    Log.d("DM","Null object response for createMobileDeviceAlertForDeviceId");
                }
            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: createMobileDeviceAlertForDeviceId");
    }

    public void deleteMobileDeviceAlertWithAlertId(String alertId,final OnCompleteListeners.ondeleteMobileDeviceAlertWithAlertIdListener listener ) {

        Log.v("DM", "Enter Function: deleteMobileDeviceAlertWithAlertId");
        final OnCompleteListeners.ondeleteMobileDeviceAlertWithAlertIdListener deleteMobileDeviceAlert = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Device/MobileAlert");

        if(isValidParameterString(alertId)){
            urlString.append("?alertId=");
            urlString.append(alertId);
        }else{
            Log.e("DM", "deleteMobileDeviceAlertWithAlertId: alertId parameter is invalid/missing,throwing exception");
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "deleteMobileDeviceAlertWithAlertId", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for deleteMobileDeviceAlertWithAlertId: " + object.toString());
                    listener.deleteMobileDeviceAlertWithAlertId();
                } else {
                    Log.d("DM","Null object response for deleteMobileDeviceAlertWithAlertId");
                }
            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: deleteMobileDeviceAlertWithAlertId");
    }

    public void deleteMobileDeviceAlertsForDeviceId(String deviceId, MobileDeviceAlert.MobileDeviceAlertType alertType, final OnCompleteListeners.ondeleteMobileDeviceAlertsWithAlertIdListener listener ) {

        Log.v("DM", "Enter Function: deleteMobileDeviceAlertWithAlertId");
        final OnCompleteListeners.ondeleteMobileDeviceAlertsWithAlertIdListener deleteMobileDeviceAlerts = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Device/MobileAlert");

        if(isValidParameterString(deviceId)){
            urlString.append("?deviceId=");
            urlString.append(deviceId);
        }else{
            Log.e("DM", "deleteMobileDeviceAlertWithAlertId: deviceId parameter is invalid/missing,throwing exception");
        }

        if( alertType !=  MobileDeviceAlert_Undefined){
            urlString.append("&alertType=");
            urlString.append(String.format("%ld", Long.parseLong(alertType.toString())));
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "deleteMobileDeviceAlertWithAlertId", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for deleteMobileDeviceAlertWithAlertId: " + object.toString());
                    listener.deleteMobileDeviceAlertsForDeviceId();
                } else {
                    Log.d("DM","Null object response for deleteMobileDeviceAlertWithAlertId");
                }
            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: deleteMobileDeviceAlertWithAlertId");
    }

    public void getOdometerReadingForCustomerId(String customerId,String deviceId, Date stopTime, final OnCompleteListeners.getOdometerReadingForCustomerIdListener listener ) {

        Log.v("DM", "Enter Function: getOdometerReadingForCustomerId");
        final OnCompleteListeners.getOdometerReadingForCustomerIdListener getOdometerReading = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Vehicle/Odometer");

        if(isValidParameterString(customerId)){
            urlString.append("?customerId=");
            urlString.append(customerId);
        }
        if(isValidParameterString(deviceId)){
            urlString.append("&deviceId=");
            urlString.append(deviceId);
        }else{
            Log.e("DM", "getOdometerReadingForCustomerId: deviceId parameter is invalid/missing,throwing exception");
        }

        if(stopTime != null){
            urlString.append("&stopTime=");
            urlString.append(getUTCFormatDate(stopTime));
        }else{
            Log.e("DM", "getOdometerReadingForCustomerId: StopTime is not set");

        }
        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue,  new OnCompleteListeners.OnRestConnectionCompleteListener(){
        @Override
        public void onRestConnectionComplete(Object object, Error error) {
            try {
                if (object != null) {
                    new JSONObject(String.valueOf(object));
                    Log.v("DM", "Received JSON Object for getOdometerReadingForCustomerId: " + object.toString());
                    TractorPOI results = new TractorPOI();
                    JSONObject jsonObject = (JSONObject) object;
                    results.readFromJSONObject(jsonObject);
                    listener.getOdometerReadingForCustomerId(results, null);
                } else {
                    Log.e("DM", "Null object ");
                    listener.getOdometerReadingForCustomerId(null, error);
                }
            } catch (JSONException e) {
                e.printStackTrace();
                Log.e("DM", "JSONException for getOdometerReadingForCustomerId" + e.getMessage());
                listener.getOdometerReadingForCustomerId(null, error);

            }
        }
    });

        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getOdometerReadingForCustomerId");
    }


    /// Reminders


    public void createReminderForSiteId(String siteId, PimmReminder.RouteTypeEnum routeType, Integer value, PimmReminder.ReminderConditionEnum condition,
                                      Date startDate, Date endDate, String customMessage,  final OnCompleteListeners.onCreateReminderForSiteIdListener listener ) {

        Log.v("DM", "Enter Function: createReminderForSiteId");
        final OnCompleteListeners.onCreateReminderForSiteIdListener createReminder = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Pimm/Reminder");

        if(isValidParameterString(siteId)){
            urlString.append("?siteId=");
            urlString.append(siteId);
        }
        if(routeType != null){
            urlString.append("&routeType=");
            urlString.append(String.format("%d", Integer.parseInt(routeType.toString())));
        }

            urlString.append("&value=");
            urlString.append(String.format("%d", value));

        if(condition != null){
            urlString.append("&condition=");
            urlString.append(String.format("%d", Integer.parseInt(condition.toString())));
        }

        if(startDate != null){
            urlString.append("&startDate=");
            urlString.append(getUTCFormatDate(startDate));
        }else{
            Log.e("DM", "createReminderForSiteId: startDate is not set");

        }
        if(endDate != null){
            urlString.append("&endDate=");
            urlString.append(getUTCFormatDate(endDate));
        }else{
            Log.e("DM", "createReminderForSiteId: endDate is not set");

        }
        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "createReminderForSiteId", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for createReminderForSiteId: " + object.toString());
                    PimmReminder results = new PimmReminder();
                    results.readFromJSONObject(object);
                    listener.createReminderForSiteId(results);
                } else {
                    Log.d("DM","Null object response for createReminderForSiteId");
                }
            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: createReminderForSiteId");
    }

    public void getReminderWithReminderId(String reminderId, final OnCompleteListeners.getReminderWithReminderIdListener listener ) {

        Log.v("DM", "Enter Function: getReminderWithReminderId");
        final OnCompleteListeners.getReminderWithReminderIdListener getReminderWithReminderId = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Pimm/Reminder");

        if(isValidParameterString(reminderId)){
            urlString.append("?reminderId=");
            urlString.append(reminderId);
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "getReminderWithReminderId", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for getReminderWithReminderId: " + object.toString());
                    PimmReminder results = new PimmReminder();
                    results.readFromJSONObject(object);
                    listener.getReminderWithReminderId(results);
                } else {
                    Log.d("DM","Null object response for getReminderWithReminderId");
                }
            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getReminderWithReminderId");
    }

    public void deleteReminderWithReminderId(String reminderId, final OnCompleteListeners.onDeleteReminderWithReminderId listener ) {

        Log.v("DM", "Enter Function: deleteReminderWithReminderId");
        final OnCompleteListeners.onDeleteReminderWithReminderId deleteReminder = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Pimm/Reminder");

        if(isValidParameterString(reminderId)){
            urlString.append("?reminderId=");
            urlString.append(reminderId);
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "deleteReminder", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for deleteReminderWithReminderId: " + object.toString());
                    listener.deleteReminderWithReminderId();
                } else {
                    Log.d("DM","Null object response for deleteReminderWithReminderId");
                }
            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: deleteReminderWithReminderId");
    }


    //Qualification Summaries

    // create Driver API

    public void getMobileDeviceBySerial(String serialNumber, String deviceType, final OnCompleteListeners.getMobileDeviceBySerialListener listener ) {

        Log.v("DM", "Enter Function: getMobileDeviceBySerial");
        final OnCompleteListeners.getMobileDeviceBySerialListener getMobileDeviceBySerial = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Device/Mobile/GetBySerial");

        if(isValidParameterString(serialNumber)){
            urlString.append("?serialNumber=");
            urlString.append(serialNumber);
        }
        if(isValidParameterString(deviceType)){
            urlString.append("&deviceType=");
            urlString.append(deviceType);
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "getMobileDeviceBySerial", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for getMobileDeviceBySerial: " + object.toString());
                    Device results = new Device();
                    results.readFromJSONObject(object);
                    listener.getMobileDeviceBySerial(results);
                } else {
                    Log.d("DM","Null object response for getMobileDeviceBySerial");
                }
            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getMobileDeviceBySerial");
    }

    public void createEmbeddedDeviceForSiteId(String siteId, String deviceId, String serialNumber, String deviceType, String deviceName, String groupName, final OnCompleteListeners.oncreateEmbeddedDeviceForSiteIdListener listener ) {

        Log.v("DM", "Enter Function: createEmbeddedDeviceForSiteId");
        final OnCompleteListeners.oncreateEmbeddedDeviceForSiteIdListener createEmbeddedDevice = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Device/Embedded/Creater");

        if(isValidParameterString(siteId)){
            urlString.append("?siteId=");
            urlString.append(siteId);
        }
        if(isValidParameterString(deviceId)){
            urlString.append("&deviceId=");
            urlString.append(deviceId);
        }
        if(isValidParameterString(serialNumber)){
            urlString.append("&serialNumber=");
            urlString.append(serialNumber);
        }
        if(isValidParameterString(deviceType)){
            urlString.append("&deviceType=");
            urlString.append(deviceType);
        }
        if(isValidParameterString(deviceName)){
            urlString.append("&deviceName=");
            urlString.append(deviceName);
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "createEmbeddedDeviceForSiteId", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for createEmbeddedDeviceForSiteId: " + object.toString());
                    Device results = new Device();
                    results.readFromJSONObject(object);
                    listener.createEmbeddedDeviceForSiteId(results);
                } else {
                    Log.d("DM","Null object response for createEmbeddedDeviceForSiteId");
                }
            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: createEmbeddedDeviceForSiteId");
    }

    public void getEmbeddedDeviceWithSerialNumber( String serialNumber, String deviceType, final OnCompleteListeners.getEmbeddedDeviceWithSerialNumberListener listener ) {

        Log.v("DM", "Enter Function: getEmbeddedDeviceWithSerialNumber");
        final OnCompleteListeners.getEmbeddedDeviceWithSerialNumberListener getEmbeddedDeviceWithSerialNumber = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Device/Embedded/GetBySerial");


        if(isValidParameterString(serialNumber)){
            urlString.append("?serialNumber=");
            urlString.append(serialNumber);
        }
        if(isValidParameterString(deviceType)){
            urlString.append("&deviceType=");
            urlString.append(deviceType);
        }
        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "getEmbeddedDeviceWithSerialNumber", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for getEmbeddedDeviceWithSerialNumber: " + object.toString());
                    Device results = new Device();
                    results.readFromJSONObject(object);
                    listener.getEmbeddedDeviceWithSerialNumber(results);
                } else {
                    Log.d("DM","Null object response for getEmbeddedDeviceWithSerialNumber");
                }
            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getEmbeddedDeviceWithSerialNumber");
    }

   public void getEmbeddedDeviceWithDeviceId( String deviceId, final OnCompleteListeners.getEmbeddedDeviceWithDeviceIdListener listener ) {

        Log.v("DM", "Enter Function: getEmbeddedDeviceWithDeviceId");
        final OnCompleteListeners.getEmbeddedDeviceWithDeviceIdListener getEmbeddedDeviceWithDeviceId = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Device/Embedded");


        if(isValidParameterString(deviceId)){
            urlString.append("?deviceId=");
            urlString.append(deviceId);
        }
        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "getEmbeddedDeviceWithDeviceId", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for getEmbeddedDeviceWithDeviceId: " + object.toString());
                    Device results = new Device();
                    results.readFromJSONObject(object);
                    listener.getEmbeddedDeviceWithDeviceId(results);
                } else {
                    Log.d("DM","Null object response for getEmbeddedDeviceWithDeviceId");
                }
            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getEmbeddedDeviceWithDeviceId");
    }

   public void deleteEmbeddedDeviceWithDeviceId( String deviceId, final OnCompleteListeners.deleteEmbeddedDeviceWithDeviceIdListener listener ) {

        Log.v("DM", "Enter Function: deleteEmbeddedDeviceWithDeviceId");
        final OnCompleteListeners.deleteEmbeddedDeviceWithDeviceIdListener deleteEmbeddedDeviceWithDeviceId = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Device/Embedded/Delete");


        if(isValidParameterString(deviceId)){
            urlString.append("?deviceId=");
            urlString.append(deviceId);
        }
        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "deleteEmbeddedDeviceWithDeviceId", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for deleteEmbeddedDeviceWithDeviceId: " + object.toString());
                    listener.deleteEmbeddedDeviceWithDeviceId();
                } else {
                    Log.d("DM","Null object response for deleteEmbeddedDeviceWithDeviceId");
                }
            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: deleteEmbeddedDeviceWithDeviceId");
    }


   public void saveSiteInfoForSiteId( String siteId, String siteName, SiteContact siteContact, AddressComponents address, Integer timezone,
                                      boolean dst,LatLon latLon, Number geofence, final OnCompleteListeners.saveSiteInfoForSiteIdListener listener ) {

        Log.v("DM", "Enter Function: saveSiteInfoForSiteId");
        final OnCompleteListeners.saveSiteInfoForSiteIdListener saveSite = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Pimm/Site/SaveSiteInfo");


        if(isValidParameterString(siteId)){
            urlString.append("?siteId=");
            urlString.append(siteId);
        }else{
            Log.e("DM","saveSiteInfoForSiteId: SiteId Parameter is invalid");
        }

        if(isValidParameterString(siteName)){
            urlString.append("&siteName=");
            urlString.append(siteName);
        }else{
            Log.e("DM","saveSiteInfoForSiteId: siteName Parameter is invalid");
        }
        if(address != null) {
            if (isValidParameterString(address.Address1)) {
                urlString.append("&address1=");
                urlString.append(address.Address1);
            }
            if (isValidParameterString(address.Address2)) {
                urlString.append("&address2=");
                urlString.append(address.Address2);
            }
            if (isValidParameterString(address.City)) {
                urlString.append("&city=");
                urlString.append(address.City);
            }
            if (isValidParameterString(address.City)) {
                urlString.append("&city=");
                urlString.append(address.City);
            }
            if (isValidParameterString(address.State)) {
                urlString.append("&state=");
                urlString.append(address.State);
            }
            if (isValidParameterString(address.Zip)) {
                urlString.append("&zip=");
                urlString.append(address.Zip);
            }
        }

        if(siteContact != null){
            if (isValidParameterString(siteContact.Name)){
                urlString.append("&contactName=");
                urlString.append(siteContact.Name);
            }
            if (isValidParameterString(siteContact.Email)){
                urlString.append("&contactEmail=");
                urlString.append(siteContact.Email);
            }
            if (isValidParameterString(siteContact.PhoneNumber)){
                urlString.append("&contactPhone=");
                urlString.append(siteContact.PhoneNumber);
            }
        }
       urlString.append("&timezone=");
       urlString.append(String.format("%d",timezone));


       urlString.append("&dst=");

       if(dst) {
           urlString.append("true");
       }else{
           urlString.append("false");
       }

       if(latLon != null) {
           urlString.append("&lat=");
           urlString.append(String.format("%f", latLon.Lat));
       }else{
           urlString.append("&lon=");
           urlString.append(String.format("%f", latLon.Lon));
       }

       if(geofence !=null){
           urlString.append("&geofence=");
           urlString.append(String.format("%f", geofence.doubleValue()));
       }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "saveSite", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for saveSiteInfoForSiteId: " + object.toString());
                    Site results = new Site();
                    results.readFromJSONObject(object);
                    listener.saveSiteInfoForSiteId(results);
                } else {
                    Log.d("DM","Null object response for saveSiteInfoForSiteId");
                }
            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: saveSiteInfoForSiteId");
    }

    public void getReeferAlarmWithId( String reeferAlarmId, final OnCompleteListeners.getReeferAlarmWithIdListener listener ) {

        Log.v("DM", "Enter Function: deleteEmbeddedDeviceWithDeviceId");
        final OnCompleteListeners.getReeferAlarmWithIdListener getReeferAlarmWithId = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Vehicle/Reefer/GetReeferAlarm");


        if(isValidParameterString(reeferAlarmId)){
            urlString.append("?reeferAlarmId=");
            urlString.append(reeferAlarmId);
        }
        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "getReeferAlarmWithId", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for getReeferAlarmWithId: " + object.toString());
                    ReeferAlarm results = new ReeferAlarm();
                    results.readFromJSONObject(object);
                    listener.getReeferAlarmWithId(results);
                } else {
                    Log.d("DM","Null object response for getReeferAlarmWithId");
                }
            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getReeferAlarmWithId");
    }

    public void getFormDefinitionListForAppId(String appId, final OnCompleteListeners.getFormDefinitionListForAppIdListener listener ) {

        Log.v("DM", "Enter Function: deleteEmbeddedDeviceWithDeviceId");
       //  final OnCompleteListeners.getFormDefinitionListForAppIdListener getFormDefinition = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Pimm/Forms");

        if(isValidParameterString(appId)){
            urlString.append("?appId=");
            urlString.append(appId);
        }
        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();

        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                //Log.v("DM", "inside pimmFormList init >>" + object +" --- " + error);
                if(error == null)
                {
                    try
                    {
                        Log.v("DM", "inside pimmFormList try >>");
                        JSONArray jsonArray = (JSONArray)object;
                        ArrayList<PimmForm> pimmFormList = new ArrayList<PimmForm>();

                        for(int i = 0; i < jsonArray.length(); i ++)
                        {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                           // Log.v("DM", "pimmFormList >>" + jsonArray.get(0));
                            PimmForm pimmForm = new PimmForm();
                            pimmForm.readFromJSONObject(jsonObject);
                            pimmFormList.add(pimmForm);
                            //Log.v("DM", "pimmForm >>" + pimmForm.formDefinitionID);
                        }

                        listener.getFormDefinitionListForAppId(pimmFormList, null);
                    }
                    catch (JSONException e)
                    {
                       // Log.e("DM", "pimmFormList error >>" + e.getMessage());
                        listener.getFormDefinitionListForAppId(null, new Error(e.getMessage()));
                    }

                }
            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getReeferAlarmWithId");
    }

    public void clearAllReeferAlarmsForDeviceId( String deviceId, final OnCompleteListeners.clearAllReeferAlarmsForDeviceIdListener listener ) {

        Log.v("DM", "Enter Function: clearAllReeferAlarmsForDeviceId");
        final OnCompleteListeners.clearAllReeferAlarmsForDeviceIdListener clearAllReeferAlarms = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Vehicle/Reefer/ReeferCommand_ClearAllAlarms");


        if(isValidParameterString(deviceId)){
            urlString.append("?deviceId=");
            urlString.append(deviceId);
        }else{
            Log.e("DM","clearAllReeferAlarmsForDeviceId: deviceId Parameter is invalid");
        }
        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "clearAllReeferAlarmsForDeviceId", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for getReeferAlarmWithId: " + object.toString());
                    listener.clearAllReeferAlarmsForDeviceId();
                } else {
                    Log.d("DM","Null object response for clearAllReeferAlarmsForDeviceId");
                }
            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: clearAllReeferAlarmsForDeviceId");
    }


    public void getReeferCommandWithId(String reeferCommandId, final OnCompleteListeners.getReeferCommandWithIdListener listener ) {

        Log.v("DM", "Enter Function: getReeferCommandWithId");
        final OnCompleteListeners.getReeferCommandWithIdListener getReeferCommand = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Vehicle/Reefer/GetReeferCommand");


        if(isValidParameterString(reeferCommandId)){
            urlString.append("?reeferCommandId=");
            urlString.append(reeferCommandId);
        }else{
            Log.e("DM","clearAllReeferAlarmsForDeviceId: reeferCommandId is not set");
        }
        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "getReeferCommandWithId", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object == null) {
                    Log.v("DM", "Received JSON Object for getReeferCommandWithId: " + object.toString());
                    ReeferCommand results = new ReeferCommand();
                    results.readFromJSONObject(object);
                    listener.getReeferCommandWithId(results);
                } else {
                    Log.d("DM","Null object response for getReeferCommandWithId");
                }
            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getReeferCommandWithId");
    }
    public void cancelReeferCommandWithId(String reeferCommandId,String deviceId, Date cancelTime, final OnCompleteListeners.oncancelReeferCommandWithIdListener listener ) {

        Log.v("DM", "Enter Function: getReeferCommandWithId");
        final OnCompleteListeners.oncancelReeferCommandWithIdListener cancelReeferCommand = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Vehicle/Reefer/ReeferCommand_Cancel");


        if(isValidParameterString(reeferCommandId)){
            urlString.append("?reeferCommandId=");
            urlString.append(reeferCommandId);
        }else{
            Log.e("DM","cancelReeferCommandWithId: reeferCommandId is not set");
        }
        if(isValidParameterString(deviceId)){
            urlString.append("&deviceId=");
            urlString.append(deviceId);
        }else{
            Log.e("DM","cancelReeferCommandWithId: deviceId is not set");
        }
        if(cancelTime != null){
            urlString.append("&cancelTime=");
            urlString.append(getUTCFormatDate(cancelTime));
        }else{
            Log.e("DM","cancelReeferCommandWithId: cancelTime is not valid");
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "cancelReeferCommandWithId", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for cancelReeferCommandWithId: " + object.toString());
                    listener.cancelReeferCommandWithId();
                } else {
                    Log.d("DM","Null object response for cancelReeferCommandWithId");
                }
            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getReeferCommandWithId");
    }

    public void getSiteSettingsForSiteId(String siteId, final OnCompleteListeners.getSiteSettingsForSiteIdListener listener) {

        Log.v("DM", "Enter Function: getSiteSettingsForSiteId");
        final OnCompleteListeners.getSiteSettingsForSiteIdListener getSiteSettings = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/Settings");


        if(isValidParameterString(siteId)){
            urlString.append("?siteId=");
            urlString.append(siteId);
        }else{
            Log.e("DM","getSiteSettingsForSiteId: siteId is not set");
        }
        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener(){
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for getSiteSettingsForSiteId: " + object.toString());
                        SiteSettings results = new SiteSettings();
                        JSONObject jsonObject = (JSONObject)object;
                        results.readFromJSONObject(jsonObject);
                        listener.getSiteSettingsForSiteId(results,null);
                    }else{
                        Log.e("DM", "Null object " );
                        listener.getSiteSettingsForSiteId(null, error);
                    }
                }catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for getSiteSettingsForSiteId: " + e.getMessage());
                    listener.getSiteSettingsForSiteId(null, error);

                }
            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getSiteSettingsForSiteId");
    }

    public void changeReeferSetPointForDeviceId(String deviceId, Integer compartment, double setPoInteger, final OnCompleteListeners.onchangeReeferSetPointForDeviceIdListener listener ) {

        Log.v("DM", "Enter Function: changeReeferSetPointForDeviceId");
        final OnCompleteListeners.onchangeReeferSetPointForDeviceIdListener changeReeferSetPoInteger = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Vehicle/Reefer/ReeferCommand_ChangeSetPoInteger");


        if(isValidParameterString(deviceId)){
            urlString.append("?deviceId=");
            urlString.append(deviceId);
        }else{
            Log.e("DM","changeReeferSetPointForDeviceId: deviceId is not set");
        }

        urlString.append("&compartment=");
        urlString.append(String.format("%d", compartment));
        urlString.append("&setPoInteger=");
        urlString.append(String.format("@", setPoInteger));
        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "changeReeferSetPoInteger", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for changeReeferSetPointForDeviceId: " + object.toString());
                    listener.changeReeferSetPointForDeviceId();
                } else {
                    Log.d("DM","Null object response for changeReeferSetPointForDeviceId");
                }
            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: changeReeferSetPointForDeviceId");
    }

    public void changeReeferModeToContinuousForDeviceId(String deviceId,  final OnCompleteListeners.onchangeReeferModeToContinuousForDeviceIdListener listener ) {

        Log.v("DM", "Enter Function: changeReeferModeToContinuousForDeviceId");
        final OnCompleteListeners.onchangeReeferModeToContinuousForDeviceIdListener changeReeferModeToContinuous = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Vehicle/Reefer/ReeferCommand_ChangeModeContinuous");


        if(isValidParameterString(deviceId)){
            urlString.append("?deviceId=");
            urlString.append(deviceId);
        }else{
            Log.e("DM","changeReeferModeToContinuousForDeviceId: deviceId is not set");
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "changeReeferModeToContinuous", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for changeReeferModeToContinuousForDeviceId: " + object.toString());
                    listener.changeReeferModeToContinuousForDeviceId();
                } else {
                    Log.d("DM","Null object response for changeReeferModeToContinuousForDeviceId");
                }
            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: changeReeferModeToContinuousForDeviceId");
    }

    public void changeReeferModeToCycleForDeviceId(String deviceId,  final OnCompleteListeners.onchangeReeferModeToCycleForDeviceIdListener listener ) {

        Log.v("DM", "Enter Function: changeReeferModeToCycleForDeviceId");
        final OnCompleteListeners.onchangeReeferModeToCycleForDeviceIdListener changeReeferModeToCycle = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Vehicle/Reefer/ReeferCommand_ChangeModeCycle");


        if(isValidParameterString(deviceId)){
            urlString.append("?deviceId=");
            urlString.append(deviceId);
        }else{
            Log.e("DM","changeReeferModeToCycleForDeviceId: deviceId is not set");
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "changeReeferModeToCycle", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for changeReeferModeToCycleForDeviceId: " + object.toString());
                    listener.changeReeferModeToCycleForDeviceId();
                } else {
                    Log.d("DM","Null object response for changeReeferModeToCycleForDeviceId");
                }
            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: changeReeferModeToCycleForDeviceId");
    }

    public void changeReeferModeToSleepForDeviceId(String deviceId,  final OnCompleteListeners.onchangeReeferModeToSleepForDeviceIdListener listener ) {

        Log.v("DM", "Enter Function: changeReeferModeToCycleForDeviceId");
        final OnCompleteListeners.onchangeReeferModeToSleepForDeviceIdListener changeReeferModeToSleep = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Vehicle/Reefer/ReeferCommand_ChangeModeSleep");


        if(isValidParameterString(deviceId)){
            urlString.append("?deviceId=");
            urlString.append(deviceId);
        }else{
            Log.e("DM","changeReeferModeToCycleForDeviceId: deviceId is not set");
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "changeReeferModeToSleep", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for changeReeferModeToCycleForDeviceId: " + object.toString());
                    listener.changeReeferModeToSleepForDeviceId();
                } else {
                    Log.d("DM","Null object response for changeReeferModeToCycleForDeviceId");
                }
            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: changeReeferModeToCycleForDeviceId");
    }

    public void changeReeferPowerToOnForDeviceId(String deviceId, Integer compartment,  final OnCompleteListeners.onchangeReeferPowerToOnForDeviceIdListener listener ) {

        Log.v("DM", "Enter Function: changeReeferPowerToOnForDeviceId");
        final OnCompleteListeners.onchangeReeferPowerToOnForDeviceIdListener  changeReeferPowerToOn= listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Vehicle/Reefer/ReeferCommand_ChangePowerOn");


        if(isValidParameterString(deviceId)){
            urlString.append("?deviceId=");
            urlString.append(deviceId);
        }else{
            Log.e("DM","changeReeferPowerToOnForDeviceId: deviceId is not set");
        }

        urlString.append("&compartment=");
        urlString.append(String.format("%d", compartment));

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "changeReeferModeToSleep", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for changeReeferPowerToOnForDeviceId: " + object.toString());
                    listener.changeReeferPowerToOnForDeviceId();
                } else {
                    Log.d("DM","Null object response for changeReeferPowerToOnForDeviceId");
                }
            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: changeReeferPowerToOnForDeviceId");
    }

   public void defrostReeferWithDeviceId(String deviceId,  final OnCompleteListeners.ondefrostReeferWithDeviceIdListener listener ) {

        Log.v("DM", "Enter Function: defrostReeferWithDeviceId");
        final OnCompleteListeners.ondefrostReeferWithDeviceIdListener  defrostReefer= listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Vehicle/Reefer/ReeferCommand_Defrost");


        if(isValidParameterString(deviceId)){
            urlString.append("?deviceId=");
            urlString.append(deviceId);
        }else{
            Log.e("DM","defrostReeferWithDeviceId: deviceId is not set");
        }



        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "defrostReefer", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for defrostReeferWithDeviceId: " + object.toString());
                    listener.defrostReeferWithDeviceId();
                } else {
                    Log.d("DM","Null object response for defrostReeferWithDeviceId");
                }
            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: defrostReeferWithDeviceId");
    }

   public void defrostReeferWithDeviceId(String deviceId, Integer compartment,  final OnCompleteListeners.ondefrostReeferWithDeviceIdListener listener ) {

        Log.v("DM", "Enter Function: defrostReeferWithDeviceId");
        final OnCompleteListeners.ondefrostReeferWithDeviceIdListener  defrostReefer= listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Vehicle/Reefer/ReeferCommand_Defrost");


        if(isValidParameterString(deviceId)){
            urlString.append("?deviceId=");
            urlString.append(deviceId);
        }else{
            Log.e("DM","defrostReeferWithDeviceId: deviceId is not set");
        }
       urlString.append("&compartment=");
       urlString.append(String.format("%d", compartment));

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "defrostReefer", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for defrostReeferWithDeviceId: " + object.toString());
                    listener.defrostReeferWithDeviceId();
                } else {
                    Log.d("DM","Null object response for defrostReeferWithDeviceId");
                }
            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: defrostReeferWithDeviceId");
    }

   public void runPreTripOnDeviceId(String deviceId,  final OnCompleteListeners.runPreTripOnDeviceIdListener listener ) {

        Log.v("DM", "Enter Function: runPreTripOnDeviceId");
        final OnCompleteListeners.runPreTripOnDeviceIdListener  runPreTripOnDeviceId= listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Vehicle/Reefer/ReeferCommand_PreTrip");

        if(isValidParameterString(deviceId)){
            urlString.append("?deviceId=");
            urlString.append(deviceId);
        }else{
            Log.e("DM","runPreTripOnDeviceId: deviceId is not set");
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "runPreTripOnDeviceId", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for runPreTripOnDeviceId: " + object.toString());
                    listener.runPreTripOnDeviceId();
                } else {
                    Log.d("DM","Null object response for runPreTripOnDeviceId");
                }
            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: runPreTripOnDeviceId");
    }


   public void getDocumentMetaDataForDocument(String documentId,  final OnCompleteListeners.getDocumentMetaDataForDocumentListener listener) {

        Log.v("DM", "Enter Function: getDocumentMetaDataForDocument");
        final OnCompleteListeners.getDocumentMetaDataForDocumentListener  getDocumentMetaData= listener;

       StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Document/Document/Get");


        if(isValidParameterString(documentId)){
            urlString.append("?documentId=");
            urlString.append(documentId);
        }else{
            Log.e("DM","getDocumentMetaDataForDocument: documentId is not set");
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue,  new OnCompleteListeners.OnRestConnectionCompleteListener(){
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for getDocumentMetaDataForDocument: " + object.toString());
                        DocumentDTO results = new DocumentDTO();
                        JSONObject jsonObject = (JSONObject)object;
                        results.readFromJSONObject(jsonObject);
                        listener.getDocumentMetaDataForDocument(results, null);
                    }else{
                        Log.e("DM", "Null object " );
                        listener.getDocumentMetaDataForDocument(null, error);
                    }
                }catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException getDocumentMetaDataForDocument >> " + e.getMessage());
                    listener.getDocumentMetaDataForDocument(null, error);

                }
            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getDocumentMetaDataForDocument");
    }

   public void addDocumentWithDocumentId(String documentId, String documentType,String documentName, String referenceId,  PimmRefTypeEnum.PimmRefTypesEnum refType, String filePath,   final OnCompleteListeners.onaddDocumentWithDocumentIdListener listener ) {
//public void addDocumentWithDocumentId(String documentId, String documentType,String documentName, String referenceId, PimmRefTypeEnum.PimmRefTypesEnum refType, String filePath,   final OnCompleteListeners.onaddDocumentWithDocumentIdListener listener ) {

        Log.v("DM", "Enter Function: addDocumentWithDocumentId");
        final OnCompleteListeners.onaddDocumentWithDocumentIdListener  addDocumentWithDocumentId= listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Document/Document/Add");


        if(isValidParameterString(documentId)){
            urlString.append("?documentId=");
            urlString.append(documentId);
        }else{
            Log.e("DM","addDocumentWithDocumentId: documentId is not set");
        }

        if(referenceId != null && refType != PimmRefType_Undefined){
            urlString.append("&refx=");
            urlString.append(referenceId);
            urlString.append("&refType=");
            urlString.append(String.format("%ld", Long.valueOf(refType.getValue())));

        }
        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue,  new OnCompleteListeners.OnRestConnectionCompleteListener(){
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {

                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for addDocumentWithDocumentId: " + object.toString());
                        DocumentDTO results = new DocumentDTO();
                        JSONObject jsonObject = (JSONObject) object;
                        results.readFromJSONObject(jsonObject);
                        listener.addDocumentWithDocumentId(results, null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.addDocumentWithDocumentId(null, error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for addDocumentWithDocumentId" + e.getMessage());
                    listener.addDocumentWithDocumentId(null, error);

                }
            }
        });
//       NSDictionary* filePathDic = [[NSDictionary alloc] initWithObjectsAndKeys:filePath,documentName, nil];
//       NSDictionary* attachmentTypeDic = [[NSDictionary alloc] initWithObjectsAndKeys:documentType,documentName, nil];


       restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: addDocumentWithDocumentId");
    }

    public void addDocumentWithDocumentId(String documentId, String documentType,String documentName, String referenceId, PimmRefTypeEnum.PimmRefTypesEnum refType ,
                                         String category, String appString, String refString, String documentPath,   final OnCompleteListeners.onaddDocumentWithDocumentIdListener listener ) {

        Log.v("DM", "Enter Function: addDocumentWithDocumentId");
        final OnCompleteListeners.onaddDocumentWithDocumentIdListener  addDocumentWithDocumentId= listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Document/Document/Add");


        if(isValidParameterString(documentId)){
            urlString.append("?documentId=");
            urlString.append(documentId);
        }else{
            Log.e("DM","addDocumentWithDocumentId: documentId is not set");
        }

        if(referenceId != null && refType != PimmRefType_Undefined){
            urlString.append("&refx=");
            urlString.append(referenceId);
            urlString.append("&refType=");
            urlString.append(String.format("%ld", Long.valueOf(refType.getValue())));

        }

        if(isValidParameterString(category)){
            urlString.append("&category=");
            urlString.append(category);
        }

        if(isValidParameterString(appString)){
            urlString.append("&appString=");
            urlString.append(appString);
        }

        if(isValidParameterString(refString)){
            urlString.append("&refString=");
            urlString.append(refString);
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue,  new OnCompleteListeners.OnRestConnectionCompleteListener(){
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                         Log.v("DM", "Received JSON Object for addDocumentWithDocumentId: " + object.toString());
                         DocumentDTO results = new DocumentDTO();
                        JSONObject jsonObject = (JSONObject) object;
                        results.readFromJSONObject(jsonObject);
                        listener.addDocumentWithDocumentId(results, null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.addDocumentWithDocumentId(null, error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for addDocumentWithDocumentId" + e.getMessage());
                    listener.addDocumentWithDocumentId(null, error);

                }
            }
        });


        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: addDocumentWithDocumentId");
    }

    public void deleteDocumentWithId(String documentId, final OnCompleteListeners.ondeleteDocumentWithIdListener listener ) {

        Log.v("DM", "Enter Function: deleteDocumentWithId");
        final OnCompleteListeners.ondeleteDocumentWithIdListener  deleteDocumentWithId= listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Document/Document/Delete");


        if(isValidParameterString(documentId)){
            urlString.append("?documentId=");
            urlString.append(documentId);
        }else{
            Log.e("DM","deleteDocumentWithId: documentId is not set");
        }


        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "deleteDocumentWithId", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for deleteDocumentWithId: " + object.toString());
                    listener.deleteDocumentWithId();
                } else {
                    Log.d("DM","Null object response for deleteDocumentWithId");
                }
            }
        });


        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: deleteDocumentWithId");
    }

    // Dashboard APIs

    public void getSuD_SupplierDashboardForStartDate(Date startDate, Date endDate, String siteId, boolean currentOnly, final OnCompleteListeners.getSuD_SupplierDashboardForStartDateListener listener ) {

        Log.v("DM", "Enter Function: deleteDocumentWithId");
        final OnCompleteListeners.getSuD_SupplierDashboardForStartDateListener  getSuD_SupplierDashboardForStartDate= listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Dashboard/Dashboard/SuD_Supplier_Dashboard");

        if (startDate != null){
            urlString.append("?startDate=");
            urlString.append(getUTCFormatDate(startDate));
        }else{
            Log.e("DM","getSuD_SupplierDashboardForStartDate: startDate is null");
        }
        if (endDate != null){
            urlString.append("&endDate=");
            urlString.append(getUTCFormatDate(endDate));
        }else{
            Log.e("DM","getSuD_SupplierDashboardForStartDate: endDate is null");
        }


        if(isValidParameterString(siteId)){
            urlString.append("&siteId=");
            urlString.append(siteId);
        }else{
            Log.e("DM","getSuD_SupplierDashboardForStartDate: siteId is not set");
        }

        if(currentOnly){
            urlString.append("&currentOnly=true");
        }else{
            urlString.append("&currentOnly=false");
        }


        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "getSuD_SupplierDashboardForStartDate", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for getSuD_SupplierDashboardForStartDate: " + object.toString());
                    SuD_SupplierDashboard results = new SuD_SupplierDashboard();
                    results.readFromJSONObject(object);
                    listener.getSuD_SupplierDashboardForStartDate(results);
                } else {
                    Log.d("DM","Null object response for getSuD_SupplierDashboardForStartDate");
                }
            }
        });


        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getSuD_SupplierDashboardForStartDate");
    }


    public void getSuD_CustomerDashboardForStartDate(Date startDate, Date endDate, String siteId, boolean currentOnly, final OnCompleteListeners.getSuD_CustomerDashboardForStartDateListener listener ) {

        Log.v("DM", "Enter Function: getSuD_CustomerDashboardForStartDate");
        final OnCompleteListeners.getSuD_CustomerDashboardForStartDateListener  getSuD_CustomerDashboardForStartDate = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Dashboard/Dashboard/SuD_Customer_Dashboard");

        if (startDate != null){
            urlString.append("?startDate=");
            urlString.append(getUTCFormatDate(startDate));
        }else{
            Log.e("DM","getSuD_CustomerDashboardForStartDate: startDate is null");
        }
        if (endDate != null){
            urlString.append("&endDate=");
            urlString.append(getUTCFormatDate(endDate));
        }else{
            Log.e("DM","getSuD_CustomerDashboardForStartDate: endDate is null");
        }


        if(isValidParameterString(siteId)){
            urlString.append("&siteId=");
            urlString.append(siteId);
        }else{
            Log.e("DM","getSuD_CustomerDashboardForStartDate: siteId is not set");
        }

        if(currentOnly){
            urlString.append("&currentOnly=true");
        }else{
            urlString.append("&currentOnly=false");
        }


        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "getSuD_CustomerDashboardForStartDate", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for getSuD_CustomerDashboardForStartDate: " + object.toString());
                    SuD_CustomerDashboard results = new SuD_CustomerDashboard();
                    results.readFromJSONObject(object);
                    listener.getSuD_CustomerDashboardForStartDate(results);
                } else {
                    Log.d("DM","Null object response for getSuD_CustomerDashboardForStartDate");
                }
            }
        });


        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getSuD_CustomerDashboardForStartDate");
    }

    public void getSuD_DistributorDashboardForStartDate(Date startDate, Date endDate, String siteId, boolean currentOnly, final OnCompleteListeners.getSuD_DistributorDashboardForStartDateListener listener ) {

        Log.v("DM", "Enter Function: getSuD_DistributorDashboardForStartDate");
        final OnCompleteListeners.getSuD_DistributorDashboardForStartDateListener  getSuD_DistributorDashboardForStartDate = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Dashboard/Dashboard/SuD_Distributor_Dashboard");

        if (startDate != null){
            urlString.append("?startDate=");
            urlString.append(getUTCFormatDate(startDate));
        }else{
            Log.e("DM","getSuD_DistributorDashboardForStartDate: startDate is null");
        }
        if (endDate != null){
            urlString.append("&endDate=");
            urlString.append(getUTCFormatDate(endDate));
        }else{
            Log.e("DM","getSuD_DistributorDashboardForStartDate: endDate is null");
        }


        if(isValidParameterString(siteId)){
            urlString.append("&siteId=");
            urlString.append(siteId);
        }else{
            Log.e("DM","getSuD_DistributorDashboardForStartDate: siteId is not set");
        }

        if(currentOnly){
            urlString.append("&currentOnly=true");
        }else{
            urlString.append("&currentOnly=false");
        }


        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "getSuD_DistributorDashboardForStartDate", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for getSuD_DistributorDashboardForStartDate: " + object.toString());
                    SuD_DistributorDashboard results = new SuD_DistributorDashboard();
                    results.readFromJSONObject(object);
                    listener.getSuD_DistributorDashboardForStartDate(results);
                } else {
                    Log.d("DM","Null object response for getSuD_DistributorDashboardForStartDate");
                }
            }
        });


        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getSuD_DistributorDashboardForStartDate");
    }

    public void getStD_DistributorDashboardForStartDate(Date startDate, Date endDate, String siteId, boolean currentOnly, final OnCompleteListeners.getStD_DistributorDashboardForStartDateListener listener ) {

        Log.v("DM", "Enter Function: getStD_DistributorDashboardForStartDate");
        final OnCompleteListeners.getStD_DistributorDashboardForStartDateListener  getSuD_DistributorDashboardForStartDate = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Dashboard/Dashboard/StD_Distributor_Dashboard");

        if (startDate != null){
            urlString.append("?startDate=");
            urlString.append(getUTCFormatDate(startDate));
        }else{
            Log.e("DM","getStD_DistributorDashboardForStartDate: startDate is null");
        }
        if (endDate != null){
            urlString.append("&endDate=");
            urlString.append(getUTCFormatDate(endDate));
        }else{
            Log.e("DM","getStD_DistributorDashboardForStartDate: endDate is null");
        }


        if(isValidParameterString(siteId)){
            urlString.append("&siteId=");
            urlString.append(siteId);
        }else{
            Log.e("DM","getStD_DistributorDashboardForStartDate: siteId is not set");
        }

        if(currentOnly){
            urlString.append("&currentOnly=true");
        }else{
            urlString.append("&currentOnly=false");
        }


        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "getStD_DistributorDashboardForStartDate", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for getStD_DistributorDashboardForStartDate: " + object.toString());
                    StD_DistributorDashboardDTO results = new StD_DistributorDashboardDTO();
                    results.readFromJSONObject(object);
                    listener.getStD_DistributorDashboardForStartDate(results);
                } else {
                    Log.d("DM","Null object response for getStD_DistributorDashboardForStartDate");
                }
            }
        });


        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getStD_DistributorDashboardForStartDate");
    }

    public void getStD_CustomerDashboardForStartDate(Date startDate, Date endDate, String siteId, boolean currentOnly, final OnCompleteListeners.getStD_CustomerDashboardForStartDateListener listener ) {

        Log.v("DM", "Enter Function: getStD_CustomerDashboardForStartDate");
        final OnCompleteListeners.getStD_CustomerDashboardForStartDateListener  getStD_CustomerDashboardForStartDate = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Dashboard/Dashboard/StD_Distributor_Dashboard");

        if (startDate != null){
            urlString.append("?startDate=");
            urlString.append(getUTCFormatDate(startDate));
        }else{
            Log.e("DM","getStD_CustomerDashboardForStartDate: startDate is null");
        }
        if (endDate != null){
            urlString.append("&endDate=");
            urlString.append(getUTCFormatDate(endDate));
        }else{
            Log.e("DM","getStD_CustomerDashboardForStartDate: endDate is null");
        }


        if(isValidParameterString(siteId)){
            urlString.append("&siteId=");
            urlString.append(siteId);
        }else{
            Log.e("DM","getStD_CustomerDashboardForStartDate: siteId is not set");
        }

        if(currentOnly){
            urlString.append("&currentOnly=true");
        }else{
            urlString.append("&currentOnly=false");
        }


        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "getStD_CustomerDashboardForStartDate", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for getStD_CustomerDashboardForStartDate: " + object.toString());
                    StD_CustomerDashboardDTO results = new StD_CustomerDashboardDTO();
                    results.readFromJSONObject(object);
                    listener.getStD_CustomerDashboardForStartDate(results);
                } else {
                    Log.d("DM","Null object response for getStD_CustomerDashboardForStartDate");
                }
            }
        });


        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getStD_CustomerDashboardForStartDate");
    }

    public void getFM_FacilityDashboardForStartDate(Date startDate, Date endDate, String siteId, boolean currentOnly, final OnCompleteListeners.getFM_FacilityDashboardForStartDateListener listener ) {

        Log.v("DM", "Enter Function: getFM_FacilityDashboardForStartDate");
        final OnCompleteListeners.getFM_FacilityDashboardForStartDateListener  getFM_FacilityDashboardForStartDate = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Dashboard/Dashboard/FM_Facility_Dashboard");

        if (startDate != null){
            urlString.append("?startDate=");
            urlString.append(getUTCFormatDate(startDate));
        }else{
            Log.e("DM","getFM_FacilityDashboardForStartDate: startDate is null");
        }
        if (endDate != null){
            urlString.append("&endDate=");
            urlString.append(getUTCFormatDate(endDate));
        }else{
            Log.e("DM","getFM_FacilityDashboardForStartDate: endDate is null");
        }


        if(isValidParameterString(siteId)){
            urlString.append("&siteId=");
            urlString.append(siteId);
        }else{
            Log.e("DM","getFM_FacilityDashboardForStartDate: siteId is not set");
        }

        if(currentOnly){
            urlString.append("&currentOnly=true");
        }else{
            urlString.append("&currentOnly=false");
        }


        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "getFM_FacilityDashboardForStartDate", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for getFM_FacilityDashboardForStartDate: " + object.toString());
                    FM_FacilityDashboardDTO results = new FM_FacilityDashboardDTO();
                    results.readFromJSONObject(object);
                    listener.getFM_FacilityDashboardForStartDate(results);
                } else {
                    Log.d("DM","Null object response for getFM_FacilityDashboardForStartDate");
                }
            }
        });


        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getFM_FacilityDashboardForStartDate");
    }

// POISiteList API

    public void getPOISiteListForDCSite(String siteId, Date token, final OnCompleteListeners.getPOISiteListForDCSiteListener listener ) {

        Log.v("DM", "Enter Function: getPOISiteListForDCSite");
        final OnCompleteListeners.getPOISiteListForDCSiteListener  getPOISiteListForDCSite = listener;


        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/POISite/GetPOISites");

        if (token != null){
            urlString.append("?token=");
           // urlString.append(token);
            //Date tok = getUTCFormatDate(token);
            urlString.append(getUTCFormatDate(token));
        }

        if(isValidParameterString(siteId)){
            urlString.append("&siteId=");
            urlString.append(siteId);
        }else{
            Log.e("DM","getPOISiteListForDCSite: siteId is not set");
        }




        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener(){
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for getPOISiteListForDCSite: " + object.toString());
                        POISiteListDTO results = new POISiteListDTO();
                        JSONObject jsonObject = (JSONObject)object;
                        results.readFromJSONObject(jsonObject);
                        listener.getPOISiteListForDCSite(results, null);
                    }else{
                        Log.e("DM", "Null getPOISiteListForDCSite object " );
                        listener.getPOISiteListForDCSite(null, error);

                    }
                }catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException getPOISiteListForDCSite >> " + e.getMessage());
                    listener.getPOISiteListForDCSite(null, error);
                }
            }
        });


        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getPOISiteListForDCSite");
    }

    public void addHOSEvent(HOSEventDTO hosEvent, final OnCompleteListeners.onaddHOSEventListener listener ) {

        Log.v("DM", "Enter Function: addHOSEvent");
        final OnCompleteListeners.onaddHOSEventListener  onaddHOSEvent = listener;


        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Driver/HOS2/HOSEvent_Add");

        StringBuilder parameterString = new StringBuilder();

        JSONObject jsonObjFB = new JSONObject(hosEvent.dictionaryWithValuesForKeys());
        String formBody = jsonObjFB.toString();
        String input = Base64Converter.toBase64(formBody);


        if(isValidParameterString(input)){
            urlString.append("hosEventDTO=");
            urlString.append(input);
        }else{
            Log.e("DM","addHOSEvent: sitehosEventDTOId is not set");
        }
        Log.v("DM", "  API Parameters: " + parameterString);

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initializePOSTRequest(this.authHeaderValue, parameterString.toString(), new OnCompleteListeners.OnRestConnectionCompleteListener(){
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for addHOSEvent: " + object.toString());
                        HOSEventDTO results = new HOSEventDTO();
                        JSONObject jsonObject = (JSONObject)object;
                        results.readFromJSONObject(jsonObject);
                        listener.addHOSEvent(results, null);
                    }else{
                        Log.e("DM", "Null addHOSEvent object " );
                        listener.addHOSEvent(null, error);

                    }
                }catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException addHOSEvent >> " + e.getMessage());
                    listener.addHOSEvent(null, error);
                }
            }
        });


        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: HOSEventDTO");
    }

    //Tag Data

    public void uploadTagDataForDeviceType(String deviceType, String serialNumber,TagDataPackage tagDataPackage, final OnCompleteListeners.onUploadTagDataForDeviceTypeListener listener ) {

        Log.v("DM", "Enter Function: uploadTagDataForDeviceType");
        final OnCompleteListeners.onUploadTagDataForDeviceTypeListener  getPOISiteListForDCSite = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Device/TagData/UploadTagData");


        if(isValidParameterString(deviceType)){
            urlString.append("?deviceType=");
            urlString.append(deviceType);
        }else{
            Log.e("DM","uploadTagDataForDeviceType: deviceType is not set");
        }

        if(isValidParameterString(serialNumber)){
            urlString.append("&serialNumber=");
            urlString.append(serialNumber);
        }

//        NSDictionary *jsonDictionary = [tagDataPackage dictionaryWithValuesForKeys];
//
//        NSString *jsonTagDataPackage = [[jsonDictionary objectForKey:@"dataPostList"] toJSONString];
//
//        jsonTagDataPackage = [jsonTagDataPackage stringByReplacingOccurrencesOfString:@"\n" withString:@""];
//        jsonTagDataPackage = [jsonTagDataPackage stringByReplacingOccurrencesOfString:@" " withString:@""];
//        jsonTagDataPackage = [jsonTagDataPackage stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        String jsonTagDataPackage = tagDataPackage.toString();

        if(isValidParameterString(jsonTagDataPackage)){
            urlString.append("&jsonTagDataPackage=");
            urlString.append(tagDataPackage);
        }


        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "uploadTagDataForDeviceType", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for uploadTagDataForDeviceType: " + object.toString());
                    TagDataUploadDTO results = new TagDataUploadDTO();
                    results.readFromJSONObject(object);
                    listener.uploadTagDataForDeviceType(results);
                } else {
                    Log.d("DM","Null object response for uploadTagDataForDeviceType");
                }
            }
        });


        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: uploadTagDataForDeviceType");
    }

    public void getTRACRegistrationDetailsForSiteID(String siteId, final OnCompleteListeners.getTRACRegistrationDetailsForSiteIDListener listener ) {

        Log.v("DM", "Enter Function: getTRACRegistrationDetailsForSiteID");
        final OnCompleteListeners.getTRACRegistrationDetailsForSiteIDListener  getTRACRegistrationDetailsForSiteID = listener;

        String baseurl = this.baseUrl.toString();
        String url = baseurl.replace("Rest","API");
        StringBuilder urlString = new StringBuilder();
        //urlString.append(this.baseUrl);
        urlString.append(url);
        urlString.append("Routes/Site/");
        urlString.append(siteId);
        urlString.append("/Trac/RegistrationDetails");

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "getTRACRegistrationDetailsForSiteID", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for getTRACRegistrationDetailsForSiteID: " + object.toString());
                    RegistrationDetails results = new RegistrationDetails();
                    results.readFromJSONObject(object);
                    listener.getTRACRegistrationDetailsForSiteID(results);
                } else {
                    Log.d("DM","Null object response for getTRACRegistrationDetailsForSiteID");
                }
            }
        });


        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getPOISiteListForDCSite");
    }

    public void getTRACConfirmationForTracRouteID(String tracRouteID, final OnCompleteListeners.getTRACConfirmationForTracRouteIDListener listener ) {

        Log.v("DM", "Enter Function: getTRACConfirmationForTracRouteID");
        final OnCompleteListeners.getTRACConfirmationForTracRouteIDListener  getTRACConfirmationForTracRouteID = listener;

        String baseurl = this.baseUrl.toString();
        String url = baseurl.replace("Rest","API");
        StringBuilder urlString = new StringBuilder();
        //urlString.append(this.baseUrl);
        urlString.append(url);
        urlString.append("Routes/Route/");
        urlString.append(tracRouteID);
        urlString.append("/Trac/Confirmation");


        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, "getTRACConfirmationForTracRouteID", new OnCompleteListeners.OnTaskCompleteListener() {
            @Override
            public void onTaskComplete(JSONObject object) {
                if (object != null) {
                    Log.v("DM", "Received JSON Object for getTRACConfirmationForTracRouteID: " + object.toString());
                    TracRoute results = new TracRoute();
                    results.readFromJSONObject(object);
                    listener.getTRACConfirmationForTracRouteID(results);
                } else {
                    Log.d("DM","Null object response for getTRACConfirmationForTracRouteID");
                }
            }
        });


        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getTRACConfirmationForTracRouteID");
    }

        //////////******************Form APIs start **********//////////
        public void createFormWithAppId(String appId, String formDefinitionId, String deliveryId, String formBody, final OnCompleteListeners.onCreateFormWithAppIdListener listener ) {

            Log.v("DM", "Enter Function: createFormWithAppId");
            final OnCompleteListeners.onCreateFormWithAppIdListener  createFormWithAppId = listener;


            StringBuilder urlString = new StringBuilder();
            urlString.append(this.baseUrl);
            urlString.append("Pimm/Forms");

            //Generate Form ID
            String formId = UUID.randomUUID().toString();
            Log.v("DM","createFormWithAppId formId : "+ formId);

            //    NSString* formId = [[NSUUID UUID] UUIDString];
        //    DDLogVerbose(@"Form ID Generated=%@",formId);

            String input = Base64Converter.toBase64(formBody);
            Log.v("DM","createFormWithAppId input : "+ input);


            if(isValidParameterString(appId)){
                urlString.append("?appId=");
                urlString.append(appId);
            }else{
                Log.e("DM","createFormWithAppId: appId is not set");
            }
            urlString.append("&formId=");
            urlString.append(formId);

            if(isValidParameterString(formDefinitionId)){
                urlString.append("&formDefinitionId=");
                urlString.append(formDefinitionId);
            }else{
                Log.e("DM","createFormWithAppId: Missing/Empty/Invalid Parameter formDefinitionId");
            }
             if(isValidParameterString(deliveryId)){
                urlString.append("&deliveryId=");
                urlString.append(deliveryId);
            }else{
                Log.e("DM","createFormWithAppId: Missing/Empty/Invalid Parameter deliveryId");
            }

            urlString.append("&body=");
            urlString.append(input);

            Log.v("DM", "Request URL: " + urlString);

            RestConnection restConnection = new RestConnection();
            restConnection.initialize(this.authHeaderValue,  new OnCompleteListeners.OnRestConnectionCompleteListener(){
                @Override
                public void onRestConnectionComplete(Object object, Error error) {
                    try {
                        if (object != null) {
                           new JSONObject(String.valueOf(object));
                            Log.v("DM", "Received JSON Object for createFormWithAppId: " + object.toString());
                            PimmForm results = new PimmForm();
                            JSONObject jsonObject = (JSONObject)object;
                            results.readFromJSONObject(jsonObject);
                            listener.createFormWithAppId(results, null);
                        }else{
                            Log.e("DM", "Null createFormWithAppId object " );
                            listener.createFormWithAppId(null, error);
                        }
                    }catch (JSONException e) {
                        e.printStackTrace();
                        Log.e("DM", "JSONException createFormWithAppId >> " + e.getMessage());
                        listener.createFormWithAppId(null, error);

                    }
                             }
            });


            restConnection.execute(String.valueOf(urlString));
            Log.v("DM", "ExitFunction: createFormWithAppId");
        }

        public void createFormWithFormId(String formId, String appId, String formDefinitionId, String deliveryId, String formBody, final OnCompleteListeners.oncreateFormWithFormIdListener listener ) {

            Log.v("DM", "Enter Function: createFormWithFormId");
            final OnCompleteListeners.oncreateFormWithFormIdListener  createFormWithFormId = listener;


            StringBuilder urlString = new StringBuilder();
            urlString.append(this.baseUrl);
            urlString.append("Pimm/Forms");

            Log.v("DM","createFormWithFormId formId : "+ formId);


            String input = Base64Converter.toBase64(formBody);
            Log.v("DM","createFormWithFormId input : "+ input);

            StringBuilder parameterString = new StringBuilder();

            if(isValidParameterString(appId)){
                parameterString.append("appId=");
                parameterString.append(appId);
            }else{
                Log.e("DM","createFormWithFormId: appId is not set");
            }
            if(isValidParameterString(formId)){
                parameterString.append("&formId=");
                parameterString.append(formId);
            }else{
                Log.e("DM","createFormWithFormId: form id is not set");
            }

            if(isValidParameterString(formDefinitionId)){
                parameterString.append("&formDefinitionId=");
                parameterString.append(formDefinitionId);
            }else{
                Log.e("DM","createFormWithFormId: Missing/Empty/Invalid Parameter formDefinitionId");
            }
             if(isValidParameterString(deliveryId)){
                 parameterString.append("&deliveryId=");
                 parameterString.append(deliveryId);
            }else{
                Log.e("DM","createFormWithFormId: Missing/Empty/Invalid Parameter deliveryId");
            }

            parameterString.append("&body=");
            parameterString.append(input);

            Log.v("DM", "Request URL: " + urlString);

            Log.v("DM", "Create Form  API Parameters:: " + parameterString);

            RestConnection restConnection = new RestConnection();
            restConnection.initializePOSTRequest(this.authHeaderValue, parameterString.toString(),  new OnCompleteListeners.OnRestConnectionCompleteListener(){
                @Override
                public void onRestConnectionComplete(Object object, Error error) {
                    try {
                        if (object != null) {
                           new JSONObject(String.valueOf(object));
                            Log.v("DM", "Received JSON Object for createFormWithFormId: " + object.toString());
                            PimmForm results = new PimmForm();
                            JSONObject jsonObject = (JSONObject)object;
                            results.readFromJSONObject(jsonObject);
                            listener.createFormWithFormId(results, null);
                        }else{
                            Log.e("DM", "Null createFormWithAppId object " );
                            listener.createFormWithFormId(null, error);
                        }
                    }catch (JSONException e) {
                        e.printStackTrace();
                        Log.e("DM", "JSONException createFormWithFormId >> " + e.getMessage());
                        listener.createFormWithFormId(null, error);

                    }
                             }
            });


            restConnection.execute(String.valueOf(urlString));
            Log.v("DM", "ExitFunction: createFormWithFormId");
        }


        public void createFormWithFormId(String formId, String appId, String formDefinitionId, String refId, String formBody, final OnCompleteListeners.oncreateFormWithFormIdRefListener listener ) {

            Log.v("DM", "Enter Function: createFormWithFormId");
            final OnCompleteListeners.oncreateFormWithFormIdRefListener  createFormWithFormId = listener;


            StringBuilder urlString = new StringBuilder();
            urlString.append(this.baseUrl);
            urlString.append("Pimm/Forms");

            Log.v("DM","createFormWithFormId formId : "+ formId);


            String input = Base64Converter.toBase64(formBody);
            Log.v("DM","createFormWithFormId input : "+ input);

            StringBuilder parameterString = new StringBuilder();

            if(isValidParameterString(appId)){
                parameterString.append("?appId=");
                parameterString.append(appId);
            }else{
                Log.e("DM","createFormWithFormId: appId is not set");
            }
            if(isValidParameterString(formId)){
                parameterString.append("&formId=");
                parameterString.append(formId);
            }else{
                Log.e("DM","createFormWithFormId: form id is not set");
            }

            if(isValidParameterString(formDefinitionId)){
                parameterString.append("&formDefinitionId=");
                parameterString.append(formDefinitionId);
            }else{
                Log.e("DM","createFormWithFormId: Missing/Empty/Invalid Parameter formDefinitionId");
            }
             if(isValidParameterString(refId)){
                 parameterString.append("&referenceId=");
                 parameterString.append(refId);
            }else{
                Log.e("DM","createFormWithFormId: Missing/Empty/Invalid Parameter referenceId");
            }

            parameterString.append("&body=");
            parameterString.append(input);

            Log.v("DM", "Create Form  API Parameters:: " + parameterString);

            Log.v("DM", "Request URL: " + urlString);

            RestConnection restConnection = new RestConnection();
            restConnection.initializePOSTRequest(this.authHeaderValue, parameterString.toString(),  new OnCompleteListeners.OnRestConnectionCompleteListener(){
                @Override
                public void onRestConnectionComplete(Object object, Error error) {
                    try {
                        if (object != null) {
                           new JSONObject(String.valueOf(object));
                            Log.v("DM", "Received JSON Object for createFormWithFormId: " + object.toString());
                            PimmForm results = new PimmForm();
                            JSONObject jsonObject = (JSONObject)object;
                            results.readFromJSONObject(jsonObject);
                            listener.createFormWithFormIdref(results, null);
                        }else{
                            Log.e("DM", "Null createFormWithAppId object " );
                            listener.createFormWithFormIdref(null, error);
                        }
                    }catch (JSONException e) {
                        e.printStackTrace();
                        Log.e("DM", "JSONException createFormWithFormId >> " + e.getMessage());
                        listener.createFormWithFormIdref(null, error);

                    }
                 }
            });


            restConnection.execute(String.valueOf(urlString));
            Log.v("DM", "ExitFunction: createFormWithFormId");
        }

    public void createFormWithAppId(String appId, String formDefinitionId, String refId, String formBody, final OnCompleteListeners.onCreateFormWithAppIdrefIdListener listener ) {

        Log.v("DM", "Enter Function: createFormWithAppId");
        final OnCompleteListeners.onCreateFormWithAppIdrefIdListener  createFormWithAppId = listener;


        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Pimm/Forms");

        //Generate Form ID
        String formId = UUID.randomUUID().toString();
        Log.v("DM","createFormWithAppId formId : "+ formId);


        String input = Base64Converter.toBase64(formBody);
        Log.v("DM","createFormWithAppId input : "+ input);

        StringBuilder parameterString = new StringBuilder();

        if(isValidParameterString(appId)){
            parameterString.append("?appId=");
            parameterString.append(appId);
        }else{
            Log.e("DM","createFormWithAppId: appId is not set");
        }
        parameterString.append("&formId=");
        parameterString.append(formId);

        if(isValidParameterString(formDefinitionId)){
            parameterString.append("&formDefinitionId=");
            parameterString.append(formDefinitionId);
        }else{
            Log.e("DM","createFormWithAppId: Missing/Empty/Invalid Parameter formDefinitionId");
        }
        if(isValidParameterString(refId)){
            parameterString.append("&referenceId=");
            parameterString.append(refId);
        }else{
            Log.e("DM","createFormWithAppId: Missing/Empty/Invalid Parameter referenceId");
        }

        parameterString.append("&body=");
        parameterString.append(input);
        Log.v("DM", "Create Form  API Parameters:: " + parameterString);

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initializePOSTRequest(this.authHeaderValue, parameterString.toString(),  new OnCompleteListeners.OnRestConnectionCompleteListener(){
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for createFormWithAppId: " + object.toString());
                        PimmForm results = new PimmForm();
                        JSONObject jsonObject = (JSONObject)object;
                        results.readFromJSONObject(jsonObject);
                        listener.createFormWithAppIdrefID(results, null);
                    }else{
                        Log.e("DM", "Null createFormWithAppId object " );
                        listener.createFormWithAppIdrefID(null, error);
                    }
                }catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException createFormWithAppId >> " + e.getMessage());
                    listener.createFormWithAppIdrefID(null, error);

                }
            }
        });


        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: createFormWithAppId");
    }

    public void createFormWithAppId(String appId, String formDefinitionId, String refId, PimmRefTypeEnum refType, String formBody, final OnCompleteListeners.onCreateFormWithAppIdrefIdListener listener ) {

        Log.v("DM", "Enter Function: createFormWithAppId");
        final OnCompleteListeners.onCreateFormWithAppIdrefIdListener  createFormWithAppId = listener;


        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Pimm/Forms");

        //Generate Form ID
        String formId = UUID.randomUUID().toString();
        Log.v("DM","createFormWithAppId formId : "+ formId);


        String input = Base64Converter.toBase64(formBody);
        Log.v("DM","createFormWithAppId input : "+ input);

        StringBuilder parameterString = new StringBuilder();

        if(isValidParameterString(appId)){
            parameterString.append("?appId=");
            parameterString.append(appId);
        }else{
            Log.e("DM","createFormWithAppId: appId is not set");
        }
        parameterString.append("&formId=");
        parameterString.append(formId);

        if(isValidParameterString(formDefinitionId)){
            parameterString.append("&formDefinitionId=");
            parameterString.append(formDefinitionId);
        }else{
            Log.e("DM","createFormWithAppId: Missing/Empty/Invalid Parameter formDefinitionId");
        }
        if(isValidParameterString(refId)){
            parameterString.append("&refx=");
            parameterString.append(refId);
        }else{
            Log.e("DM","createFormWithAppId: Missing/Empty/Invalid Parameter referenceId");
        }

        if(refType != null){
            parameterString.append("&refType=");
            parameterString.append(String.format("&ld",Long.parseLong(refType.toString())));
        }else{
            Log.e("DM","createFormWithAppId: Missing/Empty/Invalid Parameter refType");
        }

        parameterString.append("&body=");
        parameterString.append(input);
        Log.v("DM", "Create Form  API Parameters:: " + parameterString);

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initializePOSTRequest(this.authHeaderValue, parameterString.toString(),  new OnCompleteListeners.OnRestConnectionCompleteListener(){
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for createFormWithAppId: " + object.toString());
                        PimmForm results = new PimmForm();
                        JSONObject jsonObject = (JSONObject)object;
                        results.readFromJSONObject(jsonObject);
                        listener.createFormWithAppIdrefID(results, null);
                    }else{
                        Log.e("DM", "Null createFormWithAppId object " );
                        listener.createFormWithAppIdrefID(null, error);
                    }
                }catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException createFormWithAppId >> " + e.getMessage());
                    listener.createFormWithAppIdrefID(null, error);

                }
            }
        });


        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: createFormWithAppId");
    }
    public void createFormWithFormId(String formId, String appId, String formDefinitionId, String refId, PimmRefTypeEnum refType, String formBody, final OnCompleteListeners.oncreateFormWithFormIdRefListener listener ) {

        Log.v("DM", "Enter Function: createFormWithFormId");
        final OnCompleteListeners.oncreateFormWithFormIdRefListener  createFormWithFormId = listener;


        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Pimm/Forms");

        Log.v("DM","createFormWithFormId formId : "+ formId);


        String input = Base64Converter.toBase64(formBody);
        Log.v("DM","createFormWithFormId input : "+ input);
        StringBuilder parameterString = new StringBuilder();


        if(isValidParameterString(appId)){
            parameterString.append("?appId=");
            parameterString.append(appId);
        }else{
            Log.e("DM","createFormWithFormId: appId is not set");
        }
        if(isValidParameterString(formId)){
            parameterString.append("&formId=");
            parameterString.append(formId);
        }else{
            Log.e("DM","createFormWithFormId: form id is not set");
        }

        if(isValidParameterString(formDefinitionId)){
            parameterString.append("&formDefinitionId=");
            parameterString.append(formDefinitionId);
        }else{
            Log.e("DM","createFormWithFormId: Missing/Empty/Invalid Parameter formDefinitionId");
        }
        if(isValidParameterString(refId)){
            parameterString.append("&referenceId=");
            parameterString.append(refId);
        }else{
            Log.e("DM","createFormWithFormId: Missing/Empty/Invalid Parameter referenceId");
        }
        if(refType != null){
            parameterString.append("&refType=");
            parameterString.append(String.format("&ld",Long.parseLong(refType.toString())));
        }else{
            Log.e("DM","createFormWithAppId: Missing/Empty/Invalid Parameter refType");
        }


        parameterString.append("&body=");
        parameterString.append(input);

        Log.v("DM", "Request URL: " + urlString);
        Log.v("DM", "Create Form  API Parameters:: " + parameterString);

        RestConnection restConnection = new RestConnection();
        restConnection.initializePOSTRequest(this.authHeaderValue, parameterString.toString(),  new OnCompleteListeners.OnRestConnectionCompleteListener(){
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for createFormWithFormId: " + object.toString());
                        PimmForm results = new PimmForm();
                        JSONObject jsonObject = (JSONObject)object;
                        results.readFromJSONObject(jsonObject);
                        listener.createFormWithFormIdref(results, null);
                    }else{
                        Log.e("DM", "Null createFormWithAppId object " );
                        listener.createFormWithFormIdref(null, error);
                    }
                }catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException createFormWithFormId >> " + e.getMessage());
                    listener.createFormWithFormIdref(null, error);

                }
            }
        });


        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: createFormWithFormId");
    }

    public void createFormWithAppId(String appId, String formDefinitionId, String pimmFormId, String formBody, final OnCompleteListeners.onCreateFormWithAppIdpimmformIdListener listener ) {

        Log.v("DM", "Enter Function: createFormWithAppId");
        final OnCompleteListeners.onCreateFormWithAppIdpimmformIdListener createFormWithAppId = listener;


        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Pimm/Forms");

        //Generate Form ID
        String formId = UUID.randomUUID().toString();
        Log.v("DM","createFormWithAppId formId : "+ formId);

        String input = Base64Converter.toBase64(formBody);
        Log.v("DM","createFormWithAppId input : "+ input);

        StringBuilder parameterString = new StringBuilder();

        if(isValidParameterString(appId)){
            parameterString.append("?appId=");
            parameterString.append(appId);
        }else{
            Log.e("DM","createFormWithAppId: appId is not set");
        }
        parameterString.append("&formId=");
        parameterString.append(formId);

        if(isValidParameterString(formDefinitionId)){
            parameterString.append("&formDefinitionId=");
            parameterString.append(formDefinitionId);
        }else{
            Log.e("DM","createFormWithAppId: Missing/Empty/Invalid Parameter formDefinitionId");
        }
        if(isValidParameterString(pimmFormId)){
            parameterString.append("&pimmFormId=");
            parameterString.append(pimmFormId);
        }else{
            Log.e("DM","createFormWithAppId: Missing/Empty/Invalid Parameter pimmFormId");
        }

        parameterString.append("&body=");
        parameterString.append(input);
        Log.v("DM", "Create Form  API Parameters:: " + parameterString);

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initializePOSTRequest(this.authHeaderValue, parameterString.toString(),  new OnCompleteListeners.OnRestConnectionCompleteListener(){
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for createFormWithAppId: " + object.toString());
                        PimmForm results = new PimmForm();
                        JSONObject jsonObject = (JSONObject)object;
                        results.readFromJSONObject(jsonObject);
                        listener.createFormWithAppIdrefIDpimmform(results, null);
                    }else{
                        Log.e("DM", "Null createFormWithAppId object " );
                        listener.createFormWithAppIdrefIDpimmform(null, error);
                    }
                }catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException createFormWithAppId >> " + e.getMessage());
                    listener.createFormWithAppIdrefIDpimmform(null, error);

                }
            }
        });


        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: createFormWithAppId");
    }

    //////////******************Form APIs end **********//////////
    public void getRouteShipmentEquipmentForSiteId(String siteId ,boolean valid, final OnCompleteListeners.getRouteShipmentEquipmentForSiteIdListener listener ) {

        Log.v("DM", "Enter Function: getRouteShipmentEquipmentForSiteId");

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/RouteShipmentEquipment");

            urlString.append("?siteId=");
            urlString.append(siteId);
            urlString.append("&valid=");
            urlString.append(valid ? "true" : "false");

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();

        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                if(error == null && object!=null)
                {
                    try
                    {
                        Log.v("DM", "Received JSON Object for getRouteShipmentEquipmentForSiteId: " + object.toString());
                        JSONArray jsonArray = (JSONArray)object;
                        ArrayList<RouteShipmentEquipmentDTO> equipmentList = new ArrayList<RouteShipmentEquipmentDTO>();

                        for(int i = 0; i < jsonArray.length(); i ++)
                        {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);

                            RouteShipmentEquipmentDTO equipmentDTO = new RouteShipmentEquipmentDTO();
                            equipmentDTO.readFromJSONObject(jsonObject);
                            equipmentList.add(equipmentDTO);
                        }

                        listener.getRouteShipmentEquipmentForSiteId(equipmentList, null);
                    }
                    catch (JSONException e)
                    {
                        listener.getRouteShipmentEquipmentForSiteId(null, new Error(e.getMessage()));
                    }

                }
            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getRouteShipmentEquipmentForSiteId");
    }

    public void getRouteShipmentUnitListForDCSiteId(String dcSiteId, final OnCompleteListeners.getRouteShipmentUnitListForDCSiteIdListener listener ) {

        Log.v("DM", "Enter Function: getRouteShipmentUnitListForDCSiteId");

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/RouteShipmentUnit");


        if(isValidParameterString(dcSiteId)) {
            urlString.append("?siteId=");
            urlString.append(dcSiteId);
        }else{
            Log.e("DM", "getRouteShipmentUnitListForDCSiteId error :Invalid parameter dcSiteId ");
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();

        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                if(error == null)
                {
                    try
                    {
                        Log.v("DM", "Received JSON Object for getRouteShipmentUnitListForDCSiteId: " + object.toString());
                        JSONArray jsonArray = (JSONArray)object;
                        ArrayList<RouteShipmentUnit> routeShipmentUnitList = new ArrayList<RouteShipmentUnit>();

                        for(int i = 0; i < jsonArray.length(); i ++)
                        {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);

                            RouteShipmentUnit routeShipmentUnit = new RouteShipmentUnit();
                            routeShipmentUnit.readFromJSONObject(jsonObject);
                            routeShipmentUnitList.add(routeShipmentUnit);
                        }

                        listener.getRouteShipmentUnitListForDCSiteId(routeShipmentUnitList, null);
                    }
                    catch (JSONException e)
                    {
                        listener.getRouteShipmentUnitListForDCSiteId(null, new Error(e.getMessage()));
                        Log.e("DM", " getRouteShipmentUnitListForDCSiteId JSONException " + e.getMessage());
                    }
                }else{
                    listener.getRouteShipmentUnitListForDCSiteId(null, error);
                    Log.e("DM", " getRouteShipmentUnitListForDCSiteId null object "+ error);
                }


            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getRouteShipmentUnitListForDCSiteId");
    }

    public void getRouteShipmentProductListForCustomerId(String customerId, String siteId, boolean valid, final OnCompleteListeners.getRouteShipmentProductListForCustomerIdListener listener ) {

        Log.v("DM", "Enter Function: getRouteShipmentProductListForCustomerId");

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/RouteShipmentProduct");

        if(isValidParameterString(siteId)) {
            urlString.append("?siteId=");
            urlString.append(siteId);
            urlString.append("&customerId=");
            urlString.append(customerId);
            urlString.append("&valid=");
            urlString.append(valid ? "true" : "false");
        }else{
            Log.e("DM", "getRouteShipmentProductListForCustomerId error :Invalid parameter dcSiteId ");
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();

        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                if(error == null)
                {
                    try
                    {
                        Log.v("DM", "Received JSON Object for getRouteShipmentProductListForCustomerId: " + object.toString());
                        JSONArray jsonArray = (JSONArray)object;
                        ArrayList<RouteShipmentProduct> routeShipmentProductList = new ArrayList<RouteShipmentProduct>();

                        for(int i = 0; i < jsonArray.length(); i ++)
                        {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);

                            RouteShipmentProduct routeShipmentProduct = new RouteShipmentProduct();
                            routeShipmentProduct.readFromJSONObject(jsonObject);
                            routeShipmentProductList.add(routeShipmentProduct);
                        }

                        listener.getRouteShipmentProductListForCustomerId(routeShipmentProductList, null);
                    }
                    catch (JSONException e)
                    {
                        listener.getRouteShipmentProductListForCustomerId(null, new Error(e.getMessage()));
                    }

                }else{
                    listener.getRouteShipmentProductListForCustomerId(null, error);
                }
            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getRouteShipmentProductListForCustomerId");
    }

    public void getDCSitesForUserId(String userId, final OnCompleteListeners.getDCSitesForUserIdListener listener ) {

        Log.v("DM", "Enter Function: getRouteShipmentProductListForCustomerId");

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("SupplierDelivery/SupplierDeliverySite");

        if(isValidParameterString(userId)) {
            urlString.append("?userId=");
            urlString.append(userId);

        }else{
            Log.e("DM", "getDCSitesForUserId error :Invalid parameter user id ");
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();

        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                if(error == null)
                {
                    try
                    {
                        JSONArray jsonArray = (JSONArray)object;
                        ArrayList<Site> siteArrayList = new ArrayList<Site>();

                        for(int i = 0; i < jsonArray.length(); i ++)
                        {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);

                            Site result = new Site();
                            result.readFromJSONObject(jsonObject);
                            siteArrayList.add(result);
                        }

                        listener.getDCSitesForUserId(siteArrayList, null);
                    }
                    catch (JSONException e)
                    {
                        listener.getDCSitesForUserId(null, new Error(e.getMessage()));
                    }

                }else{
                    listener.getDCSitesForUserId(null, error);
                }
            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getDCSitesForUserId");
    }

    public void getRouteShipmentInvoiceItemListForSiteId( String siteId, boolean valid, final OnCompleteListeners.routeShipmentInvoiceItemListListener listener ) {

        Log.v("DM", "Enter Function: getRouteShipmentInvoiceItemListForSiteId");

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/RouteShipmentInvoiceItem");

        if(isValidParameterString(siteId)) {
            urlString.append("?siteId=");
            urlString.append(siteId);
            urlString.append("&valid=");
            urlString.append(valid ? "true" : "false");
        }else{
            Log.e("DM", "getRouteShipmentInvoiceItemListForSiteId error :Invalid parameter dcSiteId ");
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();

        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                if(error == null)
                {
                    try
                    {
                        JSONArray jsonArray = (JSONArray)object;
                        ArrayList<RouteShipmentInvoiceItem> routeShipmentInvoiceItemList = new ArrayList<RouteShipmentInvoiceItem>();

                        for(int i = 0; i < jsonArray.length(); i ++)
                        {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);

                            RouteShipmentInvoiceItem routeShipmentInvoiceItem = new RouteShipmentInvoiceItem();
                            routeShipmentInvoiceItem.readFromJSONObject(jsonObject);
                            routeShipmentInvoiceItemList.add(routeShipmentInvoiceItem);
                        }

                        listener.routeShipmentInvoiceItemList(routeShipmentInvoiceItemList, null);
                    }
                    catch (JSONException e)
                    {
                        listener.routeShipmentInvoiceItemList(null, new Error(e.getMessage()));
                    }

                }
                else
                {
                    listener.routeShipmentInvoiceItemList(null, error);
                }
            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getRouteShipmentInvoiceItemListForSiteId");
    }


    public void postGPSDataForDeviceSerialNumber(String serialNumber, String deviceType,VehicleDataPackage vehicleDataPackage, final OnCompleteListeners.postGPSDataForDeviceSerialNumberListener listener ) {

        Log.v("DM", "Enter Function: postGPSDataForDeviceSerialNumber");
        final OnCompleteListeners.postGPSDataForDeviceSerialNumberListener  postGPSDataForDeviceSerial = listener;


        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/TrailerReport");

        StringBuilder parameterString = new StringBuilder();

        if(isValidParameterString(serialNumber)){
            parameterString.append("serialNumber=");
            parameterString.append(serialNumber);
        }else{
            Log.e("DM","postGPSDataForDeviceSerialNumber: serialNumber is not set");
        }

        if(isValidParameterString(deviceType)){
            parameterString.append("&deviceType=");
            parameterString.append(deviceType);
        }else{
            Log.e("DM","postGPSDataForDeviceSerialNumber: Missing/Empty/Invalid Parameter deviceType");
        }

        if(vehicleDataPackage != null) {

            JSONObject jsonObjFB = new JSONObject(vehicleDataPackage.dictionaryWithValuesForKeys());
            String formBody = jsonObjFB.toString();
            String input = Base64Converter.toBase64(formBody);
            parameterString.append("&vehicleData=");
            parameterString.append(input);
        }


        Log.v("DM", "Create Form  API Parameters: " + parameterString);

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initializePOSTRequest(this.authHeaderValue, parameterString.toString(),  new OnCompleteListeners.OnRestConnectionCompleteListener(){
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for postGPSDataForDeviceSerialNumber: " + object.toString());
                        listener.postGPSDataForDeviceSerialNumber(null);
                    }else if (object == null && error == null){
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for postGPSDataForDeviceSerialNumber: " + object.toString());
                        listener.postGPSDataForDeviceSerialNumber(null);
                    }else{
                        Log.e("DM", "Null postGPSDataForDeviceSerialNumber object " );
                        listener.postGPSDataForDeviceSerialNumber( error);
                    }
                }catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException postGPSDataForDeviceSerialNumber >> " + e.getMessage());
                    listener.postGPSDataForDeviceSerialNumber(error);

                }
            }
        });


        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: postGPSDataForDeviceSerialNumber");
    }

    public void getFormListForShipmentId(String deliveryId, String shipmentId, String appId, final OnCompleteListeners.getFormListForShipmentIdListener listener) {

        Log.v("DM", "Enter Function: getFormListForShipmentId");

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Pimm/Forms");

        if(isValidParameterString(appId)) {
            urlString.append("?appId=");
            urlString.append(appId);
            urlString.append("&deliveryId=");
            urlString.append(shipmentId);
        }else{
            Log.e("DM", "getFormListForShipmentId error :Invalid parameter appID ");
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();

        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                if(error == null)
                {
                    try
                    {
                        JSONArray jsonArray = (JSONArray)object;
                        ArrayList<PimmForm> pimmformList = new ArrayList<PimmForm>();

                        for(int i = 0; i < jsonArray.length(); i ++)
                        {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);

                            PimmForm pimmForm = new PimmForm();
                            pimmForm.readFromJSONObject(jsonObject);
                            pimmformList.add(pimmForm);
                        }

                        listener.getFormListForShipmentId(pimmformList, null);
                    }
                    catch (JSONException e)
                    {
                        listener.getFormListForShipmentId(null, new Error(e.getMessage()));
                    }

                }
                else
                {
                    listener.getFormListForShipmentId(null, error);
                }
            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getFormListForShipmentId");
    }



    public void getHOSSummaryForDriverId( String driverId, String deliveryId, String startDate, String endDate, final OnCompleteListeners.getHOSSummaryForDriverIdListener listener ) {

        Log.v("DM", "Enter Function: getHOSSummaryForDriverId");

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Driver/HOS/HOSSummary/");

        if(isValidParameterString(driverId)) {
            urlString.append(driverId);
        }else{
            Log.e("DM", "getHOSSummaryForDriverId error :Missing/Empty/Invalid argument driverID ");
        }
        if(isValidParameterString(deliveryId)) {
            urlString.append("?deliveryId=");
            urlString.append(deliveryId);
        }else{
            Log.e("DM", "getHOSSummaryForDriverId error :Missing/Empty/Invalid argument deliveryId ");
        }
        if(isValidParameterString(startDate) && isValidParameterString(endDate)) {
            urlString.append("&start=");
            urlString.append(startDate);
            urlString.append("&end=");
            urlString.append(endDate);
        }
        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();

        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                if(error == null)
                {
                    try
                    {
                        JSONArray jsonArray = (JSONArray)object;
                        ArrayList<HOSSummary> hosSummaryArrayList = new ArrayList<HOSSummary>();

                        for(int i = 0; i < jsonArray.length(); i ++)
                        {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);

                            HOSSummary hosSummary = new HOSSummary();
                            hosSummary.readFromJSONObject(jsonObject);
                            hosSummaryArrayList.add(hosSummary);
                        }

                        listener.getHOSSummaryForDriverId(hosSummaryArrayList, null);
                    }
                    catch (JSONException e)
                    {
                        listener.getHOSSummaryForDriverId(null, new Error(e.getMessage()));
                    }

                }
                else
                {
                    listener.getHOSSummaryForDriverId(null, error);
                }
            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getHOSSummaryForDriverId");
    }

    public void getHOSActivityForUserId(String userId, HOSActivity.HOSActivityTypeEnum activityType, Date startDate, Date endDate, final OnCompleteListeners.getHOSActivityForUserIdListener listener ) {

        Log.v("DM", "Enter Function: getHOSActivityForUserId");

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Driver/HOS/HOSActivity/");

        if(isValidParameterString(userId)) {
            urlString.append(userId);
        }else{
            Log.e("DM", "getHOSActivityForUserId error :Missing/Empty/Invalid argument userId ");
        }
        if(activityType.getValue() > 0) {
            urlString.append("?activityType=");
            urlString.append(String.format("%d", activityType.getValue()));
        }else{
            Log.e("DM", "getHOSActivityForUserId error :Missing/Empty/Invalid argument activityType ");
        }
        if(startDate != null && endDate != null) {
            urlString.append("&start=");
            urlString.append(getUTCFormatDate(startDate));
            urlString.append("&end=");
            urlString.append(getUTCFormatDate(endDate));
        }
        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();

        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                if(error == null)
                {
                    try
                    {
                        JSONArray jsonArray = (JSONArray)object;
                        ArrayList<HOSActivity> hosActivityArrayList = new ArrayList<HOSActivity>();

                        for(int i = 0; i < jsonArray.length(); i ++)
                        {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);

                            HOSActivity hosActivity = new HOSActivity();
                            hosActivity.readFromJSONObject(jsonObject);
                            hosActivityArrayList.add(hosActivity);
                        }

                        listener.getHOSActivityForUserId(hosActivityArrayList, null);
                    }
                    catch (JSONException e)
                    {
                        listener.getHOSActivityForUserId(null, new Error(e.getMessage()));
                    }

                }
                else
                {
                    listener.getHOSActivityForUserId(null, error);
                }
            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getHOSActivityForUserId");
    }


    public void getAssignmentForDCSite(String siteId, final OnCompleteListeners.getAssignmentForDCSiteListener listener ) {

        Log.v("DM", "Enter Function: getAssignmentForDCSite");

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/DCSite");
         String operation = "assignment";

        if(isValidParameterString(siteId)) {
            urlString.append("?siteId=");
            urlString.append(siteId);
            urlString.append("&op=");
            urlString.append(operation);
        }else{
            Log.e("DM", "getAssignmentForDCSite error :Missing/Empty/Invalid argument siteId ");
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();

        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                if(error == null)
                {
                    try
                    {
                        JSONArray jsonArray = (JSONArray)object;
                        ArrayList<AssignmentInfo> assignmentInfos = new ArrayList<AssignmentInfo>();

                        for(int i = 0; i < jsonArray.length(); i ++)
                        {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);

                            AssignmentInfo assignmentInfo = new AssignmentInfo();
                            assignmentInfo.readFromJSONObject(jsonObject);
                            assignmentInfos.add(assignmentInfo);
                        }

                        listener.getAssignmentForDCSite(assignmentInfos, null);
                    }
                    catch (JSONException e)
                    {
                        listener.getAssignmentForDCSite(null, new Error(e.getMessage()));
                    }

                }
                else
                {
                    listener.getAssignmentForDCSite(null, error);
                }
            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getAssignmentForDCSite");
    }

    public void startRouteFromHereForDeliveryId(String deliveryId, String poiId, double latitude, double longitude, String locationName, boolean hasAddress,
                                                AddressComponents address, boolean hasComment, String comment, boolean hasManualTimes, String startTime, String departureTime, final OnCompleteListeners.startRouteFromHereForDeliveryIdListener listener ) {

        Log.v("DM", "Enter Function: startRouteFromHereForDeliveryId");
        final OnCompleteListeners.startRouteFromHereForDeliveryIdListener  startRouteFromHereForDeliveryId = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/RouteShipment2/StartRouteFromHere");


        if(isValidParameterString(deliveryId)){
            urlString.append("?deliveryId=");
            urlString.append(deliveryId);
        }else{
            Log.e("DM","startRouteFromHereForDeliveryId: deliveryId is not set");
        }

        if(isValidParameterString(poiId)){
            urlString.append("&poiId=");
            urlString.append(poiId);
        }else{
            Log.e("DM","startRouteFromHereForDeliveryId: Missing/Empty/Invalid Parameter deviceType");
        }
        urlString.append("&latitude=");
        urlString.append(String.format("%f", latitude));
        urlString.append("&longitude=");
        urlString.append(String.format("%f", longitude));


        if(isValidParameterString(locationName)){
            urlString.append("&locationName=");
            urlString.append(locationName);
        }else{
            Log.e("DM","startRouteFromHereForDeliveryId: Missing/Empty/Invalid Parameter locationName");
        }

        if(hasAddress == true){
            if(address != null){
                if (address.Address1 != null) {
                    urlString.append("&address1=");
                    urlString.append(address.Address1);
                }
                if (address.Address2 != null) {
                    urlString.append("&address2=");
                    urlString.append(address.Address2);
                }
                if (address.City != null) {
                    urlString.append("&city=");
                    urlString.append(address.City);
                }
                if (address.Province != null) {
                    urlString.append("&province=");
                    urlString.append(address.Province);
                }
                if (address.Postal != null) {
                    urlString.append("&postal=");
                    urlString.append(address.Postal);
                }
                if (address.Country != null) {
                    urlString.append("&country=");
                    urlString.append(address.Country);
                }
                if (address.StreetName != null) {
                    urlString.append("&streetName=");
                    urlString.append(address.StreetName);
                }
                if (address.StreetNumber != null) {
                    urlString.append("&streetNumber=");
                    urlString.append(address.StreetNumber);
                }
            }else {
                Log.d("DM","startRouteFromHereForDeliveryId: HasAddress parameter is set to TRUE But 'address' parameter is nil");
            }
        }
        if(hasManualTimes == true) {
            urlString.append("&hasManualTimes=");
            urlString.append("true");

            if(isValidParameterString(startTime)){
                urlString.append("&startTime=");
                urlString.append(startTime);
            }

            if(isValidParameterString(departureTime)) {
                urlString.append("&departureTime=");
                urlString.append(departureTime);
            }
        }

        if(hasComment == true) {
            urlString.append("&hasComment=");
            urlString.append("true");

            if (isValidParameterString(comment)) {
                urlString.append("&comment=");
                urlString.append(comment);
            }else {
                Log.d("DM","startRouteFromHereForDeliveryId: HasComment parameter is set to TRUE But 'comment' parameter is nil");
            }
        }


        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue,  new OnCompleteListeners.OnRestConnectionCompleteListener(){
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for startRouteFromHereForDeliveryId: " + object.toString());
                        DeliveryEndPointActionResult result = new DeliveryEndPointActionResult();
                        JSONObject jsonObject = (JSONObject) object;
                        result.readFromJSONObject(jsonObject);
                        listener.startRouteFromHereForDeliveryId(result, null);
                    }else{
                        Log.e("DM", "Null postGPSDataForDeviceSerialNumber object " );
                        listener.startRouteFromHereForDeliveryId(null,error);
                    }
                }catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException postGPSDataForDeviceSerialNumber >> " + e.getMessage());
                    listener.startRouteFromHereForDeliveryId(null,error);

                }
            }
        });


        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: postGPSDataForDeviceSerialNumber");
    }


    public void getStopListForDCSite(String siteId, final OnCompleteListeners.getStopListForDCSiteListener listener ) {

        Log.v("DM", "Enter Function: getStopListForDCSite");

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/DCSite");
        String operation = "stops";

        if(isValidParameterString(siteId)) {
            urlString.append("?siteId=");
            urlString.append(siteId);
            urlString.append("&op=");
            urlString.append(operation);
        }else{
            Log.e("DM", "getStopListForDCSite error :Missing/Empty/Invalid argument siteId ");
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();

        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                if(error == null)
                {
                    try
                    {
                        JSONArray jsonArray = (JSONArray)object;
                        ArrayList<Site> siteArrayList = new ArrayList<Site>();

                        for(int i = 0; i < jsonArray.length(); i ++)
                        {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);

                            Site site = new Site();
                            site.readFromJSONObject(jsonObject);
                            siteArrayList.add(site);
                        }

                        listener.getStopListForDCSite(siteArrayList, null);
                    }
                    catch (JSONException e)
                    {
                        listener.getStopListForDCSite(null, new Error(e.getMessage()));
                    }

                }
                else
                {
                    listener.getStopListForDCSite(null, error);
                }
            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getStopListForDCSite");
    }


  public void getDriverListForDCSite(String siteId, final OnCompleteListeners.getDriverListForDCSiteListener listener ) {

        Log.v("DM", "Enter Function: getDriverListForDCSite");

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/DCSite");
        String operation = "drivers";

        if(isValidParameterString(siteId)) {
            urlString.append("?siteId=");
            urlString.append(siteId);
            urlString.append("&op=");
            urlString.append(operation);
        }else{
            Log.e("DM", "getDriverListForDCSite error :Missing/Empty/Invalid argument siteId ");
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();

        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                if(error == null)
                {
                    try
                    {
                        JSONArray jsonArray = (JSONArray)object;
                        ArrayList<User> userArrayList = new ArrayList<User>();

                        for(int i = 0; i < jsonArray.length(); i ++)
                        {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);

                            User user = new User();
                            user.readFromJSONObject(jsonObject);
                            userArrayList.add(user);
                        }

                        listener.getDriverListForDCSite(userArrayList, null);
                    }
                    catch (JSONException e)
                    {
                        listener.getDriverListForDCSite(null, new Error(e.getMessage()));
                    }

                }
                else
                {
                    listener.getDriverListForDCSite(null, error);
                }
            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getDriverListForDCSite");
    }



  public void getTrailerListForDCSite(String siteId, final OnCompleteListeners.getTrailerListForDCSiteListener listener ) {

        Log.v("DM", "Enter Function: getTrailerListForDCSite");

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/DCSite");
        String operation = "trailers";

        if(isValidParameterString(siteId)) {
            urlString.append("?siteId=");
            urlString.append(siteId);
            urlString.append("&op=");
            urlString.append(operation);
        }else{
            Log.e("DM", "getTrailerListForDCSite error :Missing/Empty/Invalid argument siteId ");
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();

        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                if(error == null)
                {
                    try
                    {
                        JSONArray jsonArray = (JSONArray)object;
                        ArrayList<Device> deviceArrayList = new ArrayList<Device>();

                        for(int i = 0; i < jsonArray.length(); i ++)
                        {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);

                            Device device = new Device();
                            device.readFromJSONObject(jsonObject);
                            deviceArrayList.add(device);
                        }

                        listener.getTrailerListForDCSite(deviceArrayList, null);
                    }
                    catch (JSONException e)
                    {
                        listener.getTrailerListForDCSite(null, new Error(e.getMessage()));
                    }

                }
                else
                {
                    listener.getTrailerListForDCSite(null, error);
                }
            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getTrailerListForDCSite");
    }

    public void getTrailerReportForDCSite(String siteId, final OnCompleteListeners.getTrailerReportForDCSiteListener listener ) {

        Log.v("DM", "Enter Function: getTrailerReportForDCSite");

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/TrailerReport");

        if(isValidParameterString(siteId)) {
            urlString.append("?siteId=");
            urlString.append(siteId);

        }else{
            Log.e("DM", "getTrailerReportForDCSite error :Missing/Empty/Invalid argument siteId ");
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();

        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                if(error == null)
                {
                    try
                    {
                        JSONArray jsonArray = (JSONArray)object;
                        ArrayList<DeliveryTrailerDTO> resultArrayList = new ArrayList<DeliveryTrailerDTO>();

                        for(int i = 0; i < jsonArray.length(); i ++)
                        {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);

                            DeliveryTrailerDTO result = new DeliveryTrailerDTO();
                            result.readFromJSONObject(jsonObject);
                            resultArrayList.add(result);
                        }

                        listener.getTrailerReportForDCSite(resultArrayList, null);
                    }
                    catch (JSONException e)
                    {
                        listener.getTrailerReportForDCSite(null, new Error(e.getMessage()));
                    }

                }
                else
                {
                    listener.getTrailerReportForDCSite(null, error);
                }
            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getTrailerReportForDCSite");
    }

    public void getTractorReportForDCSite(String siteId, final OnCompleteListeners.getTractorReportForDCSiteListener listener ) {

        Log.v("DM", "Enter Function: getTractorReportForDCSite");

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/TrailerReport?tractor=true");

        if(isValidParameterString(siteId)) {
            urlString.append("&siteId=");
            urlString.append(siteId);

        }else{
            Log.e("DM", "getTractorReportForDCSite error :Missing/Empty/Invalid argument siteId ");
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();

        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                if(error == null)
                {
                    try
                    {
                        JSONArray jsonArray = (JSONArray)object;
                        ArrayList<DeliveryTrailerDTO> resultArrayList = new ArrayList<DeliveryTrailerDTO>();

                        for(int i = 0; i < jsonArray.length(); i ++)
                        {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);

                            DeliveryTrailerDTO result = new DeliveryTrailerDTO();
                            result.readFromJSONObject(jsonObject);
                            resultArrayList.add(result);
                        }

                        listener.getTractorReportForDCSite(resultArrayList, null);
                    }
                    catch (JSONException e)
                    {
                        listener.getTractorReportForDCSite(null, new Error(e.getMessage()));
                    }

                }
                else
                {
                    listener.getTractorReportForDCSite(null, error);
                }
            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getTractorReportForDCSite");
    }



    public void getTractorListForDCSite(String siteId, final OnCompleteListeners.getTractorListForDCSiteListener listener ) {

        Log.v("DM", "Enter Function: getTractorListForDCSite");

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/DCSite");
        String operation = "tractors";

        if(isValidParameterString(siteId)) {
            urlString.append("?siteId=");
            urlString.append(siteId);
            urlString.append("&op=");
            urlString.append(operation);
        }else{
            Log.e("DM", "getTractorListForDCSite error :Missing/Empty/Invalid argument siteId ");
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();

        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                if(error == null)
                {
                    try
                    {
                        JSONArray jsonArray = (JSONArray)object;
                        ArrayList<Device> deviceArrayList = new ArrayList<Device>();

                        for(int i = 0; i < jsonArray.length(); i ++)
                        {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);

                            Device device = new Device();
                            device.readFromJSONObject(jsonObject);
                            deviceArrayList.add(device);
                        }

                        listener.getTractorListForDCSite(deviceArrayList, null);
                    }
                    catch (JSONException e)
                    {
                        listener.getTractorListForDCSite(null, new Error(e.getMessage()));
                    }

                }
                else
                {
                    listener.getTractorListForDCSite(null, error);
                }
            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getTractorListForDCSite");
    }


    public void getRouteScheduleForDCSite(String siteId, Date timestampUTC, final OnCompleteListeners.getRouteScheduleForDCSiteListener listener ) {

        Log.v("DM", "Enter Function: getTrailerListForDCSite");

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/DCSite");
        String operation = "routeschedule2";

        if(isValidParameterString(siteId)) {
            urlString.append("?siteId=");
            urlString.append(siteId);
            urlString.append("&op=");
            urlString.append(operation);

            urlString.append("&timestampUTC=");
            urlString.append(getUTCFormatDate(timestampUTC));
        }else{
            Log.e("DM", "getRouteScheduleForDCSite error :Missing/Empty/Invalid argument siteId ");
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();

        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                if(error == null)
                {
                    try
                    {
                        JSONArray jsonArray = (JSONArray)object;
                        ArrayList<RouteShipment> routeShipmentArrayList = new ArrayList<RouteShipment>();

                        for(int i = 0; i < jsonArray.length(); i ++)
                        {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);

                            RouteShipment routeShipment = new RouteShipment();
                            routeShipment.readFromJSONObject(jsonObject);
                            routeShipmentArrayList.add(routeShipment);
                        }

                        listener.getRouteScheduleForDCSite(routeShipmentArrayList, null);
                    }
                    catch (JSONException e)
                    {
                        listener.getRouteScheduleForDCSite(null, new Error(e.getMessage()));
                    }

                }
                else
                {
                    listener.getRouteScheduleForDCSite(null, error);
                }
            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getRouteScheduleForDCSite");
    }


    public void getRouteScheduleSummaryDataForDCSite(String siteId, Date date, final OnCompleteListeners.getRouteScheduleSummaryDataForDCSiteListener listener ) {

        Log.v("DM", "Enter Function: getRouteScheduleSummaryDataForDCSite");

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/DCSite/RouteSchedule");
        String routeDay = getUTCFormatDate(date);

        if(isValidParameterString(siteId)) {
            urlString.append("?siteId=");
            urlString.append(siteId);
            urlString.append("&routeDay=");
            urlString.append(routeDay);

        }else{
            Log.e("DM", "getRouteScheduleSummaryDataForDCSite error :Missing/Empty/Invalid argument siteId ");
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();

        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                if(error == null)
                {
                    try
                    {
                        JSONArray jsonArray = (JSONArray)object;
                        ArrayList<RouteShipmentSummary> routeShipmentSummaryArrayList = new ArrayList<RouteShipmentSummary>();

                        for(int i = 0; i < jsonArray.length(); i ++)
                        {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);

                            RouteShipmentSummary routeShipment = new RouteShipmentSummary();
                            routeShipment.readFromJSONObject(jsonObject);
                            routeShipmentSummaryArrayList.add(routeShipment);
                        }

                        listener.getRouteScheduleSummaryDataForDCSite(routeShipmentSummaryArrayList, null);
                    }
                    catch (JSONException e)
                    {
                        listener.getRouteScheduleSummaryDataForDCSite(null, new Error(e.getMessage()));
                    }

                }
                else
                {
                    listener.getRouteScheduleSummaryDataForDCSite(null, error);
                }
            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getRouteScheduleSummaryDataForDCSite");
    }



    public void getSiteListForUserId(String siteId, final OnCompleteListeners.getSiteListForUserIdListener listener ) {

        Log.v("DM", "Enter Function: getSiteListForUserId");

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/Site");

        if(isValidParameterString(siteId)) {
            urlString.append("?siteId=");
            urlString.append(siteId);

        }else{
            Log.e("DM", "getSiteListForUserId error :Missing/Empty/Invalid argument siteId ");
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();

        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                if(error == null)
                {
                    try
                    {
                        JSONArray jsonArray = (JSONArray)object;
                        ArrayList<Site> siteArrayList = new ArrayList<Site>();

                        for(int i = 0; i < jsonArray.length(); i ++)
                        {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);

                            Site site = new Site();
                            site.readFromJSONObject(jsonObject);
                            siteArrayList.add(site);
                        }

                        Log.v("SiteList", "JSONArray :"+ jsonArray);
                        listener.getSiteListForUserId(siteArrayList, null);
                    }
                    catch (JSONException e)
                    {
                        listener.getSiteListForUserId(null, new Error(e.getMessage()));
                    }

                }
                else
                {
                    listener.getSiteListForUserId(null, error);
                }
            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getSiteListForUserId");
    }

    public void getHOSEventsForUser(String userID, Date startDate, Date endDate, final OnCompleteListeners.getHOSEventsForUserListener listener ) {

        Log.v("DM", "Enter Function: getHOSEventsForUser");

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Driver/HOS2/HOSEvents_Get");

        if(isValidParameterString(userID)) {
            urlString.append("?userID=");
            urlString.append(userID);

        }else{
            Log.e("DM", "getHOSEventsForUser error :Missing/Empty/Invalid argument userID ");
        }

        if(startDate != null) {
            urlString.append("&startTime=");
            urlString.append(getUTCFormatDate(startDate));

        }else{
            Log.e("DM", "getHOSEventsForUser error :Missing/Empty/Invalid argument startDate ");
        }

        if(endDate != null) {
            urlString.append("&endTime=");
            urlString.append(getUTCFormatDate(endDate));

        }else{
            Log.e("DM", "getHOSEventsForUser error :Missing/Empty/Invalid argument endDate ");
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();

        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                if(error == null)
                {
                    try
                    {
                        JSONArray jsonArray = (JSONArray)object;
                        ArrayList<HOSEventDTO> hosEventDTOs = new ArrayList<HOSEventDTO>() ;

                        for(int i = 0; i < jsonArray.length(); i ++)
                        {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);

                            HOSEventDTO hosEventDTO = new HOSEventDTO();
                            hosEventDTO.readFromJSONObject(jsonObject);
                            hosEventDTOs.add(hosEventDTO);
                        }

                        listener.getHOSEventsForUser(hosEventDTOs, null);
                    }
                    catch (JSONException e)
                    {
                        listener.getHOSEventsForUser(null, new Error(e.getMessage()));
                    }

                }
                else
                {
                    listener.getHOSEventsForUser(null, error);
                }
            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getHOSEventsForUser");
    }

    public void calculate34HourResetTimeForUserId(String userID, String startDate, String endDate, final OnCompleteListeners.calculate34HourResetTimeForUserIdListener listener ) {

        Log.v("DM", "Enter Function: getHOSEventsForUser");

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Driver/HOS/HOSRecalc/");

        if(isValidParameterString(userID)) {
            urlString.append(userID);

        }else{
            Log.e("DM", "getHOSEventsForUser error :Missing/Empty/Invalid argument userID ");
        }

        if(startDate != null) {
            urlString.append("?start=");
//            urlString.append(getUTCFormatDate(startDate));
             urlString.append(startDate);

        }else{
            Log.e("DM", "getHOSEventsForUser error :Missing/Empty/Invalid argument startDate ");
        }

        if(endDate != null) {
            urlString.append("&end=");
            urlString.append(endDate);
//            urlString.append(getUTCFormatDate(endDate));

        }else{
            Log.e("DM", "getHOSEventsForUser error :Missing/Empty/Invalid argument endDate ");
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();

        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                if(error == null)
                {
                    try
                    {
                        JSONArray jsonArray = (JSONArray)object;
                        ArrayList<HOSActivity> hosActivityArrayList = new ArrayList<HOSActivity>() ;

                        for(int i = 0; i < jsonArray.length(); i ++)
                        {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);

                            HOSActivity hosActivity = new HOSActivity();
                            hosActivity.readFromJSONObject(jsonObject);
                            hosActivityArrayList.add(hosActivity);
                        }

                        listener.calculate34HourResetTimeForUserId(hosActivityArrayList, null);
                    }
                    catch (JSONException e)
                    {
                        listener.calculate34HourResetTimeForUserId(null, new Error(e.getMessage()));
                    }

                }
                else
                {
                    listener.calculate34HourResetTimeForUserId(null, error);
                }
            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getHOSEventsForUser");
    }

    public void getAllHOSEventsWithDateRangeStart(Date startDate, Date endDate, final OnCompleteListeners.getAllHOSEventsWithDateRangeStartListener listener ) {

        Log.v("DM", "Enter Function: getAllHOSEventsWithDateRangeStart");

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Driver/HOS2/HOSEvents_Get");

        if(startDate != null) {
            urlString.append("&startTime=");
            urlString.append(getUTCFormatDate(startDate));

        }else{
            Log.e("DM", "getAllHOSEventsWithDateRangeStart error :Missing/Empty/Invalid argument startDate ");
        }

        if(endDate != null) {
            urlString.append("&endTime=");
            urlString.append(getUTCFormatDate(endDate));

        }else{
            Log.e("DM", "getAllHOSEventsWithDateRangeStart error :Missing/Empty/Invalid argument endDate ");
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();

        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                if(error == null)
                {
                    try
                    {
                        JSONArray jsonArray = (JSONArray)object;
                        ArrayList<HOSEventDTO> hosEventDTOs = new ArrayList<HOSEventDTO>() ;

                        for(int i = 0; i < jsonArray.length(); i ++)
                        {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);

                            HOSEventDTO hosEventDTO = new HOSEventDTO();
                            hosEventDTO.readFromJSONObject(jsonObject);
                            hosEventDTOs.add(hosEventDTO);
                        }

                        listener.getAllHOSEventsWithDateRangeStart(hosEventDTOs, null);
                    }
                    catch (JSONException e)
                    {
                        listener.getAllHOSEventsWithDateRangeStart(null, new Error(e.getMessage()));
                    }

                }
                else
                {
                    listener.getAllHOSEventsWithDateRangeStart(null, error);
                }
            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getAllHOSEventsWithDateRangeStart");
    }

  public void getMessageListForRecipientUser(String recipientUserId, String senderUserId, PimmMessageStatusEnum.PimmMessagesStatusEnum status, int priority, Date startTime, Date endTime, final OnCompleteListeners.getMessageListForRecipientUserListener listener ) {

        Log.v("DM", "Enter Function: getMessageListForRecipientUser");

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Pimm/Message");

        String statusString = String.format("%ld", Long.parseLong(String.valueOf(status.getValue())));
        String priorityString = String.format("%d", priority);


        if(isValidParameterString(senderUserId)) {
            urlString.append("?senderUserId=");
            urlString.append(senderUserId);

        }else{
            Log.e("DM", "getMessageListForRecipientUser error :Missing/Empty/Invalid argument sitsenderUserIdeId ");
        }

        if(isValidParameterString(recipientUserId)) {
            urlString.append("&recipientUserId=");
            urlString.append(recipientUserId);

        }else{
            Log.e("DM", "getMessageListForRecipientUser error :Missing/Empty/Invalid argument recipientUserId ");
        }

        if(isValidParameterString(priorityString)) {
            urlString.append("&priority=");
            urlString.append(priorityString);

        }else{
            Log.e("DM", "getMessageListForRecipientUser error :Missing/Empty/Invalid argument priorityString ");
        }

        if(isValidParameterString(statusString)) {
            urlString.append("&status=");
            urlString.append(statusString);

        }else{
            Log.e("DM", "getMessageListForRecipientUser error :Missing/Empty/Invalid argument status ");
        }

        if(startTime != null && endTime != null){
            urlString.append("&start=");
            urlString.append(getUTCFormatDate(startTime));

            urlString.append("&end=");
            urlString.append(getUTCFormatDate(endTime));
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();

        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                if(error == null)
                {
                    try
                    {
                        JSONArray jsonArray = (JSONArray)object;
                        ArrayList<PimmMessage> pimmMessages = new ArrayList<PimmMessage>();

                        for(int i = 0; i < jsonArray.length(); i ++)
                        {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);

                            PimmMessage pimmMessage = new PimmMessage();
                            pimmMessage.readFromJSONObject(jsonObject);
                            pimmMessages.add(pimmMessage);
                        }

                        listener.getMessageListForRecipientUser(pimmMessages, null);
                    }
                    catch (JSONException e)
                    {
                        listener.getMessageListForRecipientUser(null, new Error(e.getMessage()));
                    }

                }
                else
                {
                    listener.getMessageListForRecipientUser(null, error);
                }
            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getMessageListForRecipientUser");
    }


  public void getMessageListForPimmObjectID(String recipientUserId, String senderUserId, PimmMessageStatusEnum.PimmMessagesStatusEnum status, int priority, Date startTime, Date endTime, final OnCompleteListeners.getMessageListForPimmObjectIDListener listener ) {

        Log.v("DM", "Enter Function: getMessageListForPimmObjectIDListener");

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Pimm/Message");

        String statusString = String.format("%ld", Long.parseLong(String.valueOf(status.getValue())));
        String priorityString = String.format("%d", priority);

      if(isValidParameterString(recipientUserId)) {
          urlString.append("?recipientUserId=");
          urlString.append(recipientUserId);

      }else{
          Log.e("DM", "getMessageListForRecipientUser error :Missing/Empty/Invalid argument recipientUserId ");
      }

        if(isValidParameterString(senderUserId)) {
            urlString.append("&senderUserId=");
            urlString.append(senderUserId);

        }else{
            Log.e("DM", "getMessageListForPimmObjectID error :Missing/Empty/Invalid argument sitsenderUserIdeId ");
        }



        if(isValidParameterString(priorityString)) {
            urlString.append("&priority=");
            urlString.append(priorityString);

        }else{
            Log.e("DM", "getMessageListForPimmObjectID error :Missing/Empty/Invalid argument priorityString ");
        }

        if(isValidParameterString(statusString)) {
            urlString.append("&status=");
            urlString.append(statusString);

        }else{
            Log.e("DM", "getMessageListForPimmObjectID error :Missing/Empty/Invalid argument status ");
        }

        if(startTime != null && endTime != null){
            urlString.append("&start=");
            urlString.append(getUTCFormatDate(startTime));

            urlString.append("&end=");
            urlString.append(getUTCFormatDate(endTime));
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();

        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                if(error == null)
                {
                    try
                    {
                        JSONArray jsonArray = (JSONArray)object;
                        ArrayList<PimmMessage> pimmMessages = new ArrayList<PimmMessage>();

                        for(int i = 0; i < jsonArray.length(); i ++)
                        {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);

                            PimmMessage pimmMessage = new PimmMessage();
                            pimmMessage.readFromJSONObject(jsonObject);
                            pimmMessages.add(pimmMessage);
                        }

                        listener.getMessageListForPimmObjectID(pimmMessages, null);
                    }
                    catch (JSONException e)
                    {
                        listener.getMessageListForPimmObjectID(null, new Error(e.getMessage()));
                    }

                }
                else
                {
                    listener.getMessageListForPimmObjectID(null, error);
                }
            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getMessageListForPimmObjectID");
    }

  public void getRouteSiteListForDCSiteId(String dcSiteId, String shipmentId, String  siteId, final OnCompleteListeners.getRouteSiteListForDCSiteIdListener listener ) {

            Log.v("DM", "Enter Function: getRouteSiteListForDCSiteId");

            StringBuilder urlString = new StringBuilder();
            urlString.append(this.baseUrl);
            urlString.append("StoreDelivery/RouteSite");


          if(isValidParameterString(dcSiteId)) {
              urlString.append("?dcSiteId=");
              urlString.append(dcSiteId);

          }else{
              Log.e("DM", "getRouteSiteListForDCSiteId error :Missing/Empty/Invalid argument dcSiteId ");
          }

            if(isValidParameterString(shipmentId)) {
                urlString.append("&shipmentId=");
                urlString.append(shipmentId);

            }else{
                Log.e("DM", "getRouteSiteListForDCSiteId error :Missing/Empty/Invalid argument shipmentId ");
            }



            if(isValidParameterString(siteId)) {
                urlString.append("&siteId=");
                urlString.append(siteId);

            }else{
                Log.e("DM", "getRouteSiteListForDCSiteId error :Missing/Empty/Invalid argument siteId ");
            }



            Log.v("DM", "Request URL: " + urlString);

            RestConnection restConnection = new RestConnection();

            restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
                @Override
                public void onRestConnectionComplete(Object object, Error error) {
                    if(error == null)
                    {
                        try
                        {
                            JSONArray jsonArray = (JSONArray)object;
                            ArrayList<RouteSite> routeSiteList = new ArrayList<RouteSite>();

                            for(int i = 0; i < jsonArray.length(); i ++)
                            {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);

                                RouteSite routeSite = new RouteSite();
                                routeSite.readFromJSONObject(jsonObject);
                                routeSiteList.add(routeSite);
                            }

                            listener.getRouteSiteListForDCSiteId(routeSiteList, null);
                        }
                        catch (JSONException e)
                        {
                            listener.getRouteSiteListForDCSiteId(null, new Error(e.getMessage()));
                        }

                    }
                    else
                    {
                        listener.getRouteSiteListForDCSiteId(null, error);
                    }
                }
            });

            restConnection.execute(String.valueOf(urlString));
            Log.v("DM", "ExitFunction: getRouteSiteListForDCSiteId");
    }


    public void updateFormWithFormId(String formId, Object formBody,  final OnCompleteListeners.updateFormWithFormIdListener listener ) {

        Log.v("DM", "Enter Function: updateFormWithFormId");
        final OnCompleteListeners.updateFormWithFormIdListener updateForm = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Pimm/Forms");


//        NSString *inputStr =  formBody;
//        //  NSData *data = [inputStr dataUsingEncoding: NSUnicodeStringEncoding];
//        NSData *data = [inputStr dataUsingEncoding: NSUTF16LittleEndianStringEncoding];
//
//        NSString *input = [data base64EncodedString];

        String input = Base64Converter.toBase64(formBody.toString());
        StringBuilder parameterString = new StringBuilder();

        if(isValidParameterString(formId)){
            parameterString.append("?formId=");
            parameterString.append(formId);
        }else{
            Log.e("DM", "updateFormWithFormId: Missing/Empty/Invalid Parameter formId");
        }
        if(isValidParameterString(input)){
            parameterString.append("&body=");
            parameterString.append(input);

        }else{
            Log.e("DM", "updateFormWithFormId: Missing/Empty/Invalid Parameter body");
        }
        Log.v("DM", "Create Form  API Parameters:: " + parameterString);

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initializePOSTRequest(this.authHeaderValue, parameterString.toString(), new OnCompleteListeners.OnRestConnectionCompleteListener(){
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for updateFormWithFormId: " + object.toString());
                        PimmForm result = new PimmForm();
                        JSONObject jsonObject = (JSONObject) object;
                        result.readFromJSONObject(jsonObject);
                        listener.updateFormWithFormId(result, null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.updateFormWithFormId(null, error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for updateFormWithFormId" + e.getMessage());
                    listener.updateFormWithFormId(null, error);
                }
            }
        });


        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: updateFormWithFormId");
    }



    ////////******* Route Shuttle APIs  ******////

    public void getRouteShuttleWithID(String routeShuttleID,  final OnCompleteListeners.getRouteShuttleWithIDListener listener ) {

        Log.v("DM", "Enter Function: updateFormWithFormId");
        final OnCompleteListeners.getRouteShuttleWithIDListener getRouteShuttle = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/RouteShuttle/Get");


        if(isValidParameterString(routeShuttleID)){
            urlString.append("?routeShuttleID=");
            urlString.append(routeShuttleID);
        }else{
            Log.e("DM", "updateFormWithFormId: Missing/Empty/Invalid Parameter routeShuttleID");
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener(){
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for updateFormWithFormId: " + object.toString());
                        RouteShuttleDTO result = new RouteShuttleDTO();
                        JSONObject jsonObject = (JSONObject) object;
                        result.readFromJSONObject(jsonObject);
                        listener.getRouteShuttleWithID(result, null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.getRouteShuttleWithID(null, error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for getRouteShuttleWithID" + e.getMessage());
                    listener.getRouteShuttleWithID(null, error);
                }
            }
        });


        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: updateFormWithFormId");
    }

    public void getRouteShuttleListForShipmentID(String shipmentID, final OnCompleteListeners.getRouteShuttleListForShipmentIDListener listener ) {

        Log.v("DM", "Enter Function: getRouteShuttleListForShipmentID");

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/RouteShuttle/GetForShipment");


        if(isValidParameterString(shipmentID)) {
            urlString.append("?shipmentID=");
            urlString.append(shipmentID);

        }else{
            Log.e("DM", "getRouteShuttleListForShipmentID error :Missing/Empty/Invalid argument shipmentID ");
        }


        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();

        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                if(error == null)
                {
                    try
                    {
                        JSONArray jsonArray = (JSONArray)object;
                        ArrayList<RouteShuttleDTO> routeShuttleList = new ArrayList<RouteShuttleDTO>();

                        for(int i = 0; i < jsonArray.length(); i ++)
                        {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);

                            RouteShuttleDTO routeShuttleDTO = new RouteShuttleDTO();
                            routeShuttleDTO.readFromJSONObject(jsonObject);
                            routeShuttleList.add(routeShuttleDTO);
                        }

                        listener.getRouteShuttleListForShipmentID(routeShuttleList, null);
                    }
                    catch (JSONException e)
                    {
                        listener.getRouteShuttleListForShipmentID(null, new Error(e.getMessage()));
                    }

                }
                else
                {
                    listener.getRouteShuttleListForShipmentID(null, error);
                }
            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getRouteShuttleListForShipmentID");
    }






    ////////******* Shuttle APIs  ******////

    public void getShuttleSiteListforLoggedInUserWithCallback(final OnCompleteListeners.getShuttleSiteListforLoggedInUserWithCallbackListener listener){
        Log.v("DM", "Enter Function: getShuttleSiteListforLoggedInUserWithCallback");

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Shuttle/ShuttleSite/GetSites");

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                if(error == null)
                {
                    try
                    {
                        JSONArray jsonArray = (JSONArray)object;
                        ArrayList<ShuttleSite> shuttleSiteArrayList = new ArrayList<ShuttleSite>();

                        for(int i = 0; i < jsonArray.length(); i ++)
                        {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);

                            ShuttleSite shuttleSite = new ShuttleSite();
                            shuttleSite.readFromJSONObject(jsonObject);
                            shuttleSiteArrayList.add(shuttleSite);
                        }

                        listener.getShuttleSiteListforLoggedInUserWithCallback(shuttleSiteArrayList, null);
                    }
                    catch (JSONException e)
                    {
                        listener.getShuttleSiteListforLoggedInUserWithCallback(null, new Error(e.getMessage()));
                    }

                }
                else
                {
                    listener.getShuttleSiteListforLoggedInUserWithCallback(null, error);
                }
            }
        });


        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getShuttleSiteListforLoggedInUserWithCallback");

    }

  ////////***** HOS Editing Related APIS ********////////


    public void addHOSEventDueToEditByDriver(HOSEventDTO newHOSEvent, String originalHOSEventId, final OnCompleteListeners.addHOSEventDueToEditByDriverListener listener ){

        Log.v("DM", "Enter Function: addHOSEventDueToEditByDriver");
        final OnCompleteListeners.addHOSEventDueToEditByDriverListener setManualDeliveryStart = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Driver/HOS2/HOSEvent_EditByDriver");

        JSONObject jsonObjFB = new JSONObject(newHOSEvent.dictionaryWithValuesForKeys());
        String jsonString = jsonObjFB.toString();
        String input = Base64Converter.toBase64(jsonString);


        StringBuilder parameterString = new StringBuilder();

        parameterString.append("hosEventDTO=");
        parameterString.append(jsonString);

        parameterString.append("&originalHOSEventId=");
        parameterString.append(originalHOSEventId);

        Log.v("DM", "Create Form  API Parameters: " + parameterString);

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initializePOSTRequest(this.authHeaderValue, parameterString.toString(),  new OnCompleteListeners.OnRestConnectionCompleteListener(){
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for setManualDeliveryStartForDeliveryId: " + object.toString());
                        HOSEventDTO result = new HOSEventDTO();
                        JSONObject jsonObject = (JSONObject) object;
                        result.readFromJSONObject(jsonObject);
                        listener.addHOSEventDueToEditByDriver(result , null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.addHOSEventDueToEditByDriver(null, error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for addHOSEventDueToEditByDriver" + e.getMessage());
                    listener.addHOSEventDueToEditByDriver(null, error);

                }
            }

        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: addHOSEventDueToEditByDriver");

    }

    public void addHOSEventDueToEditByManager(HOSEventDTO newHOSEvent, String originalHOSEventId, final OnCompleteListeners.addHOSEventDueToEditByManagerListener listener ){

        Log.v("DM", "Enter Function: addHOSEventDueToEditByManager");
        final OnCompleteListeners.addHOSEventDueToEditByManagerListener setManualDeliveryStart = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Driver/HOS2/HOSEvent_EditByManager");

        JSONObject jsonObjFB = new JSONObject(newHOSEvent.dictionaryWithValuesForKeys());
        String jsonString = jsonObjFB.toString();
        String input = Base64Converter.toBase64(jsonString);


        StringBuilder parameterString = new StringBuilder();

        parameterString.append("hosEventDTO=");
        parameterString.append(jsonString);

        parameterString.append("&originalHOSEventId=");
        parameterString.append(originalHOSEventId);

        Log.v("DM", "Create Form  API Parameters: " + parameterString);

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initializePOSTRequest(this.authHeaderValue, parameterString.toString(),  new OnCompleteListeners.OnRestConnectionCompleteListener(){
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for setManualDeliveryStartForDeliveryId: " + object.toString());
                        HOSEventDTO result = new HOSEventDTO();
                        JSONObject jsonObject = (JSONObject) object;
                        result.readFromJSONObject(jsonObject);
                        listener.addHOSEventDueToEditByManager(result , null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.addHOSEventDueToEditByManager(null, error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for addHOSEventDueToEditByManager" + e.getMessage());
                    listener.addHOSEventDueToEditByManager(null, error);

                }
            }

        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: addHOSEventDueToEditByManager");

    }

    public void deleteHOSEventByDriverWithHOSEventId( String hosEventId, final OnCompleteListeners.deleteHOSEventByDriverWithHOSEventIdListener listener ){

        Log.v("DM", "Enter Function: addHOSEventDueToEditByManager");
        final OnCompleteListeners.deleteHOSEventByDriverWithHOSEventIdListener deleteHOSEventByDriverWithHOSEventId = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Driver/HOS2/HOSEvent_DeleteByDriver");

        StringBuilder parameterString = new StringBuilder();

        parameterString.append("hosEventId=");
        parameterString.append(hosEventId);

        Log.v("DM", "Create Form  API Parameters: " + parameterString);

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initializePOSTRequest(this.authHeaderValue, parameterString.toString(),  new OnCompleteListeners.OnRestConnectionCompleteListener(){
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for deleteHOSEventByDriverWithHOSEventId: " + object.toString());
                        HOSEventDTO result = new HOSEventDTO();
                        JSONObject jsonObject = (JSONObject) object;
                        result.readFromJSONObject(jsonObject);
                        listener.deleteHOSEventByDriverWithHOSEventId(result , null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.deleteHOSEventByDriverWithHOSEventId(null, error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for deleteHOSEventByDriverWithHOSEventId" + e.getMessage());
                    listener.deleteHOSEventByDriverWithHOSEventId(null, error);

                }
            }

        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: addHOSEventDueToEditByManager");

    }

    public void deleteHOSEventByManagerWithHOSEventId( String hosEventId, final OnCompleteListeners.deleteHOSEventByManagerWithHOSEventIdListener listener ){

        Log.v("DM", "Enter Function: addHOSEventDueToEditByManager");
        final OnCompleteListeners.deleteHOSEventByManagerWithHOSEventIdListener deleteHOSEventByDriverWithHOSEventId = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Driver/HOS2/HOSEvent_DeleteByManager");

        StringBuilder parameterString = new StringBuilder();

        parameterString.append("hosEventId=");
        parameterString.append(hosEventId);

        Log.v("DM", "Create Form  API Parameters: " + parameterString);

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initializePOSTRequest(this.authHeaderValue, parameterString.toString(),  new OnCompleteListeners.OnRestConnectionCompleteListener(){
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for deleteHOSEventByDriverWithHOSEventId: " + object.toString());
                        HOSEventDTO result = new HOSEventDTO();
                        JSONObject jsonObject = (JSONObject) object;
                        result.readFromJSONObject(jsonObject);
                        listener.deleteHOSEventByManagerWithHOSEventId(result , null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.deleteHOSEventByManagerWithHOSEventId(null, error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for deleteHOSEventByDriverWithHOSEventId" + e.getMessage());
                    listener.deleteHOSEventByManagerWithHOSEventId(null, error);

                }
            }

        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: addHOSEventDueToEditByManager");

    }

    public void getPendingRequestsforDriver(String driverUserId, final OnCompleteListeners.getPendingRequestsforDriverListener listener){
        Log.v("DM", "Enter Function: getPendingRequestsforDriver");

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Driver/HOS2/HOSEvent_GetPendingRequestsForDriver");

        if(isValidParameterString(driverUserId)) {

            urlString.append("?userId=");
            urlString.append(driverUserId);
        }else{
            Log.e("DM", "Missing/Empty/Invalid argument driverUserId: " + driverUserId);
        }
        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                if(error == null)
                {
                    try
                    {
                        JSONArray jsonArray = (JSONArray)object;
                        ArrayList<HOSEventDTO> hosEventDTOArrayList = new ArrayList<HOSEventDTO>();

                        for(int i = 0; i < jsonArray.length(); i ++)
                        {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);

                            HOSEventDTO hosEventDTO = new HOSEventDTO();
                            hosEventDTO.readFromJSONObject(jsonObject);
                            hosEventDTOArrayList.add(hosEventDTO);
                        }

                        listener.getPendingRequestsforDriver(hosEventDTOArrayList, null);
                    }
                    catch (JSONException e)
                    {
                        listener.getPendingRequestsforDriver(null, new Error(e.getMessage()));
                    }

                }
                else
                {
                    listener.getPendingRequestsforDriver(null, error);
                }
            }
        });


        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getPendingRequestsforDriver");

    }

    public void getPendingRequestsforAdmin(String adminUserId, final OnCompleteListeners.getPendingRequestsforAdminListener listener){
        Log.v("DM", "Enter Function: getPendingRequestsforAdmin");

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Driver/HOS2/HOSEvent_GetPendingRequestsForAdmin");

        if(isValidParameterString(adminUserId)) {

            urlString.append("?userId=");
            urlString.append(adminUserId);
        }else{
            Log.e("DM", "Missing/Empty/Invalid argument adminUserId: " + adminUserId);
        }
        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                if(error == null)
                {
                    try
                    {
                        JSONArray jsonArray = (JSONArray)object;
                        ArrayList<HOSEventDTO> hosEventDTOArrayList = new ArrayList<HOSEventDTO>();

                        for(int i = 0; i < jsonArray.length(); i ++)
                        {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);

                            HOSEventDTO hosEventDTO = new HOSEventDTO();
                            hosEventDTO.readFromJSONObject(jsonObject);
                            hosEventDTOArrayList.add(hosEventDTO);
                        }

                        listener.getPendingRequestsforAdmin(hosEventDTOArrayList, null);
                    }
                    catch (JSONException e)
                    {
                        listener.getPendingRequestsforAdmin(null, new Error(e.getMessage()));
                    }

                }
                else
                {
                    listener.getPendingRequestsforAdmin(null, error);
                }
            }
        });


        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getPendingRequestsforAdmin");

    }




    public void updateHOSEventDueToEditApprovedByDriver(String hosEventId, Date timestampUTC, final OnCompleteListeners.updateHOSEventDueToEditApprovedByDriverListener listener ){

        Log.v("DM", "Enter Function: updateHOSEventDueToEditApprovedByDriver");
        final OnCompleteListeners.updateHOSEventDueToEditApprovedByDriverListener updateHOSEventDueToEditApprovedByDriverListener = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Driver/HOS2/HOSEvent_ApproveByDriver");

        StringBuilder parameterString = new StringBuilder();

        parameterString.append("requestHOSEventId=");
        parameterString.append(hosEventId);

        parameterString.append("&timestampUTC=");
        parameterString.append(getUTCFormatDate(timestampUTC));

        Log.v("DM", "Create Form  API Parameters: " + parameterString);

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initializePOSTRequest(this.authHeaderValue, parameterString.toString(),  new OnCompleteListeners.OnRestConnectionCompleteListener(){
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for setManualDeliveryStartForDeliveryId: " + object.toString());
                        HOSEventDTO result = new HOSEventDTO();
                        JSONObject jsonObject = (JSONObject) object;
                        result.readFromJSONObject(jsonObject);
                        listener.updateHOSEventDueToEditApprovedByDriver(result , null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.updateHOSEventDueToEditApprovedByDriver(null, error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for updateHOSEventDueToEditApprovedByDriver" + e.getMessage());
                    listener.updateHOSEventDueToEditApprovedByDriver(null, error);

                }
            }

        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: updateHOSEventDueToEditApprovedByDriver");

    }


    public void updateHOSEventDueToEditRejectedByDriver(String hosEventId, Date timestampUTC, final OnCompleteListeners.updateHOSEventDueToEditRejectedByDriverListener listener ){

        Log.v("DM", "Enter Function: updateHOSEventDueToEditApprovedByDriver");
        final OnCompleteListeners.updateHOSEventDueToEditRejectedByDriverListener updateHOSEventDueToEditRejectedByDriverListener = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Driver/HOS2/HOSEvent_RejectByDriver");

        StringBuilder parameterString = new StringBuilder();

        parameterString.append("requestHOSEventId=");
        parameterString.append(hosEventId);

        parameterString.append("&timestampUTC=");
        parameterString.append(getUTCFormatDate(timestampUTC));

        Log.v("DM", "Create Form  API Parameters: " + parameterString);

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initializePOSTRequest(this.authHeaderValue, parameterString.toString(),  new OnCompleteListeners.OnRestConnectionCompleteListener(){
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for setManualDeliveryStartForDeliveryId: " + object.toString());
                        HOSEventDTO result = new HOSEventDTO();
                        JSONObject jsonObject = (JSONObject) object;
                        result.readFromJSONObject(jsonObject);
                        listener.updateHOSEventDueToEditRejectedByDriver(result , null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.updateHOSEventDueToEditRejectedByDriver(null, error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for updateHOSEventDueToEditRejectedByDriver" + e.getMessage());
                    listener.updateHOSEventDueToEditRejectedByDriver(null, error);

                }
            }

        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: updateHOSEventDueToEditRejectedByDriver");

    }


//////////*********  Driver Performance APIs **********//////////////

//APIS currently used for Saving PreTrip/PostTrip Time in TMS


    public void addDriverPerformanceEntryForUser(String userId, String driverPerformanceId, Date timestamp, String shipmentId, Date preTripStart, Date preTripEnd, Date postTripStart, Date postTripEnd, final OnCompleteListeners.addDriverPerformanceEntryForUserListener listener ){

        Log.v("DM", "Enter Function: addDriverPerformanceEntryForUser");
        final OnCompleteListeners.addDriverPerformanceEntryForUserListener addDriverPerformanceEntryForUser = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Driver/Performance/Add");

        StringBuilder parameterString = new StringBuilder();

        parameterString.append("userId=");
        parameterString.append(userId);

        parameterString.append("&driverPerformanceId=");
        parameterString.append(driverPerformanceId);

        parameterString.append("&timestamp=");
        parameterString.append(getUTCFormatDate(timestamp));

        if(preTripStart != null){

            parameterString.append("&preTripStart=");
            parameterString.append(getUTCFormatDate(preTripStart));

        }

        if(preTripEnd != null){

            parameterString.append("&preTripEnd=");
            parameterString.append(getUTCFormatDate(preTripEnd));

        }

        if(postTripStart != null){

            parameterString.append("&postTripStart=");
            parameterString.append(getUTCFormatDate(postTripStart));

        }


        if(postTripEnd != null){

            parameterString.append("&postTripEnd=");
            parameterString.append(getUTCFormatDate(postTripEnd));

        }

        Log.v("DM", "Create Form  API Parameters: " + parameterString);

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initializePOSTRequest(this.authHeaderValue, parameterString.toString(),  new OnCompleteListeners.OnRestConnectionCompleteListener(){
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for setManualDeliveryStartForDeliveryId: " + object.toString());
                        DriverPerformanceDTO result = new DriverPerformanceDTO();
                        JSONObject jsonObject = (JSONObject) object;
                        result.readFromJSONObject(jsonObject);
                        listener.addDriverPerformanceEntryForUser(result , null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.addDriverPerformanceEntryForUser(null, error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for addDriverPerformanceEntryForUser" + e.getMessage());
                    listener.addDriverPerformanceEntryForUser(null, error);

                }
            }

        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: addDriverPerformanceEntryForUser");

    }



    public void getDriverPerformanceListForUser(String userId, Date startTime, Date endTime, final OnCompleteListeners.getDriverPerformanceListForUserListener listener){
        Log.v("DM", "Enter Function: getPendingRequestsforAdmin");

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Driver/Performance/Get");

//        StringBuilder parameterString = new StringBuilder();

        if(isValidParameterString(userId)) {

            urlString.append("?userId=");
            urlString.append(userId);
        }else{
            Log.e("DM", "Missing/Empty/Invalid argument adminUserId: " + userId);
        }

        if(startTime != null){

            urlString.append("&startTime=");
            urlString.append(getUTCFormatDate(startTime));

        }

        if(endTime != null){

            urlString.append("&endTime=");
            urlString.append(getUTCFormatDate(endTime));

        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                if(error == null)
                {
                    try
                    {
                        JSONArray jsonArray = (JSONArray)object;
                        ArrayList<DriverPerformanceDTO> driverPerformanceDTOArrayList = new ArrayList<DriverPerformanceDTO>();

                        for(int i = 0; i < jsonArray.length(); i ++)
                        {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);

                            DriverPerformanceDTO driverPerformanceDTO = new DriverPerformanceDTO();
                            driverPerformanceDTO.readFromJSONObject(jsonObject);
                            driverPerformanceDTOArrayList.add(driverPerformanceDTO);
                        }

                        listener.getDriverPerformanceListForUser(driverPerformanceDTOArrayList, null);
                    }
                    catch (JSONException e)
                    {
                        listener.getDriverPerformanceListForUser(null, new Error(e.getMessage()));
                    }

                }
                else
                {
                    listener.getDriverPerformanceListForUser(null, error);
                }
            }
        });


        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getDriverPerformanceListForUser");

    }



    public void getDriverPerformanceListForSite(String siteId, Date startTime, Date endTime, final OnCompleteListeners.getDriverPerformanceListForSiteListener listener){
        Log.v("DM", "Enter Function: getPendingRequestsforAdmin");

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Driver/Performance/GetForSite");

//        StringBuilder parameterString = new StringBuilder();

        if(isValidParameterString(siteId)) {

            urlString.append("?siteId=");
            urlString.append(siteId);
        }else{
            Log.e("DM", "Missing/Empty/Invalid argument siteId: " + siteId);
        }

        if(startTime != null){

            urlString.append("&startTime=");
            urlString.append(getUTCFormatDate(startTime));

        }else{
            Log.e("DM", "Missing/Empty/Invalid start time");
        }

        if(endTime != null){

            urlString.append("&endTime=");
            urlString.append(getUTCFormatDate(endTime));

        }else{
            Log.e("DM", "Missing/Empty/Invalid end time");
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                if(error == null)
                {
                    try
                    {
                        JSONArray jsonArray = (JSONArray)object;
                        ArrayList<DriverPerformanceDTO> driverPerformanceDTOArrayList = new ArrayList<DriverPerformanceDTO>();

                        for(int i = 0; i < jsonArray.length(); i ++)
                        {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);

                            DriverPerformanceDTO driverPerformanceDTO = new DriverPerformanceDTO();
                            driverPerformanceDTO.readFromJSONObject(jsonObject);
                            driverPerformanceDTOArrayList.add(driverPerformanceDTO);
                        }

                        listener.getDriverPerformanceListForSite(driverPerformanceDTOArrayList, null);
                    }
                    catch (JSONException e)
                    {
                        listener.getDriverPerformanceListForSite(null, new Error(e.getMessage()));
                    }

                }
                else
                {
                    listener.getDriverPerformanceListForSite(null, error);
                }
            }
        });


        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getDriverPerformanceListForSite");

    }



    public void updateOutboundDCDriverPermission(OutboundDCDriverPermission driverPermission, final OnCompleteListeners.updateOutboundDCDriverPermissionListener listener){

        Log.v("DM", "Enter Function: updateOutboundDCDriverPermission");
        final OnCompleteListeners.updateOutboundDCDriverPermissionListener outboundDCDriverPermission = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Driver/OutboundDCDriverPermission/Update");

        StringBuilder parameterString = new StringBuilder();

        if(isValidParameterString(driverPermission.userID)) {

            parameterString.append("userId=");
            parameterString.append(driverPermission.userID);
        }else{
            Log.e("DM", "Missing/Empty/Invalid argument userID: " + driverPermission.userID);
        }

        if(driverPermission.storeRoutes){
            parameterString.append("&storeRoutes=1");
        }else{
            parameterString.append("&storeRoutes=0");
        }

        if(driverPermission.shuttleTrailer){
            parameterString.append("&shuttleTrailer=1");
        }else{
            parameterString.append("&shuttleTrailer=0");
        }

        if(driverPermission.shuttleContainer){
            parameterString.append("&shuttleContainer=1");
        }else{
            parameterString.append("&shuttleContainer=0");
        }

        if(driverPermission.adHoc){
            parameterString.append("&adHoc=1");
        }else{
            parameterString.append("&adHoc=0");
        }

        if(driverPermission.yardMove){
            parameterString.append("&yardMove=1");
        }else{
            parameterString.append("&yardMove=0");
        }

        if(driverPermission.personal){
            parameterString.append("&personal=1");
        }else{
            parameterString.append("&personal=0");
        }

        if(driverPermission.onDutySchedule != null){
            parameterString.append("&onDutySchedule=1");
        }else{
            parameterString.append("&onDutySchedule=0");
        }

        if(driverPermission.hosExemptionStatus != null){
            parameterString.append("&hosExemptionStatus=");
            parameterString.append(driverPermission.hosExemptionStatus);
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initializePOSTRequest(this.authHeaderValue, parameterString.toString(),  new OnCompleteListeners.OnRestConnectionCompleteListener(){
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {https://scontent.fmnl8-1.fna.fbcdn.net/v/t1.0-9/1379677_1439256632961446_1175342385_n.jpg?oh=da390a9c5c086e644e8b57127761f223&oe=5A677BB1
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for OutboundDCDriverPermission: " + object.toString());
                        OutboundDCDriverPermission result = new OutboundDCDriverPermission();
                        JSONObject jsonObject = (JSONObject) object;
                        result.readFromJSONObject(jsonObject);
                        listener.updateOutboundDCDriverPermission(result , null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.updateOutboundDCDriverPermission(null, error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for updateOutboundDCDriverPermission" + e.getMessage());
                    listener.updateOutboundDCDriverPermission(null, error);

                }
            }

        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: updateOutboundDCDriverPermission");

    }

    public void updateOutboundDCDriverLicense(OutboundDCDriverLicense driverLicense, final OnCompleteListeners.updateOutboundDCDriverLicenseListener listener ){

        Log.v("DM", "Enter Function: updateOutboundDCDriverLicense");
        final OnCompleteListeners.updateOutboundDCDriverLicenseListener updateOutboundDCDriverLicenseListener = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Driver/OutboundDCDriverLicense/Update");

        StringBuilder parameterString = new StringBuilder();

        if(isValidParameterString(driverLicense.userId)) {

            parameterString.append("userId=");
            parameterString.append(driverLicense.userId);
        }else{
            Log.e("DM", "Missing/Empty/Invalid argument userID: " + driverLicense.userId);
        }

        if(driverLicense.outboundDCDriverLicenseId != null) {
            parameterString.append("&outboundDCDriverLicenseId=");
            parameterString.append(driverLicense.outboundDCDriverLicenseId);
        }

        parameterString.append("&licenseType=");
        parameterString.append(driverLicense.licenseType);

        parameterString.append("&licenseNumber=");
        parameterString.append(driverLicense.licenseNumber);

        parameterString.append("&issuer=");
        parameterString.append(driverLicense.issuer);

        parameterString.append("&issueDate=");
        parameterString.append(driverLicense.issueDate);

        parameterString.append("&expirationDate=");
        parameterString.append(driverLicense.expirationDate);

        parameterString.append("&status=");
        parameterString.append(driverLicense.status);


        Log.v("DM", "Create Form  API Parameters: " + parameterString);

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initializePOSTRequest(this.authHeaderValue, parameterString.toString(),  new OnCompleteListeners.OnRestConnectionCompleteListener(){
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for updateOutboundDCDriverLicense: " + object.toString());
                        OutboundDCDriverLicense result = new OutboundDCDriverLicense();
                        JSONObject jsonObject = (JSONObject) object;
                        result.readFromJSONObject(jsonObject);
                        listener.updateOutboundDCDriverLicense(result , null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.updateOutboundDCDriverLicense(null, error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for updateOutboundDCDriverLicense" + e.getMessage());
                    listener.updateOutboundDCDriverLicense(null, error);

                }
            }

        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: updateOutboundDCDriverLicense");

    }

    public void addMultipleHOSEventsDueToEditByDriver(ArrayList<HOSEventDTO> hosEventList, ArrayList<HOSEventDTO> originalHOSEventIdList, final OnCompleteListeners.addMultipleHOSEventsDueToEditByDriverListener listener){
        Log.v("DM", "Enter Function: addMultipleHOSEventsDueToEditByDriver");

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Driver/HOS2/HOSEvent_EditByDriver_Multiple");

        StringBuilder parameterString = new StringBuilder();


       // JSONObject jsonObjFB = new JSONObject(hosEventList);

        if(hosEventList.size() > 0 && hosEventList != null) {
            String hosEventJSONString = hosEventList.toString();
            if(hosEventJSONString != null) {
                parameterString.append("hosEventDTOList=");
                parameterString.append(hosEventJSONString);
            }else{
                Log.e("DM", "Failed to convert hosEventList to JSON String");
            }
        }else{
            Log.e("DM", "Missing/Empty/Invalid argument hosEventList");
        }

       if(originalHOSEventIdList.size() > 0 && originalHOSEventIdList != null) {
            String originalEventJSONString = originalHOSEventIdList.toString();
            if(originalEventJSONString != null) {
                parameterString.append("originalHOSEventIdList=");
                parameterString.append(originalEventJSONString);
            }else{
                Log.e("DM", "Failed to convert originalHOSEventIdList to JSON String");
            }
        }else{
            Log.e("DM", "Missing/Empty/Invalid argument originalHOSEventIdList");
        }


        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initializePOSTRequest(this.authHeaderValue, parameterString.toString(), new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                if(error == null)
                {
                    try
                    {
                        JSONArray jsonArray = (JSONArray)object;
                        ArrayList<HOSEventDTO> hosEventDTOArrayList = new ArrayList<HOSEventDTO>();

                        for(int i = 0; i < jsonArray.length(); i ++)
                        {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);

                            HOSEventDTO hosEventDTO = new HOSEventDTO();
                            hosEventDTO.readFromJSONObject(jsonObject);
                            hosEventDTOArrayList.add(hosEventDTO);
                        }

                        listener.addMultipleHOSEventsDueToEditByDriver(hosEventDTOArrayList, null);
                    }
                    catch (JSONException e)
                    {
                        listener.addMultipleHOSEventsDueToEditByDriver(null, new Error(e.getMessage()));
                    }

                }
                else
                {
                    listener.addMultipleHOSEventsDueToEditByDriver(null, error);
                }
            }
        });


        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: addMultipleHOSEventsDueToEditByDriver");

    }
//// HOS Certification API

    public void updateHOSCertificationForDriver(String driverUserId, Date date, int status , final OnCompleteListeners.updateHOSCertificationForDriverListener listener ){

        Log.v("DM", "Enter Function: updateHOSCertificationForDriver");
        final OnCompleteListeners.updateHOSCertificationForDriverListener updateHOSCertificationForDriver = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Driver/HOS2/HOSCertification_Update");

        StringBuilder parameterString = new StringBuilder();

        if(isValidParameterString(driverUserId)) {

            parameterString.append("userId=");
            parameterString.append(driverUserId);
        }else{
            Log.e("DM", "Missing/Empty/Invalid argument userID: " + driverUserId);
        }


        parameterString.append("&date=");
        parameterString.append(getUTCFormatDate(date));

        parameterString.append("&status=");
        parameterString.append(String.format("ld", status));

        Log.v("DM", "Create Form  API Parameters: " + parameterString);

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initializePOSTRequest(this.authHeaderValue, parameterString.toString(),  new OnCompleteListeners.OnRestConnectionCompleteListener(){
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for updateHOSCertificationForDriver: " + object.toString());
                        listener.updateHOSCertificationForDriver(null);
                    }else if (object == null && error == null){
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for updateHOSCertificationForDriver: " + object.toString());
                        listener.updateHOSCertificationForDriver(null);

                    } else {
                        Log.e("DM", "Null object ");
                        listener.updateHOSCertificationForDriver(error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for updateHOSCertificationForDriver" + e.getMessage());
                    listener.updateHOSCertificationForDriver(error);

                }
            }

        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: updateHOSCertificationForDriver");

    }


    // HOSShippingDocument and HOSDriverVehicle API


    public void addHOSShippingDocumentWithId(String hosShippingDocumentId, String driverUserId , Date startTime, Date endTime, String documentType, String documentName, final OnCompleteListeners.addHOSShippingDocumentWithIdListener listener ){

        Log.v("DM", "Enter Function: addHOSShippingDocumentWithId");
        final OnCompleteListeners.addHOSShippingDocumentWithIdListener addHOSShippingDocumentWithId = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Driver/HOS2/HOSShippingDocument_Add");

        StringBuilder parameterString = new StringBuilder();


        parameterString.append("hosShippingDocumentId=");
        parameterString.append(hosShippingDocumentId);


        parameterString.append("&driverUserId=");
        parameterString.append(driverUserId);

        parameterString.append("&startTimeUTC=");
        parameterString.append(getUTCFormatDate(startTime));
        if(endTime != null) {
            parameterString.append("&endTimeUTC=");
            parameterString.append(endTime);
        }

        parameterString.append("&documentType=");
        parameterString.append(documentType);

        parameterString.append("&documentName=");
        parameterString.append(documentName);


        Log.v("DM", "Create Form  API Parameters: " + parameterString);

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initializePOSTRequest(this.authHeaderValue, parameterString.toString(),  new OnCompleteListeners.OnRestConnectionCompleteListener(){
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for addHOSShippingDocumentWithId: " + object.toString());
                        HOSShippingDocumentDTO result = new HOSShippingDocumentDTO();
                        JSONObject jsonObject = (JSONObject) object;
                        result.readFromJSONObject(jsonObject);
                        listener.addHOSShippingDocumentWithId(result , null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.addHOSShippingDocumentWithId(null, error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for addHOSShippingDocumentWithId" + e.getMessage());
                    listener.addHOSShippingDocumentWithId(null, error);

                }
            }

        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: addHOSShippingDocumentWithId");

    }

    public void updateHOSShippingDocumentWithId(String hosShippingDocumentId, Date startTime, Date endTime, String documentType, String documentName , final OnCompleteListeners.updateHOSShippingDocumentWithIdListener listener ){

        Log.v("DM", "Enter Function: updateHOSCertificationForDriver");
        final OnCompleteListeners.updateHOSShippingDocumentWithIdListener updateHOSCertificationForDriver = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Driver/HOS2/HOSShippingDocument_Update");

        StringBuilder parameterString = new StringBuilder();


        parameterString.append("hosShippingDocumentId=");
        parameterString.append(hosShippingDocumentId);

         if(startTime != null) {
             parameterString.append("&startTimeUTC=");
             parameterString.append(getUTCFormatDate(startTime));
         }
        if(endTime != null) {
            parameterString.append("&endTimeUTC=");
            parameterString.append(endTime);
        }

        if(isValidParameterString(documentType)) {
            parameterString.append("&documentType=");
            parameterString.append(documentType);
        }

        if(isValidParameterString(documentName)) {
            parameterString.append("&documentName=");
            parameterString.append(documentName);
        }

        Log.v("DM", "Create Form  API Parameters: " + parameterString);

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initializePOSTRequest(this.authHeaderValue, parameterString.toString(),  new OnCompleteListeners.OnRestConnectionCompleteListener(){
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for updateHOSShippingDocumentWithId: " + object.toString());
                        listener.updateHOSShippingDocumentWithId(null);
                    }else if (object == null && error == null){
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for updateHOSShippingDocumentWithId: " + object.toString());
                        listener.updateHOSShippingDocumentWithId(null);

                    } else {
                        Log.e("DM", "Null object ");
                        listener.updateHOSShippingDocumentWithId(error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for updateHOSShippingDocumentWithId" + e.getMessage());
                    listener.updateHOSShippingDocumentWithId(error);

                }
            }

        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: updateHOSShippingDocumentWithId");

    }


    public void addHOSDriverVehicleWithId(String hosDriverVehicleId, String driverUserId , Date startTime, Date endTime, String truckDeviceId, String truckName, String trailer1DeviceId, String trailer1Name, String trailer2DeviceId, String trailer2Name, final OnCompleteListeners.addHOSDriverVehicleWithIdListener listener ) {

        Log.v("DM", "Enter Function: addHOSDriverVehicleWithId");
        final OnCompleteListeners.addHOSDriverVehicleWithIdListener addHOSDriverVehicleWithIdListener = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Driver/HOS2/HOSDriverVehicle_Add");

        StringBuilder parameterString = new StringBuilder();

        if (isValidParameterString(hosDriverVehicleId)) {
            parameterString.append("hosDriverVehicleId=");
            parameterString.append(hosDriverVehicleId);
        }

        if (isValidParameterString(driverUserId)) {

            parameterString.append("&driverUserId=");
            parameterString.append(driverUserId);
        }
        if (startTime != null){
            parameterString.append("&startTimeUTC=");
            parameterString.append(getUTCFormatDate(startTime));
        }
        if(endTime != null) {
            parameterString.append("&endTimeUTC=");
            parameterString.append(endTime);
        }

        if (isValidParameterString(truckDeviceId)) {

            parameterString.append("&truckDeviceId=");
            parameterString.append(truckDeviceId);
        }

        if (isValidParameterString(truckName)) {

            parameterString.append("&truckName=");
            parameterString.append(truckName);
        }

        if (isValidParameterString(trailer1Name)) {

            parameterString.append("&trailer1Name=");
            parameterString.append(trailer1Name);
        }


        if (isValidParameterString(trailer1DeviceId)) {

            parameterString.append("&trailer1DeviceId=");
            parameterString.append(trailer1DeviceId);

        }

        if (isValidParameterString(trailer2Name)) {

            parameterString.append("&trailer2Name=");
            parameterString.append(trailer2Name);

        }

        if (isValidParameterString(trailer2DeviceId)) {

            parameterString.append("&trailer2DeviceId=");
            parameterString.append(trailer2DeviceId);

        }

        Log.v("DM", "Create Form  API Parameters: " + parameterString);

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initializePOSTRequest(this.authHeaderValue, parameterString.toString(),  new OnCompleteListeners.OnRestConnectionCompleteListener(){
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for addHOSDriverVehicleWithId: " + object.toString());
                        HOSShippingDocumentDTO.HOSDriverVehicleDTO result = new HOSShippingDocumentDTO.HOSDriverVehicleDTO();
                        JSONObject jsonObject = (JSONObject) object;
                        result.readFromJSONObject(jsonObject);
                        listener.addHOSDriverVehicleWithId(result , null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.addHOSDriverVehicleWithId(null, error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for addHOSDriverVehicleWithId" + e.getMessage());
                    listener.addHOSDriverVehicleWithId(null, error);

                }
            }

        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: addHOSDriverVehicleWithId");

    }

    public void updateHOSDriverVehicleWithId(String hosDriverVehicleId, Date startTime, Date endTime, final OnCompleteListeners.updateHOSDriverVehicleWithIdListener listener ){

        Log.v("DM", "Enter Function: updateHOSDriverVehicleWithId");
        final OnCompleteListeners.updateHOSDriverVehicleWithIdListener updateHOSDriverVehicleWithIdListener = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Driver/HOS2/HOSDriverVehicle_Update");

        StringBuilder parameterString = new StringBuilder();


        parameterString.append("hosDriverVehicleId=");
        parameterString.append(hosDriverVehicleId);

        if(startTime != null) {
            parameterString.append("&startTimeUTC=");
            parameterString.append(getUTCFormatDate(startTime));
        }
        if(endTime != null) {
            parameterString.append("&endTimeUTC=");
            parameterString.append(getUTCFormatDate(endTime));
        }

        Log.v("DM", "Create Form  API Parameters: " + parameterString);

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initializePOSTRequest(this.authHeaderValue, parameterString.toString(),  new OnCompleteListeners.OnRestConnectionCompleteListener(){
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for updateHOSDriverVehicleWithId: " + object.toString());
                        listener.updateHOSDriverVehicleWithId(null);
                    }else if (object == null && error == null){
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for updateHOSDriverVehicleWithId: " + object.toString());
                        listener.updateHOSDriverVehicleWithId(null);

                    } else {
                        Log.e("DM", "Null object ");
                        listener.updateHOSDriverVehicleWithId(error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for updateHOSDriverVehicleWithId" + e.getMessage());
                    listener.updateHOSDriverVehicleWithId(error);

                }
            }

        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: updateHOSDriverVehicleWithId");

    }

    public void getUnassignedHOSEventsForSite(String siteId, String tractorDeviceId, Date startDate, Date endDate , final OnCompleteListeners.getUnassignedHOSEventsForSiteListener listener){
        Log.v("DM", "Enter Function: getUnassignedHOSEventsForSite");

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Driver/HOS2/HOSEvents_GetUnassigned");

        urlString.append("?siteId=");
        urlString.append(siteId);

         if(isValidParameterString(tractorDeviceId)) {
             urlString.append("&tractorDeviceId=");
             urlString.append(tractorDeviceId);
        }else{
            Log.e("DM", "Missing/Empty/Invalid tractorDeviceId");
        }

        if(startDate != null ) {

             urlString.append("&startUTC=");
             urlString.append(getUTCFormatDate(startDate));

            if(endDate != null){
                urlString.append("&endUTC=");
                urlString.append(getUTCFormatDate(endDate));

            }

        }else{
            Log.e("DM", "Missing/Empty/Invalid tractorDeviceId");
        }


        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue,  new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                if(error == null)
                {
                    try
                    {
                        JSONArray jsonArray = (JSONArray)object;
                        ArrayList<HOSEventDTO> hosEventDTOArrayList = new ArrayList<HOSEventDTO>();

                        for(int i = 0; i < jsonArray.length(); i ++)
                        {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);

                            HOSEventDTO hosEventDTO = new HOSEventDTO();
                            hosEventDTO.readFromJSONObject(jsonObject);
                            hosEventDTOArrayList.add(hosEventDTO);
                        }

                        listener.getUnassignedHOSEventsForSite(hosEventDTOArrayList, null);
                    }
                    catch (JSONException e)
                    {
                        listener.getUnassignedHOSEventsForSite(null, new Error(e.getMessage()));
                    }

                }
                else
                {
                    listener.getUnassignedHOSEventsForSite(null, error);
                }
            }
        });


        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getUnassignedHOSEventsForSite");

    }
    public void getPOIListForShipmentID(String shipmentID, final OnCompleteListeners.getPOIListForShipmentIDListener listener){
        Log.v("DM", "Enter Function: getPOIListForShipmentID");

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("SupplierDelivery/Shipment/GetShipmentPOIs");

        urlString.append("?shipmentID=");
        urlString.append(shipmentID);



        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue,  new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                if(error == null)
                {
                    try
                    {
                        JSONArray jsonArray = (JSONArray)object;
                        ArrayList<SdrPointOfInterest> resultArrayList = new ArrayList<SdrPointOfInterest>();

                        for(int i = 0; i < jsonArray.length(); i ++)
                        {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);

                            SdrPointOfInterest result = new SdrPointOfInterest();
                            result.readFromJSONObject(jsonObject);
                            resultArrayList.add(result);
                        }

                        listener.getPOIListForShipmentID(resultArrayList, null);
                    }
                    catch (JSONException e)
                    {
                        listener.getPOIListForShipmentID(null, new Error(e.getMessage()));
                    }

                }
                else
                {
                    listener.getPOIListForShipmentID(null, error);
                }
            }
        });


        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getPOIListForShipmentID");

    }






    /****** HOS API *****/
    public void getHOSSummaryForUser(String userId , Date date, final OnCompleteListeners.getHOSSummaryForUserListener listener ){

        Log.v("DM", "Enter Function: getHOSSummaryForUser");
        final OnCompleteListeners.getHOSSummaryForUserListener getHOSSummaryForUser = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Driver/HOS2/HOSSummary_GetForUser");

        urlString.append("?driverUserId=");
        urlString.append(userId);

        urlString.append("&endTimeUTC=");
        urlString.append(getUTCFormatDate(date));


        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue,   new OnCompleteListeners.OnRestConnectionCompleteListener(){
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for getHOSSummaryForUser: " + object.toString());
                        HOSSummary result = new HOSSummary();
                        JSONObject jsonObject = (JSONObject) object;
                        result.readFromJSONObject(jsonObject);
                        listener.getHOSSummaryForUser(result , null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.getHOSSummaryForUser(null, error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for getHOSSummaryForUser" + e.getMessage());
                    listener.getHOSSummaryForUser(null, error);

                }
            }

        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getHOSSummaryForUser");

    }

    public void getHOSSummaryForSite(String siteId , Date date, final OnCompleteListeners.getHOSSummaryForSiteListener listener ){

        Log.v("DM", "Enter Function: getHOSSummaryForSite");
        final OnCompleteListeners.getHOSSummaryForSiteListener getHOSSummaryForSite = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Driver/HOS2/HOSSummary_GetForSite");

        urlString.append("?siteId=");
        urlString.append(siteId);

        urlString.append("&endTimeUTC=");
        urlString.append(getUTCFormatDate(date));


        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue,  new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                if(error == null)
                {
                    try
                    {
                        JSONArray jsonArray = (JSONArray)object;
                        ArrayList<HOSSummary> hosSummaries = new ArrayList<HOSSummary>();

                        for(int i = 0; i < jsonArray.length(); i ++)
                        {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);

                            HOSSummary result = new HOSSummary();
                            result.readFromJSONObject(jsonObject);
                            hosSummaries.add(result);
                        }

                        listener.getHOSSummaryForSite(hosSummaries, null);
                    }
                    catch (JSONException e)
                    {
                        listener.getHOSSummaryForSite(null, new Error(e.getMessage()));
                    }

                }
                else
                {
                    listener.getHOSSummaryForSite(null, error);
                }
            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getHOSSummaryForSite");

    }

    public void addCoDriverWithHosId(String hosCoDriverId , String coDriverUserId, String driverUserId, Date startTime,  final OnCompleteListeners.addCoDriverWithHosIdListener listener ){

        Log.v("DM", "Enter Function: getHOSSummaryForUser");
        final OnCompleteListeners.addCoDriverWithHosIdListener addCoDriverWithHosIds = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Driver/HOS2/HOSCoDriver_Add");

        StringBuilder parameterString = new StringBuilder();


        parameterString.append("hosCoDriverId=");
        parameterString.append(hosCoDriverId);
        parameterString.append("&driverUserId=");
        parameterString.append(driverUserId);
        parameterString.append("&startTimeUTC=");
        parameterString.append(getUTCFormatDate(startTime));
        parameterString.append("&coDriverId=");
        parameterString.append(coDriverUserId);


        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initializePOSTRequest(this.authHeaderValue, parameterString.toString(),  new OnCompleteListeners.OnRestConnectionCompleteListener(){
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for addCoDriverWithHosId: " + object.toString());
                        HOSShippingDocumentDTO.HOSCoDriverDTO result = new HOSShippingDocumentDTO.HOSCoDriverDTO();
                        JSONObject jsonObject = (JSONObject) object;
                        result.readFromJSONObject(jsonObject);
                        listener.addCoDriverWithHosId(result , null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.addCoDriverWithHosId(null, error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for addCoDriverWithHosId" + e.getMessage());
                    listener.addCoDriverWithHosId(null, error);

                }
            }

        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: addCoDriverWithHosId");

    }
    public void updateCoDriverWithHosId(String hosCoDriverId, Date endTime, final OnCompleteListeners.updateCoDriverWithHosIdListener listener ){

        Log.v("DM", "Enter Function: updateHOSDriverVehicleWithId");
        final OnCompleteListeners.updateCoDriverWithHosIdListener updateCoDriverWithHosIds  = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Driver/HOS2/HOSCoDriver_Update");

        StringBuilder parameterString = new StringBuilder();


        parameterString.append("hosCoDriverId=");
        parameterString.append(hosCoDriverId);


        if(endTime != null) {
            parameterString.append("&endTimeUTC=");
            parameterString.append(getUTCFormatDate(endTime));
        }

        Log.v("DM", "Create Form  API Parameters: " + parameterString);

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initializePOSTRequest(this.authHeaderValue, parameterString.toString(),  new OnCompleteListeners.OnRestConnectionCompleteListener(){
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for updateCoDriverWithHosId: " + object.toString());
                        listener.updateCoDriverWithHosId(null);
                    }else if (object == null && error == null){
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for updateCoDriverWithHosId: " + object.toString());
                        listener.updateCoDriverWithHosId(null);

                    } else {
                        Log.e("DM", "Null object ");
                        listener.updateCoDriverWithHosId(error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for updateCoDriverWithHosId" + e.getMessage());
                    listener.updateCoDriverWithHosId(error);

                }
            }

        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: updateHOSDriverVehicleWithId");

    }



    public void getRouteShuttleListForDriverID(String driverUserID, Date startDate, Date endDate , final OnCompleteListeners.getRouteShuttleListForDriverIDListener listener){
        Log.v("DM", "Enter Function: getRouteShuttleListForDriverID");

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("StoreDelivery/RouteShuttle/GetForDriver");

        if(isValidParameterString(driverUserID)) {
            urlString.append("?driverUserID=");
            urlString.append(driverUserID);
        }

        if(startDate != null) {
            urlString.append("&startDate=");
            urlString.append(getUTCFormatDate(startDate));
        }


        if(endDate != null) {
            urlString.append("&endDate=");
            urlString.append(getUTCFormatDate(endDate));
        }


        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue,  new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                if(error == null)
                {
                    try
                    {
                        JSONArray jsonArray = (JSONArray)object;
                        ArrayList<RouteShuttleDTO> resultArrayList = new ArrayList<RouteShuttleDTO>();

                        for(int i = 0; i < jsonArray.length(); i ++)
                        {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);

                            RouteShuttleDTO result = new RouteShuttleDTO();
                            result.readFromJSONObject(jsonObject);
                            resultArrayList.add(result);
                        }

                        listener.getRouteShuttleListForDriverID(resultArrayList, null);
                    }
                    catch (JSONException e)
                    {
                        listener.getRouteShuttleListForDriverID(null, new Error(e.getMessage()));
                    }

                }
                else
                {
                    listener.getRouteShuttleListForDriverID(null, error);
                }
            }
        });


        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getRouteShuttleListForDriverID");

    }


    //region HOS_VIOLATIONS_BEGIN

    public void getHOSViolationWithId(String hosViolationId, final OnCompleteListeners.getHOSViolationWithIdListener listener ){

        Log.v("DM", "Enter Function: getHOSViolationWithId");
        final OnCompleteListeners.getHOSViolationWithIdListener hosViolationWithId = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Driver/HOSViolation/Get");

        urlString.append("?hosViolationId=");
        urlString.append(hosViolationId);


        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue,   new OnCompleteListeners.OnRestConnectionCompleteListener(){
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for getHOSSummaryForUser: " + object.toString());
                        HOSViolation result = new HOSViolation();
                        JSONObject jsonObject = (JSONObject) object;
                        result.readFromJSONObject(jsonObject);
                        listener.getHOSViolationWithId(result , null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.getHOSViolationWithId(null, error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for getHOSViolationWithId" + e.getMessage());
                    listener.getHOSViolationWithId(null, error);

                }
            }

        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getHOSViolationWithId");

    }


    public void getHOSViolationsForDriverId(String driverUserID, Date startTimeUTC, Date endTimeUTC , final OnCompleteListeners.getHOSViolationsForDriverIdListener listener){
        Log.v("DM", "Enter Function: getRouteShuttleListForDriverID");

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Driver/HOSViolation/GetForUser");

        if(isValidParameterString(driverUserID)) {
            urlString.append("?driverUserID=");
            urlString.append(driverUserID);
        }

        if(startTimeUTC != null) {
            urlString.append("&startTimeUTC=");
            urlString.append(getUTCFormatDate(startTimeUTC));
        }


        if(endTimeUTC != null) {
            urlString.append("&endTimeUTC=");
            urlString.append(getUTCFormatDate(endTimeUTC));
        }


        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue,  new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                if(error == null)
                {
                    try
                    {
                        JSONArray jsonArray = (JSONArray)object;
                        ArrayList<HOSViolation> resultArrayList = new ArrayList<HOSViolation>();

                        for(int i = 0; i < jsonArray.length(); i ++)
                        {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);

                            HOSViolation result = new HOSViolation();
                            result.readFromJSONObject(jsonObject);
                            resultArrayList.add(result);
                        }

                        listener.getHOSViolationsForDriverId(resultArrayList, null);
                    }
                    catch (JSONException e)
                    {
                        listener.getHOSViolationsForDriverId(null, new Error(e.getMessage()));
                    }

                }
                else
                {
                    listener.getHOSViolationsForDriverId(null, error);
                }
            }
        });


        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getHOSViolationsForDriverId");

    }

    //public JsonResult GetForSite(Guid siteId, DateTime startTimeUTC, DateTime endTimeUTC)

    public void getHOSViolationsForSiteId(String siteId, Date startTimeUTC, Date endTimeUTC , final OnCompleteListeners.getHOSViolationsForSiteIdListener listener){
        Log.v("DM", "Enter Function: getHOSViolationsForSiteId");

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Driver/HOSViolation/GetForSite");

        if(isValidParameterString(siteId)) {
            urlString.append("?siteId=");
            urlString.append(siteId);
        }

        if(startTimeUTC != null) {
            urlString.append("&startTimeUTC=");
            urlString.append(getUTCFormatDate(startTimeUTC));
        }


        if(endTimeUTC != null) {
            urlString.append("&endTimeUTC=");
            urlString.append(getUTCFormatDate(endTimeUTC));
        }


        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue,  new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                if(error == null)
                {
                    try
                    {
                        JSONArray jsonArray = (JSONArray)object;
                        ArrayList<HOSViolation> resultArrayList = new ArrayList<HOSViolation>();

                        for(int i = 0; i < jsonArray.length(); i ++)
                        {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);

                            HOSViolation result = new HOSViolation();
                            result.readFromJSONObject(jsonObject);
                            resultArrayList.add(result);
                        }

                        listener.getHOSViolationsForSiteId(resultArrayList, null);
                    }
                    catch (JSONException e)
                    {
                        listener.getHOSViolationsForSiteId(null, new Error(e.getMessage()));
                    }

                }
                else
                {
                    listener.getHOSViolationsForSiteId(null, error);
                }
            }
        });


        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getHOSViolationsForDriverId");

    }


    public void createHOSViolationWithId(String hosViolationId , String driverUserId, String deliveryId, HOSViolationTypeEnum.HOSViolationTypesEnum violationType, Date violationStartTime, final OnCompleteListeners.createHOSViolationWithIdListener listener ){

        Log.v("DM", "Enter Function: getHOSSummaryForUser");
        final OnCompleteListeners.createHOSViolationWithIdListener createHOSViolationWithIdListener = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Driver/HOSViolation/Create");

        StringBuilder parameterString = new StringBuilder();


        if(isValidParameterString(driverUserId)) {
            parameterString.append("driverUserId=");
            parameterString.append(driverUserId);
        }

        if(isValidParameterString(deliveryId)) {
            parameterString.append("&deliveryId=");
            parameterString.append(deliveryId);
        }

        if(isValidParameterString(hosViolationId)) {
            parameterString.append("&hosViolationId=");
            parameterString.append(hosViolationId);
        }

        parameterString.append("&violationType=");
        parameterString.append(String.format("%ld", (long)violationType.getValue()));


        if(violationStartTime != null){
            parameterString.append("&violationTimeUTC=");
            parameterString.append(getUTCFormatDate(violationStartTime));
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initializePOSTRequest(this.authHeaderValue, parameterString.toString(),  new OnCompleteListeners.OnRestConnectionCompleteListener(){
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for addCoDriverWithHosId: " + object.toString());
                        HOSViolation result = new HOSViolation();
                        JSONObject jsonObject = (JSONObject) object;
                        result.readFromJSONObject(jsonObject);
                        listener.createHOSViolationWithId(result, null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.createHOSViolationWithId(null, error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for createHOSViolationWithId" + e.getMessage());
                    listener.createHOSViolationWithId(null, error);

                }
            }

        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: createHOSViolationWithId");

    }

    public void updateHOSViolationWithId(String hosViolationId ,  HOSViolationTypeEnum.HOSViolationStatus violationStatus, Date updateTimeUTC, final OnCompleteListeners.updateHOSViolationWithIdListener listener ){

        Log.v("DM", "Enter Function: updateHOSViolationWithId");
        final OnCompleteListeners.updateHOSViolationWithIdListener updateHOSViolationWithId = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Driver/HOSViolation/Update");

        StringBuilder parameterString = new StringBuilder();


        if(isValidParameterString(hosViolationId)) {
            parameterString.append("hosViolationId=");
            parameterString.append(hosViolationId);
        }

        parameterString.append("&status=");
        parameterString.append(String.format("%ld", (long)violationStatus.getValue()));


        if(updateTimeUTC != null){
            parameterString.append("&updateTimeUTC=");
            parameterString.append(getUTCFormatDate(updateTimeUTC));
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initializePOSTRequest(this.authHeaderValue, parameterString.toString(),  new OnCompleteListeners.OnRestConnectionCompleteListener(){
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for addCoDriverWithHosId: " + object.toString());
                        HOSViolation result = new HOSViolation();
                        JSONObject jsonObject = (JSONObject) object;
                        result.readFromJSONObject(jsonObject);
                        listener.updateHOSViolationWithId(result, null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.updateHOSViolationWithId(null, error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for updateHOSViolationWithId" + e.getMessage());
                    listener.updateHOSViolationWithId(null, error);

                }
            }

        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: updateHOSViolationWithId");

    }

    //endregion HOS_VIOLATION_END

    //region HOS_ALERTS_BEGIN

    public void getHOSAlertWithId(String hosAlertId, final OnCompleteListeners.getHOSAlertWithIdListener listener ){

        Log.v("DM", "Enter Function: getHOSAlertWithId");
        final OnCompleteListeners.getHOSAlertWithIdListener getHOSAlertWithId = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Driver/HOSAlert/Get");

        urlString.append("?hosAlertId=");
        urlString.append(hosAlertId);


        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue,   new OnCompleteListeners.OnRestConnectionCompleteListener(){
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for getHOSSummaryForUser: " + object.toString());
                        HOSAlert result = new HOSAlert();
                        JSONObject jsonObject = (JSONObject) object;
                        result.readFromJSONObject(jsonObject);
                        listener.getHOSAlertWithId(result , null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.getHOSAlertWithId(null, error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for getHOSAlertWithId" + e.getMessage());
                    listener.getHOSAlertWithId(null, error);

                }
            }

        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getHOSAlertWithId");

    }


    public void getHOSAlertsForDriverId(String driverUserID, Date startTimeUTC, Date endTimeUTC , final OnCompleteListeners.getHOSAlertsForDriverIdListener listener){
        Log.v("DM", "Enter Function: getRouteShuttleListForDriverID");

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Driver/HOSAlert/GetForUser");

        if(isValidParameterString(driverUserID)) {
            urlString.append("?driverUserID=");
            urlString.append(driverUserID);
        }

        if(startTimeUTC != null) {
            urlString.append("&startTimeUTC=");
            urlString.append(getUTCFormatDate(startTimeUTC));
        }


        if(endTimeUTC != null) {
            urlString.append("&endTimeUTC=");
            urlString.append(getUTCFormatDate(endTimeUTC));
        }


        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue,  new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                if(error == null)
                {
                    try
                    {
                        JSONArray jsonArray = (JSONArray)object;
                        ArrayList<HOSAlert> resultArrayList = new ArrayList<HOSAlert>();

                        for(int i = 0; i < jsonArray.length(); i ++)
                        {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);

                            HOSAlert result = new HOSAlert();
                            result.readFromJSONObject(jsonObject);
                            resultArrayList.add(result);
                        }

                        listener.getHOSAlertsForDriverId(resultArrayList, null);
                    }
                    catch (JSONException e)
                    {
                        listener.getHOSAlertsForDriverId(null, new Error(e.getMessage()));
                    }

                }
                else
                {
                    listener.getHOSAlertsForDriverId(null, error);
                }
            }
        });


        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getHOSViolationsForDriverId");

    }

    public void getHOSAlertsForSiteId(String siteId, Date startTimeUTC, Date endTimeUTC , final OnCompleteListeners.getHOSAlertsForSiteIdListener listener){
        Log.v("DM", "Enter Function: getRouteShuttleListForDriverID");

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Driver/HOSAlert/GetForSite");

        if(isValidParameterString(siteId)) {
            urlString.append("?siteId=");
            urlString.append(siteId);
        }

        if(startTimeUTC != null) {
            urlString.append("&startTimeUTC=");
            urlString.append(getUTCFormatDate(startTimeUTC));
        }


        if(endTimeUTC != null) {
            urlString.append("&endTimeUTC=");
            urlString.append(getUTCFormatDate(endTimeUTC));
        }


        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue,  new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                if(error == null)
                {
                    try
                    {
                        JSONArray jsonArray = (JSONArray)object;
                        ArrayList<HOSAlert> resultArrayList = new ArrayList<HOSAlert>();

                        for(int i = 0; i < jsonArray.length(); i ++)
                        {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);

                            HOSAlert result = new HOSAlert();
                            result.readFromJSONObject(jsonObject);
                            resultArrayList.add(result);
                        }

                        listener.getHOSAlertsForSiteId(resultArrayList, null);
                    }
                    catch (JSONException e)
                    {
                        listener.getHOSAlertsForSiteId(null, new Error(e.getMessage()));
                    }

                }
                else
                {
                    listener.getHOSAlertsForSiteId(null, error);
                }
            }
        });


        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getHOSViolationsForDriverId");

    }

    public void createHOSAlertForDriverId(String driverUserId, String hosAlertId, String deliveryId, HOSViolationTypeEnum.HOSViolationTypesEnum violationType, int severity, Date alertStartTimeUTC, Date alertEndTimeUTC, final OnCompleteListeners.onCreateHOSAlertForDriverIdListener listener) {

        Log.v("DM", "Enter Function: createHOSAlertForDriverId");
        final OnCompleteListeners.onCreateHOSAlertForDriverIdListener createHOSAlert = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Driver/HOSAlert/Create");


        StringBuilder parameterString = new StringBuilder();


        if (isValidParameterString(driverUserId)) {
            parameterString.append("driverUserId=");
            parameterString.append(driverUserId);
        } else {
            Log.e("DM", "createHOSAlertForDriverId: driverUserId parameter is invalid/missing,throwing exception");
        }
        if (isValidParameterString(hosAlertId)) {
            parameterString.append("&hosAlertId=");
            parameterString.append(hosAlertId);
        } else {
            Log.e("DM", "createHOSAlertForDriverId: hosAlertId parameter is invalid/missing,throwing exception");
        }

        if (violationType != null) {
            parameterString.append("&violationType=");
            parameterString.append(String.format("%d", violationType.getValue()));
        }

        if(alertStartTimeUTC != null) {
            parameterString.append("&alertStartTimeUTC=");
            parameterString.append(getUTCFormatDate(alertStartTimeUTC));
        }

        if(alertEndTimeUTC != null){
            parameterString.append("&alertEndTimeUTC=");
            parameterString.append(getUTCFormatDate(alertEndTimeUTC));
        }
        parameterString.append("&severity=");
        parameterString.append(Long.parseLong("%ld", severity));

        if (isValidParameterString(deliveryId)) {
            parameterString.append("deliveryId=");
            parameterString.append(deliveryId);
        } else {
            Log.e("DM", "createHOSAlertForDriverId: deliveryId parameter is invalid/missing,throwing exception");
        }

        Log.v("DM", "Create Form  API Parameters:: " + parameterString);

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initializePOSTRequest(this.authHeaderValue, parameterString.toString(), new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for createHOSAlertForDriverId: " + object.toString());
                        HOSAlert results = new HOSAlert();
                        JSONObject jsonObject = (JSONObject) object;
                        results.readFromJSONObject(jsonObject);
                        listener.createHOSAlertForDriverId(results, null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.createHOSAlertForDriverId(null, error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for createHOSAlertForDriverId" + e.getMessage());
                    listener.createHOSAlertForDriverId(null, error);

                }
            }
        });


        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: createHOSAlertForDriverId");
    }


    public void updateHOSAlertWithId(String hosAlertId ,  String deliveryId, int severity, Date escalateTime1UTC, Date escalateTime2UTC, Date alertEndTimeUTC, final OnCompleteListeners.updateHOSAlertWithIdListener listener ){

        Log.v("DM", "Enter Function: updateHOSViolationWithId");
        final OnCompleteListeners.updateHOSAlertWithIdListener updateHOSAlertWithId = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Driver/HOSAlert/Update");

        StringBuilder parameterString = new StringBuilder();


        if(isValidParameterString(hosAlertId)) {
            parameterString.append("hosAlertId=");
            parameterString.append(hosAlertId);
        }

        if(isValidParameterString(deliveryId)) {
            parameterString.append("deliveryId=");
            parameterString.append(deliveryId);
        }

        parameterString.append("&severity=");
        parameterString.append(String.format("%d", severity));


        if(escalateTime1UTC != null){
            parameterString.append("&escalateTime1UTC=");
            parameterString.append(getUTCFormatDate(escalateTime1UTC));
        }
        if(escalateTime2UTC != null){
            parameterString.append("&escalateTime2UTC=");
            parameterString.append(getUTCFormatDate(escalateTime2UTC));
        }
        if(alertEndTimeUTC != null){
            parameterString.append("&alertEndTimeUTC=");
            parameterString.append(getUTCFormatDate(alertEndTimeUTC));
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initializePOSTRequest(this.authHeaderValue, parameterString.toString(),  new OnCompleteListeners.OnRestConnectionCompleteListener(){
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for updateHOSAlertWithId: " + object.toString());
                        HOSAlert result = new HOSAlert();
                        JSONObject jsonObject = (JSONObject) object;
                        result.readFromJSONObject(jsonObject);
                        listener.updateHOSAlertWithId(result, null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.updateHOSAlertWithId(null, error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for updateHOSAlertWithId" + e.getMessage());
                    listener.updateHOSAlertWithId(null, error);

                }
            }

        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: updateHOSAlertWithId");

    }


    public void escalateHOSAlertWithId(String hosAlertId, int severity, Date timeUTC,  final OnCompleteListeners.escalateHOSAlertWithIdListener listener) {

        Log.v("DM", "Enter Function: escalateHOSAlertWithId");
        final OnCompleteListeners.escalateHOSAlertWithIdListener escalateHOSAlertWithId = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Driver/HOSAlert/Escalate");

        StringBuilder parameterString = new StringBuilder();


        if (isValidParameterString(hosAlertId)) {
            parameterString.append("hosAlertId=");
            parameterString.append(hosAlertId);
        } else {
            Log.e("DM", "escalateHOSAlertWithId: hosAlertId is Nil/Empty");
        }

        if(timeUTC != null){
            parameterString.append("&timeUTC=");
            parameterString.append(getUTCFormatDate(timeUTC));
        }

        parameterString.append("&severity=");
        parameterString.append(String.format("%ld", severity));

        Log.v("DM", "Create Form  API Parameters:: " + parameterString);

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initializePOSTRequest(this.authHeaderValue, parameterString.toString(), new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for escalateHOSAlertWithId: " + object.toString());
                        listener.escalateHOSAlertWithId(null);
                    } else if (object == null && error == null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for escalateHOSAlertWithId: " + object.toString());
                        listener.escalateHOSAlertWithId(null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.escalateHOSAlertWithId(error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for escalateHOSAlertWithId" + e.getMessage());
                    listener.escalateHOSAlertWithId(error);

                }

            }

        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: escalateHOSAlertWithId");
    }

    public void cancelHOSAlertWithId(String hosAlertId, Date timeUTC,  final OnCompleteListeners.cancelHOSAlertWithIdListener listener) {

        Log.v("DM", "Enter Function: cancelHOSAlertWithId");
        final OnCompleteListeners.cancelHOSAlertWithIdListener cancelHOSAlertWithId = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Driver/HOSAlert/Cancel");

        StringBuilder parameterString = new StringBuilder();


        if (isValidParameterString(hosAlertId)) {
            parameterString.append("hosAlertId=");
            parameterString.append(hosAlertId);
        } else {
            Log.e("DM", "cancelHOSAlertWithId: hosAlertId is Nil/Empty");
        }

        if(timeUTC != null){
            parameterString.append("&timeUTC=");
            parameterString.append(getUTCFormatDate(timeUTC));
        }


        Log.v("DM", "Create Form  API Parameters:: " + parameterString);

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initializePOSTRequest(this.authHeaderValue, parameterString.toString(), new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for cancelHOSAlertWithId: " + object.toString());
                        listener.cancelHOSAlertWithId(null);
                    } else if (object == null && error == null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for cancelHOSAlertWithId: " + object.toString());
                        listener.cancelHOSAlertWithId(null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.cancelHOSAlertWithId(error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for cancelHOSAlertWithId" + e.getMessage());
                    listener.cancelHOSAlertWithId(error);

                }

            }

        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: cancelHOSAlertWithId");
    }


    public void deleteHOSAlertWithId(String hosAlertId,  final OnCompleteListeners.deleteHOSAlertWithIdListener listener) {

        Log.v("DM", "Enter Function: deleteHOSAlertWithId");
        final OnCompleteListeners.deleteHOSAlertWithIdListener deleteHOSAlertWithIdListener = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Driver/HOSAlert/Delete");

        StringBuilder parameterString = new StringBuilder();


        if (isValidParameterString(hosAlertId)) {
            parameterString.append("hosAlertId=");
            parameterString.append(hosAlertId);
        } else {
            Log.e("DM", "deleteHOSAlertWithId: hosAlertId is Nil/Empty");
        }



        Log.v("DM", "Create Form  API Parameters:: " + parameterString);

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initializePOSTRequest(this.authHeaderValue, parameterString.toString(), new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for deleteHOSAlertWithId: " + object.toString());
                        listener.deleteHOSAlertWithId(null);
                    } else if (object == null && error == null) {
                        new JSONObject(String.valueOf(object));
                        Log.v("DM", "Received JSON Object for deleteHOSAlertWithId: " + object.toString());
                        listener.deleteHOSAlertWithId(null);
                    } else {
                        Log.e("DM", "Null object ");
                        listener.deleteHOSAlertWithId(error);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("DM", "JSONException for deleteHOSAlertWithId" + e.getMessage());
                    listener.deleteHOSAlertWithId(error);

                }

            }

        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: deleteHOSAlertWithId");
    }


    //endregion HOS_ALERTS_End

    //New APIs Start

    //SMS Dashboard Start

    public void getSMSDashboardScoresForSite(String siteID, final OnCompleteListeners.getSMSDashboardWithCallbackListener listener){
        Log.v("SMS Dashboard Score", "Enter Function: getSMSDashboardScoresForSite");
        final OnCompleteListeners.getSMSDashboardWithCallbackListener getSMSDashboardWithCallbackListener = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("SMS/SMSDashboard/GetScores");
        urlString.append("?siteId=");
        urlString.append(siteID);

        Log.v("SMS Dashboard Score", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null){
                        JSONObject jsonObject = (JSONObject) object;
                        System.out.println("SMS Dashboard Score: "+jsonObject.toString());
                        Log.v("SMS Dashboard Score", "Received JSON Object " + object.toString());
                        SMSDashboard smsDashboard = new SMSDashboard();
                        smsDashboard.readFromJSONObject(jsonObject);
                        listener.getSMSDashboardWithCallback(smsDashboard, null);
                    } else {
                        Log.e("SMS Dashboard Score", "Null object");
                        listener.getSMSDashboardWithCallback(null, error);
                    }
                } catch (Exception e){
                    e.printStackTrace();
                    Log.e("SMS Dashboard Score", "JSONException for getSMSDashboardScoresForSite: " + e.getMessage());
                    listener.getSMSDashboardWithCallback(null, error);
                }

            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("SMS Dashboard Score", "ExitFunction: getSMSDashboardScoresForSite");
    }

    public void getSMSDashboardCustomerCommentsForSite(String siteId, final OnCompleteListeners.getSMSDashboardCustomerCommentsWithCallbackListener listener){

        Log.v("SMS Dashboard Comments", "Enter Function: getSMSDashboardCustomerCommentsForSite");
        final OnCompleteListeners.getSMSDashboardCustomerCommentsWithCallbackListener getSMSDashboardCustomerCommentsWithCallbackListener = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("SMS/SMSDashboard/GetCustomerComments");
        urlString.append("?siteId=");
        urlString.append(siteId);

        Log.v("SMS Dashboard Comments", "Request URL: "+urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null){
                        JSONObject jsonObject = (JSONObject) object;
                        Log.v("SMS Dashboard Comments", "Recieved JSON Object: " + object.toString());
                        SMSDashboardCustomerComments smsDashboardCustomerComments = new SMSDashboardCustomerComments();
                        smsDashboardCustomerComments.readFromJSONObject(jsonObject);
                        listener.getSMSDashboardCustomerCommentsWithCallback(smsDashboardCustomerComments, null);
                    } else {
                        Log.e("SMS Dashboard Comments", "Null Object");
                        listener.getSMSDashboardCustomerCommentsWithCallback(null, error);
                    }
                } catch (Exception e){
                    e.printStackTrace();
                    Log.e("SMS Dashboard Comments", "JSONException for getSMSDashboardCustomerCommentsWithCallback: " + e.getMessage() );
                    listener.getSMSDashboardCustomerCommentsWithCallback(null, error);
                }
            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("SMS Dashboard Comments", "ExitFunction: getSMSDashboardCustomerCommentsWithCallback");

    }

    public void getLatestFormForReferenceId(String refId, String formDefinitionId, final OnCompleteListeners.getLatestFormForReferenceIdCallbackListener listener) {

        Log.v("PimmForm", "Enter Function: getLatestFormForReferenceId");
//        https://wendys.pimm.us/WebPimm5/Rest/PimmForm/Form/Latest?appId=c89d8b58-e89b-4aa5-ab78-84524df56898&refx=96070e0c-cfd0-471c-af95-2d937166f3cb&formDefinitionId=331d2c58-dcc4-4013-a9f3-8b4b73f96466
        final OnCompleteListeners.getLatestFormForReferenceIdCallbackListener getLatestForm = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("PimmForm/Form/Latest");
        urlString.append("?appId=");
        urlString.append(ApplicationID);
        urlString.append("&refx=");
        urlString.append(refId);
        urlString.append("&formDefinitionId=");
        urlString.append(formDefinitionId);


        Log.v("PimmForm", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null){
                        JSONObject jsonObject = (JSONObject) object;
                        Log.v("PimmForm", "Recieved JSON Object: " + object.toString());
                        PimmForm pimmForm = new PimmForm();
                        pimmForm.readFromJSONObject(jsonObject);


                        listener.getLatestFormForReferenceId(pimmForm, null);
                    } else {

                        listener.getLatestFormForReferenceId(null, error);
                    }
                } catch (Exception e){
                    e.printStackTrace();
                    Log.e("PimmForm", "JSONException for getInstanceListForSiteId: " + e.getMessage() );
                    listener.getLatestFormForReferenceId(null, error);
                }
            }
        });


        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getLatestFormForReferenceId");
    }

    public void getFormDefinitionListForAppId(final OnCompleteListeners.getFormDefinitionListForAppIdCallbackListener listener) {

        Log.v("Pimm Forms", "Enter Function: getFormDefinitionListForAppId");
//        https://wendys.pimm.us/WebPimm5/Rest/Pimm/Forms?appId=c89d8b58-e89b-4aa5-ab78-84524df56898
        final OnCompleteListeners.getFormDefinitionListForAppIdCallbackListener getFormDefinitionListForAppIdCallbackListener = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Pimm/Forms");
        urlString.append("?appId=");
        urlString.append(this.ApplicationID);


        Log.v("Pimm Forms", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null){
                        JSONArray jsonArray = (JSONArray) object;
                        Log.v("Pimm Forms", "Recieved JSON Object : " + jsonArray.toString());
                        ArrayList<PimmForm> resultArrayList = new ArrayList<PimmForm>();

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);

                            PimmForm result = new PimmForm();
                            result.readFromJSONObject(jsonObject);
                            resultArrayList.add(result);

                        }
                        listener.getFormDefinitionListForAppIdCallback(resultArrayList, null);
                    } else {
                        Log.e("Pimm Forms", "Null Object");
                        listener.getFormDefinitionListForAppIdCallback(null, error);
                    }
                } catch (JSONException e){
                    e.printStackTrace();
                    Log.e("Pimm Forms", "JSONException for getSiteListForLoggedInUserWithCallback: " + e.getMessage() );
                    listener.getFormDefinitionListForAppIdCallback(null, error);
                }
            }
        });


        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getLatestFormForReferenceId");
    }


    //SMS Dashboard End

    //SiteList for Logged In User - Start

    public void getSiteListForLoggedInUser(final OnCompleteListeners.getSiteListForLoggedInUserWithCallbackListener listener){
        Log.v("New Site List", "Enter Function: getSiteListForLoggedInUser");
        final OnCompleteListeners.getSiteListForLoggedInUserWithCallbackListener getSiteListForLoggedInUserWithCallbackListener = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Pimm/Site");

        Log.v("New Site List", "Request URL: "+urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.setAppContext(mContext);
        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null){

                        JSONArray jsonArray = (JSONArray) object;
                        Log.v("New Site List", "Recieved JSON Object : " + jsonArray.toString());
                        ArrayList<Site> resultArrayList = new ArrayList<Site>();

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);

                            Site result = new Site();
                            result.readFromJSONObject(jsonObject);
                            resultArrayList.add(result);

                        }
                        listener.getSiteListForLoggedInUserWithCallback(resultArrayList, null);
                    } else {
                        Log.e("New Site List", "Null Object");
                        listener.getSiteListForLoggedInUserWithCallback(null, error);
                    }
                } catch (JSONException e){
                    e.printStackTrace();
                    Log.e("New Site List", "JSONException for getSiteListForLoggedInUserWithCallback: " + e.getMessage() );
                    listener.getSiteListForLoggedInUserWithCallback(null, error);
                }
            }
        });


        restConnection.execute(String.valueOf(urlString));
        Log.v("New Site List", "ExitFunction: getSiteListForLoggedInUser");
    }

    //SiteList for Logged In User - End

    //DocumentDTO
    public void getDocumentListForReferenceId(String refx, final OnCompleteListeners.getDocumentListForReferenceIdCallbackListener listener){
        Log.v("DocumentDTO", "Enter Function: getDocumentListForReferenceId");
        final OnCompleteListeners.getDocumentListForReferenceIdCallbackListener getDocumentListForReferenceIdCallbackListener = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Document/Document/GetForRefx");
        urlString.append("?refx=");
        urlString.append(refx);

        Log.v("DocumentDTO", "Request URL: "+urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.setAppContext(mContext);
        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null){
                        System.out.println("DocumentDTO: " +object.toString());
                        JSONArray jsonArray = (JSONArray) object;
                        ArrayList<DocumentDTO> resultArrayList = new ArrayList<DocumentDTO>();

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);

                            DocumentDTO result = new DocumentDTO();
                            result.readFromJSONObject(jsonObject);
                            resultArrayList.add(result);
                        }
                        listener.getDocumentListForReferenceIdCallback(resultArrayList, null);
                    }else {
                        listener.getDocumentListForReferenceIdCallback(null, error);
                    }
                } catch (JSONException e){
                    e.printStackTrace();
                    Log.e("DocumentDTO", "JSONException for getDocumentListForReferenceId: " + e.getMessage() );
                    listener.getDocumentListForReferenceIdCallback(null, error);
                }
            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("DocumentDTO", "ExitFunction: getDocumentListForReferenceId");
    }

    public void getInstanceListForSiteId(String siteId, final OnCompleteListeners.getInstanceListForSiteIdCallbackListener listener){
        Log.v("PimmInstance List", "Enter Function: getInstanceListForSiteId");
        final OnCompleteListeners.getInstanceListForSiteIdCallbackListener getInstanceListForSiteIdCallbackListener = listener;

//        https://wendys.pimm.us/WebPimm5/Rest/PimmTree/Tree/Device/dbac2665-b61d-40b7-8ba1-81db7198d3a7/_Instances
        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("PimmTree/Tree/Device/");
        urlString.append(siteId);
        urlString.append("/_Instances");
        Log.v("PimmInstance List", "Request URL: "+urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null){
                        JSONArray jsonArray = (JSONArray) object;
                        ArrayList<PimmInstance> resultArrayList = new ArrayList<PimmInstance>();
                        Log.v("Recieved JSON Object", "PimmInstance (JSON): " + jsonArray.toString());

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            PimmInstance result = new PimmInstance();
                            result.readFromJSONObject(jsonObject);
                            resultArrayList.add(result);

                        }


                        listener.getInstanceListForSiteIdCallback(resultArrayList, null);
                    } else {

                        System.out.println("Max Enters Object is Null");
                    }
                } catch (JSONException e){
                    e.printStackTrace();
                    Log.e("PimmInstance", "JSONException for getInstanceListForSiteId: " + e.getMessage() );
                    listener.getInstanceListForSiteIdCallback(null, error);
                }
            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("PimmInstance", "ExitFunction: getInstanceListForSiteId");
    }

    public void getScorecardForSiteId(String siteId, String perspective, final OnCompleteListeners.getScorecardForSiteIdCallbackListener listener) {
//        https://wendys.pimm.us/WebPimm5/Rest/Pimm/Scorecard?siteId=96070e0c-cfd0-471c-af95-2d937166f3cb&perspective=customer
        Log.v("Scorecard", "Enter Function: getScorecardForSiteId");
        final OnCompleteListeners.getScorecardForSiteIdCallbackListener getScorecard = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Pimm/Scorecard");
        if (isValidParameterString(siteId)) {
            urlString.append("?siteId=");
            urlString.append(siteId);
            urlString.append("&perspective=");
            urlString.append(perspective.toLowerCase());
        } else {
            Log.e("Scorecard", "getScorecardForSiteId: Missing/Empty/Invalid Parameter deviceID");
        }

        Log.v("Scorecard", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null){
                        new JSONObject(String.valueOf(object));

                        Log.v("Scorecard", "Received JSON Object : " + object.toString());
                        ScorecardDTO scorecardDTO = new ScorecardDTO();
                        JSONObject jsonObject = (JSONObject)object;
                        scorecardDTO.readFromJSONObject(jsonObject);
                        getScorecard.getScorecardForSiteIdCallback(scorecardDTO, error);
                    } else {
                        getScorecard.getScorecardForSiteIdCallback(null, error);
                    }
                } catch (JSONException e){
                    getScorecard.getScorecardForSiteIdCallback(null, error);
                    Log.e("Scorecard", "JSONException getScorecardForSiteId >> " + e.getMessage());
                }
            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("Scorecard", "ExitFunction: getScorecardForSiteId");
    }

    public void getObjectListForDeviceId(String deviceId, final OnCompleteListeners.getObjectListForDeviceIdCallbackListener listener){
        Log.v("Object List", "Enter Function: getObjectListForDeviceId");
        final OnCompleteListeners.getObjectListForDeviceIdCallbackListener getObjectListForDeviceIdCallbackListener = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("PimmTree/Tree/Device/");
        urlString.append(deviceId);

        Log.v("Object List", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null){
                        new JSONObject(String.valueOf(object));

                        PimmObject pimmObject = new PimmObject();
                        JSONObject jsonObject = (JSONObject)object;
                        pimmObject.readFromJSONObject(jsonObject);
                        getObjectListForDeviceIdCallbackListener.getObjectListForDeviceIdCallback(pimmObject, error);
                    } else {
                        getObjectListForDeviceIdCallbackListener.getObjectListForDeviceIdCallback(null, error);
                    }
                } catch (JSONException e){
                    getObjectListForDeviceIdCallbackListener.getObjectListForDeviceIdCallback(null, error);
                    Log.e("Object List", "JSONException getObjectListForDeviceId >> " + e.getMessage());
                }
            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("Object List", "ExitFunction: getObjectListForDeviceId");
    }

    public void getThresholdForInstanceId(String instanceId, final OnCompleteListeners.getThresholdForInstanceIdCallbackListener listener) {

//        https://wendys.pimm.us/WebPimm5/Rest/PimmTree/Tree/Instance/8439587b-01bd-41b6-807f-e9e93982bd47/Thresholds
        Log.v("Threshold", "Enter Function: getThresholdForInstanceId");
        final OnCompleteListeners.getThresholdForInstanceIdCallbackListener getThresholdForInstanceIdCallbackListener = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("PimmTree/Tree/Instance/");
        urlString.append(instanceId);
        urlString.append("/Thresholds");

        Log.v("Threshold", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null){
                        new JSONObject(String.valueOf(object));

                        PimmThreshold pimmThreshold = new PimmThreshold();
                        JSONObject jsonObject = (JSONObject)object;
                        pimmThreshold.readFromJSONObject(jsonObject);
                        getThresholdForInstanceIdCallbackListener.getThresholdForInstanceIdCallback(pimmThreshold, error);

                        Log.v("Threshold", "Received JSON Object : " + object.toString());
                    } else {
                        getThresholdForInstanceIdCallbackListener.getThresholdForInstanceIdCallback(null, error);
                    }
                } catch (JSONException e){
                    getThresholdForInstanceIdCallbackListener.getThresholdForInstanceIdCallback(null, error);
                    Log.e("Threshold", "JSONException getThresholdForInstanceId >> " + e.getMessage());
                }
            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("Threshold", "ExitFunction: getThresholdForInstanceId");
    }

    //Added by Carlo Miclat
    //Start
    public void getReportListforLoggedInUser( final OnCompleteListeners.getReportListforLoggedInUserCallbackListener listener) {
        Log.v("Reports", "Enter Function: getReportListforLoggedInUser");
        final OnCompleteListeners.getReportListforLoggedInUserCallbackListener getReportListforLoggedInUserCallbackListener = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Pimm/Reports/Get");
        Log.v("Reports", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null){
                        ArrayList<ApplicationReport>applicationReports = new ArrayList<>();
                        JSONArray jsonArray = (JSONArray) object;
                        for (int i = 0; i <jsonArray.length() ; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            ApplicationReport applicationReport = new ApplicationReport();
                            applicationReport.readFromJSONObject(jsonObject);
                            applicationReports.add(applicationReport);
                        }
                        Log.v("Reports", "Received JSON Object : " + jsonArray.toString());
                        getReportListforLoggedInUserCallbackListener.getReportListforLoggedInUserCallback(applicationReports,error);
                    } else {
                        getReportListforLoggedInUserCallbackListener.getReportListforLoggedInUserCallback(null,error);
                    }
                } catch (JSONException e){
                    e.printStackTrace();
                    getReportListforLoggedInUserCallbackListener.getReportListforLoggedInUserCallback(null,error);
                    Log.e("Reports", "JSONException getReportListforLoggedInUser >> " + e.getMessage());
                }
            }
        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("Reports", "ExitFunction: getReportListforLoggedInUser");
    }

    public void getDaySummaryForSiteID(Date startDate, Date endDate, final OnCompleteListeners.getDaySummaryForSiteIDCallbackListener listener) {
        Log.v("Daypart Summary", "Enter Function: getDaySummaryForSiteID");

        //        https://staging.pimm.us/WebPimm5/Rest/SMS/FSLW/DaySummary_GetWithStructure?startDate=2019-11-01&endDate=2019-11-10
        final OnCompleteListeners.getDaySummaryForSiteIDCallbackListener getDaySummaryForSiteIDCallbackListener = listener;

        SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd");

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("SMS/FSL/DaySummary_GetWithStructure?startDate=");
        urlString.append(dateformat.format(startDate));
        urlString.append("&endDate=");
        urlString.append(dateformat.format(endDate));

        Log.v("Daypart Summary", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null){
                        FSLDaySummary fslDaySummary = new FSLDaySummary();

                        try {
                            JSONObject objects = (JSONObject) object;
                            fslDaySummary.readFromJSONObject(objects);
                            Log.v("Daypart Summary", "Received JSON Object : " + objects.toString());
                            getDaySummaryForSiteIDCallbackListener.getDaySummaryForSiteIDrCallback(fslDaySummary,error);
                        }catch (Exception e){
                            e.printStackTrace();
                            getDaySummaryForSiteIDCallbackListener.getDaySummaryForSiteIDrCallback(fslDaySummary,error);
                        }

                    } else {
                        getDaySummaryForSiteIDCallbackListener.getDaySummaryForSiteIDrCallback(null,error);
                    }
                } catch (Exception e){
                    e.printStackTrace();
                    getDaySummaryForSiteIDCallbackListener.getDaySummaryForSiteIDrCallback(null,error);
                    Log.e("Daypart Summary", "JSONException getReportListforLoggedInUser >> " + e.getMessage());
                }
            }
        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("Daypart Summary", "ExitFunction: getReportListforLoggedInUser");
    }

    public void  getDocumentContentForDocumentId(String documentID, final String category , final  OnCompleteListeners.getDocumentContentForDocumentIdCallbackListener listerner){
        Log.v("DocumentDTO Content", "Enter Function: getDocumentContentForDocumentId");
        final OnCompleteListeners.getDocumentContentForDocumentIdCallbackListener getDocumentContentForDocumentIdCallbackListener = listerner;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Document/Document/GetContent/");
        urlString.append("?documentId=");
        urlString.append(documentID);

        String[] strings = this.authHeaderValue.split("Basic ");

        final String pdfUrl = urlString+"&"+"authorization="+strings[1];

        Log.v("DocumentDTO Content", "Request URL: "+urlString+"&"+"authorization="+strings[1]);

        RestConnection restConnection = new RestConnection();
        restConnection.setAppContext(mContext);
        restConnection.initialize(this.authHeaderValue,new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if(object!=null){
                        if (category.equalsIgnoreCase("Cleaning_Guides")){
                            object = pdfUrl;
                            getDocumentContentForDocumentIdCallbackListener.getDocumentContentForDocumentIdCallback(object, error);

                        }else {
                            getDocumentContentForDocumentIdCallbackListener.getDocumentContentForDocumentIdCallback(object, error);
                        }
                    }
                    else {
                        getDocumentContentForDocumentIdCallbackListener.getDocumentContentForDocumentIdCallback(null, error);
                    }
                }
                catch (Exception e) {
                    getDocumentContentForDocumentIdCallbackListener.getDocumentContentForDocumentIdCallback(null, error);
                    e.printStackTrace();
                    Log.e("DocumentDTO Content", "JSONException getDocumentListForReferenceId >> " + e.getMessage());
                }

            }
        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("DocumentDTO Content", "ExitFunction: getDocumentListForReferenceId");
    }

    @SuppressLint("LongLogTag")
    public void getCertificationDefinitionsWithCallback(final OnCompleteListeners.getCertificationDefinitionsWithCallbackListener listener) {

        Log.v("Certification", "Enter Function: getCertificationDefinitionsWithCallback");
        //https://staging.pimm.us/WebPimm5/Rest/SMS/Store/GetCertificationDefinitions
        final OnCompleteListeners.getCertificationDefinitionsWithCallbackListener getCertificationDefinitionsWithCallbackListener = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("SMS/Store/GetCertificationDefinitions");

        Log.d("Request URL: ", urlString.toString());

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null){
                        ArrayList<CertificationDefinition>certificationDefinitions = new ArrayList<>();
                        JSONArray jsonArray = (JSONArray) object;

                        for (int i = 0; i <jsonArray.length() ; i++) {
                            CertificationDefinition certificationDefinition = new CertificationDefinition();
                            certificationDefinition.readFromJSONObject(jsonArray.getJSONObject(i));
                            certificationDefinitions.add(certificationDefinition);
                        }

                        getCertificationDefinitionsWithCallbackListener.getCertificationDefinitionsWithCallback(certificationDefinitions,error);

                } else {
                        getCertificationDefinitionsWithCallbackListener.getCertificationDefinitionsWithCallback(null,error);
                    }
                } catch (Exception e){
                    e.printStackTrace();
                    getCertificationDefinitionsWithCallbackListener.getCertificationDefinitionsWithCallback(null,error);

                    Log.e("Certification", "JSONException getCertificationDefinitionsWithCallback >> " + e.getMessage());
                }
            }
        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("Certification", "ExitFunction: getCertificationDefinitionsWithCallback");
    }

    public void getWithStructure(OnCompleteListeners.getWithStructureCallbackListener listerner){
        Log.v("Hierarchy", "Enter Function: getWithStructure");

//        https://wendys.pimm.us/WebPimm5/Rest/Facility/FMS/GetWithStructure
        final OnCompleteListeners.getWithStructureCallbackListener getWithStructureCallbackListener = listerner;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Facility/FMS/GetWithStructure");

        Log.v("Hierarchy", "Request URL: "+urlString.toString());

        RestConnection restConnection = new RestConnection();
        restConnection.setAppContext(mContext);
        restConnection.initialize(this.authHeaderValue,new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if(object!=null){
                        System.out.println(object.toString());
                        ArrayList<CorpStructure>corpStructures = new ArrayList<>();

                        JSONObject jsonObject = (JSONObject) object;

                        JSONArray corpjsarry = jsonObject.getJSONArray("corpStructure");

                        for (int i = 0; i <corpjsarry.length() ; i++) {
                            CorpStructure corpStructure = new CorpStructure();
                            JSONObject corp = corpjsarry.getJSONObject(i);
                            corpStructure.readFromJSONObject(corp);
                            corpStructures.add(corpStructure);
                        }

                        JSONArray siteObject = jsonObject.getJSONArray("sites");

                       getWithStructureCallbackListener.getWithStructureWithCallback(corpStructures,siteObject,null);
                    }
                    else {
                        getWithStructureCallbackListener.getWithStructureWithCallback(null,null,error);
                    }
                }
                catch (Exception e) {
                    e.printStackTrace();
                    getWithStructureCallbackListener.getWithStructureWithCallback(null,null,error);

                    Log.e("Hierarchy", "JSONException getWithStructure >> " + e.getMessage());

                }

            }
        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("Hierarchy", "ExitFunction: getWithStructure");
    }

    @SuppressLint("LongLogTag")
    public void getSMSCommunicationNotesForSite(String siteID, OnCompleteListeners.getSMSCommunicationNotesForSiteCallbackListener listerner){
        Log.v("SMS Communication Notes", "Enter Function: getSMSCommunicationNotesForSite");

//       https://staging.pimm.us/WebPimm5/Rest/Facility/CommunicationNotes/GetNotesForSite?siteId=5d74bffe-d474-48af-ae1e-bd741262b67f&includeAttachments=true
        final OnCompleteListeners.getSMSCommunicationNotesForSiteCallbackListener getSMSCommunicationNotesForSiteCallbackListener = listerner;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Facility/CommunicationNotes/GetNotesForSite?siteId=");
        urlString.append(siteID);
        urlString.append("&includeAttachments=true");

        Log.v("getSMSCommunicationNotesForSite", "Request URL: "+urlString.toString());

        RestConnection restConnection = new RestConnection();
        restConnection.setAppContext(mContext);
        restConnection.initialize(this.authHeaderValue,new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if(object!=null){
                        System.out.println(object.toString());
                        ArrayList<SMSCommunicationNotes>smsCommunicationNotes = new ArrayList<>();
                        JSONArray jsonArray = (JSONArray) object;

                        for (int i = 0; i <jsonArray.length() ; i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            SMSCommunicationNotes notes = new SMSCommunicationNotes();
                            notes.readFromJSONObject(jsonObject);
                            smsCommunicationNotes.add(notes);
                        }
                        getSMSCommunicationNotesForSiteCallbackListener.getSMSCommunicationNotesForSiteWithCallback(smsCommunicationNotes,null);
                    }
                    else {
                        getSMSCommunicationNotesForSiteCallbackListener.getSMSCommunicationNotesForSiteWithCallback(null,error);
                    }
                }
                catch (Exception e) {
                    e.printStackTrace();
                    getSMSCommunicationNotesForSiteCallbackListener.getSMSCommunicationNotesForSiteWithCallback(null,error);

                    Log.e("SMS Communication Notes", "JSONException getSMSCommunicationNotesForSite >> " + e.getMessage());

                }
            }
        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("SMS Communication Notes", "ExitFunction: getSMSCommunicationNotesForSite");
    }

    @SuppressLint("LongLogTag")
    public void getStoreUsers(String siteID,final OnCompleteListeners.getStoreUsersCallbackListener listener) {
        Log.v("User Store", "Enter Function: getStoreUsers");
        //https://staging.pimm.us/WebPimm5/Rest/SMS/Store/GetStoreUsers?siteId={siteId}
        final OnCompleteListeners.getStoreUsersCallbackListener getStoreUsersCallbackListener = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("SMS/Store/GetStoreUsers?siteId=");
        urlString.append(siteID);

        Log.d("Request URL: ", urlString.toString());

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null){
                        ArrayList<User>users = new ArrayList<>();
                        JSONArray jsonArray = (JSONArray) object;

                        for (int i = 0; i <jsonArray.length() ; i++) {
                            try {
                                User user = new User();
                                user.readFromJSONObject(jsonArray.getJSONObject(i));
                                users.add(user);
                                System.out.println("NEW USER: "+jsonArray.getJSONObject(i));
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }
                        getStoreUsersCallbackListener.getStoreUsersCallback(users,error);
                    } else {
                        getStoreUsersCallbackListener.getStoreUsersCallback(null,error);
                    }
                } catch (Exception e){
                    e.printStackTrace();
                    getStoreUsersCallbackListener.getStoreUsersCallback(null,error);

                    Log.e("User Store", "JSONException getStoreUsers >> " + e.getMessage());
                }
            }
        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("User Store", "ExitFunction: getStoreUsers");

    }

    public void createUserRecordForUser(final User user, final OnCompleteListeners.updateUserRecordWithUserIdListener listener) {

        Log.v("User Record", "Enter Function: createUserRecordForUser");
        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Pimm/User");


        String input = Base64Converter.toBase64(user.objectKVP().toString());
        Log.i("User Record", "user json string: "+input);

        StringBuilder parameterString = new StringBuilder();

        if (isValidParameterString(input)) {
            parameterString.append("userDTO=");
            parameterString.append(input);

        } else {
            Log.e("User Record", "createUserRecordForUser : Parameter userDTO is not set");
        }

        Log.v("User Record", "Parameter : "+input);
        Log.v("User Record", "Request URL: " + urlString);


        RestConnection restConnection = new RestConnection();
        restConnection.initializePOSTRequest(this.authHeaderValue, parameterString.toString(), new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {

                        listener.updateUserRecordWithUserIdCallback(true, null);
                    } else {
                        Log.e("User Record", "Null object ");
                        listener.updateUserRecordWithUserIdCallback(null, error);

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("User Record", "JSONException createUserRecordForUser >> " + e.getMessage());
                    listener.updateUserRecordWithUserIdCallback(null, error);
                }
            }
        });

        restConnection.execute(String.valueOf(urlString));

        Log.v("User Record", "ExitFunction: createUserRecordForUser");
    }

        public void updateUserRecordWithUserId(final User user, final OnCompleteListeners.updateUserRecordWithUserIdListener listener) {

        Log.v("User Record", "Enter Function: updateUserRecordWithUserId");
        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Pimm/User");

        JSONObject UserjsonObject = new JSONObject(user.dictionaryWithValuesForKeys());
        String jsonString = UserjsonObject.toString();


        String input = Base64Converter.toBase64(user.objectKVP().toString());

        Log.i("User Record", "user json string: "+user.objectKVP().toString());

        StringBuilder parameterString = new StringBuilder();
        if (isValidParameterString(user.userId)) {
            parameterString.append("userId=");
            parameterString.append(user.userId);
        } else {
            Log.e("User Record", "update USER: Parameter UserID is not set");
        }

        if (isValidParameterString(input)) {
            parameterString.append("&userDTO=");
            parameterString.append(input);

        } else {
            Log.e("User Record", "update USER: Parameter userDTO is not set");
        }


        Log.v("User Record", "Parameter : "+input);
        Log.v("User Record", "Request URL: " + urlString);


        RestConnection restConnection = new RestConnection();
        restConnection.initializePOSTRequest(this.authHeaderValue, parameterString.toString(), new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {

                        listener.updateUserRecordWithUserIdCallback(true, null);
                    } else {
                        Log.e("User Record", "Null object ");
                        listener.updateUserRecordWithUserIdCallback(null, error);

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("User Record", "JSONException updateUserRecordWithUserId >> " + e.getMessage());
                    listener.updateUserRecordWithUserIdCallback(null, error);
                }
            }
        });

        restConnection.execute(String.valueOf(urlString));

        Log.v("User Record", "ExitFunction: updateUserRecordWithUserId");
    }

    public void addRoleForUser(String userId,String role, final OnCompleteListeners.updateUserRecordWithUserIdListener listener) {

        Log.v("User Role", "Enter Function: addRoleForUser");

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Pimm/User/AddRole");


        StringBuilder parameterString = new StringBuilder();
        if (isValidParameterString(userId)) {
            parameterString.append("userId=");
            parameterString.append(userId);
        } else {
            Log.e("User Role", "addRoleForUser: Parameter UserID is not set");
        }

        if (isValidParameterString(role)) {
            parameterString.append("&role=");
            parameterString.append(role.toUpperCase());

        } else {
            Log.e("User Role", "addRoleForUser : Parameter role is not set");
        }

        Log.v("User Role", "Request URL: " + urlString);
        Log.v("User Role", "POST PARAMETER: " + parameterString.toString());


        RestConnection restConnection = new RestConnection();
        restConnection.initializePOSTRequest(this.authHeaderValue, parameterString.toString(), new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        Log.v("User Role", "addRoleForUser : " + object.toString());
                        listener.updateUserRecordWithUserIdCallback(true, null);
                    } else {
                        Log.e("User Role", "Null object ");
                        listener.updateUserRecordWithUserIdCallback(null, error);

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("User Role", "JSONException addRoleForUser >> " + e.getMessage());
                    listener.updateUserRecordWithUserIdCallback(null, error);
                }
            }
        });

        restConnection.execute(String.valueOf(urlString));

        Log.v("User Role", "ExitFunction: addRoleForUser");
    }

    public void removeRoleForUser(String userId,String role, final OnCompleteListeners.updateUserRecordWithUserIdListener listener) {

        Log.v("User Role", "Enter Function: removeRoleForUser");

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Pimm/User/RemoveRole");


        StringBuilder parameterString = new StringBuilder();
        if (isValidParameterString(userId)) {
            parameterString.append("userId=");
            parameterString.append(userId);
        } else {
            Log.e("User Role", "RemoveRole: Parameter UserID is not set");
        }

        if (isValidParameterString(role)) {
            parameterString.append("&role=");
            parameterString.append(role.toUpperCase());

        } else {
            Log.e("User Role", "RemoveRole : Parameter role is not set");
        }

        Log.v("User Role", "Request URL: " + urlString);
        Log.v("User Role", "POST PARAMETER: " + parameterString.toString());


        RestConnection restConnection = new RestConnection();
        restConnection.initializePOSTRequest(this.authHeaderValue, parameterString.toString(), new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        Log.v("User Role", "UPDATED USER : " + object.toString());
                        listener.updateUserRecordWithUserIdCallback(true, null);
                    } else {
                        Log.e("User Role", "Null object ");
                        listener.updateUserRecordWithUserIdCallback(null, error);

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("User Role", "JSONException removeRoleForUser >> " + e.getMessage());
                    listener.updateUserRecordWithUserIdCallback(null, error);
                }
            }
        });

        restConnection.execute(String.valueOf(urlString));

        Log.v("User Role", "ExitFunction: removeRoleForUser");
    }

    public void AddCertificationToUser(String userId,String certification, final OnCompleteListeners.updateUserRecordWithUserIdListener listener) {

        Log.v("User Certification", "Enter Function: AddCertificationToUser");

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("SMS/Store/AddCertificationToUser");


        StringBuilder parameterString = new StringBuilder();
        if (isValidParameterString(userId)) {
            parameterString.append("userId=");
            parameterString.append(userId);
        } else {
            Log.e("User Certification", "AddCertificationToUser: Parameter UserID is not set");
        }

        if (isValidParameterString(certification)) {
            parameterString.append("&certification=");
            parameterString.append(certification);

        } else {
            Log.e("User Certification", "AddCertificationToUser : Parameter role is not set");
        }

        Log.v("User Certification", "Request URL: " + urlString);
        Log.v("User Certification", "POST PARAMETER: " + parameterString.toString());


        RestConnection restConnection = new RestConnection();
        restConnection.initializePOSTRequest(this.authHeaderValue, parameterString.toString(), new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        Log.v("User Certification", "AddCertificationToUser USER : " + object.toString());
                        listener.updateUserRecordWithUserIdCallback(true, null);
                    } else {
                        Log.e("User Certification", "Null object ");
                        listener.updateUserRecordWithUserIdCallback(null, error);

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("User Certification", "JSONException AddCertificationToUser >> " + e.getMessage());
                    listener.updateUserRecordWithUserIdCallback(null, error);
                }
            }
        });

        restConnection.execute(String.valueOf(urlString));

        Log.v("User Certification", "Exit: AddCertificationToUser");
    }

    public void RemoveCertificationFromUser(String userId,String certification, final OnCompleteListeners.updateUserRecordWithUserIdListener listener) {

        Log.v("User Certification", "Enter Function: RemoveCertificationFromUser");

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("SMS/Store/RemoveCertificationFromUser");

        StringBuilder parameterString = new StringBuilder();
        if (isValidParameterString(userId)) {
            parameterString.append("userId=");
            parameterString.append(userId);
        } else {
            Log.e("User Certification", "RemoveCertificationFromUser: Parameter UserID is not set");
        }

        if (isValidParameterString(certification)) {
            parameterString.append("&certification=");
            parameterString.append(certification);

        } else {
            Log.e("User Certification", "RemoveCertificationFromUser : Parameter role is not set");
        }

        Log.v("User Certification", "Request URL: " + urlString);
        Log.v("User Certification", "POST PARAMETER: " + parameterString.toString());


        RestConnection restConnection = new RestConnection();
        restConnection.initializePOSTRequest(this.authHeaderValue, parameterString.toString(), new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        Log.v("User Certification", "RemoveCertificationFromUser USER : " + object.toString());
                        listener.updateUserRecordWithUserIdCallback(true, null);
                    } else {
                        Log.e("User Certification", "Null object ");
                        listener.updateUserRecordWithUserIdCallback(null, error);

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("User Certification", "JSONException RemoveCertificationFromUser >> " + e.getMessage());
                    listener.updateUserRecordWithUserIdCallback(null, error);
                }
            }
        });

        restConnection.execute(String.valueOf(urlString));

        Log.v("User Certification", "exit: RemoveCertificationFromUser");
    }

    public void AddUserToStore(String userId,String siteId, final OnCompleteListeners.updateUserRecordWithUserIdListener listener) {

        Log.v("User Store", "Enter Function: AddUserToStore");

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("SMS/Store/AddUserToStore");

        StringBuilder parameterString = new StringBuilder();
        if (isValidParameterString(userId)) {
            parameterString.append("userId=");
            parameterString.append(userId);
        } else {
            Log.e("User Store", "AddUserToStore: Parameter UserID is not set");
        }

        if (isValidParameterString(siteId)) {
            parameterString.append("&siteId=");
            parameterString.append(siteId);

        } else {
            Log.e("User Store", "AddUserToStore : Parameter siteID is not set");
        }

        Log.v("User Store", "Request URL: " + urlString);
        Log.v("User Store", "POST PARAMETER: " + parameterString.toString());


        RestConnection restConnection = new RestConnection();
        restConnection.initializePOSTRequest(this.authHeaderValue, parameterString.toString(), new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        Log.v("User Store", "AddUserToStore USER : " + object.toString());
                        listener.updateUserRecordWithUserIdCallback(true, null);
                    } else {
                        Log.e("User Store", "Null object ");
                        listener.updateUserRecordWithUserIdCallback(null, error);

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("User Store", "JSONException AddUserToStore >> " + e.getMessage());
                    listener.updateUserRecordWithUserIdCallback(null, error);
                }
            }
        });

        restConnection.execute(String.valueOf(urlString));

        Log.v("User Store", "exit: AddUserToStore");
    }

    public void RemoveUserFromStore(String userId,String siteId, final OnCompleteListeners.updateUserRecordWithUserIdListener listener) {

        Log.v("User Store", "Enter Function: RemoveUserFromStore");

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("SMS/Store/RemoveUserFromStore");

        StringBuilder parameterString = new StringBuilder();
        if (isValidParameterString(userId)) {
            parameterString.append("userId=");
            parameterString.append(userId);
        } else {
            Log.e("User Store", "RemoveUserFromStore: Parameter UserID is not set");
        }

        if (isValidParameterString(siteId)) {
            parameterString.append("&siteId=");
            parameterString.append(siteId);

        } else {
            Log.e("User", "RemoveUserFromStore : Parameter siteID is not set");
        }

        Log.v("User Store", "Request URL: " + urlString);
        Log.v("User Store", "POST PARAMETER: " + parameterString.toString());


        RestConnection restConnection = new RestConnection();
        restConnection.initializeDeleteRequest(this.authHeaderValue, parameterString.toString(), new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        Log.v("User Store", "RemoveUserFromStore USER : " + object.toString());
                        listener.updateUserRecordWithUserIdCallback(true, null);
                    } else {
                        Log.e("User Store", "Null object ");
                        listener.updateUserRecordWithUserIdCallback(null, error);

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("User Store", "JSONException RemoveUserFromStore >> " + e.getMessage());
                    listener.updateUserRecordWithUserIdCallback(null, error);
                }
            }
        });

        restConnection.execute(String.valueOf(urlString));

        Log.v("User Store", "exit: RemoveUserFromStore");
    }

    public void setSiteProperty(String siteId,String property,String value ,final OnCompleteListeners.updateUserRecordWithUserIdListener listener) {

        Log.v("Site Property", "Enter Function: setSiteProperty");

//        https://wendys.pimm.us/WebPimm5/Rest/Pimm/Site/SetProperty

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Pimm/Site/SetProperty");

        StringBuilder parameterString = new StringBuilder();

        if (isValidParameterString(siteId)){
            parameterString.append("siteId=");
            parameterString.append(siteId);
        } else {
            Log.e("Site Property", "setSiteProperty: Parameter siteId is not set");
        }

        if (isValidParameterString(property)) {
            parameterString.append("&property=");
            parameterString.append(property);

        } else {
            Log.e("Site Property", "setSiteProperty : Parameter property is not set");
        }

        if (isValidParameterString(value)) {
            parameterString.append("&value=");
            parameterString.append(value);

        } else {
            Log.e("Site Property", "setSiteProperty : Parameter value is not set");
        }

        Log.v("Site Property", "Request URL: " + urlString);
        Log.v("Site Property", "POST PARAMETER: " + parameterString.toString());

        RestConnection restConnection = new RestConnection();
        restConnection.initializePOSTRequest(this.authHeaderValue, parameterString.toString(), new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        Log.v("Site Property", "setSiteProperty : " + object.toString());
                        listener.updateUserRecordWithUserIdCallback(true, null);
                    } else {
                        Log.e("Site Property", "Null object ");
                        listener.updateUserRecordWithUserIdCallback(null, error);

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("Site Property", "JSONException setSiteProperty >> " + e.getMessage());
                    listener.updateUserRecordWithUserIdCallback(null, error);
                }
            }
        });

        restConnection.execute(String.valueOf(urlString));

        Log.v("Site Property", "Exit Function: setSiteProperty");
    }

    public void listAllFormpackWithName(String formPackname, final OnCompleteListeners.listAllFormpackWithNameListener listener) {

//        SMS/FormsDistribution/List/{formpack}/IPAD

        final OnCompleteListeners.listAllFormpackWithNameListener listAllFormpackWithNameListener = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("SMS/FormsDistribution/List/");
        urlString.append(formPackname);
        urlString.append("/IPAD");

        Log.v("Formpack list", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        Log.i("Formpack list Object", object.toString());

                        ArrayList<FormPack>formPacks = new ArrayList<>();
                        JSONArray jsonArray = (JSONArray) object;

                        for (int i = 0; i <jsonArray.length() ; i++) {
                            FormPack formPack  = new FormPack();
                            formPack.readFromJSONObject(jsonArray.getJSONObject(i));
                            formPacks.add(formPack);
                        }

                        listener.listAllFormpackWithNameCallback( formPacks,null);
                    } else {
                        Log.e("Formpack list", "Null object ");
                        listener.listAllFormpackWithNameCallback(null,error);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("Formpack list", "JSONException for getLatestFormpack: " + e.getMessage());
                    listener.listAllFormpackWithNameCallback( null,null);

                }
            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("Formpack list", "ExitFunction: getLatestFormpack");
    }


    public void loadLatestFormpackWithName(String formPackname,String serialNumber, String currentVersion, final OnCompleteListeners.getLatestFormpackListener listener) {

        Log.v("Formpack", "Enter Function: getLatestFormpack");

//        SMS/FormsDistribution/Latest/{formpack}/IPAD?serialNumber={serialNumber}&currentVersion={currentVersion}

        final OnCompleteListeners.getLatestFormpackListener loadLatestFormpackWithName = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("SMS/FormsDistribution/Latest/");
        urlString.append(formPackname);
        urlString.append("/IPAD?serialNumber=");
        urlString.append(serialNumber);
        urlString.append("&currentVersion=");
        urlString.append(currentVersion);

        Log.v("Formpack", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.setMime(true);
        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        byte []zip = (byte [])object;
                        listener.getLatestFormpackCallback( zip,null);
                    } else {
                        Log.e("Formpack", "Null object ");
                        listener.getLatestFormpackCallback(null,error);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("Formpack", "JSONException for loadLatestFormpackWithName: " + e.getMessage());
                    listener.getLatestFormpackCallback( null,null);

                }
            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("Formpack", "ExitFunction: getLatestFormpack");
    }


    public void getUserRecordForUserId(String userId, final OnCompleteListeners.getUserRecordForUserIdListener listener) {

        Log.v("USER", "Enter Function: getUserRecordForUserId");
        final OnCompleteListeners.getUserRecordForUserIdListener getUserRecord = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Pimm/User");

        if (isValidParameterString(userId)) {
            urlString.append("?userId=");
            urlString.append(userId);
        } else {
            Log.e("USER", "getUserRecordForUserId: userId parameter is invalid/missing,throwing exception");
        }

        Log.v("USER", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                if (object != null) {
                    try {
                        Log.v("USER", "Received JSON Object for getUserRecordForUserId: " + object.toString());
                        User results = new User();
                        results.readFromJSONObject((JSONObject) object);
                        listener.getUserRecordForUserId(results,null);

                    }catch (Exception e){
                        e.printStackTrace();
                        listener.getUserRecordForUserId(null,error);
                    }
                } else {
                    listener.getUserRecordForUserId(null,error);
                    Log.d("USER", "Null object response for getUserRecordForUserId");
                }
            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("USER", "ExitFunction: getUserRecordForUserId");
    }

    public void updateStarRatingForUserCertification(String userId,String certification,String StarRating,
                                                     final OnCompleteListeners.updateUserRecordWithUserIdListener listener) {

        Log.v("User Certification", "Enter Function: updateStarRatingForUserCertification");

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("SMS/Store/UpdateStarRatingForUserCertification");


        StringBuilder parameterString = new StringBuilder();
        if (isValidParameterString(userId)) {
            parameterString.append("userId=");
            parameterString.append(userId);
        } else {
            Log.e("User Certification", "updateStarRatingForUserCertification: Parameter UserID is not set");
        }

        if (isValidParameterString(certification)) {
            parameterString.append("&certification=");
            parameterString.append(certification);

        } else {
            Log.e("User Certification", "updateStarRatingForUserCertification : Parameter certification is not set");
        }

        if (isValidParameterString(StarRating)) {
            parameterString.append("&StarRating=");
            parameterString.append(StarRating);

        } else {
            Log.e("User Certification", "updateStarRatingForUserCertification : Parameter StarRating is not set");
        }

        Log.v("User Certification", "Request URL: " + urlString);
        Log.v("User Certification", "POST PARAMETER: " + parameterString.toString());


        RestConnection restConnection = new RestConnection();
        restConnection.initializePOSTRequest(this.authHeaderValue, parameterString.toString(), new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        Log.v("User Certification", "AddCertificationToUser USER : " + object.toString());
                        listener.updateUserRecordWithUserIdCallback(true, null);
                    } else {
                        Log.e("User Certification", "Null object ");
                        listener.updateUserRecordWithUserIdCallback(null, error);

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("User Certification", "JSONException AddCertificationToUser >> " + e.getMessage());
                    listener.updateUserRecordWithUserIdCallback(null, error);
                }
            }
        });

        restConnection.execute(String.valueOf(urlString));

        Log.v("User Certification", "Exit: updateStarRatingForUserCertification");
    }


    public void getFMSAuditListForSiteID(Date startTime,Date EndTime, String siteId, final OnCompleteListeners.getFMSAuditListForSiteID listener) {

        Log.v("FMS AUDIT", "Enter Function: getFMSAuditListForSiteID");

//        SMS/FormsDistribution/Latest/{formpack}/IPAD?serialNumber={serialNumber}&currentVersion={currentVersion}
        final DateFormat format =  new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
        final OnCompleteListeners.getFMSAuditListForSiteID getFMSAuditListForSiteID = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("Facility/FMSAudit/GetForSite?siteId=");
        urlString.append(siteId);
        urlString.append("&startTime=");
        urlString.append(format.format(startTime));
        urlString.append("&endTime=");
        urlString.append(format.format(EndTime));



        String auth = "&authorization=";
        Log.v("FMS AUDIT", "Request URL: " + urlString+auth+getAuthHeaderValue().replace("Basic ",""));

        RestConnection restConnection = new RestConnection();
        restConnection.setMime(true);
        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        listener.getFMSAuditListForSiteIDCallback(null);
                    } else {
                        Log.e("FMS AUDIT", "Null object ");
                        listener.getFMSAuditListForSiteIDCallback(error);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("FMS AUDIT", "JSONException for getFMSAuditListForSiteID: " + e.getMessage());
                    listener.getFMSAuditListForSiteIDCallback(error);

                }
            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("FMS AUDIT", "ExitFunction: getFMSAuditListForSiteID");
    }


    public void getDefaultSiteSalesForStoreType(int dayOfWeek, int storeType, final OnCompleteListeners.getSiteSalesDefaultListener listener) {

        Log.v("Site Sales Default", "Enter Function: getDefaultSiteSalesForStoreType");

//        https://staging.pimm.us/WebPimm5/Rest/SMS/SalesForecast/GetDefault?dayOfWeek=2&storeType=1

        final OnCompleteListeners.getSiteSalesDefaultListener getSiteSalesDefaultListener = listener;

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("SMS/SalesForecast/GetDefault?dayOfWeek=");
        urlString.append(dayOfWeek);
        urlString.append("&storeType=");
        urlString.append(storeType);

        String auth = "&authorization=";
        Log.v("Site Sales Default", "Request URL: " + urlString+auth+getAuthHeaderValue().replace("Basic ",""));

        RestConnection restConnection = new RestConnection();
        restConnection.setMime(true);
        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        String string = new String((byte[]) object);
                        System.out.println("Site Sales Default : "+ string);
                        SiteSalesDefault salesDefault = new SiteSalesDefault();
                        JSONObject jsonObject = new JSONObject(string);
                        salesDefault.readFromJSONObject(jsonObject);
                        listener.getSiteSalesDefaultCallback(salesDefault,null);
                    } else {
                        Log.e("Site Sales Default", "Null object ");
                        listener.getSiteSalesDefaultCallback(null,error);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("Site Sales Default", "JSONException for getDefaultSiteSalesForStoreType: " + e.getMessage());
                    listener.getSiteSalesDefaultCallback(null,error);

                }
            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("Site Sales Default", "ExitFunction: getDefaultSiteSalesForStoreType");
    }

    public void getSiteSalesByDate(final String siteId, Date salesDate, final OnCompleteListeners.getSiteSalesByDateListener listener) {

        Log.v("Site Sales", "Enter Function: getSiteSalesByDate");

//        https://staging.pimm.us/WebPimm5/Rest/SMS/SalesForecast/GetSiteSalesByDate?siteId=87340814-8fc7-4aa0-b321-b004ecdd02d3&salesDate=2020-04-27

        final OnCompleteListeners.getSiteSalesByDateListener getSiteSalesByDateListener = listener;

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        format.setTimeZone(TimeZone.getTimeZone("UTC"));

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("SMS/SalesForecast/GetSiteSalesByDate?siteId=");
        urlString.append(siteId);
        urlString.append("&salesDate=");
        urlString.append(format.format(salesDate));

        String auth = "&authorization=";
        Log.v("Site Sales ", "Request URL: " + urlString+auth+getAuthHeaderValue().replace("Basic ",""));

        RestConnection restConnection = new RestConnection();
        restConnection.setMime(true);
        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        String string = new String((byte[]) object);
                        System.out.println("getSiteSalesByDate :  "+ string);
                        SiteSales siteSales = new SiteSales();
                        JSONObject jsonObject = new JSONObject(string);
                        siteSales.readFromJSONObject(jsonObject);
                        listener.getSiteSalesByDateCallback(siteSales,null);
                    } else {
                        Log.e("Site Sales ", "Null object ");
                        listener.getSiteSalesByDateCallback(null,error);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("Site Sales ", "JSONException for getSiteSalesByDate: " + e.getMessage());
                    listener.getSiteSalesByDateCallback(null,error);

                }
            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("Site Sales", "ExitFunction: getSiteSalesByDate");
    }

    public void getDaypartSummaryForSiteID(final String siteId, Date startDate,Date endDate, final OnCompleteListeners.getDaypartSummaryForSiteIDListener listener) {

        Log.v("Daypart Summary", "Enter Function: getDaypartSummaryForSiteID");

//        https://wendysendys.pimm.us/WebPimm5/Rest/SMS/FSL/DaypartSummary_Get?endDate=2020-05-18&siteId=87340814-8fc7-4aa0-b321-b004ecdd02d3&startDate=2020-05-01&authorization=cGltbWdtQHdlbmR5cy5jb206cGltbWdt;


        final OnCompleteListeners.getDaypartSummaryForSiteIDListener getDaypartSummaryForSiteIDListener = listener;


        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        format.setTimeZone(TimeZone.getTimeZone("UTC"));

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("SMS/FSL/DaypartSummary_Get?endDate=");
        urlString.append(format.format(endDate));
        urlString.append("&siteId=");
        urlString.append(siteId);
        urlString.append("&startDate=");
        urlString.append(format.format(startDate));


        String auth = "&authorization=";
        Log.v("Daypart Summary", "Request URL: " + urlString+auth+getAuthHeaderValue().replace("Basic ",""));

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        FSLDaypartSummaryDTO fslDaypartSummaryDTO = new FSLDaypartSummaryDTO();
                        JSONObject jsonObject = (JSONObject) object;
                        fslDaypartSummaryDTO.readFromJSONObject(jsonObject);
                        listener.getDaypartSummaryForSiteIDCallback(fslDaypartSummaryDTO,null);
                    } else {
                        Log.e("Daypart Summary", "Null object ");
                        listener.getDaypartSummaryForSiteIDCallback(null,error);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("Daypart Summary", "JSONException for getDaypartSummaryForSiteID: " + e.getMessage());
                    listener.getDaypartSummaryForSiteIDCallback(null,error);

                }
            }
        });

        restConnection.execute(String.valueOf(urlString));
        Log.v("Daypart Summary", "ExitFunction: getDaypartSummaryForSiteID");
    }

    public void getDaySummaryForSiteID(String siteID,Date startDate, Date endDate, final OnCompleteListeners.getDaySummaryForSiteIDCallbackListener listener) {
        Log.v("Daypart Summary", "Enter Function: getDaySummaryForSiteID");

        //        https://staging.pimm.us/WebPimm5/Rest/SMS/FSLW/DaySummary_GetWithStructure?startDate=2019-11-01&endDate=2019-11-10
        final OnCompleteListeners.getDaySummaryForSiteIDCallbackListener getDaySummaryForSiteIDCallbackListener = listener;

        SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd");
        dateformat.setTimeZone(TimeZone.getTimeZone("UTC"));

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("SMS/FSL/DaySummary_GetWithStructure?startDate=");
        urlString.append(dateformat.format(startDate));
        urlString.append("&endDate=");
        urlString.append(dateformat.format(endDate));
        urlString.append("&siteId=");
        urlString.append(siteID);


        Log.v("Daypart Summary", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null){
                        FSLDaySummary fslDaySummary = new FSLDaySummary();

                        try {
                            JSONObject objects = (JSONObject) object;
                            fslDaySummary.readFromJSONObject(objects);
                            Log.v("Daypart Summary", "Received JSON Object : " + objects.toString());
                            getDaySummaryForSiteIDCallbackListener.getDaySummaryForSiteIDrCallback(fslDaySummary,error);
                        }catch (Exception e){
                            e.printStackTrace();
                            getDaySummaryForSiteIDCallbackListener.getDaySummaryForSiteIDrCallback(fslDaySummary,error);
                        }

                    } else {
                        getDaySummaryForSiteIDCallbackListener.getDaySummaryForSiteIDrCallback(null,error);
                    }
                } catch (Exception e){
                    e.printStackTrace();
                    getDaySummaryForSiteIDCallbackListener.getDaySummaryForSiteIDrCallback(null,error);
                    Log.e("Daypart Summary", "JSONException getReportListforLoggedInUser >> " + e.getMessage());
                }
            }
        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("Daypart Summary", "ExitFunction: getReportListforLoggedInUser");
    }


    public void getEmployeeSchedule(String siteID,Date startDate, Date endDate,String employeeId, final OnCompleteListeners.getEmployeeScheduleListener listener) {
        Log.v("Employee Schedule", "Enter Function: getEmployeeSchedule");

        //        https://staging.pimm.us/WebPimm5/Rest/SMS/EmployeeSchedule/GetScheduleForEmployee
        final OnCompleteListeners.getEmployeeScheduleListener getEmployeeScheduleListener = listener;

        SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd");
        dateformat.setTimeZone(TimeZone.getTimeZone("UTC"));

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("SMS/EmployeeSchedule/GetScheduleForEmployee?siteId=");
        urlString.append(siteID);
        urlString.append("&userId=");
        urlString.append(employeeId);
        urlString.append("&startDate=");
        urlString.append(dateformat.format(startDate));
        urlString.append("&endDate=");
        urlString.append(dateformat.format(endDate));


        Log.v("Employee Schedule", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null){
                        ArrayList<Schedule_Business_Site_Plan> schedules = new ArrayList<>();
                        Log.v("Employee Schedule", "Received JSON Object : " + object.toString());
                        try {
                            JSONArray jsonArray = (JSONArray) object;
                            for (int i = 0; i <jsonArray.length() ; i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                Schedule_Business_Site_Plan item = new Schedule_Business_Site_Plan();
                                item.readFromJSONObject(jsonObject);
                                schedules.add(item);
                            }
                            getEmployeeScheduleListener.getEmployeeScheduleCallback(schedules,error);
                        }catch (Exception e){
                            e.printStackTrace();
                            getEmployeeScheduleListener.getEmployeeScheduleCallback(schedules,error);
                        }

                    } else {
                        getEmployeeScheduleListener.getEmployeeScheduleCallback(null,error);
                    }
                } catch (Exception e){
                    e.printStackTrace();
                    getEmployeeScheduleListener.getEmployeeScheduleCallback(null,error);
                    Log.e("Employee Schedule", "JSONException >> " + e.getMessage());
                }
            }
        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("Employee Schedule", "ExitFunction: getEmployeeSchedule");
    }

    public void getSchedulesByDate(String siteID,Date startDate, Date endDate, final OnCompleteListeners.getEmployeeScheduleListener listener) {
        Log.v("Employee Schedule", "Enter Function: GetSchedulesByDate");

        //        https://staging.pimm.us/WebPimm5/Rest/SMS/EmployeeSchedule/GetSchedulesByDate
        final OnCompleteListeners.getEmployeeScheduleListener getEmployeeScheduleListener = listener;

        SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd");
        dateformat.setTimeZone(TimeZone.getTimeZone("UTC"));

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("SMS/EmployeeSchedule/GetSchedulesByDate?siteId=");
        urlString.append(siteID);
        urlString.append("&startDate=");
        urlString.append(dateformat.format(startDate));
        urlString.append("&endDate=");
        urlString.append(dateformat.format(endDate));


        Log.v("Employee Schedule", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue, new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null){
                        ArrayList<Schedule_Business_Site_Plan> schedules = new ArrayList<>();
                        Log.v("Employee Schedule", "Received JSON Object : " + object.toString());
                        try {
                            JSONArray jsonArray = (JSONArray) object;
                            for (int i = 0; i <jsonArray.length() ; i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                Schedule_Business_Site_Plan item = new Schedule_Business_Site_Plan();
                                item.readFromJSONObject(jsonObject);
                                schedules.add(item);
                            }
                            getEmployeeScheduleListener.getEmployeeScheduleCallback(schedules,error);
                        }catch (Exception e){
                            e.printStackTrace();
                            getEmployeeScheduleListener.getEmployeeScheduleCallback(schedules,error);
                        }

                    } else {
                        getEmployeeScheduleListener.getEmployeeScheduleCallback(null,error);
                    }
                } catch (Exception e){
                    e.printStackTrace();
                    getEmployeeScheduleListener.getEmployeeScheduleCallback(null,error);
                    Log.e("Employee Schedule", "JSONException >> " + e.getMessage());
                }
            }
        });
        restConnection.execute(String.valueOf(urlString));
        Log.v("Employee Schedule", "ExitFunction: GetSchedulesByDate");
    }

    public void getBinaryFormAttachmentWithFormId(String formId, String attachmentId, final OnCompleteListeners.getBinaryFormAttachmentWithFormIdListener listener) {

        Log.v("DM", "Enter Function: getFormAttachmentWithFormId");
        final OnCompleteListeners.getBinaryFormAttachmentWithFormIdListener getFormAttachment = listener;

        //        https://re.pimm.us/WebPimm5/Rest/PimmForm/FormAttachment/Binary/f397847d-5b0f-493f-8b1b-d6b6af919b46/297FE4DE-4757-4DA5-AD78-28C78E50EF06

        StringBuilder urlString = new StringBuilder();
        urlString.append(this.baseUrl);
        urlString.append("PimmForm/FormAttachment/Binary/");
        if (isValidParameterString(formId)) {
            urlString.append(formId);
            urlString.append("/");
        } else {
            Log.e("DM", "getFormAttachmentWithFormId: Missing/Empty/Invalid Parameter formId");
        }
        if (isValidParameterString(attachmentId)) {
            urlString.append(attachmentId);

        } else {
            Log.e("DM", "getFormAttachmentWithFormId: Missing/Empty/Invalid Parameter attachmentId");
        }

        Log.v("DM", "Request URL: " + urlString);

        RestConnection restConnection = new RestConnection();
        restConnection.initialize(this.authHeaderValue,
                new OnCompleteListeners.OnRestConnectionCompleteListener() {
            @Override
            public void onRestConnectionComplete(Object object, Error error) {
                try {
                    if (object != null) {
                        System.out.println("SIGNATURE OBJECT "+object.toString());
                        System.out.println("OBJECT CLASS" +object.getClass());
                        getFormAttachment.getBinaryFormAttachmentWithFormIdCallback(object,null);
                    } else {
                        Log.e("DM", "Null object ");
                        getFormAttachment.getBinaryFormAttachmentWithFormIdCallback(null,error);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    getFormAttachment.getBinaryFormAttachmentWithFormIdCallback(null,error);
                    Log.e("DM", "JSONException for getFormAttachmentWithFormId" + e.getMessage());
//
                }
            }
        });


        restConnection.execute(String.valueOf(urlString));
        Log.v("DM", "ExitFunction: getFormAttachmentWithFormId");
    }


    //Added by Carlo Miclat
    //END

    //New APIs End

}