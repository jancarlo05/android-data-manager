package com.procuro.apimmdatamanagerlib;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class FSLDaypartSummaryDTO extends PimmBaseObject {

    public ArrayList<FSLSummary> daySummaryList;
    public ArrayList<FSLSummary> daypartSummaryList;
    public ArrayList<FSLUser> userList;
    public ArrayList<FSLTemperaturePerformace> temperaturePerformanceList;

    @Override
    public void setValueForKey(Object value, String key) {
        super.setValueForKey(value, key);

        if (key.equalsIgnoreCase("daySummaryList")){
            ArrayList<FSLSummary>list = new ArrayList<>();
            if (value!=null){
                try {
                    JSONArray array = (JSONArray) value;
                    for (int i = 0; i <array.length() ; i++) {
                        JSONObject jsonObject = array.getJSONObject(i);
                        FSLSummary item = new FSLSummary();
                        item.readFromJSONObject(jsonObject);
                        list.add(item);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
            this.daySummaryList = list;
        }

       else if (key.equalsIgnoreCase("daypartSummaryList")){
            ArrayList<FSLSummary>list = new ArrayList<>();
            if (value!=null){
                try {
                    JSONArray array = (JSONArray) value;
                    for (int i = 0; i <array.length() ; i++) {
                        JSONObject jsonObject = array.getJSONObject(i);
                        FSLSummary item = new FSLSummary();
                        item.readFromJSONObject(jsonObject);
                        list.add(item);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
            this.daypartSummaryList = list;
        }

        else if (key.equalsIgnoreCase("userList")){
            ArrayList<FSLUser>list = new ArrayList<>();
            if (value!=null){
                try {
                    JSONArray array = (JSONArray) value;
                    for (int i = 0; i <array.length() ; i++) {
                        JSONObject jsonObject = array.getJSONObject(i);
                        FSLUser item = new FSLUser();
                        item.readFromJSONObject(jsonObject);
                        list.add(item);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
            this.userList = list;
        }

        else if (key.equalsIgnoreCase("temperaturePerformanceList")){
            ArrayList<FSLTemperaturePerformace>list = new ArrayList<>();
            if (value!=null){
                try {
                    JSONArray array = (JSONArray) value;
                    for (int i = 0; i <array.length() ; i++) {
                        JSONObject jsonObject = array.getJSONObject(i);
                        FSLTemperaturePerformace item = new FSLTemperaturePerformace();
                        item.readFromJSONObject(jsonObject);
                        list.add(item);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
            this.temperaturePerformanceList = list;
        }

    }
}
