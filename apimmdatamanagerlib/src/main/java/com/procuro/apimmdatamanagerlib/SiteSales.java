package com.procuro.apimmdatamanagerlib;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;

public class SiteSales extends  PimmBaseObject {


    public String salesId;
    public String siteId;

    public Date salesDate;

    public double revenueForecastTotal;
    public double revenueActualTotal;

    public SiteSalesData data;

    @Override
    public void setValueForKey(Object value, String key) {
        super.setValueForKey(value, key);

        if (key.equalsIgnoreCase("revenueForecastTotal")){
            if (value!=null){
                try {
                    revenueForecastTotal = Double.parseDouble(value.toString());
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
        if (key.equalsIgnoreCase("revenueActualTotal")){
            if (value!=null){
                try {
                    revenueActualTotal = Double.parseDouble(value.toString());
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }

        if (key.equalsIgnoreCase("salesDate")){
            if (value!=null){
                try {
                    salesDate = JSONDate.convertJSONDateToNSDate(value.toString());
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }

        if (key.equalsIgnoreCase("data")){
            if (value!=null){
                try {
                    SiteSalesData item = new SiteSalesData();
                    JSONObject jsonObject = (JSONObject) value;
                    item.readFromJSONObject(jsonObject);
                    this.data = item;
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
    }
}


