package com.procuro.apimmdatamanagerlib;

import android.util.Base64;

import org.json.JSONArray;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.sql.Blob;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class PimmFormAttachment extends PimmBaseObject {

    public String AttachmentID;
    public String FormID;
    public String AttachmentType;
    public String AttachmentName;
    public Date CreationDate;
    public String Oid;
    public byte[] Body;
    public String url;


//    NSMutableArray* Body; //  its a byte[] array  of Base64 Encoded bytes received in JSON Response from Server
//    NSData* AttachmentContent;  //  Decoded Attachment Bytes as sent during createAttachment

    @Override
    public void setValueForKey(Object value, String key) {

        if (key.equalsIgnoreCase("Body")) {
            if (DMUtils.ValidObject(value)) {
                try {
                    JSONArray jsonArray = (JSONArray) value;
                    byte[] bytes = new byte[jsonArray.length()];
                    for (int i = 0; i < jsonArray.length(); i++) {
                        bytes[i]=(byte)(((int)jsonArray.get(i)) & 0xFF);
                    }
                    Base64.encodeToString(bytes, Base64.DEFAULT);
                    this.Body = bytes;
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
        else   if (key.equalsIgnoreCase("CreationDate")) {
            if (DMUtils.ValidObject(value)) {
                try {
                    this.CreationDate = JSONDate.convertJSONDateToNSDate(value.toString());
                }catch (Exception ignored){}
            }
        }
        else {
            super.setValueForKey(value, key);
        }
    }

}
