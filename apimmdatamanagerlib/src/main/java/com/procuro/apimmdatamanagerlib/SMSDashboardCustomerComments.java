package com.procuro.apimmdatamanagerlib;

import android.util.Log;

import java.util.Date;
import java.util.HashMap;

public class SMSDashboardCustomerComments extends PimmBaseObject{
    public String siteID;
    public Date timestamp;
    public String c1;
    public String c2;
    public String c3;
    public String c4;

    @Override
    public void setValueForKey(Object value, String key) {
        if (key.equalsIgnoreCase("timestamp"))
        {
            this.timestamp = JSONDate.convertJSONDateToNSDate(String.valueOf(value));
            Log.v("Timestamp", "Value: "+this.timestamp);
        } else {
            super.setValueForKey(value, key);
        }
    }

    public HashMap<String,Object> dictionaryWithValuesForKeys() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();

        dictionary.put("siteid",this.siteID);

        dictionary.put("timestamp",this.timestamp);
        dictionary.put("c1",this.c1);
        dictionary.put("c2",this.c2);
        dictionary.put("c3",this.c3);
        dictionary.put("c4",this.c4);

        return dictionary;
    }


}
