package com.procuro.apimmdatamanagerlib;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by jophenmarieweeks on 12/07/2017.
 */

public class POISiteListDTO extends PimmBaseObject {

    public ArrayList<POISiteDTO> poiSiteList;
    public Date token;
   // private Object value;

    @Override
    public void setValueForKey(Object value, String key) {

        if(key.equalsIgnoreCase("poiSiteList"))
        {
            if(this.poiSiteList == null)
            {
                this.poiSiteList = new ArrayList<POISiteDTO>();
            }

            JSONArray arrPOISiteDTO = (JSONArray) value;

            if (arrPOISiteDTO != null) {
                for (int i=0; i<arrPOISiteDTO.length(); i++) {
                    try {
                        POISiteDTO poiSiteDTO = new POISiteDTO();
                        poiSiteDTO.readFromJSONObject(arrPOISiteDTO.getJSONObject(i));

                        this.poiSiteList.add(poiSiteDTO);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        else if(key.equalsIgnoreCase("token")){
            this.token = JSONDate.convertJSONDateToNSDate(String.valueOf(value));
            Log.v("DM", "Value: "+this.token);
        }
        else
        {
            super.setValueForKey(value, key);
        }
    }

    public HashMap<String,Object> dictionaryWithValuesForKeys() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();

        dictionary.put("token" , this.token);

        if (this.poiSiteList != null) {

            ArrayList<HashMap<String, Object>> POISiteDTOArray = new ArrayList<>();

            for(POISiteDTO POISiteDTOValue : this.poiSiteList) {

                HashMap<String, Object> propertyValueDict = POISiteDTOValue.dictionaryWithValuesForKeys();
                POISiteDTOArray.add(propertyValueDict);

            }

            dictionary.put("poiSiteList", POISiteDTOArray);

        }else{
            dictionary.put("poiSiteList", null);
        }

        return  dictionary;
    }


}
