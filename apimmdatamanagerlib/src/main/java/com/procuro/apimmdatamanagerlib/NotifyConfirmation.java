package com.procuro.apimmdatamanagerlib;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jophenmarieweeks on 12/07/2017.
 */

public class NotifyConfirmation extends PimmBaseObject {
    public List<NotifyConfirmationRecipient> recipients; //list of NotifyConfirmationRecipient objects


    @Override
    public void setValueForKey(Object value, String key) {
        if (key.equalsIgnoreCase("recipients")) {
            if (this.recipients == null) {
                this.recipients = new ArrayList<NotifyConfirmationRecipient>();
            }

            if (!value.equals(null)) {
                JSONArray arrStops = (JSONArray) value;

                if (arrStops != null) {
                    for (int i = 0; i < arrStops.length(); i++) {
                        try {
                            NotifyConfirmationRecipient notifyConfirmationRecipient = new NotifyConfirmationRecipient();
                            notifyConfirmationRecipient.readFromJSONObject(arrStops.getJSONObject(i));

                            this.recipients.add(notifyConfirmationRecipient);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } else {
                super.setValueForKey(value, key);
            }
        }

    }
}
