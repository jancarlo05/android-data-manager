package com.procuro.apimmdatamanagerlib;

/**
 * Created by jophenmarieweeks on 07/07/2017.
 */

public class ProductTemperatureReading extends PimmBaseObject {


    public String timestamp;
    public String productId;
    public int value1;
    public int value2;
    public int value3;
    public String refx;
    public String productTemperatureReadingId;

}
