package com.procuro.apimmdatamanagerlib;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

public class SMSPositioningEmployeePositionData extends PimmBaseObject {


    public boolean headphone;
    public boolean supportManager;
    public boolean opsLeader;

    public Date timeIn;
    public Date timeOut;
    public Date assignedDateISO;
    public Date assignedDate;

    public int position;

    public ArrayList<String>cleaningResponsibilities;
    public ArrayList<String>positionResponsibilities;


    public String positionName;
    public String employee;
    public String employeeID;

    @Override
    public void setValueForKey(Object value, String key) {
        super.setValueForKey(value, key);

        if (key.equalsIgnoreCase("timeIn")){
            if (value!=null){
                this.timeIn = JSONDate.convertJSONDateToNSDate(value.toString());
            }
        }
        if (key.equalsIgnoreCase("timeOut")){
            if (value!=null){
                this.timeOut = JSONDate.convertJSONDateToNSDate(value.toString());
            }
        }
        if (key.equalsIgnoreCase("assignedDateISO")){
            if (value!=null){
                this.assignedDateISO = JSONDate.convertJSONDateToNSDate(value.toString());
            }
        }
        if (key.equalsIgnoreCase("assignedDate")){
            if (value!=null){
                this.assignedDate = JSONDate.convertJSONDateToNSDate(value.toString());
            }
        }
        if (key.equalsIgnoreCase("position")){
            if (value!=null){
                if (value.toString().equalsIgnoreCase("null")){
                    this.position = (int) value;
                }
            }
        }
        if (key.equalsIgnoreCase("opsLeader")){
            if (value!=null){
                try {
                    if (value.toString().equalsIgnoreCase("0")){
                        this.headphone = false;
                    }else if (value.toString().equalsIgnoreCase("1")){
                        this.headphone = true;
                    }else {
                        this.headphone = (boolean) value;
                    }

                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
        if (key.equalsIgnoreCase("headphone")){
            if (value!=null){
                try {
                    if (value.toString().equalsIgnoreCase("0")){
                        this.headphone = false;
                    }else if (value.toString().equalsIgnoreCase("1")){
                        this.headphone = true;
                    }else {
                        this.headphone = (boolean) value;
                    }

                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
        if (key.equalsIgnoreCase("positionName")){
            if (value!=null){
                this.positionName = (String) value.toString();
            }
        }
        if (key.equalsIgnoreCase("supportManager")){
            if (value!=null){
                try {
                    if (value.toString().equalsIgnoreCase("0")){
                        this.headphone = false;
                    }else if (value.toString().equalsIgnoreCase("1")){
                        this.headphone = true;
                    }else {
                        this.headphone = (boolean) value;
                    }

                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
        if (key.equalsIgnoreCase("employeeID")){
            if (value!=null){
                this.employeeID = (String) value.toString();
            }
        }
        if (key.equalsIgnoreCase("employee")){
            if (value!=null){
                this.employee = (String) value.toString();
            }
        }

        if (key.equalsIgnoreCase("cleaningResponsibilities")){
            ArrayList<String>strings = new ArrayList<>();
            if (value!=null){
                JSONArray jsonArray = (JSONArray) value;
                for (int i = 0; i <jsonArray.length() ; i++) {
                    try {
                        strings.add(jsonArray.getString(i));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            this.cleaningResponsibilities = strings;
        }

        if (key.equalsIgnoreCase("positionResponsibilities")){
            ArrayList<String>strings = new ArrayList<>();
            if (value!=null){
                JSONArray jsonArray = (JSONArray) value;
                for (int i = 0; i <jsonArray.length() ; i++) {
                    try {
                        strings.add(jsonArray.getString(i));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
            this.positionResponsibilities = strings;
        }

    }


    public HashMap<String,Object> dictionaryWithValuesForKeys() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();

        dictionary.put("timeIn", this.timeIn);
        dictionary.put("timeOut", this.timeOut);
        dictionary.put("assignedDateISO", this.assignedDateISO);
        dictionary.put("assignedDate", this.assignedDate);
        dictionary.put("position", this.position);
        dictionary.put("opsLeader", this.opsLeader);
        dictionary.put("headphone", this.headphone);
        dictionary.put("positionName", this.positionName);
        dictionary.put("supportManager", this.supportManager);
        dictionary.put("employeeID", this.employeeID);
        dictionary.put("employee", this.employee);
        dictionary.put("cleaningResponsibilities", this.cleaningResponsibilities);
        dictionary.put("positionResponsibilities", this.positionResponsibilities);

        return dictionary;
    }
    
}
