package com.procuro.apimmdatamanagerlib;

import java.math.BigDecimal;
import java.util.Date;

public class SiteSalesForecastTime extends  PimmBaseObject {

   public String startTime;
   public String endTime;
   public BigDecimal percentage;

   @Override
   public void setValueForKey(Object value, String key) {
      super.setValueForKey(value, key);

      if (key.equalsIgnoreCase("percentage")){
         if (value!=null){
            try {
               percentage = new BigDecimal(value.toString());
            }catch (Exception e){
               e.printStackTrace();
            }
         }
      }

   }
}
