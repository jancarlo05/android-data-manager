package com.procuro.apimmdatamanagerlib;

import java.util.Date;
import java.util.HashMap;

public class Award extends  PimmBaseObject {

    public String userAwardID;
    public String userID;
    public Date timestamp;
    public String userAwardDefinitionID;
    public String awardReason;
    public String comment;

    @Override
    public void setValueForKey(Object value, String key) {
        super.setValueForKey(value, key);

        if (key.equalsIgnoreCase("timestamp")){
            if (value!=null){
                if (!value.toString().equalsIgnoreCase("null")){
                    this.timestamp = JSONDate.convertJSONDateToNSDate(value.toString());
                }
            }
        }
    }

    public HashMap<String,Object> dictionaryWithValuesForKeys() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();

        dictionary.put("userAwardID", this.userAwardID);
        dictionary.put("userID", this.userID);
        dictionary.put("timestamp", this.timestamp);
        dictionary.put("userAwardDefinitionID", this.userAwardDefinitionID);
        dictionary.put("awardReason", this.awardReason);
        dictionary.put("comment", this.comment);
        return dictionary;

    }


}
