package com.procuro.apimmdatamanagerlib;

/**
 * Created by jophenmarieweeks on 12/07/2017.
 */

public class CoreDataHelper extends PimmBaseObject {

   /*
    #import <Foundation/Foundation.h>
            #import <CoreData/CoreData.h>
            #import "NetworkRequest.h"
    @interface CoreDataHelper : NSObject


    @property (nonatomic,strong) NSManagedObjectContext *context;
    @property (nonatomic, strong) NSManagedObjectModel *model;
    @property (nonatomic, readonly) NSPersistentStoreCoordinator *coordinator;
    @property (nonatomic, readonly) NSPersistentStore            *store;


+ (CoreDataHelper *)sharedCoreDataHelper;

- (void)saveContext;

-(void) resetCoreData;

-(NSArray*) loadAllRequests;

-(void) removeRequest:(NetworkRequest*) request;

//-(NetworkRequest*) addRequest;



-(NetworkRequest*) addRequestWithUrlString:(NSString*) urlString
    AuthStr:(NSString*) authStr
    RequestType:(unsigned int) type
    ContentLength:( unsigned long) contentLength
    ContentType:(NSString*) contentType
    Body:(NSData*) httpBody
    RequestIdentifier:(int) requestIdentifier
    IsJSONResponse:(BOOL) isJSONResponse
    ResponseObjectClassName:(NSString*) responseObjectClassName
    Username:(NSString*) username
    RequestName:(NSString*) requestName
    Context:(NSMutableDictionary*) context
    isFileUploadRequest:(BOOL ) isFileUploadRequest
    FileUploadList:(NSDictionary*) fileUploadList
    AttachmentTypeList:(NSDictionary*) attachmentTypeList
    ContentDispositionList:(NSDictionary*) contentDispositionList;

    **/
}
