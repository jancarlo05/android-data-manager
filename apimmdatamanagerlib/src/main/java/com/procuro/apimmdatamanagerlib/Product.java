package com.procuro.apimmdatamanagerlib;

/**
 * Created by jophenmarieweeks on 06/07/2017.
 */

public class Product extends PimmBaseObject {


    public String shipmentProductId;
    public String productName;
    public String productType;

    public boolean upperThresholdEnabled;
    public boolean lowerThresholdEnabled;

    public double upperThreshold;
    public double lowerThreshold;
}
