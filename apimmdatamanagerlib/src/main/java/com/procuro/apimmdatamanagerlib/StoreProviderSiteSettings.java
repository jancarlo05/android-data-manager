package com.procuro.apimmdatamanagerlib;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jophenmarieweeks on 10/07/2017.
 */

public class StoreProviderSiteSettings extends PimmBaseObject {
    public ArrayList<PropertyValue>  settings;
    public ArrayList<PropertyValue> displaySensors ;
    public String displaySensorMatch;
    public ArrayList<PropertyValue>  requiredSensors;
    public ArrayList<PropertyValue>  otdLateCodes;
    public ArrayList<PropertyValue>  equipmentTypes;
    public ArrayList<PropertyValue>  productVerificationCodes;
    public ArrayList<PropertyValue>  debitCodes;
    public ArrayList<PropertyValue>  snrCodes;
    public ArrayList<PropertyValue>  scnList;

    @Override
    public void setValueForKey(Object value, String key) {

        if (key.equalsIgnoreCase("settings")) {

            if (this.settings == null) {
                this.settings = new ArrayList<PropertyValue>();
            }

            if (!value.equals(null)) {
                if (value instanceof JSONArray) {

                    JSONArray arrVals = (JSONArray) value;

                    if (arrVals != null) {
                        for (int i = 0; i < arrVals.length(); i++) {
                            try {
                                PropertyValue results = new PropertyValue();
                                results.readFromJSONObject(arrVals.getJSONObject(i));

                                this.settings.add(results);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                }
            }
        }else if (key.equalsIgnoreCase("otdLateCodes")) {

            if (this.otdLateCodes == null) {
                this.otdLateCodes = new ArrayList<PropertyValue>();
            }

            if (!value.equals(null)) {
                if (value instanceof JSONArray) {

                    JSONArray arrVals = (JSONArray) value;

                    if (arrVals != null) {
                        for (int i = 0; i < arrVals.length(); i++) {
                            try {
                                PropertyValue results = new PropertyValue();
                                results.readFromJSONObject(arrVals.getJSONObject(i));

                                this.otdLateCodes.add(results);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                }
            }
        }else if (key.equalsIgnoreCase("equipmentTypes")) {

            if (this.equipmentTypes == null) {
                this.equipmentTypes= new ArrayList<PropertyValue>();
            }

            if (!value.equals(null)) {
                if (value instanceof JSONArray) {

                    JSONArray arrVals = (JSONArray) value;

                    if (arrVals != null) {
                        for (int i = 0; i < arrVals.length(); i++) {
                            try {
                                PropertyValue results = new PropertyValue();
                                results.readFromJSONObject(arrVals.getJSONObject(i));

                                this.equipmentTypes.add(results);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                }
            }
        }else if (key.equalsIgnoreCase("productVerificationCodes")) {

            if (this.productVerificationCodes == null) {
                this.productVerificationCodes= new ArrayList<PropertyValue>();
            }

            if (!value.equals(null)) {
                if (value instanceof JSONArray) {

                    JSONArray arrVals = (JSONArray) value;

                    if (arrVals != null) {
                        for (int i = 0; i < arrVals.length(); i++) {
                            try {
                                PropertyValue results = new PropertyValue();
                                results.readFromJSONObject(arrVals.getJSONObject(i));

                                this.productVerificationCodes.add(results);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                }
            }
        }else if (key.equalsIgnoreCase("displaySensors")) {

            if (this.displaySensors == null) {
                this.displaySensors= new ArrayList<PropertyValue>();
            }

            if (!value.equals(null)) {
                if (value instanceof JSONArray) {

                    JSONArray arrVals = (JSONArray) value;

                    if (arrVals != null) {
                        for (int i = 0; i < arrVals.length(); i++) {
                            try {
                                PropertyValue results = new PropertyValue();
                                results.readFromJSONObject(arrVals.getJSONObject(i));

                                this.displaySensors.add(results);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                }
            }
        }else if (key.equalsIgnoreCase("requiredSensors")) {

            if (this.requiredSensors == null) {
                this.requiredSensors= new ArrayList<PropertyValue>();
            }

            if (!value.equals(null)) {
                if (value instanceof JSONArray) {

                    JSONArray arrVals = (JSONArray) value;

                    if (arrVals != null) {
                        for (int i = 0; i < arrVals.length(); i++) {
                            try {
                                PropertyValue results = new PropertyValue();
                                results.readFromJSONObject(arrVals.getJSONObject(i));

                                this.requiredSensors.add(results);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                }
            }
        }else if (key.equalsIgnoreCase("debitCodes")) {

            if (this.debitCodes == null) {
                this.debitCodes= new ArrayList<PropertyValue>();
            }

            if (!value.equals(null)) {
                if (value instanceof JSONArray) {

                    JSONArray arrVals = (JSONArray) value;

                    if (arrVals != null) {
                        for (int i = 0; i < arrVals.length(); i++) {
                            try {
                                PropertyValue results = new PropertyValue();
                                results.readFromJSONObject(arrVals.getJSONObject(i));

                                this.debitCodes.add(results);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                }
            }
        }else if (key.equalsIgnoreCase("snrCodes")) {

            if (this.snrCodes == null) {
                this.snrCodes= new ArrayList<PropertyValue>();
            }

            if (!value.equals(null)) {
                if (value instanceof JSONArray) {

                    JSONArray arrVals = (JSONArray) value;

                    if (arrVals != null) {
                        for (int i = 0; i < arrVals.length(); i++) {
                            try {
                                PropertyValue results = new PropertyValue();
                                results.readFromJSONObject(arrVals.getJSONObject(i));

                                this.snrCodes.add(results);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                }
            }
        }else if (key.equalsIgnoreCase("scnList")) {

            if (this.scnList == null) {
                this.scnList= new ArrayList<PropertyValue>();
            }

            if (!value.equals(null)) {
                if (value instanceof JSONArray) {

                    JSONArray arrVals = (JSONArray) value;

                    if (arrVals != null) {
                        for (int i = 0; i < arrVals.length(); i++) {
                            try {
                                PropertyValue results = new PropertyValue();
                                results.readFromJSONObject(arrVals.getJSONObject(i));

                                this.scnList.add(results);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                }
            }
        } else {
            super.setValueForKey(value, key);
        }
    }




}
