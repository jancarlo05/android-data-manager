package com.procuro.apimmdatamanagerlib;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.util.Log;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

/**
 * Created by jophenmarieweeks on 07/07/2017.
 */


public class LatLon extends PimmBaseObject {
    public double Lat;
    public double Lon;
    public Context mContext;


    public void setContext(Context ctx) {
        this.mContext = ctx;
    }

    @Override
    public void setValueForKey(Object value, String key) {
        super.setValueForKey(value, key);

        if (key.equalsIgnoreCase("Lat")) {
            if (value != null) {
                if (!value.toString().equalsIgnoreCase("null")){
                    this.Lat = (double) value;
                }
            }
        }
        if (key.equalsIgnoreCase("Lon")) {
            if (value != null) {
                if (!value.toString().equalsIgnoreCase("null")){
                    this.Lon = (double) value;
                }
            }
        }
    }


    public HashMap<String,Object> dictionaryWithValuesForKeys() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();

        dictionary.put("Lat", this.Lat);
        dictionary.put("Lon", this.Lon);

        return dictionary;
    }


     public void getAddressComponentsWithCallback(OnCompleteListeners.getAddressComponentsWithCallbackListener listener){

            final OnCompleteListeners.getAddressComponentsWithCallbackListener getAddress = listener;

             Geocoder geoCoder = new Geocoder(mContext, Locale.getDefault());
    //         double mLat = 14.8386;
    //         double mLong = 120.2842;

             try {
                 List<Address> addresses = geoCoder.getFromLocation(this.Lat, this.Lon, 1);

                 for (Address address : addresses) {
                     AddressComponents addressComponents = new AddressComponents();
                     addressComponents.Address1 = address.getAddressLine(0);
                     addressComponents.Street = address.getAddressLine(1);
                     addressComponents.City = address.getSubLocality() == null ? address.getSubAdminArea() : address.getLocality();
                     addressComponents.Province = address.getLocality() == null ? address.getAdminArea() : address.getLocality();
                     addressComponents.Country = address.getCountryName();
                     addressComponents.Postal = address.getPostalCode();

                     listener.getAddressComponentsWithCallback(addressComponents ,null);
                 }
             } catch (IOException e) {
                 e.printStackTrace();
                 listener.getAddressComponentsWithCallback(null, new Error(e.getMessage()));
                 Log.e("DM","Reverse Geocode failed With Error:" + e.getMessage() );
                 Log.e("DM","Reverse Geocode Failed for Location Value Lat:Lon :" + this.Lat + ", "  + this.Lon);

             }

         }


}
