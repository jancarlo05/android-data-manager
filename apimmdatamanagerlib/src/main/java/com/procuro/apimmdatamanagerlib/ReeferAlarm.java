package com.procuro.apimmdatamanagerlib;

import java.util.Date;

/**
 * Created by jophenmarieweeks on 12/07/2017.
 */

public class ReeferAlarm extends PimmBaseObject {

    public enum ReeferAlarmResolution
    {
        ReeferAlarmResolution_Unresolved(0), //Value null coming from server will get mapped to '0' value
        ReeferAlarmResolution_Automatic(1),
        ReeferAlarmResolution_Explicit(2);

        private int value;
        private ReeferAlarmResolution(int value)
        {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }

        public static ReeferAlarmResolution setIntValue (int i) {
            for (ReeferAlarmResolution type : ReeferAlarmResolution.values()) {
                if (type.value == i) { return type; }
            }
            return ReeferAlarmResolution_Unresolved;
        }
    }

    public String reeferAlarmId;
    public String deviceId;
    public Date startTime;
    public Date endTime;
    public  int alarmCode;
    public  int severity;
//public  int compartment;
    public double compartment;

    public ReeferAlarmResolution resolution;
    public String resolvedBy;


    @Override
    public void setValueForKey(Object value, String key) {

        if(key.equalsIgnoreCase("resolution")) {
            if (!value.equals(null)) {
                ReeferAlarmResolution treshold = ReeferAlarmResolution.setIntValue((int) value);
                this.resolution = treshold;
            }

        } else {
            super.setValueForKey(value, key);
        }
    }


}
