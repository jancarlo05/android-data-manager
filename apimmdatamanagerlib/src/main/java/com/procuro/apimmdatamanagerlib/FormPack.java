package com.procuro.apimmdatamanagerlib;

import android.util.JsonReader;

import java.util.Date;

public class FormPack extends PimmBaseObject{

    public String id;
    public String formpack;
    public String version;
    public String deviceType;
    public String versionDetails;
    public String publishDate;
    public String publisher;
    public String fileName;
    public String fileId;
    public Date startDate;
    public Date endDate;
    public Date name;

    @Override
    public void setValueForKey(Object value, String key) {
        super.setValueForKey(value, key);

        if (key.equalsIgnoreCase("startDate")){
            if (value!=null){
                if (!value.toString().equalsIgnoreCase("null")){
                    this.startDate = JSONDate.convertJSONDateToNSDate(value.toString());
                }
            }
        }

        if (key.equalsIgnoreCase("endDate")){
            if (value!=null){
                if (!value.toString().equalsIgnoreCase("null")){
                    this.startDate = JSONDate.convertJSONDateToNSDate(value.toString());
                }
            }
        }
    }

}
