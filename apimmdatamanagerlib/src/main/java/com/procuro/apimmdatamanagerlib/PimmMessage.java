package com.procuro.apimmdatamanagerlib;

import org.json.JSONObject;

import java.util.Date;

/**
 * Created by jophenmarieweeks on 06/07/2017.
 */

public class PimmMessage extends PimmBaseObject {

    public String pimmMessageId;
    public String senderUserId;
    public String senderUserName;
    public String recipientUserId;
    public String recipientUserName;
    public String body;
    public Date sentUTC;
    public Date receivedUTC;
    public Date ackUTC;

    public int priority;
    public String refx;
    public String recipientId;
    public String RecipientName;

    public PimmMessageStatusEnum status;
    public PimmRefTypeEnum refType;


    public void setValueForKey(Object value, String key) {
        if (key.equalsIgnoreCase("status")) {
            PimmMessageStatusEnum pimmMessageStatusEnum = new PimmMessageStatusEnum();
            JSONObject jsonObject = (JSONObject) value;
            pimmMessageStatusEnum.readFromJSONObject(jsonObject);
            this.status = pimmMessageStatusEnum;

        }else if (key.equalsIgnoreCase("refType")) {
            PimmRefTypeEnum pimmRefTypeEnum = new PimmRefTypeEnum();
            JSONObject jsonObject = (JSONObject) value;
            pimmRefTypeEnum.readFromJSONObject(jsonObject);
            this.refType = pimmRefTypeEnum;

        } else {
            super.setValueForKey(value, key);
        }
    }
}
