package com.procuro.apimmdatamanagerlib;

import org.json.JSONObject;

import java.util.Date;
import java.util.HashMap;

/**
 * Created by jophenmarieweeks on 10/07/2017.
 */

public class SdrStopMetric extends PimmBaseObject {

    public int Avg;
    public String AvgText;
 //   @property (nonatomic,assign) MetricCompartment Compartment;

    //public MetricCompartment Compartment;
    public String CompartmentName;
    public String StopMetricId;
    public boolean isStatus;
    public String Label;
    public Date LastTimestamp;
    public String LastTimestampText;
    public int LastValue;
    public String LastValueText;
    public int Max;
    public String MaxText;
//    @property (nonatomic, assign) MetricClass MetricClass;
//    public MetricClass MetricClass;
    public SdrMetric.MetricClass MetricClass;
    public SdrMetric.MetricCompartment Compartment;
    public int Min;
    public String MinText;
    public boolean OOS;
    public double Precool;
    public boolean Primary;
    public int ProductSource;
    public String DeliveryMetricId;
    public int Rules;
    public String RulesName;

    @Override
    public void setValueForKey(Object value, String key) {

        if (key.equalsIgnoreCase("LastValue")) {
            this.LastValue = DMUtils.getInstance().getIntValue((!(value == null) ? 0 : (int) value));
        } else if (key.equalsIgnoreCase("Min")) {
            this.Min = DMUtils.getInstance().getIntValue((!(value == null) ? 0 : (int) value));
        } else if (key.equalsIgnoreCase("Max")) {
            this.Max = DMUtils.getInstance().getIntValue((!(value == null) ? 0 : (int) value));
        } else if (key.equalsIgnoreCase("Avg")){
            this.Avg = DMUtils.getInstance().getIntValue((!(value == null) ? 0 : (int) value));
        }else if (key.equalsIgnoreCase("ProductSource")) {
            this.ProductSource = DMUtils.getInstance().getIntValue((!(value == null) ? 0 : (int) value));
        }else if (key.equalsIgnoreCase("Rules")) {
            this.Rules = DMUtils.getInstance().getIntValue((!(value == null) ? 0 : (int) value));
        }else if (key.equalsIgnoreCase("LastTimestamp")) {
            this.LastTimestamp = JSONDate.convertJSONDateToNSDate(String.valueOf(value));
        } else if (key.equalsIgnoreCase("MetricClass")) {
            if (!value.equals(null)) {
                SdrMetric.MetricClass mClass = SdrMetric.MetricClass.setIntValue((int) value);
                this.MetricClass = mClass;
            }
        } else if (key.equalsIgnoreCase("Compartment")) {
            if (!value.equals(null)) {
                SdrMetric.MetricCompartment metricCompartment = SdrMetric.MetricCompartment.setIntValue((int) value);
                this.Compartment = metricCompartment;
            }
        }else {
            super.setValueForKey(value, key);
        }
    }

    public HashMap<String,Object> dictionaryWithValuesForKeys() {

        HashMap<String, Object> dictionary = new HashMap<String, Object>();

        dictionary.put("CompartmentName", this.Compartment);
        dictionary.put("StopMetricId", this.StopMetricId);
        dictionary.put("isStatus", this.isStatus);
        dictionary.put("Label", this.Label);
        dictionary.put("LastTimestamp", this.LastTimestamp);
        dictionary.put("LastTimestampText", this.LastTimestampText);
        dictionary.put("LastValue", this.LastValue);
        dictionary.put("LastValueText", this.LastValueText);
        dictionary.put("Max", this.Max);
        dictionary.put("MaxText", this.MaxText);
        dictionary.put("Min", this.Min);
        dictionary.put("MinText", this.MinText);
        dictionary.put("OOS", this.OOS);
        dictionary.put("Precool", this.Precool);
        dictionary.put("Primary", this.Primary);
        dictionary.put("ProductSource", this.ProductSource);
        dictionary.put("DeliveryMetricId", this.DeliveryMetricId);
        dictionary.put("Rules", this.Rules);
        dictionary.put("RulesName", this.RulesName);
        dictionary.put("MetricClass", this.MetricClass);
        dictionary.put("Compartment", this.Compartment);

        return dictionary;

    }
}
