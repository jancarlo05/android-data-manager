package com.procuro.apimmdatamanagerlib;

import android.util.Log;

/**
 * Created by jophenmarieweeks on 06/07/2017.
 */

public class DeliveryPOITypeEnum extends PimmBaseObject {


    //public enum DeliveryPOITypeEnum { Duplicate type error
    public enum DeliveryPOITypesEnum {

        DeliveryPOIType_Undefined(-1),
        DeliveryPOIType_DeliveryStop (0), // mainly used for reporting
        DeliveryPOIType_DynamicStop(1),
        DeliveryPOIType_FuelStop(2),
        DeliveryPOIType_PickUpStop(3), //Plant PU
        DeliveryPOIType_OutOfTerritory(4),
        DeliveryPOIType_RestStop(5),
        DeliveryPOIType_Inspection(6),
        DeliveryPOIType_Service(7),
        DeliveryPOIType_Layover(8),
        DeliveryPOIType_CoffeeStop(9),
        DeliveryPOIType_Sleeper(10),
        DeliveryPOIType_Traffic(11),
        DeliveryPOIType_TruckWash(13),
        DeliveryPOIType_Shuttle(20),
        DeliveryPOIType_Depot(21),
        DeliveryPOIType_StartPoint(50),
        DeliveryPOIType_EndPoint(51),
        DeliveryPOIType_PlannedStop(60),
        DeliveryPOIType_DCFacility(61),
        DeliveryPOIType_StoppedWhileUnassigned(62),
        DeliveryPOIType_Unscheduled (100),
        DeliveryPOIType_Unauthorized(101);

        private int value;
        private DeliveryPOITypesEnum(int value)
        {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }

        public static DeliveryPOITypesEnum setIntValue (int i) {
            for (DeliveryPOITypesEnum type : DeliveryPOITypesEnum.values()) {
                if (type.value == i) { return type; }
            }
            return DeliveryPOIType_Undefined;
        }



    }

   public enum DeliveryPOIStatusEnum {
        DeliveryPOIStatus_Unknown(0),
        DeliveryPOIStatus_AtLocation(1),
        DeliveryPOIStatus_Departed(2);

        private int value;
        private DeliveryPOIStatusEnum(int value)
        {
            this.value = value;
        }

        public int getValue() {
           return this.value;
       }

       public static DeliveryPOIStatusEnum setIntValue (int i) {
           for (DeliveryPOIStatusEnum type : DeliveryPOIStatusEnum.values()) {
               if (type.value == i) { return type; }
           }
           return DeliveryPOIStatus_Unknown;
       }

   }

    public DeliveryPOITypesEnum delPOItype;
    public DeliveryPOIStatusEnum delPOIStatus;

    @Override
    public void setValueForKey(Object value, String key) {

        if(key.equalsIgnoreCase("delPOItype")) {
            if (!value.equals(null)) {
                DeliveryPOITypesEnum deliveryPOITypes = DeliveryPOITypesEnum.setIntValue((int) value);
                this.delPOItype = deliveryPOITypes;

                Log.v("DM", " delPOItype Value: " + this.delPOItype);
            }
        }else if(key.equalsIgnoreCase("delPOIStatus")){
            if (!value.equals(null)) {
                DeliveryPOIStatusEnum deliveryPOIStatus = DeliveryPOIStatusEnum.setIntValue((int) value);
                this.delPOIStatus = deliveryPOIStatus;

                Log.v("DM", " delPOIStatus Value: " + this.delPOIStatus);
            }
        } else {
            super.setValueForKey(value, key);
        }
    }
}




