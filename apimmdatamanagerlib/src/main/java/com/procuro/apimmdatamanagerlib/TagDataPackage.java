package com.procuro.apimmdatamanagerlib;

import java.util.ArrayList;

/**
 * Created by jophenmarieweeks on 01/08/2017.
 */

public class TagDataPackage extends PimmBaseObject {
    public Boolean timestampIsUtc;
    public ArrayList<TagDataPost> dataPostList;
//    @property (nonatomic, retain) NSMutableArray<TagDataPost *> *dataPostList;

}
