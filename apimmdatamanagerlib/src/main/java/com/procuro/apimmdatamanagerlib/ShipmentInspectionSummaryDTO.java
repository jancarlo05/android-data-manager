package com.procuro.apimmdatamanagerlib;

import java.util.Date;

/**
 * Created by jophenmarieweeks on 11/10/2017.
 */

public class ShipmentInspectionSummaryDTO extends PimmBaseObject {


//Scores are integers.  Scores will be assigned using these rules by default:
//5 (A) if you fill out all the inspection forms and there are no ͞fail͟ answers
//4 (B )if you fill out all the inspection forms and there is at least one ͞fail͟ answer
//3 (C )if you skip at least one form, but there are no ͞fail͟ answers
//2 (D) if you skip at least one form, and there is at least one ͞fail͟ answer
//1 (F) if you don’t fill out any of the forms

    public String shipmentId;
    public int inboundScore; //int on server side
    public String inboundUserId;
    public String inboundUserName;
    public Date inboundCompletionDate;
    public double outboundScore;
    public String outboundUserId;
    public String outboundUserName;
    public Date outboundCompletionDate;

}
