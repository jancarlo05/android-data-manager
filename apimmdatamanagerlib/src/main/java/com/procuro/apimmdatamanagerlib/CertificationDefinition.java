package com.procuro.apimmdatamanagerlib;

import java.util.HashMap;

public class CertificationDefinition extends PimmBaseObject {

    public String userCertificationDefinitionID;
    public String customerID;
    public String certification;
    public String category;
    public String description;


    public HashMap<String,Object> dictionaryWithValuesForKeys() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();

        dictionary.put("userCertificationDefinitionID", this.userCertificationDefinitionID);
        dictionary.put("customerID", this.customerID);
        dictionary.put("certification", this.certification);
        dictionary.put("category", this.category);
        dictionary.put("description", this.description);
        return dictionary;

    }


}
