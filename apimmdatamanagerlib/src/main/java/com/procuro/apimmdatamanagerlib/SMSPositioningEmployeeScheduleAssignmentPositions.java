package com.procuro.apimmdatamanagerlib;

import org.json.JSONObject;

import java.util.Date;
import java.util.HashMap;

public class SMSPositioningEmployeeScheduleAssignmentPositions extends PimmBaseObject {

   public Date timeIn;
   public Date timeOut;
   public Date assignedDateISO;
   public Date assignedDate;

   public int position;
   public String color;
   public boolean opsLeader;
   public boolean headphone;
   public String positionName;
   public boolean supportManager;
   public String employeeID;
   public String employee;

    @Override
    public void setValueForKey(Object value, String key) {
        super.setValueForKey(value, key);

       if (key.equalsIgnoreCase("timeIn")){
           if (value!=null){
               this.timeIn = JSONDate.convertJSONDateToNSDate(value.toString());
           }
       }
        if (key.equalsIgnoreCase("timeOut")){
            if (value!=null){
                this.timeOut = JSONDate.convertJSONDateToNSDate(value.toString());
            }
        }
        if (key.equalsIgnoreCase("assignedDateISO")){
            if (value!=null){
                this.assignedDateISO = JSONDate.convertJSONDateToNSDate(value.toString());
            }
        }
        if (key.equalsIgnoreCase("assignedDate")){
            if (value!=null){
                this.assignedDate = JSONDate.convertJSONDateToNSDate(value.toString());
            }
        }
        if (key.equalsIgnoreCase("position")){
            if (value!=null){
                if (!value.toString().equalsIgnoreCase("null")){
                    this.position = (int) value;
                }
            }
        }
        if (key.equalsIgnoreCase("opsLeader")){
            if (value!=null){
                if (!value.toString().equalsIgnoreCase("null")){

                    if (value.toString().equalsIgnoreCase("0")){
                        this.opsLeader = false;
                    }else if (value.toString().equalsIgnoreCase("1")){
                        this.opsLeader = true;
                    }else {
                        this.opsLeader = (boolean) value;
                    }
                }

            }
        }
        if (key.equalsIgnoreCase("headphone")){
            if (value!=null){
                if (!value.toString().equalsIgnoreCase("null")){
                    if (value.toString().equalsIgnoreCase("0")){
                        this.headphone = false;
                    }else if (value.toString().equalsIgnoreCase("1")){
                        this.headphone = true;
                    }else {
                        this.headphone = (boolean) value;
                    }
                }
            }
        }
        if (key.equalsIgnoreCase("positionName")){
            if (value!=null){
                this.positionName = (String) value.toString();
            }
        }
        if (key.equalsIgnoreCase("supportManager")){
            if (value!=null){
                if (!value.toString().equalsIgnoreCase("null")){
                    if (value.toString().equalsIgnoreCase("0")){
                        this.supportManager = false;
                    }else if (value.toString().equalsIgnoreCase("1")){
                        this.supportManager = true;
                    }else {
                        this.supportManager = (boolean) value;
                    }
                }
            }
        }
        if (key.equalsIgnoreCase("employeeID")){
            if (value!=null){
                this.employeeID = (String) value.toString();
            }
        }
        if (key.equalsIgnoreCase("employee")){
            if (value!=null){
                this.employee = (String) value.toString();
            }
        }

    }


    public HashMap<String,Object> dictionaryWithValuesForKeys() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();

        dictionary.put("timeIn", this.timeIn);
        dictionary.put("timeOut", this.timeOut);
        dictionary.put("assignedDateISO", this.assignedDateISO);
        dictionary.put("assignedDate", this.assignedDate);
        dictionary.put("position", this.position);
        dictionary.put("opsLeader", this.opsLeader);
        dictionary.put("headphone", this.headphone);
        dictionary.put("positionName", this.positionName);
        dictionary.put("supportManager", this.supportManager);
        dictionary.put("employeeID", this.employeeID);
        dictionary.put("employee", this.employee);
        dictionary.put("color", this.color);

        return dictionary;
    }



}
