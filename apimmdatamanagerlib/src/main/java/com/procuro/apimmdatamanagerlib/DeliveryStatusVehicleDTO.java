package com.procuro.apimmdatamanagerlib;

import static android.R.attr.value;

/**
 * Created by jophenmarieweeks on 12/07/2017.
 */

public class DeliveryStatusVehicleDTO extends PimmBaseObject {

    public boolean OnTheRoad;
    public DeliveryPOITypeEnum.DeliveryPOITypesEnum AtLocation;
    public StoreDeliveryEnums.GenericOnOffEnum ReeferPower;
    public StoreDeliveryEnums.DoorAccessEnum DoorStatus;

    @Override
    public void setValueForKey(Object value, String key) {

        if (key.equalsIgnoreCase("AtLocation")) {

            DeliveryPOITypeEnum.DeliveryPOITypesEnum trans = DeliveryPOITypeEnum.DeliveryPOITypesEnum.setIntValue((int) value);
            this.AtLocation = trans;

        } else if (key.equalsIgnoreCase("ReeferPower")) {

            StoreDeliveryEnums.GenericOnOffEnum genonof = StoreDeliveryEnums.GenericOnOffEnum.valueOf(value.toString());
            this.ReeferPower = genonof;

        } else if (key.equalsIgnoreCase("DoorStatus")) {

            StoreDeliveryEnums.DoorAccessEnum doorAccessEnum = StoreDeliveryEnums.DoorAccessEnum.valueOf(value.toString());
            this.DoorStatus = doorAccessEnum;

        } else {
            super.setValueForKey(value, key);
        }
    }
}

