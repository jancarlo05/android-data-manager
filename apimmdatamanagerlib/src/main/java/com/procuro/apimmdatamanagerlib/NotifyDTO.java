package com.procuro.apimmdatamanagerlib;

import java.security.PublicKey;
import java.util.Date;

/**
 * Created by jophenmarieweeks on 12/07/2017.
 */

public class NotifyDTO extends PimmBaseObject {

   public enum NotifyStatusEnum {
        NotifyStatus_Queued(0),
        NotifyStatus_Success(1),
        NotifyStatus_Failed(2),
        NotifyStatus_Cancelled(3),
        NotifyStatus_DeadLeeter(4);

       private int value;
       private NotifyStatusEnum(int value)
       {
           this.value = value;
       }

       public int getValue() {
           return this.value;
       }

       public static NotifyStatusEnum setIntValue (int i) {
           for (NotifyStatusEnum type : NotifyStatusEnum.values()) {
               if (type.value == i) { return type; }
           }
           return NotifyStatus_Queued;
       }


    }


    public Date created;  // date/time notification request was made
    public Date sendDate; // date/time notification was processed
    public NotifyStatusEnum status; // current status.
    public String userId; // pimm userid of user who made request (typically an admin account set up specially for notifications)
    public String sendTo; // recipient
    public String type; // data type, corresponds to the type of report, not the format of the data
    public String customerId; // not yet used
    public String siteId; // not yet used
    public String deliveryId; // shipmentID
    public String stopId; // store's stopId (not the siteID of the store, but the RouteStatus stopID

    @Override
    public void setValueForKey(Object value, String key) {

        if (key.equalsIgnoreCase("status")) {

            if (!value.equals(null)) {
                NotifyStatusEnum notifyStatusEnum = NotifyStatusEnum.setIntValue((int) value);
                this.status = notifyStatusEnum;
            }

        }else {
            super.setValueForKey(value, key);
        }
    }



}
