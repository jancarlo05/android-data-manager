package com.procuro.apimmdatamanagerlib;

/**
 * Created by jophenmarieweeks on 11/10/2017.
 */

public class ShipmentProductInspectionRules extends PimmBaseObject {


    public String shipmentProductInspectionRulesID;
    public String shipmentID;
    public String customerID;
    public String instructions;
    public String correctiveAction;
    public int binsToSample;
    public double binsToSamplePercent;
    public int itemsPerBin;


}
