package com.procuro.apimmdatamanagerlib;

/**
 * Created by jophenmarieweeks on 12/07/2017.
 */

public class StoreDeliveryEnums extends PimmBaseObject {

   public enum TransportStatus {
        TransportStatus_Unknown(0), /// Missing information to actually run route (start time, trailer, plant, dc?)
        TransportStatus_Unrunnable(1),/// Ready, but before start time
        TransportStatus_Scheduled(2), /// At plant, recording data
        TransportStatus_Loading(3),///// Dispatch time passed, but not departed.//Delayed = 4, // Just a placeholde for now./// In shuttle transport.
        TransportStatus_Shuttling(5), // Just a placeholde for now./// In post shuttle wait for route run.
        TransportStatus_PostShuttle(6), // Just a placeholde for now.
        TransportStatus_InRoute(7),/// Out running deliviery  ///// Trailer is in pre-selected DC, but delivery is still executing.
        TransportStatus_Complete(9),         //AtDC = 8,  // Just a placeholde for now.  /// Complete
        TransportStatus_Killed(10),
        TransportStatus_TrailerOutOfService(11);

        private int value;
        private TransportStatus(int value)
        {
            this.value = value;
        }

         public int getValue() {
           return this.value;
       }

         public static TransportStatus setIntValue (int i) {
           for (TransportStatus type : TransportStatus.values()) {
               if (type.value == i) { return type; }
           }
           return TransportStatus_Unknown;
        }


   }

    public enum LifecycleState {
        LifecycleState_Unknown(0),
        LifecycleState_Scheduled(1),
        LifecycleState_Active(2),
        LifecycleState_Terminated(3);

        private int value;
        private LifecycleState(int value)
        {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }

        public static LifecycleState setIntValue (int i) {
            for (LifecycleState type : LifecycleState.values()) {
                if (type.value == i) { return type; }
            }
            return LifecycleState_Unknown;
        }

    }

   public enum VehicleStatus {
        VehicleStatus_Stopped(0),
        VehicleStatus_Moving(1);

        private int value;
        private VehicleStatus(int value)
        {
            this.value = value;
        }

       public int getValue() {
           return this.value;
       }

       public static VehicleStatus setIntValue (int i) {
           for (VehicleStatus type : VehicleStatus.values()) {
               if (type.value == i) { return type; }
           }
           return VehicleStatus_Stopped;
       }

   }

   public enum PODDeliveryStatusEnum {

        DeliveryStatus_NOPODInformationAvailable(0),
        DeliveryStatus_DeliveryOK(1),
        DeliveryStatus_ProductDiscrepancy(2);

       private int value;
       private PODDeliveryStatusEnum(int value)
       {
           this.value = value;
       }

       public int getValue() {
           return this.value;
       }

       public static PODDeliveryStatusEnum setIntValue (int i) {
           for (PODDeliveryStatusEnum type : PODDeliveryStatusEnum.values()) {
               if (type.value == i) { return type; }
           }
           return DeliveryStatus_NOPODInformationAvailable;
       }

    }

   public enum DeliveryStatus {
        DeliveryStatus_Unknown(-1),
        DeliveryStatus_PendingLoad(0),
        DeliveryStatus_PendingShuttleDeparture(6),
        DeliveryStatus_PendingShuttleDepartureWithViolation(7),
        DeliveryStatus_InShuttleTransit(8),
        DeliveryStatus_InShuttleTransitWithViolation(9),
        DeliveryStatus_Loading(10),
        DeliveryStatus_LoadingWithViolation(11),
        DeliveryStatus_InRoute(12),
        DeliveryStatus_InRouteWithViolation(13),
        DeliveryStatus_Complete(14),
        DeliveryStatus_CompleteWithViolation(15),
        DeliveryStatus_TrailerOutOfService(16),
        DeliveryStatus_UserTerminated(18),
       DeliveryStatus_UserTerminatedWithViolation(19),
        DeliveryStatus_Locked(30);

       private int value;
       private DeliveryStatus(int value)
       {
           this.value = value;
       }

       public int getValue() {
           return this.value;
       }

       public static DeliveryStatus setIntValue (int i) {
           for (DeliveryStatus type : DeliveryStatus.values()) {
               if (type.value == i) { return type; }
           }
           return DeliveryStatus_Unknown;
       }
    }


  public enum GenericOnOffEnum {

        GenericOnOff_None(0),
        GenericOnOff_Off(1),
        GenericOnOff_On(2);

      private int value;
      private GenericOnOffEnum(int value)
      {
          this.value = value;
      }

      public int getValue() {
          return this.value;
      }

      public static GenericOnOffEnum setIntValue (int i) {
          for (GenericOnOffEnum type : GenericOnOffEnum.values()) {
              if (type.value == i) { return type; }
          }
          return GenericOnOff_None;
      }

  }


   public enum DoorAccessEnum {

        DoorAccess_None0(0),
        DoorAccess_Close(1),
        DoorAccess_Open(2);

       private int value;
       private DoorAccessEnum(int value)
       {
           this.value = value;
       }

       public int getValue() {
           return this.value;
       }

       public static DoorAccessEnum setIntValue (int i) {
           for (DoorAccessEnum type : DoorAccessEnum.values()) {
               if (type.value == i) { return type; }
           }
           return DoorAccess_None0;
       }
    }


    public TransportStatus transportStatus;
    public LifecycleState lifecycleState;
    public VehicleStatus vehicleStatus;
    public PODDeliveryStatusEnum podDeliveryStatusEnum;
    public DeliveryStatus deliveryStatus;
    public GenericOnOffEnum genericOnOffEnum;
    public DoorAccessEnum doorAccessEnum;

    @Override
    public void setValueForKey(Object value, String key) {

        if(key.equalsIgnoreCase("transportStatus")) {
            if (!value.equals(null)) {
                TransportStatus trans = TransportStatus.setIntValue((int) value);
                this.transportStatus = trans;
            }

        }else if(key.equalsIgnoreCase("lifecycleState")){
            if (!value.equals(null)) {
                LifecycleState lifecycle = LifecycleState.setIntValue((int) value);
                this.lifecycleState = lifecycle;
            }

         }else if(key.equalsIgnoreCase("vehicleStatus")){
            if (!value.equals(null)) {
                VehicleStatus vehicle = VehicleStatus.setIntValue((int) value);
                this.vehicleStatus = vehicle;
            }

         }else if(key.equalsIgnoreCase("deliveryStatus")) {
            if (!value.equals(null)) {
                DeliveryStatus delivery = DeliveryStatus.setIntValue((int) value);
                this.deliveryStatus = delivery;
            }

         }else if(key.equalsIgnoreCase("podDeliveryStatusEnum")) {
            if (!value.equals(null)) {
                PODDeliveryStatusEnum podDeliveryStatusEnum = PODDeliveryStatusEnum.setIntValue((int) value);
                this.podDeliveryStatusEnum = podDeliveryStatusEnum;
            }

         }else if(key.equalsIgnoreCase("genericOnOffEnum")) {
            if (!value.equals(null)) {
                GenericOnOffEnum genericOnOffEnum = GenericOnOffEnum.setIntValue((int) value);
                this.genericOnOffEnum = genericOnOffEnum;
            }

         }else if(key.equalsIgnoreCase("doorAccessEnum")) {
            if (!value.equals(null)) {
                DoorAccessEnum DoorAccess = DoorAccessEnum.setIntValue((int) value);
                this.doorAccessEnum = DoorAccess;
            }


        } else {
            super.setValueForKey(value, key);
        }
    }



}
