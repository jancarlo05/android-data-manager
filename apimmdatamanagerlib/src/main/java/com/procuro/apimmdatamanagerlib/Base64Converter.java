package com.procuro.apimmdatamanagerlib;

import android.util.Base64;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

/**
 * Created by EmmanKusumi on 7/4/17.
 */

public class Base64Converter
{
    static public String toBase64(String string) {
        String b64 = null;
        try {
            byte[] data = string.getBytes(StandardCharsets.US_ASCII);
            b64 = Base64.encodeToString(data, Base64.DEFAULT);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return b64;
    }

    static public String toUrlEncodedBase64(String string) {
        String b64 = null;
        try {
            byte[] data = string.getBytes("UTF-8");
            b64 = Base64.encodeToString(data, Base64.DEFAULT);
            b64 = URLEncoder.encode(b64, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return b64;
    }
    static public String ByteToUrlStringBase64(byte[] data) {
        String b64 = null;
        //            byte[] data = string.getBytes("UTF-8");
        b64 = Base64.encodeToString(data, Base64.DEFAULT);
        return b64;
//        return b64;
    }
}
