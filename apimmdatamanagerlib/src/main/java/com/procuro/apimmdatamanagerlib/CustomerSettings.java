package com.procuro.apimmdatamanagerlib;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jophenmarieweeks on 06/07/2017.
 */

public class CustomerSettings extends PimmBaseObject {

    public String displaySensorMatch;

    public ArrayList<String> settings;
    public ArrayList<String> requiredSensors;
    public ArrayList<String> displaySensors;
    public ArrayList<String> applications;

//    @Override
//    public void setValueForKey(Object value, String key)
//    {
//        if(key.equalsIgnoreCase("settings"))
//        {
//            if(this.settings == null)
//            {
//                this.settings = new ArrayList<String>();
//            }
//
//            List<String> settings = (List<String>) value;
//            for(String setting : settings)
//            {
//                this.settings.add(setting);
//            }
//        }
//        else if(key.equalsIgnoreCase("requiredSensors"))
//        {
//            if(this.requiredSensors == null)
//            {
//                this.requiredSensors = new ArrayList<String>();
//            }
//
//            List<String> requiredSensors = (List<String>) value;
//            for(String requiredSensor : requiredSensors)
//            {
//                this.requiredSensors.add(requiredSensor);
//            }
//        }
//        else if(key.equalsIgnoreCase("displaySensors"))
//        {
//            if(this.displaySensors == null)
//            {
//                this.displaySensors = new ArrayList<String>();
//            }
//
//            List<String> displaySensors = (List<String>) value;
//            for(String displaySensor : displaySensors)
//            {
//                this.displaySensors.add(displaySensor);
//            }
//        }
//        else if(key.equalsIgnoreCase("applications"))
//        {
//            if(this.applications == null)
//            {
//                this.applications = new ArrayList<String>();
//            }
//
//            List<String> applications = (List<String>) value;
//            for(String application : applications)
//            {
//                this.applications.add(application);
//            }
//        }
//        else
//        {
//            super.setValueForKey(value, key);
//        }
//    }
}
