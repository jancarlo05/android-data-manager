package com.procuro.apimmdatamanagerlib;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by jophenmarieweeks on 10/07/2017.
 */

public class SdrPointOfInterest extends PimmBaseObject {

    //Id
    public String SdrPointOfInterestId;
    public LatLon LatLon;

    //Type
    public DeliveryPOITypeEnum.DeliveryPOITypesEnum Type;
    public Date ArrivalTime;
    public Date DepartureTime;
    public double Duration;
    public double DoorOpened;
    public String NearestSiteId;
    public String NearestSiteName;
    public String NearestSiteAccountName;
    public String Label;
    public String Caption;
    public String ArrivalTimeText;
    public String DepartureTimeText;
    public String ArrivalMonthDayText;
    public String DepartureMonthDayText;
    public String DurationText;
    public String DoorText;
    public String Address;
    public AddressComponents AddressComponents;
    public String LocationName;

//Array of SdrPOIMetric
    public ArrayList<SdrPOIMetric> Metrics;
    public double InitialType;
    public String OverrideId;
    public boolean isManual;
    public Date ManualArrivalTime;
    public Date ManualDepartureTime;
    public Date ComputedArrivalTime;
    public Date ComputedDepartureTime;
    public DeliveryPOITypeEnum.DeliveryPOIStatusEnum Status;
    public Fuel Fuel;

//-(NSMutableDictionary*) dictionaryWithValuesForKeys;


    @Override
    public void setValueForKey(Object value, String key) {

        if (key.equalsIgnoreCase("Metrics")) {
            if (this.Metrics == null) {
                this.Metrics = new ArrayList<SdrPOIMetric>();
            }

            if (!value.equals(null)) {
                JSONArray arrResults = (JSONArray) value;

                if (arrResults != null) {
                    for (int i = 0; i < arrResults.length(); i++) {
                        try {
                            SdrPOIMetric sdrPOIMetric = new SdrPOIMetric();
                            sdrPOIMetric.readFromJSONObject(arrResults.getJSONObject(i));

                            this.Metrics.add(sdrPOIMetric);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }

                Log.v("Data", "Value: " + this.Metrics);
            }
        } else if (key.equalsIgnoreCase("LatLon")) {

            Log.v("LatLon", "Value: " + value);

            if (!value.equals(null)) {
                JSONObject jsonObject = (JSONObject) value;

                LatLon latLon = new LatLon();

                try {
                    latLon.Lat = jsonObject.getDouble("Lat");
                    latLon.Lon = jsonObject.getDouble("Lon");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                this.LatLon = latLon;
            }

        } else if (key.equalsIgnoreCase("Type")) {

            DeliveryPOITypeEnum.DeliveryPOITypesEnum deliveryPOITypes = DeliveryPOITypeEnum.DeliveryPOITypesEnum.setIntValue((int) value);
            this.Type = deliveryPOITypes;


        }else if(key.equalsIgnoreCase("Status")){

                DeliveryPOITypeEnum.DeliveryPOIStatusEnum deliveryPOIStatus = DeliveryPOITypeEnum.DeliveryPOIStatusEnum.setIntValue((int) value);
                this.Status = deliveryPOIStatus;

                Log.v("DM", " delPOIStatus Value: " + this.Status);

        } else if (key.equalsIgnoreCase("AddressComponents")) {

            AddressComponents addressComponents = new AddressComponents();
            JSONObject jsonObject = (JSONObject) value;
            addressComponents.readFromJSONObject(jsonObject);
            this.AddressComponents = addressComponents;

        } else if (key.equalsIgnoreCase("Fuel")) {

            Fuel fuel = new Fuel();
            JSONObject jsonObject = (JSONObject) value;
            fuel.readFromJSONObject(jsonObject);
            this.Fuel = fuel;

        }else if (key.equalsIgnoreCase("ManualArrivalTime")){
            this.ManualArrivalTime = JSONDate.convertJSONDateToNSDate(String.valueOf(value));
        }else if (key.equalsIgnoreCase("ManualDepartureTime")){
            this.ManualDepartureTime = JSONDate.convertJSONDateToNSDate(String.valueOf(value));
        }else if (key.equalsIgnoreCase("ComputedArrivalTime")){
            this.ComputedArrivalTime = JSONDate.convertJSONDateToNSDate(String.valueOf(value));
        }else if (key.equalsIgnoreCase("ComputedDepartureTime")) {
            this.ComputedDepartureTime = JSONDate.convertJSONDateToNSDate(String.valueOf(value));
        } else if(key.equalsIgnoreCase("Id")) {
            this.SdrPointOfInterestId = value.toString();
        } else {
            super.setValueForKey(value, key);
        }

    }

    public HashMap<String,Object> dictionaryWithValuesForKeys() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();

        dictionary.put("Type", this.Type);
        dictionary.put("Status", this.Status);
        dictionary.put("Label", this.Label);
        dictionary.put("Caption", this.Caption);
        dictionary.put("DoorOpened", this.DoorOpened);

        dictionary.put("Id", this.SdrPointOfInterestId);
        dictionary.put("LatLon", (this.LatLon == null) ? null : this.LatLon.dictionaryWithValuesForKeys());



        return dictionary;
    }
}
