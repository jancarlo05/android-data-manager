package com.procuro.apimmdatamanagerlib;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by jophenmarieweeks on 15/08/2017.
 */

public class DCSiteDTO extends PimmBaseObject {


    public String siteid;
    public String sitename;
    public String address1;
    public String address2;
    public String city;
    public String state;
    public String postal;
    public int timezone;
    public boolean dst;
    public int effectiveUTCOffset;
    public String siteclass;
    public int type;
    public boolean hasGPS;
    public int lat;
    public int lon;
    public String pimmRegionCode;
    public ArrayList<PropertyValue> siteProperties;
    public String deliveryInstructions;
    public String gln;
    public SiteContact Contact;
    public String customerid;

    public ArrayList<AssetTrackingRegionDTO> assetTrackingRegions;


//    public  class AssetTrackingRegionMarkerDTO{
//        public double lat;
//        public double lon;
//    }

//    public static class AssetTrackingRegionDTO{
//        public String assetTrackingRegionId;
//        public String name;
//        public ArrayList<AssetTrackingRegionMarkerDTO> markers;
//    }


    @Override
    public void setValueForKey(Object value, String key) {

        if (key.equalsIgnoreCase("siteProperties")) {
            if (this.siteProperties == null) {
                this.siteProperties = new ArrayList<PropertyValue>();
            }

            if (!value.equals(null)) {
                JSONArray arrData = (JSONArray) value;

                if (arrData != null) {
                    for (int i = 0; i < arrData.length(); i++) {
                        try {
                            PropertyValue propertyValue = new PropertyValue();
                            propertyValue.readFromJSONObject(arrData.getJSONObject(i));

                            this.siteProperties.add(propertyValue);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        } else if (key.equalsIgnoreCase("assetTrackingRegions")) {
            if(key !=null) {
                SiteContact siteContact = new SiteContact();
                JSONObject jsonObject = (JSONObject) value;
                siteContact.readFromJSONObject(jsonObject);
                this.Contact = siteContact;
            }

        } else if (key.equalsIgnoreCase("assetTrackingRegions")) {
            if (this.assetTrackingRegions == null) {
                this.assetTrackingRegions = new ArrayList<AssetTrackingRegionDTO>();
            }

            if (!value.equals(null)) {
                JSONArray arrData = (JSONArray) value;

                if (arrData != null) {
                    for (int i = 0; i < arrData.length(); i++) {
                        try {
                            AssetTrackingRegionDTO assetTrackingRegionDTO = new AssetTrackingRegionDTO();
                            assetTrackingRegionDTO.readFromJSONObject(arrData.getJSONObject(i));

                            this.assetTrackingRegions.add(assetTrackingRegionDTO);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } else {
                super.setValueForKey(value, key);
            }
        }

    }
}
