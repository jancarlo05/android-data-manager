package com.procuro.apimmdatamanagerlib;

import java.util.ArrayList;

public class FSLSite extends PimmBaseObject {
    public String siteID;
    public String siteName;
    public String corpStructureID;
    public ArrayList<FSLAggregateSiteData> aggregateSiteDataList;

    @Override
    public void setValueForKey(Object value, String key) {
        super.setValueForKey(value, key);
    }

    public ArrayList<FSLAggregateSiteData> getAggregateSiteDataList() {
        return aggregateSiteDataList;
    }

    public void setAggregateSiteDataList(ArrayList<FSLAggregateSiteData> aggregateSiteDataList) {
        this.aggregateSiteDataList = aggregateSiteDataList;
    }
}
