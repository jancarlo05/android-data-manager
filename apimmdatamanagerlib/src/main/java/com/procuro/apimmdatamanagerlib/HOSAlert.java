package com.procuro.apimmdatamanagerlib;

import java.util.Date;

/**
 * Created by jophenmarieweeks on 10/07/2017.
 */

public class HOSAlert extends PimmBaseObject {
    public String hosAlertID;
    public String deliveryId;
    public String userID;
    public Date alertStartTimeUTC;
    public Date escalateTime1UTC;
    public Date escalateTime2UTC;
    public Date alertEndTimeUTC;
//    public Date alertStartTime;
//    public Date escalateTime1;
//    public Date escalateTime2;
//    public Date alertEndTime;
    public int severity;
    public HOSViolationTypeEnum.HOSViolationTypesEnum violationType;

    @Override
    public void setValueForKey(Object value, String key) {

        if(key.equalsIgnoreCase("violationType")) {
            if (!value.equals(null)) {
                HOSViolationTypeEnum.HOSViolationTypesEnum hosViolationTypesEnum = HOSViolationTypeEnum.HOSViolationTypesEnum.setIntValue((int) value);
                this.violationType = hosViolationTypesEnum;
            }

        } else {
            super.setValueForKey(value, key);
        }
    }

}
