package com.procuro.apimmdatamanagerlib;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by jophenmarieweeks on 06/07/2017.
 */

public class AddressComponents extends PimmBaseObject {

    public String Address1;
    public String Address2;
    public String City;
    public String State;
    public String Province;
    public String Postal;
    public String Country;
    public String StreetName;
    public String StreetNumber;
    public String Street;
    public String Zip;

    public String getLocationStringForHOS(){
        String city = this.City == null ? " ": this.City ;
        String state = this.State == null ? " ": this.State;
        String postal = this.Postal == null ? " " : this.Postal;
        String country = this.Country == null ? " " : this.Country;

        if(this.City != null) {
            String addrStr = String.format("%,%,%", city, postal, country);
            return addrStr;
        }else {
            return "";
        }

    }

    public String stringValue(){
        StringBuilder address = new StringBuilder();

        if(this.Address1 != null){
            address.append(this.Address1);
            address.append(" ");

        }

        if(this.City != null){
            address.append(this.City);
            address.append(" ");

        }

        if(this.Province != null){
            address.append(this.Province);
            address.append(" ");

        }

        if(this.Postal != null){
            address.append(this.Postal);

        }
        return address.toString();

    }

    @Override
    public void setValueForKey(Object value, String key) {
        super.setValueForKey(value, key);
    }

    public HashMap<String,Object> dictionaryWithValuesForKeys() {

        HashMap<String, Object> dictionary = new HashMap<String, Object>();

        dictionary.put("Address1", this.Address1);
        dictionary.put("Address2", this.Address2);
        dictionary.put("City", this.City);
        dictionary.put("Province", this.Province);
        dictionary.put("State", this.State);
        dictionary.put("Postal", this.Postal);
        dictionary.put("Country", this.Country);
        dictionary.put("StreetName", this.StreetName);
        dictionary.put("StreetNumber", this.StreetNumber);


        return dictionary;
    }


}
