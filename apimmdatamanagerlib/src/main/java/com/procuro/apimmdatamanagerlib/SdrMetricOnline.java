package com.procuro.apimmdatamanagerlib;

import java.util.Date;

/**
 * Created by jophenmarieweeks on 12/07/2017.
 */

public class SdrMetricOnline extends PimmBaseObject {

    public enum SdrOnlineStatus
    {
        Unknown(0),
        Offline(1),
        Online(2),
        OOS(3);

        private int value;
        private SdrOnlineStatus(int value)
        {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }

        public static SdrOnlineStatus setIntValue (int i) {
            for (SdrOnlineStatus type : SdrOnlineStatus.values()) {
                if (type.value == i) { return type; }
            }
            return Unknown;
        }

    }

    public String MetricId;
    public Date LastUpdate;
    public SdrOnlineStatus Status;

    @Override
    public void setValueForKey(Object value, String key) {

        if (key.equalsIgnoreCase("Status")) {

            if (!value.equals(null)) {
                SdrOnlineStatus onlineStatus = SdrOnlineStatus.setIntValue((int) value);
                this.Status = onlineStatus;
            }

        }else {
            super.setValueForKey(value, key);
        }
    }

}
