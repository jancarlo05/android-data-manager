package com.procuro.apimmdatamanagerlib;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by jophenmarieweeks on 10/07/2017.
 */

public class SdrGisSite extends PimmBaseObject {

    public String SiteId;
    public LatLon LatLon;
    public String AccountName;
    public String SiteName;
    public String SiteType;
    public int GeoFenceRadius;
    public String Address;
    public AddressComponents AddressComponents;
    public ArrayList<String> TrailersOnSite;
    public boolean IsDC;

    public HashMap<String,Object> dictionaryWithValuesForKeys() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();

        dictionary.put("Id", this.SiteId);
        dictionary.put("AccountName", this.AccountName);
        dictionary.put("SiteType", this.SiteType);
        dictionary.put("GeoFenceRadius", this.GeoFenceRadius);
        dictionary.put("Address", this.Address);

        dictionary.put("IsDC", (this.IsDC == false) ? false : true);
        dictionary.put("LatLon", (this.LatLon == null) ? null : this.LatLon.dictionaryWithValuesForKeys());



        return dictionary;
    }

    public static class BillToSite extends SdrGisSite{
        public String StopID;
        public HashMap<String,Object> dictionaryWithValuesForKeys() {
            HashMap<String, Object> dictionary = new HashMap<String, Object>();

            dictionary.put("StopID", this.StopID);

            return dictionary;
        }

    }

    @Override
    public void setValueForKey(Object value, String key) {

        if (key.equalsIgnoreCase("AddressComponents")) {
            if (!value.equals(null)) {
                AddressComponents addressComponents = new AddressComponents();
                JSONObject jsonObject = (JSONObject) value;
                addressComponents.readFromJSONObject(jsonObject);

                this.AddressComponents = addressComponents;
            }
        } else if (key.equalsIgnoreCase("LatLon")) {
            if (!value.equals(null)) {
                JSONObject jsonObject = (JSONObject) value;

                LatLon latLon = new LatLon();

                try {
                    latLon.Lat = jsonObject.getDouble("Lat");
                    latLon.Lon = jsonObject.getDouble("Lon");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                this.LatLon = latLon;

            } else if (key.equalsIgnoreCase("TrailersOnSite")) {
                if (this.TrailersOnSite == null) {
                    this.TrailersOnSite = new ArrayList<String>();
                }

                JSONArray trailersOnSite = (JSONArray) value;

                if (trailersOnSite != null) {
                    for (int i = 0; i < trailersOnSite.length(); i++) {
                        try {
                            this.TrailersOnSite.add(trailersOnSite.getString(i));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        } else if(key.equalsIgnoreCase("Id")) {
            this.SiteId = value.toString();
        } else
            super.setValueForKey(value, key);
    }





}
