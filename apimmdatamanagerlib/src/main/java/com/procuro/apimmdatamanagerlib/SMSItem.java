package com.procuro.apimmdatamanagerlib;

import org.json.JSONObject;

import java.util.Date;
import java.util.HashMap;

public class SMSItem extends  PimmBaseObject {

   public Date completedDateISO;
   public Date assignedDate;
   public String position;
   public boolean completed;
   public String itemName;
   public Date completedDate;
   public String task;
   public String employeeID;
   public String employee;
   public Date assignedDateISO;


    @Override
    public void setValueForKey(Object value, String key) {
        super.setValueForKey(value, key);

        if (key.equalsIgnoreCase("assignedDateISO")){
            if (value!=null){
                if (!value.toString().equalsIgnoreCase("null")){
                    this.assignedDateISO = JSONDate.convertJSONDateToNSDate(value.toString());
                }
            }

        }

        if (key.equalsIgnoreCase("employee")){
            if (value!=null){
                if (!value.toString().equalsIgnoreCase("null")){
                    this.employee = (String)value.toString();
                }
            }

        }
        if (key.equalsIgnoreCase("employeeID")){
            if (value!=null){
                if (!value.toString().equalsIgnoreCase("null")){
                    this.employeeID = (String)value.toString();
                }
            }

        }

        if (key.equalsIgnoreCase("task")){
            if (value!=null){
                if (!value.toString().equalsIgnoreCase("null")){
                    this.task = (String)value.toString();
                }
            }

        }
        if (key.equalsIgnoreCase("completedDate")){
            if (value!=null){
                if (!value.toString().equalsIgnoreCase("null")){
                    this.completedDate = JSONDate.convertJSONDateToNSDate(value.toString());
                }

            }
        }

        if (key.equalsIgnoreCase("position")){
            if (value!=null){
                if (!value.toString().equalsIgnoreCase("null")){
                    this.position = (String)value.toString();
                }
            }

        }

        if (key.equalsIgnoreCase("itemName")){
            if (value!=null){
                if (!value.toString().equalsIgnoreCase("null")){
                    this.itemName = (String)value.toString();
                }
            }

        }
        if (key.equalsIgnoreCase("completed")){
            if (value!=null){
                if (!value.toString().equalsIgnoreCase("null")){

                    if (value.toString().equalsIgnoreCase("0")){
                        this.completed = false;
                    }else if (value.toString().equalsIgnoreCase("1")){
                        this.completed = true;
                    }else {
                        this.completed = (boolean) value;
                    }
                }
            }
        }


        if (key.equalsIgnoreCase("position")){
            if (value!=null){
                if (!value.toString().equalsIgnoreCase("null")){
                    this.position = (String)value.toString();
                }
            }

        }
        if (key.equalsIgnoreCase("completedDateISO")){
            if (value!=null){
                if (!value.toString().equalsIgnoreCase("null")){
                    this.completedDateISO = JSONDate.convertJSONDateToNSDate(value.toString());
                }

            }
        }
        if (key.equalsIgnoreCase("assignedDate")){
            if (value!=null){
                if (!value.toString().equalsIgnoreCase("null")){
                    this.assignedDate = JSONDate.convertJSONDateToNSDate(value.toString());
                }

            }
        }


    }


    public HashMap<String,Object> dictionaryWithValuesForKeys() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();

        dictionary.put("completedDateISO", this.completedDateISO);
        dictionary.put("assignedDate", this.assignedDate);
        dictionary.put("position", this.position);
        dictionary.put("completed", this.completed);
        dictionary.put("itemName", this.itemName);
        dictionary.put("completedDate", this.completedDate);
        dictionary.put("task", this.task);
        dictionary.put("employeeID", this.employeeID);
        dictionary.put("employee", this.employee);
        dictionary.put("assignedDateISO", this.assignedDateISO);

        return dictionary;
    }
}
