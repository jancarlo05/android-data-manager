package com.procuro.apimmdatamanagerlib;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Date;

import static com.procuro.apimmdatamanagerlib.HOSEnums.*;
import static com.procuro.apimmdatamanagerlib.HOSEnums.DriverStatusChangeEnum.BREAK;
import static com.procuro.apimmdatamanagerlib.HOSEnums.DriverStatusChangeEnum.DELIVERY;
import static com.procuro.apimmdatamanagerlib.HOSEnums.DriverStatusChangeEnum.DRIVING;
import static com.procuro.apimmdatamanagerlib.HOSEnums.DriverStatusChangeEnum.LAYOVER;
import static com.procuro.apimmdatamanagerlib.HOSEnums.DriverStatusChangeEnum.OFF_DUTY;
import static com.procuro.apimmdatamanagerlib.HOSEnums.DriverStatusChangeEnum.ON_DUTY_NON_DRIVING;
import static com.procuro.apimmdatamanagerlib.HOSEnums.DriverStatusChangeEnum.PUNCH_IN;
import static com.procuro.apimmdatamanagerlib.HOSEnums.DriverStatusChangeEnum.PUNCH_OUT_OFFDUTY;
import static com.procuro.apimmdatamanagerlib.HOSEnums.DriverStatusChangeEnum.SLEEPER_BERTH;
import static com.procuro.apimmdatamanagerlib.HOSEnums.HOSExceptionTypeEnum.HOS_EXCEPTION_14HOUR_START;
import static com.procuro.apimmdatamanagerlib.HOSEnums.HOSExceptionTypeEnum.HOS_EXCEPTION_34HOUR_RESET;
import static com.procuro.apimmdatamanagerlib.HOSEnums.HOSExceptionTypeEnum.HOS_EXCEPTION_BADWEATHER;
import static com.procuro.apimmdatamanagerlib.HOSEnums.HOSExceptionTypeEnum.HOS_EXCEPTION_BIGDAY;

/**
 * Created by jophenmarieweeks on 06/07/2017.
 */

public class HOSSummary extends PimmBaseObject {



    public class HOSSummary_Day extends PimmBaseObject{
        public int sequence;
        public int driveTime;
        public int onDutyTime;
        public int prepTime;
        public int restTime;
        public int deliveryTime;
        public int postTripTime;
        public int sleeperTime;
//        public HOSEnums.DriverHOSStatusEnum lastDriverStatus;
        public DriverStatusChangeEnum lastDriverStatus;
        public boolean shiftCompleted;
        public boolean certified;


        public Date startTimeUTC;
        public Date endTimeUTC;
        public Date punchInUTC;
        public Date punchOutUTC;
        public Date breakTimeStartUTC;
        public Date firstOnDutyTimeUTC;
        public Date hosException_Weather;
        public Date hosException_16Hr;
        public Date hosException_34Hr;
        public Date hosException_14Hr;

        public int distance;



        @Override
        public void setValueForKey(Object value, String key) {

            if(key.equalsIgnoreCase("lastDriverStatus")) {

                if (!value.equals(null)) {
                    DriverStatusChangeEnum driverHOSStatusEnum = DriverStatusChangeEnum.setIntValue((int) value);
                    this.lastDriverStatus = driverHOSStatusEnum;

                }
            }
            else
            {
                super.setValueForKey(value, key);
            }

        }


       public int hosException() {

            if (this.hosException_Weather != null)
                return HOS_EXCEPTION_BADWEATHER.getValue();


            if (this.hosException_16Hr != null)
                return HOS_EXCEPTION_BIGDAY.getValue();


            if (this.hosException_34Hr != null)
                return HOS_EXCEPTION_34HOUR_RESET.getValue();

            if (this.hosException_14Hr != null)
                return HOS_EXCEPTION_14HOUR_START.getValue();


           return 0;

        }


    }

    public String deliveryId;
    public String userID;

    public long driveTime;
    public int onDutyTime;
    public int prepTime;
    public int restTime;
    public int deliveryTime;
    public int postTripTime;
    public int sleeperTime;
    public int distance;
    public DriverStatusChangeEnum lastDriverStatus;

    public Date startTimeUTC;
    public Date endTimeUTC;

    public double OD6;
    public double OD7;
    public double OD8;

   public  ArrayList<HOSSummary_Day> valuesByDay;

    public Date punchInTimeUTC ;
    public Date punchOutTimeUTC ;
    public int onDutyNonDrivingTime;

//   public ArrayList<HOSSummaryValues> valuesByDay;
//
   @Override
   public void setValueForKey(Object value, String key) {
      if (key.equalsIgnoreCase("valuesByDay")) {
          if (this.valuesByDay == null) {
              this.valuesByDay = new ArrayList<HOSSummary_Day>();
          }

          if (!value.equals(null)) {
              JSONArray arrValues = (JSONArray) value;

              if (arrValues != null) {
                  for (int i = 0; i < arrValues.length(); i++) {
                      try {
                          HOSSummary_Day summaryValues = new HOSSummary_Day();
                          summaryValues.readFromJSONObject(arrValues.getJSONObject(i));

                          this.valuesByDay.add(summaryValues);
                      } catch (JSONException e) {
                          e.printStackTrace();
                      }
                  }
              }
              Log.v("DM", "Value: " + this.valuesByDay);
          }
      }  else if(key.equalsIgnoreCase("hosEventTypeEnum")) {

          if (!value.equals(null)) {
              DriverStatusChangeEnum driverHOSStatusEnum = DriverStatusChangeEnum.setIntValue((int) value);
              this.lastDriverStatus = driverHOSStatusEnum;

          }
      }
      else
      {
         super.setValueForKey(value, key);
      }

   }



   public void buildHOSSummaryForUser (User driver , ArrayList<HOSEventDTO> hosEventList){
      this.userID = driver.userId;

      //Set Punch-In and Punch-Out Time
      //TBD how to handle if there is more then one hosevent for Punch-in time
      for(HOSEventDTO hosEvent : hosEventList)
      {
         if(hosEvent.eventCode == PUNCH_IN.getValue()) {


            this.punchInTimeUTC = hosEvent.eventTimeUTC;
            break;
         }
      }
       //TBD how to handle if there is more then one hosevent for Punch-out time
       for(HOSEventDTO hosEvent : hosEventList)
       {
           if(hosEvent.eventCode == PUNCH_OUT_OFFDUTY.getValue())
           {
               this.punchOutTimeUTC = hosEvent.eventTimeUTC;
               break;
           }
       }

       //Calculate total On-Duty Time ( Breaks are considered as off-duty time)
       long totalOnDutyTime = 0;
       Date onDutyStartTime = null;
       Date onDutyEndTime = null;
       boolean onDutyStartTimeSet = false;

       for(HOSEventDTO hosEvent : hosEventList) {
           if (!onDutyStartTimeSet) {
               if (hosEvent.eventCode == PUNCH_IN.getValue() || hosEvent.eventCode == ON_DUTY_NON_DRIVING.getValue()
                       || hosEvent.eventCode == DELIVERY.getValue() || hosEvent.eventCode == DRIVING.getValue()) {
                   onDutyStartTime = hosEvent.eventTimeUTC;
                   onDutyStartTimeSet = true;

               }
           } else {
               if (hosEvent.eventCode == OFF_DUTY.getValue() || hosEvent.eventCode == PUNCH_OUT_OFFDUTY.getValue()
                       || hosEvent.eventCode == BREAK.getValue() || hosEvent.eventCode == SLEEPER_BERTH.getValue()
                       || hosEvent.eventCode == LAYOVER.getValue()) {
                   onDutyEndTime = hosEvent.eventTimeUTC;
                   onDutyStartTimeSet = false;

                   // NSTimeInterval onDutyTime =[onDutyEndTime timeIntervalSinceDate:onDutyStartTime];
                   //long onDutyTime = 1.0;
//                   long onDutyTime = onDutyStartTime.getTime() - onDutyEndTime.getTime();
//                   long diffInDays = TimeUnit.MILLISECONDS.toDays(onDutyTime);
//                   long diffInHours = TimeUnit.MILLISECONDS.toHours(onDutyTime);
//                   long diffInMin = TimeUnit.MILLISECONDS.toMinutes(onDutyTime);
//                   long diffInSec = TimeUnit.MILLISECONDS.toSeconds(onDutyTime);
                   //long onDutyTime = onDutyEndTime.getTime() - onDutyStartTime.getTime();


                   long onDutyTime = onDutyEndTime.getTime() - onDutyStartTime.getTime();
                   Log.v("DM", "OnDuty Time: " + onDutyTime);
                   totalOnDutyTime = totalOnDutyTime + onDutyTime;
                   Log.v("DM", "totalOnDutyTime: " + totalOnDutyTime);


               }
           }
       }

       this.onDutyTime = Math.round(totalOnDutyTime/60);
////Calculate Drive Time
//
//       NSTimeInterval totalDriveTime = 0;
       long totalDriveTime = 0;
       Date driveStartTime = null;
       Date driveEndTime = null;
       boolean driveStartTimeSet = false;

       for(HOSEventDTO hosEvent : hosEventList)
       {
           if(!driveStartTimeSet)
           {
               if(hosEvent.eventCode == DRIVING.getValue())
               {
                   driveStartTime = hosEvent.eventTimeUTC;
                   driveStartTimeSet = true;

               }
           }
           else
           {
               if(hosEvent.eventCode != DRIVING.getValue())
               {
                   driveEndTime = hosEvent.eventTimeUTC;
                   driveStartTimeSet = false;

                   long driveTime = driveEndTime.getTime() - driveStartTime.getTime();
                   Log.v("DM", "driveTime Time: " + driveTime);
                   totalDriveTime = totalDriveTime + driveTime;
                   Log.v("DM", "totalDriveTime: " + totalDriveTime);


               }
           }
       }

       this.driveTime = Math.round(totalDriveTime/60);
       Log.v("DM", "Total Drive Time: " + convertTimeIntervalToHourMinuteSeconds(totalDriveTime));


       long totalOnDutyNonDrivingTime = totalOnDutyTime - totalDriveTime;

       this.onDutyNonDrivingTime = (int) Math.round(totalOnDutyNonDrivingTime/60);
       Log.v("DM", "Total On-Duty Non-Driving Time: " + convertTimeIntervalToHourMinuteSeconds(totalOnDutyNonDrivingTime));


       //Calculate BreakTime
       long totalBreakTime = 0;
       Date breakStartTime = null;
       Date breakEndTime = null;
       boolean breakStartTimeSet = false;

       for(HOSEventDTO hosEvent : hosEventList)
       {
           if(!breakStartTimeSet)
           {
               if(hosEvent.eventCode == BREAK.getValue())
               {
                   breakStartTime = hosEvent.eventTimeUTC;
                   breakStartTimeSet = true;

               }
           }
           else
           {
               if(hosEvent.eventCode != BREAK.getValue())
               {
                   breakEndTime = hosEvent.eventTimeUTC;
                   breakStartTimeSet = false;

                   long breakTime = breakEndTime.getTime() - breakStartTime.getTime();
                   Log.v("DM", "Break Time: " + breakTime);
                   totalBreakTime = totalBreakTime + breakTime;
                   Log.v("DM", "totalBreakTime: " + totalOnDutyTime);


               }
           }
       }

       this.restTime = Math.round(totalBreakTime/60);

       Log.v("DM","Total Rest Break Time: "+ convertTimeIntervalToHourMinuteSeconds(totalBreakTime));

       //Calculate DeliveryTime

       long totalDeliveryTime = 0;
       Date deliveryStartTime = null;
       Date deliveryEndTime = null;
       boolean deliveryStartTimeSet = false;

       for(HOSEventDTO hosEvent : hosEventList)
       {
           if(!deliveryStartTimeSet)
           {
               if(hosEvent.eventCode == DELIVERY.getValue())
               {
                   deliveryStartTime = hosEvent.eventTimeUTC;
                   deliveryStartTimeSet = true;

               }
           }
           else
           {
               if(hosEvent.eventCode != DELIVERY.getValue())
               {
                   deliveryEndTime = hosEvent.eventTimeUTC;
                   deliveryStartTimeSet = false;

                   long deliveryTime = deliveryEndTime.getTime() - deliveryStartTime.getTime();
                   Log.v("DM", "Delivery Time: " + deliveryTime);
                   totalDeliveryTime = totalDeliveryTime + deliveryTime;
                   Log.v("DM", "totalBreakTime: " + totalOnDutyTime);

               }
           }
       }

       this.deliveryTime = Math.round(totalDeliveryTime/60);

       Log.v("DM","Total Delivery Time: " + convertTimeIntervalToHourMinuteSeconds(totalDeliveryTime));

       //Calculate Sleeper Time


       long totalSleeperTime = 0;
       Date sleeperStartTime = null;
       Date sleeperEndTime = null;
       boolean sleeperStartTimeSet = false;

       for(HOSEventDTO hosEvent : hosEventList)
       {
           if(!sleeperStartTimeSet)
           {
               if(hosEvent.eventCode == SLEEPER_BERTH.getValue())
               {
                   sleeperStartTime = hosEvent.eventTimeUTC;
                   sleeperStartTimeSet = true;

               }
           }
           else
           {
               if(hosEvent.eventCode != SLEEPER_BERTH.getValue())
               {
                   sleeperEndTime = hosEvent.eventTimeUTC;
                   sleeperStartTimeSet = false;

                   long sleeperTime = sleeperEndTime.getTime() - sleeperStartTime.getTime();
                   Log.v("DM", "Sleeper Time: " + sleeperTime);
                   totalSleeperTime = totalSleeperTime + sleeperTime;
                   Log.v("DM", "totalSleeperTime: " + totalSleeperTime);

               }
           }
       }

       this.sleeperTime = Math.round(totalSleeperTime/60);

       Log.v("DM","Total Sleeper Time: " + convertTimeIntervalToHourMinuteSeconds(totalSleeperTime));

   }


public void logHOSSummary()
    {

        Log.v("DM","logHOSSummary + "+ getHOSSummaryAsString());

    }

public String getHOSSummaryAsString()
    {

        String onDutyTimeStr = convertTimeIntervalToHourMinuteSeconds(this.onDutyTime*60);
        String driveTimeStr = convertTimeIntervalToHourMinuteSeconds(this.driveTime*60);
        String deliveryTimeStr = convertTimeIntervalToHourMinuteSeconds(this.deliveryTime*60);
        String restTimeStr = convertTimeIntervalToHourMinuteSeconds(this.restTime*60);
        String sleeperTimeStr = convertTimeIntervalToHourMinuteSeconds(this.sleeperTime*60);


        String hosSummaryStr = "HOSSummary for Driver: " + this.userID + " Punch-InTime: " + this.punchInTimeUTC + " OnDutyTime: " + onDutyTimeStr + " DriveTime: " + driveTimeStr + " RestTime: " + restTimeStr + " DeliveryTime: " + deliveryTimeStr + " SleeperTime: " + sleeperTimeStr + " Punch-OutTime: " + this.punchOutTimeUTC;
//        [NSString stringWithFormat:@"HOSSummary for Driver:%@\nPunch-InTime:%@\nOnDutyTime:%@\nDriveTime:%@\nRestTime:%@\nDeliveryTime:%@\nSleeperTime:%@\nPunch-OutTime:%@",_userID,[NSDate ShortFormatStringForDateTime:_punchInTimeUTC], onDutyTimeStr, driveTimeStr, restTimeStr, deliveryTimeStr, sleeperTimeStr,[NSDate ShortFormatStringForDateTime:_punchOutTimeUTC]];

        return  hosSummaryStr;

    }

public String convertTimeIntervalToHourMinuteSeconds(long time)
    {
        long TimeInMillis = time;

        int hour = (int) ((TimeInMillis / 1000) / 3600);
        int minutes = (int) (((TimeInMillis / 1000) / 60) % 60);
        int secs = (int) ((TimeInMillis / 1000) % 60);
        // DDLogVerbose( @"Current Drive Time is:%@",[NSString stringWithFormat:@"%d:%02d:%02d", hour, minutes, secs]);
        Log.v("DM", "Current Drive Time is: " + String.format("%d:%02d:%02d",  hour, minutes, secs));
        return String.format("%d:%02d:%02d",  hour, minutes, secs);
    }

}

