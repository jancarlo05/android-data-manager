package com.procuro.apimmdatamanagerlib;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by jophenmarieweeks on 20/07/2017.
 */

public class LoadInfo2DTO extends PimmBaseObject {

    public String deliveryId;
    public ArrayList<CompartmentDTO> compartments;
    @Override
    public void setValueForKey(Object value, String key)
    {
    if(key.equalsIgnoreCase("compartments"))
        {
            if(this.compartments == null) {
                this.compartments = new ArrayList<CompartmentDTO>();
            }
            if (!value.equals(null)) {
                if (value instanceof JSONArray) {

                    JSONArray arrData = (JSONArray) value;

                    if (arrData != null) {
                        for (int i = 0; i < arrData.length(); i++) {
                            try {
                                CompartmentDTO compartmentDTO = new CompartmentDTO();
                                compartmentDTO.readFromJSONObject(arrData.getJSONObject(i));

                                this.compartments.add(compartmentDTO);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                    }

                }
            }
        }
        else
        {
            super.setValueForKey(value, key);
        }
    }

    public HashMap<String,Object> dictionaryWithValuesForKeys() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();

        dictionary.put(deliveryId, this.deliveryId);
        if (this.compartments != null) {

            ArrayList<HashMap<String, Object>> compartmentDicArray = new ArrayList<>();

            for(CompartmentDTO compartment : this.compartments) {

                HashMap<String, Object> compartmentDic = compartment.dictionaryWithValuesForKeys();
                compartmentDicArray.add(compartmentDic);

            }

            dictionary.put("compartments", compartmentDicArray);

        }else{
            dictionary.put("compartments", null);
        }

        return dictionary;
    }


}
