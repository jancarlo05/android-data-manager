package com.procuro.apimmdatamanagerlib;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.Dictionary;
import java.util.List;

/**
 * Created by jophenmarieweeks on 10/07/2017.
 */

public class Shipment extends PimmBaseObject {

  public enum ShipmentAssetType {
   ShipmentAssetType_None(0),
   ShipmentAssetType_Trailer(1),
   ShipmentAssetType_Tractor(2),
   ShipmentAssetType_StraightTruck(3),
   ShipmentAssetType_AirTag(50),
   ShipmentAssetType_LogTag(51),
   ShipmentAssetType_BlueTag(52);


   private int value;
   private ShipmentAssetType(int value) {
    this.value = value;
   }

      public int getValue() {
          return this.value;
      }

      public static ShipmentAssetType setIntValue (int i) {
          for (ShipmentAssetType type : ShipmentAssetType.values()) {
              if (type.value == i) { return type; }
          }
          return ShipmentAssetType_None;
      }

  }
  public enum ShipmentResolution {
    ShipmentResolution_Undefined(0),
    ShipmentResolution_Accepted(1),
    ShipmentResolution_Rejected(2);

      private int value;
      private ShipmentResolution(int value) {
          this.value = value;
      }

      public int getValue() {
          return this.value;
      }

      public static ShipmentResolution setIntValue (int i) {
          for (ShipmentResolution type : ShipmentResolution.values()) {
              if (type.value == i) { return type; }
          }
          return ShipmentResolution_Undefined;
      }

   }


 public enum ShipmentProductCategory {
        ShipmentProductCategory_Unspecified(0),
        ShipmentProductCategory_FreshSeafood(1),
        ShipmentProductCategory_FreshProtein(2),
        ShipmentProductCategory_R_and_F(3),
        ShipmentProductCategory_Raw(4),
        ShipmentProductCategory_ProduceChilled(5),
        ShipmentProductCategory_ProducAmbient(6);


     private int value;
     private ShipmentProductCategory(int value) {
         this.value = value;
     }

     public int getValue() {
         return this.value;
     }

     public static ShipmentProductCategory setIntValue (int i) {
         for (ShipmentProductCategory type : ShipmentProductCategory.values()) {
             if (type.value == i) { return type; }
         }
         return ShipmentProductCategory_Unspecified;
     }

 }
   public enum ShipmentGrade
    {
        ShipmentGrade_A(5),
        ShipmentGrade_B(4),
        ShipmentGrade_C(3),
        ShipmentGrade_D(2),
        ShipmentGrade_F(1),
        ShipmentGrade_Unknown(0);

        private int value;
        private ShipmentGrade(int value) {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }

        public static ShipmentGrade setIntValue (int i) {
            for (ShipmentGrade type : ShipmentGrade.values()) {
                if (type.value == i) { return type; }
            }
            return ShipmentGrade_A;
        }
    }
    public class ShipmentSiteLatLon extends  PimmBaseObject{
        public float Lat;
        public float Lon;
    }


//******* ShipmentProperties  *******///
         public class ShipmentProperties extends PimmBaseObject {

            public float Avg;
            public int DeliveryStatus;
            public int ProximityAlert;
            public int TempAlert;
            public String EngineVersion;
            public Date ExpectedArrivalTime;
            public int ExpectedArrivalTimeSource;
            public int Gen3Data;
            public String GradeDebug;
            public boolean IsTrackHybrid;
            public String LastDataTime;
            public float Max;
            public float Min;

            public String PostIgnore;
            public String PreIgnore;
            public int RunVersion;
            public int SampleCount;
            public float StdDev;
            public float StdDevM2;
            public float StdDevMean;
            public float Sum;
            public int Telematic;

            public String TrackKey;
            public String TrackRouteId;
            public String TrackTrailerName;
            public int UpperAlgorithm;
            public float VHigh;

            public String ViolationAlgorithmName;
            public String ViolationRulesetKey;
            public int ViolationUnits;
            public int direction;
            public float lat;
            public float lon;
            public Boolean permission_resolve;
            public ShipmentResolution resolution;
            //@property (nonatomic, copy) NSMutableDictionary * _Nullable undefinedProperties;

            public  ShipmentGrade Grade;
            public Date ManualArrivalTime;
            public String Avg_Text;
            public String Max_Text;
            public String Min_Text;

            public String Avg_Text(){
                return null;
            }

//            public String getStringValue(double value){
//                 double number = value;
//
//
//                return null;
//            }

        //- (public String _Nullable)Avg_Text;
        //- (public String _Nullable)Max_Text;
        //- (public String _Nullable)Min_Text;


    @Override
    public void setValueForKey(Object value, String key) {

        if (key.equalsIgnoreCase("resolution")) {
            if (!value.equals(null)) {
                ShipmentResolution shipmentResolution = ShipmentResolution.setIntValue((int) value);
                this.resolution = shipmentResolution;
            }
        }else if (key.equalsIgnoreCase("Grade")) {
            if (!value.equals(null)) {
                ShipmentGrade shipmentGrade = ShipmentGrade.setIntValue((int) value);
                this.Grade = shipmentGrade;
            }
        }else {
                super.setValueForKey(value, key);
            }
        }


    }

  ///////****Shipment******////

    public String shipmentId;
    public String version;
    public String supplierCustomerId;
    public String supplierCustomerName;
    public String receiverCustomerId;
    public String receiverCustomerName;
    public String supplierSiteId;
    public String supplierSiteName;
    public String receiverSiteId;
    public String receiverSiteName;
    public String customerId;
    public String customerName;
    public String container;
    public String carrier;
    public String compartment;
    public String compartmentLocation;
    public Date creationDate;
    public Date shipDate;
    public Date receiveDate;
    public String monitorSerial;
    public String trackingNumber;
    public Date modificationDate;
    public String modifiedFromVersion;
    public String notes;
    public String userId;
    public String userEmail;
    public Date departureDate;
    public Date arrivalDate;
    public String sealNumber;
    public int status;
    public boolean versioningEnabled;

    public ShipmentAssetType trackAssetType;
    public ShipmentProperties properties;

    public String shipmentProductId;
    public String shipmentProductType;
    public String shipmentProductName;
    public String brokerId;
    public String broker;
    public int pickupType;
    public int driverStatus;
    public String driverUserId;
    public String ShipmentDriverUserID;

    public String trackKey;
    public String trackRouteId;


    public String supplierContactName;
    public String supplierContactPhoneNumber;
    public String supplierContactEmail;

    public String receiverContactName;
    public String receiverContactPhoneNumber;
    public String receiverContactEmail;

    public String customerContactName;
    public String customerContactTitle;
    public String customerContactPhoneNumber;
    public String customerContactEmail;


    public String driverName;
    public String driverMobileNumber;
    public String codeDate;
    public Date anticipatedDeliveryTime;
    public Date deliveryStartTime;
    public Date deliveryEndTime;

    public PimmEvent pimmEvent;
    public ShipmentSiteLatLon supplierSiteLatLon;
    public ShipmentSiteLatLon receiverSiteLatLon;

    public ArrayList<ShipmentReadingDTO> shipmentReadings;

    public ShipmentProductCategory shipmentProductCategory;
    public List<RuleSet>  ruleSets; //list of RuleSet objects
    public ArrayList<PimmForm> pimmForms;

    public ArrayList<ShipmentLot> shipmentLots;

    public Date creationDateLocal;
    public Date shipDateLocal;
    public Date receiveDateLocal;
    public Date anticipatedDeliveryTimeLocal;
    public Date deliveryStartTimeLocal;
    public Date deliveryEndTimeLocal;
    public Date departureDateLocal;
    public Date arrivalDateLocal;
    //public LatLon supplierSiteLatLon;
   // public LatLon receiverSiteLatLon;

    public ShipmentGrade grade;
    public Date gradeBTime;
    public Date gradeCTime;
    public Date gradeDTime;
    public Date gradeFTime;
    public Date gradeBTimeLocal;
    public Date gradeCTimeLocal;
    public Date gradeDTimeLocal;
    public Date gradeFTimeLocal;


    @Override
    public void setValueForKey(Object value, String key) {

        if (key.equalsIgnoreCase("trackAssetType")) {
            if (!value.equals(null)) {
                ShipmentAssetType shipmentAssetType = ShipmentAssetType.setIntValue((int) value);
                this.trackAssetType = shipmentAssetType;
            }
        } else if (key.equalsIgnoreCase("properties")) {
            if (!value.equals(null)) {
                ShipmentProperties shipmentProperties = new ShipmentProperties();
                JSONObject jsonObject = (JSONObject) value;
                shipmentProperties.readFromJSONObject(jsonObject);
                this.properties = shipmentProperties;
            }
        } else if (key.equalsIgnoreCase("pimmEvent")) {
            if (!value.equals(null)) {
                PimmEvent event = new PimmEvent();
                JSONObject jsonObject = (JSONObject) value;
                event.readFromJSONObject(jsonObject);
                this.pimmEvent = event;
            }
        } else if (key.equalsIgnoreCase("supplierSiteLatLon")) {
            if (!value.equals(null)) {
                ShipmentSiteLatLon shipmentSiteLatLon = new ShipmentSiteLatLon();
                JSONObject jsonObject = (JSONObject) value;
                shipmentSiteLatLon.readFromJSONObject(jsonObject);
                this.supplierSiteLatLon = shipmentSiteLatLon;
            }
        } else if (key.equalsIgnoreCase("receiverSiteLatLon")) {
            if (!value.equals(null)) {
                ShipmentSiteLatLon shipmentSiteLatLon = new ShipmentSiteLatLon();
                JSONObject jsonObject = (JSONObject) value;
                shipmentSiteLatLon.readFromJSONObject(jsonObject);
                this.receiverSiteLatLon = shipmentSiteLatLon;
            }
        } else if (key.equalsIgnoreCase("shipmentReadings")) {

            if (this.shipmentReadings == null) {
                this.shipmentReadings = new ArrayList<ShipmentReadingDTO>();
            }

            if (!value.equals(null)) {
                JSONArray arrValues = (JSONArray) value;

                if (arrValues != null) {
                    for (int i = 0; i < arrValues.length(); i++) {
                        try {
                            ShipmentReadingDTO shipmentReadingDTO = new ShipmentReadingDTO();
                            shipmentReadingDTO.readFromJSONObject(arrValues.getJSONObject(i));

                            this.shipmentReadings.add(shipmentReadingDTO);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
         } else if (key.equalsIgnoreCase("ruleSets")) {

            if (this.ruleSets == null) {
                this.ruleSets = new ArrayList<RuleSet>();
            }

            if (!value.equals(null)) {
                JSONArray arrValues = (JSONArray) value;

                if (arrValues != null) {
                    for (int i = 0; i < arrValues.length(); i++) {
                        try {
                            RuleSet ruleSet = new RuleSet();
                            ruleSet.readFromJSONObject(arrValues.getJSONObject(i));

                            this.ruleSets.add(ruleSet);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

         } else if (key.equalsIgnoreCase("pimmForms")) {

            if (this.pimmForms == null) {
                this.pimmForms = new ArrayList<PimmForm>();
            }

            if (!value.equals(null)) {
                JSONArray arrValues = (JSONArray) value;

                if (arrValues != null) {
                    for (int i = 0; i < arrValues.length(); i++) {
                        try {
                            PimmForm pimmForm = new PimmForm();
                            pimmForm.readFromJSONObject(arrValues.getJSONObject(i));

                            this.pimmForms.add(pimmForm);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

         } else if (key.equalsIgnoreCase("shipmentLots")) {

            if (this.shipmentLots == null) {
                this.shipmentLots = new ArrayList<ShipmentLot>();
            }

            if (!value.equals(null)) {
                JSONArray arrValues = (JSONArray) value;

                if (arrValues != null) {
                    for (int i = 0; i < arrValues.length(); i++) {
                        try {
                            ShipmentLot shipmentLot = new ShipmentLot();
                            shipmentLot.readFromJSONObject(arrValues.getJSONObject(i));

                            this.shipmentLots.add(shipmentLot);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

        }else if (key.equalsIgnoreCase("grade")) {
            if (!value.equals(null)) {
                ShipmentGrade shipmentGrade = ShipmentGrade.setIntValue((int) value);
                this.grade = shipmentGrade;
            }
        }else if (key.equalsIgnoreCase("shipmentProductCategory")) {
            if (!value.equals(null)) {
                ShipmentProductCategory category = ShipmentProductCategory.setIntValue((int) value);
                this.shipmentProductCategory = category;
            }
        }else {
            super.setValueForKey(value, key);
        }
    }


}




