package com.procuro.apimmdatamanagerlib;

/**
 * Created by jophenmarieweeks on 06/07/2017.
 */

public class FacilityProductDTO extends PimmBaseObject {

    public String facilityProductID;
    public String customerID;
    public String description;
    public String notes;
    public boolean valid;

    //classes
    public ProductCompartmentEnum.ProductCompartmentsEnum compartment;
    public ProductCategoryEnum.ProductCategorysEnum category;

    @Override
    public void setValueForKey(Object value, String key) {

        if(key.equalsIgnoreCase("compartment")) {
            if (!value.equals(null)) {
                ProductCompartmentEnum.ProductCompartmentsEnum productCompartments = ProductCompartmentEnum.ProductCompartmentsEnum.setIntValue((int) value);
                this.compartment = productCompartments;
            }
        }else if(key.equalsIgnoreCase("category")) {
            if (!value.equals(null)) {
                ProductCategoryEnum.ProductCategorysEnum categorysEnum = ProductCategoryEnum.ProductCategorysEnum.setIntValue((int) value);
                this.category = categorysEnum;
            }

        } else {
            super.setValueForKey(value, key);
        }
    }
}
