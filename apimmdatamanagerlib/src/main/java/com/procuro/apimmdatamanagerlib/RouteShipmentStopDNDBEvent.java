package com.procuro.apimmdatamanagerlib;

import java.util.Date;

/**
 * Created by jophenmarieweeks on 11/07/2017.
 */

public class RouteShipmentStopDNDBEvent extends PimmBaseObject {

    public enum RouteStatusDNDBEventTypeEnum
    {
        RouteStatusDNDBEventType_Uninitialized(-1), //You should never see this, it indicates server error
        RouteStatusDNDBEventType_Plan(0), //not yet implemented, no customer gives us dndb instructions yet
        RouteStatusDNDBEventType_Cancel(1), //new feature you are working on to make it as if an arrival never happened
        RouteStatusDNDBEventType_Redeliver(2); //current usage, driver does actually arrive but is told to come back later

        private int value;
        private RouteStatusDNDBEventTypeEnum(int value)
        {
            this.value = value;
        }
        public int getValue() {
            return this.value;
        }

        public static RouteStatusDNDBEventTypeEnum setIntValue (int i) {
            for (RouteStatusDNDBEventTypeEnum type : RouteStatusDNDBEventTypeEnum.values()) {
                if (type.value == i) { return type; }
            }
            return RouteStatusDNDBEventType_Uninitialized;
        }

    }

    public String RouteStatusDNDBEventID ;            // GUID
    public String ShipmentId ;                        // deliveryID
    public String RouteId ;                           // routeID
    public String SiteId ;                            // store site
    public int DeliveryStopId;                          // key into RouteStatus table
    public Date ArrivalTime;        // start time of event, arrival at stieId
    public Date DepartureTime;      // end time of event, departure from siteId, acts as cutoff for DNDB event

    public RouteStatusDNDBEventTypeEnum eventType;

    @Override
    public void setValueForKey(Object value, String key) {

        if(key.equalsIgnoreCase("eventType")) {
            if (!value.equals(null)) {
                RouteStatusDNDBEventTypeEnum routeStatusDNDBEventTypeEnum = RouteStatusDNDBEventTypeEnum.setIntValue((int) value);
                this.eventType = routeStatusDNDBEventTypeEnum;

            }
        } else {
            super.setValueForKey(value, key);
        }
    }




}
