package com.procuro.apimmdatamanagerlib;

import org.json.JSONObject;

import java.util.Date;

/**
 * Created by jophenmarieweeks on 06/07/2017.
 */

public class DashboardEvents extends PimmBaseObject {

    public Date Timestamp;
    public Integer TimezoneId;
    public DashboardEventBuilder EventData;

    @Override
    public void setValueForKey(Object value, String key) {

        if(key.equalsIgnoreCase("EventData")) {
            DashboardEventBuilder dashboardEventBuilder = new DashboardEventBuilder();
            JSONObject jsonObject = (JSONObject) value;
            dashboardEventBuilder.readFromJSONObject(jsonObject);
            this.EventData = dashboardEventBuilder;

        } else {
            super.setValueForKey(value, key);
        }
    }
}
