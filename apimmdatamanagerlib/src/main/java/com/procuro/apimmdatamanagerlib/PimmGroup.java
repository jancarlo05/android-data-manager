package com.procuro.apimmdatamanagerlib;

/**
 * Created by jophenmarieweeks on 06/07/2017.
 */

public class PimmGroup extends PimmBaseObject {

    public String groupId;
    public String groupName;
    public String description;
    public Boolean global;
    public int state;
    public int severity;
    public int eventSeverity;
    public int devCount;
    public String groupClass;
    public String refx;

}

