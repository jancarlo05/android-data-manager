package com.procuro.apimmdatamanagerlib;

/**
 * Created by jophenmarieweeks on 06/07/2017.
 */

public class PimmRefTypeEnum extends PimmBaseObject {

    //enum PimmRefTypeEnum // duplicate class error
   public enum PimmRefTypesEnum
    {
        PimmRefType_Undefined(1),
        PimmRefType_User(0),
        PimmRefType_Shipment(1),
        PimmRefType_Device(2),
        PimmRefType_Site(3),
        PimmRefType_Customer(4),
        PimmRefType_PimmForm(5),
        PimmRefType_PimmMessage(6),
        PimmRefType_Role(7),
        PimmRefType_Object(8),
        PimmRefType_Instance(9),
        PimmRefType_InspectionSection(10),
        PimmRefType_PimmEvent(11);

        private int value;
        private PimmRefTypesEnum(int value)
        {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }

        public static PimmRefTypesEnum setIntValue (int i) {
            for (PimmRefTypesEnum type : PimmRefTypesEnum.values()) {
                if (type.value == i) { return type; }
            }
            return PimmRefType_Undefined;
        }


    }

    public PimmRefTypesEnum pimmRefTypesEnum;
    @Override
    public void setValueForKey(Object value, String key) {

        if(key.equalsIgnoreCase("pimmRefTypesEnum")) {
            if (!value.equals(null)) {
                PimmRefTypesEnum refTypesEnum = PimmRefTypesEnum.setIntValue((int) value);
                this.pimmRefTypesEnum = refTypesEnum;
            }
        } else {
            super.setValueForKey(value, key);
        }
    }
//    public static int PimmRefType_Undefined  =  1;
//    public static int PimmRefType_User                  =     0;
//    public static int PimmRefType_Shipment              =     1;
//    public static int PimmRefType_Device                =     2;
//    public static int PimmRefType_Site                  =     3;
//    public static int PimmRefType_Customer              =     4;
//    public static int PimmRefType_PimmForm              =     5;
//    public static int PimmRefType_PimmMessage           =     6;
//    public static int PimmRefType_Role                  =     7;
//    public static int PimmRefType_Object                =     8;
//    public static int PimmRefType_Instance              =     9;
//    public static int PimmRefType_InspectionSection     =     10;
//    public static int PimmRefType_PimmEvent             =     11;

}
