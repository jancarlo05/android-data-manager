package com.procuro.apimmdatamanagerlib;

/**
 * Created by jophenmarieweeks on 06/07/2017.
 */

public class DriverScores extends PimmBaseObject {


    public String DriverName;
    public String TotalPoints;
    public String TotalPointsGrade;
    public String EcoPoints;
    public String SpeedingPoints;
    public String RPMPoints;
    public String AccPoints;
    public String BrakingPoints;
    public String IdlePoints;
    public String TotalTime;
    public String EngineHours;
    public String DriveTime;

    public int NumRoutes;
    public int NumStops;
    public int NumOnTimeStops;
    public int NumDelayedStops;
    public int NumCasesDelivered;


    public double TotalPointsPercentage;
    public double EcoPointsPercentage;
    public double SpeedingPointsPercentage;
    public double RPMPointsPercentage;
    public double AccPointsPercentage;
    public double BrakingPointsPercentage;
    public double IdlePointsPercentage;
    public double TotalDistance;
    public double TotalTimeSeconds;
    public double EngineHourSeconds;
    public double DriveTimeSeconds;
    public double FuelUsed;
    public double OnTimePer;
    public double CasesPerHr;
    public double CasesPerMin;
    public double EngineHoursSeconds;


}

