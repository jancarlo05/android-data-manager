package com.procuro.apimmdatamanagerlib;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by jophenmarieweeks on 07/07/2017.
 */

public class Site extends PimmBaseObject{

    public String siteid;
    public String sitename;
    public String siteclass;
    public String address1;
    public String address2;
    public String city;
    public String state;
    public String postal;

    public int timezone;
    public int type;
    public boolean dst;
    public boolean hasGPS;
    public double lat;
    public double lon;
    public String pimmRegionCode;
    public ArrayList<PropertyValue> siteProperties;
    public int effectiveUTCOffset;

    public Date manualArrive;
    public Date manualDeliveryEnd;
    public Date manualDeliveryStart;
    public Date manualDepart;


// New Fields
    public String Id; //SiteId
    public String RemoteInstruction;
    public String CustomerId;
    public String CustomerName;
    public String SiteName;
    public AddressComponents Address;
    public Date CreationDate;
    public String SiteClass;
    public int SiteType;
    public int TimezoneId;
    public String MasterSiteId;
    public LatLon LatLon; // Class
    public ArrayList<PropertyValue> PropertiesList;
    public LocalizationSettings LocalizationSettings;
    public SiteLocale Locale;
    public int POD_Status;
    public int ProductSource;
    public Date expectedDeliveryTime;
    public boolean hasExpectedDeliveryTime;
    public SiteContact Contact;
    public ArrayList<AssetTrackingRegionDTO> assetTrackingRegions;
    public String localName;

    //properties Connected to corpStructure
    public String FMSiteID;
    public String DeviceID;
    public String ScorecardDeviceID;
    public int AlarmSeverity;
    public int EventSeverity;
    public String os;
    public boolean oos;
    public String CorpStructureID;
    public String Chain;




//-(void) print;
//
//-(NSDictionary* ) siteDictionary;


    @Override
    public void setValueForKey(Object value, String key) {
        super.setValueForKey(value, key);

        if (key.equalsIgnoreCase("siteProperties")) {
            if (this.siteProperties == null) {
                this.siteProperties = new ArrayList<PropertyValue>();
            }
            if (value != null) {
                JSONArray arrData = (JSONArray) value;

                for (int i = 0; i < arrData.length(); i++) {
                    try {
                        PropertyValue propertyValue = new PropertyValue();
                        propertyValue.readFromJSONObject(arrData.getJSONObject(i));

                        this.siteProperties.add(propertyValue);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }if (key.equalsIgnoreCase("Address")) {
            AddressComponents addressComponents = new AddressComponents();
            JSONObject jsonObject = (JSONObject) value;
            addressComponents.readFromJSONObject(jsonObject);
            this.Address = addressComponents;

        }if (key.equalsIgnoreCase("LatLon")) {

            if(value!=null){
                if (!value.toString().equalsIgnoreCase("null")){
                    try {
                        LatLon latLon = new LatLon();
                        JSONObject jsonObject = (JSONObject) value;
                        latLon.readFromJSONObject(jsonObject);
                        this.LatLon = latLon;
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }




        } if (key.equalsIgnoreCase("PropertiesList")) {
            if (this.PropertiesList == null) {
                this.PropertiesList = new ArrayList<PropertyValue>();
            }

            if (value != null) {
                JSONArray arrData = (JSONArray) value;

                for (int i = 0; i < arrData.length(); i++) {
                    try {
                        PropertyValue propertyValue = new PropertyValue();
                        propertyValue.readFromJSONObject(arrData.getJSONObject(i));

                        this.PropertiesList.add(propertyValue);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        } if (key.equalsIgnoreCase("LocalizationSettings")) {
//            LocalizationSettings localizationSettings = new LocalizationSettings();
//            JSONObject jsonObject = (JSONObject) value;
//            localizationSettings.readFromJSONObject(jsonObject);
//            this.LocalizationSettings = localizationSettings;

        } if (key.equalsIgnoreCase("Locale")) {
            SiteLocale siteLocale = new SiteLocale();
            JSONObject jsonObject = (JSONObject) value;
            siteLocale.readFromJSONObject(jsonObject);
            this.Locale = siteLocale;

        } if (key.equalsIgnoreCase("Contact")) {
            SiteContact siteContact = new SiteContact();
            JSONObject jsonObject = (JSONObject) value;
            siteContact.readFromJSONObject(jsonObject);
            this.Contact = siteContact;

        }  if (key.equalsIgnoreCase("assetTrackingRegions")) {
            if (this.assetTrackingRegions == null) {
                this.assetTrackingRegions = new ArrayList<AssetTrackingRegionDTO>();
            }

            if (value != null) {
                JSONArray arrData = (JSONArray) value;

                for (int i = 0; i < arrData.length(); i++) {
                    try {
                        AssetTrackingRegionDTO assetTrackingRegionDTO = new AssetTrackingRegionDTO();
                        assetTrackingRegionDTO.readFromJSONObject(arrData.getJSONObject(i));

                        this.assetTrackingRegions.add(assetTrackingRegionDTO);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }  if (key.equalsIgnoreCase("Id")) {
            this.siteid = value.toString();
        }
    }


    public HashMap<String,Object> dictionaryWithValuesForKeys() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();

        dictionary.put("siteid",this.siteid);
        dictionary.put("sitename",this.sitename);
        dictionary.put("siteclass",this.siteclass);
        dictionary.put("address1",this.address1);
        dictionary.put("address2",this.address2);
        dictionary.put("city",this.city);
        dictionary.put("state",this.state);
        dictionary.put("postal",this.postal);
        dictionary.put("timezone",this.timezone);
        dictionary.put("type",this.type);
        dictionary.put("lat",this.lat);
        dictionary.put("lon",this.lon);
        dictionary.put("pimmRegionCode",this.pimmRegionCode);
        dictionary.put("effectiveUTCOffset",this.effectiveUTCOffset);
        dictionary.put("SiteName",this.SiteName);
        dictionary.put("Id",this.Id);
        dictionary.put("SiteClass",this.SiteClass);
        dictionary.put("SiteType",this.SiteType);
        dictionary.put("CustomerId",this.CustomerId);
        dictionary.put("CustomerName",this.CustomerName);

        dictionary.put("FMSiteID",this.FMSiteID);
        dictionary.put("DeviceID",this.DeviceID);
        dictionary.put("ScorecardDeviceID",this.ScorecardDeviceID);
        dictionary.put("AlarmSeverity",this.AlarmSeverity);
        dictionary.put("EventSeverity",this.EventSeverity);
        dictionary.put("os",this.os);
        dictionary.put("oos",this.oos);
        dictionary.put("CorpStructureID",this.CorpStructureID);
        dictionary.put("Chain",this.Chain);


        if (this.siteProperties != null) {

            ArrayList<HashMap<String, Object>> sitePropertiesArray = new ArrayList<>();

            for(PropertyValue propertyValue : this.siteProperties) {

                HashMap<String, Object> propertyValueDict = propertyValue.dictionaryWithValuesForKeys();
                sitePropertiesArray.add(propertyValueDict);

            }

            dictionary.put("siteProperties", sitePropertiesArray);

        }else{
            dictionary.put("siteProperties", null);
        }

        if(this.Contact != null){
            dictionary.put("Contact", this.Contact.dictionaryWithValuesForKeys());
        }

        dictionary.put("manualArrive", (this.manualArrive == null) ? null : DateTimeFormat.convertToJsonDateTime(this.manualArrive));
        dictionary.put("manualDeliveryEnd", (this.manualDeliveryEnd == null) ? null : DateTimeFormat.convertToJsonDateTime(this.manualDeliveryEnd));
        dictionary.put("manualArrive", (this.manualDeliveryStart == null) ? null : DateTimeFormat.convertToJsonDateTime(this.manualDeliveryStart));
        dictionary.put("manualDepart", (this.manualDepart == null) ? null : DateTimeFormat.convertToJsonDateTime(this.manualDepart));

        dictionary.put("dst",(this.dst == true) ? "true" : "false");
        dictionary.put("hasGPS",(this.hasGPS == true) ? "true" : "false");

        dictionary.put("POD_Status", this.POD_Status);
        dictionary.put("ProductSource", this.ProductSource);

        if(this.hasExpectedDeliveryTime == true){
            dictionary.put("hasExpectedDeliveryTime", true);

            if(this.expectedDeliveryTime != null){
                dictionary.put("expectedDeliveryTime", DateTimeFormat.convertToJsonDateTime(this.expectedDeliveryTime));
            }else{
                dictionary.put("expectedDeliveryTime", null);
            }

        }else{
            dictionary.put("hasExpectedDeliveryTime", false);
        }

        if(this.assetTrackingRegions == null){
            dictionary.put("assetTrackingRegions", null);
        }else{
            ArrayList<HashMap<String, Object>> assetTrackingRegions = new ArrayList<>();

            for(AssetTrackingRegionDTO region : this.assetTrackingRegions) {

                HashMap<String, Object> regionDictionary = region.dictionaryWithValuesForKeys();
                assetTrackingRegions.add(regionDictionary);

            }

            dictionary.put("assetTrackingRegions", assetTrackingRegions);

        }


        return dictionary;
    }


    //Method to return dictionary containing key-value pairs for New Fiels in Site Object
    public HashMap<String,Object> siteDictionary() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();

        dictionary.put("siteid", this.siteid);
        dictionary.put("SiteName", this.SiteName);
        dictionary.put("CustomerId", this.CustomerId);
        dictionary.put("CustomerName", this.CustomerName);
        dictionary.put("SiteType", this.SiteType);
        dictionary.put("TimezoneId", this.TimezoneId);

        if(this.CreationDate != null){
            dictionary.put("CreationDate", DateTimeFormat.convertToJsonDateTime(this.CreationDate));
        }

        if(this.LatLon != null){
            dictionary.put("LatLon", this.LatLon.dictionaryWithValuesForKeys());
        }

        if (this.Address != null) {
            dictionary.put("Address" , this.Address.dictionaryWithValuesForKeys());
        }

        if(this.Contact != null){
            dictionary.put("Contact", this.Contact.dictionaryWithValuesForKeys());
        }


        return dictionary;
    }


    }
