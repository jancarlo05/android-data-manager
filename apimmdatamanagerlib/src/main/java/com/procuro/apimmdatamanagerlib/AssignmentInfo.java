package com.procuro.apimmdatamanagerlib;

import java.util.Date;
import java.util.HashMap;

/**
 * Created by jophenmarieweeks on 06/07/2017.
 */

public class AssignmentInfo extends PimmBaseObject {

    public String shipmentID;
    public String trailerDeviceID;
    public String trailerName;
    public String routeName;
    public String tractorName;
    public String tractorDeviceId;
    public String routeID;
    public int dow;

    public Date dispatch;


    public HashMap<String,Object> dictionaryWithValuesForKeys() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();

        dictionary.put("shipmentID", this.shipmentID);
        dictionary.put("trailerDeviceID", this.tractorDeviceId);
        dictionary.put("trailerName", this.trailerName);
        dictionary.put("routeName", this.routeName);
        dictionary.put("tractorName", this.tractorName);
        dictionary.put("tractorDeviceId", this.tractorDeviceId);
        dictionary.put("routeID", this.routeID);
        dictionary.put("dow", this.dow);

        return dictionary;
    }

}
