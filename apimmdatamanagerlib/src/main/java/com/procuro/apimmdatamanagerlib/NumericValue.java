package com.procuro.apimmdatamanagerlib;

import java.util.Date;

/**
 * Created by jophenmarieweeks on 12/07/2017.
 */

public class NumericValue extends PimmBaseObject {

    public Date Timestamp;
    public double Value ; // Server returns
}
