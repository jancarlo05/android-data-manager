package com.procuro.apimmdatamanagerlib;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by jophenmarieweeks on 10/07/2017.
 */

public class SiteSettings extends PimmBaseObject {

    public ArrayList<PropertyValue> settings;
    public ArrayList<PropertyValue>  displaySensors ;
    public String displaySensorMatch;
    public ArrayList<PropertyValue>  requiredSensors;
    public ArrayList<PropertyValue>  debitCodes;
    public ArrayList<PropertyValue>  productVerificationCodes;
    public ArrayList<PropertyValue>  otdLateCodes;
    public ArrayList<PropertyValue>  equipmentTypes;
    public ArrayList<PropertyValue>  snrCodes;
    public ArrayList<PropertyValue>  scnList;
    public ArrayList<PropertyValue>  categoryList;
    public ArrayList<PropertyValue> dpReasonCodes;
    public ArrayList<PropertyValue> bcrCodes;

//    @property (nonatomic,strong) NSArray<PropertyValue*>* dpReasonCodes;
//    @property (nonatomic,strong) NSArray<PropertyValue*>* bcrCodes;


    @Override
    public void setValueForKey(Object value, String key)
    {
        if(key.equalsIgnoreCase("settings")) {

            if(this.settings == null) {
                this.settings = new ArrayList<PropertyValue>();
            }

            JSONArray arrSettings = (JSONArray) value;
            if (arrSettings != null) {
                for (int i=0; i<arrSettings.length(); i++) {
                    try {
                        PropertyValue propertyValueSettings = new PropertyValue();
                        propertyValueSettings.readFromJSONObject(arrSettings.getJSONObject(i));

                        this.settings.add(propertyValueSettings);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        else if(key.equalsIgnoreCase("displaySensors")) {

            if(this.displaySensors == null) {
                this.displaySensors = new ArrayList<PropertyValue>();
            }

            JSONArray arrDisplaySensors = (JSONArray) value;
            if (arrDisplaySensors != null) {
                for (int i=0; i< arrDisplaySensors.length(); i++) {
                    try {
                        PropertyValue propertyValueSensors = new PropertyValue();
                        propertyValueSensors.readFromJSONObject(arrDisplaySensors.getJSONObject(i));

                        this.displaySensors.add(propertyValueSensors);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        else if(key.equalsIgnoreCase("requiredSensors")) {

            if(this.requiredSensors == null) {
                this.requiredSensors = new ArrayList<PropertyValue>();
            }

            JSONArray arrRequiredSensors = (JSONArray) value;
            if (arrRequiredSensors != null) {
                for (int i=0; i< arrRequiredSensors.length(); i++) {
                    try {
                        PropertyValue propertyValueRequiredSensors = new PropertyValue();
                        propertyValueRequiredSensors.readFromJSONObject(arrRequiredSensors.getJSONObject(i));

                        this.requiredSensors.add(propertyValueRequiredSensors);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        else if(key.equalsIgnoreCase("debitCodes")) {

            if (this.debitCodes == null) {
                this.debitCodes = new ArrayList<PropertyValue>();
            }

            JSONArray arrDebitCodes = (JSONArray) value;
            if (arrDebitCodes != null) {
                for (int i = 0; i < arrDebitCodes.length(); i++) {
                    try {
                        PropertyValue propertyValueDebit = new PropertyValue();
                        propertyValueDebit.readFromJSONObject(arrDebitCodes.getJSONObject(i));

                        this.debitCodes.add(propertyValueDebit);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            }
        }
        else if(key.equalsIgnoreCase("productVerificationCodes")) {


                if(this.productVerificationCodes == null) {
                    this.productVerificationCodes = new ArrayList<PropertyValue>();
                }

                JSONArray arrProductVerification = (JSONArray) value;
                if (arrProductVerification != null) {
                    for (int i = 0; i < arrProductVerification.length(); i++) {
                        try {
                            PropertyValue propertyVerification = new PropertyValue();
                            propertyVerification.readFromJSONObject(arrProductVerification.getJSONObject(i));

                            this.productVerificationCodes.add(propertyVerification);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
        }
        else if(key.equalsIgnoreCase("otdLateCodes")) {
//
            if(this.otdLateCodes == null) {
                this.otdLateCodes = new ArrayList<PropertyValue>();
            }

            JSONArray arrOtdLateCodes = (JSONArray) value;
            if (arrOtdLateCodes != null) {
                for (int i = 0; i < arrOtdLateCodes.length(); i++) {
                    try {
                        PropertyValue propertyValueOtd= new PropertyValue();
                        propertyValueOtd.readFromJSONObject(arrOtdLateCodes.getJSONObject(i));

                        this.otdLateCodes.add(propertyValueOtd);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        else if(key.equalsIgnoreCase("equipmentTypes")) {

            if(this.equipmentTypes == null) {
                this.equipmentTypes = new ArrayList<PropertyValue>();
            }

            JSONArray arrEquipmentTypes = (JSONArray) value;
            if (arrEquipmentTypes != null) {
                for (int i = 0; i < arrEquipmentTypes.length(); i++) {
                    try {
                        PropertyValue propertyValueEquipment = new PropertyValue();
                        propertyValueEquipment.readFromJSONObject(arrEquipmentTypes.getJSONObject(i));

                        this.equipmentTypes.add(propertyValueEquipment);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        else if(key.equalsIgnoreCase("snrCodes")) {

            if(this.snrCodes == null) {
                this.snrCodes = new ArrayList<PropertyValue>();
            }

            JSONArray arrSnrCodes = (JSONArray) value;
            if (arrSnrCodes != null) {
                for (int i = 0; i < arrSnrCodes.length(); i++) {
                    try {
                        PropertyValue propertyValuearrSnr = new PropertyValue();
                        propertyValuearrSnr.readFromJSONObject(arrSnrCodes.getJSONObject(i));

                        this.snrCodes.add(propertyValuearrSnr);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        else if(key.equalsIgnoreCase("scnList")) {

            if(this.scnList == null) {
                this.scnList = new ArrayList<PropertyValue>();
            }

            JSONArray arrscnList = (JSONArray) value;
            if (arrscnList != null) {
                for (int i = 0; i < arrscnList.length(); i++) {
                    try {
                        PropertyValue propertyValuearrSCN = new PropertyValue();
                        propertyValuearrSCN.readFromJSONObject(arrscnList.getJSONObject(i));

                        this.scnList.add(propertyValuearrSCN);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        else if(key.equalsIgnoreCase("categoryList")) {

            if(this.categoryList == null) {
            this.categoryList = new ArrayList<PropertyValue>();
            }

            JSONArray arrCategoryList = (JSONArray) value;
            if (arrCategoryList != null) {
                for (int i = 0; i < arrCategoryList.length(); i++) {
                    try {
                        PropertyValue propertyValueCategory = new PropertyValue();
                        propertyValueCategory.readFromJSONObject(arrCategoryList.getJSONObject(i));

                        this.categoryList.add(propertyValueCategory);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        else if(key.equalsIgnoreCase("dpReasonCodes")){

            if(this.dpReasonCodes == null) {
                this.dpReasonCodes = new ArrayList<PropertyValue>();
            }

            JSONArray arrdpReasonCodes = (JSONArray) value;
            if (arrdpReasonCodes != null) {
                for (int i = 0; i < arrdpReasonCodes.length(); i++) {
                    try {
                        PropertyValue propertyValuedpReason = new PropertyValue();
                        propertyValuedpReason.readFromJSONObject(arrdpReasonCodes.getJSONObject(i));

                        this.dpReasonCodes.add(propertyValuedpReason);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        else if(key.equalsIgnoreCase("bcrCodes")){

            if(this.bcrCodes == null) {
                this.dpReasonCodes = new ArrayList<PropertyValue>();
            }

            JSONArray arrdpReasonCodes = (JSONArray) value;
            if (arrdpReasonCodes != null) {
                for (int i = 0; i < arrdpReasonCodes.length(); i++) {
                    try {
                        PropertyValue propertyValuedpReason = new PropertyValue();
                        propertyValuedpReason.readFromJSONObject(arrdpReasonCodes.getJSONObject(i));

                        this.dpReasonCodes.add(propertyValuedpReason);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

        }
        else {
            super.setValueForKey(value, key);
        }
    }

    public HashMap<String,Object> dictionaryWithValuesForKeys() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();

        if (this.settings != null) {

            ArrayList<HashMap<String, Object>> settingsList = new ArrayList<>();

            for(PropertyValue propertyValue : this.settings) {

                HashMap<String, Object> propertyValueDict = propertyValue.dictionaryWithValuesForKeys();
                settingsList.add(propertyValueDict);

            }

            dictionary.put("settings", settingsList);

        }else{
            dictionary.put("settings", null);
        }

        if (this.settings != null) {

            ArrayList<HashMap<String, Object>> propertyList = new ArrayList<>();

            for(PropertyValue propertyValue : this.debitCodes) {

                HashMap<String, Object> propertyValueDict = propertyValue.dictionaryWithValuesForKeys();
                propertyList.add(propertyValueDict);

            }

            dictionary.put("debitCodes", propertyList);

        }else{
            dictionary.put("debitCodes", null);
        }

        if (this.displaySensors != null) {

            ArrayList<HashMap<String, Object>> propertyList = new ArrayList<>();

            for(PropertyValue propertyValue : this.displaySensors) {

                HashMap<String, Object> propertyValueDict = propertyValue.dictionaryWithValuesForKeys();
                propertyList.add(propertyValueDict);

            }

            dictionary.put("displaySensors", propertyList);

        }else{
            dictionary.put("displaySensors", null);
        }

        if (this.otdLateCodes != null) {

            ArrayList<HashMap<String, Object>> propertyList = new ArrayList<>();

            for(PropertyValue propertyValue : this.displaySensors) {

                HashMap<String, Object> propertyValueDict = propertyValue.dictionaryWithValuesForKeys();
                propertyList.add(propertyValueDict);

            }

            dictionary.put("otdLateCodes", propertyList);

        }else{
            dictionary.put("otdLateCodes", null);
        }

        if (this.equipmentTypes != null) {

            ArrayList<HashMap<String, Object>> propertyList = new ArrayList<>();

            for(PropertyValue propertyValue : this.displaySensors) {

                HashMap<String, Object> propertyValueDict = propertyValue.dictionaryWithValuesForKeys();
                propertyList.add(propertyValueDict);

            }

            dictionary.put("equipmentTypes", propertyList);

        }else{
            dictionary.put("equipmentTypes", null);
        }

        if (this.snrCodes != null) {

            ArrayList<HashMap<String, Object>> propertyList = new ArrayList<>();

            for(PropertyValue propertyValue : this.displaySensors) {

                HashMap<String, Object> propertyValueDict = propertyValue.dictionaryWithValuesForKeys();
                propertyList.add(propertyValueDict);

            }

            dictionary.put("snrCodes", propertyList);

        }else{
            dictionary.put("snrCodes", null);
        }

        if (this.scnList != null) {

            ArrayList<HashMap<String, Object>> propertyList = new ArrayList<>();

            for(PropertyValue propertyValue : this.displaySensors) {

                HashMap<String, Object> propertyValueDict = propertyValue.dictionaryWithValuesForKeys();
                propertyList.add(propertyValueDict);

            }

            dictionary.put("scnList", propertyList);

        }else{
            dictionary.put("scnList", null);
        }

        if (this.categoryList != null) {

            ArrayList<HashMap<String, Object>> propertyList = new ArrayList<>();

            for(PropertyValue propertyValue : this.displaySensors) {

                HashMap<String, Object> propertyValueDict = propertyValue.dictionaryWithValuesForKeys();
                propertyList.add(propertyValueDict);

            }

            dictionary.put("categoryList", propertyList);

        }else{
            dictionary.put("categoryList", null);
        }

        if (this.dpReasonCodes != null) {

            ArrayList<HashMap<String, Object>> propertyList = new ArrayList<>();

            for(PropertyValue propertyValue : this.displaySensors) {

                HashMap<String, Object> propertyValueDict = propertyValue.dictionaryWithValuesForKeys();
                propertyList.add(propertyValueDict);

            }

            dictionary.put("dpReasonCodes", propertyList);

        }else{
            dictionary.put("dpReasonCodes", null);
        }

        if (this.bcrCodes != null) {

            ArrayList<HashMap<String, Object>> propertyList = new ArrayList<>();

            for(PropertyValue propertyValue : this.displaySensors) {

                HashMap<String, Object> propertyValueDict = propertyValue.dictionaryWithValuesForKeys();
                propertyList.add(propertyValueDict);

            }

            dictionary.put("bcrCodes", propertyList);

        }else{
            dictionary.put("bcrCodes", null);
        }

        dictionary.put("displaySensorMatch", (this.displaySensorMatch == null) ? null : this.displaySensorMatch);



        return dictionary;
    }

}
