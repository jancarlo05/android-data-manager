package com.procuro.apimmdatamanagerlib;

import java.util.Date;

/**
 * Created by jophenmarieweeks on 06/07/2017.
 */

public class GPSValue extends PimmBaseObject {

    public Date Timestamp;

    public double Speed;
    public double Direction;
    public double Lat;
    public double Lon;
    public boolean Empty;
}
