package com.procuro.apimmdatamanagerlib;

import java.util.HashMap;

public class SMSDashboard extends PimmBaseObject{

    public String siteID;
    public int totalCount;
    public double osat;
    public double zod;
    public double tasteScore;
    public double friendlinessScore;
    public double speedScore;
    public double accuracyScore;
    public double cleanlinessScore;
    public double likelyReturn;
    public double likelyRecommend;
    public double problemScore;
    public double resolutionScore;

    @Override
    public void setValueForKey(Object value, String key) {
        super.setValueForKey(value, key);

        if (key.equalsIgnoreCase("siteID")){
            if (!value.toString().equalsIgnoreCase("null")){
                this.siteID = (String) value;
            }

        }
        if (key.equalsIgnoreCase("totalCount")){
            if (!value.toString().equalsIgnoreCase("null")){
                this.totalCount = (int) value;
            }

        }
        if (key.equalsIgnoreCase("osat")){
            if (!value.toString().equalsIgnoreCase("null")){
                this.osat = Double.valueOf(String.valueOf(value));
            }


        }
        if (key.equalsIgnoreCase("zod")){
            if (!value.toString().equalsIgnoreCase("null")){
                this.zod = Double.valueOf(String.valueOf(value));            }


        }
        if (key.equalsIgnoreCase("tasteScore")){
            if (!value.toString().equalsIgnoreCase("null")){
                this.tasteScore = Double.valueOf(String.valueOf(value));
            }

        }
        if (key.equalsIgnoreCase("friendlinessScore")){
            if (!value.toString().equalsIgnoreCase("null")){
                this.friendlinessScore = Double.valueOf(String.valueOf(value));
            }

        }
        if (key.equalsIgnoreCase("speedScore")){
            if (!value.toString().equalsIgnoreCase("null")){
                this.speedScore = Double.valueOf(String.valueOf(value));
            }

        }
        if (key.equalsIgnoreCase("accuracyScore")){
            if (!value.toString().equalsIgnoreCase("null")){
                this.accuracyScore = Double.valueOf(String.valueOf(value));
            }

        }
        if (key.equalsIgnoreCase("cleanlinessScore")){
            if (!value.toString().equalsIgnoreCase("null")){
                this.cleanlinessScore = Double.valueOf(String.valueOf(value));
            }

        }
        if (key.equalsIgnoreCase("likelyReturn")){
            if (!value.toString().equalsIgnoreCase("null")){
                this.likelyReturn = Double.valueOf(String.valueOf(value));
            }

        }
        if (key.equalsIgnoreCase("likelyRecommend")){
            if (!value.toString().equalsIgnoreCase("null")){
                this.likelyRecommend = Double.valueOf(String.valueOf(value));
            }

        }
        if (key.equalsIgnoreCase("problemScore")){
            if (!value.toString().equalsIgnoreCase("null")){
                this.problemScore = Double.valueOf(String.valueOf(value));
            }

        }
        if (key.equalsIgnoreCase("resolutionScore")){
            if (!value.toString().equalsIgnoreCase("null")){
                this.resolutionScore = Double.valueOf(String.valueOf(value));
            }

        }
    }

    public HashMap<String,Object> dictionaryWithValuesForKeys() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();
        dictionary.put("siteID",this.siteID);
        dictionary.put("totalCount",this.totalCount);
        dictionary.put("osat",this.osat);
        dictionary.put("zod",this.zod);
        dictionary.put("tasteScore",this.tasteScore);
        dictionary.put("friendlinessScore",this.friendlinessScore);
        dictionary.put("speedScore",this.speedScore);
        dictionary.put("accuracyScore",this.accuracyScore);
        dictionary.put("likelyReturn",this.likelyReturn);
        dictionary.put("likelyRecommend",this.likelyRecommend);
        dictionary.put("problemScore",this.problemScore);
        dictionary.put("resolutionScore",this.resolutionScore);
        return dictionary;
    }
}
