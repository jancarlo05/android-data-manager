package com.procuro.apimmdatamanagerlib;

import android.util.Base64;

import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.HashMap;


public class PimmForm extends PimmBaseObject {

    //list property
    public String CustomerID;
    public String name;

    //actual Pimmform
    public String formDefinitionID;
    public String formID;
    public String Refx;
    public byte[] bodyData;
    public String StringBody;

    public int RefType;
    public Date StartTime;
    public Date EndTime;

    @Override
    public void setValueForKey(Object value, String key) {
        super.setValueForKey(value, key);

        if (key.equalsIgnoreCase("formDefinitionID")){
            if (value!=null){
                this.formDefinitionID = (String) value;
            }
        }

        if (key.equalsIgnoreCase("formID")){
            if (value!=null){
                this.formID = (String) value;
            }
        }

        if (key.equalsIgnoreCase("RefType")){
            if (value!=null){
                this.RefType = (int) value;
            }
        }

        if (key.equalsIgnoreCase("CustomerID")){
            if (value!=null){
                this.CustomerID = (String) value;
            }
        }

        if (key.equalsIgnoreCase("Name")){
            if (value!=null){
                this.name = (String) value.toString();
            }
        }

        if (key.equalsIgnoreCase("Body")){
            if (value!=null){
                try {
                    String finaldecode = value.toString().replace("\\r","").replace("\\n","").replace("\\","");
                    byte[] bt = Base64.decode(finaldecode, Base64.DEFAULT);
                    String strNewEncode = new String(bt, StandardCharsets.UTF_8);
                    String s = strNewEncode.replaceAll("[^\\x20-\\x7E]", "");
                    this.StringBody = s;
                    this.bodyData = s.getBytes();
                }catch (Exception e){
                    System.out.println(e.getMessage());
                }

            }
        }
        if (key.equalsIgnoreCase("StartTime")) {
            if (value != null) {
                this.StartTime = JSONDate.convertJSONDateToNSDate(String.valueOf(value));
            }
        }
        if (key.equalsIgnoreCase("EndTime")){
            if (value != null) {
                this.EndTime = JSONDate.convertJSONDateToNSDate(String.valueOf(value));
            }
        }

    }

    public HashMap<String,Object> dictionaryWithValuesForKeys() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();

        dictionary.put("formDefinitionID", this.formDefinitionID);
        dictionary.put("formID", this.formID);
        dictionary.put("Refx", this.Refx);
        dictionary.put("bodyData", this.bodyData);
        dictionary.put("CustomerID", this.CustomerID);
        dictionary.put("Name", this.name);
        dictionary.put("RefType", this.RefType);
        dictionary.put("StartTime", JSONDate.convertJSONDateToNSDate(String.valueOf(this.StartTime)));
        dictionary.put("EndTime", JSONDate.convertJSONDateToNSDate(String.valueOf(this.EndTime)));

        return dictionary;
    }
}
