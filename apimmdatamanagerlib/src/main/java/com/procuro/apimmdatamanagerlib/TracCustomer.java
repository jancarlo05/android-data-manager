package com.procuro.apimmdatamanagerlib;

/**
 * Created by jophenmarieweeks on 01/08/2017.
 */

public class TracCustomer extends PimmBaseObject {

    public String tracCustomerId;
    public String name;
    public TracContact contact;
}
