package com.procuro.apimmdatamanagerlib;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by jophenmarieweeks on 10/07/2017.
 */

public class SdrReport extends PimmBaseObject {

    public Date Timestamp;
    public String TimestampText;
    public String Title;
    public int TimezoneId;
    public SdrDelivery Hierarchy;
    public SdrDeliveryTrailer Trailer;
    public List<ValueSeries> Data; //Array of ValueSeries
    public List<PimmForm> Forms;
    public String Tractor;

    public String IconSet;
    public String Annotations;
    public boolean SidebarStartExpanded;

    @Override
    public void setValueForKey(Object value, String key) {
        if (key.equalsIgnoreCase("Hierarchy")) {
            SdrDelivery sdrDelivery = new SdrDelivery();
            JSONObject jsonObject = (JSONObject) value;
            sdrDelivery.readFromJSONObject(jsonObject);
            this.Hierarchy = sdrDelivery;

        } else if (key.equalsIgnoreCase("Trailer")) {

            if (!value.equals(null)) {
                SdrDeliveryTrailer sdrDeliveryTrailer = new SdrDeliveryTrailer();
                JSONObject jsonObject = (JSONObject) value;
                sdrDeliveryTrailer.readFromJSONObject(jsonObject);
                this.Trailer = sdrDeliveryTrailer;
            }

        } else if (key.equalsIgnoreCase("Data")) {
            if (this.Data == null) {
                this.Data = new ArrayList<ValueSeries>();
            }

            if (!value.equals(null)) {
                if (value instanceof JSONArray) {

                    JSONArray arrData = (JSONArray) value;

                    if (arrData != null) {
                        for (int i = 0; i < arrData.length(); i++) {
                            try {
                                ValueSeries valueSeries = new ValueSeries();
                                valueSeries.readFromJSONObject(arrData.getJSONObject(i));

                                this.Data.add(valueSeries);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    Log.v("Data", "Value: " + this.Data);
                }
            }
        }
        else if (key.equalsIgnoreCase("Timestamp"))
        {
            this.Timestamp = JSONDate.convertJSONDateToNSDate(String.valueOf(value));
            Log.v("Timestamp", "Value: "+this.Timestamp);
        }
        else if (key.equalsIgnoreCase("Forms"))
        {
            if(this.Forms == null) {
                this.Forms = new ArrayList<PimmForm>();
            }

            if (!value.equals(null)) {
                if (value instanceof JSONArray) {

                    JSONArray arrForms = (JSONArray) value;

                    if (arrForms != null) {
                        for (int i = 0; i < arrForms.length(); i++) {
                            try {
                                PimmForm pimmForm = new PimmForm();
                                pimmForm.readFromJSONObject(arrForms.getJSONObject(i));

                                this.Forms.add(pimmForm);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    Log.v("Forms", "Value: " + this.Forms);
                }
            }
        }
        else
        {
            super.setValueForKey(value, key);
        }
    }
}
