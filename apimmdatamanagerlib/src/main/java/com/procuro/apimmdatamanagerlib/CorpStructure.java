package com.procuro.apimmdatamanagerlib;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class CorpStructure extends PimmBaseObject {

    public String corpStructureID;
    public String corpStructureLayerID;
    public String name;
    public String parentID;
    public int layer;
    public String layerName;
    public int allowSite;
    public CorpUser corpUser;

    @Override
    public void setValueForKey(Object value, String key) {
        super.setValueForKey(value, key);

        if (key.equalsIgnoreCase("corpUser")){
            if (value!=null){
                if (!value.toString().equalsIgnoreCase("null")){
                    CorpUser corpUser = new CorpUser();
                    JSONObject jsonObject = (JSONObject) value;
                    corpUser.readFromJSONObject(jsonObject);
                    this.corpUser = corpUser;
                }
            }
        }
    }
    public HashMap<String,Object> dictionaryWithValuesForKeys() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();
        dictionary.put("corpStructureID",this.corpStructureID);
        dictionary.put("corpStructureLayerID",this.corpStructureLayerID);
        dictionary.put("name",this.name);
        dictionary.put("parentID",this.parentID);
        dictionary.put("layer",this.layer);
        dictionary.put("layerName",this.layerName);
        dictionary.put("allowSite",this.allowSite);
        dictionary.put("corpUser",this.corpUser);
        return dictionary;
    }



}
