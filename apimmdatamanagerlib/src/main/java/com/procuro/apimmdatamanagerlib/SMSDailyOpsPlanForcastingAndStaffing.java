package com.procuro.apimmdatamanagerlib;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

public class SMSDailyOpsPlanForcastingAndStaffing extends PimmBaseObject {

    public String verifiedBy;
    public Date verifiedDate;
    public ArrayList<ForcastingItem>forecastItems;
    public ArrayList<HuddleTeam>huddleTeams;
    public ArrayList<String>staffingItems;



    @Override
    public void setValueForKey(Object value, String key) {
        super.setValueForKey(value, key);

        if (key.equalsIgnoreCase("verifiedDate")){
         if (value!=null){
             this.verifiedDate = JSONDate.convertJSONDateToNSDate(value.toString());
         }
        }

        if (key.equalsIgnoreCase("forecastItems")){
           ArrayList<ForcastingItem>items = new ArrayList<>();
           if (value!=null){
               JSONArray jsonArray = (JSONArray) value;

               for (int i = 0; i <jsonArray.length() ; i++) {
                   ForcastingItem forcastingItem = new ForcastingItem();
                   try {
                       forcastingItem.readFromJSONObject(jsonArray.getJSONObject(i));
                   } catch (JSONException e) {
                       e.printStackTrace();
                   }
                   items.add(forcastingItem);
               }
               this.forecastItems = items;
           }
        }

        if (key.equalsIgnoreCase("huddleTeams")){
            ArrayList<HuddleTeam>items = new ArrayList<>();
            if (value!=null){
                JSONArray jsonArray = (JSONArray) value;

                for (int i = 0; i <jsonArray.length() ; i++) {
                    HuddleTeam item = new HuddleTeam();
                    try {
                        item.readFromJSONObject(jsonArray.getJSONObject(i));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    items.add(item);
                }
                this.huddleTeams = items;
            }
        }

        if (key.equalsIgnoreCase("verifiedBy")){
            if (value!=null){
                this.verifiedBy = (String) value.toString();
            }
        }

        if (key.equalsIgnoreCase("staffingItems")){
            ArrayList<String>items = new ArrayList<>();
            if (value!=null){
                JSONArray jsonArray = (JSONArray) value;
                for (int i = 0; i <jsonArray.length() ; i++) {
                    try {
                        items.add(jsonArray.getString(i));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                this.staffingItems = items;
            }
        }


    }


    public HashMap<String,Object> dictionaryWithValuesForKeys() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();

        dictionary.put("verifiedDate", this.verifiedDate);
        dictionary.put("forecastItems", this.forecastItems);
        dictionary.put("huddleTeams", this.huddleTeams);
        dictionary.put("verifiedBy", this.verifiedBy);
        dictionary.put("staffingItems", this.staffingItems);



        return dictionary;
    }


}
