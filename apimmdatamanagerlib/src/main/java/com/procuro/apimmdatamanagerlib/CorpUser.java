package com.procuro.apimmdatamanagerlib;

import java.util.HashMap;

public class CorpUser extends PimmBaseObject{

    public String firstName;
    public String lastName;
    public String userName;
    public String userID;
    public String jobTitle;
    public String phoneNumber;

    @Override
    public void setValueForKey(Object value, String key) {
        super.setValueForKey(value, key);
    }

    public HashMap<String,Object> dictionaryWithValuesForKeys() {

        HashMap<String, Object> dictionary = new HashMap<String, Object>();
        dictionary.put("firstName",this.firstName);
        dictionary.put("lastName",this.lastName);
        dictionary.put("userName",this.userName);
        dictionary.put("userID",this.userID);
        dictionary.put("jobTitle",this.jobTitle);
        dictionary.put("phoneNumber",this.phoneNumber);

        return dictionary;
    }
}
