package com.procuro.apimmdatamanagerlib;

import java.text.DateFormat;
import java.util.Date;

public class SiteSalesActual extends  PimmBaseObject {

    public double amount;
    public Date startTime;
    public Date endTime;

    @Override
    public void setValueForKey(Object value, String key) {
        super.setValueForKey(value, key);

        if (key.equalsIgnoreCase("amount")){
            if (value!=null){
                try {
                    amount = Double.parseDouble(value.toString());
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }

        if (key.equalsIgnoreCase("startTime")){
            if (value!=null){
                try {
                    startTime = DateTimeFormat.ConvertISOToDate(value.toString());
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }

        if (key.equalsIgnoreCase("endTime")){
            if (value!=null){
                try {
                    endTime = DateTimeFormat.ConvertISOToDate(value.toString());
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
    }
}


