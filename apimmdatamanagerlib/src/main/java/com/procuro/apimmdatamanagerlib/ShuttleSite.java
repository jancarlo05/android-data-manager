package com.procuro.apimmdatamanagerlib;

/**
 * Created by jophenmarieweeks on 30/08/2017.
 */

public class ShuttleSite extends Site {

    public String deliveryInstructions;
    public String gln;
    public String armCode;
    public String disarmCode;
    public String keyNumber;
    public double paymentTerms;
    public double paymentTermsDays;
    public String localName;
    public String customerid;


}
