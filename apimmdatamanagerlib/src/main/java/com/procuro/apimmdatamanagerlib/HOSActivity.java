package com.procuro.apimmdatamanagerlib;

import java.util.Date;

import static com.procuro.apimmdatamanagerlib.HOSActivity.HOSActivityTypeEnum.HOSActivityType_34HOURRESTART;
import static com.procuro.apimmdatamanagerlib.HOSActivity.HOSActivityTypeEnum.HOSActivityType_ALL;

/**
 * Created by jophenmarieweeks on 06/07/2017.
 */

public class HOSActivity extends PimmBaseObject {


    public enum HOSActivityTypeEnum
    {
        HOSActivityType_ALL(-1),
        HOSActivityType_UNDEFINED(0),
        HOSActivityType_34HOURRESTART(1),
        HOSActivityType_PUNCHOUT(2),
        HOSActivityType_BADWEATHER(3),
        HOSActivityType_PUNCHIN(4),
        HOSActivityType_BIGDAY(5),
        HOSActivityType_SLEEPER(6),
        HOSActivityType_LAYOVER(7);

        private int value;
        private HOSActivityTypeEnum(int value)
        {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }

        public static HOSActivityTypeEnum setIntValue (int i) {
            for (HOSActivityTypeEnum type : HOSActivityTypeEnum.values()) {
                if (type.value == i) { return type; }
            }
            return HOSActivityType_UNDEFINED;
        }
    }

    public enum HOSActivityCancelledReasonEnum
    {
        HOSActivityCancelledReason_NotCancelled(0),
        HOSActivityCancelledReason_NotAccepted(1),
        HOSActivityCancelledReason_NotEligible(2),
        HOSActivityCancelledReason_Manual(3);

        private int value;
        private HOSActivityCancelledReasonEnum(int value)
        {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }

        public static HOSActivityCancelledReasonEnum setIntValue (int i) {
            for (HOSActivityCancelledReasonEnum type : HOSActivityCancelledReasonEnum.values()) {
                if (type.value == i) { return type; }
            }
            return HOSActivityCancelledReason_NotCancelled;
        }

    }

    public String hosActivityID;
    public String userID;
    public Date timestamp;

    public String authorizedBy;
    public Date authorizedDate;
    public Date ackDate;
    public String cancelledBy;
    public Date cancelledDate;

    public String ackBy;
    public String cancelledAckBy;
    public Date cancelledAckDate;

    public HOSActivityTypeEnum activityType;
    public HOSActivityCancelledReasonEnum cancelledReason;

//    -(NSString*) getHOSActivityTypeAsString;

    @Override
    public void setValueForKey(Object value, String key) {

        if (key.equalsIgnoreCase("activityType")) {
            if (!value.equals(null)) {
                HOSActivityTypeEnum hosActivityTypeEnum = HOSActivityTypeEnum.setIntValue((int) value);
                this.activityType = hosActivityTypeEnum;
            }

        } else if (key.equalsIgnoreCase("cancelledReason")) {
            if (!value.equals(null)) {
                HOSActivityCancelledReasonEnum hosActivityCancelledReasonEnum = HOSActivityCancelledReasonEnum.setIntValue((int) value);
                this.cancelledReason = hosActivityCancelledReasonEnum;
            }
        } else {
            super.setValueForKey(value, key);
        }
    }


        public String getHOSActivityTypeAsString() {
                String result = null;
            switch (activityType) {
                    case HOSActivityType_ALL:
                        result = "ALL";
                    break;
                    case HOSActivityType_UNDEFINED:
                    break;
                    case HOSActivityType_34HOURRESTART:
                        result= "34HourRestart";
                    break;
                    case HOSActivityType_PUNCHOUT:
                        result = "PunchOut";
                    break;
                    case HOSActivityType_PUNCHIN:
                        result= "PunchIn";
                    break;
                    case HOSActivityType_BADWEATHER:
                        result= "BadWeather";
                    break;
                    case HOSActivityType_BIGDAY:
                        result= "BigDay";
                    break;
                    case HOSActivityType_SLEEPER:
                        result = "Sleeper";
                    break;
                    case HOSActivityType_LAYOVER:
                        result = "Layover";
                    break;
                    default:
                        result = "";
                    break;

            }
            return result;
        }

}
