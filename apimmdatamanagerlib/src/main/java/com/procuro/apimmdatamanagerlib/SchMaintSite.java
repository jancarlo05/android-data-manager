package com.procuro.apimmdatamanagerlib;

import java.util.Date;

/**
 * Created by jophenmarieweeks on 07/07/2017.
 */

public class SchMaintSite extends PimmBaseObject {
    public String schMaintSiteID ; // PIMM GUID
    public String userID ;         // submitter, PIMM User
    public String siteID ;         // PIMM Site for this record
    public Date creationDate;        // timestamp for when this WO was created

    public String deviceType ;     // optional, what type of device
    //     Expected to be Device.OS
    public String deviceClass ;    // optional, what class of device
    //    Device.DeviceClass
    public int repeatType;               // the trigger for creating WorkOrders
    public int repteatParam;             // parameter to be used when RepeatType is applied
    public int service;                  // customer-specific id for the work performed
    public String instructions ;   // free text instructions from submitter
    public int enabled;              // 0 = disabled, 1 = enabled
}
