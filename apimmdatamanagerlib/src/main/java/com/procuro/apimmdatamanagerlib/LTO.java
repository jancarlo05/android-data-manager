package com.procuro.apimmdatamanagerlib;

import com.procuro.apimmdatamanagerlib.PODData.BarcodeLogEntry;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by jophenmarieweeks on 11/10/2017.
 */

public class LTO extends PimmBaseObject {
    public String    ltoId;
    public String    name;
    public String    userId;
    public String    userName;
    public Date creationDate;
    public Date    startDate;
    public Date    endDate;
    public boolean    shift0;
    public boolean    shift1;
    public boolean    shift2;
    public boolean    shift3;
    public boolean    shift4;
    public boolean    shift5;
    public boolean    shift6;
    public boolean    shift7;

    public boolean    dow0;
    public boolean    dow1;
    public boolean    dow2;
    public boolean    dow3;
    public boolean    dow4;
    public boolean    dow5;
    public boolean    dow6;

    public double lowerThreshold;
    public double upperThreshold;

    public String  category;

    public List<SiteInfo> siteInfoList; //List of SiteInfo objects


    @Override
    public void setValueForKey(Object value, String key) {

        if (key.equalsIgnoreCase("siteInfoList")) {
            if (this.siteInfoList == null) {
                this.siteInfoList = new ArrayList<SiteInfo>();
            }

            if (!value.equals(null)) {
                JSONArray arrStops = (JSONArray) value;

                if (arrStops != null) {
                    for (int i = 0; i < arrStops.length(); i++) {
                        try {
                            SiteInfo siteInfo = new SiteInfo();
                            siteInfo.readFromJSONObject(arrStops.getJSONObject(i));

                            this.siteInfoList.add(siteInfo);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } else {
                super.setValueForKey(value, key);
            }
        }
    }


    public HashMap<String, Object> getPrintString() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();

        dictionary.put("ltoId", this.ltoId);
        dictionary.put("name", this.name);
        dictionary.put("userId", this.userId);
        dictionary.put("userName", this.userName);
        dictionary.put("lowerThreshold", this.lowerThreshold);
        dictionary.put("upperThreshold", this.upperThreshold);
        dictionary.put("category", this.category);

        dictionary.put("creationDate", (this.creationDate == null ) ? null : aPimmDataManager.getUTCFormatDate(this.creationDate));
        dictionary.put("startDate", (this.startDate == null ) ? null : aPimmDataManager.getUTCFormatDate(this.startDate));
        dictionary.put("endDate", (this.endDate == null ) ? null : aPimmDataManager.getUTCFormatDate(this.endDate));

        dictionary.put("shift0", (this.shift0));
        dictionary.put("shift1", (this.shift1));
        dictionary.put("shift2", (this.shift2));
        dictionary.put("shift3", (this.shift3));
        dictionary.put("shift4", (this.shift4));
        dictionary.put("shift5", (this.shift5));
        dictionary.put("shift6", (this.shift6));
        dictionary.put("shift7", (this.shift7));

        dictionary.put("shift0", (this.dow0));
        dictionary.put("shift1", (this.dow1));
        dictionary.put("shift2", (this.dow2));
        dictionary.put("shift3", (this.dow3));
        dictionary.put("shift4", (this.dow4));
        dictionary.put("shift5", (this.dow5));
        dictionary.put("shift6", (this.dow6));




        return  dictionary;
    }


}
