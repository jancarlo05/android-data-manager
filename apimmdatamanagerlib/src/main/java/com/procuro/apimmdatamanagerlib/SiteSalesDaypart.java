package com.procuro.apimmdatamanagerlib;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;

public class SiteSalesDaypart extends  PimmBaseObject {

    public String name;
    public Date startTime;
    public Date endTime;

    public ArrayList<SiteSalesForecast> forecast;

    @Override
    public void setValueForKey(Object value, String key) {
        super.setValueForKey(value, key);

        if (key.equalsIgnoreCase("startTime")){
            if (value!=null){
                try {
                    startTime = DateTimeFormat.ConvertISOToDate(value.toString());
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }

        if (key.equalsIgnoreCase("endTime")){
            if (value!=null){
                try {
                    endTime = DateTimeFormat.ConvertISOToDate(value.toString());
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }

        if (key.equalsIgnoreCase("forecast")){
            ArrayList<SiteSalesForecast>list = new ArrayList<>();
            if (value!=null){
                JSONArray jsonArray = (JSONArray) value;
                for (int i = 0; i <jsonArray.length() ; i++) {
                    try {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        SiteSalesForecast item = new SiteSalesForecast();
                        item.readFromJSONObject(jsonObject);
                        list.add(item);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }
            this.forecast = list;
        }

    }
}


