package com.procuro.apimmdatamanagerlib;

/**
 * Created by jophenmarieweeks on 06/07/2017.
 */

public class HOSViolationTypeEnum extends PimmBaseObject {

//   public enum HOSViolationTypeEnum  // duplicate class error
    public enum HOSViolationTypesEnum {

//        HOSViolationTypeEnum_Undefined(-1),
//        HOSViolationTypeEnum_DrivingWindowLimitViolation(1),
//        HOSViolationTypeEnum_DrivingLimitViolation(2),
//        HOSViolationTypeEnum_SevenDayDutyLimitViolation(3),
//        HOSViolationTypeEnum_EightDayDutyLimitViolation(4),
////        HOSViolationTypeEnum_BreakRequirementViolation(5),
//        HOSViolationTypeEnum_NoBreakViolation(5),
//        HOSViolationTypeEnum_ShortBreakViolation(6);

        HOSViolationTypeEnum_Undefined(-1),
        HOSViolationTypeEnum_Driving_Window(1),
        HOSViolationTypeEnum_Driving_Limit(2),
        HOSViolationTypeEnum_OD_7(3),
        HOSViolationTypeEnum_OD_8(4),
        HOSViolationTypeEnum_Break_None(5),
        HOSViolationTypeEnum_Break_Short(6);

        private int value;
        private HOSViolationTypesEnum(int value)
        {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }

        public static HOSViolationTypesEnum setIntValue (int i) {
            for (HOSViolationTypesEnum type : HOSViolationTypesEnum.values()) {
                if (type.value == i) { return type; }
            }
            return HOSViolationTypeEnum_Undefined;
        }

    }

    public enum HOSViolationStatus
    {
        HOSViolationStatus_Corrected (0),
        HOSViolationStatus_Violation(1);

        private int value;
        private HOSViolationStatus(int value)
        {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }

        public static HOSViolationStatus setIntValue (int i) {
            for (HOSViolationStatus type : HOSViolationStatus.values()) {
                if (type.value == i) { return type; }
            }
            return HOSViolationStatus_Corrected;
        }
    }

    public HOSViolationStatus hosViolationStatus;


    public HOSViolationTypesEnum violationTypesEnum;

    @Override
    public void setValueForKey(Object value, String key) {

        if(key.equalsIgnoreCase("violationTypesEnum")) {
            if (!value.equals(null)) {
                HOSViolationTypesEnum hosViolationTypesEnum = HOSViolationTypesEnum.setIntValue((int) value);
                this.violationTypesEnum = hosViolationTypesEnum;
            }

        } else {
            super.setValueForKey(value, key);
        }
    }

}
