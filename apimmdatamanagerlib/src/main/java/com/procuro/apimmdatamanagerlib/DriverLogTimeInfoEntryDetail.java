package com.procuro.apimmdatamanagerlib;

import org.json.JSONObject;

import java.util.HashMap;

import static com.procuro.apimmdatamanagerlib.DriverLogTimeInfoEntryDetail.DriverLogTimeInfoEntryDetailType.DriverLogTimeInfoEntryDetailType_DC;

/**
 * Created by jophenmarieweeks on 11/07/2017.
 */

public class DriverLogTimeInfoEntryDetail extends PimmBaseObject {

    public enum DriverLogTimeInfoEntryDetailType {
        DriverLogTimeInfoEntryDetailType_DC(0),
        DriverLogTimeInfoEntryDetailType_Stop(1),
        DriverLogTimeInfoEntryDetailType_POI(2);

        private int value;
        private DriverLogTimeInfoEntryDetailType(int value)
        {
            this.value = value;
        }


        public int getValue() {
            return this.value;
        }

        public static DriverLogTimeInfoEntryDetailType setIntValue (int i) {
            for (DriverLogTimeInfoEntryDetailType type : DriverLogTimeInfoEntryDetailType.values()) {
                if (type.value == i) { return type; }
            }
            return DriverLogTimeInfoEntryDetailType_DC;
        }


    }

    public AddressComponents AddressComponents;
    public LatLon LatLon;
    public DriverLogTimeInfoEntryDetailType driverLogTimeInfoEntryDetailType;
    public String DriverLogTimeInfoEntryDetailId;
    public String Label;

//-(NSDictionary* ) dictionaryWithValuesForKeysForPrint;

    @Override
    public void setValueForKey(Object value, String key) {

        if(key.equalsIgnoreCase("driverLogTimeInfoEntryDetailType")) {
            if (!value.equals(null)) {
                DriverLogTimeInfoEntryDetailType logTimeInfoEntryDetailType = DriverLogTimeInfoEntryDetailType.setIntValue((int) value);
                this.driverLogTimeInfoEntryDetailType = logTimeInfoEntryDetailType;

            }
        }else if(key.equalsIgnoreCase("AddressComponents")){

            if(!value.equals(null)) {
                AddressComponents addressComponents = new AddressComponents();
                JSONObject jsonObject = (JSONObject) value;
                addressComponents.readFromJSONObject(jsonObject);
                this.AddressComponents = addressComponents;
            }
        }else if(key.equalsIgnoreCase("LatLon")){

            if(!value.equals(null)) {
                LatLon latLon = new LatLon();
                JSONObject jsonObject = (JSONObject) value;
                latLon.readFromJSONObject(jsonObject);
                this.LatLon = latLon;
            }
        } else {
            super.setValueForKey(value, key);
        }
    }
    public HashMap<String,Object> dictionaryWithValuesForKeys() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();


        dictionary.put("AddressComponents", (this.AddressComponents == null) ? null : this.AddressComponents.dictionaryWithValuesForKeys());
        dictionary.put("LatLon", (this.LatLon == null) ? null : this.AddressComponents.dictionaryWithValuesForKeys());
        dictionary.put("DriverLogTimeInfoEntryDetailId", this.DriverLogTimeInfoEntryDetailId);
        dictionary.put("Label", this.Label);
        dictionary.put("driverLogTimeInfoEntryDetailType", (this.driverLogTimeInfoEntryDetailType == null) ? DriverLogTimeInfoEntryDetailType_DC : this.driverLogTimeInfoEntryDetailType.value);

        return dictionary;
    }

}
