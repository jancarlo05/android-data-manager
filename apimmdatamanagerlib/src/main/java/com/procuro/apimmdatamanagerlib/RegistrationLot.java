package com.procuro.apimmdatamanagerlib;

import java.util.Date;

/**
 * Created by jophenmarieweeks on 01/08/2017.
 */

public class RegistrationLot extends PimmBaseObject {

    public String registrationLotId;
    public String itemNumber;
    public String lotNumber;
    public String productDescription;
    public Number binCount;
    public Number caseCount;
    public Date harvestDate;
    public Date useByDate;

}
