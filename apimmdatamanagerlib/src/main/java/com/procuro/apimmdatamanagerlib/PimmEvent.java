package com.procuro.apimmdatamanagerlib;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by jophenmarieweeks on 06/07/2017.
 */

public class PimmEvent extends PimmBaseObject {

    public int eventId;
    public Date creationTime;
    public String instanceId;
    public String instanceName;
    public int eventType;
    public Date eventStartTime;
    public Date eventEndTime;
    public double severity;
    public String displayString;
    public String deviceId;
    public String referenceInfo;
    public String bound;
    public String location;
    public String progression;
    public String severityString;
    public String durationString;
    public String compartmentLabel;
    public Date flaggedTime;
    public List<PimmEventNote> notes; // Array of PimmEventNotes

    @Override
    public void setValueForKey(Object value, String key) {

        if (key.equalsIgnoreCase("notes")) {
            if (this.notes == null) {
                this.notes = new ArrayList<PimmEventNote>();
            }

            JSONArray arrData = (JSONArray) value;

            if (arrData != null) {
                for (int i = 0; i < arrData.length(); i++) {
                    try {
                        PimmEventNote pimmEventNote = new PimmEventNote();
                        pimmEventNote.readFromJSONObject(arrData.getJSONObject(i));

                        this.notes.add(pimmEventNote);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        } else {
            super.setValueForKey(value, key);
        }
    }
    public String getPrintString() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();

        dictionary.put("progression", this.progression);
        dictionary.put("location", this.location);
        dictionary.put("flagType", this.severity);
        dictionary.put("eventType", this.eventType);
        dictionary.put("displayString", this.displayString);
        dictionary.put("severityString", this.severityString);
        dictionary.put("referenceInfo", this.referenceInfo);
        dictionary.put("instanceId", this.instanceId);
        dictionary.put("deviceId", this.deviceId);
        dictionary.put("eventId", this.eventId);
        dictionary.put("instanceName", this.instanceName);
        dictionary.put("compartmentLabel", this.compartmentLabel);
        dictionary.put("bound", this.bound);

        dictionary.put("flaggedTime", this.flaggedTime);
        dictionary.put("eventStartTime", this.eventStartTime);
        dictionary.put("eventEndTime", this.eventEndTime);
        dictionary.put("creationTime", this.creationTime);

        return dictionary.toString();
    }

}
