package com.procuro.apimmdatamanagerlib;

import org.json.JSONObject;

import java.util.Date;

/**
 * Created by jophenmarieweeks on 27/07/2017.
 */

public class ELDSequenceNum extends PimmBaseObject {
    public String eldEventID;
    public HOSEnums.HOSEventTypeEnum eventType;
    public int eventCode;
    public Date creationTimeUTC;
    public Date eventTimeUTC;
    public int eldSequenceNumber;
    public String eldDeviceID;
    public HOSEnums.ELDEventProviderEnum eldEventProvider;
    public int odometer; //int
    public double engineHours; //double
    public String locationString;
    public double lat;
    public double lon;

    @Override
    public void setValueForKey(Object value, String key) {

        if(key.equalsIgnoreCase("eventType")) {
            if (!value.equals(null)) {
                if (!value.equals(null)) {
                    HOSEnums.HOSEventTypeEnum hosEventTypeEnum = HOSEnums.HOSEventTypeEnum.setIntValue((int) value);
                    this.eventType = hosEventTypeEnum;
                }
            }
        }else if(key.equalsIgnoreCase("eldEventProvider")) {
            if (!value.equals(null)) {
                if (!value.equals(null)) {
                    HOSEnums.ELDEventProviderEnum eldEventProvider = HOSEnums.ELDEventProviderEnum.setIntValue((int) value);
                    this.eldEventProvider = eldEventProvider;
                }
            }

        } else {
            super.setValueForKey(value, key);
        }
    }


}
