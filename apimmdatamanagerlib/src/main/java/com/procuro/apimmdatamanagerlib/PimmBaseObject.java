package com.procuro.apimmdatamanagerlib;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.Date;
import java.util.Iterator;

/**
 * Created by EmmanKusumi on 7/4/17.
 */

public class PimmBaseObject
{
    public void readFromJSONObject(JSONObject jsonObject)
    {
        Iterator<String> iterator = jsonObject.keys();
        while(iterator.hasNext())
        {
            String key = iterator.next();
            Object value = null;
            try {
                value = jsonObject.get(key);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if(value != null)
            {
                this.setValueForKey(value, key);
            }
        }
    }

    public void setValueForKey(Object value, String key)
    {
        Field[] fields = this.getClass().getFields();

        for(Field field : fields)
        {
            if(field.getName().equalsIgnoreCase(key))
            {
                try
                {
                    if(field.getType() == String.class)
                    {
                        field.set(this, String.valueOf(value));
                    }
                    else if(field.getType() == Date.class)
                    {
                        Date date = JSONDate.convertJSONDateToNSDate(String.valueOf(value));
                        field.set(this, date);
                    }
                    else if(field.getType() == boolean.class)
                    {
                        field.set(this, Boolean.valueOf(value.toString()));
                    }
                    else if(field.getType() == int.class)
                    {
                        field.set(this, ((!value.equals(null)) ? Integer.parseInt(value.toString()) : 0));
                    }
                }
                catch (IllegalAccessException e)
                {
                    e.printStackTrace();
                }
            }
        }
    }
}
