package com.procuro.apimmdatamanagerlib;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

public class PimmInstance extends PimmBaseObject {
    public int instance;
    public String instanceId;
    public String description;
    public String staticValue;
    public String type;
    public int  charttype;
    public int  isdynamic;
    public Date creationDate;
    public Date lastUpdateDate;
    public Date lastAlarmDate;
    public int  state;
    public int  severity;
    public Boolean oos;
    public Boolean eventsEnabled;
    public Boolean commandsEnabled;
    public int  instanceOrder;
    public int  nodeClass;
    public int  nodeState;
    public int  eventSeverity;
    public Boolean trackingData;
    public int  inactivityTimer;
    public String realInstanceId;
    public String objectName;

    @Override
    public void setValueForKey(Object value, String key) {
        super.setValueForKey(value, key);
    }

    public HashMap<String,Object> dictionaryWithValuesForKeys() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();

        dictionary.put("instance",this.instance);
        dictionary.put("instanceId",this.instanceId);
        dictionary.put("description",this.description);
        dictionary.put("staticValue",this.staticValue);
        dictionary.put("type",this.type);
        dictionary.put("charttype",this.charttype);
        dictionary.put("isdynamic",this.isdynamic);
        dictionary.put("creationDate",this.creationDate);
        dictionary.put("lastUpdateDate",this.lastUpdateDate);
        dictionary.put("lastAlarmDate",this.lastAlarmDate);
        dictionary.put("state",this.state);
        dictionary.put("severity",this.severity);
        dictionary.put("oos",this.oos);
        dictionary.put("eventsEnabled",this.eventsEnabled);
        dictionary.put("commandsEnabled",this.commandsEnabled);
        dictionary.put("instanceOrder",this.instanceOrder);
        dictionary.put("nodeClass",this.nodeClass);
        dictionary.put("nodeState",this.nodeState);
        dictionary.put("eventSeverity",this.eventSeverity);
        dictionary.put("trackingData",this.trackingData);
        dictionary.put("inactivityTimer",this.inactivityTimer);
        dictionary.put("realInstanceId",this.realInstanceId);
        dictionary.put("objectName",this.objectName);


        return dictionary;
    }

}
