package com.procuro.apimmdatamanagerlib;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;

public class HuddleTeam extends  PimmBaseObject {

    public  int shift;
    public String opsLeader;
    public String supportManager1;
    public String supportManager2;


    @Override
    public void setValueForKey(Object value, String key) {
        super.setValueForKey(value, key);

        if (key.equalsIgnoreCase("shift")){
            if (value!=null){
                this.shift = (int) value;
            }
        }

        if (key.equalsIgnoreCase("opsLeader")){
            if (value!=null){
                this.opsLeader = value.toString();
            }
        }

        if (key.equalsIgnoreCase("supportManager1")){
            if (value!=null){
                this.supportManager1 = (String) value.toString();
            }
        }

        if (key.equalsIgnoreCase("supportManager2")){
            if (value!=null){
                this.supportManager2 = (String) value.toString();
            }
        }

    }


    public HashMap<String,Object> dictionaryWithValuesForKeys() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();

        dictionary.put("shift", this.shift);
        dictionary.put("opsLeader", this.opsLeader);
        dictionary.put("opsLeadsupportManager1er", this.supportManager1);
        dictionary.put("supportManager2", this.supportManager2);

        return dictionary;
    }

}
