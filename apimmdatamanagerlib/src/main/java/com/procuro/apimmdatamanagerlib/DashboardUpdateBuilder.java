package com.procuro.apimmdatamanagerlib;

/**
 * Created by jophenmarieweeks on 06/07/2017.
 */

public class DashboardUpdateBuilder extends PimmBaseObject {

    public double AvgSpeed;
    public double MPG;
    public double Distance;
}
