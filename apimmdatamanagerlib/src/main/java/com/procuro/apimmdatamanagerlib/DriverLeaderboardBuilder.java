package com.procuro.apimmdatamanagerlib;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jophenmarieweeks on 06/07/2017.
 */

public class DriverLeaderboardBuilder extends PimmBaseObject {

    public ArrayList<DriverScores> DriversScores;

    @Override
    public void setValueForKey(Object value, String key) {
        if (key.equalsIgnoreCase("DriversScores")) {
            if (this.DriversScores == null) {
                this.DriversScores = new ArrayList<DriverScores>();
            }

            if (!value.equals(null)) {
                JSONArray arrData = (JSONArray) value;

                if (arrData != null) {
                    for (int i = 0; i < arrData.length(); i++) {
                        try {
                            DriverScores driverScores = new DriverScores();
                            driverScores.readFromJSONObject(arrData.getJSONObject(i));

                            this.DriversScores.add(driverScores);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        } else {
            super.setValueForKey(value, key);
        }
    }

}

