package com.procuro.apimmdatamanagerlib.HOSDataModel.HOSEditor;

import com.procuro.apimmdatamanagerlib.AddressComponents;
import com.procuro.apimmdatamanagerlib.DeliveryPOITypeEnum;
import com.procuro.apimmdatamanagerlib.DriverLogTimeInfoDetails;
import com.procuro.apimmdatamanagerlib.HOSDataModel.HOSDataManager;
import com.procuro.apimmdatamanagerlib.LatLon;

import org.json.JSONObject;

import java.util.Date;

/**
 * Created by jophenmarieweeks on 13/12/2017.
 */

public class HOSTimeEntry extends HOSDataManager {


   public enum TimeEntryType {
        TimeEntryType_Unknown(0),
        TimeEntryType_Stop(1),
        TimeEntryType_POI(2),
        TimeEntryType_OnDutyNonDriving(3),
        TimeEntryType_RestBreak(4),
        TimeEntryType_Sleeper(5),
        TimeEntryType_Driving(6),
        TimeEntryType_OffDuty(7),
        TimeEntryType_PreTrip(8),
        TimeEntryType_PostTrip(9),
        TimeEntryType_Departure(10),
        TimeEntryType_Return(11),
        TimeEntryType_StartPoint(12),
        TimeEntryType_EndPoint(13),
        TimeEntryType_EndOfRoute(14),
        TimeEntryType_LastStatus(15);

       private int value;

       private TimeEntryType( int value){
           this.value = value;
       }

       public int getValue() {
           return this.value;
       }

       public static TimeEntryType setIntValue (int i) {
           for (TimeEntryType type : TimeEntryType.values()) {
               if (type.value == i) { return type; }
           }
           return TimeEntryType_Unknown;
       }
    }
    public TimeEntryType entryType;
    public DeliveryPOITypeEnum.DeliveryPOITypesEnum deliveryType;
    public DriverLogTimeInfoDetails.DriverLastStatusEnum lastStatus;
    public AddressComponents address;
    public LatLon latLon;

    public String stopId, stopName, label, annotation;
    public String startText, endText, duration, auditBy, comment;
    public Date start, end, originalStart, originalEnd, deliveryStart, deliveryEnd, auditDate;



    @Override
    public void setValueForKey(Object value, String key) {

        if(key.equalsIgnoreCase("entryType")) {
            if (!value.equals(null)) {
                TimeEntryType result = TimeEntryType.setIntValue((int) value);
                this.entryType = result;
            }
        } else if(key.equalsIgnoreCase("deliveryType")) {
            if (!value.equals(null)) {
                DeliveryPOITypeEnum.DeliveryPOITypesEnum result = DeliveryPOITypeEnum.DeliveryPOITypesEnum.setIntValue((int) value);
                this.deliveryType = result;
            }
        } else if(key.equalsIgnoreCase("lastStatus")) {
            if (!value.equals(null)) {
                DriverLogTimeInfoDetails.DriverLastStatusEnum result = DriverLogTimeInfoDetails.DriverLastStatusEnum.setIntValue((int) value);
                this.lastStatus = result;
            }
        }else if(key.equalsIgnoreCase("address")) {
            if (!value.equals(null)) {
                AddressComponents result = new AddressComponents();
                JSONObject jsonObject = (JSONObject) value;
                result.readFromJSONObject(jsonObject);
                this.address = result;
            }
        }else if(key.equalsIgnoreCase("latLon")) {
            if (!value.equals(null)) {
                LatLon result = new LatLon();
                JSONObject jsonObject = (JSONObject) value;
                result.readFromJSONObject(jsonObject);
                this.latLon = result;
            }

        } else {
            super.setValueForKey(value, key);
        }
    }


}

