package com.procuro.apimmdatamanagerlib.HOSDataModel;

import com.procuro.apimmdatamanagerlib.AddressComponents;
import com.procuro.apimmdatamanagerlib.PimmBaseObject;
import com.procuro.apimmdatamanagerlib.SdrDriver;

import org.json.JSONObject;

import java.util.Date;

/**
 * Created by jophenmarieweeks on 11/10/2017.
 */

public class HOSRouteInfo extends PimmBaseObject {
    public String DeliveryId;
    public String RouteName;
    public String TractorName;
    public String TrailerName;
    public Date DepartureTime;
    public Date  ArrivalTime;
    public SdrDriver Driver;
    public SdrDriver CoDriver;

    @Override
    public void setValueForKey(Object value, String key) {

        if (key.equalsIgnoreCase("Driver")) {
            if (!value.equals(null)) {
                SdrDriver sdrDriver = new SdrDriver();
                JSONObject jsonObject = (JSONObject) value;
                sdrDriver.readFromJSONObject(jsonObject);

                this.Driver = sdrDriver;
            }
        } else if (key.equalsIgnoreCase("CoDriver")) {
            if (!value.equals(null)) {
                SdrDriver sdrDriver = new SdrDriver();
                JSONObject jsonObject = (JSONObject) value;
                sdrDriver.readFromJSONObject(jsonObject);

                this.CoDriver = sdrDriver;
            }

        } else {
            super.setValueForKey(value, key);
        }
    }



}
