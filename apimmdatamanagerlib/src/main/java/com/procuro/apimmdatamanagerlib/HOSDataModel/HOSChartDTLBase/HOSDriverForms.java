package com.procuro.apimmdatamanagerlib.HOSDataModel.HOSChartDTLBase;

import android.content.Context;
import android.util.Log;

import com.procuro.apimmdatamanagerlib.DateTimeFormat;
import com.procuro.apimmdatamanagerlib.DriverData;
import com.procuro.apimmdatamanagerlib.DriverLogTimeInfoDetails;
import com.procuro.apimmdatamanagerlib.DriverTimeLog;
import com.procuro.apimmdatamanagerlib.HOSDataModel.HOSDataManager;
import com.procuro.apimmdatamanagerlib.HOSDataModel.HOSEditor.HOSTimeEntry;
import com.procuro.apimmdatamanagerlib.PimmForm;
import com.procuro.apimmdatamanagerlib.SdrDriver;
import com.procuro.apimmdatamanagerlib.iPimmDriverLogManager;

import org.json.JSONObject;

import java.util.ArrayList;

import static com.procuro.apimmdatamanagerlib.DriverLogTimeInfoDetails.DriverLastStatusEnum.DriverLastStatusEnum_GoHome;
import static com.procuro.apimmdatamanagerlib.DriverLogTimeInfoDetails.DriverLastStatusEnum.DriverLastStatusEnum_GoOnBreak;
import static com.procuro.apimmdatamanagerlib.DriverLogTimeInfoDetails.DriverLastStatusEnum.DriverLastStatusEnum_Sleeper;
import static com.procuro.apimmdatamanagerlib.DriverLogTimeInfoDetails.DriverLastStatusEnum.DriverLastStatusEnum_StayOnDuty;
import static com.procuro.apimmdatamanagerlib.DriverLogTimeInfoDetails.DriverLastStatusEnum.DriverLastStatusEnum_Undefined;

/**
 * Created by jophenmarieweeks on 07/12/2017.
 */

public class HOSDriverForms extends HOSDataManager {


    public DriverLogTimeInfoDetails.DriverLastStatusEnum prev, nextStatus;
    public DriverData first, last;
    public SdrDriver driver;
    public String referenceId;
    public ArrayList<HOSDriverLogSummary> summaries;
    private static final HOSDriverForms ourInstance = new HOSDriverForms();


    public Context mContext;
    public void setContext(Context ctx) {
        this.mContext = ctx;
    }

    public static HOSDriverForms getInstance()
    {
        return ourInstance;
    }

    private HOSDriverForms()
    {

    }



    @Override
    public void setValueForKey(Object value, String key) {

        if (key.equalsIgnoreCase("first")) {
            if (!value.equals(null)) {
                DriverData result = new DriverData();
                JSONObject jsonObject = (JSONObject) value;
                result.readFromJSONObject(jsonObject);

                this.first = result;
            }
        }else if (key.equalsIgnoreCase("last")) {
            if (!value.equals(null)) {
                DriverData result = new DriverData();
                JSONObject jsonObject = (JSONObject) value;
                result.readFromJSONObject(jsonObject);

                this.last = result;
            }
        }else if (key.equalsIgnoreCase("driver")) {
            if (!value.equals(null)) {
                SdrDriver result = new SdrDriver();
                JSONObject jsonObject = (JSONObject) value;
                result.readFromJSONObject(jsonObject);

                this.driver = result;
            }
        } else if (key.equalsIgnoreCase("nextStatus")) {

            if (!value.equals(null)) {
                DriverLogTimeInfoDetails.DriverLastStatusEnum driverLastStatusEnum = DriverLogTimeInfoDetails.DriverLastStatusEnum.setIntValue((int) value);
                this.nextStatus = driverLastStatusEnum;
            }
        } else if (key.equalsIgnoreCase("prev")) {

            if (!value.equals(null)) {
                DriverLogTimeInfoDetails.DriverLastStatusEnum driverLastStatusEnum = DriverLogTimeInfoDetails.DriverLastStatusEnum.setIntValue((int) value);
                this.prev = driverLastStatusEnum;
            }
        }else {
            super.setValueForKey(value, key);
        }
    }


    public static HOSDriverForms initWithHOSDriver(SdrDriver driver, ArrayList<PimmForm> arrForms)
    {
        ourInstance.summaries = new ArrayList<>();
        ourInstance.driver = driver;
        ourInstance.buildDriverSummaryWithPimmForms(arrForms);

        return ourInstance;
    }

    public void buildDriverSummaryWithPimmForms(ArrayList<PimmForm> arrForms) {

        ArrayList<HOSDriverLogSummary> arrSummaries = new ArrayList<>();
        String strDriverRouteLogs = "";


        for (PimmForm pimmForm : arrForms){
//            @autoreleasepool {
            iPimmDriverLogManager driverManager = iPimmDriverLogManager.getDriverLogManager();
            DriverTimeLog timeLog = driverManager.getDriverTimeLogFromJSONData(pimmForm.bodyData);

            HOSDriverLogSummary driverSummary =  HOSDriverLogSummary.initWithDriverTimeLog(timeLog);
            HOSDriverLogSummary lastDriverSummary = null;

            if (arrSummaries.size() > 0) {
                lastDriverSummary = arrSummaries.get(arrSummaries.size() - 1);

                driverSummary.firstStatus = lastDriverSummary.lastStatus;
                driverSummary.firstEntry = lastDriverSummary.lastEntry;
            }

            driverSummary.buildRouteEntriesForDriverId(driver.UserId);

            if (lastDriverSummary != null) {
                HOSTimeEntry prevLastEntry = lastDriverSummary.entries.get(lastDriverSummary.entries.size() - 1);
                DriverLogTimeInfoDetails.DriverLastStatusEnum prevLastStatus = lastDriverSummary.lastStatus;

                // Get info of next route delivery
                lastDriverSummary.routeNext = driverSummary.routeName;
                lastDriverSummary.deliveryNextId = driverSummary.deliveryId;
                lastDriverSummary.formNextId = driverSummary.formId;

                strDriverRouteLogs = strDriverRouteLogs + " - " + lastDriverSummary.routeName + "(" + prevLastStatus + ")";

                if (prevLastStatus == DriverLastStatusEnum_GoHome) {
                    // If the previous route is already punch-out, update the pre-trip start time using punch-in time
                    if (driverSummary.punchStart != null) {
                        HOSTimeEntry preTripEntry = driverSummary.entries.get(0);

                        preTripEntry.startText = DateTimeFormat.ShortFormatStringForDateTime(driverSummary.punchStart);
                        preTripEntry.start = driverSummary.punchStart;

                        driverSummary.preTrip.Start = driverSummary.punchStart;
                    }

                } else {
                    // Update punch start using the previous route punch start
                    if (lastDriverSummary.punchStart != null) {
                        if (lastDriverSummary.lastStatus != DriverLastStatusEnum_Undefined) {
                            driverSummary.punchStart = lastDriverSummary.punchStart;
                        }
                    }

                    if (prevLastStatus == DriverLastStatusEnum_StayOnDuty) {
                        HOSTimeEntry preTripEntry = driverSummary.entries.get(0);

                        preTripEntry.startText = DateTimeFormat.ShortFormatStringForDateTime(prevLastEntry.end);
                        preTripEntry.start = prevLastEntry.end;

                    } else if (prevLastStatus == DriverLastStatusEnum_GoOnBreak || prevLastStatus == DriverLastStatusEnum_Sleeper) {
                        HOSTimeEntry preTripEntry = driverSummary.entries.get(0);

                        lastDriverSummary.lastEntry.endText = DateTimeFormat.ShortFormatStringForDateTime(preTripEntry.start);
                        lastDriverSummary.lastEntry.end = preTripEntry.start;
                    }
                }

                if (driverSummary.lastStatus == DriverLastStatusEnum_GoHome) {
                    if (lastDriverSummary.lastStatus != DriverLastStatusEnum_GoHome) {
                        // strDriverRouteLogs = [strDriverRouteLogs stringByAppendingFormat:@" - %@", routeSummary.punchEnd];
                        // Display current punch end time to the previous work day
                        lastDriverSummary.punchEnd = driverSummary.punchEnd;
                    }
                }

                // Update original start and end time
                lastDriverSummary.lastEntry.originalStart = lastDriverSummary.lastEntry.start;
                lastDriverSummary.lastEntry.originalEnd = lastDriverSummary.lastEntry.end;
            }

            // Update original start and end time
            driverSummary.lastEntry.originalStart = driverSummary.lastEntry.start;
            driverSummary.lastEntry.originalEnd = driverSummary.lastEntry.end;

            arrSummaries.add(driverSummary);
//        }
        }
        Log.v("DM", "Found driver form entries: " + strDriverRouteLogs);

        this.summaries = arrSummaries;

    }


//    - (id)copyWithZone:(NSZone *)zone {
//        HOSDriverForms *driverForms = [[HOSDriverForms allocWithZone:zone] init];
//
//        NSMutableArray *arrSummaries = [[NSMutableArray alloc] init];
//
//        for (HOSDriverLogSummary *driverSummary in self.summaries) {
//        [arrSummaries addObject:driverSummary.copy];
//        }
//
//        driverForms.summaries = arrSummaries;
//
//        return driverForms;
//    }




}
