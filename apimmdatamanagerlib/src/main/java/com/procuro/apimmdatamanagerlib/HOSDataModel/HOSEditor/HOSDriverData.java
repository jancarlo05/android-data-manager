package com.procuro.apimmdatamanagerlib.HOSDataModel.HOSEditor;

import com.procuro.apimmdatamanagerlib.DriverData;
import com.procuro.apimmdatamanagerlib.DriverLogTimeInfoEntryDetail;
import com.procuro.apimmdatamanagerlib.HOSDataModel.HOSDataManager;
import com.procuro.apimmdatamanagerlib.SdrDriver;

import org.json.JSONObject;

/**
 * Created by jophenmarieweeks on 15/12/2017.
 */

public class HOSDriverData extends HOSDataManager {

    public SdrDriver driver;
    public DriverData data;

    public String referenceId;

    @Override
    public void setValueForKey(Object value, String key) {

        if (key.equalsIgnoreCase("driver")) {
            if (!value.equals(null)) {
                SdrDriver result = new SdrDriver();
                JSONObject jsonObject = (JSONObject) value;
                result.readFromJSONObject(jsonObject);
                this.driver = result;
            }

        }else if (key.equalsIgnoreCase("data")) {
            if (!value.equals(null)) {
                DriverData result = new DriverData();
                JSONObject jsonObject = (JSONObject) value;
                result.readFromJSONObject(jsonObject);
                this.data = result;
            }

        } else {
            super.setValueForKey(value, key);
        }

    }
}
