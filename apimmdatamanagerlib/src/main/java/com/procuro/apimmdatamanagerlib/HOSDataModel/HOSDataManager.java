package com.procuro.apimmdatamanagerlib.HOSDataModel;

import android.content.Context;

import com.procuro.apimmdatamanagerlib.DriverData;
import com.procuro.apimmdatamanagerlib.DriverTimeLog;
import com.procuro.apimmdatamanagerlib.PimmBaseObject;
import com.procuro.apimmdatamanagerlib.iPimmDriverLogManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by jophenmarieweeks on 11/10/2017.
 */

public class HOSDataManager extends PimmBaseObject {


    private static HOSDataManager ourInstance = new HOSDataManager();
    public static final String HOSPunchInText= "Punch-In";
    public static final String HOSPunchOutText= "Punch-Out";
    public static final String HOSPreTripText= "Pre-Trip";
    public static final String HOSPostTripText= "Post-Trip";
    public static final String HOSDepartureText= "Departure";
    public static final String HOSReturnText= "Return";
    public static final String HOSStartPointText= "Start Point";
    public static final String HOSStayOnDutyText= "Stay On Duty";
    public static final String HOSEndPointText= "End Point";
    public static final String HOSUnscheduledText= "Unscheduled Stop";
    public static final String HOSUnauthorizedText= "Unauthorized Stop";
    public static final String HOSEndOfRouteText= "End of Route";
    public static final String HOSDrivingText= "Driving";
    public static final String HOSGoOnBreakText= "Go On Break";
    public static final String HOSSleeperText= "Sleeper";
    public static final String HOSNotAvailableText= "Not Available";





    public Context mContext;
    public void setContext(Context ctx) {
        this.mContext = ctx;
    }

    public static HOSDataManager getHOSDataManager()
    {

        if(ourInstance == null){
            ourInstance = new HOSDataManager();
        }
        return ourInstance;
    }

     public void initWithDriverData(DriverData driverData, Date startDate , Date endDate){

        if(startDate!= null) {
            startDate = convertToStartDate(startDate);
        }

        if(endDate != null) {
            endDate = convertToEndDate(endDate);
        }

        this.currentDriverData = driverData;
        this.currentStartDate = startDate;
        this.currentEndDate = endDate;
    }

    private Date convertToEndDate(Date endDate) {
        return null;
    }

    private Date convertToStartDate(Date startDate) {
        return null;
    }


    public enum RestInsideStop {
        RestInsideStop_Unidentified(0),
        RestInsideStop_Begin(1),
        RestInsideStop_End(2),
        RestInsideStop_Middle(3),
        RestInsideStop_RestStop(4),
        RestInsideStop_Before(5),
        RestInsideStop_After(6);

        private int value;

        private RestInsideStop( int value){
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }

        public static RestInsideStop setIntValue (int i) {
            for (RestInsideStop type : RestInsideStop.values()) {
                if (type.value == i) { return type; }
            }
            return RestInsideStop_Unidentified;
        }

    }

    public iPimmDriverLogManager driverLogManager;
    public RestInsideStop restInsideStop;
    public DriverData currentDriverData;
    public Date currentStartDate;
    public Date currentEndDate;
    public HOSDayLogEntry currentRouteView;
    public ArrayList<HOSDayLogEntry> currentHOSViewArray; // Array of HOSDayLogEntry Objects
    public DriverTimeLog currentDriverTimeLog;

    @Override
    public void setValueForKey(Object value, String key) {

        if (key.equalsIgnoreCase("driverLogManager")) {
            if (!value.equals(null)) {
                iPimmDriverLogManager result = new iPimmDriverLogManager();
                JSONObject jsonObject = (JSONObject) value;
                result.readFromJSONObject(jsonObject);

                this.driverLogManager = result;
            }
        }else if (key.equalsIgnoreCase("currentDriverData")) {
            if (!value.equals(null)) {
                DriverData result = new DriverData();
                JSONObject jsonObject = (JSONObject) value;
                result.readFromJSONObject(jsonObject);

                this.currentDriverData = result;
            }
        }else if (key.equalsIgnoreCase("currentRouteView")) {
            if (!value.equals(null)) {
                HOSDayLogEntry result = new HOSDayLogEntry();
                JSONObject jsonObject = (JSONObject) value;
                result.readFromJSONObject(jsonObject);

                this.currentRouteView = result;
            }
        }else if (key.equalsIgnoreCase("currentDriverTimeLog")) {
            if (!value.equals(null)) {
                DriverTimeLog result = new DriverTimeLog();
                JSONObject jsonObject = (JSONObject) value;
                result.readFromJSONObject(jsonObject);

                this.currentDriverTimeLog = result;
            }
        } else if (key.equalsIgnoreCase("restInsideStop")) {

            if (!value.equals(null)) {
                RestInsideStop result =RestInsideStop.setIntValue((int) value);
                this.restInsideStop = result;
            }
        } else if (key.equalsIgnoreCase("currentHOSViewArray")) {

            if (this.currentHOSViewArray == null) {
                this.currentHOSViewArray = new ArrayList<HOSDayLogEntry>();
            }

            if (!value.equals(null)) {
                JSONArray arrStops = (JSONArray) value;

                if (arrStops != null) {
                    for (int i = 0; i < arrStops.length(); i++) {
                        try {
                            HOSDayLogEntry result = new HOSDayLogEntry();
                            result.readFromJSONObject(arrStops.getJSONObject(i));

                            this.currentHOSViewArray.add(result);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }

            }
        }else {
            super.setValueForKey(value, key);
        }
    }




}
