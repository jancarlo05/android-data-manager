package com.procuro.apimmdatamanagerlib.HOSDataModel;

import com.procuro.apimmdatamanagerlib.PimmBaseObject;
import com.procuro.apimmdatamanagerlib.PimmForm;
import com.procuro.apimmdatamanagerlib.SdrDelivery;
import com.procuro.apimmdatamanagerlib.SdrDriver;
import com.procuro.apimmdatamanagerlib.SdrReport;

import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

import static com.procuro.apimmdatamanagerlib.DriverLogTimeInfoDetails.DriverLastStatusEnum.DriverLastStatusEnum_GoOnBreak;
import static com.procuro.apimmdatamanagerlib.DriverLogTimeInfoDetails.DriverLastStatusEnum.DriverLastStatusEnum_Sleeper;
import static com.procuro.apimmdatamanagerlib.DriverLogTimeInfoDetails.DriverLastStatusEnum.DriverLastStatusEnum_StayOnDuty;

/**
 * Created by jophenmarieweeks on 11/10/2017.
 */

public class HOSEditorManager extends PimmBaseObject {

    public SdrDriver driver;
    public Date routeScheduled;
    public TimeZone timeZone;
    public ArrayList<SdrReport> shipments;
    public ArrayList<PimmForm> forms;
    public ArrayList  timelogs;
    public HOSRouteTimeLog routeTimeLog;



    public void initWithShipments (ArrayList arrShipments, ArrayList arrForms){
        this.shipments = arrShipments;
        this.forms = arrForms;
    }

    public ArrayList buildRouteTimeLogsForDriver (SdrDriver sdrDriver){
        this.driver = sdrDriver;

        ArrayList<HOSRouteTimeLog> arrTimeLogs = new ArrayList();
        ArrayList<PimmForm> arrRefxs = this.forms;

        for (SdrReport sdrReport : this.shipments) {
//            @autoreleasepool {
                SdrDelivery sdrDelivery = sdrReport.Hierarchy;
                PimmForm pimmForm = null;

                if (arrRefxs.contains(sdrDelivery.DeliveryId)) {
                    int index = arrRefxs.indexOf(sdrDelivery.DeliveryId);

                    pimmForm = this.forms.get(index);
                }

                HOSRouteTimeLog timeLog = HOSRouteTimeLog.initWithSdrReport(sdrReport ,pimmForm);

            timeLog.buildTimeLogEntriesForDriver(sdrDriver);
            arrTimeLogs.add(timeLog);
//            }
        }


        for (HOSRouteTimeLog routeTimeLog : arrTimeLogs){
            int index = arrTimeLogs.indexOf(routeTimeLog);
            // Check pre-trip start time using the previous route last status

            if (index > 0) {
                HOSRouteTimeLog prevRouteLog = arrTimeLogs.get(index-1);

                // Update pre-trip start time using previous route last entry
                if (prevRouteLog.lastStatus == DriverLastStatusEnum_StayOnDuty ||
                        prevRouteLog.lastStatus == DriverLastStatusEnum_GoOnBreak ||
                        prevRouteLog.lastStatus == DriverLastStatusEnum_Sleeper) {

                    HOSTimeLogEntry timeLogEntry = prevRouteLog.routeEntries.get(prevRouteLog.routeEntries.size() - 1);

                   routeTimeLog.routeEntries.get(0).StartText = timeLogEntry.EndText;
                    routeTimeLog.routeEntries.get(0).Start = timeLogEntry.End;

                    routeTimeLog.preTripStart = timeLogEntry.End;
                }
            }
        }

        this.timelogs = arrTimeLogs;

        return this.timelogs;

    }
}
