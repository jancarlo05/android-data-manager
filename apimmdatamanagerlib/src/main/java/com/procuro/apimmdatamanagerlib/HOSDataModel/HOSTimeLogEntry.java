package com.procuro.apimmdatamanagerlib.HOSDataModel;

import com.procuro.apimmdatamanagerlib.DateTimeFormat;
import com.procuro.apimmdatamanagerlib.DeliveryPOITypeEnum;
import com.procuro.apimmdatamanagerlib.DriverLogTimeInfoEntryDetail;
import com.procuro.apimmdatamanagerlib.DriverTimeLog;
import com.procuro.apimmdatamanagerlib.PimmBaseObject;

import org.json.JSONObject;

import java.util.Date;
import java.util.Objects;

import static com.procuro.apimmdatamanagerlib.HOSDataModel.HOSDataManager.RestInsideStop.RestInsideStop_Before;
import static com.procuro.apimmdatamanagerlib.HOSDataModel.HOSDataManager.RestInsideStop.RestInsideStop_Begin;
import static com.procuro.apimmdatamanagerlib.HOSDataModel.HOSDataManager.RestInsideStop.RestInsideStop_End;
import static com.procuro.apimmdatamanagerlib.HOSDataModel.HOSDataManager.RestInsideStop.RestInsideStop_Middle;
import static com.procuro.apimmdatamanagerlib.HOSDataModel.HOSDataManager.RestInsideStop.RestInsideStop_RestStop;
import static com.procuro.apimmdatamanagerlib.HOSDataModel.HOSDataManager.RestInsideStop.RestInsideStop_Unidentified;

/**
 * Created by jophenmarieweeks on 11/10/2017.
 */

public class HOSTimeLogEntry extends PimmBaseObject {



    public enum TimeLogEntryType {
        TimeLogEntryType_Unknown(0),
        TimeLogEntryType_Stop(1),
        TimeLogEntryType_POI(2),
        TimeLogEntryType_OnDutyNonDriving(3),
        TimeLogEntryType_RestBreak(4),
        TimeLogEntryType_Sleeper(5),
        TimeLogEntryType_Driving(6),
        TimeLogEntryType_OffDuty(7),
        TimeLogEntryType_PreTrip(8),
        TimeLogEntryType_PostTrip(9),
        TimeLogEntryType_Departure(10),
        TimeLogEntryType_Return(11),
        TimeLogEntryType_StartPoint(12),
        TimeLogEntryType_EndPoint(13),
        TimeLogEntryType_EndOfRoute(14);

       private int value;

       private TimeLogEntryType( int value){
           this.value = value;
       }

       public int getValue() {
           return this.value;
       }

       public static TimeLogEntryType setIntValue (int i) {
           for (TimeLogEntryType type : TimeLogEntryType.values()) {
               if (type.value == i) { return type; }
           }
           return TimeLogEntryType_Unknown;
       }
    }

    public TimeLogEntryType timeLogEntryType;
    public String Label;
    public Date Start;
    public Date End;
    public String StartText;
    public String EndText;
    public String StopId;
    public String StopName;
    public String Annotation;
    public String DeliveryId;
    public DeliveryPOITypeEnum.DeliveryPOITypesEnum DeliveryType;
    public DriverLogTimeInfoEntryDetail StartDetail;
    public DriverLogTimeInfoEntryDetail EndDetail;

    public Date OriginalStart;
    public Date OriginalEnd;
    public Date DeliveryStart;
    public Date DeliveryEnd;
    public String Comment;
    public String Duration;
    public String AuditBy;
    public Date AuditDate;


    @Override
    public void setValueForKey(Object value, String key) {

        if(key.equalsIgnoreCase("timeLogEntryType")) {
            if (!value.equals(null)) {
                TimeLogEntryType result = TimeLogEntryType.setIntValue((int) value);
                this.timeLogEntryType = result;
            }
        }else if(key.equalsIgnoreCase("DeliveryType")) {
            if (!value.equals(null)) {
//                DeliveryPOITypeEnum result = new DeliveryPOITypeEnum();
//                JSONObject jsonObject = (JSONObject) value;
//                result.readFromJSONObject(jsonObject);
//                this.DeliveryType = result;
                DeliveryPOITypeEnum.DeliveryPOITypesEnum deliveryPOITypes = DeliveryPOITypeEnum.DeliveryPOITypesEnum.setIntValue((int) value);
                this.DeliveryType = deliveryPOITypes;

            }
        }else if(key.equalsIgnoreCase("StartDetail")) {
            if (!value.equals(null)) {
                DriverLogTimeInfoEntryDetail result = new DriverLogTimeInfoEntryDetail();
                JSONObject jsonObject = (JSONObject) value;
                result.readFromJSONObject(jsonObject);
                this.StartDetail = result;
            }
        }else if(key.equalsIgnoreCase("EndDetail")) {
            if (!value.equals(null)) {
                DriverLogTimeInfoEntryDetail result = new DriverLogTimeInfoEntryDetail();
                JSONObject jsonObject = (JSONObject) value;
                result.readFromJSONObject(jsonObject);
                this.EndDetail = result;
            }

        } else {
            super.setValueForKey(value, key);
        }
    }

    public HOSTimeLogEntry copyObject(){

       HOSTimeLogEntry  hosTimeLogEntry = new HOSTimeLogEntry();

        hosTimeLogEntry.Label = this.Label;
        hosTimeLogEntry.Start = this.Start;
        hosTimeLogEntry.StartText = this.StartText;
        hosTimeLogEntry.End = this.End;
        hosTimeLogEntry.EndText = this.EndText;
        hosTimeLogEntry.OriginalStart = this.OriginalStart;
        hosTimeLogEntry.OriginalEnd = this.OriginalEnd;
        hosTimeLogEntry.StopId = this.StopId;
        hosTimeLogEntry.StopName = this.StopName;
        hosTimeLogEntry.Comment = this.Comment;
        hosTimeLogEntry.Duration = this.Duration;
        hosTimeLogEntry.StartDetail = this.StartDetail;
        hosTimeLogEntry.EndDetail = this.EndDetail;
        hosTimeLogEntry.timeLogEntryType = this.timeLogEntryType;
        hosTimeLogEntry.DeliveryType = this.DeliveryType;
        hosTimeLogEntry.AuditBy = this.AuditBy;
        hosTimeLogEntry.AuditDate = this.AuditDate;
        hosTimeLogEntry.DeliveryStart = this.DeliveryStart;
        hosTimeLogEntry.DeliveryEnd = this.DeliveryEnd;


        return hosTimeLogEntry;
    }

//    public boolean isEqual(Objects object){
//
//        boolean equals = false;
//        if(this.Label.equalsIgnoreCase(object.Label) && (this.Start.compareTo(object.Start)) && (this.End.compareTo(object.End))) {
//            equals  = true;
//        } else {
//            equals = false;
//        }
//
//        return equals;
//    }

    public int isInsideStop(HOSTimeLogEntry stopEntry ){
        Date dateRestStart = this.Start;//DateTimeFormat.truncateSeconds(this.Start);
        Date dateRestEnd = this.End; // truncateSeconds];
        Date dateStopStart = stopEntry.Start; //truncateSeconds];
        Date dateStopEnd = stopEntry.End; //truncateSeconds];

        if (dateStopStart == dateRestStart) {
            // Same as Rest Stop
            if (dateStopEnd == dateRestEnd) {
                return RestInsideStop_RestStop.getValue();
            }

            return RestInsideStop_Begin.getValue();

        } else if (dateStopEnd == dateRestEnd) {
            // Same as Rest Stop
            if (dateStopStart == dateRestStart) {
                return RestInsideStop_RestStop.getValue();

            }
            return RestInsideStop_End.getValue();

        } else if ( DateTimeFormat.isBetweenDate(dateStopStart ,dateStopEnd)) {
            return RestInsideStop_Middle.getValue();

        } else {
            if ((dateStopStart.compareTo(dateRestStart) == 1)  && (dateStopStart.compareTo(dateRestEnd) == 1)) {
                return RestInsideStop_Before.getValue();
            }
        }

        return RestInsideStop_Unidentified.getValue();

    }

    public HOSTimeLogEntry switchStartEndValues() {

        Date dateStart = this.Start;
        Date dateEnd = this.End;

        this.StartText = DateTimeFormat.ShortFormatStringForDateTime(dateEnd);
        this.EndText = DateTimeFormat.ShortFormatStringForDateTime(dateStart);
        this.Start = dateEnd;
        this.End = dateStart;

        return this;

    }





}
