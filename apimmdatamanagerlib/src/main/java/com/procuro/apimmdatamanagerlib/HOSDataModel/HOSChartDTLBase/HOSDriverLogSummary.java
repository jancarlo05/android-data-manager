package com.procuro.apimmdatamanagerlib.HOSDataModel.HOSChartDTLBase;

import android.content.Context;
import android.util.Log;

import com.procuro.apimmdatamanagerlib.DateTimeFormat;
import com.procuro.apimmdatamanagerlib.DriverLogRestBreakInfoEntry;
import com.procuro.apimmdatamanagerlib.DriverLogTimeInfoDetails;
import com.procuro.apimmdatamanagerlib.DriverLogTimeInfoEntry;
import com.procuro.apimmdatamanagerlib.DriverLogTimeInfoEntryDetail;
import com.procuro.apimmdatamanagerlib.DriverTimeLog;
import com.procuro.apimmdatamanagerlib.HOSDataModel.HOSDataManager;
import com.procuro.apimmdatamanagerlib.HOSDataModel.HOSEditor.HOSTimeEntry;
import com.procuro.apimmdatamanagerlib.HOSDataModel.HOSRouteTimeLog;
import com.procuro.apimmdatamanagerlib.HOSDataModel.HOSTimeLogEntry;
import com.procuro.apimmdatamanagerlib.PimmForm;
import com.procuro.apimmdatamanagerlib.SdrDriver;
import com.procuro.apimmdatamanagerlib.SdrGisSite;
import com.procuro.apimmdatamanagerlib.SdrPointOfInterest;
import com.procuro.apimmdatamanagerlib.SdrStop;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static com.procuro.apimmdatamanagerlib.DeliveryPOITypeEnum.DeliveryPOITypesEnum.DeliveryPOIType_DeliveryStop;
import static com.procuro.apimmdatamanagerlib.DeliveryPOITypeEnum.DeliveryPOITypesEnum.DeliveryPOIType_EndPoint;
import static com.procuro.apimmdatamanagerlib.DeliveryPOITypeEnum.DeliveryPOITypesEnum.DeliveryPOIType_StartPoint;
import static com.procuro.apimmdatamanagerlib.DeliveryPOITypeEnum.DeliveryPOITypesEnum.DeliveryPOIType_Undefined;
import static com.procuro.apimmdatamanagerlib.DriverLogTimeInfoEntryDetail.DriverLogTimeInfoEntryDetailType.DriverLogTimeInfoEntryDetailType_DC;
import static com.procuro.apimmdatamanagerlib.DriverLogTimeInfoEntryDetail.DriverLogTimeInfoEntryDetailType.DriverLogTimeInfoEntryDetailType_POI;
import static com.procuro.apimmdatamanagerlib.HOSDataModel.HOSEditor.HOSTimeEntry.TimeEntryType.TimeEntryType_Departure;
import static com.procuro.apimmdatamanagerlib.HOSDataModel.HOSEditor.HOSTimeEntry.TimeEntryType.TimeEntryType_EndOfRoute;
import static com.procuro.apimmdatamanagerlib.HOSDataModel.HOSEditor.HOSTimeEntry.TimeEntryType.TimeEntryType_EndPoint;
import static com.procuro.apimmdatamanagerlib.HOSDataModel.HOSEditor.HOSTimeEntry.TimeEntryType.TimeEntryType_POI;
import static com.procuro.apimmdatamanagerlib.HOSDataModel.HOSEditor.HOSTimeEntry.TimeEntryType.TimeEntryType_PostTrip;
import static com.procuro.apimmdatamanagerlib.HOSDataModel.HOSEditor.HOSTimeEntry.TimeEntryType.TimeEntryType_PreTrip;
import static com.procuro.apimmdatamanagerlib.HOSDataModel.HOSEditor.HOSTimeEntry.TimeEntryType.TimeEntryType_RestBreak;
import static com.procuro.apimmdatamanagerlib.HOSDataModel.HOSEditor.HOSTimeEntry.TimeEntryType.TimeEntryType_Return;
import static com.procuro.apimmdatamanagerlib.HOSDataModel.HOSEditor.HOSTimeEntry.TimeEntryType.TimeEntryType_StartPoint;
import static com.procuro.apimmdatamanagerlib.HOSDataModel.HOSEditor.HOSTimeEntry.TimeEntryType.TimeEntryType_Stop;
import static com.procuro.apimmdatamanagerlib.HOSDataModel.HOSEditor.HOSTimeEntry.TimeEntryType.TimeEntryType_Unknown;
import static com.procuro.apimmdatamanagerlib.HOSDataModel.HOSTimeLogEntry.TimeLogEntryType.TimeLogEntryType_POI;
import static com.procuro.apimmdatamanagerlib.HOSDataModel.HOSTimeLogEntry.TimeLogEntryType.TimeLogEntryType_Stop;

/**
 * Created by jophenmarieweeks on 22/12/2017.
 */

public class HOSDriverLogSummary extends HOSDataManager {

    private static final HOSDriverLogSummary driverLogSummary = new HOSDriverLogSummary();


    public Context mContext;

    public void setContext(Context ctx) {
        this.mContext = ctx;
    }

    public static HOSDriverLogSummary getInstance()
    {
        return driverLogSummary;
    }

    private HOSDriverLogSummary()
    {

    }



   public DriverLogTimeInfoDetails.DriverLastStatusEnum firstStatus, lastStatus;

   public SdrDriver driver, coDriver;
   public DriverTimeLog timeLog;
   public DriverLogTimeInfoDetails infoDetails;
   public DriverLogTimeInfoEntry punch, preTrip, postTrip;
   public HOSTimeEntry firstEntry, lastEntry;
   public PimmForm form;

   public String deliveryId, deliveryNextId, formId, formNextId, routeId, routeName;
   public String trailerId, trailerName, tractorId, tractorName, routeNext, auditBy;
   public String dcId, dcName, dcAddress, startPOIId, endPOIId;
   public Date punchStart, punchEnd, dispatch, departure, startTime, arrival, auditAt;
    public ArrayList  restStops, sleeperStops;
    public ArrayList<SdrGisSite> sites;
    public ArrayList<SdrPointOfInterest> interestStops;
    public ArrayList<SdrStop> regularStops;
   public ArrayList<HOSTimeEntry> entries;
    public ArrayList missedStops, driving;
   public ArrayList alteredEntries;



    public static HOSDriverLogSummary initWithDriverTimeLog(DriverTimeLog timeLog) {


            driverLogSummary.driver = new SdrDriver();
            driverLogSummary.coDriver = new SdrDriver();
            driverLogSummary.timeLog = timeLog;

            if (timeLog.DriverID != null) {
                driverLogSummary.driver.UserId = timeLog.DriverID;
                driverLogSummary.driver.Username = timeLog.Driver;
            }

            if (timeLog.CoDriverID != null) {
                driverLogSummary.coDriver.UserId = timeLog.CoDriverID;
                driverLogSummary.coDriver.Username = timeLog.CoDriver;
            }

            driverLogSummary.deliveryId = timeLog.DeliveryID;
            driverLogSummary.formId = timeLog.FormID;
            driverLogSummary.routeName = timeLog.RouteName;
            driverLogSummary.tractorName = timeLog.Tractor;
            driverLogSummary.trailerName = timeLog.Trailer;
            driverLogSummary.dcName = timeLog.DC;
            driverLogSummary.dcAddress = timeLog.Address;


        return driverLogSummary;



    }

    public void buildRouteEntriesForDriverId(String strDriverId) {

        this.infoDetails = this.timeLog.TimeInfoDetails;

        ArrayList<DriverLogRestBreakInfoEntry> arrRestStops = this.infoDetails.RestStops;

        if (!(this.timeLog.DriverID.equalsIgnoreCase(strDriverId))) {
            this.infoDetails = this.timeLog.CoDriverTimeInfoDetails;
        }
        Log.v("DM","Driver time info details: " +  this.infoDetails.dictionaryWithValuesForKeys());

        // Get the first rest break related to the previous route last status: "Go on Break"

        if (arrRestStops.size() > 0) {
            DriverLogRestBreakInfoEntry restBreak = arrRestStops.get(0);

            if (this.preTrip.Start.equals(restBreak.End)) {
                this.firstEntry = new HOSTimeEntry();

                this.firstEntry.entryType = TimeEntryType_RestBreak;
                this.firstEntry.startText = DateTimeFormat.ShortFormatStringForDateTime(restBreak.Start);
                this.firstEntry.endText = DateTimeFormat.ShortFormatStringForDateTime(restBreak.End);
                this.firstEntry.start = restBreak.Start;
                this.firstEntry.end = restBreak.End;

                this.firstEntry.address = restBreak.StartDetail.AddressComponents;
                this.firstEntry.latLon = restBreak.StartDetail.LatLon;

            arrRestStops.remove(0);
            }

            this.restStops = arrRestStops;
        }

        ArrayList<HOSTimeEntry> arrRouteEntries = new ArrayList<>();
        ArrayList<HOSTimeEntry> arrDCEntries = new ArrayList<>();
        ArrayList<HOSTimeEntry> arrSegments = new ArrayList<>();

        // Get stops and point of interests
        ArrayList<HOSTimeLogEntry> arrStops = this.buildStopLogEntries(this.regularStops);
        ArrayList<HOSTimeLogEntry> arrPOIs = this.buildPOILogEntries(this.interestStops);

        List<String> arrPlanLabels = new ArrayList<>(Arrays.asList(HOSDepartureText, HOSReturnText, HOSPostTripText, HOSEndOfRouteText, HOSPreTripText));
        ArrayList arrPOILabels = null;

        this.arrival = this.infoDetails.Dispatch.End;

        // this.departure = sdrDelivery.DepartureTime;
        // this.startTime = sdrDelivery.StartTime;

        this.punch = this.infoDetails.Punch;
        this.preTrip = this.infoDetails.PreTrip;
        this.postTrip = this.infoDetails.PostTrip;

        this.punchStart = this.punch.Start;
        this.punchEnd = this.punch.End;
        this.lastStatus = this.infoDetails.LastStatus;

        if (this.coDriver != null) {
            for (DriverLogTimeInfoEntry segment : this.infoDetails.DriveSegments) {
                DriverLogTimeInfoEntryDetail endDetail = segment.EndDetail;
                HOSTimeEntry timeEntry = new HOSTimeEntry();


                timeEntry.stopId = endDetail.DriverLogTimeInfoEntryDetailId;
                timeEntry.entryType = TimeEntryType_Stop;
                timeEntry.label = "Regular Stop";
                timeEntry.start = segment.Start;
                timeEntry.end = segment.End;

                if (endDetail.driverLogTimeInfoEntryDetailType == DriverLogTimeInfoEntryDetailType_POI) {
                    timeEntry.entryType = TimeEntryType_POI;
                    timeEntry.label = "POI Stop";

                } else if (endDetail.driverLogTimeInfoEntryDetailType == DriverLogTimeInfoEntryDetailType_DC) {
                    timeEntry.entryType = TimeEntryType_Return;
                    timeEntry.label = "DC Stop";
                }

                if (timeEntry.end == null)
                    timeEntry.end = timeEntry.start;

                timeEntry.startText = DateTimeFormat.ShortFormatStringForDateTime(timeEntry.start);
                timeEntry.endText = DateTimeFormat.ShortFormatStringForDateTime(timeEntry.end);

                 arrSegments.add(timeEntry);
            }
        }

        // Get dispatch time if departure time is not detected
        // if (this.departure == nil)
        //     this.departure = this.dispatch;

        // replace departure or return label using existing start or end point
        if (this.interestStops != null) {
            if (this.interestStops.size() > 0) {
                arrPOILabels = this.interestStops;

                if (arrPOILabels.size() > 0) {
                    List<String> arrLabels = arrPlanLabels;

                    // Replace departure or return if start or end point exist
                    for (int x=0; x<2; x++) {
//                        @autoreleasepool {
                            String strLabel = x == 0 ? HOSStartPointText : HOSEndPointText;

                            if (!(arrPOILabels.contains(strLabel))) continue;

                        arrLabels.remove(x);
                        arrLabels.add(x, strLabel);
//                        }
                    }

                    arrPlanLabels = arrLabels;
                }
            }
        }

        // Create each dc entries (Pre-Trip, Departure/Start Point, Return/End Point, Post-Trip, End of Route)
        for (String strLabel : arrPlanLabels) {
//            @autoreleasepool {
                HOSTimeEntry timeEntry = new HOSTimeEntry();

                timeEntry.entryType = TimeEntryType_Unknown;
                timeEntry.deliveryType = DeliveryPOIType_Undefined;
                timeEntry.label = strLabel;
                timeEntry.annotation = "";

                // Add start detail for departure and return
                if (HOSDepartureText.contains(strLabel) &&  HOSReturnText.contains(strLabel)) {
                    if (this.sites.size() > 0) {
                        SdrGisSite sdrSite = this.sites.get(0);

                        if (strLabel.equalsIgnoreCase(HOSReturnText) && this.sites.size() > 1)
                        sdrSite = this.sites.get(1);

                        timeEntry.address = sdrSite.AddressComponents;
                        timeEntry.latLon = sdrSite.LatLon;
                    }
                }

                // Append departure entry
                if (strLabel.equalsIgnoreCase(HOSDepartureText)) {
                    timeEntry.entryType = TimeEntryType_Departure;
                    timeEntry.start = this.departure == null ? this.dispatch : this.departure;
                    timeEntry.end = timeEntry.start;

                    timeEntry.startText = DateTimeFormat.ShortFormatStringForDateTime(timeEntry.start);
                    timeEntry.endText = DateTimeFormat.ShortFormatStringForDateTime(timeEntry.start);

                    if (this.departure != null) {
                        timeEntry.originalStart = timeEntry.start;
                        timeEntry.originalEnd = timeEntry.start;
                    }

                    // Update pre-trip end time using departure time
                    this.preTrip.End = timeEntry.end;

                    // Append start and end point entry if exist
                } else if (HOSStartPointText.contains(strLabel) && HOSEndPointText.contains(strLabel)) {
                    if (arrPOILabels.size() == 0) continue;
                    if (!(arrPOILabels.contains(strLabel))) continue;

                    int index = arrPOILabels.indexOf(strLabel);

                    SdrPointOfInterest sdrPOI = this.interestStops.get(index);

                    if (sdrPOI != null) {
                        timeEntry.stopId = sdrPOI.SdrPointOfInterestId;
                        timeEntry.address = sdrPOI.AddressComponents;
                        timeEntry.latLon = sdrPOI.LatLon;

                        if (strLabel.equalsIgnoreCase(HOSEndPointText)) {
                            timeEntry.deliveryType = DeliveryPOIType_EndPoint;
                            timeEntry.entryType = TimeEntryType_EndPoint;
                            timeEntry.start = sdrPOI.ArrivalTime;

                            // Update post trip start time using end point arrival time
                            this.postTrip.Start = sdrPOI.ArrivalTime;

                        } else {
                            timeEntry.deliveryType = DeliveryPOIType_StartPoint;
                            timeEntry.entryType = TimeEntryType_StartPoint;
                            timeEntry.start = sdrPOI.DepartureTime;

                            // Update pre-trip end time using start point departure time
                            this.preTrip.End = sdrPOI.DepartureTime;
                        }

                        // Start/End point start and end time are always the equal
                        timeEntry.startText = DateTimeFormat.ShortFormatStringForDateTime(timeEntry.start);
                        timeEntry.endText = DateTimeFormat.ShortFormatStringForDateTime(timeEntry.start);
                        timeEntry.end = timeEntry.start;

                        timeEntry.originalStart = timeEntry.start;
                        timeEntry.originalEnd = timeEntry.start;
                    }

                    // Append return entry
                } else if (strLabel.equalsIgnoreCase(HOSReturnText)) {
                    timeEntry.startText = DateTimeFormat.ShortFormatStringForDateTime(this.arrival);
                    timeEntry.endText = DateTimeFormat.ShortFormatStringForDateTime(this.arrival);
                    timeEntry.entryType = TimeEntryType_Return;
                    timeEntry.start = this.arrival;
                    timeEntry.end = this.arrival;

                    timeEntry.originalStart = this.arrival;
                    timeEntry.originalEnd = this.arrival;

                    // Update post trip start time using return time
                    this.postTrip.Start = this.arrival;

                    // Append post trip entry
                } else if (strLabel.equalsIgnoreCase(HOSPostTripText)) {
                    timeEntry.startText = DateTimeFormat.ShortFormatStringForDateTime(this.postTrip.Start);
                    timeEntry.endText = DateTimeFormat.ShortFormatStringForDateTime(this.postTrip.End);
                    timeEntry.entryType = TimeEntryType_PostTrip;
                    timeEntry.start = this.postTrip.Start;
                    timeEntry.end = this.postTrip.End;

                    timeEntry.originalStart = this.postTrip.Start;
                    timeEntry.originalEnd = this.postTrip.End;

                    // Append end of route entry
                }  else if (strLabel.equalsIgnoreCase(HOSEndOfRouteText)) {
                    timeEntry.startText = DateTimeFormat.ShortFormatStringForDateTime(this.postTrip.End);
                    timeEntry.endText = DateTimeFormat.ShortFormatStringForDateTime(this.postTrip.End);
                    timeEntry.entryType = TimeEntryType_EndOfRoute;
                    timeEntry.start = this.postTrip.End;
                    timeEntry.end = this.postTrip.End;

                    timeEntry.originalStart = this.postTrip.End;
                    timeEntry.originalEnd = this.postTrip.End;

                    // Append pre trip entry
                } else if (strLabel.equalsIgnoreCase(HOSPreTripText)) {
                    timeEntry.entryType = TimeEntryType_PreTrip;
                    timeEntry.start = this.preTrip.Start;
                    timeEntry.end = this.preTrip.End;

                    if (timeEntry.start == null)
                        timeEntry.start = timeEntry.end;

                    timeEntry.endText = DateTimeFormat.ShortFormatStringForDateTime(timeEntry.end);
                    timeEntry.startText = DateTimeFormat.ShortFormatStringForDateTime(timeEntry.start);
                    timeEntry.originalStart = timeEntry.start;
                    timeEntry.originalEnd = timeEntry.end;

                    arrDCEntries.add(0, timeEntry);
                    break;
                }

                arrDCEntries.add(timeEntry);
            }

        // Merge point of interest and regular stops
        arrRouteEntries = arrDCEntries;

        if (arrPOIs.size() > 0)
//            arrStops = arrStops.size() == 0 ? arrPOIs : arrStops.add(arrPOIs.clone());

        if (arrStops.size() > 0) {
            int count = arrStops.size();

            // Making sure that the stops are sorted base on arrival time
//            arrStops = arrStops.sort(arrStops);

            // Append all route entries after the start event with index == 1
            for (int index=0; index<count; index++) {
//                arrRouteEntries.add(index + 2, arrStops.get(index));
            }
        }

        }


    public ArrayList[] arrayByAddingObjectsFromArray(ArrayList a, ArrayList b) {
        int aLen = a.size();
        int bLen = b.size();
        ArrayList[] c= new ArrayList[aLen+bLen];
        System.arraycopy(a, 0, c, 0, aLen);
        System.arraycopy(b, 0, c, aLen, bLen);
        return c;
    }





    public ArrayList<HOSTimeLogEntry> buildStopLogEntries(ArrayList<SdrStop> arrStops) {

        ArrayList<HOSTimeLogEntry> arrStopsEntries = new ArrayList<HOSTimeLogEntry>();

        for (SdrStop sdrStop : arrStops) {
//            @autoreleasepool {
                HOSTimeLogEntry timeLogEntry =  new HOSTimeLogEntry();
                DriverLogTimeInfoEntryDetail infoDetail = new DriverLogTimeInfoEntryDetail();

                infoDetail.LatLon = sdrStop.LatLon;
                infoDetail.AddressComponents = sdrStop.AddressComponents;

                if (sdrStop.AddressComponents != null) {
                    StringBuilder annotation = new StringBuilder();

                    if (sdrStop.AddressComponents.City != null) {
                        annotation.append(sdrStop.AddressComponents.City);
                        annotation.append(" ");
                    }

                    if (sdrStop.AddressComponents.Province != null) {
                        annotation.append(sdrStop.AddressComponents.Province);
                    }

                    timeLogEntry.Annotation = annotation.toString();
                }

                timeLogEntry.StopId = sdrStop.SdrStopId;
                timeLogEntry.timeLogEntryType = TimeLogEntryType_Stop;
                timeLogEntry.DeliveryType = DeliveryPOIType_DeliveryStop;
                timeLogEntry.StopName = sdrStop.SiteName;
                timeLogEntry.Duration = sdrStop.DurationText;
                timeLogEntry.Label = "Stop " +  sdrStop.StopText;
                timeLogEntry.DeliveryStart = sdrStop.DeliveryStartTime;
                timeLogEntry.DeliveryEnd = sdrStop.DeliveryEndTime;
                timeLogEntry.StartDetail = infoDetail;

                // Use expected delivery time to enable to display the terminated route with stops
                timeLogEntry.Start = sdrStop.ArrivalTime == null ? sdrStop.ExpectedDeliveryTime : sdrStop.ArrivalTime;
                timeLogEntry.End = sdrStop.DepartureTime == null ? sdrStop.ExpectedDeliveryTime : sdrStop.DepartureTime;
                timeLogEntry.StartText = DateTimeFormat.ShortFormatStringForDateTime(timeLogEntry.Start);
                timeLogEntry.EndText = DateTimeFormat.ShortFormatStringForDateTime(timeLogEntry.End);

                timeLogEntry.OriginalStart = sdrStop.ArrivalTime;
                timeLogEntry.OriginalEnd = sdrStop.DepartureTime;

                // Make sure to handle duplicate stop entries
                if (!(arrStopsEntries.contains(timeLogEntry))) {
                    arrStopsEntries.add(this.getRestBreakInsideStop(timeLogEntry));
                }
//            }



        }

        return arrStopsEntries;
    }

    public ArrayList<HOSTimeLogEntry> buildPOILogEntries(ArrayList<SdrPointOfInterest> arrPOIs) {

        ArrayList<HOSTimeLogEntry> arrPOIEntries = new ArrayList<HOSTimeLogEntry>();

        for (SdrPointOfInterest sdrPOI : arrPOIs) {
//            @autoreleasepool {
                // Skip adding POI start and end point and it will be use to replace departure or return entries
                if (sdrPOI.Type == DeliveryPOIType_StartPoint || sdrPOI.Type == DeliveryPOIType_EndPoint) {
                    // Get start and end point interest
                    HOSRouteTimeLog hosRoute = new HOSRouteTimeLog();

                    if (sdrPOI.Type == DeliveryPOIType_StartPoint) {
                        hosRoute.startPoint = sdrPOI;
                        continue;
                    }

                    hosRoute.endPoint = sdrPOI;
                    continue;
                }

                HOSTimeLogEntry timeLogEntry = new HOSTimeLogEntry();
                DriverLogTimeInfoEntryDetail infoDetail = new DriverLogTimeInfoEntryDetail();

                infoDetail.AddressComponents = sdrPOI.AddressComponents;
                infoDetail.LatLon = sdrPOI.LatLon;

                if (infoDetail.AddressComponents != null) {
                    StringBuilder annotation = new StringBuilder();

                    if (infoDetail.AddressComponents.City != null) {
                        annotation.append(infoDetail.AddressComponents.City);
                        annotation.append(" ");
                    }

                    if (infoDetail.AddressComponents.Province != null) {
                        annotation.append(infoDetail.AddressComponents.Province);
                    }

                    timeLogEntry.Annotation = annotation.toString();
                }

                timeLogEntry.StartText = DateTimeFormat.ShortFormatStringForDateTime(sdrPOI.ArrivalTime);
                timeLogEntry.Start = sdrPOI.ArrivalTime;
                timeLogEntry.EndText = DateTimeFormat.ShortFormatStringForDateTime(sdrPOI.DepartureTime);
                timeLogEntry.End = sdrPOI.DepartureTime;
                timeLogEntry.OriginalStart = timeLogEntry.Start;
                timeLogEntry.OriginalEnd = timeLogEntry.End;
                timeLogEntry.timeLogEntryType = TimeLogEntryType_POI;
                timeLogEntry.DeliveryType = sdrPOI.Type;
                timeLogEntry.StopId = sdrPOI.SdrPointOfInterestId;
                timeLogEntry.Duration = sdrPOI.DurationText;
                timeLogEntry.StartDetail = infoDetail;
                timeLogEntry.Label = sdrPOI.Label;

                arrPOIEntries.add(timeLogEntry);
           // }
        }

        return arrPOIEntries;


    }
    private HOSTimeLogEntry getRestBreakInsideStop(HOSTimeLogEntry timeLogEntry) {
    return null;

    }
}
