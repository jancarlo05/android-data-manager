package com.procuro.apimmdatamanagerlib.HOSDataModel.HOSEditor;

import com.procuro.apimmdatamanagerlib.HOSDataModel.HOSDataManager;
import com.procuro.apimmdatamanagerlib.HOSDataModel.HOSTimeLogEntry;

import java.util.Date;

/**
 * Created by jophenmarieweeks on 15/12/2017.
 */

public class HOSAlteredTimeEntry extends HOSDataManager {


    public enum HOSAlteredEntryType {

        HOSAlteredType_Unknown(0),
        HOSAlteredType_AddPunchInTime(1),
        HOSAlteredType_AddPunchOutTime(2),
        HOSAlteredType_UpdatePunchInTime(3),
        HOSAlteredType_UpdatePunchOutTime(4),
        HOSAlteredType_UpdatePreTripTime(5),
        HOSAlteredType_UpdatePostTripTime(6),
        HOSAlteredType_ManualStartEventTime(7),
        HOSAlteredType_ManualEndEventTime(8),
        HOSAlteredType_ManualStopArrivalDepartureTime(9),
        HOSAlteredType_ManualPOIArrivalDepartureTime(10),
        HOSAlteredType_UpdateDepartureInfo(11),
        HOSAlteredType_UpdateReturnInfo(12),
        HOSAlteredType_RemoveStopEntry(13),
        HOSAlteredType_RemovePOIEntry(14),
        HOSAlteredType_UpdatePOIInfo(15),
        HOSAlteredType_UpdateLastStatus(16);

        private int value;

        private HOSAlteredEntryType( int value){
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }

        public static HOSAlteredEntryType setIntValue (int i) {
            for (HOSAlteredEntryType type : HOSAlteredEntryType.values()) {
                if (type.value == i) { return type; }
            }
            return HOSAlteredType_Unknown;
        }

    }



//    @property (nonatomic, strong) id object;

    public HOSAlteredEntryType alteredType;
    public HOSTimeLogEntry.TimeLogEntryType entryType;
    public String auditBy;
    public Date auditAt;
    @Override
    public void setValueForKey(Object value, String key) {

        if(key.equalsIgnoreCase("entryType")) {
            if (!value.equals(null)) {
                HOSTimeLogEntry.TimeLogEntryType result = HOSTimeLogEntry.TimeLogEntryType.setIntValue((int) value);
                this.entryType = result;
            }
        } else if(key.equalsIgnoreCase("alteredType")) {
            if (!value.equals(null)) {
                HOSAlteredEntryType result = HOSAlteredEntryType.setIntValue((int) value);
                this.alteredType = result;
            }
        } else {
            super.setValueForKey(value, key);
        }
    }

}
