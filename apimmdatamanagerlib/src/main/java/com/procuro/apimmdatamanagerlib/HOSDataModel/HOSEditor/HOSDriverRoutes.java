package com.procuro.apimmdatamanagerlib.HOSDataModel.HOSEditor;

import com.procuro.apimmdatamanagerlib.DateTimeFormat;
import com.procuro.apimmdatamanagerlib.DriverData;
import com.procuro.apimmdatamanagerlib.DriverLogTimeInfoDetails;
import com.procuro.apimmdatamanagerlib.DriverTimeLog;
import com.procuro.apimmdatamanagerlib.HOSDataModel.HOSDataManager;
import com.procuro.apimmdatamanagerlib.PimmForm;
import com.procuro.apimmdatamanagerlib.SdrDelivery;
import com.procuro.apimmdatamanagerlib.SdrDriver;
import com.procuro.apimmdatamanagerlib.SdrReport;
import com.procuro.apimmdatamanagerlib.iPimmDriverLogManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.procuro.apimmdatamanagerlib.DriverLogTimeInfoDetails.DriverLastStatusEnum.DriverLastStatusEnum_GoHome;
import static com.procuro.apimmdatamanagerlib.DriverLogTimeInfoDetails.DriverLastStatusEnum.DriverLastStatusEnum_GoOnBreak;
import static com.procuro.apimmdatamanagerlib.DriverLogTimeInfoDetails.DriverLastStatusEnum.DriverLastStatusEnum_Sleeper;
import static com.procuro.apimmdatamanagerlib.DriverLogTimeInfoDetails.DriverLastStatusEnum.DriverLastStatusEnum_StayOnDuty;
import static com.procuro.apimmdatamanagerlib.DriverLogTimeInfoDetails.DriverLastStatusEnum.DriverLastStatusEnum_Undefined;

/**
 * Created by jophenmarieweeks on 15/12/2017.
 */

public class HOSDriverRoutes extends HOSDataManager {

    public DriverLogTimeInfoDetails.DriverLastStatusEnum prev, nextStatus;

    public DriverData first, last;
    public SdrDriver driver;
    public HOSRouteSummary routeSummary;

    public String referenceId;
    public ArrayList<HOSRouteSummary> routes;


    @Override
    public void setValueForKey(Object value, String key) {

        if (key.equalsIgnoreCase("routeSummary")) {
            if (!value.equals(null)) {
                HOSRouteSummary result = new HOSRouteSummary();
                JSONObject jsonObject = (JSONObject) value;
                result.readFromJSONObject(jsonObject);
                this.routeSummary = result;
            }
        }else if (key.equalsIgnoreCase("first")) {
            if (!value.equals(null)) {
                DriverData result = new DriverData();
                JSONObject jsonObject = (JSONObject) value;
                result.readFromJSONObject(jsonObject);
                this.first = result;
            }
        }else if (key.equalsIgnoreCase("last")) {
            if (!value.equals(null)) {
                DriverData result = new DriverData();
                JSONObject jsonObject = (JSONObject) value;
                result.readFromJSONObject(jsonObject);
                this.last = result;
            }
        } else if (key.equalsIgnoreCase("prev")) {

            if (!value.equals(null)) {
                DriverLogTimeInfoDetails.DriverLastStatusEnum driverLastStatusEnum = DriverLogTimeInfoDetails.DriverLastStatusEnum.setIntValue((int) value);
                this.prev = driverLastStatusEnum;
            }
        } else if (key.equalsIgnoreCase("nextStatus")) {

            if (!value.equals(null)) {
                DriverLogTimeInfoDetails.DriverLastStatusEnum driverLastStatusEnum = DriverLogTimeInfoDetails.DriverLastStatusEnum.setIntValue((int) value);
                this.nextStatus = driverLastStatusEnum;
            }
        }else if (key.equalsIgnoreCase("routes")) {
            if (!value.equals(null)) {
                if (this.routes == null) {
                    this.routes = new ArrayList<HOSRouteSummary>();
                }

                if (!value.equals(null)) {
                    JSONArray arrResults = (JSONArray) value;

                    if (arrResults != null) {
                        for (int i = 0; i < arrResults.length(); i++) {
                            try {
                                HOSRouteSummary result = new HOSRouteSummary();
                                result.readFromJSONObject(arrResults.getJSONObject(i));

                                this.routes.add(result);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                }
            }
        } else {
            super.setValueForKey(value, key);
        }

    }


    public void initWithDriverData(HOSDriverData driverData){

        this.routes = new ArrayList<HOSRouteSummary>();
        this.referenceId = driverData.referenceId;
        this.driver = driverData.driver;

        buildRoutesSummaryWithDriverData(driverData.data);
    }

    public void buildRoutesSummaryWithDriverData(DriverData driverData) {
        ArrayList<HOSRouteSummary> arrRoutes = new ArrayList<HOSRouteSummary>();
        ArrayList<SdrReport> arrShipments = driverData.shipments;
        ArrayList<PimmForm> arrForms = driverData.forms;
        ArrayList arrRefxs = null;
        String strDriverRouteLogs = "";

        if(arrForms.size() >0)
            arrRefxs = arrForms;


        for (SdrReport sdrReport : arrShipments) {
//            @autoreleasepool {
                HOSRouteSummary lastRouteSummary = null;
                SdrDelivery sdrDelivery = sdrReport.Hierarchy;
                PimmForm pimmForm = null;

                strDriverRouteLogs = strDriverRouteLogs + " |- " + sdrDelivery.RouteName + " - " + sdrDelivery.RouteId + "(" + sdrDelivery.Status+ ")";
//            [strDriverRouteLogs stringByAppendingFormat:@"\n   |- %@ - %@ (%d)", sdrDelivery.RouteName, sdrDelivery.RouteId,sdrDelivery.Status];

                if (arrRefxs.size() > 0) {
                    if (arrRefxs.contains(sdrDelivery.DeliveryId))
                    pimmForm = arrForms.get(arrRefxs.indexOf(sdrDelivery.DeliveryId));
//                    [arrForms objectAtIndex:[arrRefxs indexOfObject:sdrDelivery.DeliveryId]];


                    if (pimmForm != null) {
                        iPimmDriverLogManager driverManager = iPimmDriverLogManager.getDriverLogManager();
                        DriverTimeLog timeLog = driverManager.getDriverTimeLogFromJSONData(pimmForm.bodyData);
                        strDriverRouteLogs = strDriverRouteLogs + " - " + DateTimeFormat.ShortFormatStringForDateTime(timeLog.TimeInfoDetails.Punch.Start);

                    }
                }

//                HOSRouteSummary routeSummary =  HOSRouteSummary.initWithSdrReport(sdrReport, pimmForm);

            iPimmDriverLogManager driverManager = iPimmDriverLogManager.getDriverLogManager();
            DriverTimeLog timeLog = driverManager.getDriverTimeLogFromJSONData(pimmForm.bodyData);

            strDriverRouteLogs = strDriverRouteLogs + " - " + DateTimeFormat.ShortFormatStringForDateTime(routeSummary.lastEntry.end);

            if (routeSummary.deliveryId == null) continue;

                if (arrRoutes.size() > 0) {
                    lastRouteSummary = arrRoutes.get(arrRoutes.size()-1);

                    routeSummary.firstStatus = lastRouteSummary.lastStatus;
                    routeSummary.firstEntry = lastRouteSummary.lastEntry;
                }

            routeSummary.buildRouteEntriesForDriverId(this.driver.UserId, timeLog);

                if (lastRouteSummary == null) {
                    HOSTimeEntry prevLastEntry = lastRouteSummary.entries.get(lastRouteSummary.entries.size()-1);
                    DriverLogTimeInfoDetails.DriverLastStatusEnum prevLastStatus = lastRouteSummary.lastStatus;

                    // Get info of next route delivery
                    lastRouteSummary.routeNext = routeSummary.routeName;
                    lastRouteSummary.deliveryNextId = routeSummary.deliveryId;
                    lastRouteSummary.formNextId = routeSummary.formId;

                    // strDriverRouteLogs = [strDriverRouteLogs stringByAppendingFormat:@" - %@ (%d)", lastRouteSummary.routeName, prevLastStatus];

                    if (prevLastStatus == DriverLastStatusEnum_GoHome) {
                        // If the previous route is already punch-out, update the pre-trip start time using punch-in time
                        if (routeSummary.punchStart != null) {
                            HOSTimeEntry preTripEntry = routeSummary.entries.get(0);

                            preTripEntry.startText = DateTimeFormat.ShortFormatStringForDateTime(routeSummary.punchStart);
                            preTripEntry.start = routeSummary.punchStart;

                            routeSummary.preTrip.Start = routeSummary.punchStart;
                        }

                    } else {

                        // Update punch start using the previous route punch start
                        if (lastRouteSummary.punchStart != null) {
                            if (lastRouteSummary.lastStatus != DriverLastStatusEnum_Undefined) {
                                routeSummary.punchStart = lastRouteSummary.punchStart;
                            }
                        }

                        if (prevLastStatus == DriverLastStatusEnum_StayOnDuty) {
                            HOSTimeEntry preTripEntry = routeSummary.entries.get(routeSummary.entries.size() - 1);

                            preTripEntry.startText = DateTimeFormat.ShortFormatStringForDateTime(prevLastEntry.end);
                            preTripEntry.start = prevLastEntry.end;

                        } else if (prevLastStatus == DriverLastStatusEnum_GoOnBreak || prevLastStatus == DriverLastStatusEnum_Sleeper) {
                            HOSTimeEntry preTripEntry = routeSummary.entries.get(0);

                            lastRouteSummary.lastEntry.endText = DateTimeFormat.ShortFormatStringForDateTime(preTripEntry.start);
                            lastRouteSummary.lastEntry.end = preTripEntry.start;
                        }
                    }

                    if (routeSummary.lastStatus == DriverLastStatusEnum_GoHome) {
                        if (lastRouteSummary.lastStatus != DriverLastStatusEnum_GoHome) {
                            // strDriverRouteLogs = [strDriverRouteLogs stringByAppendingFormat:@" - %@", routeSummary.punchEnd];
                            // Display current punch end time to the previous work day
                            lastRouteSummary.punchEnd = routeSummary.punchEnd;
                        }
                    }

                    // Update original start and end time
                    lastRouteSummary.lastEntry.originalStart = lastRouteSummary.lastEntry.start;
                    lastRouteSummary.lastEntry.originalEnd = lastRouteSummary.lastEntry.end;

                    // strDriverRouteLogs = [strDriverRouteLogs stringByAppendingFormat:@" - %@", [NSDate ShortFormatStringForDateTime:lastRouteSummary.punchStart]];
                }

                // Update original start and end time
                routeSummary.lastEntry.originalStart = routeSummary.lastEntry.start;
                routeSummary.lastEntry.originalEnd = routeSummary.lastEntry.end;

            arrRoutes.add(routeSummary);
//            }
        }








        }


}
