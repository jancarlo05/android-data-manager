package com.procuro.apimmdatamanagerlib.HOSDataModel.HOSEditor;

import com.procuro.apimmdatamanagerlib.DriverLogTimeInfoDetails;
import com.procuro.apimmdatamanagerlib.DriverLogTimeInfoEntry;
import com.procuro.apimmdatamanagerlib.DriverTimeLog;
import com.procuro.apimmdatamanagerlib.HOSDataModel.HOSDataManager;
import com.procuro.apimmdatamanagerlib.PimmForm;
import com.procuro.apimmdatamanagerlib.SdrDelivery;
import com.procuro.apimmdatamanagerlib.SdrDriver;
import com.procuro.apimmdatamanagerlib.SdrReport;
import com.procuro.apimmdatamanagerlib.StoreDeliveryEnums;
import com.procuro.apimmdatamanagerlib.iPimmDriverLogManager;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.procuro.apimmdatamanagerlib.StoreDeliveryEnums.DeliveryStatus.DeliveryStatus_Complete;
import static com.procuro.apimmdatamanagerlib.StoreDeliveryEnums.DeliveryStatus.DeliveryStatus_CompleteWithViolation;
import static com.procuro.apimmdatamanagerlib.StoreDeliveryEnums.DeliveryStatus.DeliveryStatus_UserTerminated;

/**
 * Created by jophenmarieweeks on 18/12/2017.
 */

public class HOSRouteSummary extends HOSDataManager {


    public DriverLogTimeInfoDetails.DriverLastStatusEnum firstStatus, lastStatus;

    public SdrDriver driver, coDriver;
    public DriverLogTimeInfoEntry punch, preTrip, postTrip;
    public HOSTimeEntry firstEntry, lastEntry;
    public PimmForm form;

    public String deliveryId, deliveryNextId, formId, formNextId, routeId, routeName;
    public String trailerId, trailerName, tractorId, tractorName, routeNext, auditBy;
    public String dcId, dcName, dcAddress, startPOIId, endPOIId;
    public Date punchStart, punchEnd, dispatch, departure, startTime, arrival, auditAt;
    public ArrayList sites, interestStops, regularStops, restStops, sleeperStops;
    public ArrayList<HOSTimeEntry> entries;
    public ArrayList missedStops, driving;
    public List alteredEntries;




    public HOSRouteSummary initWithSdrReport(SdrReport sdrReport, PimmForm pimmForm) {



        SdrDelivery sdrDelivery = sdrReport.Hierarchy;
        StoreDeliveryEnums.DeliveryStatus deliveryStatus = sdrDelivery.Status;

        // Route must be complete or terminated
        if (deliveryStatus == DeliveryStatus_Complete ||
                deliveryStatus == DeliveryStatus_CompleteWithViolation ||
                deliveryStatus == DeliveryStatus_UserTerminated) {

            this.deliveryId = sdrDelivery.DeliveryId;
            this.routeId = sdrDelivery.RouteId;
            this.routeName = sdrDelivery.RouteName;
            this.trailerId = sdrDelivery.TrailerId;
            ;
            this.trailerName = sdrDelivery.TrailerName;
            this.tractorId = sdrDelivery.TractorId;
            this.tractorName = sdrDelivery.Tractor;
            this.dcId = sdrDelivery.DCId;
            this.dcName = sdrDelivery.DCName;
            this.dcAddress = sdrDelivery.DCAddress;

            this.driver = sdrDelivery.Driver;
            this.coDriver = sdrDelivery.CoDriver;

            this.dispatch = sdrDelivery.DispatchTime;
            this.departure = sdrDelivery.DepartureTime;
            this.arrival = sdrDelivery.ArrivalTime;
            this.startTime = sdrDelivery.StartTime;

            this.interestStops = sdrDelivery.GIS.PointsOfInterest;
            this.regularStops = sdrDelivery.GIS.Stops;
            this.sites = sdrDelivery.GIS.Sites;
            this.form = form;

            if (this.form != null) {
                iPimmDriverLogManager driverManager = iPimmDriverLogManager.getDriverLogManager();
                DriverTimeLog timeLog = driverManager.getDriverTimeLogFromJSONData(this.form.bodyData);
                this.formId = timeLog.FormID;
            }




        }
        return this;
    }

    public void buildRouteEntriesForDriverId(String userId, DriverTimeLog timeLog) {


//            // Update driver time log version
//            // self.driverTimeLog.Version = [NSNumber numberWithDouble:1.1];
//            DriverLogTimeInfoDetails *infoDetails = timeLog.TimeInfoDetails;
//
//            NSMutableArray *arrRestStops = [infoDetails.RestStops mutableCopy];
//
//            if (![timeLog.DriverID isEqual:strDriverId])
//            infoDetails = timeLog.CoDriverTimeInfoDetails;
//
//            // Get the first rest break related to the previous route last status: "Go on Break"
//            if (arrRestStops.count > 0) {
//                DriverLogRestBreakInfoEntry *restBreak = arrRestStops.firstObject;
//
//                if ([self.preTrip.Start isEqualToDate:restBreak.End]) {
//                    self.firstEntry = [[HOSTimeEntry alloc] init];
//
//                    self.firstEntry.entryType = TimeEntryType_RestBreak;
//                    self.firstEntry.startText = [NSDate ShortFormatStringForDateTime:restBreak.Start];
//                    self.firstEntry.endText = [NSDate ShortFormatStringForDateTime:restBreak.End];
//                    self.firstEntry.start = restBreak.Start;
//                    self.firstEntry.end = restBreak.End;
//
//                    self.firstEntry.address = restBreak.StartDetail.AddressComponents;
//                    self.firstEntry.latLon = restBreak.StartDetail.LatLon;
//
//            [arrRestStops removeObjectAtIndex:0];
//                }
//
//                self.restStops = arrRestStops;
//            }
//
//            NSMutableArray *arrRouteEntries = [[NSMutableArray alloc] init];
//            NSMutableArray *arrDCEntries = [[NSMutableArray alloc] init];
//            NSMutableArray *arrSegments = [[NSMutableArray alloc] init];
//
//            // Get stops and point of interests
//            NSArray *arrStops = [self buildStopLogEntries:self.regularStops];
//            NSArray *arrPOIs = [self buildPOILogEntries:self.interestStops];
//
//            NSArray *arrPlanLabels = @[HOSDepartureText, HOSReturnText, HOSPostTripText, HOSEndOfRouteText, HOSPreTripText];
//            NSArray *arrPOILabels = nil;
//
//            self.punch = infoDetails.Punch;
//            self.preTrip = infoDetails.PreTrip;
//            self.postTrip = infoDetails.PostTrip;
//
//            self.punchStart = self.punch.Start;
//            self.punchEnd = self.punch.End;
//            self.lastStatus = infoDetails.LastStatus;
//
//            if (self.coDriver != nil) {
//                for (DriverLogTimeInfoEntry *segment in infoDetails.DriveSegments) {
//                    DriverLogTimeInfoEntryDetail *endDetail = segment.EndDetail;
//                    HOSTimeEntry *timeEntry = [[HOSTimeEntry alloc] init];
//
//            /*
//             endDetail.DriverLogTimeInfoEntryDetailType
//
//             DriverLogTimeInfoEntryDetailType_DC = 0,
//             DriverLogTimeInfoEntryDetailType_Stop = 1,
//             DriverLogTimeInfoEntryDetailType_POI = 2
//             */
//
//                    timeEntry.stopId = endDetail.DriverLogTimeInfoEntryDetailId;
//                    timeEntry.entryType = TimeEntryType_Stop;
//                    timeEntry.label = @"Regular Stop";
//                    timeEntry.start = segment.Start;
//                    timeEntry.end = segment.End;
//
//                    if (endDetail.DriverLogTimeInfoEntryDetailType == DriverLogTimeInfoEntryDetailType_POI) {
//                        timeEntry.entryType = TimeEntryType_POI;
//                        timeEntry.label = @"POI Stop";
//
//                    } else if (endDetail.DriverLogTimeInfoEntryDetailType == DriverLogTimeInfoEntryDetailType_DC) {
//                        timeEntry.entryType = TimeEntryType_Return;
//                        timeEntry.label = @"DC Stop";
//                    }
//
//                    if (timeEntry.end == nil)
//                        timeEntry.end = timeEntry.start;
//
//                    timeEntry.startText = [NSDate ShortFormatStringForDateTime:timeEntry.start];
//                    timeEntry.endText = [NSDate ShortFormatStringForDateTime:timeEntry.end];
//
//            [arrSegments addObject:timeEntry];
//                }
//            }
//
//            DDLogVerbose(@"Driver segments: %@ - %@", self.routeName, infoDetails.DriveSegments);
//
//            // Get dispatch time if departure time is not detected
//            // if (self.departure == nil)
//            //     self.departure = self.dispatch;
//
//            // replace departure or return label using existing start or end point
//            if (self.interestStops) {
//                if (self.interestStops.count > 0) {
//                    arrPOILabels = [self.interestStops valueForKey:@"Label"];
//
//                    if (arrPOILabels.count > 0) {
//                        NSMutableArray *arrLabels = [arrPlanLabels mutableCopy];
//
//                        // Replace departure or return if start or end point exist
//                        for (NSUInteger x=0; x<2; x++) {
//                            @autoreleasepool {
//                                NSString *strLabel = x == 0 ? HOSStartPointText : HOSEndPointText;
//
//                                if (![arrPOILabels containsObject:strLabel]) continue;
//
//                        [arrLabels removeObjectAtIndex:x];
//                        [arrLabels insertObject:strLabel atIndex:x];
//                            }
//                        }
//
//                        arrPlanLabels = arrLabels;
//                    }
//                }
//            }
//
//            // Create each dc entries (Pre-Trip, Departure/Start Point, Return/End Point, Post-Trip, End of Route)
//            for (NSString *strLabel in arrPlanLabels) {
//            @autoreleasepool {
//                HOSTimeEntry *timeEntry = [[HOSTimeEntry alloc] init];
//
//                timeEntry.entryType = TimeEntryType_Unknown;
//                timeEntry.deliveryType = DeliveryPOIType_Undefined;
//                timeEntry.Label = strLabel;
//                timeEntry.Annotation = @"";
//
//                // Add start detail for departure and return
//                if ([@[HOSDepartureText, HOSReturnText] containsObject:strLabel]) {
//                    if (self.sites.count > 0) {
//                        SdrGisSite *sdrSite = [self.sites objectAtIndex:0];
//
//                        if ([strLabel isEqualToString:HOSReturnText] && self.sites.count > 1)
//                        sdrSite = [self.sites objectAtIndex:1];
//
//                        timeEntry.address = sdrSite.AddressComponents;
//                        timeEntry.latLon = sdrSite.LatLon;
//                    }
//                }
//
//                // Append departure entry
//                if ([strLabel isEqualToString:HOSDepartureText]) {
//                    timeEntry.entryType = TimeEntryType_Departure;
//                    timeEntry.start = self.departure == nil ? self.dispatch : self.departure;
//                    timeEntry.end = timeEntry.start;
//
//                    timeEntry.startText = [NSDate ShortFormatStringForDateTime:timeEntry.start];
//                    timeEntry.endText = [NSDate ShortFormatStringForDateTime:timeEntry.start];
//
//                    if (self.departure != nil) {
//                        timeEntry.originalStart = timeEntry.start;
//                        timeEntry.originalEnd = timeEntry.start;
//                    }
//
//                    // Update pre-trip end time using departure time
//                    self.preTrip.End = timeEntry.end;
//
//                    // Append start and end point entry if exist
//                } else if ([@[HOSStartPointText, HOSEndPointText] containsObject:strLabel]) {
//                    if (arrPOILabels.count == 0) continue;
//                    if (![arrPOILabels containsObject:strLabel]) continue;
//
//                    NSUInteger index = [arrPOILabels indexOfObject:strLabel];
//
//                    SdrPointOfInterest *sdrPOI = [self.interestStops objectAtIndex:index];
//
//                    if (sdrPOI) {
//                        timeEntry.stopId = sdrPOI.SdrPointOfInterestId;
//                        timeEntry.address = sdrPOI.AddressComponents;
//                        timeEntry.latLon = sdrPOI.LatLon;
//
//                        if ([strLabel isEqualToString:HOSEndPointText]) {
//                            timeEntry.deliveryType = DeliveryPOIType_EndPoint;
//                            timeEntry.entryType = TimeEntryType_EndPoint;
//                            timeEntry.start = sdrPOI.ArrivalTime;
//
//                            // Update post trip start time using end point arrival time
//                            self.postTrip.Start = sdrPOI.ArrivalTime;
//
//                        } else {
//                            timeEntry.deliveryType = DeliveryPOIType_StartPoint;
//                            timeEntry.entryType = TimeEntryType_StartPoint;
//                            timeEntry.start = sdrPOI.DepartureTime;
//
//                            // Update pre-trip end time using start point departure time
//                            self.preTrip.End = sdrPOI.DepartureTime;
//                        }
//
//                        // Start/End point start and end time are always the equal
//                        timeEntry.startText = [NSDate ShortFormatStringForDateTime:timeEntry.start];
//                        timeEntry.endText = [NSDate ShortFormatStringForDateTime:timeEntry.start];
//                        timeEntry.end = timeEntry.start;
//
//                        timeEntry.originalStart = timeEntry.start;
//                        timeEntry.originalEnd = timeEntry.start;
//                    }
//
//                    // Append return entry
//                } else if ([strLabel isEqualToString:HOSReturnText]) {
//                    timeEntry.StartText = [NSDate ShortFormatStringForDateTime:self.arrival];
//                    timeEntry.EndText = [NSDate ShortFormatStringForDateTime:self.arrival];
//                    timeEntry.entryType = TimeEntryType_Return;
//                    timeEntry.start = self.arrival;
//                    timeEntry.end = self.arrival;
//
//                    timeEntry.originalStart = self.arrival;
//                    timeEntry.originalEnd = self.arrival;
//
//                    // Update post trip start time using return time
//                    self.postTrip.Start = self.arrival;
//
//                    // Append post trip entry
//                } else if ([strLabel isEqualToString:HOSPostTripText]) {
//                    timeEntry.startText = [NSDate ShortFormatStringForDateTime:self.postTrip.Start];
//                    timeEntry.endText = [NSDate ShortFormatStringForDateTime:self.postTrip.End];
//                    timeEntry.entryType = TimeEntryType_PostTrip;
//                    timeEntry.start = self.postTrip.Start;
//                    timeEntry.end = self.postTrip.End;
//
//                    timeEntry.originalStart = self.postTrip.Start;
//                    timeEntry.originalEnd = self.postTrip.End;
//
//                    // Append end of route entry
//                }  else if ([strLabel isEqualToString:HOSEndOfRouteText]) {
//                    timeEntry.startText = [NSDate ShortFormatStringForDateTime:self.postTrip.End];
//                    timeEntry.endText = [NSDate ShortFormatStringForDateTime:self.postTrip.End];
//                    timeEntry.entryType = TimeEntryType_EndOfRoute;
//                    timeEntry.start = self.postTrip.End;
//                    timeEntry.end = self.postTrip.End;
//
//                    timeEntry.originalStart = self.postTrip.End;
//                    timeEntry.originalEnd = self.postTrip.End;
//
//                    // Append pre trip entry
//                } else if ([strLabel isEqualToString:HOSPreTripText]) {
//                    timeEntry.entryType = TimeEntryType_PreTrip;
//                    timeEntry.start = self.preTrip.Start;
//                    timeEntry.end = self.preTrip.End;
//
//                    if (timeEntry.start == nil)
//                        timeEntry.start = timeEntry.end;
//
//                    timeEntry.endText = [NSDate ShortFormatStringForDateTime:timeEntry.end];
//                    timeEntry.startText = [NSDate ShortFormatStringForDateTime:timeEntry.start];
//                    timeEntry.originalStart = timeEntry.start;
//                    timeEntry.originalEnd = timeEntry.end;
//
//                [arrDCEntries insertObject:timeEntry atIndex:0];
//                    break;
//                }
//
//            [arrDCEntries addObject:timeEntry];
//            }
//        }
//
//            // Merge point of interest and regular stops
//            arrRouteEntries = arrDCEntries;
//
//            if (arrPOIs.count > 0)
//                arrStops = arrStops.count == 0 ? arrPOIs : [arrStops arrayByAddingObjectsFromArray:arrPOIs];
//
//            if (arrStops.count > 0) {
//                NSUInteger count = arrStops.count;
//
//                // Making sure that the stops are sorted base on arrival time
//                arrStops = [arrStops sortBy:@"start"];
//
//                // Append all route entries after the start event with index == 1
//                for (NSUInteger index=0; index<count; index++) {
//            [arrRouteEntries insertObject:[arrStops objectAtIndex:index] atIndex:(index+2)];
//                }
//            }
//
//            NSString *strSegmentLogs = @"";
//
//            for (HOSTimeEntry *timeEntry in arrSegments) {
//            strSegmentLogs = [strSegmentLogs stringByAppendingFormat:@"\n |- %@ - %@", timeEntry.stopId, timeEntry.label];
//        }
//
//            // Insert driving entries
//            self.entries = [arrRouteEntries insertDrivingEntriesWithSegments:arrSegments];
//            self.driving = arrSegments;
//
//            // Create last status entry
//    [self buildLastEntryWithEndOfRoute:arrRouteEntries.lastObject];
//        }
    }
}
