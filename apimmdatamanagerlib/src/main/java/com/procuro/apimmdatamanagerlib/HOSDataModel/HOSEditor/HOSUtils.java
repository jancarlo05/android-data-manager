package com.procuro.apimmdatamanagerlib.HOSDataModel.HOSEditor;

import com.procuro.apimmdatamanagerlib.DateTimeFormat;
import com.procuro.apimmdatamanagerlib.HOSDataModel.HOSTimeLogEntry;
import com.procuro.apimmdatamanagerlib.PimmBaseObject;

import java.util.ArrayList;

import static com.procuro.apimmdatamanagerlib.DeliveryPOITypeEnum.DeliveryPOITypesEnum.DeliveryPOIType_Undefined;
import static com.procuro.apimmdatamanagerlib.HOSDataModel.HOSTimeLogEntry.TimeLogEntryType.TimeLogEntryType_Departure;
import static com.procuro.apimmdatamanagerlib.HOSDataModel.HOSTimeLogEntry.TimeLogEntryType.TimeLogEntryType_Driving;
import static com.procuro.apimmdatamanagerlib.HOSDataModel.HOSTimeLogEntry.TimeLogEntryType.TimeLogEntryType_EndOfRoute;
import static com.procuro.apimmdatamanagerlib.HOSDataModel.HOSTimeLogEntry.TimeLogEntryType.TimeLogEntryType_PostTrip;
import static com.procuro.apimmdatamanagerlib.HOSDataModel.HOSTimeLogEntry.TimeLogEntryType.TimeLogEntryType_PreTrip;
import static com.procuro.apimmdatamanagerlib.HOSDataModel.HOSTimeLogEntry.TimeLogEntryType.TimeLogEntryType_RestBreak;
import static com.procuro.apimmdatamanagerlib.HOSDataModel.HOSTimeLogEntry.TimeLogEntryType.TimeLogEntryType_StartPoint;

/**
 * Created by jophenmarieweeks on 13/12/2017.
 */

public class HOSUtils extends PimmBaseObject {



    public static ArrayList replaceObjectAtIndex(int index, HOSTimeLogEntry timeEntry) {
        ArrayList<HOSTimeLogEntry> arrEntries = new ArrayList<HOSTimeLogEntry>();

        if(arrEntries.indexOf(index) == 1) {
            arrEntries.set(index, timeEntry);
        }

//        return arrEntries.rebuildTimeEntries();
        return  rebuildTimeEntries(arrEntries);
    }

    public static ArrayList rebuildTimeEntries(ArrayList<HOSTimeLogEntry> arrEntry) {

        ArrayList<HOSTimeLogEntry> arrEntries = new ArrayList<HOSTimeLogEntry>();

        // Clear driving logs
        for (HOSTimeLogEntry timeLogEntry : arrEntries) {
            if (timeLogEntry.timeLogEntryType == TimeLogEntryType_Driving) continue;

            arrEntries.add(timeLogEntry);
        }


        return insertDrivingTimes(arrEntries);

    }

    public static ArrayList insertDrivingTimes(ArrayList<HOSTimeLogEntry> arrEntry) {

        ArrayList<HOSTimeLogEntry> arrTimeEntries = new ArrayList<HOSTimeLogEntry>();
        ArrayList<HOSTimeLogEntry> arrEntries = new ArrayList<HOSTimeLogEntry>();

        // Clear driving entries before adding new
        for (HOSTimeLogEntry timeLogEntry : arrEntry) {
             if (timeLogEntry.timeLogEntryType == TimeLogEntryType_Driving) continue;

                arrEntries.add(timeLogEntry);
             }

            int total = arrEntries.size();

            for (int index=0; index<total; index++) {
                HOSTimeLogEntry timeLogEntry = arrEntries.get(index);
                HOSTimeLogEntry lastTimeLogEntry = null;

                if (arrTimeEntries.size() > 0)
                    lastTimeLogEntry = arrTimeEntries.get(arrTimeEntries.size() -1);

                // Add entries without driving segment
                if (timeLogEntry.timeLogEntryType == TimeLogEntryType_PreTrip ||
                        timeLogEntry.timeLogEntryType == TimeLogEntryType_Departure ||
                        timeLogEntry.timeLogEntryType == TimeLogEntryType_StartPoint ||
                        timeLogEntry.timeLogEntryType == TimeLogEntryType_PostTrip ||
                        timeLogEntry.timeLogEntryType == TimeLogEntryType_EndOfRoute) {

                    arrTimeEntries.add(timeLogEntry);
                    // lastTimeLogEntry = timeLogEntry;
                    continue;
                }

                // Skip adding driving time for stop with rest breaks
                if (lastTimeLogEntry != null) {
                    if (lastTimeLogEntry.End.compareTo(timeLogEntry.Start) == 0) {
                        if (timeLogEntry.Start.compareTo(timeLogEntry.End) != 1) {
                                arrTimeEntries.add(timeLogEntry);
                            continue;
                        }
                    }
                }

                // Skip adding driving for duplicate rest breaks
                if (timeLogEntry.timeLogEntryType == TimeLogEntryType_RestBreak) {
                    if (lastTimeLogEntry.timeLogEntryType == TimeLogEntryType_RestBreak) {
                            arrTimeEntries.add(timeLogEntry);
                        continue;
                    }
                }

                HOSTimeLogEntry drivingTimeEntry = new HOSTimeLogEntry();

                drivingTimeEntry.timeLogEntryType = TimeLogEntryType_Driving;
                drivingTimeEntry.DeliveryType = DeliveryPOIType_Undefined;
                drivingTimeEntry.Start = lastTimeLogEntry.End;
                drivingTimeEntry.End = timeLogEntry.Start;
                drivingTimeEntry.Label = "Driving";

                if (drivingTimeEntry.End == null)
                    drivingTimeEntry.End = drivingTimeEntry.Start;

                drivingTimeEntry.StartText = DateTimeFormat.ShortFormatStringForDateTime(lastTimeLogEntry.End);
                drivingTimeEntry.EndText = DateTimeFormat.ShortFormatStringForDateTime(timeLogEntry.Start);

                arrTimeEntries.add(drivingTimeEntry);
                arrTimeEntries.add(timeLogEntry);
            }

            return arrTimeEntries;
        }


    public static ArrayList removeEntryAtIndex(int index) {
        ArrayList<HOSTimeLogEntry> arrEntries = new ArrayList<HOSTimeLogEntry>();

        arrEntries.remove(index);

        return insertDrivingTimes(arrEntries);

    }
}
