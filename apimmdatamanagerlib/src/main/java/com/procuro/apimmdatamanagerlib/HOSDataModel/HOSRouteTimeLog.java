package com.procuro.apimmdatamanagerlib.HOSDataModel;

import com.procuro.apimmdatamanagerlib.AddressComponents;
import com.procuro.apimmdatamanagerlib.DateTimeFormat;
import com.procuro.apimmdatamanagerlib.DriverLogRestBreakInfoEntry;
import com.procuro.apimmdatamanagerlib.DriverLogTimeInfoDetails;
import com.procuro.apimmdatamanagerlib.DriverLogTimeInfoEntry;
import com.procuro.apimmdatamanagerlib.DriverLogTimeInfoEntryDetail;
import com.procuro.apimmdatamanagerlib.DriverTimeLog;
import com.procuro.apimmdatamanagerlib.HOSDataModel.HOSEditor.HOSUtils;
import com.procuro.apimmdatamanagerlib.PimmForm;
import com.procuro.apimmdatamanagerlib.SdrDelivery;
import com.procuro.apimmdatamanagerlib.SdrDriver;
import com.procuro.apimmdatamanagerlib.SdrGisSite;
import com.procuro.apimmdatamanagerlib.SdrPointOfInterest;
import com.procuro.apimmdatamanagerlib.SdrReport;
import com.procuro.apimmdatamanagerlib.SdrStop;
import com.procuro.apimmdatamanagerlib.iPimmDriverLogManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.TimeZone;

import static com.procuro.apimmdatamanagerlib.DeliveryPOITypeEnum.DeliveryPOITypesEnum.DeliveryPOIType_DeliveryStop;
import static com.procuro.apimmdatamanagerlib.DeliveryPOITypeEnum.DeliveryPOITypesEnum.DeliveryPOIType_EndPoint;
import static com.procuro.apimmdatamanagerlib.DeliveryPOITypeEnum.DeliveryPOITypesEnum.DeliveryPOIType_StartPoint;
import static com.procuro.apimmdatamanagerlib.DeliveryPOITypeEnum.DeliveryPOITypesEnum.DeliveryPOIType_Undefined;
import static com.procuro.apimmdatamanagerlib.DriverLogTimeInfoDetails.DriverLastStatusEnum.DriverLastStatusEnum_GoHome;
import static com.procuro.apimmdatamanagerlib.DriverLogTimeInfoDetails.DriverLastStatusEnum.DriverLastStatusEnum_GoOnBreak;
import static com.procuro.apimmdatamanagerlib.DriverLogTimeInfoDetails.DriverLastStatusEnum.DriverLastStatusEnum_Sleeper;
import static com.procuro.apimmdatamanagerlib.DriverLogTimeInfoDetails.DriverLastStatusEnum.DriverLastStatusEnum_StayOnDuty;
import static com.procuro.apimmdatamanagerlib.DriverLogTimeInfoEntryDetail.DriverLogTimeInfoEntryDetailType.DriverLogTimeInfoEntryDetailType_DC;
import static com.procuro.apimmdatamanagerlib.HOSDataModel.HOSDataManager.RestInsideStop.RestInsideStop_Begin;
import static com.procuro.apimmdatamanagerlib.HOSDataModel.HOSDataManager.RestInsideStop.RestInsideStop_End;
import static com.procuro.apimmdatamanagerlib.HOSDataModel.HOSDataManager.RestInsideStop.RestInsideStop_Middle;
import static com.procuro.apimmdatamanagerlib.HOSDataModel.HOSRouteTimeLog.ModifiedEntryIdentifier.ModifiedEntryType_AddPunchInTime;
import static com.procuro.apimmdatamanagerlib.HOSDataModel.HOSRouteTimeLog.ModifiedEntryIdentifier.ModifiedEntryType_AddPunchOutTime;
import static com.procuro.apimmdatamanagerlib.HOSDataModel.HOSRouteTimeLog.ModifiedEntryIdentifier.ModifiedEntryType_ManualEndEventTime;
import static com.procuro.apimmdatamanagerlib.HOSDataModel.HOSRouteTimeLog.ModifiedEntryIdentifier.ModifiedEntryType_ManualPOIArrivalDepartureTime;
import static com.procuro.apimmdatamanagerlib.HOSDataModel.HOSRouteTimeLog.ModifiedEntryIdentifier.ModifiedEntryType_ManualStartEventTime;
import static com.procuro.apimmdatamanagerlib.HOSDataModel.HOSRouteTimeLog.ModifiedEntryIdentifier.ModifiedEntryType_ManualStartPointDepartureTime;
import static com.procuro.apimmdatamanagerlib.HOSDataModel.HOSRouteTimeLog.ModifiedEntryIdentifier.ModifiedEntryType_ManualStopArrivalDepartureTime;
import static com.procuro.apimmdatamanagerlib.HOSDataModel.HOSRouteTimeLog.ModifiedEntryIdentifier.ModifiedEntryType_RemovePOIEntry;
import static com.procuro.apimmdatamanagerlib.HOSDataModel.HOSRouteTimeLog.ModifiedEntryIdentifier.ModifiedEntryType_RemoveStopEntry;
import static com.procuro.apimmdatamanagerlib.HOSDataModel.HOSRouteTimeLog.ModifiedEntryIdentifier.ModifiedEntryType_Unknown;
import static com.procuro.apimmdatamanagerlib.HOSDataModel.HOSRouteTimeLog.ModifiedEntryIdentifier.ModifiedEntryType_UpdatePunchInTime;
import static com.procuro.apimmdatamanagerlib.HOSDataModel.HOSRouteTimeLog.ModifiedEntryIdentifier.ModifiedEntryType_UpdatePunchOutTime;
import static com.procuro.apimmdatamanagerlib.HOSDataModel.HOSTimeLogEntry.TimeLogEntryType.TimeLogEntryType_Departure;
import static com.procuro.apimmdatamanagerlib.HOSDataModel.HOSTimeLogEntry.TimeLogEntryType.TimeLogEntryType_EndOfRoute;
import static com.procuro.apimmdatamanagerlib.HOSDataModel.HOSTimeLogEntry.TimeLogEntryType.TimeLogEntryType_EndPoint;
import static com.procuro.apimmdatamanagerlib.HOSDataModel.HOSTimeLogEntry.TimeLogEntryType.TimeLogEntryType_POI;
import static com.procuro.apimmdatamanagerlib.HOSDataModel.HOSTimeLogEntry.TimeLogEntryType.TimeLogEntryType_PostTrip;
import static com.procuro.apimmdatamanagerlib.HOSDataModel.HOSTimeLogEntry.TimeLogEntryType.TimeLogEntryType_PreTrip;
import static com.procuro.apimmdatamanagerlib.HOSDataModel.HOSTimeLogEntry.TimeLogEntryType.TimeLogEntryType_RestBreak;
import static com.procuro.apimmdatamanagerlib.HOSDataModel.HOSTimeLogEntry.TimeLogEntryType.TimeLogEntryType_Return;
import static com.procuro.apimmdatamanagerlib.HOSDataModel.HOSTimeLogEntry.TimeLogEntryType.TimeLogEntryType_StartPoint;
import static com.procuro.apimmdatamanagerlib.HOSDataModel.HOSTimeLogEntry.TimeLogEntryType.TimeLogEntryType_Stop;
import static com.procuro.apimmdatamanagerlib.HOSDataModel.HOSTimeLogEntry.TimeLogEntryType.TimeLogEntryType_Unknown;

/*
  Created by jophenmarieweeks on 08/12/2017.
 */

public class HOSRouteTimeLog extends HOSDataManager {




    public enum ModifiedEntryIdentifier {
        ModifiedEntryType_Unknown(0),
        ModifiedEntryType_AddPunchInTime(1),
        ModifiedEntryType_AddPunchOutTime(2),
        ModifiedEntryType_UpdatePunchInTime(3),
        ModifiedEntryType_UpdatePunchOutTime(4),
        ModifiedEntryType_ManualStopArrivalDepartureTime(5),
        ModifiedEntryType_ManualPOIArrivalDepartureTime(6),
        ModifiedEntryType_ManualStartEventTime(7),
        ModifiedEntryType_ManualEndEventTime(8),
        ModifiedEntryType_ManualStartPointDepartureTime(9),
        ModifiedEntryType_ManualEndPointArrivalTime(10),
        ModifiedEntryType_ManualUpdateRestBreakTime(11),
        ModifiedEntryType_ManualCreatePOIEntry(12),
        ModifiedEntryType_RemoveStopEntry(13),
        ModifiedEntryType_RemovePOIEntry(14),
        ModifiedEntryType_RemoveRestBreakEntry(15);

        private int value;

        private ModifiedEntryIdentifier( int value){
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }

        public static ModifiedEntryIdentifier setIntValue (int i) {
            for (ModifiedEntryIdentifier type : ModifiedEntryIdentifier.values()) {
                if (type.value == i) { return type; }
            }
            return ModifiedEntryType_Unknown;
        }
    }


    public class HOSModifiedTimeLogEntry extends HOSDataManager{
//        public id entry;

        public ModifiedEntryIdentifier typeIdentifier;
        public HOSTimeLogEntry.TimeLogEntryType typeEntry;
        public String refxId, audit;
        public String auditBy;
        public Date auditDate;
        public HOSTimeLogEntry entry;
        public Date entryDate;

        @Override
        public void setValueForKey(Object value, String key) {

            if(key.equalsIgnoreCase("typeIdentifier")) {
                if (!value.equals(null)) {
                    ModifiedEntryIdentifier result = ModifiedEntryIdentifier.setIntValue((int) value);
                    this.typeIdentifier = result;
                }
            }else if(key.equalsIgnoreCase("typeEntry")) {
                if (!value.equals(null)) {
                    HOSTimeLogEntry.TimeLogEntryType result = HOSTimeLogEntry.TimeLogEntryType.setIntValue((int) value);
                    this.typeEntry = result;
                }

            } else {
                super.setValueForKey(value, key);
            }
        }




    }



    public DriverLogTimeInfoDetails.DriverLastStatusEnum lastStatus, origLastStatus;
    public DriverLogTimeInfoDetails.DriverStatusEnum driverStatus;
    public HOSTimeLogEntry.TimeLogEntryType startEntry, endEntry;

    public HOSTimeLogEntry pickEntry, lastEntry;
    public static SdrReport report;
    public SdrDelivery delivery;
    public static PimmForm form;
    public SdrDriver driver, coDriver;
    public SdrGisSite site;
    public SdrPointOfInterest startPoint, endPoint;
    public DriverLogTimeInfoDetails timeInfoDetails;
    public DriverLogTimeInfoEntry punch;
    public DriverTimeLog driverTimeLog;

    public double version;
    public int changes;

    public String deliveryId, formId, loggedUsername, guardSignId;
    public String routeId, routeName, tractorId, tractorName, trailerId, trailerName;
    public Date dateToday, dispatch, punchStart, punchEnd, departure, arrival;
    public Date preTripStart, preTripEnd, postTripStart, postTripEnd, endOfRoute;
    public TimeZone timezone;
    public ArrayList dcEntries, poiStops;
    public ArrayList<HOSTimeLogEntry> routeEntries;
    public static ArrayList<HOSModifiedTimeLogEntry> modifiedEntries;
    public ArrayList<DriverLogRestBreakInfoEntry> restStops;
    private static final HOSRouteTimeLog ourInstance = new HOSRouteTimeLog();

    public static HOSRouteTimeLog getInstance()
    {
        return ourInstance;
    }



    @Override
    public void setValueForKey(Object value, String key) {

        if(key.equalsIgnoreCase("lastStatus")) {
            if (!value.equals(null)) {
                DriverLogTimeInfoDetails.DriverLastStatusEnum result = DriverLogTimeInfoDetails.DriverLastStatusEnum.setIntValue((int) value);
                this.lastStatus = result;
            }
        }else if(key.equalsIgnoreCase("origLastStatus")) {
            if (!value.equals(null)) {
                DriverLogTimeInfoDetails.DriverLastStatusEnum result = DriverLogTimeInfoDetails.DriverLastStatusEnum.setIntValue((int) value);
                this.origLastStatus = result;
            }
        }else if(key.equalsIgnoreCase("driverStatus")) {
            if (!value.equals(null)) {
                DriverLogTimeInfoDetails.DriverStatusEnum result = DriverLogTimeInfoDetails.DriverStatusEnum.setIntValue((int) value);
                this.driverStatus = result;
            }

        }else if(key.equalsIgnoreCase("startEntry")) {
            if (!value.equals(null)) {
                HOSTimeLogEntry.TimeLogEntryType result = HOSTimeLogEntry.TimeLogEntryType.setIntValue((int) value);
                this.startEntry = result;
            }
        }else if(key.equalsIgnoreCase("endEntry")) {
            if (!value.equals(null)) {
                HOSTimeLogEntry.TimeLogEntryType result = HOSTimeLogEntry.TimeLogEntryType.setIntValue((int) value);
                this.endEntry = result;
            }
        }else if(key.equalsIgnoreCase("pickEntry")) {
            if (!value.equals(null)) {
                HOSTimeLogEntry result = new HOSTimeLogEntry();
                JSONObject jsonObject = (JSONObject) value;
                result.readFromJSONObject(jsonObject);
                this.pickEntry = result;
            }
        }else if(key.equalsIgnoreCase("lastEntry")) {
            if (!value.equals(null)) {
                HOSTimeLogEntry result = new HOSTimeLogEntry();
                JSONObject jsonObject = (JSONObject) value;
                result.readFromJSONObject(jsonObject);
                this.lastEntry = result;
            }

        }else if(key.equalsIgnoreCase("report")) {
            if (!value.equals(null)) {
                SdrReport result = new SdrReport();
                JSONObject jsonObject = (JSONObject) value;
                result.readFromJSONObject(jsonObject);
                this.report = result;
            }

        }else if(key.equalsIgnoreCase("startPoint")) {
            if (!value.equals(null)) {
                SdrPointOfInterest result = new SdrPointOfInterest();
                JSONObject jsonObject = (JSONObject) value;
                result.readFromJSONObject(jsonObject);
                this.startPoint = result;
            }

        }else if(key.equalsIgnoreCase("endPoint")) {
            if (!value.equals(null)) {
                SdrPointOfInterest result = new SdrPointOfInterest();
                JSONObject jsonObject = (JSONObject) value;
                result.readFromJSONObject(jsonObject);
                this.endPoint = result;
            }
        }else if(key.equalsIgnoreCase("timeInfoDetails")) {
            if (!value.equals(null)) {
                DriverLogTimeInfoDetails result = new DriverLogTimeInfoDetails();
                JSONObject jsonObject = (JSONObject) value;
                result.readFromJSONObject(jsonObject);
                this.timeInfoDetails = result;
            }
        }else if(key.equalsIgnoreCase("punch")) {
            if (!value.equals(null)) {
                DriverLogTimeInfoEntry result = new DriverLogTimeInfoEntry();
                JSONObject jsonObject = (JSONObject) value;
                result.readFromJSONObject(jsonObject);
                this.punch = result;
            }

        }else if(key.equalsIgnoreCase("driverTimeLog")) {
            if (!value.equals(null)) {
                DriverTimeLog result = new DriverTimeLog();
                JSONObject jsonObject = (JSONObject) value;
                result.readFromJSONObject(jsonObject);
                this.driverTimeLog = result;
            }
        }else if(key.equalsIgnoreCase("coDriver")) {
            if (!value.equals(null)) {
                SdrDriver result = new SdrDriver();
                JSONObject jsonObject = (JSONObject) value;
                result.readFromJSONObject(jsonObject);
                this.coDriver = result;
            }

        }else if(key.equalsIgnoreCase("driver")) {
            if (!value.equals(null)) {
                SdrDriver result = new SdrDriver();
                JSONObject jsonObject = (JSONObject) value;
                result.readFromJSONObject(jsonObject);
                this.driver = result;
            }
        } else if (key.equalsIgnoreCase("modifiedEntries")) {

            if (this.modifiedEntries == null) {
                this.modifiedEntries = new ArrayList<HOSModifiedTimeLogEntry>();
            }

            if (!value.equals(null)) {
                JSONArray arrData = (JSONArray) value;

                if (arrData != null) {
                    for (int i = 0; i < arrData.length(); i++) {
                        try {
                            HOSModifiedTimeLogEntry result = new HOSModifiedTimeLogEntry();
                            result.readFromJSONObject(arrData.getJSONObject(i));

                            this.modifiedEntries.add(result);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        } else if (key.equalsIgnoreCase("restStops")) {

            if (this.restStops == null) {
                this.restStops = new ArrayList<DriverLogRestBreakInfoEntry>();
            }

            if (!value.equals(null)) {
                JSONArray arrData = (JSONArray) value;

                if (arrData != null) {
                    for (int i = 0; i < arrData.length(); i++) {
                        try {
                            DriverLogRestBreakInfoEntry result = new DriverLogRestBreakInfoEntry();
                            result.readFromJSONObject(arrData.getJSONObject(i));

                            this.restStops.add(result);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        } else {
            super.setValueForKey(value, key);
        }
    }


    public static HOSRouteTimeLog initWithSdrReport(SdrReport sdrReport, PimmForm pimmForm){


        modifiedEntries = new ArrayList<HOSModifiedTimeLogEntry>();
        report = sdrReport;
        form = pimmForm;

        return ourInstance;

    }
    public void buildTimeLogEntriesForDriver(SdrDriver sdrDriver) {
        if (this.report == null) return;


        iPimmDriverLogManager driverManager = new iPimmDriverLogManager();

        this.delivery = this.report.Hierarchy;
        this.formId = null;
        this.deliveryId = this.delivery.DeliveryId;
        this.routeId = this.delivery.RouteId;
        this.routeName = this.delivery.RouteName;
        this.tractorId = this.delivery.TractorId;
        this.tractorName = this.delivery.Tractor;
        this.trailerId = this.delivery.TrailerId;
        this.trailerName = this.delivery.TrailerName;
        this.driver = this.delivery.Driver;
        this.coDriver = this.delivery.CoDriver;

        this.dispatch = this.delivery.DispatchTime;


        if (this.form == null) {
            // Initialize new driver time log for empty pimm body
            this.driverTimeLog = new DriverTimeLog();

            this.driverTimeLog.DeliveryID = this.deliveryId;
            this.driverTimeLog.RouteName = this.routeName;
            this.driverTimeLog.Tractor = this.tractorName;
            this.driverTimeLog.Trailer = this.trailerName;
            this.driverTimeLog.Driver = this.driver.Username;
            this.driverTimeLog.DriverID = this.driver.UserId;

            if (this.coDriver != null) {
                this.driverTimeLog.CoDriver = this.coDriver.Username;
                this.driverTimeLog.CoDriverID = this.coDriver.UserId;
            }
        } else {
            this.driverTimeLog = driverManager.getDriverTimeLogFromJSONData(this.form.bodyData);
            this.formId = this.form.formID;

        }

        // Update driver time log version

        this.timeInfoDetails = this.driverTimeLog.TimeInfoDetails;
        this.restStops = this.timeInfoDetails.RestStops;
        this.poiStops = this.delivery.GIS.PointsOfInterest;

        DriverLogTimeInfoEntry infoPreTrip = this.timeInfoDetails.PreTrip;
        DriverLogTimeInfoEntry infoPostTrip = this.timeInfoDetails.PostTrip;
        DriverLogTimeInfoEntry infoDispatch = this.timeInfoDetails.Dispatch;

        ArrayList arrRouteEntries = new ArrayList();
        ArrayList arrDCEntries = new ArrayList();
        ArrayList<SdrGisSite> arrSites = this.delivery.GIS.Sites;
//        ArrayList arrStops = this.buildStopLogEntries(this.delivery.GIS.Stops);
//        ArrayList arrPOIs = this.buildPOILogEntries(this.delivery.GIS.PointsOfInterest);

        ArrayList<SdrStop> arrStops = this.buildStopLogEntries(this.delivery.GIS.Stops);
        ArrayList<SdrPointOfInterest> arrPOIs = this.buildPOILogEntries(this.delivery.GIS.PointsOfInterest);

        List<String> arrPlanLabels = Arrays.asList("HOSPreTripText", "HOSDepartureText", "HOSReturnText", "HOSPostTripText", "HOSEndOfRouteText");
        ArrayList arrPOILabels = null;

        if(sdrDriver.DriverId.equalsIgnoreCase(this.driverTimeLog.CoDriverID)){
            this.timeInfoDetails = this.driverTimeLog.CoDriverTimeInfoDetails;

        }

        // Display departure time using the update DTL time
        if (infoDispatch.Start != null) {
            this.departure = infoDispatch.Start;
        }
        // Display return time using the update DTL time
        if (infoDispatch.End != null) {
            this.arrival = infoDispatch.End;
        }
        this.version = this.driverTimeLog.Version;
        this.punch = this.timeInfoDetails.Punch;

        if (this.departure == null) {
            this.departure = this.delivery.DepartureTime;
        }
        if (this.arrival == null) {
            this.arrival = this.delivery.ArrivalTime;
        }


        this.punchStart = this.punch.Start;
        this.punchEnd = this.punch.End;
        this.preTripStart = infoPreTrip.Start; // this.punchStart;
        this.preTripEnd = this.departure;
        this.postTripStart = this.arrival;
        this.postTripEnd = this.arrival;
        this.lastStatus = this.timeInfoDetails.LastStatus;
        this.origLastStatus = this.timeInfoDetails.LastStatus;
        this.lastEntry = new HOSTimeLogEntry();

        // Get dispatch time if departure time is not detected
        if (this.departure == null)
            this.departure = this.dispatch;

        // Get start post-trip if exist
        if (infoPostTrip != null) {
            if (infoPostTrip.Start != null)
                this.postTripStart = infoPostTrip.Start;

            if (infoPostTrip.End != null)
                this.postTripEnd = infoPostTrip.End;
        }

        // Get DC site address and coordinates

        if (arrSites.size() > 0) {
            // Get POI labels
            if (this.delivery.GIS != null) {
                if (this.delivery.GIS.PointsOfInterest.size() > 0) {
//                    arrPOILabels = [this.delivery.GIS.PointsOfInterest valueForKey:@"Label"];
                    // arrPOILabels.add(this.delivery.GIS.PointsOfInterest);

                    arrPOILabels = this.delivery.GIS.PointsOfInterest;

                    List<String> arrLabels = null;
                    if (arrPOILabels.size() > 0) {
                        arrLabels = arrPlanLabels;

                        // Replace departure or return if start or end point exist
                        for (int x = 1; x < 3; x++) {
//                            @autoreleasepool {
                            String strLabel = x == 1 ? HOSStartPointText : HOSEndPointText;

                            if (!arrPOILabels.contains(strLabel)) continue;

                            arrLabels.remove(x);
                            arrLabels.add(x, strLabel);
                        }
                    }

                    arrPlanLabels = arrLabels;

                }
            }
        }

// Create each dc entries (Pre-Trip, Departure/Start Point, Return/End Point, Post-Trip, End of Route)
        for (String strLabel : arrPlanLabels) {
//            @autoreleasepool {
                HOSTimeLogEntry timeLogEntry =  new HOSTimeLogEntry();

                timeLogEntry.timeLogEntryType = TimeLogEntryType_Unknown;
                timeLogEntry.DeliveryType =  DeliveryPOIType_Undefined;
                timeLogEntry.Annotation = "";
                timeLogEntry.StartDetail = null;
                timeLogEntry.Label = strLabel;

                // Add start detail for departure and return
                if (HOSDepartureText.contains(strLabel) || HOSReturnText.contains(strLabel)){
                    if (arrSites.size() > 0) {
                        DriverLogTimeInfoEntryDetail infoDetail = new DriverLogTimeInfoEntryDetail();

                        infoDetail.driverLogTimeInfoEntryDetailType = DriverLogTimeInfoEntryDetailType_DC;
                        infoDetail.AddressComponents = this.site.AddressComponents;
                        infoDetail.LatLon = this.site.LatLon;

                        timeLogEntry.StartDetail = infoDetail;
                    }
                }

                // Append pre trip entry
                if (strLabel.equalsIgnoreCase(HOSPreTripText)) {
                    timeLogEntry.timeLogEntryType = TimeLogEntryType_PreTrip;

                    if (this.preTripStart != null) {
                        timeLogEntry.StartText = String.valueOf(this.preTripStart);
                        timeLogEntry.OriginalStart = this.preTripStart;
                        timeLogEntry.Start = this.preTripStart;
                    }

                    timeLogEntry.EndText = String.valueOf(this.preTripEnd);
                    timeLogEntry.OriginalEnd = this.preTripEnd;
                    timeLogEntry.End = this.preTripEnd;

                    // Update pre-trip start and end time entry
                    if (this.preTripStart != null) {
                        this.timeInfoDetails.PreTrip.StartText = timeLogEntry.StartText;
                        this.timeInfoDetails.PreTrip.Start = timeLogEntry.Start;
                    }

                    this.timeInfoDetails.PreTrip.EndText = timeLogEntry.EndText;
                    this.timeInfoDetails.PreTrip.End = timeLogEntry.End;

                    // Append departure entry
                } else if (strLabel.equalsIgnoreCase(HOSDepartureText)) {
                    timeLogEntry.StartText = String.valueOf(this.departure);
                    timeLogEntry.EndText = String.valueOf(this.departure);
                    timeLogEntry.timeLogEntryType = TimeLogEntryType_Departure;
                    timeLogEntry.Start = this.departure;
                    timeLogEntry.End = this.departure;

                    timeLogEntry.OriginalStart = this.delivery.DepartureTime == null ? null : this.departure;
                    timeLogEntry.OriginalEnd = this.departure;

                    this.startEntry = TimeLogEntryType_Departure;

                    // Append start and end point entry if exist
                } else if (HOSStartPointText.contains(strLabel) || HOSEndPointText.contains(strLabel)) {
                    if (arrPOILabels.size() == 0) continue;
                    if (!arrPOILabels.contains(strLabel)) continue;

                    int index = arrPOILabels.indexOf(strLabel);

                    SdrPointOfInterest sdrPOI = this.delivery.GIS.PointsOfInterest.get(index);

                    if (sdrPOI != null) {
                        timeLogEntry.StartDetail = new DriverLogTimeInfoEntryDetail();

                        timeLogEntry.StartDetail.AddressComponents = sdrPOI.AddressComponents;
                        timeLogEntry.StartDetail.LatLon = sdrPOI.LatLon;

                        timeLogEntry.StartText = String.valueOf(sdrPOI.ArrivalTime);
                        timeLogEntry.EndText = String.valueOf(sdrPOI.DepartureTime);
                        timeLogEntry.timeLogEntryType = TimeLogEntryType_StartPoint;
                        timeLogEntry.Start = sdrPOI.ArrivalTime;
                        timeLogEntry.End = sdrPOI.DepartureTime;

                        timeLogEntry.OriginalStart = sdrPOI.ArrivalTime;
                        timeLogEntry.OriginalEnd = sdrPOI.DepartureTime;

                        if (strLabel.equalsIgnoreCase(HOSEndPointText)) {
                            timeLogEntry.timeLogEntryType = TimeLogEntryType_EndPoint;
                            // Update post trip start time using end point arrival time
                            this.postTripStart = sdrPOI.ArrivalTime;
                            this.startEntry = TimeLogEntryType_EndPoint;

                        } else {
                            this.startEntry = TimeLogEntryType_StartPoint;
                        }
                    }

                    // Append return entry
                } else if (strLabel.equalsIgnoreCase(HOSReturnText)) {
                    timeLogEntry.StartText = String.valueOf(this.arrival);
                    timeLogEntry.EndText = String.valueOf(this.arrival);
                    timeLogEntry.timeLogEntryType = TimeLogEntryType_Return;
                    timeLogEntry.Start = this.arrival == null ? this.postTripStart : this.arrival;
                    timeLogEntry.End = this.postTripStart;

                    timeLogEntry.OriginalStart = this.arrival;
                    timeLogEntry.OriginalEnd = this.postTripStart;

                    this.startEntry = TimeLogEntryType_Return;

                    // Append post trip entry
                } else if (strLabel.equalsIgnoreCase(HOSPostTripText)) {
                    DriverLogTimeInfoEntry entryPostTrip = this.timeInfoDetails.PostTrip;

                    timeLogEntry.StartText = String.valueOf(this.postTripStart);
                    timeLogEntry.EndText = String.valueOf(this.postTripEnd);
                    timeLogEntry.timeLogEntryType = TimeLogEntryType_PostTrip;
                    timeLogEntry.Start = this.arrival == null ? this.postTripStart : this.arrival;
                    timeLogEntry.End = this.postTripEnd;

                    timeLogEntry.OriginalStart = this.postTripStart;
                    timeLogEntry.OriginalEnd = this.postTripEnd;

                    if (entryPostTrip.End != null) {
                        timeLogEntry.EndText = String.valueOf(entryPostTrip.End);
                        timeLogEntry.End = entryPostTrip.End;
                    }

                    // Append end of route entry
                }  else if (strLabel.equalsIgnoreCase(HOSEndOfRouteText)) {
                    timeLogEntry.StartText = String.valueOf(this.postTripEnd);
                    timeLogEntry.EndText = String.valueOf(this.postTripEnd);
                    timeLogEntry.timeLogEntryType = TimeLogEntryType_EndOfRoute;
                    timeLogEntry.Start = this.postTripEnd;
                    timeLogEntry.End = this.postTripEnd;

                    timeLogEntry.OriginalStart = this.postTripEnd;
                    timeLogEntry.OriginalEnd = this.postTripEnd;
                }

                    arrDCEntries.add(timeLogEntry);
//            }
        }

        // Merge point of interest and regular stops
        arrRouteEntries = arrDCEntries;

//        ArrayList<?> sampleArrList = new ArrayList<?>();
        if (arrPOIs.size() > 0) {
//            arrStops = arrStops.size() == 0 ? arrPOIs : arrStops.addAll(arrStops, arrPOIs);

//            if (arrStops.size() == 0) {
//                sampleArrList = arrPOIs;
//            } else
//                sampleArrList = sampleArrList.addAll(arrStops, arrPOIs);
        }


        if (arrStops.size() > 0) {
            int count = arrStops.size();

            // Making sure that the stops are sorted base on arrival time
            arrStops = arrStops; //sortBy:@"Start "];

            // Append all route entries after the index of 2 = departure or start point
            for (int index=0; index<count; index++) {
//                [arrRouteEntries insertObject:[arrStops objectAtIndex:index] atIndex:(index+2)];
            }
        }

        // Create last status entry
        this.lastEntry.StartText = String.valueOf(this.postTripEnd);
        this.lastEntry.EndText = String.valueOf(this.postTripEnd);
        this.lastEntry.Label = getLastStatusText();
        this.lastEntry.Start = this.postTripEnd;
        this.lastEntry.End = this.postTripEnd;

        this.routeEntries =arrRouteEntries; //arrRouteEntries.insertDrivingTimes();
        this.dcEntries = arrDCEntries;


    }

    public String buildDriverTimeLogString(){
        iPimmDriverLogManager driverManager = iPimmDriverLogManager.getDriverLogManager();
        this.driverTimeLog.GuardSignatureID = this.guardSignId;

        return driverManager.getJSONStringForDriverTimeLog(this.driverTimeLog);
    }

    public ArrayList<SdrStop> buildStopLogEntries(ArrayList<SdrStop> arrStops){
        ArrayList arrStopsEntries = new ArrayList();

        for (SdrStop sdrStop : arrStops) {
//            @autoreleasepool {
                HOSTimeLogEntry timeLogEntry = new HOSTimeLogEntry();
                DriverLogTimeInfoEntryDetail infoDetail = new DriverLogTimeInfoEntryDetail();

                infoDetail.LatLon = sdrStop.LatLon;
                infoDetail.AddressComponents = sdrStop.AddressComponents;

                if (sdrStop.AddressComponents != null) {

                    StringBuilder annotation = new StringBuilder();

                    if (sdrStop.AddressComponents.City != null) {
                        annotation.append(sdrStop.AddressComponents.City);
                        annotation.append(",");
                    }

                    if (sdrStop.AddressComponents.Province != null) {
                        annotation.append(sdrStop.AddressComponents.Province);
                    }

                    timeLogEntry.Annotation = annotation.toString();
                }

                timeLogEntry.StopId = sdrStop.SdrStopId;
                timeLogEntry.timeLogEntryType = TimeLogEntryType_Stop;
                timeLogEntry.DeliveryType = DeliveryPOIType_DeliveryStop;
                timeLogEntry.StopName = sdrStop.SiteName;
                timeLogEntry.Duration = sdrStop.DurationText;
                timeLogEntry.Label = "Stop " + sdrStop.StopText;
                timeLogEntry.DeliveryStart = sdrStop.DeliveryStartTime;
                timeLogEntry.DeliveryEnd = sdrStop.DeliveryEndTime;
                timeLogEntry.StartDetail = infoDetail;

                // Use expected delivery time to enable to display the terminated route with stops
                timeLogEntry.Start = sdrStop.ArrivalTime == null ? sdrStop.ExpectedDeliveryTime : sdrStop.ArrivalTime;
                timeLogEntry.End = sdrStop.DepartureTime == null ? sdrStop.ExpectedDeliveryTime : sdrStop.DepartureTime;
                timeLogEntry.StartText = DateTimeFormat.ShortFormatStringForDateTime(timeLogEntry.Start);
                timeLogEntry.EndText = DateTimeFormat.ShortFormatStringForDateTime(timeLogEntry.End);

                timeLogEntry.OriginalStart = sdrStop.ArrivalTime;
                timeLogEntry.OriginalEnd = sdrStop.DepartureTime;

                // Make sure to handle duplicate stop entries
                if (!arrStopsEntries.contains(timeLogEntry)) {
                    arrStopsEntries.add(getRestBreakInsideStop(timeLogEntry));
               }
//            }
        }


        return arrStopsEntries;

    }



    public String getLastStatusText() {

        if (this.lastStatus == DriverLastStatusEnum_StayOnDuty)
            return HOSStayOnDutyText;

        if (this.lastStatus == DriverLastStatusEnum_GoHome)
            return HOSPunchOutText;

        if (this.lastStatus == DriverLastStatusEnum_GoOnBreak)
            return HOSGoOnBreakText;

        if (this.lastStatus == DriverLastStatusEnum_Sleeper)
            return HOSSleeperText;

        return HOSNotAvailableText;

    }

    public ArrayList buildPOILogEntries(ArrayList<SdrPointOfInterest> arrPOIs) {
        ArrayList arrPOIEntries = new ArrayList();

        for (SdrPointOfInterest sdrPOI : arrPOIs) {
//            @autoreleasepool {
                // Skip adding POI start and end point and it will be use to replace departure or return entries
                if (sdrPOI.Type == DeliveryPOIType_StartPoint || sdrPOI.Type == DeliveryPOIType_EndPoint) {
                    // Get start and end point interest
                    if (sdrPOI.Type == DeliveryPOIType_StartPoint) {
                        this.startPoint = sdrPOI;
                        continue;
                    }

                    this.endPoint = sdrPOI;
                    continue;
                }

                HOSTimeLogEntry timeLogEntry =  new HOSTimeLogEntry();
                DriverLogTimeInfoEntryDetail infoDetail = new DriverLogTimeInfoEntryDetail();

                infoDetail.AddressComponents = sdrPOI.AddressComponents;
                infoDetail.LatLon = sdrPOI.LatLon;

                if (infoDetail.AddressComponents != null) {
                    StringBuilder annotation = new StringBuilder();

                    if (infoDetail.AddressComponents.City != null) {
                        annotation.append(infoDetail.AddressComponents.City);
                        annotation.append(",");
                    }

                    if (infoDetail.AddressComponents.Province != null) {
                        annotation.append(infoDetail.AddressComponents.Province);
                    }
                    timeLogEntry.Annotation = annotation.toString();
                }

                timeLogEntry.StartText = DateTimeFormat.ShortFormatStringForDateTime(sdrPOI.ArrivalTime);
                timeLogEntry.Start = sdrPOI.ArrivalTime;
                timeLogEntry.EndText = DateTimeFormat.ShortFormatStringForDateTime(sdrPOI.DepartureTime);
                timeLogEntry.End = sdrPOI.DepartureTime;
                timeLogEntry.OriginalStart = timeLogEntry.Start;
                timeLogEntry.OriginalEnd = timeLogEntry.End;
                timeLogEntry.timeLogEntryType = TimeLogEntryType_POI;
                timeLogEntry.DeliveryType = sdrPOI.Type;
                timeLogEntry.StopId = sdrPOI.SdrPointOfInterestId;
                timeLogEntry.Duration = sdrPOI.DurationText;
                timeLogEntry.StartDetail = infoDetail;
                timeLogEntry.Label = sdrPOI.Label;

            arrPOIEntries.add(timeLogEntry);

//            }
        }

        return arrPOIEntries;
    }


    public ArrayList getRestBreakInsideStop(HOSTimeLogEntry stopEntry) {

        if(this.restStops.size() == 0){
            return (ArrayList) Arrays.asList(stopEntry);
        }


        StringBuilder annotation = new StringBuilder();
        ArrayList arrEntries = new ArrayList();

        for (DriverLogRestBreakInfoEntry restBreak : this.restStops) {
            HOSTimeLogEntry timeLogEntry = new HOSTimeLogEntry();
            AddressComponents address = restBreak.StartDetail.AddressComponents;

            StringBuilder strAnnotation = new StringBuilder();
            JSONObject restDict = new JSONObject(restBreak.dictionaryWithValuesForKeys());
            timeLogEntry.readFromJSONObject(restDict);
//           [timeLogEntry readFromJSONDictionary:[[restBreak dictionaryWithValuesForKeys] mutableCopy]];

            if (address.City != null) {
                strAnnotation.append(address.City);
                strAnnotation.append(",");
            }

            if (address.Province != null) {
                strAnnotation.append(address.Province);
            }

            timeLogEntry.timeLogEntryType = TimeLogEntryType_RestBreak;
            timeLogEntry.DeliveryType = DeliveryPOIType_Undefined;
            timeLogEntry.Annotation = strAnnotation.toString();
            timeLogEntry.Label = "Rest Break";

            int RestInsideStop = timeLogEntry.isInsideStop(stopEntry);

            // Add rest break before the regular stop
            if (RestInsideStop == RestInsideStop_Begin.getValue()) {
                HOSTimeLogEntry newStopEntry = new HOSTimeLogEntry();

                newStopEntry = stopEntry.copyObject();

                newStopEntry.Start = stopEntry.DeliveryStart;
                newStopEntry.End = stopEntry.DeliveryEnd;

            arrEntries.add(timeLogEntry);

                if (newStopEntry.End.compareTo(newStopEntry.Start) != 1) {
                    if (!arrEntries.contains(newStopEntry)) {
                        arrEntries.add(newStopEntry);
                    }
                } else {
                    newStopEntry = newStopEntry.switchStartEndValues();

                    if (!(arrEntries.contains(newStopEntry))) {
                        arrEntries.add(newStopEntry);
                    }
                }

                return arrEntries;

                // Add rest break after the regular stop
            } else if(RestInsideStop == RestInsideStop_End.getValue()) {
                HOSTimeLogEntry newStopEntry = new HOSTimeLogEntry();
                newStopEntry = stopEntry.copyObject();

                newStopEntry.EndText = DateTimeFormat.ShortFormatStringForDateTime(timeLogEntry.Start);
                newStopEntry.End = timeLogEntry.Start;

                if (! arrEntries.contains(newStopEntry.StopId)) {
                    arrEntries.add(newStopEntry);
                }

            arrEntries.add(timeLogEntry);

                return arrEntries;

                // Add rest break between start and end time of regular stop
            } else if (RestInsideStop == RestInsideStop_Middle.getValue()) {

                HOSTimeLogEntry beginEntry = stopEntry.copyObject();
                HOSTimeLogEntry endEntry = stopEntry.copyObject();

                beginEntry.EndText = DateTimeFormat.ShortFormatStringForDateTime(timeLogEntry.Start);
                beginEntry.End = timeLogEntry.Start;

                endEntry.StartText = DateTimeFormat.ShortFormatStringForDateTime(timeLogEntry.End);
                endEntry.Start = timeLogEntry.End;

                if (beginEntry.End.compareTo(beginEntry.Start) != 1) {
                    if (!(arrEntries.contains(beginEntry))) {
                       arrEntries.add(beginEntry);
                    }
                }

                if (beginEntry.End.compareTo(beginEntry.Start) != 1) {
                    arrEntries.add(timeLogEntry);
                }

                if (endEntry.End.compareTo(endEntry.Start) != 1) {
                    if (!(arrEntries.contains(endEntry))) {
                        arrEntries.add(endEntry);
                    }
                }

                return arrEntries;
            }
        }

        return (ArrayList) Arrays.asList(stopEntry);


    }

/* insert, update and delete */
    public void insertModifiedTimeLogEntry (HOSTimeLogEntry timeLogEntry){

        boolean isRemove =  this.routeEntries.contains(timeLogEntry) ? false :true;
        ArrayList arrTypeEntries = this.modifiedEntries;

        // Skip adding modify entries for (Unknown, Start-Point and End-Point)
        if ((timeLogEntry.timeLogEntryType == TimeLogEntryType_Unknown) || (timeLogEntry.timeLogEntryType == TimeLogEntryType_StartPoint) || (timeLogEntry.timeLogEntryType == TimeLogEntryType_EndPoint)) {
            return;
        }

        HOSModifiedTimeLogEntry modifiedEntry = new HOSModifiedTimeLogEntry();
        HOSTimeLogEntry.TimeLogEntryType timeLogType = timeLogEntry.timeLogEntryType;

        modifiedEntry.typeIdentifier = ModifiedEntryType_Unknown;
        modifiedEntry.typeEntry = timeLogEntry.timeLogEntryType;
        modifiedEntry.entry = timeLogEntry;
        modifiedEntry.refxId = timeLogEntry.StopId;
        modifiedEntry.auditBy = this.loggedUsername;
        modifiedEntry.auditDate = this.dateToday;

        // Insert dc manual pre and post trip
        // if (timeLogType == TimeLogEntryType_PreTrip || timeLogType == TimeLogEntryType_PostTrip)
        //    modifiedEntry.typeIdentifier = ModifiedEntryType_ManualDCDepartureAndReturn;
        if (timeLogType == TimeLogEntryType_PreTrip)
            modifiedEntry.typeIdentifier = ModifiedEntryType_ManualStartEventTime;

        if (timeLogType == TimeLogEntryType_PostTrip)
            modifiedEntry.typeIdentifier = ModifiedEntryType_ManualEndEventTime;

        // Insert stop manual arrival and departure time
        if (timeLogType == TimeLogEntryType_Stop) {
            modifiedEntry.typeIdentifier = isRemove ? ModifiedEntryType_RemoveStopEntry : ModifiedEntryType_ManualStopArrivalDepartureTime;
        }

        // Insert poi manual arrival and departure time
        if (timeLogType == TimeLogEntryType_POI ||
                timeLogType == TimeLogEntryType_StartPoint ||
                timeLogType == TimeLogEntryType_EndPoint)
            modifiedEntry.typeIdentifier = isRemove ?  ModifiedEntryType_RemovePOIEntry : ModifiedEntryType_ManualPOIArrivalDepartureTime;

        // Insert manual start point departure time
        if (timeLogType == TimeLogEntryType_StartPoint || timeLogType == TimeLogEntryType_EndPoint)
            modifiedEntry.typeIdentifier = ModifiedEntryType_ManualStartPointDepartureTime;

        if (isRemove == false) {
            if (arrTypeEntries.size() > 0) {
                int identifier =modifiedEntry.typeIdentifier.getValue();

                // Handle duplicate entries by checking if exist
                if (arrTypeEntries.contains(identifier)) {
                    int index = arrTypeEntries.indexOf(identifier);

                    this.modifiedEntries.remove(index);
                }
            }
        }

        if (modifiedEntry.typeIdentifier == ModifiedEntryType_Unknown) return;

        this.modifiedEntries.add(modifiedEntry);


    }


    public void updatePunchStartTime (Date dateStart){

        HOSModifiedTimeLogEntry modifiedEntry = new HOSModifiedTimeLogEntry();
        modifiedEntry.typeIdentifier = this.punchStart == null ? ModifiedEntryType_AddPunchInTime : ModifiedEntryType_UpdatePunchInTime;
        modifiedEntry.entryDate =  dateStart;

        this.punchStart = dateStart;
        if (this.modifiedEntries.size() > 0) {
            ArrayList arrIdentifiers = this.modifiedEntries;
            int number = ModifiedEntryType_AddPunchInTime.getValue();

            // Cancel adding new modify punch-out entry if added time already exist
            if (arrIdentifiers.contains(number)) return;
        }

        // Logs all modified time log entries
        this.modifiedEntries.add(modifiedEntry);

    }

    public void updatePunchEndTime(Date dateEnd) {
        HOSModifiedTimeLogEntry modifiedEntry = new HOSModifiedTimeLogEntry();

        modifiedEntry.typeIdentifier = this.punchEnd == null ? ModifiedEntryType_AddPunchOutTime : ModifiedEntryType_UpdatePunchOutTime;
        modifiedEntry.entryDate = dateEnd;

        this.punchEnd = dateEnd;

        if (this.version >= 1.1) {
            if (this.timeInfoDetails.PunchTimes.size() > 0) {
                ArrayList<DriverLogTimeInfoEntry> arrPunchTimes = this.timeInfoDetails.PunchTimes;

                DriverLogTimeInfoEntry lastPunch = arrPunchTimes.get(arrPunchTimes.size()-1);

                lastPunch.EndText = DateTimeFormat.ShortFormatStringForDateTime(dateEnd);
                lastPunch.End = dateEnd;

                this.timeInfoDetails.PunchTimes = arrPunchTimes;
            }
        }

        if (this.modifiedEntries.size() > 0) {
            ArrayList arrIdentifiers = this.modifiedEntries;
            int number = ModifiedEntryType_AddPunchOutTime.getValue();

            // Cancel adding new modify punch-out entry if added time already exist
            if (arrIdentifiers.contains(number)) return;
        }

        // Logs all modified time log entries
         this.modifiedEntries.add(modifiedEntry);
    }

    public void updateTimeLogEntryAtIndex(int index, Date dateStart, Date dateEnd) {
        HOSTimeLogEntry timeLogEntry = (HOSTimeLogEntry) this.routeEntries.get(index);

        if (dateStart != null) {
            timeLogEntry.StartText = DateTimeFormat.ShortFormatStringForDateTime(dateStart);
            timeLogEntry.Start = dateStart;
        }

        if (dateEnd != null) {
            timeLogEntry.EndText = DateTimeFormat.ShortFormatStringForDateTime(dateEnd);
            timeLogEntry.End = dateEnd;
        }

        this.routeEntries = HOSUtils.replaceObjectAtIndex(index,timeLogEntry);

        // Append auto modified entry
            insertModifiedTimeLogEntry(timeLogEntry);
    }
    public void updateTimeLogEntryWithStartDate(Date dateStart, Date dateEnd){

        if (!(this.routeEntries.contains(pickEntry))){
            return;
        }

        HOSTimeLogEntry timeLogEntry = this.pickEntry;
        int index = this.routeEntries.indexOf(timeLogEntry);


        if (timeLogEntry.timeLogEntryType == TimeLogEntryType_EndOfRoute) {
            dateEnd = dateStart;

        }
        switch (this.pickEntry.timeLogEntryType) {
            case TimeLogEntryType_PreTrip:
                // Auto update departure/start point start and end time
                if (!(timeLogEntry.End == dateEnd)) {
                 updateTimeLogEntryAtIndex(1 ,dateEnd, dateEnd);
            }
            break;

            case TimeLogEntryType_PostTrip:
                // Auto update return/end point start and end time
            updateTimeLogEntryAtIndex(this.routeEntries.size()-3 ,dateStart, dateEnd);
                // Auto update end of route start and end time;
             updateTimeLogEntryAtIndex(this.routeEntries.size()-1 ,dateStart,dateEnd);
                break;

            case TimeLogEntryType_EndOfRoute:
                // Auto update post-trip end time
            updateTimeLogEntryAtIndex(this.routeEntries.size()-2,  dateStart, dateEnd);
                break;

            case TimeLogEntryType_Stop:
                break;

            default:
                break;
        }
        timeLogEntry.StartText = DateTimeFormat.ShortFormatStringForDateTime(dateStart);
        timeLogEntry.EndText = DateTimeFormat.ShortFormatStringForDateTime(dateEnd);
        timeLogEntry.Start = dateStart;
        timeLogEntry.End = dateEnd;

        this.routeEntries = HOSUtils.replaceObjectAtIndex(index ,timeLogEntry);
//        this.routeEntries = this.routeEntries.replaceObjectAtIndex:index timeLogEntry:timeLogEntry];
//        // Append auto modified entry
              insertModifiedTimeLogEntry(timeLogEntry);



    }

    public void updateTimeLogEntryAtIndex(Integer index, Date date, String StartEnd) {

        if(StartEnd == "start") {
            updateTimeLogEntryAtIndex(index, date, null);
        }else{
            updateTimeLogEntryAtIndex(index ,null, date);
        }
    }
//    public void updateTimeLogEntryAtIndex(Integer index, Date dateEnd) {
//        updateTimeLogEntryAtIndex(index ,null, dateEnd );
//    }


    public void updateTimeLogEntryWithStartDate(Date dateStart){
        updateTimeLogEntryWithStartDate(dateStart, this.pickEntry.End);
    }
    public void updateTimeLogEntryWithEndDate(Date dateEnd){
        updateTimeLogEntryWithStartDate(this.pickEntry.Start, dateEnd);
    }

    public void updateEventWithEntry(HOSTimeLogEntry.TimeLogEntryType entryType, AddressComponents address){

            HOSTimeLogEntry timeLogEntry = (HOSTimeLogEntry) this.routeEntries.get(1);
            DriverLogTimeInfoEntryDetail detail = timeLogEntry.StartDetail;

            if (entryType == TimeLogEntryType_Return || entryType == TimeLogEntryType_EndPoint) {
                timeLogEntry = (HOSTimeLogEntry) this.routeEntries.get(this.routeEntries.size() - 3);
                detail = timeLogEntry.StartDetail;
            }

            detail.AddressComponents.Address1 = address.Address1;
            detail.AddressComponents.City = address.City;
            detail.AddressComponents.Province = address.Province;
            detail.AddressComponents.Country = address.Country;
            detail.AddressComponents.Postal = address.Postal;

            // Skip switching logging updated event type
            if (timeLogEntry.timeLogEntryType == entryType) return;

            ArrayList arrTypeEntries = this.modifiedEntries;
            String[][] eventIdentifier = {
                    {TimeLogEntryType_Departure.name(),HOSDepartureText},
                    {TimeLogEntryType_Return.name(),HOSReturnText},
                    {TimeLogEntryType_StartPoint.name(),HOSStartPointText},
                    {TimeLogEntryType_EndPoint.name(),HOSEndPointText}

            };
//        {
//                    TimeLogEntryType_Departure[HOSDepartureText],
//                    [TimeLogEntryType_Return] = HOSReturnText,
//                    [TimeLogEntryType_StartPoint] =HOSStartPointText,
//                    [TimeLogEntryType_EndPoint] =HOSEndPointText
//                };

            timeLogEntry.Label = eventIdentifier[entryType.getValue()].toString();
            timeLogEntry.timeLogEntryType = entryType;

            HOSModifiedTimeLogEntry modifiedEntry = new HOSModifiedTimeLogEntry();

            modifiedEntry.typeIdentifier = ModifiedEntryType_ManualStartEventTime;
            modifiedEntry.typeEntry = timeLogEntry.timeLogEntryType;
            modifiedEntry.entry = timeLogEntry;
            modifiedEntry.refxId = timeLogEntry.StopId;
            modifiedEntry.auditBy = this.loggedUsername;
            modifiedEntry.auditDate = this.dateToday;

            if (entryType == TimeLogEntryType_Return || entryType == TimeLogEntryType_EndPoint) {
                modifiedEntry.typeIdentifier = ModifiedEntryType_ManualEndEventTime;
            }

            if (arrTypeEntries.size() > 0) {
                int identifier = modifiedEntry.typeIdentifier.getValue();

                // Handle duplicate entries by checking if exist
                if (arrTypeEntries.contains(identifier)) {
                    int index = arrTypeEntries.indexOf(identifier);

                    this.modifiedEntries.remove(index);
                }
            }

          this.modifiedEntries.add(modifiedEntry);
        }


        public void updateInfoDetail(){}


        public void removeEntryAtIndex(int index){
            HOSTimeLogEntry timeLogEntry = (HOSTimeLogEntry) this.routeEntries.get(index);

            this.routeEntries = HOSUtils.removeEntryAtIndex(index) ;

                insertModifiedTimeLogEntry(timeLogEntry);

        }

        public boolean entriesContainsObject(Objects object) {
        if (this.routeEntries.size() > 0) {
            if (this.routeEntries.contains(object))
            return true;
        }

        return false;
    }








}
