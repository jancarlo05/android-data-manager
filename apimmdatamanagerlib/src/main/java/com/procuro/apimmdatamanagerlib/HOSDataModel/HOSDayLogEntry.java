package com.procuro.apimmdatamanagerlib.HOSDataModel;

import com.procuro.apimmdatamanagerlib.CompartmentDTO;
import com.procuro.apimmdatamanagerlib.PimmBaseObject;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by jophenmarieweeks on 11/10/2017.
 */

public class HOSDayLogEntry extends PimmBaseObject {

    public Date StartDate;
    public Date EndDate;
    public List<HOSRouteInfo> RouteInfoList; // Array of HOSRouteInfo
    public List<HOSTimeLogEntry> TimeLogList; // Array of HOSTimeLogEntry
    public List<HOSTimeLogEntry> CombinedTimeLogList;


    @Override
    public void setValueForKey(Object value, String key)
    {
        if(key.equalsIgnoreCase("RouteInfoList"))
        {
            if(this.RouteInfoList == null) {
                this.RouteInfoList = new ArrayList<HOSRouteInfo>();
            }
            if (!value.equals(null)) {
                if (value instanceof JSONArray) {

                    JSONArray arrData = (JSONArray) value;

                    if (arrData != null) {
                        for (int i = 0; i < arrData.length(); i++) {
                            try {
                                HOSRouteInfo hosRouteInfo = new HOSRouteInfo();
                                hosRouteInfo.readFromJSONObject(arrData.getJSONObject(i));

                                this.RouteInfoList.add(hosRouteInfo);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                    }

                }
            }
        }else if(key.equalsIgnoreCase("TimeLogList"))
        {
            if(this.TimeLogList == null) {
                this.TimeLogList = new ArrayList<HOSTimeLogEntry>();
            }
            if (!value.equals(null)) {
                if (value instanceof JSONArray) {

                    JSONArray arrData = (JSONArray) value;

                    if (arrData != null) {
                        for (int i = 0; i < arrData.length(); i++) {
                            try {
                                HOSTimeLogEntry hosTimeLogEntry = new HOSTimeLogEntry();
                                hosTimeLogEntry.readFromJSONObject(arrData.getJSONObject(i));

                                this.TimeLogList.add(hosTimeLogEntry);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                    }

                }
            }
        }else if(key.equalsIgnoreCase("CombinedTimeLogList"))
        {
            if(this.CombinedTimeLogList == null) {
                this.CombinedTimeLogList = new ArrayList<HOSTimeLogEntry>();
            }
            if (!value.equals(null)) {
                if (value instanceof JSONArray) {

                    JSONArray arrData = (JSONArray) value;

                    if (arrData != null) {
                        for (int i = 0; i < arrData.length(); i++) {
                            try {
                                HOSTimeLogEntry hosTimeLogEntry = new HOSTimeLogEntry();
                                hosTimeLogEntry.readFromJSONObject(arrData.getJSONObject(i));

                                this.CombinedTimeLogList.add(hosTimeLogEntry);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                    }

                }
            }
        }
        else
        {
            super.setValueForKey(value, key);
        }
    }


}
