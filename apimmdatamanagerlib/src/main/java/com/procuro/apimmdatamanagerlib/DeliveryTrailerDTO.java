package com.procuro.apimmdatamanagerlib;

import android.text.BoringLayout;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jophenmarieweeks on 06/07/2017.
 */

public class DeliveryTrailerDTO extends PimmBaseObject {

    public String DCId;
    public String DCName;
    public String DeliveryId;
    public boolean DoorOpen;
    public boolean HasDoorSensors;
    public String TrailerId;
    public GPSValue LastGPS;
    public StoreDeliveryEnums.LifecycleState LifecycleState;
    public ArrayList<SdrMetric> Metrics;
    public boolean ReeferPower;
    public String RouteId;
    public String RouteName;
    public String TrailerName;
    public StoreDeliveryEnums.TransportStatus TransportStatus;
    public StoreDeliveryEnums.VehicleStatus VehicleStatus;

    @Override
    public void setValueForKey(Object value, String key) {

        if (key.equalsIgnoreCase("LastGPS")) {
            if (!value.equals(null)) {
                GPSValue result = new GPSValue();
                JSONObject jsonObject = (JSONObject) value;
                result.readFromJSONObject(jsonObject);
                this.LastGPS = result;
            }

        } else if (key.equalsIgnoreCase("Metrics")) {
            if (this.Metrics == null) {
                this.Metrics = new ArrayList<SdrMetric>();
            }

            if (!value.equals(null)) {
                JSONArray arrStops  = (JSONArray) value;

                if (arrStops != null) {
                    for (int i = 0; i < arrStops.length(); i++) {
                        try {
                            SdrMetric result = new SdrMetric();
                            result.readFromJSONObject(arrStops.getJSONObject(i));

                            this.Metrics.add(result);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }

                Log.v("Data","Value: "+this.Metrics);
            }
        }else if (key.equalsIgnoreCase("TransportStatus")) {
            if (!value.equals(null)) {
                StoreDeliveryEnums.TransportStatus trans = StoreDeliveryEnums.TransportStatus.setIntValue((int) value);
                this.TransportStatus = trans;

            }
        }else if (key.equalsIgnoreCase("LifecycleState")) {
            if (!value.equals(null)) {
                StoreDeliveryEnums.LifecycleState lifecycleState = StoreDeliveryEnums.LifecycleState.setIntValue((int) value);
                this.LifecycleState = lifecycleState;

            }

        }else if (key.equalsIgnoreCase("VehicleStatus")) {
            if (!value.equals(null)) {
                StoreDeliveryEnums.VehicleStatus vehicleStatus = StoreDeliveryEnums.VehicleStatus.setIntValue((int) value);
                this.VehicleStatus = vehicleStatus;
            }

        } else {
            super.setValueForKey(value, key);
        }
    }
}
