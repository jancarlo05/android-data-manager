package com.procuro.apimmdatamanagerlib;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by jophenmarieweeks on 04/09/2017.
 */

public class DriverLogTimeInfoDetails extends PimmBaseObject {


    public enum DriverLastStatusEnum
    {
        DriverLastStatusEnum_Undefined(0),
        DriverLastStatusEnum_StayOnDuty(1),
        DriverLastStatusEnum_GoOnBreak(2),
        DriverLastStatusEnum_GoHome(3),
        DriverLastStatusEnum_Sleeper(4);

        private int value;
        private DriverLastStatusEnum(int value)
        {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }

        public static DriverLastStatusEnum setIntValue (int i) {
            for (DriverLastStatusEnum type : DriverLastStatusEnum.values()) {
                if (type.value == i) { return type; }
            }
            return DriverLastStatusEnum_Undefined;
        }


    }

   public enum DriverStatusEnum
    {
        DriverStatusEnum_Undefined(0),
        DriverStatusEnum_OnDuty(1),
        DriverStatusEnum_OnBreak(2),
        DriverStatusEnum_Driving(3),
        DriverStatusEnum_Sleeper(4),
        DriverStatusEnum_PunchOut(5);

        private int value;
        private DriverStatusEnum(int value)
        {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }

        public static DriverStatusEnum setIntValue (int i) {
            for (DriverStatusEnum type : DriverStatusEnum.values()) {
                if (type.value == i) { return type; }
            }
            return DriverStatusEnum_Undefined;
        }


    }


    public DriverLogTimeInfoEntry Punch;
    public DriverLogTimeInfoEntry PreTrip;
    public DriverLogTimeInfoEntry PostTrip;
    public DriverLogTimeInfoEntry Dispatch;

    public DriverLogTimeInfoEntry LayOver;
    public ArrayList<DriverLogTimeInfoEntry> NonRouteOnDuty;

    public ArrayList<DriverLogRestBreakInfoEntry> RestStops; //Array of DriverLogRestBreakInfoEntry objects

    public ArrayList<DriverLogTimeInfoEntry> DriveSegments; //Array of DriverLogTimeInfoEntry objects

    public DriverLastStatusEnum LastStatus;
    public DriverStatusEnum DriverStatus;

    public ArrayList<AuditTrailEntry> AuditTrail;

    public ArrayList<DriverLogTimeInfoEntry> PunchTimes; //DriverLogTimeInfoEntry
    public ArrayList<DriverLogTimeInfoEntry> Sleeper; //DriverLogTimeInfoEntry


    @Override
    public void setValueForKey(Object value, String key) {

        if (key.equalsIgnoreCase("Punch")) {
            if (!value.equals(null)) {
                DriverLogTimeInfoEntry driverLogTimeInfoEntry = new DriverLogTimeInfoEntry();
                JSONObject jsonObject = (JSONObject) value;
                driverLogTimeInfoEntry.readFromJSONObject(jsonObject);

                this.Punch = driverLogTimeInfoEntry;
            }
        }else if (key.equalsIgnoreCase("PreTrip")) {
            if (!value.equals(null)) {
                DriverLogTimeInfoEntry driverLogTimeInfoEntry = new DriverLogTimeInfoEntry();
                JSONObject jsonObject = (JSONObject) value;
                driverLogTimeInfoEntry.readFromJSONObject(jsonObject);

                this.PreTrip = driverLogTimeInfoEntry;
            }
        }else if (key.equalsIgnoreCase("PostTrip")) {
            if (!value.equals(null)) {
                DriverLogTimeInfoEntry driverLogTimeInfoEntry = new DriverLogTimeInfoEntry();
                JSONObject jsonObject = (JSONObject) value;
                driverLogTimeInfoEntry.readFromJSONObject(jsonObject);

                this.PostTrip = driverLogTimeInfoEntry;
            }

        }else if (key.equalsIgnoreCase("Dispatch")) {
            if (!value.equals(null)) {
                DriverLogTimeInfoEntry driverLogTimeInfoEntry = new DriverLogTimeInfoEntry();
                JSONObject jsonObject = (JSONObject) value;
                driverLogTimeInfoEntry.readFromJSONObject(jsonObject);

                this.Dispatch = driverLogTimeInfoEntry;
            }
        }else if (key.equalsIgnoreCase("LayOver")) {
            if (!value.equals(null)) {
                DriverLogTimeInfoEntry driverLogTimeInfoEntry = new DriverLogTimeInfoEntry();
                JSONObject jsonObject = (JSONObject) value;
                driverLogTimeInfoEntry.readFromJSONObject(jsonObject);

                this.Dispatch = driverLogTimeInfoEntry;
            }
        }else if (key.equalsIgnoreCase("NonRouteOnDuty")) {
            if (!value.equals(null)) {
                if (this.NonRouteOnDuty == null) {
                    this.NonRouteOnDuty = new ArrayList<DriverLogTimeInfoEntry>();
                }

                if (!value.equals(null)) {
                    JSONArray arrResults = (JSONArray) value;

                    if (arrResults != null) {
                        for (int i = 0; i < arrResults.length(); i++) {
                            try {
                                DriverLogTimeInfoEntry driverLogTimeInfoEntry = new DriverLogTimeInfoEntry();
                                driverLogTimeInfoEntry.readFromJSONObject(arrResults.getJSONObject(i));

                                this.NonRouteOnDuty.add(driverLogTimeInfoEntry);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                }
            }

        }else if (key.equalsIgnoreCase("RestStops")) {
            if (!value.equals(null)) {
                if (this.RestStops == null) {
                    this.RestStops = new ArrayList<DriverLogRestBreakInfoEntry>();
                }

                if (!value.equals(null)) {
                    JSONArray arrResults = (JSONArray) value;

                    if (arrResults != null) {
                        for (int i = 0; i < arrResults.length(); i++) {
                            try {
                                DriverLogRestBreakInfoEntry driverLogRestBreakInfoEntry = new DriverLogRestBreakInfoEntry();
                                driverLogRestBreakInfoEntry.readFromJSONObject(arrResults.getJSONObject(i));

                                this.RestStops.add(driverLogRestBreakInfoEntry);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                }
            }

        }else if (key.equalsIgnoreCase("DriveSegments")) {
            if (!value.equals(null)) {
                if (this.DriveSegments == null) {
                    this.DriveSegments = new ArrayList<DriverLogTimeInfoEntry>();
                }

                if (!value.equals(null)) {
                    JSONArray arrResults = (JSONArray) value;

                    if (arrResults != null) {
                        for (int i = 0; i < arrResults.length(); i++) {
                            try {
                                DriverLogTimeInfoEntry driverLogTimeInfoEntry = new DriverLogTimeInfoEntry();
                                driverLogTimeInfoEntry.readFromJSONObject(arrResults.getJSONObject(i));

                                this.DriveSegments.add(driverLogTimeInfoEntry);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                }
            }

        }else if (key.equalsIgnoreCase("PunchTimes")) {
            if (!value.equals(null)) {
                if (this.PunchTimes == null) {
                    this.PunchTimes = new ArrayList<DriverLogTimeInfoEntry>();
                }

                if (!value.equals(null)) {
                    JSONArray arrResults = (JSONArray) value;

                    if (arrResults != null) {
                        for (int i = 0; i < arrResults.length(); i++) {
                            try {
                                DriverLogTimeInfoEntry driverLogTimeInfoEntry = new DriverLogTimeInfoEntry();
                                driverLogTimeInfoEntry.readFromJSONObject(arrResults.getJSONObject(i));

                                this.PunchTimes.add(driverLogTimeInfoEntry);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                }
            }
        }else if (key.equalsIgnoreCase("Sleeper")) {
            if (!value.equals(null)) {
                if (this.Sleeper == null) {
                    this.Sleeper = new ArrayList<DriverLogTimeInfoEntry>();
                }

                if (!value.equals(null)) {
                    JSONArray arrResults = (JSONArray) value;

                    if (arrResults != null) {
                        for (int i = 0; i < arrResults.length(); i++) {
                            try {
                                DriverLogTimeInfoEntry driverLogTimeInfoEntry = new DriverLogTimeInfoEntry();
                                driverLogTimeInfoEntry.readFromJSONObject(arrResults.getJSONObject(i));

                                this.Sleeper.add(driverLogTimeInfoEntry);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                }
            }
        } else if (key.equalsIgnoreCase("LastStatus")) {

            if (!value.equals(null)) {
                DriverLastStatusEnum driverLastStatusEnum = DriverLastStatusEnum.setIntValue((int) value);
                this.LastStatus = driverLastStatusEnum;
            }
        } else if (key.equalsIgnoreCase("DriverStatus")) {

            if (!value.equals(null)) {
                DriverStatusEnum driverStatusEnum = DriverStatusEnum.setIntValue((int) value);
                this.DriverStatus = driverStatusEnum;
            }
        }else if (key.equalsIgnoreCase("AuditTrail")) {
            if (!value.equals(null)) {
                if (this.AuditTrail == null) {
                    this.AuditTrail = new ArrayList<AuditTrailEntry>();
                }

                if (!value.equals(null)) {
                    JSONArray arrResults = (JSONArray) value;

                    if (arrResults != null) {
                        for (int i = 0; i < arrResults.length(); i++) {
                            try {
                                AuditTrailEntry auditTrailEntry = new AuditTrailEntry();
                                auditTrailEntry.readFromJSONObject(arrResults.getJSONObject(i));

                                this.AuditTrail.add(auditTrailEntry);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                }
            }
        }else {
            super.setValueForKey(value, key);
        }
    }

    public double getTotalBreakTime(){
        double totalBreakTime = 0;
        if(this.RestStops != null && this.RestStops.size() > 0){
            for (DriverLogRestBreakInfoEntry restStop : this.RestStops){
                if(restStop.Start != null && restStop.End != null) {
                    Date startTime = DMUtils.truncateSecondsForDate(restStop.Start);
                    Date endTime = DMUtils.truncateSecondsForDate(restStop.End);
                    double breakTime = endTime.getTime() - startTime.getTime();
                    totalBreakTime = totalBreakTime + breakTime;
                    Log.v("DM","Added Break Time: "+ breakTime +" for Rest Break From "+restStop.StartText+" to " + restStop.EndText);
                }else{
                    Log.e("DM","Unclosed Rest Break found starting at "+restStop.StartText);


                }

            }
            return totalBreakTime;
        }else{
            Log.e("DM","No Rest Stops Found, Total BreakTime is:0");
            return totalBreakTime;
        }

    }
    public  double getTotalSleeperTime(){
        double totalSleeperTime = 0;
        if(this.Sleeper != null && this.Sleeper.size() > 0){
            for (DriverLogTimeInfoEntry sleeperEntry : this.Sleeper){
                if(sleeperEntry.Start != null && sleeperEntry.End != null) {
                    Date startTime = DMUtils.truncateSecondsForDate(sleeperEntry.Start);
                    Date endTime = DMUtils.truncateSecondsForDate(sleeperEntry.End);
                    double sleeperTime = endTime.getTime() - startTime.getTime();
                    totalSleeperTime += sleeperTime;
                    Log.v("DM","Added Sleeper Time: "+ sleeperTime +" for Sleeper Break From "+sleeperEntry.StartText+" to " + sleeperEntry.EndText);
                }else{
                    Log.e("DM","Unclosed Rest Break found starting at "+sleeperEntry.StartText);


                }

            }
            return totalSleeperTime;
        }else{
            Log.e("DM","No Rest Stops Found, Total Sleeper Time is:0");
            return totalSleeperTime;
        }

    }

    public double getTotalDrivingTime(){
        double totalDrivingTime = 0;
        if(this.DriveSegments != null && this.DriveSegments.size() > 0){
            for (DriverLogTimeInfoEntry drivingEntry : this.Sleeper){
                if(drivingEntry.Start != null && drivingEntry.End != null) {
                    Date startTime = DMUtils.truncateSecondsForDate(drivingEntry.Start);
                    Date endTime = DMUtils.truncateSecondsForDate(drivingEntry.End);

                    double drivingTime = endTime.getTime() - startTime.getTime();
                    totalDrivingTime = totalDrivingTime + drivingTime ;
                    Log.v("DM","Added Driving Time: "+ drivingTime +" for DriveSegment From "+drivingEntry.StartText+" to " + drivingEntry.EndText);
                }else{
                    Log.e("DM","Unclosed DriveSegment found starting at "+ drivingEntry.StartText);


                }

            }
            return totalDrivingTime;
        }else{
            Log.e("DM","No Rest Stops Found, Total Sleeper Time is:0");
            return totalDrivingTime;
        }

    }

    public Date getLastOpenBreakStartTime(){
        Date restsTop = null;
        if(this.RestStops != null && this.RestStops.size() > 0){
            for(DriverLogRestBreakInfoEntry restStop : this.RestStops){
                if(restStop.Start != null && restStop.End != null) {
                    Log.v("DM", "Unclosed Rest Break found starting at: " + restStop.StartText);
                    restsTop = restStop.Start;
                }else{
                    restsTop = null;
                }
            }

        }else {
            restsTop =  null;
        }

        return restsTop;
    }


    public DriverLogRestBreakInfoEntry getLastOpenRestBreakEntry(){
        String restsTop = null;
        if(this.RestStops != null && this.RestStops.size() > 0){
            for(DriverLogRestBreakInfoEntry restStop : this.RestStops){
                if(restStop.Start != null && restStop.End != null) {
                    Log.v("DM", "Unclosed Rest Break found starting at: " + restStop.StartText);
//                    restsTop =  restStop.StartText;
                    return restStop;
                }else{
//                    restsTop = null;
                    return  null;
                }
            }

        }
//            restsTop =  null;
            return null;

//        return restsTop;

    }

    public Object dictionaryWithValuesForKeys() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();

        if(PunchTimes.size() > 0){
            ArrayList<HashMap<String, Object>> PunchArr = new ArrayList<>();

            for(DriverLogTimeInfoEntry punchObject : this.PunchTimes)
            {
                HashMap<String, Object> objDic = punchObject.dictionaryWithValuesForKeys();
                PunchArr.add(objDic);
            }

            dictionary.put("PunchTimes", PunchArr);

        }else{
            dictionary.put("PunchTimes", null);
        }


        if(NonRouteOnDuty.size() > 0){
            ArrayList<HashMap<String, Object>> NonRouteOnDutyArr = new ArrayList<>();

            for(DriverLogTimeInfoEntry NonRouteOnDutyObject : this.NonRouteOnDuty)
            {
                HashMap<String, Object> objDic = NonRouteOnDutyObject.dictionaryWithValuesForKeys();
                NonRouteOnDutyArr.add(objDic);
            }

            dictionary.put("NonRouteOnDuty", NonRouteOnDutyArr);

        }else{
            dictionary.put("NonRouteOnDuty", null);
        }

        if(Sleeper.size() > 0){
            ArrayList<HashMap<String, Object>> SleeperArr = new ArrayList<>();

            for(DriverLogTimeInfoEntry SleeperObject : this.Sleeper)
            {
                HashMap<String, Object> objDic = SleeperObject.dictionaryWithValuesForKeys();
                SleeperArr.add(objDic);
            }

            dictionary.put("Sleeper", SleeperArr);

        }else{
            dictionary.put("Sleeper", null);
        }

        if(DriveSegments.size() > 0){
            ArrayList<HashMap<String, Object>> DriveSegmentsArr = new ArrayList<>();

            for(DriverLogTimeInfoEntry DriveSegmentsObject : this.DriveSegments)
            {
                HashMap<String, Object> objDic = DriveSegmentsObject.dictionaryWithValuesForKeys();
                DriveSegmentsArr.add(objDic);
            }

            dictionary.put("DriveSegments", DriveSegmentsArr);

        }else{
            dictionary.put("DriveSegments", null);
        }

        if(RestStops.size() > 0){
            ArrayList<HashMap<String, Object>> RestStopsArr = new ArrayList<>();

            for(DriverLogRestBreakInfoEntry RestStopsObject : this.RestStops)
            {
                HashMap<String, Object> objDic = RestStopsObject.dictionaryWithValuesForKeys();
                RestStopsArr.add(objDic);
            }

            dictionary.put("RestStops", RestStopsArr);

        }else{
            dictionary.put("RestStops", null);
        }

        if(AuditTrail.size() > 0){
            ArrayList<HashMap<String, Object>> AuditTrailArr = new ArrayList<>();

            for(AuditTrailEntry AuditTrailObject : this.AuditTrail)
            {
                HashMap<String, Object> objDic = AuditTrailObject.dictionaryWithValuesForKeys();
                AuditTrailArr.add(objDic);
            }

            dictionary.put("AuditTrail", AuditTrailArr);

        }else{
            dictionary.put("AuditTrail", null);
        }

        dictionary.put("PreTrip", (this.PreTrip == null) ? null : this.PreTrip.dictionaryWithValuesForKeys());
        dictionary.put("PostTrip", (this.PostTrip == null) ? null : this.PostTrip.dictionaryWithValuesForKeys());
        dictionary.put("Dispatch", (this.Dispatch == null) ? null : this.Dispatch.dictionaryWithValuesForKeys());
        dictionary.put("LayOver", (this.LayOver == null) ? null : this.LayOver.dictionaryWithValuesForKeys());
        dictionary.put("Punch", (this.Punch == null) ? null : this.Punch.dictionaryWithValuesForKeys());

        if(LastStatus == DriverLastStatusEnum.DriverLastStatusEnum_StayOnDuty){
            dictionary.put("LastStatus", 1);
        } else if(LastStatus == DriverLastStatusEnum.DriverLastStatusEnum_GoOnBreak){
            dictionary.put("LastStatus", 2);
        } else if(LastStatus == DriverLastStatusEnum.DriverLastStatusEnum_GoHome){
            dictionary.put("LastStatus", 3);
        } else if(LastStatus == DriverLastStatusEnum.DriverLastStatusEnum_Sleeper){
            dictionary.put("LastStatus", 4);
        }else{
            dictionary.put("LastStatus", LastStatus.getValue());
        }

        return dictionary;

    }



}
