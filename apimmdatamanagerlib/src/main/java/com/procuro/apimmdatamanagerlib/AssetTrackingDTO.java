package com.procuro.apimmdatamanagerlib;

import android.util.Log;

import java.util.Date;
import java.util.HashMap;

import static com.procuro.apimmdatamanagerlib.AssetTrackingDTO.DeviceStatusEnum.Status_OTR;

/**
 * Created by jophenmarieweeks on 12/07/2017.
 */

public class AssetTrackingDTO extends PimmBaseObject {

  public enum DeviceStatusEnum {
        Status_OTR(0),
        Status_OOS(1),
        Status_AtSite(2);

        private int value;
        private DeviceStatusEnum(int value) {
            this.value = value;
        }

      public int getValue() {
          return this.value;
      }

      public static DeviceStatusEnum setIntValue (int i) {
          for (DeviceStatusEnum type : DeviceStatusEnum.values()) {
              if (type.value == i) { return type; }
          }
          return Status_OTR;
      }

    }


    public Date startTime;
    public Date endTime;  //optional, If null this is the current status
    public DeviceStatusEnum status;
    public String siteId; // if null, the vehicle was not a known site during this time

    public String deviceId;
    public String deviceName;
    public String siteName;

    @Override
    public void setValueForKey(Object value, String key) {

        if(key.equalsIgnoreCase("status")) {
            if (!value.equals(null)) {
                DeviceStatusEnum treshold = DeviceStatusEnum.setIntValue((int) value);
                this.status = treshold;
            }

        } else {
            super.setValueForKey(value, key);
        }
    }

    public HashMap<String, Object> dictionaryWithValuesForKeys(){

        HashMap<String, Object> dictionary = new HashMap<String, Object>();

        dictionary.put("startTime", this.startTime);
        dictionary.put("endTime", this.endTime);
        dictionary.put("siteId", this.siteId);
        dictionary.put("siteId", this.siteId);
        dictionary.put("deviceId", this.deviceId);
        dictionary.put("deviceName", this.deviceName);
        dictionary.put("siteName", this.siteName);
        dictionary.put("status", (this.status == null) ? Status_OTR : this.status.value);


        return dictionary;
    }
//-(void) print;

    public void print(){
        HashMap<String, Object> dictionary = this.dictionaryWithValuesForKeys();
        Object object = (Object) dictionary;
        Log.v("DM", "AssetTrackingDTO: " + object.toString());

    }

}
