package com.procuro.apimmdatamanagerlib;

import java.util.Date;

/**
 * Created by jophenmarieweeks on 09/10/2017.
 */

public class DriverPerformanceDTO extends PimmBaseObject {


    public String driverPerformanceID;

    public String userID;
    public String shipmentID;

    public Date timestamp;

    public Date preTripStart;
    public Date preTripEnd;
    public Date postTripStart;
    public Date postTripEnd;


}
