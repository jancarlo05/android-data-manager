package com.procuro.apimmdatamanagerlib;

/**
 * Created by jophenmarieweeks on 12/07/2017.
 */

public class SdDriverStatus extends PimmBaseObject {

    // start: inherited from SdDriverQualificationSummary
    //     This part is identical to SdDriverQualificationSummary and on the server
    //     SdDriverStatus actually extends SdDriverQualificationSummary.
    public String UserId;
    public String Username;
    public String FirstName;
    public String LastName;
    public String EmployeeId;

    public int  MandatoryRequested;
    public int  MandatorySubmitted;
    public int  MandatoryPending;
    public int  MandatoryAccepted;
    public int  MandatoryRejected;
    //public int  MandatoryDeleted;
    public int  MandatoryMailed;
    public int  MandatoryMissing;
    public int  MandatoryExpired;

    public int  OptionalRequested;
    public int  OptionalSubmitted;
    public int  OptionalPending;
    public int  OptionalAccepted;
    public int  OptionalRejected;
    //public int  OptionalDeleted;
    public int  OptionalMailed;
    public int  OptionalMissing;
    public int  OptionalExpired;
    // end: inherited from SdDriverQualificationSummary

    public String DeliveryId;
    public String TrailerId;
    public String TrailerName;
    public String TractorId;
    public String TractorName;

    public String RouteName;
    public StoreDeliveryEnums.DeliveryStatus DeliveryStatus;
    public StoreDeliveryEnums.TransportStatus DeliveryTransportStatus;
    public int DeliveryFlagStatus; // Need to replace NSint with EventPrioroty Enum
    public Boolean DeliveryTemperatureOK;
    public Boolean OnTheRoad;
    public DeliveryPOITypeEnum.DeliveryPOIStatusEnum AtLocation;
    public StoreDeliveryEnums.DoorAccessEnum DoorStatus;

    @Override
    public void setValueForKey(Object value, String key) {

        if(key.equalsIgnoreCase("DeliveryTransportStatus")) {
            if (!value.equals(null)) {
                StoreDeliveryEnums.TransportStatus trans = StoreDeliveryEnums.TransportStatus.setIntValue((int) value);
                this.DeliveryTransportStatus = trans;
            }

        }else if(key.equalsIgnoreCase("AtLocation")){
            if (!value.equals(null)) {
                DeliveryPOITypeEnum.DeliveryPOIStatusEnum lifecycle = DeliveryPOITypeEnum.DeliveryPOIStatusEnum.setIntValue((int) value);
                this.AtLocation = lifecycle;
            }

        }else if(key.equalsIgnoreCase("DeliveryStatus")) {
            if (!value.equals(null)) {
                StoreDeliveryEnums.DeliveryStatus delivery = StoreDeliveryEnums.DeliveryStatus.setIntValue((int) value);
                this.DeliveryStatus = delivery;
            }

        }else if(key.equalsIgnoreCase("DoorStatus")) {
            if (!value.equals(null)) {
                StoreDeliveryEnums.DoorAccessEnum DoorAccess = StoreDeliveryEnums.DoorAccessEnum.setIntValue((int) value);
                this.DoorStatus = DoorAccess;
            }

        } else {
            super.setValueForKey(value, key);
        }
    }



}
