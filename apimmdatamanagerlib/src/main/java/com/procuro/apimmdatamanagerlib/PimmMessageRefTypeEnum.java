package com.procuro.apimmdatamanagerlib;

/**
 * Created by jophenmarieweeks on 06/07/2017.
 */

public class PimmMessageRefTypeEnum extends PimmBaseObject {

   //public enum PimmMessageRefTypeEnum // duplicate class error
    public enum PimmMessageRefTypesEnum
    {
        PimmMessageRefType_Undefined(1),
        PimmMessageRefType_User(0),
        PimmMessageRefType_Shipment(1),
        PimmMessageRefType_Device(2),
        PimmMessageRefType_Site(3),
        PimmMessageRefType_Customer(4),
        PimmMessageRefType_PimmForm(5),
        PimmMessageRefType_PimmMessage(6);

        private int value;
        private PimmMessageRefTypesEnum(int value)
        {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }

        public static PimmMessageRefTypesEnum setIntValue (int i) {
            for (PimmMessageRefTypesEnum type : PimmMessageRefTypesEnum.values()) {
                if (type.value == i) { return type; }
            }
            return PimmMessageRefType_Undefined;
        }

    }
 public PimmMessageRefTypesEnum pimmMessageRefTypeEnum;

    @Override
    public void setValueForKey(Object value, String key) {

        if(key.equalsIgnoreCase("pimmMessageRefTypeEnum")) {
            if (!value.equals(null)) {
                PimmMessageRefTypesEnum messageRefTypeEnum = PimmMessageRefTypesEnum.setIntValue((int) value);
                this.pimmMessageRefTypeEnum = messageRefTypeEnum;
            }

        } else {
            super.setValueForKey(value, key);
        }
    }
}
