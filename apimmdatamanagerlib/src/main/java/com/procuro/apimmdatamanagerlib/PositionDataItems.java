package com.procuro.apimmdatamanagerlib;



import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.util.ArrayList;
import java.util.HashMap;

public class PositionDataItems extends PimmBaseObject{

   public int width;
   public int height;
   public int ycoordinate;
   public int xcoordinate;
   public int position;
   public int layoutType;
   public float angle;
   public int totalEmployees;
   public int daypart;

   public boolean opsLeader;
   public boolean headphone;

   public String buildingType;
   public String color;
   public String kitchenType;

   public String positionName;
   public ArrayList<String> cleaningResponsibilities;
   public ArrayList<String> positionResponsibilities;


    @Override
    public void setValueForKey(Object value, String key) {
        super.setValueForKey(value, key);

        if (key.equalsIgnoreCase("angle")){
            if (value!=null){
                if (!value.toString().equalsIgnoreCase("null")){
                    this.angle = Float.parseFloat(value.toString());this.angle = Float.parseFloat(value.toString());
                }
            }
        }
        if (key.equalsIgnoreCase("daypart")){
            this.daypart = (int) value;
        }
        if (key.equalsIgnoreCase("height")){
            this.height = (int) value;
        }
        if (key.equalsIgnoreCase("width")){
            this.width = (int) value;
        }
        if (key.equalsIgnoreCase("ycoordinate")){
            this.ycoordinate = (int) value;
        }
        if (key.equalsIgnoreCase("xcoordinate")){
            this.xcoordinate = (int) value;
        }
        if (key.equalsIgnoreCase("position")){
            this.position = (int) value;
        }
        if (key.equalsIgnoreCase("layoutType")){
            this.layoutType = (int) value;
        }
        if (key.equalsIgnoreCase("totalEmployees")){
            this.totalEmployees = (int) value;
        }
        if (key.equalsIgnoreCase("opsLeader")){
            this.opsLeader = (boolean) value;
        }
        if (key.equalsIgnoreCase("headphone")){
            this.headphone = (boolean) value;
        }
        if (key.equalsIgnoreCase("buildingType")){
            this.buildingType = (String) value;
        }
        if (key.equalsIgnoreCase("color")){
            this.color = (String) value;
        }

        if (key.equalsIgnoreCase("kitchenType")){
            this.kitchenType = (String) value;
        }

        if (key.equalsIgnoreCase("positionName")){
            this.positionName = (String) value;
        }

        if (key.equalsIgnoreCase("buildingType")){
            this.buildingType = (String) value;
        }

        if (key.equalsIgnoreCase("cleaningResponsibilities")){

            ArrayList<String>strings = new ArrayList<>();
            if (value!=null){
                String data = value.toString();
                Object json = null;
                try {
                    json = new JSONTokener(data).nextValue();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if (json instanceof JSONObject){
                    //you have an object

                } else if (json instanceof JSONArray){
                    //you have an array
                    JSONArray jsonArray = (JSONArray) value;
                    for (int i = 0; i <jsonArray.length() ; i++) {
                        try {
                            strings.add((String) jsonArray.get(i));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            this.cleaningResponsibilities = strings;
        }

        if (key.equalsIgnoreCase("positionResponsibilities")){
            ArrayList<String>strings = new ArrayList<>();

            if (value!=null){
                try {
                    JSONArray jsonArray = (JSONArray) value;
                    for (int i = 0; i <jsonArray.length() ; i++) {
                        try {
                            strings.add((String) jsonArray.get(i));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }

            }
            this.positionResponsibilities = strings;
        }


    }

    public HashMap<String,Object> dictionaryWithValuesForKeys() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();

        dictionary.put("height",this.height);
        dictionary.put("width",this.width);
        dictionary.put("ycoordinate",this.ycoordinate);
        dictionary.put("xcoordinate",this.xcoordinate);
        dictionary.put("position",this.position);
        dictionary.put("layoutType",this.layoutType);
        dictionary.put("totalEmployees",this.totalEmployees);
        dictionary.put("daypart",this.daypart);
        dictionary.put("opsLeader",this.opsLeader);
        dictionary.put("headphone",this.headphone);
        dictionary.put("buildingType",this.buildingType);
        dictionary.put("color",this.color);
        dictionary.put("kitchenType",this.kitchenType);
        dictionary.put("positionName",this.positionName);
        dictionary.put("cleaningResponsibilities",this.cleaningResponsibilities);
        dictionary.put("positionResponsibilities",this.positionResponsibilities);

        return dictionary;
    }





}
