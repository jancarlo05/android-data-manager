package com.procuro.apimmdatamanagerlib;

import java.util.HashMap;

/**
 * Created by jophenmarieweeks on 15/08/2017.
 */

public class AssetTrackingRegionMarkerDTO extends PimmBaseObject {
    public double lat;
    public double lon;

    public HashMap<String,Object> dictionaryWithValuesForKeys() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();

        dictionary.put("lat",this.lat);
        dictionary.put("lon",this.lon);

        return dictionary;
    }
}
