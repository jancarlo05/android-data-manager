package com.procuro.apimmdatamanagerlib;

import java.util.Date;

/**
 * Created by jophenmarieweeks on 10/07/2017.
 */

public class Instance_Performance extends PimmBaseObject {

   public String deviceID ;
   public String deviceName ;
   public String Description ;
   public String instanceID ;
   public Date timestamp;
   public double percentInThreshold;
   public double percentInService;
   public double eventType;      // if null, no event typess are configured for this instance

}
