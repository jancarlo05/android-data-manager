package com.procuro.apimmdatamanagerlib;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

public class SMSCleaningMonthly extends  PimmBaseObject {

    public String verifiedBy;
    public String formID;
    public String deviceType;
    public double version;
    public Date startDateISO;
    public Date verifiedDateISO;
    public Date endDateISO;
    public Date endDate;
    public Date startDate;
    public Date verifiedDate;
    public Date creationDateISO;
   public ArrayList<SMSItem>customerViewItems;
    public ArrayList<SMSItem>productionItems;
    public ArrayList<SMSItem>serviceItems;
    public ArrayList<SMSItem>backroomItems;


    @Override
    public void setValueForKey(Object value, String key) {
        super.setValueForKey(value, key);


        if (key.equalsIgnoreCase("backroomItems")){
            ArrayList<SMSItem>Items = new ArrayList<>();
            if (value!=null) {
                if (!value.toString().equalsIgnoreCase("null")){
                    JSONArray jsonArray = (JSONArray) value;
                    for (int i = 0; i < jsonArray.length(); i++) {
                        SMSItem item = new SMSItem();
                        try {
                            item.readFromJSONObject(jsonArray.getJSONObject(i));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Items.add(item);
                    }
                }
            }
            this.backroomItems = Items;
        }


        if (key.equalsIgnoreCase("serviceItems")){
            ArrayList<SMSItem>Items = new ArrayList<>();
            if (value!=null) {
                if (!value.toString().equalsIgnoreCase("null")){
                    JSONArray jsonArray = (JSONArray) value;
                    for (int i = 0; i < jsonArray.length(); i++) {
                        SMSItem item = new SMSItem();
                        try {
                            item.readFromJSONObject(jsonArray.getJSONObject(i));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Items.add(item);
                    }
                }
            }
            this.serviceItems = Items;
        }

        if (key.equalsIgnoreCase("productionItems")){
            ArrayList<SMSItem>Items = new ArrayList<>();
            if (value!=null) {
                if (!value.toString().equalsIgnoreCase("null")){
                    JSONArray jsonArray = (JSONArray) value;
                    for (int i = 0; i < jsonArray.length(); i++) {
                        SMSItem item = new SMSItem();
                        try {
                            item.readFromJSONObject(jsonArray.getJSONObject(i));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Items.add(item);
                    }
                }
            }
            this.productionItems = Items;
        }


        if (key.equalsIgnoreCase("customerViewItems")){
            ArrayList<SMSItem>Items = new ArrayList<>();
            if (value!=null) {
                if (!value.toString().equalsIgnoreCase("null")){
                    JSONArray jsonArray = (JSONArray) value;
                    for (int i = 0; i < jsonArray.length(); i++) {
                        SMSItem item = new SMSItem();
                        try {
                            item.readFromJSONObject(jsonArray.getJSONObject(i));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Items.add(item);
                    }
                }
            }
                this.customerViewItems = Items;
        }



        if (key.equalsIgnoreCase("verifiedBy")){
            if (value!=null){
                if (!value.toString().equalsIgnoreCase("null")){
                    this.verifiedBy = (String)value.toString();
                }
            }

        }
        if (key.equalsIgnoreCase("deviceType")){
            if (value!=null){
                if (!value.toString().equalsIgnoreCase("null")){
                    this.deviceType = (String)value.toString();
                }
            }

        }
        if (key.equalsIgnoreCase("version")){
            if (value!=null){
                if (!value.toString().equalsIgnoreCase("null")){
                    this.version = (double) value;
                }
            }

        }
        if (key.equalsIgnoreCase("endDateISO")){
            if (value!=null){
                this.endDateISO = JSONDate.convertJSONDateToNSDate(value.toString());
            }
        }
        if (key.equalsIgnoreCase("endDate")){
            if (value!=null){
                this.endDate = JSONDate.convertJSONDateToNSDate(value.toString());
            }
        }
        if (key.equalsIgnoreCase("startDateISO")){
            if (value!=null){
                this.startDateISO = JSONDate.convertJSONDateToNSDate(value.toString());
            }
        }
        if (key.equalsIgnoreCase("verifiedDateISO")){
            if (value!=null){
                this.verifiedDateISO = JSONDate.convertJSONDateToNSDate(value.toString());
            }
        }

        if (key.equalsIgnoreCase("formID")){
            if (value!=null){
                if (!value.toString().equalsIgnoreCase("null")){
                    this.formID = (String)value.toString();
                }
            }
        }
        if (key.equalsIgnoreCase("startDate")){
            if (value!=null){
                this.startDate = JSONDate.convertJSONDateToNSDate(value.toString());
            }
        }
        if (key.equalsIgnoreCase("verifiedDate")){
            if (value!=null){
                this.verifiedDate = JSONDate.convertJSONDateToNSDate(value.toString());
            }
        }
        if (key.equalsIgnoreCase("creationDateISO")){
            if (value!=null){
                this.creationDateISO = JSONDate.convertJSONDateToNSDate(value.toString());
            }
        }


    }


    public HashMap<String,Object> dictionaryWithValuesForKeys() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();
        dictionary.put("verifiedBy", this.verifiedBy);
        dictionary.put("version", this.version);
        dictionary.put("endDateISO", this.endDateISO);
        dictionary.put("endDate", this.endDate);
        dictionary.put("startDateISO", this.startDateISO);
        dictionary.put("verifiedDateISO", this.verifiedDateISO);
        dictionary.put("formID", this.formID);
        dictionary.put("startDate", this.startDate);
        dictionary.put("verifiedDate", this.verifiedDate);
        dictionary.put("creationDateISO", this.creationDateISO);
        dictionary.put("deviceType", this.creationDateISO);

        dictionary.put("customerViewItems", this.customerViewItems);
        dictionary.put("productionItems", this.productionItems);
        dictionary.put("serviceItems", this.serviceItems);
        dictionary.put("backroomItems", this.backroomItems);

        return dictionary;
    }
}
