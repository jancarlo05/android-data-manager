package com.procuro.apimmdatamanagerlib;

/**
 * Created by jophenmarieweeks on 10/07/2017.
 */

public class SiteLocale extends PimmBaseObject {

    public String Culture;

    public LocalizationSettings.UnitSystemEnum UnitSystem;
    public LocalizationSettings.TemperatureUnitEnum TemperatureUnits;
    public String Id; //Mapped to 'Id' field in JSON object
    public String SiteId;
    public Integer timezoneId;
    @Override
    public void setValueForKey(Object value, String key) {

        if(key.equalsIgnoreCase("UnitSystem")) {
            if (!value.equals(null)) {
                LocalizationSettings.UnitSystemEnum unitSystem = LocalizationSettings.UnitSystemEnum.setIntValue((int) value);
                this.UnitSystem = unitSystem;
            }

        }else if(key.equalsIgnoreCase("TemperatureUnits")){
            if (!value.equals(null)) {
                LocalizationSettings.TemperatureUnitEnum temperatureUnitEnum = LocalizationSettings.TemperatureUnitEnum.setIntValue((int) value);
                this.TemperatureUnits = temperatureUnitEnum;
            }

        } else {
            super.setValueForKey(value, key);
        }
    }


}
