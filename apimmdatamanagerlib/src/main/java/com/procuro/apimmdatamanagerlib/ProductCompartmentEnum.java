package com.procuro.apimmdatamanagerlib;

/**
 * Created by jophenmarieweeks on 07/07/2017.
 */

public class ProductCompartmentEnum extends PimmBaseObject {

  //  enum ProductCompartmentEnum  // duplicate class error
    public enum ProductCompartmentsEnum
    {
        Compartment_Unmonitored(0),
        Compartment_Freezer(1),
        Compartment_Cooler(2),
        Compartment_Produce(3),
        Compartment_Dry(4);

        private int value;
        private ProductCompartmentsEnum(int value)
        {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }

        public static ProductCompartmentsEnum setIntValue (int i) {
            for (ProductCompartmentsEnum type : ProductCompartmentsEnum.values()) {
                if (type.value == i) { return type; }
            }
            return Compartment_Unmonitored;
        }
    }

    public ProductCompartmentsEnum productCompartmentsEnum;
    @Override
    public void setValueForKey(Object value, String key) {

        if(key.equalsIgnoreCase("productCompartmentsEnum")) {
            if (!value.equals(null)) {
                ProductCompartmentsEnum productCompartments = ProductCompartmentsEnum.setIntValue((int) value);
                this.productCompartmentsEnum = productCompartments;

            }

        } else {
            super.setValueForKey(value, key);
        }
    }

}
