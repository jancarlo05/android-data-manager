package com.procuro.apimmdatamanagerlib;

public class PimmThreshold extends PimmBaseObject {

   public enum PimmThresholdMode
    {
        PimmThresholdMode_Upper(0),
        PimmThresholdMode_Lower(1),
        PimmThresholdMode_Both(2),
        PimmThresholdMode_Off(0),
        PimmThresholdMode_Line(1),
        PimmThresholdMode_Range(2);

        private int value;
        private PimmThresholdMode(int value)
        {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }

        public static PimmThresholdMode setIntValue (int i) {
            for (PimmThresholdMode type : PimmThresholdMode.values()) {
                if (type.value == i) { return type; }
            }
            return PimmThresholdMode_Upper;
        }

    }
    public boolean enabled;
    public int mode;
    public boolean useCustomMessages;

   public int green;
   public int yellow;
   public int red;
   public int greenLow;
   public int yellowLow;
   public int redLow;

    public String greenMessage;
    public String yellowMessage;
    public String redMessage;
    public String greenLowMessage;
    public String yellowLowMessage;
    public String redLowMessage;

   public int targetMode;
   public int targetValue;
   public int targetUpperBound;
   public int targetLowerBound;
    public PimmThresholdMode pimmThresholdMode;
//- (void)encodeWithCoder:(NSCoder *)encoder;
//- (id)initWithCoder:(NSCoder *)decoder;

    @Override
    public void setValueForKey(Object value, String key) {

        if(key.equalsIgnoreCase("pimmThresholdMode")) {
            if (!value.equals(null)) {
                PimmThresholdMode treshold = PimmThresholdMode.setIntValue((int) value);
                this.pimmThresholdMode = treshold;
            }

        } else {
            super.setValueForKey(value, key);
        }
    }

}
