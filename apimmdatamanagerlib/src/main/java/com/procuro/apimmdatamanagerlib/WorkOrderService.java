package com.procuro.apimmdatamanagerlib;

import java.util.Date;

/**
 * Created by jophenmarieweeks on 10/07/2017.
 */

public class WorkOrderService extends PimmBaseObject {

    public String workOrderID ;           // parent WO
    public String workOrderServiceID ;    // PIMM GUID
    public String assetProfileID ;        // optional asset
    public String assigneeID ;            // optional assigned user
    public Date creationDate;               // when this WOS was created
    public Date scheduledStartDate;          // planned start
    public Date scheduledEndDate;           // planned end
    public Date startDate;                   // actual start
    public Date endDate;                    // actual end
    public int status;                          // current status
    public int service;                         // customer-defined service id
    public String instructions ;          // optional input from submitter
    public String notes ;
    public String reference ;


}
