package com.procuro.apimmdatamanagerlib;

import org.json.JSONObject;

import java.util.Date;

/**
 * Created by jophenmarieweeks on 11/07/2017.
 */

public class TractorPOI extends PimmBaseObject {

    public Date TimeRequested;
    public TractorPOIBuilder Data;

    @Override
    public void setValueForKey(Object value, String key) {
        if (key.equalsIgnoreCase("Data")) {
            TractorPOIBuilder tractorPOIBuilder = new TractorPOIBuilder();
            JSONObject jsonObject = (JSONObject) value;
            tractorPOIBuilder.readFromJSONObject(jsonObject);
            this.Data = tractorPOIBuilder;

        } else {
            super.setValueForKey(value, key);
        }
    }



}
