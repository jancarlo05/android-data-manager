package com.procuro.apimmdatamanagerlib;

import java.util.HashMap;

/**
 * Created by jophenmarieweeks on 10/07/2017.
 */

public class SdrRule extends PimmBaseObject {

    public enum ComplianceBounds
    {
        ComplianceBounds_Upper(0),
        ComplianceBounds_Lower(1);

        private int value;
        private ComplianceBounds(int value)
        {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }

        public static ComplianceBounds setIntValue (int i) {
            for (ComplianceBounds type : ComplianceBounds.values()) {
                if (type.value == i) { return type; }
            }
            return ComplianceBounds_Upper;
        }

    }

   public enum ComplianceAlgorithmType
    {
        ComplianceAlgorithmType_ConsecutiveMinutes(0),
        ComplianceAlgorithmType_AggregateMinutes(1),
        ComplianceAlgorithmType_AggregateTempMinutes(2),
        ComplianceAlgorithmType_ThresholdViolation(3),
        ComplianceAlgorithmType_ThresholdGuide(4),
        ComplianceAlgorithmType_Unmonitored(5);

        private int value;
        private ComplianceAlgorithmType(int value)
        {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }

        public static ComplianceAlgorithmType setIntValue (int i) {
            for (ComplianceAlgorithmType type : ComplianceAlgorithmType.values()) {
                if (type.value == i) { return type; }
            }
            return ComplianceAlgorithmType_ConsecutiveMinutes;
        }

    }

    public String RuleId;
    public String Name;
    public ComplianceBounds Bounds;
    public ComplianceAlgorithmType AlgorithmType;
    public String BoundsText;
    public String AlgorithmName;
    public String TriggerClause;
    public boolean MetricInclusive;
    public boolean RuleInclusive;
    public double MetricRed;
    public double MetricYellow;
    public double MetricGreen;
    public double RuleRed;
    public double RuleYellow;
    public double RuleGreen;
    public String MetricRedText;
    public String MetricYellowText;
    public String MetricGreenText;
    public String RuleRedText;
    public String RuleYellowText;
    public String RuleGreenText;
    public boolean IsTrigger;


    @Override
    public void setValueForKey(Object value, String key) {

        if (key.equalsIgnoreCase("Bounds")) {
            if (!value.equals(null)) {
                ComplianceBounds complianceBounds = ComplianceBounds.setIntValue((int) value);
                this.Bounds = complianceBounds;
            }
        }else if (key.equalsIgnoreCase("AlgorithmType")) {
            if (!value.equals(null)) {
                ComplianceAlgorithmType complianceAlgorithmType = ComplianceAlgorithmType.setIntValue((int) value);
                this.AlgorithmType = complianceAlgorithmType;
            }
        } else {
            super.setValueForKey(value, key);
        }
    }

}
