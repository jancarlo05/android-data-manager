package com.procuro.apimmdatamanagerlib;

import java.util.Date;

/**
 * Created by jophenmarieweeks on 06/07/2017.
 */

public class AssetProfile_TelematicsWirelessSensor extends PimmBaseObject {

    public String profileType;
    public String make;
    public String model;
    public String serial;
    public String hardwareVersion;
    public String softwareVersion;

}
