package com.procuro.apimmdatamanagerlib;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jophenmarieweeks on 12/07/2017.
 */

public class SdFleetVehicle extends PimmBaseObject {

    public ArrayList<SdrMetric> Metrics; // List of SdrMetric objects
    public GPSValue Location;

    public SdrMetricOnline GSMOnline;
    public SdrMetricOnline GPSOnline;
    public ArrayList<SdrMetricOnline> TemperaturesOnline;

    @Override
    public void setValueForKey(Object value, String key)
    {
        if(key.equalsIgnoreCase("Metrics")) {
            if (this.Metrics == null) {
                this.Metrics = new ArrayList<SdrMetric>();
            }

            if (!value.equals(null)) {
                JSONArray arrStops = (JSONArray) value;

                if (arrStops != null) {
                    for (int i = 0; i < arrStops.length(); i++) {
                        try {
                            SdrMetric result = new SdrMetric();
                            result.readFromJSONObject(arrStops.getJSONObject(i));

                            this.Metrics.add(result);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }

            }
        }else if(key.equalsIgnoreCase("TemperaturesOnline")) {
            if (this.TemperaturesOnline == null) {
                this.TemperaturesOnline = new ArrayList<SdrMetricOnline>();
            }

            if (!value.equals(null)) {
                JSONArray arrStops = (JSONArray) value;

                if (arrStops != null) {
                    for (int i = 0; i < arrStops.length(); i++) {
                        try {
                            SdrMetricOnline result = new SdrMetricOnline();
                            result.readFromJSONObject(arrStops.getJSONObject(i));

                            this.TemperaturesOnline.add(result);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }

            }
        } else if (key.equalsIgnoreCase("GSMOnline")) {
            if (value != null) {
                if (value instanceof JSONObject) {
                    SdrMetricOnline result = new SdrMetricOnline();
                    JSONObject jsonObject = (JSONObject) value;
                    result.readFromJSONObject(jsonObject);
                    this.GSMOnline = result;
                }
            }

        } else if (key.equalsIgnoreCase("GPSOnline")) {
            if (value != null) {
                if (value instanceof JSONObject) {
                    SdrMetricOnline result = new SdrMetricOnline();
                    JSONObject jsonObject = (JSONObject) value;
                    result.readFromJSONObject(jsonObject);
                    this.GPSOnline = result;
                }
            }
        } else if (key.equalsIgnoreCase("Location")) {
            if (value != null) {
                if (value instanceof JSONObject) {
                    GPSValue result = new GPSValue();
                    JSONObject jsonObject = (JSONObject) value;
                    result.readFromJSONObject(jsonObject);
                    this.Location = result;
                }
            }
        }else {
            super.setValueForKey(value, key);
        }
    }
}
