package com.procuro.apimmdatamanagerlib;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by jophenmarieweeks on 01/08/2017.
 */

public class TracStop extends PimmBaseObject {

    public String tracStopId;
    public String trackKey;
    public TracDepot dcDepot;
    public String deliveryOrder;
    public String po;
    public String bolNumber;
    public Date scheduledArrival;
    public ArrayList<TracLot> lots;
}
