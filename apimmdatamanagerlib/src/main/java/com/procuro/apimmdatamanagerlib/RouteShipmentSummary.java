package com.procuro.apimmdatamanagerlib;

import org.json.JSONObject;

import java.util.Date;

/**
 * Created by jophenmarieweeks on 11/08/2017.
 */

public class RouteShipmentSummary extends PimmBaseObject {

    public String   shipmentID ;
    public RouteShipmentDriverDTO2 driver;
    public RouteShipmentDriverDTO2 coDriver;
    public String  driverID ;
    public String  coDriverID ;

    public String  tractor ;
    public String  tractorID ;
    public String  trailer ;
    public String  trailerID ;

    public String  routeID ;               // GUID for route
    public String  routeName ;             // name of route
    public int dayOfWeek ;                  // day of week, 0 = Sun

    public Date routeDay;

//Currently Supported Values for this field are:
//StoreDelivery = 0,
//Shuttle = 1,
//AdHoc = 10

    public int routeType;
    public Date loadTime;
    public Date dispatchTime;
    public String loadTimeText;
    public String dispatchTimeText;
    public StoreDeliveryEnums.DeliveryStatus status;


    @Override
    public void setValueForKey(Object value, String key) {

        if (key.equalsIgnoreCase("driver")) {
            if (!value.equals(null)) {
                RouteShipmentDriverDTO2 RouteShipment = new RouteShipmentDriverDTO2();
                JSONObject jsonObject = (JSONObject) value;
                RouteShipment.readFromJSONObject(jsonObject);
                this.driver = RouteShipment;
            }

        } else if (key.equalsIgnoreCase("coDriver")) {

            if (!value.equals(null)) {
                RouteShipmentDriverDTO2 RouteShipment = new RouteShipmentDriverDTO2();
                JSONObject jsonObject = (JSONObject) value;
                RouteShipment.readFromJSONObject(jsonObject);
                this.coDriver = RouteShipment;
            }

        } else if(key.equalsIgnoreCase("status")) {

            StoreDeliveryEnums.DeliveryStatus deliveryStatus = StoreDeliveryEnums.DeliveryStatus.setIntValue((int) value);
            this.status = deliveryStatus;

        }else
        {
            super.setValueForKey(value, key);
        }
    }
}
