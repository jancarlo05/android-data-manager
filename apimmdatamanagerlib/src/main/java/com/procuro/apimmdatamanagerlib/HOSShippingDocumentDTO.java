package com.procuro.apimmdatamanagerlib;

import java.util.Date;
import java.util.HashMap;

/**
 * Created by jophenmarieweeks on 13/11/2017.
 */

public class HOSShippingDocumentDTO extends PimmBaseObject {



    public static class  HOSCoDriverDTO extends PimmBaseObject {

        public String hosCoDriverID;
        public String driverUserID;
        public String coDriverUserID;
        public String userID;
        public Date creationTimeUTC;
        public Date startTimeUTC;
        public Date endTimeUTC;

        public HashMap<String,Object> dictionaryWithValuesForKeys() {
            HashMap<String, Object> dictionary = new HashMap<String, Object>();


            dictionary.put("hosCoDriverID", this.hosCoDriverID);
            dictionary.put("driverUserID", this.driverUserID);
            dictionary.put("coDriverUserID", this.coDriverUserID);
            dictionary.put("userID", this.userID);
            dictionary.put("creationTimeUTC", DateTimeFormat.convertToJsonDateTime(this.creationTimeUTC));
            dictionary.put("startTimeUTC", DateTimeFormat.convertToJsonDateTime(this.startTimeUTC));
            dictionary.put("endTimeUTC", DateTimeFormat.convertToJsonDateTime(this.endTimeUTC));

            return dictionary;
        }

    }



    public static class  HOSDriverVehicleDTO extends PimmBaseObject{
        public String hosDriverVehicleID;
        public String driverUserID;
        public String userID;
        public Date creationTimeUTC;
        public Date startTimeUTC;
        public Date endTimeUTC;
        public String trailer1Name;
        public String trailer1DeviceID;
        public String trailer2Name;
        public String trailer2DeviceID;
        public String truckName;
        public String truckDeviceID;

        public HashMap<String,Object> dictionaryWithValuesForKeys() {
            HashMap<String, Object> dictionary = new HashMap<String, Object>();

            dictionary.put("hosDriverVehicleID", this.hosDriverVehicleID);
            dictionary.put("driverUserID", this.driverUserID);
            dictionary.put("userID", this.userID);

            if(this.creationTimeUTC != null) {
                dictionary.put("creationTimeUTC", this.creationTimeUTC);
            }
            if(this.startTimeUTC != null) {
                dictionary.put("startTimeUTC", this.startTimeUTC);
            }

            if(this.endTimeUTC != null) {
                dictionary.put("endTimeUTC", this.endTimeUTC);
            }

            dictionary.put("trailer1Name", this.trailer1Name);
            dictionary.put("trailer1DeviceID", this.trailer1DeviceID);
            dictionary.put("trailer2Name", this.trailer2Name);
            dictionary.put("trailer2DeviceID", this.trailer2DeviceID);
            dictionary.put("truckName", this.truckName);
            dictionary.put("truckDeviceID", this.truckDeviceID);

            return dictionary;
        }


    }



    public String hosShippingDocumentID;
    public String driverUserID;
    public String userID;
    public Date creationTimeUTC;
    public Date startTimeUTC;
    public Date endTimeUTC;
    public String documentType;
    public String documentName;

    public HashMap<String,Object> dictionaryWithValuesForKeys() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();

        dictionary.put("hosShippingDocumentID", this.hosShippingDocumentID);
        dictionary.put("driverUserID", this.driverUserID);
        dictionary.put("userID", this.userID);

        if(this.creationTimeUTC != null) {
            dictionary.put("creationTimeUTC", this.creationTimeUTC);
        }
        if(this.startTimeUTC != null) {
            dictionary.put("startTimeUTC", this.startTimeUTC);
        }

        if(this.endTimeUTC != null) {
            dictionary.put("endTimeUTC", this.endTimeUTC);
        }

        dictionary.put("documentType", this.documentType);
        dictionary.put("documentName", this.documentName);

        return dictionary;
    }






}
