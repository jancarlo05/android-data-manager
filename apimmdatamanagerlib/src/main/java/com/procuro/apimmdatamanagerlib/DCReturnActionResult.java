package com.procuro.apimmdatamanagerlib;

import android.util.Log;

import org.json.JSONObject;

import java.util.Date;

/**
 * Created by jophenmarieweeks on 12/07/2017.
 */

public class DCReturnActionResult extends PimmBaseObject {

    public SdrDCReturn DCReturn;
    public Date EndTime;
    public Date ArrivalTime;
    public DeliveryStatusDTO Status;
    public StoreDeliveryEnums.TransportStatus TransportStatus;

    @Override
    public void setValueForKey(Object value, String key) {

        if (key.equalsIgnoreCase("DCReturn")) {

            SdrDCReturn sdrDCReturn = new SdrDCReturn();
            JSONObject jsonObject = (JSONObject) value;
            sdrDCReturn.readFromJSONObject(jsonObject);
            this.DCReturn = sdrDCReturn;

        }else if (key.equalsIgnoreCase("Status")) {

            DeliveryStatusDTO deliveryStatusDTO = new DeliveryStatusDTO();
            JSONObject jsonObject = (JSONObject) value;
            deliveryStatusDTO.readFromJSONObject(jsonObject);
            this.Status = deliveryStatusDTO;

        }else if(key.equalsIgnoreCase("TransportStatus")) {

            StoreDeliveryEnums.TransportStatus trans = StoreDeliveryEnums.TransportStatus.setIntValue((int) value);
            this.TransportStatus = trans;

        } else if (key.equalsIgnoreCase("EndTime")){
                this.EndTime = JSONDate.convertJSONDateToNSDate(String.valueOf(value));

        } else if (key.equalsIgnoreCase("ArrivalTime")){
                this.ArrivalTime = JSONDate.convertJSONDateToNSDate(String.valueOf(value));

        } else {
            super.setValueForKey(value, key);
        }
    }

}
