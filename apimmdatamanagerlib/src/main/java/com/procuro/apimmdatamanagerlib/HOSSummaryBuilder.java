package com.procuro.apimmdatamanagerlib;

import android.util.Log;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;

import static com.procuro.apimmdatamanagerlib.DeliveryPOITypeEnum.DeliveryPOITypesEnum.DeliveryPOIType_EndPoint;
import static com.procuro.apimmdatamanagerlib.DeliveryPOITypeEnum.DeliveryPOITypesEnum.DeliveryPOIType_StartPoint;

/**
 * Created by jophenmarieweeks on 05/07/2017.
 */

public class HOSSummaryBuilder extends PimmBaseObject {

    public SdrReport _sdrReport;
    public DriverTimeLog _driverTimeLog;
    public HOSSummary _hosSummary;
    public String _userId;
    public String _deliveryId;
    public int _driveTime; //driveTime in Minutes
    public int _onDutyTime; //OnDutyTime in Minutes
    public int _restTime; //RestTime in Minutes
    public int _prepTime; //PreTripTime in Minutes
    public int _deliveryTime; //DeliveryTime In Minutes
    public int _sleeperTime; //Sleeper time in minutes;


    @Override
    public void setValueForKey(Object value, String key) {

        if (key.equalsIgnoreCase("_sdrReport"))
        {
            if (!value.equals(null)) {
                SdrReport sdrReport = new SdrReport();
                JSONObject jsonObject = (JSONObject) value;
                sdrReport.readFromJSONObject(jsonObject);
                this._sdrReport = sdrReport;
            }
        }else if (key.equalsIgnoreCase("_driverTimeLog")) {

            if (!value.equals(null)) {
                DriverTimeLog driverTimeLog = new DriverTimeLog();
                JSONObject jsonObject = (JSONObject) value;
                driverTimeLog.readFromJSONObject(jsonObject);
                this._driverTimeLog = driverTimeLog;
            }
        }else if (key.equalsIgnoreCase("_hosSummary")) {

            if (!value.equals(null)) {
                HOSSummary hosSummary = new HOSSummary();
                JSONObject jsonObject = (JSONObject) value;
                hosSummary.readFromJSONObject(jsonObject);
                this._hosSummary = hosSummary;
            }
        }
        else
        {
            super.setValueForKey(value, key);
        }
    }


    public void initializeWithUserId(String userId , SdrReport sdrReport , DriverTimeLog driverTimeLog){

        this._userId = userId;
        this._sdrReport = sdrReport;
        this._driverTimeLog = driverTimeLog;
        this._hosSummary = null;
        this._prepTime = 0;
        this._driveTime = 0;
        this._onDutyTime = 0;
        this._restTime = 0;
        this._deliveryTime = 0;
        this._sleeperTime = 0;


    }

    public boolean buildHOSSummary(){

        calculateDriveTime();
        calculateBreakTime();
        calculateSleeperTime();
        
        if(calculateOnDutyTime() == false){
            return false;

        }
         if(calculatePrepTime() == false){
            return false;
        }

        this._deliveryId = this._driverTimeLog.DeliveryID;
        this._hosSummary = new HOSSummary();

        this._hosSummary.userID = this._userId;
        this._hosSummary.deliveryId = this._deliveryId;
        this._hosSummary.driveTime = this._driveTime;
        this._hosSummary.onDutyTime = this._onDutyTime;
        this._hosSummary.prepTime = this._prepTime;

        this._hosSummary.restTime = this._restTime;
        this._hosSummary.deliveryTime = this._deliveryTime;
        this._hosSummary.sleeperTime = this._sleeperTime;

        return true;
    }

    public void logHOSSummary(){
        Log.v("DM", "HOSSummmary : " + getHOSSummaryAsString());
    }
    public void setDriveTime(int driveTime){

        this._driveTime = driveTime;
    }

    public void setDeliveryTime(int deliveryTime){

        this._deliveryTime = deliveryTime;
    }

    public String getHOSSummaryAsString(){

        if (this._hosSummary == null){
            if(!buildHOSSummary()){
                String errMessage = "Error while computing HOSSummary";
                return errMessage;
            }
        }

        String onDutyTimeStr = convertTimeIntervalToHourMinuteSeconds(this._onDutyTime * 60);
        String prepTimeStr = convertTimeIntervalToHourMinuteSeconds(this._prepTime * 60);
        String driveTimeStr = convertTimeIntervalToHourMinuteSeconds(this._driveTime * 60);
        String deliveryTimeStr = convertTimeIntervalToHourMinuteSeconds(this._deliveryTime * 60);
        String restTimeStr = convertTimeIntervalToHourMinuteSeconds(this._restTime * 60);
        String sleeperTimeStr = convertTimeIntervalToHourMinuteSeconds(this._sleeperTime * 60);
        String routeName = this._driverTimeLog.RouteName;

        String hosSummaryStr = String.format("HOSSummary for RouteName:%@\nOnDutyTime:%@\nDriveTime:%@\nRestTime:%@\nPrepTime:%@\nDeliveryTime:%@\nSleeperTime:%@\n",routeName,onDutyTimeStr, driveTimeStr, restTimeStr, prepTimeStr, deliveryTimeStr, sleeperTimeStr);



        return hosSummaryStr;
    }

    public void postHOSSummaryToServer(){

    try{
        if(this._hosSummary ==null){

            if(!buildHOSSummary()){
                Log.e("DM","Could not compute HOSSummary, cannot post to Server");
                return;
            }
        }

        if(this._hosSummary != null){
            aPimmDataManager datamanager = aPimmDataManager.getInstance();
            datamanager.postHOSSummary(this._hosSummary, new OnCompleteListeners.onPostHOSSummaryListener() {
                @Override
                public void postHOSSummary(Error error) {
                    if(error != null){
                        Log.e("DM", "Error posting HOSSummary");
                    }else{
                        Log.e("DM", "HOSSummary Successfully posted to Server");
                    }

                }
            });
        }

    }   catch (Exception e){
        Log.e("DM","CRASH : " + e.getMessage());

    }

    }

    public boolean calculateOnDutyTime(){

        Log.v("DM","Enter Function : calculateOnDutyTime");

        boolean iscalculateOnDutyTime = false;
        //Total On-Duty time needs to be calculated from PreTripStart to EndOf Route
        Date endOfRouteTime = null;
        Date preTripStartTime = null;
        Date punchOutTime = null;

        DriverLogTimeInfoDetails.DriverLastStatusEnum lastStatus = this._driverTimeLog.TimeInfoDetails.LastStatus;

        preTripStartTime = DateTimeFormat.truncateSecondsForDate(this._driverTimeLog.TimeInfoDetails.PreTrip.Start);

        if(preTripStartTime == null){
            Log.e("DM", "Error: Time Log doesnot contain PreTrip Start Time. Cannot calculate OnDuty Time");
            iscalculateOnDutyTime = false;
        }

        if(lastStatus == DriverLogTimeInfoDetails.DriverLastStatusEnum.DriverLastStatusEnum_GoHome){
            punchOutTime = DateTimeFormat.truncateSecondsForDate(this._driverTimeLog.TimeInfoDetails.Punch.End);
            Log.v("DM","PreTrip Start time: "+ preTripStartTime +", PunchOutTime:" + endOfRouteTime);
        }

        if(punchOutTime != null){

            long totalTimeBetweenPreTripStartAndPunchOut = punchOutTime.getTime() - preTripStartTime.getTime();
            Log.v("DM","Total Time Between PreTripStart and PunchOut: " + totalTimeBetweenPreTripStartAndPunchOut);

            this._onDutyTime = (int) (totalTimeBetweenPreTripStartAndPunchOut/60) - (this._restTime + this._sleeperTime);

            iscalculateOnDutyTime = true;
        }else if(endOfRouteTime != null){
            long totalTimeBetweenPreTripStartAndEndOfRoute = endOfRouteTime.getTime() - preTripStartTime.getTime();
            Log.v("DM","Total Time Between PreTripStart and endOfRouteTime: " + totalTimeBetweenPreTripStartAndEndOfRoute);

            this._onDutyTime = (int) (totalTimeBetweenPreTripStartAndEndOfRoute/60) - (this._restTime + this._sleeperTime);

            iscalculateOnDutyTime = true;

        }else{
            Log.v("DM","Error: Time Log doesnot contain PunchOut Time or PostTripEnd time.Cannot calculate OnDuty Time");
            iscalculateOnDutyTime = false;
        }



        Log.v("DM","exit Function : calculateOnDutyTime");
        return iscalculateOnDutyTime;

    }



    public boolean calculatePrepTime() {

        Log.v("DM","Enter Function : calculatePrepTime");

        boolean isCalculatePrepTime = false;


        //Prep Time is time difference between PreTrip Start Time and DC Departure + difference between PostTripEnd and DCReturn
        Date preTripStartTime = null;
        Date departureTime = null;
        Date postTripEndTime = null;
        Date dcReturnTime = null;

        preTripStartTime = DateTimeFormat.truncateSecondsForDate(_driverTimeLog.TimeInfoDetails.PreTrip.Start);

            if(preTripStartTime == null){
                Log.e("DM","PreTripStartTime is nil in DTL, cannot compute PrepTime for HOSSummary");
                return false;
            }

            if(postTripEndTime == null) {
                postTripEndTime = DateTimeFormat.truncateSecondsForDate(_driverTimeLog.TimeInfoDetails.PostTrip.End);
                if (postTripEndTime == null) {
                    Log.e("DM", "postTripEndTime is nil in DTL, cannot compute PrepTime for HOSSummary");
                    return false;
                }
            }

            if(this._sdrReport != null){
                SdrDelivery hierarchy = _sdrReport.Hierarchy;
                //Check if driver has started driving yet or not
                departureTime = hierarchy.DepartureTime;
                dcReturnTime = hierarchy.ArrivalTime;
                if(departureTime == null || dcReturnTime == null)
                {
                    Log.e("DM","DCDepartureTime or DCReturnTime is nil in SdrReport, cannot compute PrepTime for HOSSummary");
                    return  false;
                }

            }else{
                departureTime = DateTimeFormat.truncateSecondsForDate(this._driverTimeLog.TimeInfoDetails.Dispatch.Start);
                dcReturnTime = DateTimeFormat.truncateSecondsForDate(this._driverTimeLog.TimeInfoDetails.Dispatch.End);

                if(departureTime == null || dcReturnTime == null)
                {
                    Log.e("DM","DCDepartureTime or DCReturnTime is nil in SdrReport, cannot compute PrepTime for HOSSummary");
                    return  false;
                }
            }

            long totalTimeBetweenPreTripStartAndDCDeparture = departureTime.getTime() - preTripStartTime.getTime();
            long totalTimeBetweenDCReturnAndPostTripEnd = postTripEndTime.getTime() - dcReturnTime.getTime();

        this._prepTime = (int)((int) totalTimeBetweenPreTripStartAndDCDeparture/60 + totalTimeBetweenDCReturnAndPostTripEnd/60);




        Log.v("DM","exit Function : calculatePrepTime");

        return true;
    }


    public void calculateDriveTime(){
        if(this._sdrReport == null){
            return;
        }

//        Reset the Drive Time to Zero and Recalculate from SdrReport
        _driveTime = 0;
        _deliveryTime = 0;

        double totalDriveTime = 0;
        double totalDeliveryTime = 0;



        SdrDelivery hierarchy = _sdrReport.Hierarchy;

        //Check if driver has started driving yet or not
        String departureTimeFromDCStr = hierarchy.DepartureTimeText;

        Date departureTimeFromDC = DateTimeFormat.convertJSONDateToDate(departureTimeFromDCStr);
        if(departureTimeFromDC == null)
        {
            Log.v("DM","Driver has not yet departed from DC");
            _driveTime = 0;
            return;
        }
        SdrGis gis = hierarchy.GIS;
        ArrayList<SdrStop> scheduledStops = gis.Stops;
        ArrayList<SdrPointOfInterest> unscheduledStops = gis.PointsOfInterest;

        ArrayList<HashMap<String, Object>> departureTimeArray = new ArrayList<>();
        ArrayList<HashMap<String, Object>> arrivalTimeArray = new ArrayList<>();

        HashMap<String, Object> departureTimeLog = new HashMap<>();
        departureTimeLog.put("DepartureTime", departureTimeFromDC);
        departureTimeArray.add(departureTimeLog);

        //Check if driver has returned to DC

        Date dcReturnTime = hierarchy.ArrivalTime;

        if(dcReturnTime == null) {
            Log.v("DM","Driver has not yet returned to DC");
        } else {
            HashMap<String, Object>  returnTimeLog = new HashMap<>();
            returnTimeLog.put("ArrivalTime",dcReturnTime);
            arrivalTimeArray.add(returnTimeLog);
        }

        for(SdrStop stop : scheduledStops){
            String arrivalTimeStr = String.valueOf(stop.ArrivalTime);
            Date arrivalTime = DateTimeFormat.truncateSecondsForDate(DateTimeFormat.convertJSONDateToDate(arrivalTimeStr));

            if(arrivalTime != null){
                HashMap<String, Object> arrivalTimeLog = new HashMap<>();
                arrivalTimeLog.put("ArrivalTime",arrivalTime);

                arrivalTimeArray.add(arrivalTimeLog);

                String departureTimeStr = String.valueOf(stop.DepartureTime);
                Date departureTime = DateTimeFormat.truncateSecondsForDate(DateTimeFormat.convertJSONDateToDate(departureTimeStr));

                if(departureTime != null){
                    HashMap<String, Object> departTimeLog = new HashMap<>();
                    departTimeLog.put("DepartureTime",departureTime);

                    departureTimeArray.add(departTimeLog);

                    String deliveryStartTimeStr = String.valueOf(stop.DeliveryStartTime);
                    String deliveryEndTimeStr = String.valueOf(stop.DeliveryEndTime);

                    Date deliveryStartTime = DateTimeFormat.truncateSecondsForDate(DateTimeFormat.convertJSONDateToDate(deliveryStartTimeStr));

                    Date deliveryEndTime = DateTimeFormat.truncateSecondsForDate(DateTimeFormat.convertJSONDateToDate(deliveryEndTimeStr));

                    if(deliveryStartTime != null && deliveryEndTime != null){
                        long deliverytime = deliveryEndTime.getTime() - deliveryStartTime.getTime();
                        totalDeliveryTime = totalDeliveryTime + deliverytime;
                    }


                }

            }

        }
        Log.v("DM", "Total DeliveryTime: " +  convertTimeIntervalToHourMinuteSeconds((int) totalDeliveryTime));


        for(SdrPointOfInterest stop : unscheduledStops){
            //Check the Stop Type

            DeliveryPOITypeEnum.DeliveryPOITypesEnum stopType = stop.Type;
            if(stopType == DeliveryPOIType_StartPoint || stopType == DeliveryPOIType_EndPoint)
            {
                continue;
            }

            String arrivalTimeStr = String.valueOf(stop.ArrivalTime);
            Date arrivalTime = DateTimeFormat.truncateSecondsForDate(DateTimeFormat.convertJSONDateToDate(arrivalTimeStr));
            if(arrivalTime != null )
            {
                 HashMap<String, Object> arrivalTimeLog = new HashMap<>();
                arrivalTimeLog.put("ArrivalTime",arrivalTime);

                arrivalTimeArray.add(arrivalTimeLog);

                String departureTimeStr = String.valueOf(stop.DepartureTime);
                Date departureTime = DateTimeFormat.truncateSecondsForDate(DateTimeFormat.convertJSONDateToDate(departureTimeStr));

                if(departureTime != null){
                    HashMap<String, Object> departTimeLog = new HashMap<>();
                    departTimeLog.put("DepartureTime",departureTime);
                    departureTimeArray.add(departTimeLog);

                }
            }

        }

        if(arrivalTimeArray.size() > 0 ){
//           ArrayList<HashMap<String, Object>> sortedArrivalTimeArray = Collections.sort(arrivalTimeArray, 1);
          /*  NSSortDescriptor *sortDescriptor1 = [[NSSortDescriptor alloc] initWithKey:@"ArrivalTime" ascending:YES];
            NSArray *sortedArrivalTimeArray = [arrivalTimeArray sortedArrayUsingDescriptors:[NSArray arrayWithObject:sortDescriptor1]];
            NSSortDescriptor *sortDescriptor2 = [[NSSortDescriptor alloc] initWithKey:@"DepartureTime" ascending:YES];
            NSArray *sortedDepartureTimeArray = [departureTimeArray sortedArrayUsingDescriptors:[NSArray arrayWithObject:sortDescriptor2]];


            for(int i=0; i< sortedArrivalTimeArray.count; i++)
            {
                NSDictionary* arrivalTimeLog = [sortedArrivalTimeArray objectAtIndex:i];
                NSDate* arrivalTime = [arrivalTimeLog objectForKey:@"ArrivalTime"];

                if ([sortedDepartureTimeArray count]>i) {
                NSDictionary* departureTimeLog = [sortedDepartureTimeArray objectAtIndex:i];
                NSDate* departureTime = [departureTimeLog objectForKey:@"DepartureTime"];


                NSTimeInterval driveTime =[arrivalTime timeIntervalSinceDate:departureTime];
                DDLogVerbose(@"Drive Time:%f", driveTime);
                totalDriveTime = totalDriveTime + driveTime;
            }
            }
            if(arrivalTimeArray.count < departureTimeArray.count)
            {
                DDLogVerbose(@"Driver is currently Driving");

                NSDate* currentTime = [NSDate date];



                int lastIndex = [sortedDepartureTimeArray count] - 1;
                NSDictionary* departureTimeLog = [sortedDepartureTimeArray objectAtIndex: lastIndex];
                NSDate* lastDepartureTime = [departureTimeLog objectForKey:@"DepartureTime"];



                NSTimeInterval driveTime =[currentTime timeIntervalSinceDate:lastDepartureTime];

                DDLogVerbose(@"Drive Time Since Last Departure:%@", [self convertTimeIntervalToHourMinuteSeconds:driveTime]);

                totalDriveTime = totalDriveTime + driveTime;

*/
        } else {
            Log.v("DM","Driver  is currently driving  and has not yet arrived at any stop");

            Date currentTime = new Date();

            long timeSinceDepartureFromDC  = currentTime.getTime() - departureTimeFromDC.getTime();
            totalDriveTime = timeSinceDepartureFromDC;


             Log.v("DM","Drive Time since Departure From DC: " + convertTimeIntervalToHourMinuteSeconds((int) totalDriveTime));
        }

        this._driveTime = (int) Math.round(totalDriveTime/60);
        this._deliveryTime = (int) Math.round(totalDeliveryTime/60);

        Log.v("Dm","Total Drive Time: " +  convertTimeIntervalToHourMinuteSeconds((int) totalDriveTime));



    }

    public void calculateBreakTime() {

        double totalBreakTime = 0;

        totalBreakTime = _driverTimeLog.getTotalBreakTimeForDriver();
        this._restTime = (int) Math.round(totalBreakTime/60);

        Log.v("DM","Total Break Time: " +  convertTimeIntervalToHourMinuteSeconds((long) totalBreakTime));
    }

    public void calculateSleeperTime() {
        double sleeperTime = 0;
        sleeperTime = _driverTimeLog.getTotalSleeperTimeForDriver();
        _sleeperTime = (int) Math.round(sleeperTime/60);

        Log.v("DM","Total Sleeper Time:%@" + convertTimeIntervalToHourMinuteSeconds((long) sleeperTime));


    }


    private String convertTimeIntervalToHourMinuteSeconds(long time) {

        long TimeInMillis = time;

        int hour = (int) ((TimeInMillis / 1000) / 3600);
        int minutes = (int) (((TimeInMillis / 1000) / 60) % 60);
        int secs = (int) ((TimeInMillis / 1000) % 60);
        // Log.v("DM", "Current Drive Time is: " + String.format("%d:%02d:%02d",  hour, minutes, secs));
        return String.format("%d:%02d:%02d",  hour, minutes, secs);
    }



}