package com.procuro.apimmdatamanagerlib;

import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by jophenmarieweeks on 06/07/2017.
 */

public class DeliveryReportActionResult extends PimmBaseObject {

    public String DeliveryId;
    public String Rebuild;
    public Boolean Changed;

    public SdrDelivery Delivery;

    @Override
    public void setValueForKey(Object value, String key) {

        if(key.equalsIgnoreCase("Delivery")) {
            if (!value.equals(null)) {
                SdrDelivery sdrDelivery = new SdrDelivery();
                JSONObject jsonObject = (JSONObject) value;
                sdrDelivery.readFromJSONObject(jsonObject);
                this.Delivery = sdrDelivery;
            }
        } else {
            super.setValueForKey(value, key);
        }
    }

    public HashMap<String,Object> dictionaryWithValuesForKeys() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();

        dictionary.put("DeliveryId", this.Delivery);
        dictionary.put("Rebuild", this.Rebuild);
        dictionary.put("Changed", this.Changed);
        dictionary.put("Delivery", (this.Delivery == null) ? null : this.Delivery.dictionaryWithValuesForKeys() );

        return dictionary;
    }

}
