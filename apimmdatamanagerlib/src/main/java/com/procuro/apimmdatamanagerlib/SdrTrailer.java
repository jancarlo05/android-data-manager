package com.procuro.apimmdatamanagerlib;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jophenmarieweeks on 10/07/2017.
 */

public class SdrTrailer extends PimmBaseObject {

    public String LastValidGPSTime;
    public String LastValidGPSTimeText;
    public LatLon CurrentLocation;
    public double CurrentSpeed;

    public List<SdrTrailerCompliance> Compliance; //Array of SdrTrailerCompliance

    @Override
    public void setValueForKey(Object value, String key)
    {
        if(key.equalsIgnoreCase("Compliance"))
        {
            if (this.Compliance == null) {
                this.Compliance = new ArrayList<SdrTrailerCompliance>();
            }

            if (value instanceof JSONArray) {
                JSONArray arrData = (JSONArray) value;

                if (arrData != null) {
                    for (int i = 0; i < arrData.length(); i++) {
                        try {
                            SdrTrailerCompliance sdrTrailerCompliance = new SdrTrailerCompliance();
                            sdrTrailerCompliance.readFromJSONObject(arrData.getJSONObject(i));

                            this.Compliance.add(sdrTrailerCompliance);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
        else if (key.equalsIgnoreCase("CurrentLocation"))
        {
            if (ValidObject(value)) {
                LatLon latLon = new LatLon();
                JSONObject jsonObject = (JSONObject) value;
                latLon.readFromJSONObject(jsonObject);
                this.CurrentLocation = latLon;
            }
        }
        else
        {
            super.setValueForKey(value, key);
        }
    }

    private boolean ValidObject(Object object){
        if (object!=null && !object.toString().equalsIgnoreCase("null") && !object.toString().equalsIgnoreCase("")){
            return true;
        }else {
            return false;
        }
    }
}
