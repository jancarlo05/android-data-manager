package com.procuro.apimmdatamanagerlib;

import java.util.Date;

/**
 * Created by jophenmarieweeks on 11/07/2017.
 */

public class SdVehicleAssetTracking extends PimmBaseObject {

    public String SiteId;
    public Date StartTime;
    public Date EndTime;
    public int status;
    public double FuelIn;
    public double FuelOut;
    public double ReeferHoursIn;
    public double ReeferHoursOut;
    public String AssetTrackingRegionId;

}
