package com.procuro.apimmdatamanagerlib;

import java.util.HashMap;

public class TrainingDataItems extends  PimmBaseObject{

    public String thumbnailURL;
    public String videoURL;
    public String title;
    public String category;


    @Override
    public void setValueForKey(Object value, String key) {
        super.setValueForKey(value, key);

    }


    public HashMap<String,Object> dictionaryWithValuesForKeys() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();

        dictionary.put("thumbnailURL", this.thumbnailURL);
        dictionary.put("videoURL", this.videoURL);
        dictionary.put("title", this.title);
        dictionary.put("category", this.category);

        return dictionary;
    }
}
