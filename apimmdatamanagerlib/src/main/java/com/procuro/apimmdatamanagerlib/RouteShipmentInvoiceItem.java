package com.procuro.apimmdatamanagerlib;

import android.util.Log;

/**
 * Created by jophenmarieweeks on 12/07/2017.
 */

public class RouteShipmentInvoiceItem extends PimmBaseObject {

    public enum RouteShipmentInvoiceItemCategoryEnum {
        NONE(0),
        DISCOUNT(1),
        CHARGE(2),
        TAX(3),
        OTHER(4);

        private int value;
        private RouteShipmentInvoiceItemCategoryEnum(int value)
        {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }

        public static RouteShipmentInvoiceItemCategoryEnum setIntValue (int i) {
            for (RouteShipmentInvoiceItemCategoryEnum type : RouteShipmentInvoiceItemCategoryEnum.values()) {
                if (type.value == i) { return type; }
            }
            return NONE;
        }

    }



    public String RouteShipmentInvoiceItemID;
    public String DCID;
    public RouteShipmentInvoiceItemCategoryEnum category;
    public String Description;
    public boolean valid;
    public String CustomerInvoiceItemID;
    public String po;


    @Override
    public void setValueForKey(Object value, String key) {

        if(key.equalsIgnoreCase("category")) {
            if (!value.equals(null)) {
                RouteShipmentInvoiceItemCategoryEnum deliveryPOITypes = RouteShipmentInvoiceItemCategoryEnum.setIntValue((int) value);
                this.category = deliveryPOITypes;
            }
       } else {
            super.setValueForKey(value, key);
        }
    }

}
