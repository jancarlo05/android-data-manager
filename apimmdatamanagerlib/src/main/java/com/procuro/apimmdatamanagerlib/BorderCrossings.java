package com.procuro.apimmdatamanagerlib;

import org.json.JSONObject;

import java.util.Date;

/**
 * Created by jophenmarieweeks on 06/07/2017.
 */

public class BorderCrossings extends PimmBaseObject {

    public Date Timestamp;
    public Integer TimezoneId;
    public BorderCrossingBuilder Data;


    @Override
    public void setValueForKey(Object value, String key) {
        if (key.equalsIgnoreCase("Data")) {
            BorderCrossingBuilder borderCrossingBuilder = new BorderCrossingBuilder();
            JSONObject jsonObject = (JSONObject) value;
            borderCrossingBuilder.readFromJSONObject(jsonObject);
            this.Data = borderCrossingBuilder;

        } else {
            super.setValueForKey(value, key);
        }
    }

}
