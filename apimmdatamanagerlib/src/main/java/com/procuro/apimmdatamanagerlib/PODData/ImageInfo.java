package com.procuro.apimmdatamanagerlib.PODData;

import com.procuro.apimmdatamanagerlib.PimmBaseObject;

import java.util.HashMap;

import static com.procuro.apimmdatamanagerlib.PODData.ImageInfo.TMSImageType.TMSImageType_PODSignature;


public class ImageInfo extends PimmBaseObject {


    public String attachmentId;
    public String imageName;
    public String signerName;
    public String comments;
    public String imagePath;

    public TMSImageType type;
    public boolean savedOnServer; //indicated whether image was successfully saved on Server or not

    @Override
    public void setValueForKey(Object value, String key) {

        if(key.equalsIgnoreCase("type")) {
            if (value != null) {
                try {
                    this.type = TMSImageType.setIntValue(Integer.parseInt(value.toString()));
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

        } else {
            super.setValueForKey(value, key);
        }
    }


    public enum TMSImageType {
        TMSImageType_PODSignature(0),
        TMSImageType_CreditMemoSignature(1),
        TMSImageType_DebitMemoSignature(2),
        TMSImageType_CameraImage_POD(3),
        TMSImageType_CameraImage_CreditMemo(4),
        TMSImageType_CameraImage_DebitMemo(5),
        TMSImageType_AdditionalDeliverySignature(6);

        private int value;

        private TMSImageType( int value){
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }

        public static TMSImageType setIntValue (int i) {
            for (TMSImageType type : TMSImageType.values()) {
                if (type.value == i) { return type; }
            }
            return TMSImageType_PODSignature;
        }
    }


    public HashMap<String,Object> dictionaryWithValuesForKeys() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();

        dictionary.put("attachmentId", this.attachmentId);
        dictionary.put("imageName", this.imageName);
        dictionary.put("signerName", this.signerName);
        dictionary.put(" savedOnServer", this.savedOnServer);
        dictionary.put("comments", this.comments);
        dictionary.put("imagePath", this.imagePath);
        dictionary.put("type", (this.type == null) ? TMSImageType_PODSignature : this.type.value);

        return dictionary;
    }


}
