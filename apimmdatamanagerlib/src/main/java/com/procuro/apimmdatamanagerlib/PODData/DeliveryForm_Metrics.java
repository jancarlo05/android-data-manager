package com.procuro.apimmdatamanagerlib.PODData;

import android.util.Log;

import com.procuro.apimmdatamanagerlib.JSONDate;
import com.procuro.apimmdatamanagerlib.PimmBaseObject;
import com.procuro.apimmdatamanagerlib.SdrStopMetric;

import java.util.Date;
import java.util.HashMap;

public class DeliveryForm_Metrics extends PimmBaseObject{

    public String Label;
    public int MetricClass; //int
    public int Compartment; //int
    public double LastValue; //double
    public Date LastTimestamp;
    public String CompartmentName;


    @Override
    public void setValueForKey(Object value, String key) {
        if (key.equalsIgnoreCase("LastTimestamp")){
            if (value!=null){
                this.LastTimestamp = JSONDate.convertJSONDateToNSDate(value.toString());
            }
        }

        super.setValueForKey(value, key);
    }

    public HashMap<String,Object> dictionaryWithValuesForKeys() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();

        dictionary.put("Label", this.Label);
        dictionary.put("MetricClass", this.MetricClass);
        dictionary.put("Compartment", this.Compartment);
        dictionary.put("LastValue", this.LastValue);
        dictionary.put("LastTimestamp", this.LastTimestamp);
        dictionary.put("CompartmentName", this.CompartmentName);

        return dictionary;
    }
}

