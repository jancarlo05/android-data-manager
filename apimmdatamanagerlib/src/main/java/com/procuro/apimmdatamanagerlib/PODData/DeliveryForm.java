package com.procuro.apimmdatamanagerlib.PODData;

import android.util.Log;

import com.procuro.apimmdatamanagerlib.AssignmentInfo;
import com.procuro.apimmdatamanagerlib.DMUtils;
import com.procuro.apimmdatamanagerlib.JSONDate;
import com.procuro.apimmdatamanagerlib.PimmBaseObject;
import com.procuro.apimmdatamanagerlib.RouteShipmentEquipmentDTO;
import com.procuro.apimmdatamanagerlib.RouteShipmentInvoiceItem;
import com.procuro.apimmdatamanagerlib.RouteShipmentProduct;
import com.procuro.apimmdatamanagerlib.RouteShipmentUnit;
import com.procuro.apimmdatamanagerlib.RouteStatusInvoiceItem;
import com.procuro.apimmdatamanagerlib.SdrStop;
import com.procuro.apimmdatamanagerlib.SiteSettings;
import com.procuro.apimmdatamanagerlib.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class DeliveryForm extends PimmBaseObject {

    public String SiteID;
    public String DriverId;
    public String SiteName;
    public String Trailer;
    public String Tractor;
    public String RouteNo;
    public String Driver;
    public String DeliveryComments;
    public String PODSignatureSignerName;
    public String CreditMemoSignatureSignerName;
    public String DebitMemoSignatureSignerName;
    public String AdditionalDeliveryComments;
    public String CoolerBarcode;
    public String FreezerBarcode;
    public String PODSignatureAttachmentID;
    public String CreditMemoSignatureAttachmentID;
    public String DebitMemoSignatureAttachmentID;

    public Date LastUpdated;
    public Date ArrivalScheduleTimeJSON;
    public Date ArrivalActualTime;
    public Date PODSaveButtonPressTime;
    public Date DeliveryStartTime;
    public Date DeliveryEndTime;
    public Date AdditionalDeliveryStartTime;
    public Date AdditionalDeliveryEndTime;

    public boolean PaymentSplit;
    public boolean Verified;
    public boolean Disclaimer;
    public boolean keyStop;

    public int OdometerReading;

    public DeliveryForm_StoreInfo StoreInfo;
    public DeliveryForm_StopProductInfo StopProductInfo;
    public DeliveryForm_CreditMemoInfo CreditMemoInfo;
    public DeliveryForm_DebitMemoInfo DebitMemoInfo;
    public DeliveryForm_DeliveryEquipmentInfo DeliveryEquipmentInfo;
    public DeliveryForm_PaymentCollectionInfo PaymentCollectionInfo;
    public DeliveryForm_StoreReady StoreReady;

    public ArrayList<DeliveryForm_StopInvoiceItem> StopInvoiceItemInfo;
    public ArrayList<DeliveryForm_PaymentCollectionInfo> PaymentCollectionInfoList;
    public ArrayList<ImageInfo> PODCameraImageAttachments;
    public ArrayList<ImageInfo> CreditMemoCameraImageAttachments;
    public ArrayList<ImageInfo> DebitMemoCameraImageAttachments;
    public ArrayList<ImageInfo> StoreBarcodeCameraImageAttachments;
    public ArrayList<ImageInfo> ClosedDoorCameraImageAttachments;

    @Override
    public void setValueForKey(Object value, String key) {

        if (key.equalsIgnoreCase("LastUpdated")){
            if (DMUtils.ValidObject(value)){
                this.LastUpdated = JSONDate.convertJSONDateToNSDate(value.toString());
            }
        }
        else  if (key.equalsIgnoreCase("keyStop")){
            if (DMUtils.ValidObject(value)){
                this.keyStop = DMUtils.ConvertToBoolean(value);
            }
        }
        else  if (key.equalsIgnoreCase("ArrivalScheduleTimeJSON")){
            if (DMUtils.ValidObject(value)){
                this.ArrivalScheduleTimeJSON = JSONDate.convertJSONDateToNSDate(value.toString());
            }
        }
        else  if (key.equalsIgnoreCase("ArrivalActualTime")){
            if (DMUtils.ValidObject(value)){
                this.ArrivalActualTime = JSONDate.convertJSONDateToNSDate(value.toString());
            }
        }
        else  if (key.equalsIgnoreCase("PODSaveButtonPressTime")){
            if (DMUtils.ValidObject(value)){
                this.PODSaveButtonPressTime = JSONDate.convertJSONDateToNSDate(value.toString());
            }
        }
        else  if (key.equalsIgnoreCase("DeliveryStartTime")){
            if (DMUtils.ValidObject(value)){
                this.DeliveryStartTime = JSONDate.convertJSONDateToNSDate(value.toString());
            }
        }
        else  if (key.equalsIgnoreCase("DeliveryEndTime")){
            if (DMUtils.ValidObject(value)){
                this.DeliveryEndTime = JSONDate.convertJSONDateToNSDate(value.toString());
            }
        }
        else  if (key.equalsIgnoreCase("AdditionalDeliveryStartTime")){
            if (DMUtils.ValidObject(value)){
                this.AdditionalDeliveryStartTime = JSONDate.convertJSONDateToNSDate(value.toString());
            }
        }
        else  if (key.equalsIgnoreCase("AdditionalDeliveryEndTime")){
            if (DMUtils.ValidObject(value)){
                this.AdditionalDeliveryEndTime = JSONDate.convertJSONDateToNSDate(value.toString());
            }
        }
        else  if (key.equalsIgnoreCase("StoreInfo")){
            if (DMUtils.ValidObject(value)){
                try {
                    JSONObject jsonObject = (JSONObject) value;
                    this.StoreInfo = new DeliveryForm_StoreInfo();
                    this.StoreInfo.readFromJSONObject(jsonObject);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
        else  if (key.equalsIgnoreCase("StopProductInfo")){
            if (DMUtils.ValidObject(value)){
                try {
                    JSONObject jsonObject = (JSONObject) value;
                    this.StopProductInfo = new DeliveryForm_StopProductInfo();
                    this.StopProductInfo.readFromJSONObject(jsonObject);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
        else  if (key.equalsIgnoreCase("CreditMemoInfo")){
            if (DMUtils.ValidObject(value)){
                try {
                    JSONObject jsonObject = (JSONObject) value;
                    this.CreditMemoInfo = new DeliveryForm_CreditMemoInfo();
                    this.CreditMemoInfo.readFromJSONObject(jsonObject);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
        else  if (key.equalsIgnoreCase("DebitMemoInfo")){
            if (DMUtils.ValidObject(value)){
                try {
                    JSONObject jsonObject = (JSONObject) value;
                    this.DebitMemoInfo = new DeliveryForm_DebitMemoInfo();
                    this.DebitMemoInfo.readFromJSONObject(jsonObject);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
        else  if (key.equalsIgnoreCase("DeliveryEquipmentInfo")){
            if (DMUtils.ValidObject(value)){
                try {
                    JSONObject jsonObject = (JSONObject) value;
                    this.DeliveryEquipmentInfo = new DeliveryForm_DeliveryEquipmentInfo();
                    this.DeliveryEquipmentInfo.readFromJSONObject(jsonObject);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
        else  if (key.equalsIgnoreCase("PaymentCollectionInfo")){
            if (DMUtils.ValidObject(value)){
                try {
                    JSONObject jsonObject = (JSONObject) value;
                    this.PaymentCollectionInfo = new DeliveryForm_PaymentCollectionInfo();
                    this.PaymentCollectionInfo.readFromJSONObject(jsonObject);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
        else  if (key.equalsIgnoreCase("StoreReady")){
            if (DMUtils.ValidObject(value)){
                try {
                    JSONObject jsonObject = (JSONObject) value;
                    this.StoreReady = new DeliveryForm_StoreReady();
                    this.StoreReady.readFromJSONObject(jsonObject);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
        else  if (key.equalsIgnoreCase("StopInvoiceItemInfo")){
            if (DMUtils.ValidObject(value)){
                try {
                    this.StopInvoiceItemInfo = new ArrayList<>();
                    JSONArray jsonArray = (JSONArray) value;
                    for (int i = 0; i <jsonArray.length() ; i++) {
                        try {
                            DeliveryForm_StopInvoiceItem item = new DeliveryForm_StopInvoiceItem();
                            item.readFromJSONObject(jsonArray.getJSONObject(i));
                            this.StopInvoiceItemInfo.add(item);
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
        else  if (key.equalsIgnoreCase("PaymentCollectionInfoList")){
            if (DMUtils.ValidObject(value)){
                try {
                    this.PaymentCollectionInfoList = new ArrayList<>();
                    JSONArray jsonArray = (JSONArray) value;
                    for (int i = 0; i <jsonArray.length() ; i++) {
                        try {
                            DeliveryForm_PaymentCollectionInfo item = new DeliveryForm_PaymentCollectionInfo();
                            item.readFromJSONObject(jsonArray.getJSONObject(i));
                            this.PaymentCollectionInfoList.add(item);
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
        else  if (key.equalsIgnoreCase("PODCameraImageAttachments")){
            if (DMUtils.ValidObject(value)){
                try {
                    this.PODCameraImageAttachments = new ArrayList<>();
                    JSONArray jsonArray = (JSONArray) value;
                    for (int i = 0; i <jsonArray.length() ; i++) {
                        try {
                            ImageInfo item = new ImageInfo();
                            item.readFromJSONObject(jsonArray.getJSONObject(i));
                            this.PODCameraImageAttachments.add(item);
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
        else  if (key.equalsIgnoreCase("CreditMemoCameraImageAttachments")){
            if (DMUtils.ValidObject(value)){
                try {
                    this.CreditMemoCameraImageAttachments = new ArrayList<>();
                    JSONArray jsonArray = (JSONArray) value;
                    for (int i = 0; i <jsonArray.length() ; i++) {
                        try {
                            ImageInfo item = new ImageInfo();
                            item.readFromJSONObject(jsonArray.getJSONObject(i));
                            this.CreditMemoCameraImageAttachments.add(item);
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
        else  if (key.equalsIgnoreCase("DebitMemoCameraImageAttachments")){
            if (DMUtils.ValidObject(value)){
                try {
                    this.DebitMemoCameraImageAttachments = new ArrayList<>();
                    JSONArray jsonArray = (JSONArray) value;
                    for (int i = 0; i <jsonArray.length() ; i++) {
                        try {
                            ImageInfo item = new ImageInfo();
                            item.readFromJSONObject(jsonArray.getJSONObject(i));
                            this.DebitMemoCameraImageAttachments.add(item);
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
        else  if (key.equalsIgnoreCase("StoreBarcodeCameraImageAttachments")){
            if (DMUtils.ValidObject(value)){
                try {
                    this.StoreBarcodeCameraImageAttachments = new ArrayList<>();
                    JSONArray jsonArray = (JSONArray) value;
                    for (int i = 0; i <jsonArray.length() ; i++) {
                        try {
                            ImageInfo item = new ImageInfo();
                            item.readFromJSONObject(jsonArray.getJSONObject(i));
                            this.StoreBarcodeCameraImageAttachments.add(item);
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
        else  if (key.equalsIgnoreCase("ClosedDoorCameraImageAttachments")){
            if (DMUtils.ValidObject(value)){
                try {
                    this.ClosedDoorCameraImageAttachments = new ArrayList<>();
                    JSONArray jsonArray = (JSONArray) value;
                    for (int i = 0; i <jsonArray.length() ; i++) {
                        try {
                            ImageInfo item = new ImageInfo();
                            item.readFromJSONObject(jsonArray.getJSONObject(i));
                            this.ClosedDoorCameraImageAttachments.add(item);
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
        else {
            super.setValueForKey(value, key);
        }
    }
}
