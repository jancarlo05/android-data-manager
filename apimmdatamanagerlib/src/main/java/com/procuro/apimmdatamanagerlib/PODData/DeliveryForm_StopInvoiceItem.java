package com.procuro.apimmdatamanagerlib.PODData;

import com.procuro.apimmdatamanagerlib.PimmBaseObject;
import com.procuro.apimmdatamanagerlib.RouteShipmentInvoiceItem;
import com.procuro.apimmdatamanagerlib.RouteStatusInvoiceItem;

import java.util.HashMap;
import java.util.List;


public class DeliveryForm_StopInvoiceItem extends PimmBaseObject {

    public String RouteShipmentInvoiceItemID;
    public double Amount;
    public RouteShipmentInvoiceItem.RouteShipmentInvoiceItemCategoryEnum Category;
    public String Description;
    public String CustomerInvoiceItemID;
    public String PO;

    @Override
    public void setValueForKey(Object value, String key) {

        if (key.equalsIgnoreCase("Category")) {
            if (value != null) {
                try {
                    this.Category = RouteShipmentInvoiceItem.RouteShipmentInvoiceItemCategoryEnum.setIntValue(Integer.parseInt(value.toString()));
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

        }else{
            super.setValueForKey(value, key);
        }
    }

    public HashMap<String,Object> dictionaryWithValuesForKeys() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();

        dictionary.put("RouteShipmentInvoiceItemID", this.RouteShipmentInvoiceItemID);
        dictionary.put("Amount", this.Amount);
        dictionary.put("Category", this.Category);
        dictionary.put("Description", this.Description);
        dictionary.put("CustomerInvoiceItemID", this.CustomerInvoiceItemID);
        dictionary.put("PO", this.PO);

        return  dictionary;
    }
}
