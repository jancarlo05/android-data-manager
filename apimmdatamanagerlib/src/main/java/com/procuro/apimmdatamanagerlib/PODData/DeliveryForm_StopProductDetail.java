package com.procuro.apimmdatamanagerlib.PODData;

import android.util.Log;

import com.procuro.apimmdatamanagerlib.DMUtils;
import com.procuro.apimmdatamanagerlib.DateTimeFormat;
import com.procuro.apimmdatamanagerlib.PimmBaseObject;
import com.procuro.apimmdatamanagerlib.ProductCategoryEnum;
import com.procuro.apimmdatamanagerlib.ProductCompartmentEnum.ProductCompartmentsEnum;
import com.procuro.apimmdatamanagerlib.RouteShipmentProduct;
import com.procuro.apimmdatamanagerlib.RouteShipmentUnit;
import com.procuro.apimmdatamanagerlib.SdrStopProduct;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static com.procuro.apimmdatamanagerlib.ProductCategoryEnum.ProductCategorysEnum.Category_None;


public class DeliveryForm_StopProductDetail extends PimmBaseObject {

   public   String Group;
    public String ProductDescription;
    public String Unit;
    public String AltPO;
    public String Location;
    public String RouteShipmentID;
    public String ItemNo;
    public String BarcodeIdentifier; //14 Digit GTIN-14 identifier for product
    public String ProductionDate; // YYMMDD format date from GS1-128 Barcode
    public String PackagingDate;
    public String SellByDate;
    public String ExpirationDate;
    public String LotID;
    public String DeliveryNote;
    public String BestBefore;
    public String ReturnDate;
    public String PO;
    public String ReasonCode;
    public String ReasonCodeText;
    public String BarcodeDiscrepancyReasonCode;
    public String Temp;
    public String UserField1;
    public String UserField2;
    public String UserField3;
    public String ApprovedBy;

    public int ShortPlus;
    public int DeliveryType;
    public int QuantityOrdered;
    public int QuantityShipped;
    public int Sort;
    public int DuplicateScans;
    public int QuantityReturned;

    public double CatchWeight;
    public double UnitWeight;
    public double UnitPrice;
    public double TotalWeight;
    public double QuantityReceived;
    public double QuantityScanned;

    public ProductCategorysEnum Category;

    public boolean hasManualQuantityReceived;
    public boolean Status;
    public boolean returnedItemInOriginalPackaging;

    public Map<String,List<Object>> barcodeIdentifierList;
    public Map<String,List<Object>> barcodeLog;

    //Custom
    public boolean ChecklistStatus;


    @Override
    public void setValueForKey(Object value, String key) {

        if (key.equalsIgnoreCase("Category")){
            if (DMUtils.ValidObject(value)){
                try {
//                    BigInteger bigInteger = new BigInteger(value.toString());
//                    if (bigInteger.intValue()>=0){
//                        this.Category = ProductCategorysEnum.setIntValue(bigInteger.intValue());
//                    }
                }catch (Exception ignored){
                    ignored.printStackTrace();
                }
            }
        }
        else if (key.equalsIgnoreCase("barcodeIdentifierList")){
            this.barcodeIdentifierList = new HashMap<>();
            if (DMUtils.ValidObject(value)){
                try {
                    for(int i =1; i< QuantityShipped; i++){
                        String numStr = String.format("%03d",i);
                        //First value in array indicated Quantity Scanned and Second Value indicates QuantityReceived
                        List<Object> array = new ArrayList<Object>();
                        array.add(0);
                        array.add(0);
                        barcodeIdentifierList.put(numStr, array);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
        else if (key.equalsIgnoreCase("CatchWeight")){
            if (DMUtils.ValidObject(value)){
                try {
                    this.CatchWeight = Double.parseDouble(value.toString());
                }catch (Exception ignored){
                    ignored.printStackTrace();
                }
            }
        }
        else if (key.equalsIgnoreCase("UnitWeight")){
            if (DMUtils.ValidObject(value)){
                try {
                    this.UnitWeight = Double.parseDouble(value.toString());
                }catch (Exception ignored){
                    ignored.printStackTrace();
                }
            }
        }

        else if (key.equalsIgnoreCase("UnitPrice")){
            if (DMUtils.ValidObject(value)){
                try {
                    this.UnitPrice = Double.parseDouble(value.toString());
                }catch (Exception ignored){
                    ignored.printStackTrace();
                }
            }
        }

        else if (key.equalsIgnoreCase("TotalWeight")){
            if (DMUtils.ValidObject(value)){
                try {
                    this.TotalWeight = Double.parseDouble(value.toString());
                }catch (Exception ignored){
                    ignored.printStackTrace();
                }
            }
        }
        else if (key.equalsIgnoreCase("QuantityReceived")){
            if (DMUtils.ValidObject(value)){
                try {
                    this.QuantityReceived = Double.parseDouble(value.toString());
                }catch (Exception ignored){
                    ignored.printStackTrace();
                }
            }
        }

        else if (key.equalsIgnoreCase("QuantityScanned")){
            if (DMUtils.ValidObject(value)){
                try {
                    this.QuantityScanned = Double.parseDouble(value.toString());
                }catch (Exception ignored){
                    ignored.printStackTrace();
                }
            }
        }
        else if (key.equalsIgnoreCase("Status")){
            if (DMUtils.ValidObject(value)){
                try {
                    this.Status = DMUtils.ConvertToBoolean(value);
                    this.ChecklistStatus = DMUtils.ConvertToBoolean(value);
                }catch (Exception ignored){
                    ignored.printStackTrace();
                }
            }
        }
        else if (key.equalsIgnoreCase("PO")){
            if (DMUtils.ValidObject(value)){
                try {
                    System.out.println("PO VALUE : "+value.toString());
                    this.PO = value.toString();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
        else if (key.equalsIgnoreCase("returnedItemInOriginalPackaging")){
            if (DMUtils.ValidObject(value)){
                try {
                    this.returnedItemInOriginalPackaging = DMUtils.ConvertToBoolean(value);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }


        else {
            super.setValueForKey(value, key);
        }
    }

    public enum ProductCategorysEnum {
        Category_None(0),
        Category_Food(1),
        Category_Unknown2(2),
        Category_Paper(3),
        Category_Cleaning(4),
        Category_Uniform(5),
        Category_Replacement(6),
        Category_Other(7),
        Category_Taxes(8);

        private int value;
        private ProductCategorysEnum(int value)
        {
            this.value = value;
        }


        public int getValue() {
            return this.value;
        }

        public static   ProductCategorysEnum setIntValue (int i) {
            for (ProductCategorysEnum type :
                    ProductCategorysEnum.values()) {
                if (type.getValue() == i) {
                    return type;
                }
            }
            return Category_None;
        }
    }

    public HashMap<String,Object> dictionaryWithValuesForKeys(){
        HashMap<String, Object> dictionary = new HashMap<String, Object>();

        dictionary.put("ShortPlus", this.ShortPlus);
        dictionary.put("DeliveryType", this.DeliveryType);
        dictionary.put("Group", this.Group);
        dictionary.put("ProductDescription", this.ProductDescription);
        dictionary.put("DeliveryNote", this.DeliveryNote);
        dictionary.put("BestBefore", this.BestBefore);
        dictionary.put("Unit", this.Unit);
        dictionary.put("QuantityOrdered", this.QuantityOrdered);
        dictionary.put("ReasonCode", this.ReasonCode);
        dictionary.put("Temp", this.Temp);
        dictionary.put("PO", this.PO);
        dictionary.put("AltPO", this.AltPO);
        dictionary.put("Location", this.Location);
        dictionary.put("UnitWeight", this.UnitWeight);
        dictionary.put("UnitPrice", this.UnitPrice);
        dictionary.put("TotalWeight", this.TotalWeight);
        dictionary.put("QuantityShipped", this.QuantityShipped);
        dictionary.put("RouteShipmentID", this.RouteShipmentID);
        dictionary.put("ItemNo", this.ItemNo);
        dictionary.put("BarcodeIdentifier", this.BarcodeIdentifier);
        dictionary.put("ProductionDate", this.ProductionDate);
        dictionary.put("PackagingDate", this.PackagingDate);
        dictionary.put("SellByDate", this.SellByDate);
        dictionary.put("ExpirationDate", this.ExpirationDate);
        dictionary.put("LotID", this.LotID);
        dictionary.put("BarcodeDiscrepancyReasonCode", this.BarcodeDiscrepancyReasonCode);
        dictionary.put("Status", this.Status);
        dictionary.put("CatchWeight", this.CatchWeight);

        dictionary.put("QuantityReceived", DateTimeFormat.truncateDecimal(this.QuantityReceived, 2));
        dictionary.put("QuantityScanned", DateTimeFormat.truncateDecimal(this.QuantityScanned, 2));

        dictionary.put("QuantityReturned", DateTimeFormat.truncateDecimal(this.QuantityReturned, 2));

        dictionary.put("barcodeLog", this.barcodeLog);
        dictionary.put("barcodeIdentifierList", this.barcodeIdentifierList);

        dictionary.put("returnedItemInOriginalPackaging", this.returnedItemInOriginalPackaging);

        dictionary.put("Category", (this.Category == null ) ? Category_None : this.Category);



        return dictionary;
    }


}
