package com.procuro.apimmdatamanagerlib.PODData;

import com.procuro.apimmdatamanagerlib.CompartmentDTO;
import com.procuro.apimmdatamanagerlib.PimmBaseObject;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by jophenmarieweeks on 22/08/2017.
 */

public class BarcodeScanSessionInfo extends PimmBaseObject {

//    @property (nonatomic,assign) unsigned int sessionId;
     public int sessionId;

    public String deliveryStopID;
    public String PO;
    public Date sessionStartTime;
    public Date sessionEndTime;
    public ArrayList<BarcodeLogEntry> barcodeList;

    @Override
    public void setValueForKey(Object value, String key) {

        if (key.equalsIgnoreCase("barcodeList")) {
            if (this.barcodeList == null) {
                this.barcodeList = new ArrayList<BarcodeLogEntry>();
            }

            if (!value.equals(null)) {
                JSONArray arrStops = (JSONArray) value;

                if (arrStops != null) {
                    for (int i = 0; i < arrStops.length(); i++) {
                        try {
                            BarcodeLogEntry barcodeLogEntry = new BarcodeLogEntry();
                            barcodeLogEntry.readFromJSONObject(arrStops.getJSONObject(i));

                            this.barcodeList.add(barcodeLogEntry);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } else {
                super.setValueForKey(value, key);
            }
        }
    }

    public HashMap<String,Object> dictionaryWithValuesForKeys() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();


        dictionary.put("sessionId",this.sessionId);
        dictionary.put("deliveryStopID",this.deliveryStopID);
        dictionary.put("PO",this.PO);
        dictionary.put("sessionStartTime",this.sessionStartTime);
        dictionary.put("sessionEndTime",this.sessionEndTime);

        if (this.barcodeList != null && this.barcodeList.size() > 0) {

            ArrayList<HashMap<String, Object>> BarcodeLogEntryDicArray = new ArrayList<>();

            for(BarcodeLogEntry result : this.barcodeList) {

                HashMap<String, Object> resultDic = result.dictionaryWithValuesForKeys();
                BarcodeLogEntryDicArray.add(resultDic);

            }

            dictionary.put("barcodeList", BarcodeLogEntryDicArray);

        }else{
            dictionary.put("barcodeList", null);
        }


        return dictionary;

    }
}


