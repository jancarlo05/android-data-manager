package com.procuro.apimmdatamanagerlib.PODData;

import android.util.Log;

import com.procuro.apimmdatamanagerlib.DMUtils;
import com.procuro.apimmdatamanagerlib.JSONDate;
import com.procuro.apimmdatamanagerlib.PimmBaseObject;
import com.procuro.apimmdatamanagerlib.PropertyValue;
import com.procuro.apimmdatamanagerlib.SdrStop;
import com.procuro.apimmdatamanagerlib.SdrStopMetric;
import com.procuro.apimmdatamanagerlib.SiteSettings;

import org.json.JSONArray;
import org.json.JSONException;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import static com.procuro.apimmdatamanagerlib.SdrMetric.MetricCompartment.MetricCompartment_Cooler;
import static com.procuro.apimmdatamanagerlib.SdrMetric.MetricCompartment.MetricCompartment_Dry;
import static com.procuro.apimmdatamanagerlib.SdrMetric.MetricCompartment.MetricCompartment_Freezer;

public class DeliveryForm_StoreInfo extends PimmBaseObject {

    public Double C1; //double
    public Double C2; //double
    public Double C3; //double

    public String C1Label;
    public String C2Label;
    public String C3Label;
    public String BillTo;

    public int LateReasonCode; //int
    public int MinutesLate; //int

    public Date DepartureTime;
    public Date ArrivalActualTime;
    public Date ArrivalScheduleTime;
    public Date C1Timestamp;
    public Date C2Timestamp;
    public Date C3Timestamp;

    public SdrStop.OnTimeStatus OnTime;
    public ArrayList<DeliveryForm_Metrics> Metrics;

    @Override
    public void setValueForKey(Object value, String key) {

        if (key.equalsIgnoreCase("Metrics")) {
            if (value != null) {
                Metrics = new ArrayList<>();
                JSONArray arrStops = (JSONArray) value;
                for (int i = 0; i < arrStops.length(); i++) {
                    try {
                        DeliveryForm_Metrics deliveryFormMetrics = new DeliveryForm_Metrics();
                        deliveryFormMetrics.readFromJSONObject(arrStops.getJSONObject(i));
                        this.Metrics.add(deliveryFormMetrics);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            }
        }

        else if (key.equalsIgnoreCase("OnTime")) {
                if (value != null) {
                    SdrStop.OnTimeStatus onTimeStatus = SdrStop.OnTimeStatus.setIntValue((int) value);
                    this.OnTime = onTimeStatus;
                }
        }

        else if (key.equalsIgnoreCase("DepartureTime")){
            if (value!=null){
                this.DepartureTime = JSONDate.convertJSONDateToNSDate(value.toString());
            }
        }
        else if (key.equalsIgnoreCase("ArrivalActualTime")){
            if (value!=null){
                this.ArrivalActualTime = JSONDate.convertJSONDateToNSDate(value.toString());
            }
        }
        else if (key.equalsIgnoreCase("ArrivalScheduleTime")){
            if (value!=null){
                this.ArrivalScheduleTime = JSONDate.convertJSONDateToNSDate(value.toString());
            }
        }
        else if (key.equalsIgnoreCase("C1Timestamp")){
            if (value!=null){
                this.C1Timestamp = JSONDate.convertJSONDateToNSDate(value.toString());
            }
        } else if (key.equalsIgnoreCase("C2Timestamp")){
            if (value!=null){
                this.C2Timestamp = JSONDate.convertJSONDateToNSDate(value.toString());
            }
        }
        else if (key.equalsIgnoreCase("C3Timestamp")){
            if (value!=null){
                this.C3Timestamp = JSONDate.convertJSONDateToNSDate(value.toString());
            }
        }

        else if (key.equalsIgnoreCase("C1")){
            if (DMUtils.ValidObject(value)){
                try {
                    this.C1 = Double.parseDouble(value.toString());
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }

        else if (key.equalsIgnoreCase("C2")){
            if (DMUtils.ValidObject(value)){
                try {
                    this.C2 = Double.parseDouble(value.toString());
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }

        else if (key.equalsIgnoreCase("C3")){
            if (DMUtils.ValidObject(value)){
                try {
                    this.C3 = Double.parseDouble(value.toString());
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }

        else {
            super.setValueForKey(value, key);
        }
    }


}
