package com.procuro.apimmdatamanagerlib.PODData;

import android.util.Log;

import com.procuro.apimmdatamanagerlib.DMUtils;
import com.procuro.apimmdatamanagerlib.PimmBaseObject;
import com.procuro.apimmdatamanagerlib.RouteShipmentProduct;
import com.procuro.apimmdatamanagerlib.RouteShipmentUnit;
import com.procuro.apimmdatamanagerlib.SdrStop;
import com.procuro.apimmdatamanagerlib.SdrStopProduct;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DeliveryForm_DebitMemoInfo extends PimmBaseObject {


    public String DebitMemoComment;
    public ArrayList<DeliveryForm_StopProductDetail> DebitProductArray;
    public boolean Verified;


    @Override
    public void setValueForKey(Object value, String key) {

        if (key.equalsIgnoreCase("DebitProductArray")) {
            this.DebitProductArray = new ArrayList<>();
            if (DMUtils.ValidObject(value)){
                if (value instanceof  JSONArray){
                    JSONArray arrStops = (JSONArray) value;
                    for (int i = 0; i < arrStops.length(); i++) {
                        try {
                            DeliveryForm_StopProductDetail deliveryForm_stopProductDetail = new DeliveryForm_StopProductDetail();
                            deliveryForm_stopProductDetail.readFromJSONObject(arrStops.getJSONObject(i));
                            this.DebitProductArray.add(deliveryForm_stopProductDetail);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }else if (value instanceof JSONObject){
                    JSONObject jsonObject = (JSONObject) value;
                    DeliveryForm_StopProductDetail deliveryForm_stopProductDetail = new DeliveryForm_StopProductDetail();
                    deliveryForm_stopProductDetail.readFromJSONObject(jsonObject);
                    this.DebitProductArray.add(deliveryForm_stopProductDetail);
                }
            }
        }
        else {
            super.setValueForKey(value, key);
        }
    }


}
