package com.procuro.apimmdatamanagerlib.PODData;

import android.util.Base64;

import com.procuro.apimmdatamanagerlib.DMUtils;
import com.procuro.apimmdatamanagerlib.PimmBaseObject;
import com.procuro.apimmdatamanagerlib.SdrStop;

import java.util.HashMap;

import static com.procuro.apimmdatamanagerlib.PODData.DeliveryForm_PaymentCollectionInfo.PaymentMethodEnum.PaymentMethod_Cash;
import static com.procuro.apimmdatamanagerlib.SdrStop.PaymentTerms.PaymentTerms_Net;

public class DeliveryForm_PaymentCollectionInfo extends PimmBaseObject {



    public String transactionId;
    public String driverComments;
    public String spokeWith;
    public String creditCardType;
    public String last4Digits;

    public double paymentAmount;
    public double totalInvoiceAmount;

    public byte[] imageData; // NSData
    public byte[] signatureData; //NSData


    public PaymentMethodEnum paymentMethod;
    public SdrStop.PaymentTerms paymentTerms;

    @Override
    public void setValueForKey(Object value, String key) {

        if (key.equalsIgnoreCase("paymentMethod")) {
            if (value != null) {
                try {
                    this.paymentMethod = PaymentMethodEnum.setIntValue(Integer.parseInt(value.toString()));
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

        } else if (key.equalsIgnoreCase("paymentTerms")) {
            if (value != null) {
                try {
                    this.paymentTerms = SdrStop.PaymentTerms.setIntValue(Integer.parseInt(value.toString()));

                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }else if (key.equalsIgnoreCase("imageData")){
            if (value != null) {
                try {
                    this.imageData = DMUtils.ConvertStringToByteArray(value.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }else if (key.equalsIgnoreCase("signatureData")){
            if (value != null) {
                try {
                    this.signatureData = DMUtils.ConvertStringToByteArray(value.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        } else {
            super.setValueForKey(value, key);
        }
    }

    public enum PaymentMethodEnum {
        PaymentMethod_Cash(0),
        PaymentMethod_CashierCheque(1),
        PaymentMethod_MoneyOrder(2),
        PaymentMethod_PersonalCheque(3),
        PaymentMethod_CreditCard(4),
        PaymentMethod_Billing(5),
        PaymentMethod_NoPaymentAvailable_Deliver(6),
        PaymentMethod_NoPaymentAvailable_DoNotDeliver(7);

        public int value;

        private PaymentMethodEnum(int value)
        {
            this.value = value;
        }
        public int getValue() {
            return this.value;
        }

        public static PaymentMethodEnum setIntValue (int i) {
            for (PaymentMethodEnum type : PaymentMethodEnum.values()) {
                if (type.value == i) { return type; }
            }
            return PaymentMethod_Cash;
        }

    }



    public HashMap<String,Object> dictionaryWithValuesForKeys() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();

        dictionary.put("paymentTerms", (this.paymentTerms == null) ? PaymentTerms_Net : this.paymentTerms.getValue() );
        dictionary.put("transactionId", this.transactionId);
        dictionary.put("paymentAmount", this.paymentAmount);
        dictionary.put("totalInvoiceAmount", this.totalInvoiceAmount);
        dictionary.put("paymentMethod", (this.paymentMethod == null) ? PaymentMethod_Cash : this.paymentMethod.value);
        dictionary.put("driverComments", this.driverComments);

        dictionary.put("spokeWith", this.spokeWith);
        dictionary.put("creditCardType", this.creditCardType);
        dictionary.put("last4Digits", this.last4Digits);


        if (this.imageData != null){
            String base64String = Base64.encodeToString(this.imageData, Base64.DEFAULT);
            dictionary.put("imageData", base64String );
        }else{
            dictionary.put("imageData", null);
        }

        if (this.signatureData != null){
            String base64String = Base64.encodeToString(this.signatureData, Base64.DEFAULT);
            dictionary.put("signatureData", base64String );
        }else{
            dictionary.put("signatureData", null);
        }


        return dictionary;
    }

}
