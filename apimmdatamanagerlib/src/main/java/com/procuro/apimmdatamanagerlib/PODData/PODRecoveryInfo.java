package com.procuro.apimmdatamanagerlib.PODData;

import com.procuro.apimmdatamanagerlib.PimmBaseObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by jophenmarieweeks on 04/09/2017.
 */

public class PODRecoveryInfo extends PimmBaseObject {

    public class PODRecoveryStopInfo extends PimmBaseObject{
        public String  siteName;
        public String  deliveryStopId;
        public Date actualArrivalTime;

        public HashMap<String,Object> dictionaryWithValuesForKeys() {
            HashMap<String, Object> dictionary = new HashMap<String, Object>();


            dictionary.put("siteName", this.siteName);
            dictionary.put("deliveryStopId", this.deliveryStopId);
            dictionary.put("actualArrivalTime", this.actualArrivalTime);
        return dictionary;
        }

    }

    public String  deliveryId;
    public String  routeName;
    public String  driverName;
    public String  trailerName;
    Map<String, String> driverComments;
//    @property (nonatomic,strong) NSMutableDictionary* driverComments;
    public ArrayList<PODRecoveryStopInfo> stopList; //Array of PODRecoveryStopInfo objects

    @Override
    public void setValueForKey(Object value, String key) {

        if (key.equalsIgnoreCase("stopList")) {
            if (this.stopList == null) {
                this.stopList = new ArrayList<PODRecoveryStopInfo>();
            }

            if (!value.equals(null)) {
                if (value instanceof JSONArray) {

                    JSONArray arrData = (JSONArray) value;

                    if (arrData != null) {
                        for (int i = 0; i < arrData.length(); i++) {
                            try {
                                PODRecoveryStopInfo podRecoveryStopInfo = new PODRecoveryStopInfo();
                                podRecoveryStopInfo.readFromJSONObject(arrData.getJSONObject(i));

                                this.stopList.add(podRecoveryStopInfo);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    }

                }
            }
        } else {
            super.setValueForKey(value, key);
        }
    }

    public HashMap<String,Object> dictionaryWithValuesForKeys() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();

        dictionary.put("deliveryId", this.deliveryId);
        dictionary.put("routeName", this.routeName);
        dictionary.put("driverName", this.driverName);
        dictionary.put("trailerName", this.trailerName);
        dictionary.put("DriverComments", (this.driverComments == null) ? null : this.driverComments);

        if(this.stopList != null && this.stopList.size() > 0 ){
            ArrayList<HashMap<String, Object>> PODRecoveryStopInfoList = new ArrayList<>();

            for(PODRecoveryStopInfo  podRecoveryStopInfo : this.stopList) {

                HashMap<String, Object> podRecoveryStopInfoDict = podRecoveryStopInfo.dictionaryWithValuesForKeys();
                PODRecoveryStopInfoList.add(podRecoveryStopInfoDict);

            }

            dictionary.put("stopList", PODRecoveryStopInfoList);

        }else{
            dictionary.put("stopList", null);
        }

        return dictionary;
    }

}
