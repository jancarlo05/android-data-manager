package com.procuro.apimmdatamanagerlib.PODData;

import com.procuro.apimmdatamanagerlib.PimmBaseObject;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DeliveryForm_DeliveryEquipmentInfo extends PimmBaseObject {

    public ArrayList<DeliveryForm_EquipmentDetails> EquipmentDetails;
    public String deliveryEquipmentComment;
    public boolean Verified;

    @Override
    public void setValueForKey(Object value, String key) {

        if (key.equalsIgnoreCase("EquipmentDetails")) {
            this.EquipmentDetails = new ArrayList<>();
            if (value!=null){
                JSONArray arrStops = (JSONArray) value;
                for (int i = 0; i < arrStops.length(); i++) {
                    try {
                        DeliveryForm_EquipmentDetails deliveryForm_equipmentDetails = new DeliveryForm_EquipmentDetails();
                        deliveryForm_equipmentDetails.readFromJSONObject(arrStops.getJSONObject(i));
                        this.EquipmentDetails.add(deliveryForm_equipmentDetails);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        } else {
            super.setValueForKey(value, key);
        }
    }
}
