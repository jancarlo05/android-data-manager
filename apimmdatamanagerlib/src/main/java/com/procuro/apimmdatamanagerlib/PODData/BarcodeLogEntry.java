package com.procuro.apimmdatamanagerlib.PODData;

import android.util.Base64;

import com.procuro.apimmdatamanagerlib.GS1BarcodeComponents;
import com.procuro.apimmdatamanagerlib.PimmBaseObject;

import org.json.JSONObject;

import java.util.HashMap;

import static com.procuro.apimmdatamanagerlib.PODData.BarcodeLogEntry.InvalidBarcodeTypeEnum.InvalidBarcode_Unknown;

/**
 * Created by jophenmarieweeks on 22/08/2017.
 */

public class BarcodeLogEntry extends PimmBaseObject {

    public enum BarcodeType_Enum
    {
        BarcodeType_Unknown(-1),
        BarcodeType_UPC(0),
        BarcodeType_GS1_128(1),
        BarcodeType_Custom_SFM(2);

        private int value;
        private BarcodeType_Enum(int value)
        {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }

        public static BarcodeType_Enum setIntValue (int i) {
            for (BarcodeType_Enum type : BarcodeType_Enum.values()) {
                if (type.value == i) { return type; }
            }
            return BarcodeType_Unknown;
        }


    }

   public enum InvalidBarcodeTypeEnum
    {
        InvalidBarcode_Unknown(-1),
        InvalidBarcode_NotInPO(0),
        InvalidBarcode_UnknownItem(1),
        InvalidBarcode_LotMismatch(2),
        InvalidBarcode_ProductionDateMismatch(3),
        InvalidBarcode_SellByDateMismatch(4),
        InvalidBarcode_ExpirationDateMismatch(5),
        InvalidBarcode_CatchWeightMismatch(6),
        InvalidBarcode_Over(7),
        InvalidBarcode_DuplicateScan(8),
        InvalidBarcode_UnsuppportedFormat(9);

        private int value;
        private InvalidBarcodeTypeEnum(int value)
        {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }

        public static InvalidBarcodeTypeEnum setIntValue (int i) {
            for (InvalidBarcodeTypeEnum type : InvalidBarcodeTypeEnum.values()) {
                if (type.value == i) { return type; }
            }
            return InvalidBarcode_Unknown;
        }


    }


    public int barcodeEntryId;
    public String barcode;
    public boolean matchFound;
    public String GTIN14;
    public BarcodeType_Enum barcodeType;
    public double quantityReceived;
    public double quantityScanned;
    public String comments;
    public String itemID;
    public String itemName;
    public String PO;
    public String deliveryStopId;
    public String deliveryId;

    public boolean valid;
    public byte[] imageData;
    public InvalidBarcodeTypeEnum invalidBarcodeType;
    public GS1BarcodeComponents gs1BarcodeComponents;
    public String numOfValueStr;


    @Override
    public void setValueForKey(Object value, String key) {

        if(key.equalsIgnoreCase("barcodeType")) {
            if (!value.equals(null)) {
                BarcodeType_Enum barcodeType_enum = BarcodeType_Enum.setIntValue((int) value);
                this.barcodeType = barcodeType_enum;

            }
        }else if(key.equalsIgnoreCase("invalidBarcodeType")){
            if (!value.equals(null)) {
                InvalidBarcodeTypeEnum invalidBarcodeTypeEnum = InvalidBarcodeTypeEnum.setIntValue((int) value);
                this.invalidBarcodeType = invalidBarcodeTypeEnum;

            }

        }else if(key.equalsIgnoreCase("gs1BarcodeComponents")){

            if(!value.equals(null)) {
                GS1BarcodeComponents gs1BarcodeComponents = new GS1BarcodeComponents();
                JSONObject jsonObject = (JSONObject) value;
                gs1BarcodeComponents.readFromJSONObject(jsonObject);
                this.gs1BarcodeComponents = gs1BarcodeComponents;
            }
        } else {
            super.setValueForKey(value, key);
        }
    }

    public HashMap<String,Object> dictionaryWithValuesForKeys() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();

        dictionary.put("barcodeEntryId", this.barcodeEntryId);
        dictionary.put("barcode", this.barcode);
        dictionary.put("matchFound", this.matchFound);
        dictionary.put("quantityReceived", this.quantityReceived);
        dictionary.put("quantityScanned", this.quantityScanned);
        dictionary.put("comments", this.comments);
        dictionary.put("itemID", this.itemID);
        dictionary.put("itemName", this.itemName);
        dictionary.put("PO", this.PO);
        dictionary.put("valid", this.valid);
        dictionary.put("deliveryStopId", this.deliveryStopId);
        dictionary.put("deliveryId", this.deliveryId);
        dictionary.put("numOfValueStr", this.numOfValueStr);
        dictionary.put("barcodeType", (this.barcodeType == null) ? InvalidBarcode_Unknown : this.barcodeType.value);

        if (this.imageData != null){
            String base64String = Base64.encodeToString(this.imageData, Base64.DEFAULT);
            dictionary.put("imageData", base64String );
        }else{
            dictionary.put("imageData", null);
        }

        dictionary.put("invalidBarcodeType", (this.invalidBarcodeType == null) ? InvalidBarcode_Unknown : this.invalidBarcodeType.value);
        dictionary.put("gs1BarcodeComponents", (this.gs1BarcodeComponents == null) ? null : this.gs1BarcodeComponents.dictionaryWithValuesForKeys());

        return  dictionary;
    }


}
