package com.procuro.apimmdatamanagerlib.PODData;

import com.procuro.apimmdatamanagerlib.PimmBaseObject;
import com.procuro.apimmdatamanagerlib.SdrStop;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import static com.procuro.apimmdatamanagerlib.PODData.DeliveryPerformance.ReturnedProductTypeEnum.ReturnType_Credit;
import static com.procuro.apimmdatamanagerlib.SdrStop.OnTimeStatus.OnTimeStatus_None;

/**
 * Created by jophenmarieweeks on 17/08/2017.
 */

public class DeliveryPerformance extends PimmBaseObject {

   public enum ReturnedProductTypeEnum {
        ReturnType_Credit(0),
        ReturnType_Rejected(1);

       private int value;
       private ReturnedProductTypeEnum( int value){
           this.value = value;
       }

       public int getValue() {
           return this.value;
       }

       public static ReturnedProductTypeEnum setIntValue (int i) {
           for (ReturnedProductTypeEnum type : ReturnedProductTypeEnum.values()) {
               if (type.value == i) { return type; }
           }
           return ReturnType_Credit;
       }
    }


    public String stopName;
    public String deliveryStopId;
    public int stopOrder;
    public double casesPerMinute;
    public int scheduledDuration; //duration in minutes
    public int actualDuration; //duration in minutes
    public int minutesOver;

    public int reasonCode;

    public HashMap<String,Object> dictionaryWithValuesForKeys() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();

        dictionary.put("stopName", this.stopName);
        dictionary.put("stopOrder", this.stopOrder);
        dictionary.put("deliveryStopId", this.deliveryStopId);
        dictionary.put("stopOrder", this.stopOrder);
        dictionary.put("casesPerMinute", this.casesPerMinute);
        dictionary.put("scheduledDuration", this.scheduledDuration);
        dictionary.put("actualDuration", this.actualDuration);
        dictionary.put("minutesOver", this.minutesOver);
        dictionary.put("reasonCode", this.reasonCode);

        return  dictionary;
    }
    public void print(){
        HashMap<String, Object> dictionary = dictionaryWithValuesForKeys();
    }





    public static class StopPerformance extends PimmBaseObject {


        public String stopName;
        public int stopOrder;
        public SdrStop.OnTimeStatus violationtype;
        public Date scheduledDeliveryStartTime;
        public Date actualDeliveryStartTime;
        public int minutesLateOrEarly;
        public int reasonCode;

        @Override
        public void setValueForKey(Object value, String key) {

            if(key.equalsIgnoreCase("violationtype")) {
                if (!value.equals(null)) {
                    SdrStop.OnTimeStatus onTimeStatus = SdrStop.OnTimeStatus.setIntValue((int) value);
                    this.violationtype = onTimeStatus;
                }
            } else {
                super.setValueForKey(value, key);
            }
        }

        public HashMap<String,Object> dictionaryWithValuesForKeys() {
            HashMap<String, Object> dictionary = new HashMap<String, Object>();

            dictionary.put("stopName", this.stopName);
            dictionary.put("stopOrder", this.stopOrder);
            dictionary.put("violationType", (this.violationtype == null) ? OnTimeStatus_None : this.violationtype);

            dictionary.put("scheduledDeliveryStartTime", (this.scheduledDeliveryStartTime == null ) ? null : this.scheduledDeliveryStartTime);
            dictionary.put("actualDeliveryStartTime", (this.actualDeliveryStartTime == null ) ? null : this.actualDeliveryStartTime);


            return  dictionary;
        }

        public void print(){
            HashMap<String, Object> dictionary = dictionaryWithValuesForKeys();
        }


    }

    public static class OrderAccuracyPerformance extends PimmBaseObject {

        public String stopName;
        public int stopOrder;
        public String itemNumber;
        public String itemName;
        public double quantityOrdered;
        public double quantityReceived;
        public String reasonCode;

        public HashMap<String,Object> dictionaryWithValuesForKeys() {
            HashMap<String, Object> dictionary = new HashMap<String, Object>();

            dictionary.put("stopName", this.stopName);
            dictionary.put("stopOrder", this.stopOrder);
            dictionary.put("itemNumber", this.itemNumber);
            dictionary.put("itemName", this.itemName);
            dictionary.put("quantityOrdered", this.quantityOrdered);
            dictionary.put("quantityReceived", this.quantityReceived);
            dictionary.put("reasonCode", this.reasonCode);

            return  dictionary;
        }
        public void print(){
            HashMap<String, Object> dictionary = dictionaryWithValuesForKeys();
        }


    }

    public static class ReturnedProductInfo extends PimmBaseObject {

        public String stopId;
        public String stopName;
        public String po;
        public int stopOrder;
        public String itemNumber;
        public String itemName;
        public double quantityReturned;
        public String reasonCode;
        public boolean itemInOriginalPackaging;
        public ReturnedProductTypeEnum returnType;
        public double unitWeight;
        public double unitPrice;
        public String bestBeforeDate;
        public String remarks;
        public boolean verified;


            @Override
            public void setValueForKey(Object value, String key) {

                if(key.equalsIgnoreCase("returnType")) {
                    if (!value.equals(null)) {
                        ReturnedProductTypeEnum returnedProductTypeEnum = ReturnedProductTypeEnum.setIntValue((int) value);
                        this.returnType = returnedProductTypeEnum;

                    }
            } else {
                super.setValueForKey(value, key);
            }
        }


        public HashMap<String,Object> dictionaryWithValuesForKeys() {
            HashMap<String, Object> dictionary = new HashMap<String, Object>();

            dictionary.put("stopName", this.stopName);
            dictionary.put("stopOrder", this.stopOrder);
            dictionary.put("itemNumber", this.itemNumber);
            dictionary.put("itemName", this.itemName);
            dictionary.put("quantityReturned", this.quantityReturned);
            dictionary.put("itemInOriginalPackaging", this.itemInOriginalPackaging);
            dictionary.put("reasonCode", this.reasonCode);

            dictionary.put("returnType", (this.returnType == null) ? ReturnType_Credit : this.returnType);
            dictionary.put("unitPrice", this.unitPrice);
            dictionary.put("unitWeight", this.unitWeight);
            dictionary.put("bestBeforeDate", this.bestBeforeDate);
            dictionary.put("remarks", this.remarks);
            dictionary.put("verified", this.verified);

            return  dictionary;
        }


        public void print(){
            HashMap<String, Object> dictionary = dictionaryWithValuesForKeys();
        }
    }

    public class RouteReturnedProductsInfo extends PimmBaseObject {

        public String deliveryId;
        public String driverComments;
        public ArrayList<ReturnedProductInfo> routeReturnAddOnsList;

        @Override
        public void setValueForKey(Object value, String key) {

            if (key.equalsIgnoreCase("routeReturnAddOnsList")) {
                if (this.routeReturnAddOnsList == null) {
                    this.routeReturnAddOnsList = new ArrayList<ReturnedProductInfo>();
                }

                if (!value.equals(null)) {
                    if (value instanceof JSONArray) {


                        JSONArray arrStops = (JSONArray) value;

                        if (arrStops != null) {
                            for (int i = 0; i < arrStops.length(); i++) {
                                try {
                                    ReturnedProductInfo returnedProductInfo = new ReturnedProductInfo();
                                    returnedProductInfo.readFromJSONObject(arrStops.getJSONObject(i));

                                    this.routeReturnAddOnsList.add(returnedProductInfo);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                    }
                } else {
                    super.setValueForKey(value, key);
                }
            }
        }


        public HashMap<String, Object> dictionaryWithValuesForKeys() {
            HashMap<String, Object> dictionary = new HashMap<String, Object>();

            dictionary.put("deliveryId", this.deliveryId);
            dictionary.put("driverComments", this.driverComments);

            if(this.routeReturnAddOnsList != null ){
                ArrayList<HashMap<String, Object>> routeReturnAddOnsListList = new ArrayList<>();

                for(ReturnedProductInfo  entry : this.routeReturnAddOnsList) {

                    HashMap<String, Object> routeReturnAddOnsListDict = entry.dictionaryWithValuesForKeys();
                    routeReturnAddOnsListList.add(routeReturnAddOnsListDict);

                }

                dictionary.put("routeReturnAddOnsList", routeReturnAddOnsListList);

            }else{
                dictionary.put("routeReturnAddOnsList", null);
            }
            return dictionary;
        }
        public void print(){
            HashMap<String, Object> dictionary = dictionaryWithValuesForKeys();
        }


    }
}
