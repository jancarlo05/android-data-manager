package com.procuro.apimmdatamanagerlib.PODData;

import com.procuro.apimmdatamanagerlib.DMUtils;
import com.procuro.apimmdatamanagerlib.PimmBaseObject;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Objects;

public class DeliveryForm_StopProductInfo_Groups extends PimmBaseObject {

    public String PO;

    public int QuantityOrdered; //int
    public int LineItems; //int
    public int QuantityReceived; //int
    public int QuantityShipped; //int

    public boolean Verified; //NSString
    public double TotalPrice; //double
    public double TotalWeight; //double
    public Double Produce;
    public Double Chilled;
    public Double Dry;
    public Double Frozen;


    @Override
    public void setValueForKey(Object value, String key) {

        if (key.equalsIgnoreCase("TotalPrice")) {
            if (DMUtils.ValidObject(value)) {
                try {
                    this.TotalPrice = Double.parseDouble(value.toString());
                } catch (Exception ignored) {
                    ignored.printStackTrace();
                }
            }
        } else if (key.equalsIgnoreCase("TotalWeight")) {
            if (DMUtils.ValidObject(value)) {
                try {
                    this.TotalWeight = Double.parseDouble(value.toString());
                } catch (Exception ignored) {
                    ignored.printStackTrace();
                }
            }
        } else if (key.equalsIgnoreCase("Produce")) {
            if (DMUtils.ValidObject(value)) {
                try {
                    this.Produce = Double.parseDouble(value.toString());
                } catch (Exception ignored) {
                    ignored.printStackTrace();
                }
            }
        } else if (key.equalsIgnoreCase("Chilled")) {
            if (DMUtils.ValidObject(value)) {
                try {
                    this.Chilled = Double.parseDouble(value.toString());
                } catch (Exception ignored) {
                    ignored.printStackTrace();
                }
            }
        } else if (key.equalsIgnoreCase("Dry")) {
            if (DMUtils.ValidObject(value)) {
                try {
                    this.Dry = Double.parseDouble(value.toString());
                } catch (Exception ignored) {
                    ignored.printStackTrace();
                }
            }
        } else if (key.equalsIgnoreCase("Frozen")) {
            if (DMUtils.ValidObject(value)) {
                try {
                    this.Frozen = Double.parseDouble(value.toString());
                } catch (Exception ignored) {
                    ignored.printStackTrace();
                }
            }
        } else {
            super.setValueForKey(value, key);
        }
    }

    public HashMap<String,Object> dictionaryWithValuesForKeys() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();

        dictionary.put("PO", this.PO);
        dictionary.put("TotalPrice", this.TotalPrice);
        dictionary.put("QuantityOrdered", this.QuantityOrdered);
        dictionary.put("LineItems", this.LineItems);
        dictionary.put("QuantityReceived", this.QuantityShipped);
        dictionary.put("QuantityShipped", this.QuantityShipped);

        dictionary.put("Verified", this.Verified);
        dictionary.put("TotalWeight", this.TotalWeight);

        dictionary.put("Produce", this.Produce);
        dictionary.put("Chilled", this.Chilled);
        dictionary.put("Dry", this.Dry);
        dictionary.put("Frozen", this.Frozen);


        return  dictionary;
    }

}
