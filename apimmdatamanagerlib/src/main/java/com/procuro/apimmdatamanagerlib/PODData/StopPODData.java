package com.procuro.apimmdatamanagerlib.PODData;

import android.os.Environment;
import android.util.Log;

import com.procuro.apimmdatamanagerlib.AssignmentInfo;
import com.procuro.apimmdatamanagerlib.DMUtils;
import com.procuro.apimmdatamanagerlib.DeliveryActionResult;
import com.procuro.apimmdatamanagerlib.DeliveryStopActionResult;
import com.procuro.apimmdatamanagerlib.DocumentDTO;
import com.procuro.apimmdatamanagerlib.GS1Barcode;
import com.procuro.apimmdatamanagerlib.GS1BarcodeComponents;
import com.procuro.apimmdatamanagerlib.OnCompleteListeners;
import com.procuro.apimmdatamanagerlib.OnCompleteListeners.onDeleteFormAttachmentWithFormIdListener;
import com.procuro.apimmdatamanagerlib.PimmBaseObject;
import com.procuro.apimmdatamanagerlib.PimmForm;
import com.procuro.apimmdatamanagerlib.PimmFormAttachment;
import com.procuro.apimmdatamanagerlib.PimmRefTypeEnum;
import com.procuro.apimmdatamanagerlib.RouteShipmentEquipmentDTO;
import com.procuro.apimmdatamanagerlib.RouteShipmentInvoiceItem;
import com.procuro.apimmdatamanagerlib.RouteShipmentProduct;
import com.procuro.apimmdatamanagerlib.RouteShipmentStopDNDBEvent;
import com.procuro.apimmdatamanagerlib.RouteShipmentUnit;
import com.procuro.apimmdatamanagerlib.SdrStop;
import com.procuro.apimmdatamanagerlib.SiteSettings;
import com.procuro.apimmdatamanagerlib.StoreDeliveryEnums;
import com.procuro.apimmdatamanagerlib.User;
import com.procuro.apimmdatamanagerlib.aPimmDataManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.locks.Lock;

import static com.procuro.apimmdatamanagerlib.PODData.BarcodeLogEntry.BarcodeType_Enum.BarcodeType_Custom_SFM;
import static com.procuro.apimmdatamanagerlib.PODData.BarcodeLogEntry.BarcodeType_Enum.BarcodeType_GS1_128;
import static com.procuro.apimmdatamanagerlib.PODData.BarcodeLogEntry.BarcodeType_Enum.BarcodeType_Unknown;
import static com.procuro.apimmdatamanagerlib.PODData.BarcodeLogEntry.InvalidBarcodeTypeEnum.InvalidBarcode_CatchWeightMismatch;
import static com.procuro.apimmdatamanagerlib.PODData.BarcodeLogEntry.InvalidBarcodeTypeEnum.InvalidBarcode_DuplicateScan;
import static com.procuro.apimmdatamanagerlib.PODData.BarcodeLogEntry.InvalidBarcodeTypeEnum.InvalidBarcode_ExpirationDateMismatch;
import static com.procuro.apimmdatamanagerlib.PODData.BarcodeLogEntry.InvalidBarcodeTypeEnum.InvalidBarcode_LotMismatch;
import static com.procuro.apimmdatamanagerlib.PODData.BarcodeLogEntry.InvalidBarcodeTypeEnum.InvalidBarcode_NotInPO;
import static com.procuro.apimmdatamanagerlib.PODData.BarcodeLogEntry.InvalidBarcodeTypeEnum.InvalidBarcode_Over;
import static com.procuro.apimmdatamanagerlib.PODData.BarcodeLogEntry.InvalidBarcodeTypeEnum.InvalidBarcode_ProductionDateMismatch;
import static com.procuro.apimmdatamanagerlib.PODData.BarcodeLogEntry.InvalidBarcodeTypeEnum.InvalidBarcode_SellByDateMismatch;
import static com.procuro.apimmdatamanagerlib.PODData.BarcodeLogEntry.InvalidBarcodeTypeEnum.InvalidBarcode_UnknownItem;
import static com.procuro.apimmdatamanagerlib.PODData.BarcodeLogEntry.InvalidBarcodeTypeEnum.InvalidBarcode_UnsuppportedFormat;
import static com.procuro.apimmdatamanagerlib.SdrStop.OnTimeStatus.OnTimeStatus_Early;
import static com.procuro.apimmdatamanagerlib.SdrStop.OnTimeStatus.OnTimeStatus_Late;
import static com.procuro.apimmdatamanagerlib.SdrStop.OnTimeStatus.OnTimeStatus_None;
import static com.procuro.apimmdatamanagerlib.SdrStop.OnTimeStatus.OnTimeStatus_OnTime;
import static com.procuro.apimmdatamanagerlib.PODData.StopPODData.TMSStopDeliveryStatusEnum.TMSDeliveryStatus_AdditionalDeliveryCompleted;
import static com.procuro.apimmdatamanagerlib.PODData.StopPODData.TMSStopDeliveryStatusEnum.TMSDeliveryStatus_AdditionalDeliveryStarted;
import static com.procuro.apimmdatamanagerlib.PODData.StopPODData.TMSStopDeliveryStatusEnum.TMSDeliveryStatus_DeliveryCompleted;
import static com.procuro.apimmdatamanagerlib.PODData.StopPODData.TMSStopDeliveryStatusEnum.TMSDeliveryStatus_DeliveryStarted;
import static com.procuro.apimmdatamanagerlib.PODData.StopPODData.TMSStopDeliveryStatusEnum.TMSDeliveryStatus_NoPaymentAvailable_DonotDeliver;
import static com.procuro.apimmdatamanagerlib.PODData.StopPODData.TMSStopDeliveryStatusEnum.TMSDeliveryStatus_Pending;
import static com.procuro.apimmdatamanagerlib.PODData.StopPODData.TMSStopDeliveryStatusEnum.TMSDeliveryStatus_StoreResetInsideGeofence;
import static com.procuro.apimmdatamanagerlib.StoreDeliveryEnums.PODDeliveryStatusEnum.DeliveryStatus_DeliveryOK;
import static com.procuro.apimmdatamanagerlib.StoreDeliveryEnums.PODDeliveryStatusEnum.DeliveryStatus_ProductDiscrepancy;
import static java.lang.Math.round;

/**
 * Created by jophenmarieweeks on 17/08/2017.
 */

public class StopPODData extends PimmBaseObject {



    public enum TMSStopDeliveryStatusEnum {
        TMSDeliveryStatus_Undefined(0),
        TMSDeliveryStatus_Pending(1),
        TMSDeliveryStatus_DeliveryStarted(2),
        TMSDeliveryStatus_DeliveryCompleted(3),
        TMSDeliveryStatus_AdditionalDeliveryStarted(4),
        TMSDeliveryStatus_AdditionalDeliveryCompleted(5),
        TMSDeliveryStatus_StoreResetInsideGeofence(6),
        TMSDeliveryStatus_NoPaymentAvailable_DonotDeliver(7);

        private int value;
        private TMSStopDeliveryStatusEnum(int value) {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }

        public static TMSStopDeliveryStatusEnum setIntValue(int i) {
            for (TMSStopDeliveryStatusEnum type : TMSStopDeliveryStatusEnum.values()) {
                if (type.value == i) {
                    return type;
                }
            }
            return TMSDeliveryStatus_Undefined;
        }
    }

    public enum StopViolationTypeEnum {
        StopViolation_Late(0),
        StopViolation_Early(1),
        StopViolation_Missed(2),
        StopViolation_NoPOD(3),
        StopViolation_Redelivery(4);

        private int value;
        private StopViolationTypeEnum(int value) {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }

        public static StopViolationTypeEnum setIntValue(int i) {
            for (StopViolationTypeEnum type : StopViolationTypeEnum.values()) {
                if (type.value == i) {
                    return type;
                }
            }
            return StopViolation_Late;
        }
    }


    public class DocumentInfo extends PimmBaseObject {
        public String name;
        public String documentId;
        public String documentPath;
        public String referenceNumber;
        public String category;
    }


    public class DeliveryInfo extends PimmBaseObject {

        public String formID;
        public String formDefinitionId;
        public DeliveryForm deliveryForm;

        public Date deliveryConfirmationTime;
        public StoreDeliveryEnums.PODDeliveryStatusEnum podDeliveryStatus;
        public TMSStopDeliveryStatusEnum tmsDeliveryStatus;
        public StopViolationTypeEnum stopViolationTypeEnum;
        public boolean paymentCollectionTaskCompleted;
        public String deliveryInstructions;
        public boolean podReportSentToServer;

        public String storeName;
        public String stopId; //StopId

        public String deliveryId;
        public String tractor;
        public String trailer;
        public String routeNo;
        public String customerName;
        public String customerId;
        public String address;

        public boolean podSaveAction;
        public String podReportFileName;
        public String barcodeScanReportFileName;


        public String storeResetDNDBEventId;
        public ArrayList<ImageInfo> cameraImageList;
        public ArrayList<DocumentInfo> documentList;
        public DeliveryPerformance deliveryPerformanceViolation;
        public ArrayList<BarcodeDiscrepancyDetail> barcodeDiscrepancyList;


        @Override
        public void setValueForKey(Object value, String key) {

            if (key.equalsIgnoreCase("tmsDeliveryStatus")) {
                if (!value.equals(null)) {
                    TMSStopDeliveryStatusEnum TmsDeliveryStatus = TMSStopDeliveryStatusEnum.setIntValue((int) value);
                    this.tmsDeliveryStatus = TmsDeliveryStatus;

                }
            }else if (key.equalsIgnoreCase("stopViolationTypeEnum")) {
                if (!value.equals(null)) {
                    StopViolationTypeEnum stopViolationType = StopViolationTypeEnum.setIntValue((int) value);
                    this.stopViolationTypeEnum = stopViolationType;

                }
            } else if (key.equalsIgnoreCase("podDeliveryStatus")) {
                if (!value.equals(null)) {
                    StoreDeliveryEnums.PODDeliveryStatusEnum podDeliveryStatusEnum = StoreDeliveryEnums.PODDeliveryStatusEnum.setIntValue((int) value);
                    this.podDeliveryStatus = podDeliveryStatusEnum;
                }
            } else if (key.equalsIgnoreCase("deliveryForm")) {
                if (!value.equals(null)) {
                    DeliveryForm DelivForm = new DeliveryForm();
                    JSONObject jsonObject = (JSONObject) value;
                    DelivForm.readFromJSONObject(jsonObject);
                    this.deliveryForm = DelivForm;
                }
            } else if (key.equalsIgnoreCase("deliveryPerformanceViolation")) {
                if (!value.equals(null)) {
                    DeliveryPerformance deliveryPerformance = new DeliveryPerformance();
                    JSONObject jsonObject = (JSONObject) value;
                    deliveryPerformance.readFromJSONObject(jsonObject);
                    this.deliveryPerformanceViolation = deliveryPerformance;
                }

            } else if (key.equalsIgnoreCase("cameraImageList")) {

                if (this.cameraImageList == null) {
                    this.cameraImageList = new ArrayList<ImageInfo>();
                }

                if (!value.equals(null)) {
                    JSONArray arrData = (JSONArray) value;

                    if (arrData != null) {
                        for (int i = 0; i < arrData.length(); i++) {
                            try {
                                ImageInfo imageInfo = new ImageInfo();
                                imageInfo.readFromJSONObject(arrData.getJSONObject(i));

                                this.cameraImageList.add(imageInfo);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            } else if (key.equalsIgnoreCase("documentList")) {

                if (this.documentList == null) {
                    this.documentList = new ArrayList<DocumentInfo>();
                }

                if (!value.equals(null)) {
                    JSONArray arrData = (JSONArray) value;

                    if (arrData != null) {
                        for (int i = 0; i < arrData.length(); i++) {
                            try {
                                DocumentInfo documentInfo = new DocumentInfo();
                                documentInfo.readFromJSONObject(arrData.getJSONObject(i));

                                this.documentList.add(documentInfo);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            } else if (key.equalsIgnoreCase("barcodeDiscrepancyList")) {

                if (this.barcodeDiscrepancyList == null) {
                    this.barcodeDiscrepancyList = new ArrayList<BarcodeDiscrepancyDetail>();
                }

                if (!value.equals(null)) {
                    JSONArray arrData = (JSONArray) value;

                    if (arrData != null) {
                        for (int i = 0; i < arrData.length(); i++) {
                            try {
                                BarcodeDiscrepancyDetail barcodeDiscrepancyDetail = new BarcodeDiscrepancyDetail();
                                barcodeDiscrepancyDetail.readFromJSONObject(arrData.getJSONObject(i));

                                this.barcodeDiscrepancyList.add(barcodeDiscrepancyDetail);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            } else {
                super.setValueForKey(value, key);
            }
//            deliveryForm.StopProductInfo.Groups;
        }

        public void initWithStopData(SdrStop shipmentStop, AssignmentInfo assignmentInfo) {

            this.deliveryId = assignmentInfo.shipmentID;
            this.deliveryConfirmationTime = shipmentStop.DeliveryConfirmationTime;
            this.deliveryInstructions = shipmentStop.DeliveryInstructions;
            this.customerId = shipmentStop.CustomerId;
            this.customerName = shipmentStop.CustomerName;
            this.tractor = assignmentInfo.tractorName;
            this.trailer = assignmentInfo.trailerName;
            this.routeNo = assignmentInfo.routeName;
            this.address = shipmentStop.Address;
            this.stopId = shipmentStop.StopID;
            this.storeName = shipmentStop.SiteName;
            this.tmsDeliveryStatus = TMSDeliveryStatus_Pending;
            this.podReportSentToServer = false;
        }

    }
    public Lock barcodeEntry_id_lock;
    public Lock barcodeSession_id_lock;
    /* NSLock* barcodeEntry_id_lock;
     NSLock* barcodeSession_id_lock;
 */
    //static NSLock* connection_dictionary_lock;
//    public String barcodeEntryIdKey;

    public int barcodeEntryId;
//    public String barcodeSessionIdKey;
    public int barcodeSessionId;

    public SdrStop sdrStop;
    public String siteId;
    public String deliveryStopId;
    public DeliveryInfo deliveryInfo;
    public ImageInfo deliverySignatureInfo;
    public ImageInfo creditMemoSignatureInfo;
    public ImageInfo debitMemoSignatureInfo;
    public ImageInfo additionalDeliverySignatureInfo;
    public String siteName;
    //iPimmOutboundDeliveryDataManager dataManager
    public aPimmDataManager dataManager;

    public String deliveryId; // public String deliveryID
    public String appId;
    public String deliveryFormDefinitionId;

    public DeliveryDataCacheManager routeDataCacheManager;

    public List<RouteShipmentProduct> routeshipmentProductList;

    public List<RouteShipmentEquipmentDTO> routeShipmentEquipmentList;
    public List<RouteShipmentUnit> routeShipmentUnitList;
    public List<RouteShipmentInvoiceItem> routeShipmentInvoiceItemList;
    public AssignmentInfo assignmentInfo;
    public SiteSettings siteSettings;
    public User driverInfo;

    public boolean hasCameraImages;

//    @property (nonatomic,strong) NSMutableDictionary<NSString*,
//    NSArray<BarcodeScanSessionInfo*>*> *barcodeDictionary;

   public Map<String, ArrayList<BarcodeScanSessionInfo>> barcodeDictionary;
   public Map<String, ArrayList<BarcodeLogEntry>> scannedBarcodeDictionary;

  public   ArrayList<BarcodeLogEntry> validScanBarcodeList;
   public ArrayList<BarcodeLogEntry> invaidScanBarcodeList;

    public String barcodeEntryIdKey;
    public String barcodeSessionIdKey;
    ////*********initWithAppId******//////

    /*static NSLock* barcodeEntry_id_lock;
static NSLock* barcodeSession_id_lock;

static unsigned int barcodeEntryId = 0;


static unsigned int barcodeSessionId = 0; */

    public void initialize() {

        Log.d("DM", "Enter Function:initialize");

        this.dataManager = aPimmDataManager.getInstance(); //[iPimmOutboundDeliveryDataManager getOutboundDeliveryDataManager];
        this.routeDataCacheManager = DeliveryDataCacheManager.getInstance();    //[DeliveryDataCacheManager getInstance];
        this.deliveryInfo = null;
        this.deliverySignatureInfo = null;
        this.creditMemoSignatureInfo = null;
        this.debitMemoSignatureInfo = null;


        DeliveryInfo deliveryInfo = new DeliveryInfo();

        this.deliveryInfo = deliveryInfo;
        this.hasCameraImages = false;
        barcodeEntryId = 0;
        barcodeSessionId = 0;
        Log.v("DM", "Exit Function : initWithAppId ");

    }

    public void clearData(){
        Log.d("DM", "Enter Function:clearData");
        Log.d("DM", "Clearing Stop Data for Stop : "+ this.siteId);

        this.sdrStop = null;
        this.routeShipmentEquipmentList = null;
        this.routeShipmentUnitList = null;
        this.routeShipmentInvoiceItemList = null;
        this.assignmentInfo = null;
        this.siteSettings = null;
        this.deliveryInfo = null;
        this.deliverySignatureInfo = null;
        this.creditMemoSignatureInfo = null;
        this.debitMemoSignatureInfo= null;
        //    [[NSUserDefaults standardUserDefaults] removeObjectForKey:_barcodeEntryIdKey];
        //    [[NSUserDefaults standardUserDefaults] removeObjectForKey:_barcodeSessionIdKey];
        this.dataManager = null;
        this.routeDataCacheManager = null;


        Log.d("DM", "Exit Function:clearData");

    }
    public int generateBarcodeEntryId() {
        int new_Id;
        barcodeEntry_id_lock.lock();
        new_Id = ++barcodeEntryId;
        barcodeEntry_id_lock.unlock();
        return new_Id;
    }

    public int generateBarcodeSessionId() {
        int new_Id;
        barcodeSession_id_lock.lock();
        new_Id = ++barcodeSessionId;
        barcodeEntry_id_lock.unlock();
        return new_Id;
    }



    public void initWithAppId(String appId, String deliveryStopId, String deliveryId) {

        Log.v("DM", "Enter Function : initWithAppId ");

        // if(self){
        this.initialize();
        this.appId = appId;
        this.deliveryStopId = deliveryStopId;
        this.deliveryId = deliveryId;
        // }
        Log.v("DM", "Exit Function : initWithAppId ");

    }

    public void initWithAppId(String appId, User driverInfo, SdrStop sdrStop, AssignmentInfo assignmentInfo,
                              List<RouteShipmentProduct> customerProductList, List<RouteShipmentEquipmentDTO> equipmentList, List<RouteShipmentUnit> unitList,
                              String formDefinitionId, List<RouteShipmentInvoiceItem> routeShipmentInvoiceItemList, SiteSettings siteSettings, OnCompleteListeners.completionCallbackListener listener) {

        Log.v("DM", "Enter Function : initWithAppId " + assignmentInfo.routeName);
        this.initialize();
        this.sdrStop = sdrStop;
        this.driverInfo = driverInfo;
        this.deliveryFormDefinitionId = formDefinitionId;
        this.appId = appId;
        this.deliveryId = assignmentInfo.shipmentID;
        this.deliveryStopId = sdrStop.SdrStopId;
        this.siteName = sdrStop.SiteName;
        this.routeshipmentProductList = customerProductList;
        this.routeShipmentUnitList = unitList;
        this.routeShipmentEquipmentList = equipmentList;
        this.routeShipmentInvoiceItemList = routeShipmentInvoiceItemList;
        this.assignmentInfo = assignmentInfo;
        this.siteSettings = siteSettings;

        this.barcodeEntryIdKey = String.format("BarcodeEntryId_Stop_", this.deliveryStopId);
        this.barcodeSessionIdKey = String.format("BarcodeSessionId_Stop_", this.deliveryStopId);

        initDeliveryInfoWithStopData(sdrStop, assignmentInfo, this.driverInfo, customerProductList, equipmentList, unitList, routeShipmentInvoiceItemList, siteSettings, listener);

        Log.v("DM", "Exit Function : initWithAppId ");

    }


    public DeliveryForm getDeliveryForm() {
        Log.v("DM", "Enter Function : getDeliveryForm ");
        return this.deliveryInfo.deliveryForm;
    }
     public DeliveryInfo getDeliveryInfo() {
        Log.v("DM", "Enter Function : getDeliveryInfo ");
        return this.deliveryInfo;
    }

    public SdrStop.OnTimeStatus getOnTimeStatus() {
        Log.v("DM", "Enter Function : getDeliveryForm ");
        return this.deliveryInfo.deliveryForm.StoreInfo.OnTime;
    }

    public void initDeliveryInfoWithStopData(final SdrStop shipmentStop, final AssignmentInfo assignmentInfo, final User driverInfo, final List<RouteShipmentProduct> customerProductList, final List<RouteShipmentEquipmentDTO> equipmentList, final List<RouteShipmentUnit> unitList, final List<RouteShipmentInvoiceItem> routeShipmentInvoiceItemList, final SiteSettings siteSettings, final OnCompleteListeners.completionCallbackListener listener) {
        Log.v("DM", "Enter Function:initDeliveryInfoWithStopData");

        Log.v("=========", "StopProduct: "+ shipmentStop.StopProduct);

        final OnCompleteListeners.completionCallbackListener completionCallback = listener;

        final DeliveryInfo deliveryInfo = routeDataCacheManager.getDeliveryFormInfoForDeliveryId(this.deliveryId, this.deliveryStopId);

        if (deliveryInfo != null) {

            Log.i("DM", "Intializing Delivery Form from Local copy on IPAD for Stop: " + shipmentStop.SiteName);
            this.deliveryInfo = deliveryInfo;
            getLocalSignatureImages();
            getBarcodeDataFromCache();

            int barcodeEntryId = Integer.parseInt(barcodeEntryIdKey);
            int barcodeSessionId = Integer.parseInt(barcodeSessionIdKey);
            listener.completionCallback(null);

        } else {
            Log.i("DM", "Local Copy of Delivery Form not found ");
            //Check if DeliveryForm Exists on Server/
            //Need to check this in case IPAD assignment changed after first login to the route and
            //delivery forms were already created
            this.deliveryInfo.initWithStopData(shipmentStop, assignmentInfo);
            final DeliveryInfo formDelInfo = this.deliveryInfo;

            getDeliveryFormFromServerWithCallback(new OnCompleteListeners.getDeliveryFormFromServerWithCallbackListener() {
                @Override
                public void getDeliveryFormFromServerWithCallback(boolean formFoundOnServer) {
                    if (formFoundOnServer) {
                        routeDataCacheManager.saveDeliveryFormInfo(deliveryInfo, deliveryStopId);
                        if (listener != null) {
                            listener.completionCallback(null);
                        }

                    } else {

                        barcodeSessionId = 0;
                        barcodeEntryId = 0;

                        //Create DeliveryForm on Server
                        DeliveryForm deliveryForm = new DeliveryForm();
//                        deliveryForm.initializeWithStopData(shipmentStop, assignmentInfo, driverInfo, customerProductList, equipmentList, unitList, routeShipmentInvoiceItemList, siteSettings);
                        formDelInfo.formDefinitionId = deliveryFormDefinitionId;
                        formDelInfo.deliveryForm = deliveryForm;

                        Log.v("DM", "initializeWithStopData values:shipmentStop : " + shipmentStop +" assignmentInfo: " + assignmentInfo + " driverInfo: " + driverInfo + " customerProductList: " + customerProductList + " equipmentList " + equipmentList +  " unitList " + unitList + " routeShipmentInvoiceItemList " + routeShipmentInvoiceItemList + " siteSettings: " + siteSettings);
                        //createDeliveryFormWithCompletionCallback(Error error, String formid);
                        createDeliveryFormWithCompletionCallback(new OnCompleteListeners.createDeliveryFormWithCompletionCallbackListener() {
                            @Override
                            public void createDeliveryFormWithCompletionCallback(Error error, String formID) {
                                Log.v("DM", "initializeWithStopData error : " + error +" formID: " + formID );

                                if (error == null) {

                                    formDelInfo.formID = formID;
                                    routeDataCacheManager.saveDeliveryFormInfo(deliveryInfo, deliveryStopId);
                                    if (listener != null) {
                                        listener.completionCallback(null);
                                    }

                                } else {
                                    listener.completionCallback(error);
                                }
                            }
                        });

                    }
                }
            });

        }

        Log.v("DM", "Exit Function : initDeliveryInfoWithStopData ");
    }

    public byte[] getSignatureImageOfType(ImageInfo.TMSImageType imagetype) throws IOException {
        Log.v("DM", "Enter Function : getSignatureImageOfType ");
        switch (imagetype) {
            case TMSImageType_PODSignature:
                return getImageDataFromFilePath(deliverySignatureInfo.imagePath);

            case TMSImageType_CreditMemoSignature:
                return getImageDataFromFilePath(creditMemoSignatureInfo.imagePath);

            case TMSImageType_DebitMemoSignature:
                return getImageDataFromFilePath(debitMemoSignatureInfo.imagePath);

            case TMSImageType_AdditionalDeliverySignature:
                return getImageDataFromFilePath(additionalDeliverySignatureInfo.imagePath);

            default:
                Log.e("DM", "Wrong image Type, cannot return image Data");
                return null;
        }

    }

    private byte[] getImageDataFromFilePath(String relativeFilePath) throws IOException {
        Log.v("DM", "Enter Function : getImageDataFromFilePath ");

//        NSString* directoryPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask,YES) lastObject];
//        NSString* absoluteFilePath = [directoryPath stringByAppendingFormat:@"/%@",relativeFilePath];

        File cacheDir = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "/" + relativeFilePath);
        String absoluteFilePath = cacheDir.getAbsolutePath();

        if (!(cacheDir.exists())) {
            Log.e("DM", "Signature Image file doesnot exist.");
            return null;
        }
        //Read Attachment Content From Disk

        Log.v("DM", "Reading  Image Data From AbsoluteFilePath: " + absoluteFilePath);
        String error = null;
        //Byte imagedata =
        int size = (int) cacheDir.length();
        byte[] bytes = new byte[size];
        try {
            BufferedInputStream buf = new BufferedInputStream(new FileInputStream(cacheDir));
            buf.read(bytes, 0, bytes.length);
            buf.close();
            error = null;
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            error = e.getMessage();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            error = e.getMessage();
        }

        if(error == null) {
            return bytes;
        }else{
            Log.e("DM","Could not read image data from file: " + absoluteFilePath);
            return null;
        }



    }

    public ArrayList<ImageInfo> getCameraImagesOfType(ImageInfo.TMSImageType imageType){

        Log.v("DM", "Enter Function : getCameraImagesOfType ");

        if((deliveryInfo.cameraImageList != null) && (deliveryInfo.cameraImageList.size() > 0 )){

            ArrayList<ImageInfo> imageArray = new ArrayList<ImageInfo>();
            for(ImageInfo imageInfo : deliveryInfo.cameraImageList){
                if(imageInfo.type == imageType){
                    imageArray.add(imageInfo);
                }
            }

            if(imageArray.size() == 0){
                Log.i("DM","No Images found with ImageType: "+ imageType);
                imageArray= null;
            }

            return imageArray;
        }else{
            Log.d("DM", "No Camera Images saved for Stop: " + siteName);
            return null;
        }


    }


    public void saveDeliveryForm(DeliveryForm deliveryForm){
        Log.v("DM", "Enter Function: saveDeliveryForm");

        if(siteId.equalsIgnoreCase(deliveryForm.SiteID)){
            if(deliveryInfo.tmsDeliveryStatus == TMSDeliveryStatus_DeliveryCompleted || deliveryInfo.tmsDeliveryStatus == TMSDeliveryStatus_AdditionalDeliveryCompleted){
                deliveryInfo.deliveryForm = deliveryForm;
                routeDataCacheManager.saveDeliveryFormInfo(deliveryInfo, deliveryStopId);
            }
        }else{
            Log.e("DM", "Save Delivery Form function called with wrong data for Stop: " + siteName);
            Log.e("DM", "Delivery Form  passed as argument has siteId:: " + siteId);
        }



        Log.v("DM", "Exit Function: saveDeliveryForm");
    }

    public  void saveImageOnServer (String filePath, String imageName, final String attachmentID , final OnCompleteListeners.podSaveImageOnServerListener listener ){
        Log.v("DM", "Enter Function: saveImageOnServer");

        final OnCompleteListeners.podSaveImageOnServerListener saveImage = listener;
        Log.v("DM", "TMS is trying to save Image to server for Stop:  " + this.siteName);

        this.dataManager.createBinaryFormAttachmentWithAttachmentId(attachmentID, deliveryInfo.formID, filePath, imageName, "jpg", new OnCompleteListeners.onCreateBinaryFormAttachmentWithAttachmentIdListener() {
            @Override
            public void createBinaryFormAttachmentWithAttachmentId(PimmFormAttachment pimmFormAttachment, Error error) {

                if(error != null){
                    Log.e("DM", "TMS failed to save Image to server for Stop  "+ siteName+" , ErrorCode: "+error.hashCode() +", Description: " + error.getMessage());

                    if(listener != null){
                        listener.podSaveImageOnServer(error, null);
                    }

                }else{
//                    Log.v("DM", "Successfully set additional delivery time on server for stop: " + siteName);

                    if(listener != null){
                        listener.podSaveImageOnServer(null, attachmentID);
                    }
                }

            }
        });



        Log.v("DM", "Exit Function: saveImageOnServer");
    }

    public  void setPODReportFileName (String fileName){
        Log.v("DM", "Enter Function: setPODReportFileName");

        this.deliveryInfo.podReportFileName = fileName;
        routeDataCacheManager.saveDeliveryFormInfo(this.deliveryInfo, this.deliveryStopId);


        Log.v("DM", "Exit Function: setPODReportFileName");
    }

    public  void setBarcodeReportFileName (String fileName){
        Log.v("DM", "Enter Function: setBarcodeReportFileName");

        this.deliveryInfo.barcodeScanReportFileName = fileName;

        Log.v("DM", "Exit Function: setBarcodeReportFileName");
    }


    /****Redelivery APIs****/
    public boolean activateAdditionalDelivery(){
        Log.v("DM", "Enter Function: activateAdditionalDelivery");

        if(this.deliveryInfo.tmsDeliveryStatus != TMSDeliveryStatus_DeliveryCompleted){
            Log.e("DM","Activate ReDelivery API called in invalid delivery state for stop: " + this.siteName);
            return false;
        }
        Log.v("DM","Activating Redelivery For Stop: " + this.siteName );
        Date additionalDeliveryStartTime = new Date();
        this.deliveryInfo.tmsDeliveryStatus = TMSDeliveryStatus_AdditionalDeliveryStarted;

        this.dataManager.setAdditionalDeliveryTimeForDeliveryId(this.deliveryId, Integer.parseInt(this.deliveryStopId), additionalDeliveryStartTime, null, 0, new OnCompleteListeners.setAdditionalDeliveryTimeForDeliveryIdListener() {
            @Override
            public void setAdditionalDeliveryTimeForDeliveryId(Error error) {
                if(error != null){
                    Log.e("DM", "Failed to set additional delivery time on server for stop: " + siteName);

                }else{
                    Log.v("DM", "Successfully set additional delivery time on server for stop: " + siteName);

                }
            }
        });

        deliveryInfo.deliveryForm.AdditionalDeliveryStartTime = additionalDeliveryStartTime;

        //Resetting POD Save Action because driver will need press POD Save Button again
        this.deliveryInfo.podSaveAction = false;

        //Resetting POD Report Sent because driver will need to send the POD Report again
        this.deliveryInfo.podReportSentToServer = false;

        routeDataCacheManager.saveDeliveryFormInfo(this.deliveryInfo, this.deliveryStopId);

        Log.v("DM", "Exit Function: activateAdditionalDelivery");
        return true;
    }

    public boolean saveAdditionalDeliveryComments(String additionalDeliveryComments){
        Log.v("DM", "Enter Function: saveAdditionalDeliveryComments");

        if(this.deliveryInfo.tmsDeliveryStatus != TMSDeliveryStatus_AdditionalDeliveryStarted && this.deliveryInfo.tmsDeliveryStatus != TMSDeliveryStatus_AdditionalDeliveryCompleted){

            Log.e("DM","Save ReDelivery Comments API called in invalid delivery state for stop: " + this.siteName);
            return false;
        }

        Log.v("DM","Saving Additional Delivery Comments For Stop: " + this.siteName );
        deliveryInfo.deliveryForm.AdditionalDeliveryComments = additionalDeliveryComments;
        routeDataCacheManager.saveDeliveryFormInfo(this.deliveryInfo,this.deliveryStopId);

        Log.v("DM", "Exit Function: saveAdditionalDeliveryComments");


        return true;
    }
    public  void saveCameraImage(byte[] imageData, ImageInfo.TMSImageType imageType, String name){
        Log.v("DM", "Enter Function: saveCameraImage");
        switch (imageType) {
            case TMSImageType_PODSignature:
            case TMSImageType_CreditMemoSignature:
            case TMSImageType_DebitMemoSignature:

                Log.e("DM","This API doesnot support saving SignatureImages");
                break;
            case TMSImageType_CameraImage_POD:
            case TMSImageType_CameraImage_CreditMemo:
            case TMSImageType_CameraImage_DebitMemo:
            saveCameraImage(imageData ,name ,imageType);

                break;

            default:
                break;
        }

        Log.v("DM", "Exit Function: saveCameraImage");

    }

    public void saveCameraImage(byte[] imageData, final String name, ImageInfo.TMSImageType type){
        Log.v("DM", "Enter Function: saveCameraImage");

        Log.v("DM", "Saving Camera Image for Stop: " + this.siteName);

        if(this.deliveryInfo.cameraImageList == null){
            this.deliveryInfo.cameraImageList = new ArrayList<ImageInfo>();
        }
        this.hasCameraImages = true;

        ImageInfo imageInfo = new ImageInfo();
        imageInfo.type = type;
        imageInfo.imageName = name;

        final String attachmentID = UUID.randomUUID().toString();

        Log.v("DM","Attachment ID: " +attachmentID+ " Generated for Camera Image: " + imageInfo.imageName);
        imageInfo.attachmentId = attachmentID;

    /* Note

     Writing image data to file here and not through DeliveryDataCacheManager because this data needs to be
     written to disk before we call the API to createBinaryAttachment

     */

        String filePath = this.routeDataCacheManager.getFilePathForImageWithName(imageInfo.imageName, this.deliveryId, this.deliveryStopId);

        String[] relativeFilePathArr = filePath.split("Documents/");
        String relativeFilePath = relativeFilePathArr[relativeFilePathArr.length-1];

        Log.v("DM", "Absolute File Path of Image: " + filePath);
        Log.v("DM", "Relative File Path of Image: " + relativeFilePath);

        Error error = null;

//        [imageData writeToFile:filePath options:NSDataWritingAtomic error:(&error)];


        if(error != null)
        {
            Log.e("DM","Error while saving Iamge Data to Disk: " + error.getMessage());
            return;
        }
        else
        {
            Log.v("DM","Image File successfully saved on Disk for ImageName: " + name);
            imageInfo.imagePath = relativeFilePath;
        }

        this.deliveryInfo.cameraImageList.add(imageInfo);

        this.routeDataCacheManager.saveImageWithImageInfo(imageInfo, this.deliveryId, this.deliveryStopId);

        switch (type) {

            case TMSImageType_CameraImage_POD: {
                if (this.deliveryInfo.deliveryForm.PODCameraImageAttachments == null) {
                    this.deliveryInfo.deliveryForm.PODCameraImageAttachments = new ArrayList<ImageInfo>();
                }

                this.deliveryInfo.deliveryForm.PODCameraImageAttachments.add(imageInfo);
                break;
            }
            case TMSImageType_CameraImage_CreditMemo: {
                if (this.deliveryInfo.deliveryForm.CreditMemoCameraImageAttachments == null) {
                    this.deliveryInfo.deliveryForm.CreditMemoCameraImageAttachments = new ArrayList<ImageInfo>();
                }

                this.deliveryInfo.deliveryForm.CreditMemoCameraImageAttachments.add(imageInfo);
                break;
            }
            case TMSImageType_CameraImage_DebitMemo: {
                if (this.deliveryInfo.deliveryForm.DebitMemoCameraImageAttachments == null) {
                    this.deliveryInfo.deliveryForm.DebitMemoCameraImageAttachments = new ArrayList<ImageInfo>();
                }

                this.deliveryInfo.deliveryForm.DebitMemoCameraImageAttachments.add(imageInfo);
                break;

            }
            default:
                break;
        }

        this.routeDataCacheManager.saveDeliveryFormInfo(this.deliveryInfo, this.deliveryStopId);

        saveImageOnServer(relativeFilePath, name, attachmentID, new OnCompleteListeners.podSaveImageOnServerListener() {
            @Override
            public void podSaveImageOnServer(Error error, String attachmentID) {
                if(error != null){
                    //TBD Possibly retry
                    // [imageInfo setSavedOnServer:false];
                }else{
                    Log.v("DM", "TMS successfully saved Camera Image: "+ name +" to server for Stop " + siteName);

                    // [imageInfo setSavedOnServer:true];

                    //Save ImageInfo Again on Disk to update the status that image is saved on the server
                    // [_routeDataCacheManager saveImageWithImageInfo:imageInfo DeliveryStopId:_deliveryStopId];



                }
            }
        });

        Log.v("DM", "Exit Function: saveCameraImage");
    }


    public void saveSignatureImage(byte[] imageData, ImageInfo.TMSImageType imageType, Date signatureTime, String signerName){
        Log.v("DM", "Enter Function: saveSignatureImage");
        switch (imageType) {
            case TMSImageType_PODSignature:
            {
                //POD Information is locked at this point
            savePODSignature(imageData, signatureTime,signerName);


            }
            break;
            case TMSImageType_CreditMemoSignature:
            saveCreditMemoSignature(imageData, signatureTime, signerName);
                break;
            case TMSImageType_DebitMemoSignature:
            saveDebitMemoSignature(imageData, signatureTime , signerName);
                break;
            case TMSImageType_CameraImage_POD:
            case TMSImageType_CameraImage_CreditMemo:
            case TMSImageType_CameraImage_DebitMemo:
                Log.e("DM","API does not support this image type");

                break;
            case TMSImageType_AdditionalDeliverySignature:
            saveAdditionalDeliverySignature(imageData, signatureTime, signerName);
                break;

            default:
                break;
        }

        Log.v("DM", "Exit Function: saveSignatureImage");
    }



    public void saveAdditionalDeliverySignature(byte[] imageData, Date signatureTime, String signerName) {
        Log.v("DM", "Enter Function: saveAdditionalDeliverySignature");
        Log.v("DM", "Saving ReDelivery Signature for Stop: " + this.siteName);

        String imageName = "AdditionalDeliverySignature";
        this.additionalDeliverySignatureInfo = new ImageInfo();

        this.additionalDeliverySignatureInfo.type = ImageInfo.TMSImageType.TMSImageType_AdditionalDeliverySignature;
        this.additionalDeliverySignatureInfo.imageName = imageName;
        this.additionalDeliverySignatureInfo.signerName = signerName;

        final String attachmentID = UUID.randomUUID().toString();

        Log.v("DM", "Attachment ID: "+ attachmentID +" Generated for Additional Delivery Signature Image");
        additionalDeliverySignatureInfo.attachmentId = attachmentID;

         /* Note

         Writing image data to file here and not through DeliveryDataCacheManager because this data needs to be
         written to disk before we call the API to createBinaryAttachment

         */

         String filePath = this.routeDataCacheManager.getFilePathForImageWithName(imageName, this.deliveryId, this.deliveryStopId);

        String[] relativeFilePathArr = filePath.split("Documents/");
        String relativeFilePath = relativeFilePathArr[relativeFilePathArr.length-1];

        Log.v("DM", "Absolute File Path of Image: " + filePath);
        Log.v("DM", "Relative File Path of Image: " + relativeFilePath);

        Error error;






        Log.v("DM", "Exit Function: saveAdditionalDeliverySignature");


    }

    public void saveDebitMemoSignature(byte[] imageData, Date signatureTime, String signerName) {
    }

    public void saveCreditMemoSignature(byte[] imageData, Date signatureTime, String signerName) {
    }

    public void savePODSignature(byte[] imageData, Date signatureTime, String signerName) {
        Log.v("DM", "Enter Function: savePODSignature");

        deliverySignatureInfo = new ImageInfo();
        String imageName= "PODSignature";

        deliverySignatureInfo.type = ImageInfo.TMSImageType.TMSImageType_PODSignature;
        deliverySignatureInfo.imageName = imageName;
        deliverySignatureInfo.signerName = signerName;

         final String attachmentID = UUID.randomUUID().toString();

        Log.d("DM","Attachment ID: "+ attachmentID +" Generated for POD Signature Image");

        /* Note

             Writing image data to file here and not through DeliveryDataCacheManager because this data needs to be
             written to disk before we call the API to createBinaryAttachment

             */

        File directory = new File(Environment.getRootDirectory() + "/TMS");

        if (!directory.exists())
        {
            Log.v("MAKEDIR", "NOT EXIST: Creating Directory TMS");
            directory.mkdir();
        }
        else
        {
            Log.v("MAKEDIR", "TMS Directory Exist!");
        }
//        String filePath = routeDataCacheManager.getFilePathForImageWithName(imageName, deliveryId, deliveryStopId);
//        String[] relativeFilePath =  filePath.split("Documents/");
//       // String relativeFilePathOne= relativeFilePath.toString();
//        Log.v("DM","Absolute File Path of Image: "+ filePath);
//        Log.v("DM","Relative File Path of Image: "+ relativeFilePath);
        //[imageData writeToFile:filePath options:NSDataWritingAtomic error:(&error)];
        String error = null;
        //FileOutputStream outputStream;
//        String relativeFilePathOne = TextUtils.join(" ", relativeFilePath);

//        try {
//
//            FileOutputStream outputStream;
//            outputStream = Context.openFileOutput(relativeFilePathOne, MODE_PRIVATE);
//            outputStream.write(relativeFilePathOne.getBytes());
//            outputStream.close();
//        } catch (Exception e) {
//            e.printStackTrace();
//            error= e.getMessage();
//        }
//        if(error != null)
//        {
//            Log.e("DM","Error while saving Image Data to Disk: " + error);
//            return;
//        } else {
//
//            Log.i("DM","Image File successfully saved on Disk for ImageName: " + imageName);
//            deliverySignatureInfo.imagePath = relativeFilePathOne;
//        }

        deliveryInfo.deliveryConfirmationTime = signatureTime;

        saveDeliveryConfirmationTimeOnServer();
        setPODDeliveryStatus();

        deliveryInfo.deliveryForm.DeliveryEndTime = signatureTime;
        deliveryInfo.tmsDeliveryStatus = TMSDeliveryStatus_DeliveryCompleted;
        deliveryInfo.deliveryForm.PODSignatureAttachmentID = attachmentID;
        deliveryInfo.deliveryForm.PODSignatureSignerName = signerName;

//        if(updateDeliveryFormOnServerWithCompletionCallback)



//        imageData

        Log.v("DM", "Exit Function: savePODSignature");

    }

    public boolean addCommentsToCameraImageWithName (String imageName, ImageInfo.TMSImageType imageType, String comments){
        Log.v("DM", "Enter Function: addCommentsToCameraImageWithName");
        switch (imageType) {
            case TMSImageType_PODSignature:
            case TMSImageType_CreditMemoSignature:
            case TMSImageType_DebitMemoSignature:

                Log.e("DM","This API doesnot support adding comments to SignatureImages");
                return false;

            case TMSImageType_CameraImage_POD:
            case TMSImageType_CameraImage_CreditMemo:
            case TMSImageType_CameraImage_DebitMemo:
            {
                boolean found = false;
                for(ImageInfo imageInfo : this.deliveryInfo.cameraImageList)
                {

                    if(imageName.equalsIgnoreCase(imageInfo.imageName))
                    {
                        Log.d("DM","Found Image with Name for Adding Comment: " + imageInfo.imageName);
                        imageInfo.comments =comments;

                        this.routeDataCacheManager.addCommentsToImageWithImageInfo(imageInfo, this.deliveryId , this.deliveryStopId);
//                        this.deliveryInfo.deliveryForm.addCommentsToCameraImageWithName(imageName, imageType, comments);
                        found = true;
                    }
                }
                if(!found)
                {
                    Log.e("DM","Image not found with Name:%@, Cannot Add comment " + imageName);
                    return false;
                } else {
                    return true;
                }

            }


            default:

                return false;

        }

    }

    public boolean deleteCameraImageWithName(String imageName, ImageInfo.TMSImageType imageType){
        Log.v("DM", "Enter Function: deleteCameraImageWithName");

        switch (imageType) {
            case TMSImageType_PODSignature:
            case TMSImageType_CreditMemoSignature:
            case TMSImageType_DebitMemoSignature:

                Log.e("DM","This API doesnot support adding comments to SignatureImages");
                return false;

            case TMSImageType_CameraImage_POD:
            case TMSImageType_CameraImage_CreditMemo:
            case TMSImageType_CameraImage_DebitMemo:
            {
                boolean found = false;
                ImageInfo imageToDelete = new ImageInfo();
                for(ImageInfo imageInfo : this.deliveryInfo.cameraImageList)
                {

                    if(imageName.equalsIgnoreCase(imageInfo.imageName))
                    {
                        Log.d("DM","Found Image with Name for  Deletion: " + imageInfo.imageName);
                        imageToDelete = imageInfo; //Donot want to alter the array while enumerating

//                        this.deliveryInfo.deliveryForm.deleteCameraImageWithName(imageName, imageType);
                        found = true;
                    }
                }
                if(!found)
                {
                    Log.e("DM","Image not found with Name:%@, Cannot Add comment " + imageName);
                    return false;
                } else {
                    deleteCameraImage(imageToDelete);
                    return true;
                }


            }


            default:

                return false;

        }

    }


    public void deleteCameraImage(final ImageInfo imageInfo){
        Log.v("DM", "Enter Function: deleteCameraImage");
        Log.v("DM", "Deleting Camera Image: " + imageInfo.imageName + " for " + this.siteName);

        routeDataCacheManager.deleteImageWithImageInfo(imageInfo, this.deliveryId, this.deliveryStopId);
        deleteImageOnServer(imageInfo, new OnCompleteListeners.completionCallbackListener() {
            @Override
            public void completionCallback(Error error) {
                if(error == null){
                    Log.v("DM","Successfully deleted Image: "+imageInfo.imageName+" for Stop: "+ siteName);
                }
            }
        });

        routeDataCacheManager.saveDeliveryFormInfo(deliveryInfo,deliveryStopId);

        deliveryInfo.cameraImageList.remove(imageInfo);
        if(this.deliveryInfo.cameraImageList.size() == 0){
            this.hasCameraImages = false;
        }
        Log.v("DM", "Exit Function: deleteCameraImage");

    }

    public void deleteImageOnServer(ImageInfo imageInfo, final OnCompleteListeners.completionCallbackListener listener){
        Log.v("DM", "Enter Function: deleteImageOnServer");
        Log.v("DM", "TMS is trying to delete Camera Image from server for Stop " + this.siteName);

        final OnCompleteListeners.completionCallbackListener completionCallback = listener;
        dataManager.deleteFormAttachmentWithFormId(deliveryInfo.formID, imageInfo.attachmentId, new onDeleteFormAttachmentWithFormIdListener() {
            @Override
            public void deleteFormAttachmentWithFormId(Error error) {
                if(error != null){
                    Log.e("DM","TMS failed to delete Image to server for Stop "+ siteName +" , ErrorCode: "+ error.getLocalizedMessage() +", Description: " + error.getMessage());
                        listener.completionCallback(error);
                }else{
                    listener.completionCallback(null);
                }
            }
        });


        Log.v("DM", "Exit Function: deleteImageOnServer");


    }



    public void saveDeliveryConfirmationTimeOnServer() {
        Log.v("DM", "Enter Function: saveDeliveryConfirmationTimeOnServer");

        Log.v("DM","Saving DeliveryConfirmationTime "+ deliveryInfo.deliveryConfirmationTime +" on Server for Stop: " + this.siteName);
        final String siteName = this.siteName;

        dataManager.setDeliveryConfirmationTimeForDeliveryId(this.deliveryId, this.deliveryStopId, deliveryInfo.deliveryConfirmationTime, false, new OnCompleteListeners.setDeliveryConfirmationTimeForDeliveryIdListener() {
            @Override
            public void setDeliveryConfirmationTimeForDeliveryId(DeliveryStopActionResult result, Error error) {
                if(error != null){
                    Log.e("DM", "Failed to set DeliveryConfirmationTime on Server for Stop: " + siteName);
                }else{
                    Log.v("DM", "Successfully set DeliveryConfirmationTime on Server for Stop: " + siteName);

                }

            }
        });

        Log.v("DM", "Exit Function: saveDeliveryConfirmationTimeOnServer");


    }

    public void setPODDeliveryStatus() {
        Log.v("DM", "Enter Function: setPODDeliveryStatus");

        boolean productDiscrepancy = false;

        ArrayList<DeliveryForm_StopProductDetail> stopProductDetail = deliveryInfo.deliveryForm.StopProductInfo.StopProductDetail;

        for(DeliveryForm_StopProductDetail productDetail :  stopProductDetail)
        {
            if((int)productDetail.QuantityOrdered != (int)productDetail.QuantityReceived)
            {
                productDiscrepancy = true;
            }
        }

        StoreDeliveryEnums.PODDeliveryStatusEnum deliveryStatus = StoreDeliveryEnums.PODDeliveryStatusEnum.setIntValue(0);
        if(productDiscrepancy == true){
            Log.v("DM","Delivery Status is set to have ProductDiscrepancy for Stop: " + this.siteName);
            deliveryStatus = DeliveryStatus_ProductDiscrepancy;
        }else{
            Log.v("DM","Delivery Status is set to 'OK' for Stop: " + this.siteName);
            deliveryStatus = DeliveryStatus_DeliveryOK;
        }

        deliveryInfo.podDeliveryStatus = deliveryStatus;
        saveDeliveryStatusOnServer();

        Log.v("DM", "Exit Function: setPODDeliveryStatus");
    }

    public void saveDeliveryStatusOnServer() {
        Log.v("DM", "Enter Function: saveDeliveryStatusOnServer");

        Log.v("DM","Saving DeliveryStatus on Server for Stop: " + this.siteName);
        final String siteName = this.siteName;

        dataManager.SetDeliveryStatusForDeliveryId(this.deliveryId, this.siteId, this.deliveryInfo.podDeliveryStatus, new OnCompleteListeners.SetDeliveryStatusForDeliveryIdListener() {
            @Override
            public void setDeliveryStatusForDeliveryId(Error error) {
                if(error != null){
                    Log.e("DM", "Failed to set DeliveryStatus on Server for Stop: " + siteName);
                }else{
                    Log.v("DM", "Successfully set DeliveryStatus on Server for Stop: " + siteName);

                }

            }
        });

        Log.v("DM", "Exit Function: saveDeliveryStatusOnServer");


    }

    public void getLocalSignatureImages() {
        Log.v("DM", "Enter Function: getLocalSignatureImages");
            this.deliverySignatureInfo = routeDataCacheManager.getImageDataforImageName("PODSignature", this.deliveryId, this.deliveryStopId );
            this.creditMemoSignatureInfo = routeDataCacheManager.getImageDataforImageName("CreditMemoSignature", this.deliveryId, this.deliveryStopId );
            this.debitMemoSignatureInfo = routeDataCacheManager.getImageDataforImageName("DebitMemoSignature", this.deliveryId, this.deliveryStopId );


            if(this.deliveryInfo.tmsDeliveryStatus == TMSDeliveryStatus_AdditionalDeliveryCompleted){
                this.additionalDeliverySignatureInfo = routeDataCacheManager.getImageDataforImageName("AdditionalDeliverySignature", this.deliveryId, this.deliveryStopId );
            }
        Log.d("DM", "Exit Function: getLocalSignatureImages");

    }

    public void setManualDeliveryStartTime(Date deliveryStartTime){
        Log.v("DM", "Enter Function: setManualDeliveryStartTime");

        if(deliveryStartTime != null){
            deliveryInfo.deliveryForm.DeliveryStartTime = deliveryStartTime;
            setOnTimeStatus();

            routeDataCacheManager.saveDeliveryFormInfo(deliveryInfo,deliveryStopId);

            saveManualDeliveryStartTimeOnServer();
        }else{
            Log.e("DM","Delivery End Time parameter is null");
        }

        Log.v("DM", "Exit Function: setManualDeliveryStartTime");

    }

    public void setManualDeliveryEndTime(Date deliveryEndTime){
        Log.v("DM", "Enter Function: setManualDeliveryEndTime");

        if(deliveryEndTime != null){
            deliveryInfo.deliveryForm.DeliveryEndTime = deliveryEndTime;

            routeDataCacheManager.saveDeliveryFormInfo(deliveryInfo,deliveryStopId);

            saveManualDeliveryEndTimeOnServer();
        }else{
            Log.e("DM","Delivery End Time parameter is null");
        }

        Log.v("DM", "Exit Function: setManualDeliveryEndTime");

    }

    public void setOdometerReading(double odometerReading){
        Log.v("DM", "Enter Function: setOdometerReading");

        if(odometerReading > 0){
//            deliveryInfo.deliveryForm.OdometerReading = odometerReading;

            routeDataCacheManager.saveDeliveryFormInfo(deliveryInfo,deliveryStopId);

            saveStopOdometerReadingOnServer();
        }else{
            Log.e("DM","Odometer Reading Parameter is invalid");
        }

        Log.v("DM", "Exit Function: setOdometerReading");


    }

    public void setStoreArrivalTime(Date arrivalTime){
        Log.v("DM", "Enter Function: setStoreArrivalTime");

        if(arrivalTime != null){
            deliveryInfo.deliveryForm.DeliveryStartTime = arrivalTime;
            deliveryInfo.deliveryForm.StoreInfo.ArrivalActualTime = arrivalTime;
            deliveryInfo.deliveryForm.ArrivalActualTime = arrivalTime;
            setOnTimeStatus();
            deliveryInfo.tmsDeliveryStatus = TMSDeliveryStatus_DeliveryStarted;

            routeDataCacheManager.saveDeliveryFormInfo(deliveryInfo,deliveryStopId);

        }else{
            Log.e("DM","Arrival Time Parameter is invalid");
        }

        Log.v("DM", "Exit Function: setStoreArrivalTime");


    }

    public void setStoreDepartureTime(Date departureTime){
        Log.v("DM", "Enter Function: setStoreDepartureTime");

        if(departureTime != null){


            if(deliveryInfo.tmsDeliveryStatus == TMSDeliveryStatus_StoreResetInsideGeofence){
                Log.v("DM","Store Reset was Activated for this stop inside store geofence, hence not setting departure time in Stop Info");

                Date arrivalTime = this.deliveryInfo.deliveryForm.StoreInfo.ArrivalActualTime;
                dataManager.createOrUpdateDNDBEventWithEventId(this.deliveryInfo.storeResetDNDBEventId, this.deliveryId, Integer.parseInt(deliveryStopId.toString()), arrivalTime, departureTime, RouteShipmentStopDNDBEvent.RouteStatusDNDBEventTypeEnum.RouteStatusDNDBEventType_Cancel, new OnCompleteListeners.onCreateOrUpdateDNDBEventWithEventIdListener() {
                    @Override
                    public void createOrUpdateDNDBEventWithEventId(RouteShipmentStopDNDBEvent dndbEvent, Error error) {
                        if(error != null){
                            Log.e("DM","Failed to update DNDB Event to cancel store Arrival on Server for Stop: "+ siteName +", Error: "
                            + error.getMessage());
                        }else {
                            Log.v("DM","Successfully updated DNDB Event to reset Arrival Time on Server for Stop: " + siteName);
                        }
                    }
                });

                this.deliveryInfo.tmsDeliveryStatus = TMSDeliveryStatus_Pending;
                this.deliveryInfo.deliveryForm.StoreInfo.ArrivalActualTime = null;
                Log.v("DM","Resetting Delivery Form on Server due to Manual Store Reset");

                updateDeliveryFormOnServerWithCompletionCallback(new OnCompleteListeners.completionCallbackListener() {
                    @Override
                    public void completionCallback(Error error) {
                        if(error == null)
                        {
                            Log.v("DM","Delivery Form Reset on Server due to Manual Store Reset");
                        }
                        else
                        {
                            Log.e("DM","Failed to Reset Delivery Form on Server after Manual Store Reset");
                        }
                    }
                });

            }else{
                Log.v("DM","Setting Store Departure Time for Stop: "+siteName+", "+departureTime);
                deliveryInfo.deliveryForm.StoreInfo.DepartureTime = departureTime;
            }

            routeDataCacheManager.saveDeliveryFormInfo(deliveryInfo,deliveryStopId);

        }else{
            Log.e("DM","Arrival Time Parameter is invalid");
        }

        Log.v("DM", "Exit Function: setStoreDepartureTime");


    }

    public void setDeliveryComments(String deliveryComments){
        Log.v("DM", "Enter Function: setDeliveryComments");
        Log.v("DM", "Setting deliveryComments: " + deliveryComments);


        if(deliveryComments != null){
            deliveryInfo.deliveryForm.DeliveryComments = deliveryComments;
            routeDataCacheManager.saveDeliveryFormInfo(deliveryInfo,deliveryStopId);

        }else{
            Log.e("DM","DeliveryComments Parameter is invalid");
        }

        Log.v("DM", "Exit Function: setDeliveryComments");

    }

    public void setPODSaveAction(){
        Log.v("DM", "Enter Function: setPODSaveAction");

        this.deliveryInfo.podSaveAction = true;
        this.deliveryInfo.deliveryForm.PODSaveButtonPressTime = new Date();
        routeDataCacheManager.saveDeliveryFormInfo(this.deliveryInfo,this.deliveryStopId);

        updateDeliveryFormOnServerWithCompletionCallback(new OnCompleteListeners.completionCallbackListener() {
            @Override
            public void completionCallback(Error error) {
                if(error != null ){
                    Log.e("DM", "Failed to update  Delivery Form on Server after PODSaveButtonPress Action Server For Stop: " + siteName);

                }else{

                    Log.i("DM", "Successfully updated Delivery Form on Server after  PODSaveButtonPress Action  for Stop: " + siteName);

                }


            }
        });

        Log.v("DM", "Exit Function: setPODSaveAction");


    }



    public void updateDeliveryFormOnServerWithCompletionCallback(final OnCompleteListeners.completionCallbackListener listener){
       Log.v("DM", "Enter Function: updateDeliveryFormOnServerWithCompletionCallback");
       Log.v("DM", "Updating LastUpdated Time in DeliveryForm for Stop: "+ this.siteName);
        OnCompleteListeners.completionCallbackListener completion = listener;
       deliveryInfo.deliveryForm.LastUpdated = new Date();
//       NSDictionary* inputDictionary = [[_deliveryInfo deliveryForm] dictionaryWithValuesForKeys];
//       NSString* formBody = [inputDictionary toJSONString];
//       JSONObject jsonObjFB = new JSONObject(deliveryInfo.deliveryForm.dictionaryWithValuesForKeys());
//       String formBody = jsonObjFB.toString();
       String formID = this.deliveryInfo.formID;

       dataManager.updateFormWithFormId(formID, "formBody", new OnCompleteListeners.updateFormWithFormIdListener() {
           @Override
           public void updateFormWithFormId(PimmForm pimmForm, Error error) {
               if(error != null){
                   Log.e("DM","Failed to update DeliveryForm  on Server for Stop: "+siteName+", Error: "+ error.getMessage());
                   listener.completionCallback(error);
               }else{
                   Log.v("DM","Successfully Updated DeliveryForm on Server for Stop: "+siteName);
                   listener.completionCallback(null);
               }
           }
       });

       Log.v("DM", "Exit Function: updateDeliveryFormOnServerWithCompletionCallback");
   }



    public void setOnTimeStatus() {
        Log.v("DM", "Enter Function: setOnTimeStatus");
        int lateThreshold = 0;
        int earlyThreshold = 0;

        SdrStop.SdrOnTimeRule onTimeRule = this.sdrStop.OnTimeRule;

        if(onTimeRule != null){
            if(onTimeRule.LateThreshold != 0){
                lateThreshold = onTimeRule.LateThreshold;
            }

            if(onTimeRule.EarlyThreshold != 0){
                earlyThreshold = onTimeRule.EarlyThreshold;
            }
        }else {
            Log.e("DM","OnTimeRule not found for Stop: " +sdrStop.SiteName+", Setting Ontime Status to 'OnTime'");
        }

        Date expectedDeliveryTime = sdrStop.ExpectedDeliveryTime;

        if(expectedDeliveryTime == null){
            Log.v("DM","Expected Delivery Time is not set for Stop:" +sdrStop.SiteName+", Setting Ontime Status to 'OnTime'");

            deliveryInfo.deliveryForm.StoreInfo.OnTime = OnTimeStatus_OnTime;
            return;
        }

        expectedDeliveryTime = DMUtils.truncateSecondsForDate(expectedDeliveryTime);

        Date actualDeliveryTime = deliveryInfo.deliveryForm.DeliveryStartTime;
        actualDeliveryTime = DMUtils.truncateSecondsForDate(actualDeliveryTime);

        Log.v("DM","ExpectedDeliveryTime: "+ expectedDeliveryTime + "ActualDeliveryTime:" + actualDeliveryTime );
            ///hinditapos
        if(actualDeliveryTime.getTime() == expectedDeliveryTime.getTime()){
            this.deliveryInfo.deliveryForm.StoreInfo.OnTime = OnTimeStatus_OnTime;
            this.deliveryInfo.deliveryForm.StoreInfo.MinutesLate = 0;
        }else if (actualDeliveryTime.getTime() > expectedDeliveryTime.getTime()){
            int minutesLate = (int) ((actualDeliveryTime.getTime() - expectedDeliveryTime.getTime()) / 60);

            if(lateThreshold == 0){
                //No Ontime Rule set
                deliveryInfo.deliveryForm.StoreInfo.OnTime = OnTimeStatus_OnTime;
                deliveryInfo.deliveryForm.StoreInfo.MinutesLate = minutesLate;
            }else{

                if(minutesLate > lateThreshold){
                    Log.v("DM", "Delivery  For Stop : "+ this.siteName +" Late by "+(long) minutesLate+" Minutes,  which is greater than Late Threshold is "+ (long) lateThreshold +", setting OnTime Status as 'Late'");
                    deliveryInfo.deliveryForm.StoreInfo.OnTime= OnTimeStatus_Late;
                    deliveryInfo.deliveryForm.StoreInfo.MinutesLate = minutesLate;

                }else{
                    Log.v("DM","Delivery  For Stop : "+ this.siteName +" Late by "+ (int)minutesLate +" Minutes,  which is less than Late Threshold is "+ (int) lateThreshold +", setting OnTime Status as 'OnTime'");
                    deliveryInfo.deliveryForm.StoreInfo.OnTime = OnTimeStatus_OnTime;
                    deliveryInfo.deliveryForm.StoreInfo.MinutesLate = minutesLate;
                }
            }
        }else{
            int minutesEarly = (int) ((expectedDeliveryTime.getTime() - actualDeliveryTime.getTime()) /60);

            if(earlyThreshold == 0){
                deliveryInfo.deliveryForm.StoreInfo.OnTime= OnTimeStatus_Early;
                deliveryInfo.deliveryForm.StoreInfo.MinutesLate = minutesEarly;
            }else{
                if(minutesEarly > earlyThreshold){
                    Log.v("DM", "Delivery  For Stop : "+ this.siteName +" Early by "+(long) minutesEarly+" Minutes,  which is greater than Early Threshold is "+ (long) earlyThreshold +", setting OnTime Status as 'Late'");
                    deliveryInfo.deliveryForm.StoreInfo.OnTime= OnTimeStatus_Early;
                    deliveryInfo.deliveryForm.StoreInfo.MinutesLate = earlyThreshold;

                }else{
                    Log.v("DM", "Delivery  For Stop : "+ this.siteName +" Early by "+(int) minutesEarly+" Minutes,  which is greater than Early Threshold is "+ (int) earlyThreshold +", setting OnTime Status as 'Late'");
                    deliveryInfo.deliveryForm.StoreInfo.OnTime= OnTimeStatus_OnTime;
                    deliveryInfo.deliveryForm.StoreInfo.MinutesLate = minutesEarly;
                }

            }

        }


        Log.v("DM", "Exit Function: setOnTimeStatus");

    }


    public void saveManualDeliveryStartTimeOnServer() {
        Log.v("DM", "Enter Function: saveManualDeliveryStartTimeOnServer");

        Date deliveryStartTime = deliveryInfo.deliveryForm.DeliveryStartTime;

        Log.v("DM", "Saving Manual Delivery Start Time: "+(Date) deliveryStartTime+" on Server for Stop: "+ this.siteName);
        final String siteName = this.siteName;

        dataManager.setManualDeliveryStartForDeliveryId(this.deliveryId, this.deliveryStopId, deliveryStartTime, new OnCompleteListeners.setManualDeliveryStartForDeliveryIdListener() {
            @Override
            public void setManualDeliveryStartForDeliveryId(DeliveryStopActionResult result, Error error) {

                if(error != null){
                    Log.e("DM","Failed to set Manual Delivery Start Time on Server for Stop: "+ siteName);
                }else{
                    Log.v("DM", "Successfully set Manual Delivery Start Time on Server for Stop: " + siteName);
                }
            }

        });

        Log.v("DM", "Exit Function: saveManualDeliveryStartTimeOnServer");
    }

    public void saveManualDeliveryEndTimeOnServer() {

        Log.v("DM", "Enter Function: saveManualDeliveryEndTimeOnServer");

        Date deliveryEndTime = deliveryInfo.deliveryForm.DeliveryEndTime;

        Log.v("DM", "Saving Manual Delivery End Time: "+(Date) deliveryEndTime+" on Server for Stop: "+ this.siteName);
        final String siteName = this.siteName;

        dataManager.setManualDeliveryEndForDeliveryId(this.deliveryId, this.deliveryStopId, deliveryEndTime, new OnCompleteListeners.setManualDeliveryEndForDeliveryIdListener() {
                    @Override
                    public void setManualDeliveryEndForDeliveryId(DeliveryStopActionResult result, Error error) {
                        if(error != null){
                            Log.e("DM","Failed to set Manual Delivery End Time on Server for Stop: "+ siteName);
                        }else{
                            Log.v("DM", "Successfully set Manual Delivery End Time on Server for Stop: " + siteName);
                        }
                    }
                });

        Log.v("DM", "Exit Function: saveManualDeliveryEndTimeOnServer");

    }

    public void saveStopOdometerReadingOnServer() {

        Log.v("DM", "Enter Function: saveManualDeliveryEndTimeOnServer");

        Log.v("DM", "Saving Ododmeter Reading on Server for Stop: "+ this.siteName);

        final String siteName = this.siteName;

        dataManager.setStopOdometerforDeliveryId(this.deliveryId, this.deliveryStopId, deliveryInfo.deliveryForm.OdometerReading, new OnCompleteListeners.setStopOdometerforDeliveryIdListener() {
            @Override
            public void setStopOdometerforDeliveryId(DeliveryStopActionResult result, Error error) {
                if(error != null){
                    Log.e("DM","Failed to set OdometerReading on Server for Stop: "+ siteName);
                }else{
                    Log.v("DM", "Successfully set OdometerReading on Server for Stop: " + siteName);
                }
            }
        });

    }

    public void getBarcodeDataFromCache() {
        Log.v("DM", "Enter Function: getBarcodeDataFromCache");

        ArrayList<DeliveryForm_StopProductInfo_Groups> POList = deliveryInfo.deliveryForm.StopProductInfo.Groups;

        for(DeliveryForm_StopProductInfo_Groups group : POList){
            ArrayList<BarcodeScanSessionInfo> barcodeSessionList = routeDataCacheManager.getBarcodeDataForPO(group.PO, deliveryStopId, deliveryId );

            if(barcodeSessionList != null){
                Log.v("DM", "Barcode Scan Data found in cache for PO: " + group.PO);
                    if(barcodeDictionary == null){
                        barcodeDictionary.put(group.PO , barcodeSessionList);
                    }else{
                        Log.e("DM", "Barcode Scan Data NOT found in cache for PO" + group.PO);
                    }
            }

        }
        Log.v("DM", "Exit Function: getBarcodeDataFromCache");
    }

    public void resendDeliveryDataToServerWithCompletionCallback(OnCompleteListeners.completionCallbackListener listener){
        Log.v("DM", "Enter Function: resendDeliveryDataToServerWithCompletionCallback");

        Log.v("DM", "Resending Delivery Data to Server for Stop: " + this.siteName);

        DeliveryInfo deliveryInfo = this.routeDataCacheManager.getDeliveryFormInfoForDeliveryId(this.deliveryId, this.deliveryStopId);

        if(deliveryInfo != null){
            this.deliveryInfo = deliveryInfo;
            this.siteId = this.deliveryInfo.deliveryForm.SiteID;
            this.siteName = this.deliveryInfo.deliveryForm.SiteName;
            getLocalSignatureImages();
        }
        final Error[] sendError = {null};

//        dispatch_group_t dispatchGroup =  dispatch_group_create();
//
//        __block NSError* sendError;
//
//        dispatch_group_enter(dispatchGroup);

        updateDeliveryFormOnServerWithCompletionCallback(new OnCompleteListeners.completionCallbackListener() {
            @Override
            public void completionCallback(Error error) {
                if(error != null){
                    Log.e("DM","Failed to Update Delivery Form on Server for Stop as part of PODRecovery: " + siteName);
                }else{
                    //success
                }
//                dispatch_group_leave(dispatchGroup);

            }


        });
//        dispatch_group_t documentDispatchGroup = dispatch_group_create();

        if(this.deliveryInfo.documentList != null && this.deliveryInfo.documentList.size() > 0){

            for(final DocumentInfo documentInfo : deliveryInfo.documentList){
//                dispatch_group_enter(documentDispatchGroup);

                String documentPath = documentInfo.documentPath;

                String relativeDocumentPaths = documentPath;

                if(documentPath.contains("Documents/")){

                    String[] relativeDocumentPathArr = documentPath.split("Documents/");
                    String relativeDocumentPath = relativeDocumentPathArr[relativeDocumentPathArr.length-1];
                    Log.v("DM", "Absolute File Path of Document: " + documentPath);
                    Log.v("DM", "Absolute File Path of Image: " + relativeDocumentPath);

                }

                final String siteName = this.siteName;

                this.dataManager.addDocumentWithDocumentId(documentInfo.documentId, "application/pdf", documentInfo.name, this.deliveryId, PimmRefTypeEnum.PimmRefTypesEnum.PimmRefType_Shipment, documentInfo.category, this.deliveryStopId, documentInfo.referenceNumber, relativeDocumentPaths, new OnCompleteListeners.onaddDocumentWithDocumentIdListener() {
                    @Override
                    public void addDocumentWithDocumentId(DocumentDTO document, Error error) {
                        if(document != null){
                            Log.v("DM", "Successfully uploaded document: "+documentInfo.name+" on Server for Stop: " + siteName);
                        }else{
                            if(error != null && error.hashCode() == 409){
                                Log.e("DM","Document "+documentInfo.name+" already present on server for stop: " + siteName);
                            }else{
                                Log.e("DM","Failed to upload document: "+documentInfo.name+" due to error: " + error.getMessage());
                            }
                        }
//                        dispatch_group_leave(documentDispatchGroup);
                    }
                });

            }
        }

//        dispatch_group_notify(documentDispatchGroup, dispatch_get_main_queue(), ^{

        if(this.deliveryInfo.podReportFileName != null){
//            dispatch_group_enter(dispatchGroup);

//            dispatch_group_enter(dispatchGroup);
//
//            NSString* directoryPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask,YES) lastObject];
//
//
//            NSString *filename = [_deliveryInfo podReportFileName];
//
//            NSString *pdfFilePath = [directoryPath stringByAppendingFormat:@"/PODData/%@/%@/%@.pdf",_deliveryInfo.deliveryId,_deliveryStopId,filename];
//
//            DDLogDebug(@" Resending PDF Report  at File Path:%@", pdfFilePath);
//
//            [self sendPODEmailWithPDFFilePath:pdfFilePath FileName:[_deliveryInfo podReportFileName] Text:nil CompletionCallback:^(NSError *err) {
//
//                if(err != nil)
//                {
//                    DDLogError(@"Failed to Send POD PDF  Report  for Stop As Part of Recovery:%@", _siteName);
//                    sendError = err;
//                }
//                else
//                {
//                    DDLogInfo(@"Successfully sent POD PDF Report for Stop as Part of Recovery:%@", _siteName);
//                }
//                dispatch_group_leave(dispatchGroup);
//            }];
//        }


    }

//        dispatch_group_enter(dispatchGroup);


    final String siteName = this.siteName;

        this.dataManager.setDeliveryConfirmationTimeForDeliveryId(this.deliveryId, this.deliveryStopId, this.deliveryInfo.deliveryConfirmationTime, true, new OnCompleteListeners.setDeliveryConfirmationTimeForDeliveryIdListener() {
            @Override
            public void setDeliveryConfirmationTimeForDeliveryId(DeliveryStopActionResult result, Error error) {

                if(error != null){
                    Log.e("DM","Failed to set DeliveryConfirmationTime on Server for Stop: " + siteName);
                    sendError[0] = error;
                }else{
                    Log.v("DM","Successfully set DeliveryConfirmationTime on Server for Stop: " + siteName);

                }
//                dispatch_group_leave(dispatchGroup);
            }
        });

//        dispatch_group_enter(dispatchGroup);


        dataManager.SetDeliveryStatusForDeliveryId(this.deliveryId, this.siteId,this.deliveryInfo.podDeliveryStatus, new OnCompleteListeners.SetDeliveryStatusForDeliveryIdListener() {
            @Override
            public void setDeliveryStatusForDeliveryId(Error error) {

                if(error != null){
                    Log.e("DM","Failed to set DeliveryStatus on Server for Stop: " + siteName);
                    sendError[0] = error;
                }else{
                    Log.v("DM","Successfully set DeliveryStatus on Server for Stop: " + siteName);

                }
//                dispatch_group_leave(dispatchGroup);

            }
        });
//        dispatch_group_enter(dispatchGroup);

        this.dataManager.setStopOdometerforDeliveryId(this.deliveryId, this.deliveryStopId, (double) deliveryInfo.deliveryForm.OdometerReading, new OnCompleteListeners.setStopOdometerforDeliveryIdListener() {
            @Override
            public void setStopOdometerforDeliveryId(DeliveryStopActionResult result, Error error) {

                if(error != null){
                    Log.e("DM","Failed to set OdometerReading on Server for Stop: " + siteName);
                    sendError[0] = error;
                }else{
                    Log.v("DM","Successfully set OdometerReading on Server for Stop: " + siteName);

                }
//                dispatch_group_leave(dispatchGroup);
            }
        });
        //TBD need to check if delivery start and end times also need to be sent to Server or not

//        dispatch_group_enter(dispatchGroup);


        this.dataManager.getFormAttachmentWithFormId(this.deliveryInfo.formID, this.deliverySignatureInfo.attachmentId, new OnCompleteListeners.getFormAttachmentWithFormIdListener() {
            @Override
            public void getFormAttachmentWithFormId(PimmFormAttachment pimmFormAttachment, Error error) {
                if (error != null) {

                    if (error.hashCode() == 404) {
                        Log.v("DM", "Delivery Signature not found on Server for Stop: " + siteName);

                        saveImageOnServer(deliverySignatureInfo.imagePath, deliverySignatureInfo.imageName, deliverySignatureInfo.attachmentId, new OnCompleteListeners.podSaveImageOnServerListener() {
                            @Override
                            public void podSaveImageOnServer(Error error, String attachmentID) {
                                if (error != null) {
                                    Log.e("DM", "Failed to save POD Signature Image on Server for Stop: " + siteName);
                                    sendError[0] = error;
                                } else {
                                    Log.v("DM", "Successfully saved POD Signature Image on Server for Stop: " + siteName);

                                }
//                                dispatch_group_leave(dispatchGroup);
                            }
                        });
                    } else {
                        //TBD need to think how to handle it..
                        sendError[1] = error;
//                        dispatch_group_leave(dispatchGroup);
                    }
                } else {
                    Log.e("DM", "POD  Signature is already present on Server for Stop: " + siteName);
                }

            }
        });


        if(this.additionalDeliverySignatureInfo != null) {
//            dispatch_group_enter(dispatchGroup);

            this.dataManager.getFormAttachmentWithFormId(this.deliveryInfo.formID, this.additionalDeliverySignatureInfo.attachmentId, new OnCompleteListeners.getFormAttachmentWithFormIdListener() {
                @Override
                public void getFormAttachmentWithFormId(PimmFormAttachment pimmFormAttachment, Error error) {
                    if (error != null) {

                        if (error.hashCode() == 404) {
                            Log.v("DM", "POD ReDelivery Signature not found on Server for Stop: " + siteName);

                            saveImageOnServer(deliverySignatureInfo.imagePath, deliverySignatureInfo.imageName, deliverySignatureInfo.attachmentId, new OnCompleteListeners.podSaveImageOnServerListener() {
                                @Override
                                public void podSaveImageOnServer(Error error, String attachmentID) {
                                    if (error != null) {
                                        Log.e("DM", "Failed to save POD ReDelivery Signature Image on Server for Stop: " + siteName);
                                        sendError[0] = error;
                                    } else {
                                        Log.v("DM", "Successfully saved POD ReDelivery Signature Image on Server for Stop: " + siteName);

                                    }
//                                dispatch_group_leave(dispatchGroup);
                                }
                            });
                        } else {
                            //TBD need to think how to handle it..
                            sendError[1] = error;
//                        dispatch_group_leave(dispatchGroup);
                        }
                    } else {
                        Log.e("DM", "POD ReDelivery Signature is already present on Server for Stop: " + siteName);
                    }

                }
            });
        }

        if(this.creditMemoSignatureInfo != null){
//            dispatch_group_enter(dispatchGroup);

            dataManager.getFormAttachmentWithFormId(this.deliveryInfo.formID, creditMemoSignatureInfo.attachmentId, new OnCompleteListeners.getFormAttachmentWithFormIdListener() {
                @Override
                public void getFormAttachmentWithFormId(PimmFormAttachment pimmFormAttachment, Error error) {
                    if (error != null) {

                        if (error.hashCode() == 404) {
                            Log.v("DM", "Credit Memo Signature not found on Server for Stop: " + siteName);

                            saveImageOnServer(deliverySignatureInfo.imagePath, deliverySignatureInfo.imageName, deliverySignatureInfo.attachmentId, new OnCompleteListeners.podSaveImageOnServerListener() {
                                @Override
                                public void podSaveImageOnServer(Error error, String attachmentID) {
                                    if (error != null) {
                                        Log.e("DM", "Failed to save Credit Memo SignatureImage on Server for Stop: " + siteName);
                                        sendError[0] = error;
                                    } else {
                                        Log.v("DM", "Successfully saved Credit Memo Signature Image on Server for Stop: " + siteName);

                                    }
//                                dispatch_group_leave(dispatchGroup);
                                }
                            });
                        } else {
                            //TBD need to think how to handle it..
                            sendError[1] = error;
//                        dispatch_group_leave(dispatchGroup);
                        }
                    } else {
                        Log.e("DM", "Credit Memo Signature is already present on Server for Stop: " + siteName);
                    }

                }
            });

        }

        if(this.debitMemoSignatureInfo != null){
//            dispatch_group_enter(dispatchGroup);

            dataManager.getFormAttachmentWithFormId(this.deliveryInfo.formID, debitMemoSignatureInfo.attachmentId, new OnCompleteListeners.getFormAttachmentWithFormIdListener() {
                @Override
                public void getFormAttachmentWithFormId(PimmFormAttachment pimmFormAttachment, Error error) {
                    if (error != null) {

                        if (error.hashCode() == 404) {
                            Log.v("DM", "Debit Memo Signature not found on Server for Stop: " + siteName);

                            saveImageOnServer(deliverySignatureInfo.imagePath, deliverySignatureInfo.imageName, deliverySignatureInfo.attachmentId, new OnCompleteListeners.podSaveImageOnServerListener() {
                                @Override
                                public void podSaveImageOnServer(Error error, String attachmentID) {
                                    if (error != null) {
                                        Log.e("DM", "Failed to save Debit Memo SignatureImage on Server for Stop: " + siteName);
                                        sendError[0] = error;
                                    } else {
                                        Log.v("DM", "Successfully saved Debit Memo Signature Image on Server for Stop: " + siteName);

                                    }
//                                dispatch_group_leave(dispatchGroup);
                                }
                            });
                        } else {
                            //TBD need to think how to handle it..
                            sendError[1] = error;
//                        dispatch_group_leave(dispatchGroup);
                        }
                    } else {
                        Log.e("DM", "Debit Memo Signature is already present on Server for Stop: " + siteName);
                    }

                }
            });

        }


        if(this.deliveryInfo.cameraImageList != null && this.deliveryInfo.cameraImageList.size() > 0){

//            this.routeDataCacheManager.getCameraImagesForDeliveryId(this.deliveryId, this.deliveryStopId, new OnCompleteListeners.completionCallbackListener listener){
//                for(ImageInfo imageInfo : imageList
//            }

            for(final ImageInfo imageInfo : this.deliveryInfo.cameraImageList){
//                dispatch_group_enter(dispatchGroup);
                dataManager.getFormAttachmentWithFormId(this.deliveryInfo.formID, imageInfo.attachmentId, new OnCompleteListeners.getFormAttachmentWithFormIdListener() {
                    @Override
                    public void getFormAttachmentWithFormId(PimmFormAttachment pimmFormAttachment, Error error) {
                        if (error != null) {

                            if (error.hashCode() == 404) {
                                Log.v("DM", "Camera Memo "+imageInfo.imageName+" Signature not found on Server for Stop: " + siteName);

                                saveImageOnServer(imageInfo.imagePath, imageInfo.imageName, imageInfo.attachmentId, new OnCompleteListeners.podSaveImageOnServerListener() {
                                    @Override
                                    public void podSaveImageOnServer(Error error, String attachmentID) {
                                        if (error != null) {
                                            Log.e("DM", "Failed to save Camera Image "+imageInfo.imageName+"  SignatureImage on Server for Stop: " + siteName);
                                            sendError[0] = error;
                                        } else {
                                            Log.v("DM", "Successfully saved Camera Image "+imageInfo.imageName+"   Signature Image on Server for Stop: " + siteName);

                                        }
//                                dispatch_group_leave(dispatchGroup);
                                    }
                                });
                            } else {
                                //TBD need to think how to handle it..
                                sendError[0] = error;
//                        dispatch_group_leave(dispatchGroup);
                            }
                        } else {
                            Log.e("DM", "Camera Image "+imageInfo.imageName+"   is already present on Server for Stop: " + siteName);
//                            dispatch_group_leave(dispatchGroup);
                        }

                    }
                });

            }
        }

//        dispatch_group_enter(dispatchGroup);
    this.dataManager.completeTMSWithDeliveryID(this.deliveryId, new Date(), new OnCompleteListeners.onCompleteTMSWithDeliveryIDListener() {
        @Override
        public void completeTMSWithDeliveryID(DeliveryActionResult result, Error error) {
            if(error != null){
                sendError[0] = error;
            }else{
                Log.v("DM", "Successfully send TMSComplete Request as part of Recovery ");

            }
//            dispatch_group_leave(dispatchGroup);
        }
    });
//        dispatch_group_notify(dispatchGroup, dispatch_get_main_queue(), ^{

//        dispatch_group_notify(dispatchGroup, dispatch_get_main_queue(), ^{

        if(sendError != null ){
        Log.e("DM","Some Error occured while resending Delivery Data for Stop: " + this.siteName);
        listener.completionCallback(sendError[0]);
    }else{
        Log.v("DM","All Delivery Data successfully sent for Stop: " + this.siteName);
        listener.completionCallback(null);
    }

    Log.v("DM", "Exit Function: resendDeliveryDataToServerWithCompletionCallback");


    }

    public boolean activateManualStoreResetInsideGeofence(){
        Log.v("DM", "Enter Function: activateManualStoreResetInsideGeofence");

        if(this.deliveryInfo.tmsDeliveryStatus == TMSDeliveryStatus_DeliveryCompleted){
            Log.e("DM","Delivery Already Signed.Cannot reset Data");
            return  false;
        }

        Log.v("DM", "Reset Delivery Data API called due to Manaul Store Reset Inside Store Geofence");


        Date arrivalTime = this.deliveryInfo.deliveryForm.StoreInfo.ArrivalActualTime;

        if(arrivalTime == null){
            Log.e("DM", "Arrival time is nil, cannot create RouteStatusDNDBEvent for Store Reset Inside geofenc");
        }

        String eventId = UUID.randomUUID().toString();

        this.dataManager.createOrUpdateDNDBEventWithEventId(eventId, this.deliveryId, Integer.parseInt(this.deliveryStopId), arrivalTime, null, RouteShipmentStopDNDBEvent.RouteStatusDNDBEventTypeEnum.RouteStatusDNDBEventType_Cancel, new OnCompleteListeners.onCreateOrUpdateDNDBEventWithEventIdListener() {
            @Override
            public void createOrUpdateDNDBEventWithEventId(RouteShipmentStopDNDBEvent dndbEvent, Error error) {
                if(error != null){
                    Log.e("DM", "Failed to create DNDB Event to cancel store Arrival on Server for Stop: " + siteName +", Error: " + error.getMessage());

                }else{
                    Log.v("DM", "Successfully  created DNDB Event to reset Arrival Time on Server for Stop: " + siteName);
                }
            }
        });

        DeliveryInfo deliveryInfo = new DeliveryInfo();
        deliveryInfo.initWithStopData(this.sdrStop,this.assignmentInfo);
        deliveryInfo.formDefinitionId = this.deliveryFormDefinitionId;
        deliveryInfo.formID = this.deliveryInfo.formID;
        deliveryInfo.tmsDeliveryStatus = TMSDeliveryStatus_StoreResetInsideGeofence;
        deliveryInfo.storeResetDNDBEventId = eventId;


        DeliveryForm deliveryForm = new DeliveryForm();
//        deliveryForm.initializeWithStopData(this.sdrStop, this.assignmentInfo, this.driverInfo,this.routeshipmentProductList,this.routeShipmentEquipmentList,this.routeShipmentUnitList,this.routeShipmentInvoiceItemList, this.siteSettings);

        deliveryInfo.deliveryForm = deliveryForm;
            //Note : ArrivalTime will be reset to nil after Store Departure because arrival time is required to
        //update the DNDB Event at store departure

        deliveryInfo.deliveryForm.StoreInfo.ArrivalActualTime = null;
        deliveryInfo.deliveryForm.DeliveryStartTime = null;
        this.deliveryInfo = deliveryInfo;



        Log.v("DM", "Resetting Delivery Form on Server due to Manual Store Reset");

        routeDataCacheManager.saveDeliveryFormInfo(this.deliveryInfo, this.deliveryStopId);

        if(this.creditMemoSignatureInfo != null){
            //Delete Credit Memo Signature
            Log.v("DM", "Deleting Credit Memo Signature image from Server and Cache");

            this.dataManager.deleteFormAttachmentWithFormId(this.deliveryInfo.formID, this.creditMemoSignatureInfo.attachmentId, new onDeleteFormAttachmentWithFormIdListener() {
                @Override
                public void deleteFormAttachmentWithFormId(Error error) {
                    if(error != null){
                        Log.e("DM","Failed to delete Credit Memo Signature Attachement due to Manual Store Reset");
                    }
                }
            });
            this.routeDataCacheManager.deleteImageWithImageInfo(this.creditMemoSignatureInfo,this.deliveryId,this.deliveryStopId);
            this.creditMemoSignatureInfo = null;
        }


        if(this.debitMemoSignatureInfo != null){

            //Delete Debit Memo Signature
            Log.v("DM", "Deleting Debit Memo Signature image from Server and Cache");

            this.dataManager.deleteFormAttachmentWithFormId(this.deliveryInfo.formID, this.debitMemoSignatureInfo.attachmentId, new onDeleteFormAttachmentWithFormIdListener() {
                @Override
                public void deleteFormAttachmentWithFormId(Error error) {
                    if(error != null){
                        Log.e("DM","Failed to delete Debit Memo Signature Attachement due to Manual Store Reset");
                    }
                }
            });
            this.routeDataCacheManager.deleteImageWithImageInfo(this.debitMemoSignatureInfo,this.deliveryId,this.deliveryStopId);
            this.debitMemoSignatureInfo = null;
        }

        if(this.deliveryInfo.cameraImageList != null){

            //Delete Debit Memo Signature
            Log.v("DM", "Deleting Camera Images from Server and Cache");

            for(ImageInfo imageInfo : this.deliveryInfo.cameraImageList) {
                this.dataManager.deleteFormAttachmentWithFormId(this.deliveryInfo.formID, imageInfo.attachmentId, new onDeleteFormAttachmentWithFormIdListener() {
                    @Override
                    public void deleteFormAttachmentWithFormId(Error error) {
                        if (error != null) {
                            Log.e("DM", "Failed to Delete Camera Image from Server after Manual Store Reset");
                        }
                    }
                });

                this.routeDataCacheManager.deleteImageWithImageInfo(imageInfo, this.deliveryId, this.deliveryStopId);

            }
            this.deliveryInfo.cameraImageList.clear();
            this.deliveryInfo.cameraImageList = null;
        }

        resetBarCodeData();

        Log.v("DM", "Exit Function: activateManualStoreResetInsideGeofence");
        return true;
    }

    public boolean activateManualStoreResetOutsideGeofence(){
        Log.v("DM", "Enter Function: activateManualStoreResetOutsideGeofence");

        if(this.deliveryInfo.tmsDeliveryStatus == TMSDeliveryStatus_DeliveryCompleted){
            Log.e("DM","Delivery Already Signed.Cannot reset Data");
            return  false;
        }

        Log.v("DM", "Reset Delivery Data API called due to Manaul Store Reset");

        this.deliveryInfo.tmsDeliveryStatus = TMSDeliveryStatus_Pending;

        Date arrivalTime = this.deliveryInfo.deliveryForm.StoreInfo.ArrivalActualTime;
        Date departureTime = this.deliveryInfo.deliveryForm.StoreInfo.DepartureTime;

        if(arrivalTime == null){
            Log.e("DM", "Arrival time is nil, cannot create RouteStatusDNDBEvent for Store Reset Outside geofenc");
        }
        if(departureTime == null){
            Log.e("DM", "Departure time is nil, cannot create RouteStatusDNDBEvent for Store Reset Outside geofenc");
        }

        //Reset Stop Arrival Time on Server


        String eventId = UUID.randomUUID().toString();

        this.dataManager.createOrUpdateDNDBEventWithEventId(eventId, this.deliveryId, Integer.parseInt(this.deliveryStopId), arrivalTime, departureTime, RouteShipmentStopDNDBEvent.RouteStatusDNDBEventTypeEnum.RouteStatusDNDBEventType_Cancel, new OnCompleteListeners.onCreateOrUpdateDNDBEventWithEventIdListener() {
            @Override
            public void createOrUpdateDNDBEventWithEventId(RouteShipmentStopDNDBEvent dndbEvent, Error error) {
                if(error != null){
                    Log.e("DM", "Failed to create DNDB Event to cancel store Arrival on Server for Stop: " + siteName +", Error: " + error.getMessage());

                }else{
                    Log.v("DM", "Successfully  created DNDB Event to reset Arrival Time on Server for Stop: " + siteName);
                }
            }
        });

        DeliveryInfo deliveryInfo = new DeliveryInfo();
        deliveryInfo.initWithStopData(this.sdrStop,this.assignmentInfo);
        deliveryInfo.formDefinitionId = this.deliveryFormDefinitionId;
        deliveryInfo.formID = this.deliveryInfo.formID;

        DeliveryForm deliveryForm = new DeliveryForm();
//        deliveryForm.initializeWithStopData(this.sdrStop, this.assignmentInfo, this.driverInfo,this.routeshipmentProductList,this.routeShipmentEquipmentList,this.routeShipmentUnitList,this.routeShipmentInvoiceItemList, this.siteSettings);

        deliveryInfo.deliveryForm = deliveryForm;

        this.deliveryInfo = deliveryInfo;



        Log.v("DM", "Resetting Delivery Form on Server due to Manual Store Reset");

        updateDeliveryFormOnServerWithCompletionCallback(new OnCompleteListeners.completionCallbackListener() {
            @Override
            public void completionCallback(Error error) {
                if(error == null){
                    Log.v("DM","Delivery Form Reset on Server due to Manual Store Reset");
                }else{
                    Log.e("DM","Failed to Reset Delivery Form on Server after Manual Store Reset");
                }
            }
        });
        Log.v("DM", "Resetting Delivery Info in Cache due to Manual Store Reset");

        routeDataCacheManager.saveDeliveryFormInfo(this.deliveryInfo, this.deliveryStopId);

        if(this.creditMemoSignatureInfo != null){
            //Delete Credit Memo Signature
            Log.v("DM", "Deleting Credit Memo Signature image from Server and Cache");

            this.dataManager.deleteFormAttachmentWithFormId(this.deliveryInfo.formID, this.creditMemoSignatureInfo.attachmentId, new onDeleteFormAttachmentWithFormIdListener() {
                @Override
                public void deleteFormAttachmentWithFormId(Error error) {
                    if(error != null){
                        Log.e("DM","Failed to delete Credit Memo Signature Attachement due to Manual Store Reset");
                    }
                }
            });
            this.routeDataCacheManager.deleteImageWithImageInfo(this.creditMemoSignatureInfo,this.deliveryId,this.deliveryStopId);
            this.creditMemoSignatureInfo = null;
        }


        if(this.debitMemoSignatureInfo != null){

            //Delete Debit Memo Signature
            Log.v("DM", "Deleting Debit Memo Signature image from Server and Cache");

            this.dataManager.deleteFormAttachmentWithFormId(this.deliveryInfo.formID, this.debitMemoSignatureInfo.attachmentId, new onDeleteFormAttachmentWithFormIdListener() {
                @Override
                public void deleteFormAttachmentWithFormId(Error error) {
                    if(error != null){
                        Log.e("DM","Failed to delete Debit Memo Signature Attachement due to Manual Store Reset");
                    }
                }
            });
            this.routeDataCacheManager.deleteImageWithImageInfo(this.debitMemoSignatureInfo,this.deliveryId,this.deliveryStopId);
            this.debitMemoSignatureInfo = null;
        }

        if(this.deliveryInfo.cameraImageList != null){

            //Delete Debit Memo Signature
            Log.v("DM", "Deleting Camera Images from Server and Cache");

            for(ImageInfo imageInfo : this.deliveryInfo.cameraImageList) {
                this.dataManager.deleteFormAttachmentWithFormId(this.deliveryInfo.formID, imageInfo.attachmentId, new onDeleteFormAttachmentWithFormIdListener() {
                    @Override
                    public void deleteFormAttachmentWithFormId(Error error) {
                        if (error != null) {
                            Log.e("DM", "Failed to Delete Camera Image from Server after Manual Store Reset");
                        }
                    }
                });

                this.routeDataCacheManager.deleteImageWithImageInfo(imageInfo, this.deliveryId, this.deliveryStopId);

            }
            this.deliveryInfo.cameraImageList.clear();
            this.deliveryInfo.cameraImageList = null;
        }

        resetBarCodeData();

        Log.v("DM", "Exit Function: activateManualStoreResetOutsideGeofence");
        return true;
    }

   public void getDeliveryFormFromServerWithCallback(final OnCompleteListeners.getDeliveryFormFromServerWithCallbackListener listener){
       Log.v("DM", "Enter Function: getDeliveryFormFromServerWithCallback");
       final OnCompleteListeners.getDeliveryFormFromServerWithCallbackListener  getDeliveryForm = listener;


       Log.i("DM", "Fetching existing delivery forms from server for current Delivery: " + deliveryId);

       dataManager.getFormListForShipmentId(deliveryId, deliveryFormDefinitionId, appId , new OnCompleteListeners.getFormListForShipmentIdListener() {
           @Override
           public void getFormListForShipmentId(ArrayList<PimmForm> pimmFormList, Error error) {
                boolean found = false;

               if(error != null){
                   if(error.getMessage() == "404") {
                       Log.i("DM","No DeliveryForms found on server for deliveryId: " + deliveryId);
                   }
                   if(listener != null) {
                       listener.getDeliveryFormFromServerWithCallback(found);
                   }
                   return;
               }else{
                   if(pimmFormList != null){
                       if(pimmFormList.size() > 0){
                           for(PimmForm form : pimmFormList){
                               if(form.formDefinitionID.equalsIgnoreCase(deliveryFormDefinitionId)){

                                   if(form.bodyData == null){
                                       Log.e("DM","BodyData is nil for the Form, Skipping it");
                                       continue;
                                   }

                                   Object deliveryFormDic = form.bodyData;

                                   if(deliveryFormDic == null){
                                       Log.e("DM", "Error while Reading  DeliveryForm received from Server, Skipping it");
                                       continue;
                                   }
                                   DeliveryForm deliveryForm = new DeliveryForm();

                                   JSONObject jsonObject = (JSONObject)deliveryFormDic;
                                   deliveryForm.readFromJSONObject(jsonObject);
                                   /// [deliveryForm readFromJSONDictionary:deliveryFormDic];

                                    if(siteId.equalsIgnoreCase(deliveryForm.SiteID)){

                                        Log.i("DM","Found Delivery Form on Server for SiteId: " + siteName);
                                        Log.d("DM","DeliveryForm Dictionary received from Server For Stop: " + siteName + deliveryFormDic.toString());

                                        deliveryInfo.deliveryForm = deliveryForm;
                                        deliveryInfo.formID = form.formID;
                                        deliveryInfo.formDefinitionId = form.formDefinitionID;

                                        found = true;

                                    }else{
                                        Log.e("DM","DeliveryForm does not match siteId: " + siteName );
                                    }

                               }else{
                                   Log.e("DM","FormDefinitionId does not match Delivery Form Definition Id" );
                               }

                           }
                           if(!found){
                               Log.e("DM","DeliveryForm not found in form List returned from Server" );
                           }
                       }else{
                           Log.e("DM","No DeliveryForms found on server for deliveryId: "  + deliveryId);
                       }
                   }else{
                       Log.e("DM","Unexpected Error: PimmFormList object is nil even after Success Response");
                   }

                   if(listener != null){
                       listener.getDeliveryFormFromServerWithCallback(found);
                   }

               }

           }
       });



       Log.v("DM", "Exit Function: getDeliveryFormFromServerWithCallback");

   }

   public void createDeliveryFormWithCompletionCallback(final OnCompleteListeners.createDeliveryFormWithCompletionCallbackListener listener){

       Log.v("DM", "Enter Function: createDeliveryFormWithCompletionCallback");
       final OnCompleteListeners.createDeliveryFormWithCompletionCallbackListener  createDeliveryForm = listener;

       Log.i("DM", "Setting LastUpdated Time in DeliveryForm for Stop: " + siteName);

       deliveryInfo.deliveryForm.LastUpdated = new Date();
//       JSONObject jsonObjFB = new JSONObject(deliveryInfo.deliveryForm.dictionaryWithValuesForKeys());
//       String formBody = jsonObjFB.toString();
//       Log.i("DM", "formBody: " + formBody);
//        Log.i("DM", "jsonObjFB: " + jsonObjFB);

       final String formId = UUID.randomUUID().toString();
       Log.i("DM", "Creating DeliveryForm on Server for Stop: " + siteName);

       dataManager.createFormWithFormId(formId, appId, deliveryInfo.formDefinitionId, deliveryId, "formBody", new OnCompleteListeners.oncreateFormWithFormIdListener() {
           @Override
           public void createFormWithFormId(PimmForm pimmForm, Error error) {

               if(error != null){
                   Log.e("DM", "Failed to create DeliveryForm  on Server for Stop:%@, Error: " + siteName + " -- " + error.getMessage());
                   if(listener != null){
                       listener.createDeliveryFormWithCompletionCallback(error , null);
                   }
                   return;
               }
               if(error == null) {
                   if (pimmForm != null) {
                        if(!(pimmForm.formID.equalsIgnoreCase(formId))){
                            //Log.ASSERT("DM","FormId from server is not same as one created by TMS");
                            Log.e("DM","FormId from server is not same as one created by TMS");
                             Log.e("DM","Form ID: "+ pimmForm.formID +" sent back by Server in response is not the FormId: "+formId+" created by TMS");

                        }else{
                            Log.v("DM","Successfully Created DeliveryForm on Server for Stop: "+siteName+" With FormId: " + pimmForm.formID);

                        }

                       if(listener != null){
                           listener.createDeliveryFormWithCompletionCallback(null , pimmForm.formID);
                       } // if(listener != null)

                   } else {
                       Log.e("DM","Failed to create DeliveryForm  on Server for Stop: "+ siteName +" due to Unexpected Error:PimmForm object is nil");
                       if(listener != null){
                           listener.createDeliveryFormWithCompletionCallback(error , null);
                       }

                   } //if (pimmForm != null)
               } //if(error == null)
           }

       });
       Log.v("DM", "Exit Function: createDeliveryFormWithCompletionCallback");


   }

   public void updateQuantityReceivedForBarcodeEntryWithId(int barcodeEntryID, String barcode, String POName, double partialValue){
       Log.v("DM", "Enter Function: updateQuantityReceivedForBarcodeEntryWithId");

       ArrayList<BarcodeScanSessionInfo> barcodeSessionList = (ArrayList<BarcodeScanSessionInfo>) barcodeDictionary.get(POName);

       if(barcodeSessionList == null){
           Log.e("DM","No Barcode Sessions Found for PO: " + POName);
       }else {

           boolean found = false;
           for(BarcodeScanSessionInfo barcodeSession : barcodeSessionList){

               ArrayList<BarcodeLogEntry> barcodeList = barcodeSession.barcodeList;

               for(BarcodeLogEntry barcodeEntry : barcodeList){

                   if(barcodeEntry.barcodeEntryId == barcodeEntryID && barcodeEntry.barcode.equalsIgnoreCase(barcode)){
                       Log.v("DM","Saving Image for BarcodeEntry with EntryId: "+ barcodeEntryID +" Barcode: "+ barcodeEntry.barcode);
                       barcodeEntry.quantityReceived = partialValue;
                       found = true;
                       break;
                   }

               }
               if(found){
                   break;
               }

           }
           if(found){
               processBarCodeDataForPO(POName);
               saveBarcodeDataToDiskForPO(POName);
           }else{
               Log.e("DM", "BarcodeEntry with Id: "+ barcodeEntryID +" not found for PO: " + POName);
           }

       }


       Log.v("DM", "Exit Function: updateQuantityReceivedForBarcodeEntryWithId");


   }

    public void processCustomBarCodeDataForPO(String POName){
        Log.v("DM", "Enter Function: processCustomBarCodeDataForPO");

        ArrayList<DeliveryForm_StopProductDetail> productsForSelectedPO = getProductsForPO(POName);

        ArrayList<BarcodeScanSessionInfo> barcodeSessionList = (ArrayList<BarcodeScanSessionInfo>) barcodeDictionary.get(POName);
        if (barcodeSessionList == null) {
            Log.e("DM","No Barcode Sessions Found for PO: " + POName);
            return;
        }
        for(BarcodeScanSessionInfo barcodeSession : barcodeSessionList) {

            ArrayList<BarcodeLogEntry> barcodeList = barcodeSession.barcodeList;

            for (BarcodeLogEntry barcodeEntry : barcodeList) {
                barcodeEntry.matchFound = false;

            }
        }

        for(DeliveryForm_StopProductDetail stopProductDetail :  productsForSelectedPO){
            double barcodeScanReceivedQuantity = 0.0;
            int barcodeScanCount = 0;

        //Set Quantity Scanned and Quantity Received for each case in barcodeIdentifier List to 0
            for(String key : stopProductDetail.barcodeIdentifierList.keySet())
            {
                List<Object> array = (ArrayList<Object>) stopProductDetail.barcodeIdentifierList.get(key);
                int newScanCount = 0;
                double newQuantityReceived = 0.0;

                List<Object> editedArray = array;
                editedArray.add(0, newScanCount);
                editedArray.add(1, newQuantityReceived);

                List<Object> updatedArray = editedArray;
                stopProductDetail.barcodeIdentifierList.put(key, updatedArray);

            }



            for(BarcodeScanSessionInfo barcodeSession : barcodeSessionList) {

                ArrayList<BarcodeLogEntry> barcodeList = barcodeSession.barcodeList;

                for (BarcodeLogEntry barcodeEntry : barcodeList) {

                    if(barcodeEntry.matchFound == true){
                        continue;
                    }
                    if(barcodeEntry.barcodeType != BarcodeLogEntry.BarcodeType_Enum.BarcodeType_Custom_SFM){
                        barcodeEntry.valid = false;
                        barcodeEntry.invalidBarcodeType = InvalidBarcode_UnsuppportedFormat;
                        continue;
                    }

                    if(barcodeEntry.itemID.equalsIgnoreCase(stopProductDetail.ItemNo) && barcodeEntry.PO.equalsIgnoreCase(stopProductDetail.PO)){
                        //GTIN MATCH

                        barcodeEntry.itemName = stopProductDetail.ProductDescription;
                        barcodeEntry.matchFound = true;

                        try {
                            for(String key : stopProductDetail.barcodeIdentifierList.keySet()){

                                if(key.equalsIgnoreCase(barcodeEntry.numOfValueStr)){
                                    Number scanCount = (Number) stopProductDetail.barcodeIdentifierList.get(key);

                                    int newCount = scanCount.intValue() + 1;

                                    if(newCount > 1){
                                        Log.v("DM","Duplicate Scan of the same box for ItemNo: "+ barcodeEntry.itemID +", Item: " + barcodeEntry.itemName);

                                        barcodeEntry.valid = false;
                                        barcodeEntry.invalidBarcodeType = InvalidBarcode_DuplicateScan;
                                        //Duplicate Scan, dont update Barcode Scan Count for Quantity Received

                                    }else{
                                        //Update Quantity Received
                                        barcodeScanReceivedQuantity = barcodeScanReceivedQuantity + barcodeEntry.quantityReceived;
                                        barcodeEntry.valid = true;
                                    }
                                }
                            }
                        }catch (Exception e){
                            Log.e("DM","Caught Exception: " + e.getMessage());

                        }
                    } //GTIN doesnot match


                }
            }

            if(barcodeScanReceivedQuantity > 0.0){
                stopProductDetail.QuantityScanned = barcodeScanReceivedQuantity;
                if(stopProductDetail.hasManualQuantityReceived == false){
                    stopProductDetail.QuantityReceived = barcodeScanReceivedQuantity;
                }
            }
        }

        routeDataCacheManager.saveDeliveryFormInfo(this.deliveryInfo, this.deliveryStopId);

        Log.v("DM", "Exit Function: processCustomBarCodeDataForPO");

    }
    public ArrayList<BarcodeLogEntry> getBarCodeLogForLineItem(DeliveryForm_StopProductDetail stopProductDetail) {
        Log.v("DM", "Enter Function: getBarCodeLogForLineItem");
        ArrayList<BarcodeLogEntry> array = new ArrayList<BarcodeLogEntry>();

        String POName = stopProductDetail.PO;

        ArrayList<BarcodeScanSessionInfo> barcodeSessionList = (ArrayList<BarcodeScanSessionInfo>) barcodeDictionary.get(POName);
        if (barcodeSessionList == null) {
            Log.e("DM","No Barcode Sessions Found for PO: " + POName);
            return null;
        }

        for(BarcodeScanSessionInfo barcodeSession : barcodeSessionList){

            ArrayList<BarcodeLogEntry> barcodeList = barcodeSession.barcodeList;

            for(BarcodeLogEntry barcodeEntry : barcodeList) {

                if(barcodeEntry.itemID.equalsIgnoreCase(stopProductDetail.ItemNo)){

                    if(barcodeEntry.PO.equalsIgnoreCase(stopProductDetail.PO)){
                        //match additional criteria

                        if(barcodeEntry.barcodeType != BarcodeType_GS1_128){
                            //Dont match any additional criteria
                            array.add(barcodeEntry);
                        }else {
                            //Barcode is GS1-128,match additional criteria

                            if(stopProductDetail.CatchWeight > 0.0 ){

                                if(stopProductDetail.CatchWeight == barcodeEntry.gs1BarcodeComponents.catchWeight){
                                    //match
                                }else{
                                    //item does not much
                                    continue;
                                }
                            }

                            if(barcodeEntry.gs1BarcodeComponents.lotNumber != null && stopProductDetail.LotID != null){

                                if(barcodeEntry.gs1BarcodeComponents.lotNumber.equalsIgnoreCase(stopProductDetail.LotID)){
                                    //match
                                }else{
                                    //item does not much
                                    continue;
                                }
                            }

                            if(barcodeEntry.gs1BarcodeComponents.sellByDateStr != null && stopProductDetail.SellByDate != null){

                                if(barcodeEntry.gs1BarcodeComponents.sellByDateStr.equalsIgnoreCase(stopProductDetail.SellByDate)){
                                    //match
                                }else{
                                    //item does not much
                                    continue;
                                }
                            }

                            if(barcodeEntry.gs1BarcodeComponents.packagingDateStr != null && stopProductDetail.PackagingDate != null){

                                if(barcodeEntry.gs1BarcodeComponents.packagingDateStr.equalsIgnoreCase(stopProductDetail.PackagingDate)){
                                    //match
                                }else{
                                    //item does not much
                                    continue;
                                }
                            }

                            if(barcodeEntry.gs1BarcodeComponents.productionDateStr != null && stopProductDetail.ProductionDate != null){

                                if(barcodeEntry.gs1BarcodeComponents.productionDateStr.equalsIgnoreCase(stopProductDetail.ProductionDate)){
                                    //match
                                }else{
                                    //item does not much
                                    continue;
                                }
                            }

                            if(barcodeEntry.gs1BarcodeComponents.expirationDateStr != null && stopProductDetail.ExpirationDate != null){

                                if(barcodeEntry.gs1BarcodeComponents.expirationDateStr.equalsIgnoreCase(stopProductDetail.ExpirationDate)){
                                    //match
                                }else{
                                    //item does not much
                                    continue;
                                }
                            }

                            if(barcodeEntry.gs1BarcodeComponents.bestBeforeDateStr != null && stopProductDetail.BestBefore != null){

                                if(barcodeEntry.gs1BarcodeComponents.bestBeforeDateStr.equalsIgnoreCase(stopProductDetail.BestBefore)){
                                    //match
                                }else{
                                    //item does not much
                                    continue;
                                }
                            }

                            array.add(barcodeEntry);

                        }

                    }else{
                        // not in the same PO
                    }
                }
            }
        }

        Log.v("DM", "Exit Function: getBarCodeLogForLineItem");
        return array;
    }

    public ArrayList<BarcodeLogEntry> getInvalidScanBarcodeListForStop(){
        return  this.invaidScanBarcodeList;
    }

    public ArrayList<BarcodeLogEntry> getValidScanBarcodeListForStop(){
        return  this.validScanBarcodeList;
    }

   public ArrayList<BarcodeLogEntry> getInvalidScanBarcodeListForPO(String POName){
       Log.v("DM", "Enter Function: getInvalidScanBarcodeListForPO");

       ArrayList<BarcodeLogEntry> array = new ArrayList<BarcodeLogEntry>();

       ArrayList<BarcodeScanSessionInfo> barcodeSessionList = (ArrayList<BarcodeScanSessionInfo>) barcodeDictionary.get(POName);
       if (barcodeSessionList == null) {
           Log.e("DM","No Barcode Sessions Found for PO: " + POName);
           return null;
       }
       for(BarcodeScanSessionInfo barcodeSession : barcodeSessionList){

           ArrayList<BarcodeLogEntry> barcodeList = barcodeSession.barcodeList;

           for(BarcodeLogEntry barcodeEntry : barcodeList){

               if(!barcodeEntry.matchFound){
                   if(barcodeEntry.itemID == null){
                       //set the type of invalid barcode
                       boolean found = false;
                       for(RouteShipmentProduct routeShipmentProduct : routeshipmentProductList){

                           if(routeShipmentProduct.customerProductAltID.equalsIgnoreCase(barcodeEntry.GTIN14)){

                               barcodeEntry.invalidBarcodeType = InvalidBarcode_NotInPO;
                               barcodeEntry.itemID = routeShipmentProduct.customerProductID;
                               barcodeEntry.itemName = routeShipmentProduct.shortDescription;

                               found = true;

                           }
                       }

                       if(!found){
                           barcodeEntry.invalidBarcodeType = InvalidBarcode_UnknownItem;
                       }

                   }else{
                        //Item ID is set but item is not in PO
                       boolean found = false;
                       for(RouteShipmentProduct routeShipmentProduct : this.routeshipmentProductList) {
                           if(routeShipmentProduct.customerProductID.equalsIgnoreCase(barcodeEntry.itemID)){
                                barcodeEntry.invalidBarcodeType = InvalidBarcode_NotInPO;
                               barcodeEntry.itemName = routeShipmentProduct.shortDescription;

                               found = true;
                           }
                       }
                       if(!found){
                           barcodeEntry.invalidBarcodeType = InvalidBarcode_UnknownItem;
                       }

                   }


                   boolean entryAlreadyExists = false;

                   for (BarcodeLogEntry entry : array) {
                       if (entry.barcode.equalsIgnoreCase(barcodeEntry.barcode)) {
                           Log.v("DM", "Same Barcode Already  exists in Invalid Barcode List: " + barcodeEntry.barcode);
                           double quantityReceived = entry.quantityReceived + barcodeEntry.quantityReceived;

                           entry.quantityReceived = quantityReceived;
                           entryAlreadyExists = true;
                           break;
                       }
                   }
                   if(!entryAlreadyExists){
                       array.add(barcodeEntry);
                   }


               }
           }

       }

           Log.v("DM", "Exit Function: getInvalidScanBarcodeListForPO");
       return array;
   }

   public void saveBarcodeScanReport (String documentName, String documentPath){
       Log.v("DM", "Enter Function: saveBarcodeScanReport");

       String categoryString ="BarcodeScanReport";

       Log.v("DM", "Saving BarcodeScan Report with DocumentName: "+documentName+" on Server");

       saveDocumentWithName(documentName, null, categoryString, documentPath );

       Log.v("DM", "Exit Function: saveBarcodeScanReport");

   }

    public Boolean saveDocumentWithName(final String documentName, String referenceNumber, String category, String documentPath) {
        Log.v("DM", "Enter Function: saveDocumentWithName");

        String documentId =  UUID.randomUUID().toString();
        String[] relativeDocumentPathArr = documentPath.split("Documents/");
        String relativeDocumentPath = relativeDocumentPathArr[relativeDocumentPathArr.length-1];

        Log.v("DM", "Absolute File Path of Document: " + documentPath);
        Log.v("DM", "Absolute File Path of Image: " + relativeDocumentPath);

        DocumentInfo documentInfo = new DocumentInfo();
        documentInfo.name = documentName;
        documentInfo.documentId = documentId;
        documentInfo.documentPath = relativeDocumentPath;
        documentInfo.category = category;
        documentInfo.referenceNumber = referenceNumber;

        if(this.deliveryInfo.documentList == null){

            ArrayList<DocumentInfo> documentList = new ArrayList<DocumentInfo>();
            this.deliveryInfo.documentList = documentList;
        }

        this.deliveryInfo.documentList.add(documentInfo);

        //Upload document to Server

        final String siteName = this.siteName;

        this.dataManager.addDocumentWithDocumentId(documentId, "application/pdf", documentName, this.deliveryId, PimmRefTypeEnum.PimmRefTypesEnum.PimmRefType_Shipment, category, this.deliveryStopId, referenceNumber, relativeDocumentPath, new OnCompleteListeners.onaddDocumentWithDocumentIdListener() {
            @Override
            public void addDocumentWithDocumentId(DocumentDTO document, Error error) {
                if(error == null){
                    Log.v("DM","Successfully uploaded document: "+documentName+" on Server for Stop: " + siteName);

                }else{
                    Log.e("DM", "Failed to upload document: "+documentName+" due to error: " + error.getMessage());
                }

            }
        });

        Log.v("DM", "Exit Function: saveDocumentWithName");

        return true;

    }

    public ArrayList<DocumentInfo> getDocumentList(){
        Log.v("DM", "Enter Function: getDocumentList");
            ArrayList<DocumentInfo> docInfoArray = null;

            if(this.deliveryInfo.documentList.size() > 0 ){
                docInfoArray = this.deliveryInfo.documentList;
            }else{
                docInfoArray = null;
            }

            Log.v("DM", "Exit Function: getDocumentList");
            return docInfoArray;

        }

        public DeliveryPerformance getDeliveryPerformanceViolation() {
            Log.v("DM", "Enter Function: getDeliveryPerformanceViolation");

            if(this.deliveryInfo.deliveryPerformanceViolation != null){
                return deliveryInfo.deliveryPerformanceViolation;
            }

            if(!(deliveryInfo.tmsDeliveryStatus == TMSDeliveryStatus_DeliveryCompleted || deliveryInfo.tmsDeliveryStatus == TMSDeliveryStatus_AdditionalDeliveryCompleted)){
                Log.v("DM", "Delivery not completed for stop: " + this.siteName + " , " + this.deliveryStopId + " , hence not checking for delivery performance violation");
                return null;
            }

            if(this.sdrStop.ExpectedDeliveryDuration < 0.0 ){
                Log.v("DM", "No Scheduled Delivery Duration set for stop: " + this.siteName + " , " + this.deliveryStopId + " , hence not checking for delivery performance violation");
                return null;

            }

            int scheduledDuration = (int)sdrStop.ExpectedDeliveryDuration;
            //Date actualDeliveryDuration = this.
            long actualDeliveryDuration = deliveryInfo.deliveryForm.DeliveryEndTime.getTime() - this.deliveryInfo.deliveryForm.DeliveryStartTime.getTime();
            int deliveryDurationInMinutes = round(actualDeliveryDuration / 60);


            if(deliveryDurationInMinutes <= scheduledDuration){
                Log.v("DM", "Acutal Delivery Duration: " + (long)deliveryDurationInMinutes+ " is  less then or equal to scheduledDuration: " + scheduledDuration + ", hence not returning delivery performance for stop: " + this.siteName + " , " + this.deliveryStopId + " , hence not checking for delivery performance violation");
                return null;

            }

            DeliveryPerformance deliveryPerformance = new DeliveryPerformance();
            deliveryPerformance.stopName = this.sdrStop.SiteName;
            deliveryPerformance.deliveryStopId = this.sdrStop.SdrStopId;
            deliveryPerformance.stopOrder = (int) this.sdrStop.DeliveryOrder;
            deliveryPerformance.scheduledDuration = (int) sdrStop.ExpectedDeliveryDuration;


            deliveryPerformance.actualDuration = deliveryDurationInMinutes;

            double casesPerMinute = (double) sdrStop.TotalCases / deliveryDurationInMinutes;

            deliveryPerformance.casesPerMinute = casesPerMinute;

            int minutesOver = deliveryDurationInMinutes - (int) this.sdrStop.ExpectedDeliveryDuration;

            deliveryPerformance.minutesOver = minutesOver;
            this.deliveryInfo.deliveryPerformanceViolation = deliveryPerformance;
            routeDataCacheManager.saveDeliveryFormInfo(this.deliveryInfo, this.deliveryStopId);

            Log.v("DM", "Exit Function: getDeliveryPerformanceViolation");
            return deliveryPerformance;
        }

        public List<DeliveryPerformance.StopPerformance> getStopPerformanceViolation() {
        Log.v("DM", "Enter Function: getStopPerformanceViolation");

        ArrayList<DeliveryPerformance.StopPerformance> stopPerformanceViolationArray = new ArrayList<DeliveryPerformance.StopPerformance>();

        if(this.deliveryInfo.tmsDeliveryStatus == TMSDeliveryStatus_Pending){
            Log.v("DM", "No arrival for this stop, hence  returning Missed violation for stop: " + this.siteName + " , " + this.deliveryStopId);

            DeliveryPerformance.StopPerformance stopPerformance = new DeliveryPerformance.StopPerformance();
            stopPerformance.stopName = this.sdrStop.SiteName;
            stopPerformance.stopOrder = (int) this.sdrStop.OriginalDeliveryOrder;
            stopPerformance.scheduledDeliveryStartTime = this.sdrStop.ExpectedDeliveryTime;
//            stopPerformance.violationtype = StopViolation_Missed;
            stopPerformance.violationtype = OnTimeStatus_Late;
            stopPerformance.reasonCode = -1;
            stopPerformanceViolationArray.add(stopPerformance);

        }else if( this.deliveryInfo.tmsDeliveryStatus == TMSDeliveryStatus_DeliveryStarted ){

            Log.v("DM", "No Signature for this stop, hence  returning NoPOD violation for stop: " + this.siteName + " , " + this.deliveryStopId);

            DeliveryPerformance.StopPerformance stopPerformance = new DeliveryPerformance.StopPerformance();
            stopPerformance.stopName = this.sdrStop.SiteName;
            stopPerformance.stopOrder = (int) this.sdrStop.DeliveryOrder;
            stopPerformance.scheduledDeliveryStartTime = this.sdrStop.ExpectedDeliveryTime;
            stopPerformance.actualDeliveryStartTime = this.deliveryInfo.deliveryForm.DeliveryStartTime;
//            stopPerformance.violationtype = StopViolation_NoPOD;
            stopPerformance.violationtype = OnTimeStatus_None;
            stopPerformance.reasonCode = -1;
            stopPerformanceViolationArray.add(stopPerformance);
        }

        if(this.deliveryInfo.tmsDeliveryStatus == TMSDeliveryStatus_AdditionalDeliveryCompleted || this.deliveryInfo.tmsDeliveryStatus == TMSDeliveryStatus_AdditionalDeliveryStarted ){
            Log.v("DM", "Redelivery was activated for this stop, hence  returning Redelivery violation for stop: " + this.siteName + " , " + this.deliveryStopId);

            DeliveryPerformance.StopPerformance stopPerformance = new DeliveryPerformance.StopPerformance();
            stopPerformance.stopName = this.sdrStop.SiteName;
            stopPerformance.stopOrder = (int) this.sdrStop.DeliveryOrder;
            stopPerformance.scheduledDeliveryStartTime = this.sdrStop.ExpectedDeliveryTime;
            stopPerformance.actualDeliveryStartTime = this.deliveryInfo.deliveryForm.DeliveryStartTime;
//            stopPerformance.violationtype = StopViolation_Redelivery;
            stopPerformance.violationtype = OnTimeStatus_None;
            stopPerformance.reasonCode = -1;
            stopPerformanceViolationArray.add(stopPerformance);
        }


        if(deliveryInfo.deliveryForm.StoreInfo.OnTime == OnTimeStatus_OnTime){
            Log.v("DM", "On Time Status is OnTime , hence not returning onTime Performance Violation for this stop: " + this.siteName + " , " + this.deliveryStopId);
            return null;
        }else{
            DeliveryPerformance.StopPerformance stopPerformance = new DeliveryPerformance.StopPerformance();
            stopPerformance.stopName = this.sdrStop.SiteName;
            stopPerformance.stopOrder = (int) this.sdrStop.DeliveryOrder;
            stopPerformance.scheduledDeliveryStartTime = this.sdrStop.ExpectedDeliveryTime;
            stopPerformance.actualDeliveryStartTime = this.deliveryInfo.deliveryForm.DeliveryStartTime;
//            stopPerformance.violationtype = StopViolation_Redelivery;
            if(this.deliveryInfo.deliveryForm.StoreInfo.OnTime == OnTimeStatus_Late){
                //            stopPerformance.violationtype = StopViolation_Late;
                stopPerformance.violationtype = OnTimeStatus_Late;
            }else if(deliveryInfo.deliveryForm.StoreInfo.OnTime == OnTimeStatus_Early){
                //            stopPerformance.violationtype = StopViolation_Early;
                stopPerformance.violationtype = OnTimeStatus_Early;

            }
            stopPerformance.minutesLateOrEarly = (int) this.deliveryInfo.deliveryForm.StoreInfo.MinutesLate;
            stopPerformance.reasonCode = this.deliveryInfo.deliveryForm.StoreInfo.LateReasonCode;
            stopPerformanceViolationArray.add(stopPerformance);
        }

        if(stopPerformanceViolationArray.size() == 0){
            Log.v("DM","No peiolationrformance v found for stop: " + siteName + " , " + deliveryStopId);
            stopPerformanceViolationArray =  null;
        }

        Log.v("DM", "Exit Function: getStopPerformanceViolation");
        return stopPerformanceViolationArray;

    }

    public List<DeliveryPerformance.OrderAccuracyPerformance> getOrderAccuracyPerformanceViolation(){
       Log.v("DM", "Enter Function: getOrderAccuracyPerformanceViolation");

       List<DeliveryPerformance.OrderAccuracyPerformance> orderAccuracyPerformanceArray = null;
       List<DeliveryForm_StopProductDetail> stopProductDetail = this.deliveryInfo.deliveryForm.StopProductInfo.StopProductDetail;

       if(stopProductDetail != null) {
           for (DeliveryForm_StopProductDetail productDetail : stopProductDetail) {
               if (productDetail.QuantityShipped !=  productDetail.QuantityReceived) {
                   if (orderAccuracyPerformanceArray == null) {
                       orderAccuracyPerformanceArray = new ArrayList<DeliveryPerformance.OrderAccuracyPerformance>();
                   }

                   DeliveryPerformance.OrderAccuracyPerformance entry = new DeliveryPerformance.OrderAccuracyPerformance();
                   entry.stopName = this.sdrStop.SiteName;
                   entry.stopOrder = (int) sdrStop.DeliveryOrder;
                   entry.itemNumber = productDetail.ItemNo;
                   entry.itemName = productDetail.ProductDescription;
                   entry.quantityOrdered = (double) productDetail.QuantityOrdered;
                   entry.quantityReceived = (double) productDetail.QuantityReceived;
                   entry.reasonCode = productDetail.ReasonCode;
                   orderAccuracyPerformanceArray.add(entry);

               }
           }
       }
       if(orderAccuracyPerformanceArray.size() == 0){
           Log.v("DM","No Order Discrepancy found for stop: " + siteName + " , " + deliveryStopId);
           return  null;
       }


       Log.v("DM", "Exit Function: getOrderAccuracyPerformanceViolation");
       return orderAccuracyPerformanceArray;
   }

  public List<DeliveryPerformance.ReturnedProductInfo> getListOfProductsWithReturnToDC(){
       Log.v("DM", "Enter Function: getListOfProductsWithReturnToDC");

       List<DeliveryPerformance.ReturnedProductInfo> productsWithReturns = null;
       List<DeliveryForm_StopProductDetail> stopProductDetail = this.deliveryInfo.deliveryForm.StopProductInfo.StopProductDetail;

       if(stopProductDetail != null) {
           for (DeliveryForm_StopProductDetail productDetail : stopProductDetail) {
               if (productDetail.QuantityReturned != 0.0) {
                   if (productsWithReturns == null) {
                       productsWithReturns = new ArrayList<DeliveryPerformance.ReturnedProductInfo>();
                   }

                   DeliveryPerformance.ReturnedProductInfo entry = new DeliveryPerformance.ReturnedProductInfo();
                   entry.stopId = this.sdrStop.StopID;
                   entry.stopName = this.sdrStop.SiteName;
                   entry.po = productDetail.PO;
                   entry.stopOrder = (int) sdrStop.DeliveryOrder;
                   entry.itemNumber = productDetail.ItemNo;
                   entry.itemName = productDetail.ProductDescription;
                   entry.quantityReturned = (double) productDetail.QuantityReturned;
                   entry.reasonCode = productDetail.ReasonCode;
                   entry.returnType = DeliveryPerformance.ReturnedProductTypeEnum.ReturnType_Rejected;
                   entry.itemInOriginalPackaging = productDetail.returnedItemInOriginalPackaging;
                   productsWithReturns.add(entry);

               }
           }
       }
       if(productsWithReturns.size() == 0){
           Log.v("DM","No Products with Returns found for stop: " + siteName + " , " + deliveryStopId);
           return  null;
       }


       Log.v("DM", "Exit Function: getListOfProductsWithReturnToDC");
       return productsWithReturns;
   }

   public void saveDeliveryPerformance(DeliveryPerformance deliveryPerformance){
       Log.v("DM", "Enter Function: saveDeliveryPerformance");
       Log.v("DM", "Saving Delivery Performance Violation for Stop: " + this.siteName + " , "+ this.deliveryStopId  );

        deliveryInfo.deliveryPerformanceViolation = deliveryPerformance;
       routeDataCacheManager.saveDeliveryFormInfo(this.deliveryInfo, this.deliveryStopId);
       Log.v("DM", "Exit Function: saveDeliveryPerformance");

   }



   public void updateSdrStopData(SdrStop sdrStop){
       Log.v("DM", "Enter Function: updateSdrStopData");
       this.sdrStop = sdrStop;
       Log.v("DM", "Exit Function: updateSdrStopData");

   }

   public  void setDeliveryStatusToStarted(){
       Log.v("DM", "Enter Function: setDeliveryStatusToStarted");

       this.deliveryInfo.tmsDeliveryStatus = TMSDeliveryStatus_DeliveryStarted;
       routeDataCacheManager.saveDeliveryFormInfo(this.deliveryInfo, this.deliveryStopId);

       Log.v("DM", "Exit Function: setDeliveryStatusToStarted");
   }
   public ArrayList<BarcodeLogEntry> addBarcodeSessionForPO(String PO, Date sessionStartTime, Date sessionEndTime, ArrayList<String> barcodeList){
       Log.v("DM", "Enter Function: addBarcodeSessionForPO");

       int scanCounter = 0;
       if(this.barcodeDictionary.get(PO) != null){
           ArrayList<BarcodeScanSessionInfo> barcodeSessionList =  (ArrayList<BarcodeScanSessionInfo>) this.barcodeDictionary.get(PO);

           for(BarcodeScanSessionInfo session : barcodeSessionList){
               scanCounter = scanCounter + session.barcodeList.size();
           }
       }


       BarcodeScanSessionInfo barcodeScanSession = new BarcodeScanSessionInfo();

       barcodeScanSession.sessionId = this.generateBarcodeSessionId();
       barcodeScanSession.deliveryStopID = this.deliveryStopId;
       barcodeScanSession.PO = PO;
       barcodeScanSession.sessionStartTime = sessionStartTime;
       barcodeScanSession.sessionEndTime = sessionEndTime;

       ArrayList<BarcodeLogEntry> barcodeLogArray = new ArrayList<BarcodeLogEntry>();

       for (String barcode : barcodeList){
           BarcodeLogEntry barcodeLogEntry = decodeBarcode(barcode);

           if(barcodeLogEntry != null){

               barcodeLogEntry.barcodeEntryId = this.generateBarcodeEntryId();
               barcodeLogArray.add(barcodeLogEntry);
           }
       }

       barcodeScanSession.barcodeList = barcodeLogArray;

       if(this.barcodeDictionary.get(PO) == null){
           Log.v("DM","Adding new  entry for PO: " + PO + " to barcodeDictionary");

           ArrayList<BarcodeScanSessionInfo> barcodeSessionList = new ArrayList<BarcodeScanSessionInfo>();
           barcodeSessionList.add(barcodeScanSession);
           this.barcodeDictionary.put(PO, barcodeSessionList);

       }else{
           Log.v("DM","Entry Already exists for PO: " + PO + " in barcodeDictionary");
           Log.v("DM","Adding new Scan Session to BarcodeSessionList for PO: " + PO + " ,SessionId " + barcodeScanSession.sessionId);

           ArrayList<BarcodeScanSessionInfo> barcodeSessionList = (ArrayList<BarcodeScanSessionInfo>) this.barcodeDictionary.get(PO);
           barcodeSessionList.add(barcodeScanSession);
           this.barcodeDictionary.put(PO, barcodeSessionList);

//             [[NSUserDefaults standardUserDefaults] setValue:[[NSNumber alloc] initWithInteger:barcodeEntryId] forKey:_barcodeEntryIdKey];
//
//            [[NSUserDefaults standardUserDefaults] setValue:[[NSNumber alloc] initWithInteger:barcodeSessionId] forKey:_barcodeSessionIdKey];
//
//            [[NSUserDefaults standardUserDefaults] synchronize];

       }
       Log.v("DM","Added New Barcode Session For PO With Barcode Count: " +PO+ ", " + barcodeScanSession.barcodeList);
        ArrayList<BarcodeLogEntry> invalidScanList = getInvalidScanBarcodeListForBarcodeSession(barcodeScanSession);

       saveBarcodeDataToDiskForPO(PO);
       Log.v("DM", "Enter Function: addBarcodeSessionForPO");
       return invalidScanList ;
   }


   public void resetQuantityReceivedValuesForPO(String POName){

       Log.v("DM", "Enter Function: resetQuantityReceivedValuesForPO");
        ArrayList<BarcodeScanSessionInfo> barcodeSessionList = (ArrayList<BarcodeScanSessionInfo>) this.barcodeDictionary.get(POName);
       // Check if this is the First Barcode Scan Session

       if(barcodeSessionList == null){

           ArrayList<DeliveryForm_StopProductDetail> productsForPO = getProductsForPO(POName);

           for(DeliveryForm_StopProductDetail stopProductDetail : productsForPO){
               stopProductDetail.QuantityReceived = (double) 0.0;
               stopProductDetail.hasManualQuantityReceived = false;

           }
           routeDataCacheManager.saveDeliveryFormInfo(this.deliveryInfo, this.deliveryStopId);
       }
           Log.v("DM", "Exit Function: resetQuantityReceivedValuesForPO");

   }

   public BarcodeLogEntry addScannedBarcode(String scannedBarcode, String POName){
       Log.v("DM", "Enter Function: addScannedBarcode");

        if(this.validScanBarcodeList == null){
            this.validScanBarcodeList = new ArrayList<BarcodeLogEntry>();
        }

        BarcodeLogEntry barcodeLogEntry = decodeBarcode(scannedBarcode);
       //check for Duplicate Scan

       if(this.validScanBarcodeList != null && validScanBarcodeList.size() > 0){

           for(BarcodeLogEntry entry : this.validScanBarcodeList){

               if(entry.barcode.equalsIgnoreCase(scannedBarcode)){
                    Log.v("DM", "Duplicate Scan for Barcode:" + scannedBarcode);

                   barcodeLogEntry.valid = false;
                   barcodeLogEntry.invalidBarcodeType = InvalidBarcode_DuplicateScan;

                   if (this.invaidScanBarcodeList == null) {
                    this.invaidScanBarcodeList = new ArrayList<BarcodeLogEntry>();
                   }

                   this.invaidScanBarcodeList.add(barcodeLogEntry);
                   return barcodeLogEntry;


                   }
               }
           }

       //Check if Barcode belongs to given PO
        if(barcodeLogEntry.PO.equalsIgnoreCase(POName)){
            Log.v("DM", "Scanned Barcode Belongs to given PO");

            barcodeLogEntry.valid = true;
            //Add Entry to Valid Barcode Scan List
            this.validScanBarcodeList.add(barcodeLogEntry);

            //Update Quantity Scanned/Received in DeliveryForm_StopProductDetail for the item

            List<DeliveryForm_StopProductDetail> productsForSelectedPO = getProductsForPO(POName);

            if(barcodeLogEntry.PO.equalsIgnoreCase(POName)){

                for(DeliveryForm_StopProductDetail stopProductDetail : productsForSelectedPO){

                    if(stopProductDetail.PO.equalsIgnoreCase(POName)){

                        if(barcodeLogEntry.itemID.equalsIgnoreCase(stopProductDetail.ItemNo)){

                            barcodeLogEntry.itemName = stopProductDetail.ProductDescription;
                            barcodeLogEntry.matchFound = true;

                            for(String key : stopProductDetail.barcodeIdentifierList.keySet()){

                                if(key.equalsIgnoreCase(barcodeLogEntry.numOfValueStr)){

                                    List<Object> array = (ArrayList<Object>) stopProductDetail.barcodeIdentifierList.get(key);

                                    double scanCount =  array.indexOf(0);

                                    if(scanCount > 0){
                                        Log.e("DM","Scan Count already 1, this should have been caught before as  Duplicate Scan");
                                    }else{
                                        List<Object> editedArray = array;
                                        editedArray.add(0, 1);
                                        editedArray.add(1, 1);

                                        List<Object> updatedArray = editedArray;

                                        stopProductDetail.barcodeIdentifierList.put(key, updatedArray);

                                    }
                                }
                            }
                        }
                    }

                }


            }
        }else{
            Log.v("DM", "Scanned Barcode: "+ scannedBarcode +"  does not belong to PO: " + POName);

            barcodeLogEntry.valid= false;
            boolean found = false;

            for(RouteShipmentProduct routeShipmentProduct : this.routeshipmentProductList){

                if(routeShipmentProduct.customerProductID.equalsIgnoreCase(barcodeLogEntry.itemID)){

                    barcodeLogEntry.invalidBarcodeType = InvalidBarcode_NotInPO;
                    barcodeLogEntry.itemID = routeShipmentProduct.customerProductID;
                    barcodeLogEntry.itemName = routeShipmentProduct.shortDescription;
                    found = true;
                }
            }

            if(!found){

               barcodeLogEntry.invalidBarcodeType = InvalidBarcode_UnknownItem;
            }

            if(this.invaidScanBarcodeList == null){
                invaidScanBarcodeList = new ArrayList<BarcodeLogEntry>();

            }
            this.invaidScanBarcodeList.add(barcodeLogEntry);
        }



       Log.v("DM", "Enter Function: addScannedBarcode");
       return  barcodeLogEntry;

   }


    public BarcodeScanSessionInfo addNewBarcodeSessionForPO(String PO, Date sessionStartTime, Date sessionEndTime, ArrayList<String> barcodeList){
        Log.v("DM", "Enter Function: addNewBarcodeSessionForPO");

        BarcodeScanSessionInfo barcodeScanSession = new BarcodeScanSessionInfo();

        barcodeScanSession.sessionId = this.generateBarcodeSessionId();
        barcodeScanSession.deliveryStopID = this.deliveryStopId;
        barcodeScanSession.PO = PO;
        barcodeScanSession.sessionStartTime = sessionStartTime;
        barcodeScanSession.sessionEndTime = sessionEndTime;

        ArrayList<BarcodeLogEntry> barcodeLogArray = new ArrayList<BarcodeLogEntry>();

        for (String barcode : barcodeList){
            BarcodeLogEntry barcodeLogEntry = decodeBarcode(barcode);

            if(barcodeLogEntry != null){

                barcodeLogEntry.barcodeEntryId = this.generateBarcodeEntryId();
                barcodeLogArray.add(barcodeLogEntry);
            }
        }

        barcodeScanSession.barcodeList = barcodeLogArray;

        if(this.barcodeDictionary.get(PO) == null){
            Log.v("DM","Adding new  entry for PO: " + PO + " to barcodeDictionary");

            ArrayList<BarcodeScanSessionInfo> barcodeSessionList = new ArrayList<BarcodeScanSessionInfo>();
            barcodeSessionList.add(barcodeScanSession);
            this.barcodeDictionary.put(PO, barcodeSessionList);

        }else{
            Log.v("DM","Entry Already exists for PO: " + PO + " in barcodeDictionary");
            Log.v("DM","Adding new Scan Session to BarcodeSessionList for PO: " + PO + " ,SessionId " + barcodeScanSession.sessionId);

            ArrayList<BarcodeScanSessionInfo> barcodeSessionList = (ArrayList<BarcodeScanSessionInfo>) this.barcodeDictionary.get(PO);
            barcodeSessionList.add(barcodeScanSession);
            this.barcodeDictionary.put(PO, barcodeSessionList);

//             [[NSUserDefaults standardUserDefaults] setValue:[[NSNumber alloc] initWithInteger:barcodeEntryId] forKey:_barcodeEntryIdKey];
//
//            [[NSUserDefaults standardUserDefaults] setValue:[[NSNumber alloc] initWithInteger:barcodeSessionId] forKey:_barcodeSessionIdKey];
//
//            [[NSUserDefaults standardUserDefaults] synchronize];

        }


        Log.v("DM","Added New Barcode Session For PO With Barcode Count: " +PO+ ", " + barcodeScanSession.barcodeList);

        saveBarcodeDataToDiskForPO(PO);

        Log.v("DM", "Exit Function: addNewBarcodeSessionForItem");
        return barcodeScanSession;
    }

    public BarcodeScanSessionInfo addNewBarcodeSessionForItem (DeliveryForm_StopProductDetail stopProductDetail, Date sessionStartTime, Date sessionEndTime, ArrayList<String> barcodeList){
        Log.v("DM", "Enter Function: addNewBarcodeSessionForItem");
//        if(this.barcodeDictionary == null){
//            this.barcodeDictionary =
//        }

        String PO = stopProductDetail.PO;
        BarcodeScanSessionInfo barcodeScanSession = new BarcodeScanSessionInfo();

        barcodeScanSession.sessionId = this.generateBarcodeSessionId();
        barcodeScanSession.deliveryStopID = this.deliveryStopId;
        barcodeScanSession.PO = stopProductDetail.PO;
        barcodeScanSession.sessionStartTime = sessionStartTime;
        barcodeScanSession.sessionEndTime = sessionEndTime;

        ArrayList<BarcodeLogEntry> barcodeLogArray = new ArrayList<BarcodeLogEntry>();
        for (String barcode : barcodeList){
            BarcodeLogEntry barcodeLogEntry = decodeBarcode(barcode);

            if(barcodeLogEntry != null){
                //Filter out the entries not for the given item

                if(barcodeLogEntry.barcodeType != BarcodeType_Custom_SFM){
                    Log.v("DM","Ignoring Barcode: " + barcodeLogEntry.barcode + " because its for a different item");
                    continue;
                }

                if(barcodeLogEntry.itemID.equalsIgnoreCase(stopProductDetail.ItemNo)){
                    barcodeLogEntry.barcodeEntryId = this.generateBarcodeEntryId();
                    barcodeLogArray.add(barcodeLogEntry);
                }
            }
        }

        barcodeScanSession.barcodeList = barcodeLogArray;

        if(this.barcodeDictionary.get(PO) == null){
            Log.v("DM","Adding new  entry for PO: " + PO + " to barcodeDictionary");

            ArrayList<BarcodeScanSessionInfo> barcodeSessionList = new ArrayList<BarcodeScanSessionInfo>();
            barcodeSessionList.add(barcodeScanSession);
            this.barcodeDictionary.put(PO, barcodeSessionList);

        }else{
            Log.v("DM","Entry Already exists for PO: " + PO + " in barcodeDictionary");
            Log.v("DM","Adding new Scan Session to BarcodeSessionList for PO: " + PO + " ,SessionId " + barcodeScanSession.sessionId);

            ArrayList<BarcodeScanSessionInfo> barcodeSessionList = (ArrayList<BarcodeScanSessionInfo>) this.barcodeDictionary.get(PO);
            barcodeSessionList.add(barcodeScanSession);
            this.barcodeDictionary.put(PO, barcodeSessionList);

//             [[NSUserDefaults standardUserDefaults] setValue:[[NSNumber alloc] initWithInteger:barcodeEntryId] forKey:_barcodeEntryIdKey];
//
//            [[NSUserDefaults standardUserDefaults] setValue:[[NSNumber alloc] initWithInteger:barcodeSessionId] forKey:_barcodeSessionIdKey];
//
//            [[NSUserDefaults standardUserDefaults] synchronize];

        }

        Log.v("DM","Added New Barcode Session For PO With Barcode Count: " +PO+ ", " + barcodeScanSession.barcodeList);

        saveBarcodeDataToDiskForPO(PO);




        Log.v("DM", "Exit Function: addNewBarcodeSessionForItem");

        return  barcodeScanSession;
    }

    public ArrayList<DeliveryForm_StopProductDetail> processBarCodeDataForSession(BarcodeScanSessionInfo barcodeSession){

        Log.v("DM", "Enter Function: processBarCodeDataForSession");

        String POName = barcodeSession.PO;
        //Update the barcode List from the saved Barcode List in Dictionary because it could change due to deletion of entries.

        ArrayList<BarcodeScanSessionInfo> barcodeSessionList = (ArrayList<BarcodeScanSessionInfo>) this.barcodeDictionary.get(POName);
        if(barcodeSessionList == null){
            Log.e("DM","No Barcode Sessions Found for PO: " + POName);

        }

        ArrayList<BarcodeLogEntry> barcodeList = barcodeSession.barcodeList;
        for(BarcodeLogEntry barcodeEntry : barcodeList){
            barcodeEntry.matchFound = false;
        }

        ArrayList<DeliveryForm_StopProductDetail> productsForSelectedPO = getProductsForPO(POName);

        ArrayList<DeliveryForm_StopProductDetail> productsWithDiscrepancy = new ArrayList<DeliveryForm_StopProductDetail>();

        for(DeliveryForm_StopProductDetail stopProductDetail :  productsForSelectedPO){
            double barcodeScanReceivedQuantity = 0.0;

            barcodeScanReceivedQuantity = stopProductDetail.QuantityScanned;

            for(BarcodeLogEntry barcodeEntry : barcodeList){
                if(barcodeEntry.matchFound == true){
                    continue;
                }

                if(barcodeEntry.GTIN14.equalsIgnoreCase(stopProductDetail.BarcodeIdentifier)){

                    //GTIN Match

                    barcodeEntry.itemID = stopProductDetail.ItemNo;
                    barcodeEntry.itemName = stopProductDetail.ProductDescription;
                    barcodeEntry.PO = stopProductDetail.PO;

                    //LotNumber
                    if(barcodeEntry.gs1BarcodeComponents.lotNumber != null && !barcodeEntry.gs1BarcodeComponents.lotNumber.equalsIgnoreCase("")){

                        if(stopProductDetail.LotID != null && !stopProductDetail.LotID.equalsIgnoreCase("")){

                            if(stopProductDetail.LotID.equalsIgnoreCase(barcodeEntry.gs1BarcodeComponents.lotNumber)){
                                //Lot id matched
                            }else{
                                //Lot id does not match
                                barcodeEntry.valid = false;
                                barcodeEntry.invalidBarcodeType = InvalidBarcode_LotMismatch;
                                continue;
                            }
                        } //LOT ID is not provided in the order detail....

                    }

                    /// Best Before TBD Check the valid format YYMMDD for the date
                     if(barcodeEntry.gs1BarcodeComponents.bestBeforeDateStr != null && !barcodeEntry.gs1BarcodeComponents.bestBeforeDateStr.equalsIgnoreCase("")){

                        if(stopProductDetail.BestBefore != null && !stopProductDetail.BestBefore.equalsIgnoreCase("")){

                            if(stopProductDetail.BestBefore.equalsIgnoreCase(barcodeEntry.gs1BarcodeComponents.bestBeforeDateStr)){
                                //bestBefore id matched
                            }else{
                                //bestBefore does not match

                                barcodeEntry.valid = false;
                                barcodeEntry.invalidBarcodeType = InvalidBarcode_SellByDateMismatch;
                                continue;
                            }
                        }

                    }
                    // SellByDate
                    if(barcodeEntry.gs1BarcodeComponents.sellByDateStr != null && !barcodeEntry.gs1BarcodeComponents.sellByDateStr.equalsIgnoreCase("")){

                        if(stopProductDetail.SellByDate != null && !stopProductDetail.SellByDate.equalsIgnoreCase("")){

                            if(stopProductDetail.SellByDate.equalsIgnoreCase(barcodeEntry.gs1BarcodeComponents.sellByDateStr)){
                                //SellByDate id matched
                            }else{
                                //SellByDate does not match

                                barcodeEntry.valid = false;
                                barcodeEntry.invalidBarcodeType = InvalidBarcode_SellByDateMismatch;
                                continue;
                            }
                        }

                    }
                    // PackagingDate
                    if(barcodeEntry.gs1BarcodeComponents.packagingDateStr != null && !barcodeEntry.gs1BarcodeComponents.packagingDateStr.equalsIgnoreCase("")){

                        if(stopProductDetail.PackagingDate != null && !stopProductDetail.PackagingDate.equalsIgnoreCase("")){

                            if(stopProductDetail.PackagingDate.equalsIgnoreCase(barcodeEntry.gs1BarcodeComponents.packagingDateStr)){
                                //PackagingDate id matched
                            }else{
                                //PackagingDate does not match

                                barcodeEntry.valid = false;
                                barcodeEntry.invalidBarcodeType = InvalidBarcode_ProductionDateMismatch;
                                continue;
                            }
                        }

                    }
                    // PackagingDate
                    if(barcodeEntry.gs1BarcodeComponents.productionDateStr != null && !barcodeEntry.gs1BarcodeComponents.productionDateStr.equalsIgnoreCase("")){

                        if(stopProductDetail.ProductionDate != null && !stopProductDetail.ProductionDate.equalsIgnoreCase("")){

                            if(stopProductDetail.ProductionDate.equalsIgnoreCase(barcodeEntry.gs1BarcodeComponents.productionDateStr)){
                                //ProductionDate id matched
                            }else{
                                //ProductionDate does not match

                                barcodeEntry.valid = false;
                                barcodeEntry.invalidBarcodeType = InvalidBarcode_ProductionDateMismatch;
                                continue;
                            }
                        }

                    }
                    // ExpirationDate
                    if(barcodeEntry.gs1BarcodeComponents.expirationDateStr != null && !barcodeEntry.gs1BarcodeComponents.expirationDateStr.equalsIgnoreCase("")){

                        if(stopProductDetail.ExpirationDate != null && !stopProductDetail.ExpirationDate.equalsIgnoreCase("")){

                            if(stopProductDetail.ExpirationDate.equalsIgnoreCase(barcodeEntry.gs1BarcodeComponents.expirationDateStr)){
                                //ExpirationDate id matched
                            }else{
                                //ExpirationDate does not match

                                barcodeEntry.valid = false;
                                barcodeEntry.invalidBarcodeType = InvalidBarcode_ExpirationDateMismatch;
                                continue;
                            }
                        }

                    }
                    // CatchWeight
                    if(barcodeEntry.gs1BarcodeComponents.catchWeight > 0.0){

                        if(stopProductDetail.CatchWeight > 0.0){

                            if(stopProductDetail.CatchWeight == barcodeEntry.gs1BarcodeComponents.catchWeight){
                                //CatchWeight id matched
                            }else{
                                //CatchWeight does not match

                                barcodeEntry.valid = false;
                                barcodeEntry.invalidBarcodeType = InvalidBarcode_CatchWeightMismatch;
                                continue;
                            }
                        }

                    }

                    // Match Found update quantity scanned
//                    barcodeScanReceivedQuantity = barcodeScanReceivedQuantity + barcodeEntry.quantityReceived;
                    barcodeScanReceivedQuantity = barcodeScanReceivedQuantity + barcodeEntry.quantityScanned;
                    barcodeEntry.matchFound = true;

                    if(barcodeScanReceivedQuantity > stopProductDetail.QuantityShipped){
                        barcodeEntry.valid = false;
                        barcodeEntry.invalidBarcodeType = InvalidBarcode_Over;
                        productsWithDiscrepancy.add(stopProductDetail);
                    }else{
                        barcodeEntry.valid = true;
                    }

                }//GTIN doesnot match
            }

            if(barcodeScanReceivedQuantity > 0.0){
                stopProductDetail.QuantityShipped = (int) round(barcodeScanReceivedQuantity);
            }
        }

        routeDataCacheManager.saveDeliveryFormInfo(this.deliveryInfo, this.deliveryStopId);



        Log.v("DM", "Exit Function: processBarCodeDataForSession");
        return  productsWithDiscrepancy;
    }

    public ArrayList<DeliveryForm_StopProductDetail> processCustomBarCodeDataForSession(BarcodeScanSessionInfo barcodeSession){

        Log.v("DM", "Enter Function: processCustomBarCodeDataForSession");
        String POName = barcodeSession.PO;
        //Update the barcode List from the saved Barcode List in Dictionary because it could change due to deletion of entries.

        ArrayList<BarcodeScanSessionInfo> barcodeSessionList = (ArrayList<BarcodeScanSessionInfo>) this.barcodeDictionary.get(POName);
        if(barcodeSessionList == null){
            Log.e("DM","No Barcode Sessions Found for PO: " + POName);

        }

        ArrayList<BarcodeLogEntry> barcodeList = barcodeSession.barcodeList;
        for(BarcodeLogEntry barcodeEntry : barcodeList){
            barcodeEntry.matchFound = false;
        }

        ArrayList<DeliveryForm_StopProductDetail> productsForSelectedPO = getProductsForPO(POName);

        ArrayList<DeliveryForm_StopProductDetail> productsWithDiscrepancy = new ArrayList<DeliveryForm_StopProductDetail>();

        for(DeliveryForm_StopProductDetail stopProductDetail :  productsForSelectedPO) {
//            double barcodeScanReceivedQuantity = 0.0;
//
//            barcodeScanReceivedQuantity = stopProductDetail.QuantityScanned;

            for (BarcodeLogEntry barcodeEntry : barcodeList) {
                if (barcodeEntry.matchFound == true) {
                    continue;
                }

                if(barcodeEntry.barcodeType != BarcodeType_Custom_SFM){
                    barcodeEntry.valid = false;
                    barcodeEntry.invalidBarcodeType = InvalidBarcode_UnsuppportedFormat;
                    continue;
                }

                if(barcodeEntry.itemID.equalsIgnoreCase(stopProductDetail.ItemNo) && barcodeEntry.PO.equalsIgnoreCase(stopProductDetail.PO)){
                    //GTIN Match

                    barcodeEntry.itemName = stopProductDetail.ProductDescription;
                    barcodeEntry.matchFound = true;

                    try {

                        for(String key : stopProductDetail.barcodeIdentifierList.keySet()){

                            if(key.equalsIgnoreCase(barcodeEntry.numOfValueStr)){

//                                Number scanCount = (Number) stopProductDetail.barcodeIdentifierList.get(key);

                                ArrayList<Object> array = (ArrayList<Object>) stopProductDetail.barcodeIdentifierList.get(key);
                                double scanCount = array.indexOf(0);
//                                int newCount = scanCount + 1;

                                int quantityReceived = array.lastIndexOf(1);
                                int newScanCount = (int) (scanCount + 1);

                                double newQuantityReceived = (double) quantityReceived;

                                ArrayList<Object> editedArray = array;
                                editedArray.add(0, newScanCount);
                                editedArray.add(1, newQuantityReceived);

                                ArrayList<Object> updatedArray = editedArray;


                                stopProductDetail.barcodeIdentifierList.put(key, updatedArray);

                                if(newScanCount > 1){
                                    Log.v("DM", "Duplicate Scan of the same box for ItemNo: " + barcodeEntry.itemID + " Item: " + barcodeEntry.itemName);
                                    barcodeEntry.valid = false;
                                    barcodeEntry.invalidBarcodeType = InvalidBarcode_DuplicateScan;

                                    //Duplicate Scan, dont update Barcode Scan Count for Quantity Received
                                }
//                                else{
//                                    //Update Quantity Received
//                                        barcodeScanReceivedQuantity = barcodeScanReceivedQuantity + barcodeEntry.quantityReceived;
//                                        barcodeEntry.valid = true;
//
//                                }
                            }
                        }

                    }catch (Exception e){
                        Log.e("DM","Caught Exception: " + e.getMessage());

                    }
                } // GTIN does not match

            }

//            if(barcodeScanReceivedQuantity > 0.0){
//                stopProductDetail.QuantityScanned = (int) round(barcodeScanReceivedQuantity);
//
//                if(stopProductDetail.hasManualQuantityReceived == false){
//                    stopProductDetail.QuantityReceived = (int) round(barcodeScanReceivedQuantity);
//                }
//            }

            double barcodeScanReceivedQuantity = 0.0;

            double barcodeScanCount  = 0.0;

            for(String key : stopProductDetail.barcodeIdentifierList.keySet()){
                ArrayList<Object> array = (ArrayList<Object>) stopProductDetail.barcodeIdentifierList.get(key);
                double scanCount = array.indexOf(0);

                double quantityReceived = array.lastIndexOf(1);

                barcodeScanReceivedQuantity = barcodeScanReceivedQuantity +  quantityReceived;
                barcodeScanCount = barcodeScanCount + (int)scanCount;

            }

            stopProductDetail.QuantityScanned = (int)barcodeScanCount;
            stopProductDetail.QuantityReceived = (double) barcodeScanReceivedQuantity;

        }

        routeDataCacheManager.saveDeliveryFormInfo(this.deliveryInfo, this.deliveryStopId);
        Log.v("DM", "Exit Function: processCustomBarCodeDataForSession");
        return  productsWithDiscrepancy;
    }


    public BarcodeLogEntry decodeBarcode(String barcode) {
        Log.v("DM", "Enter Function: decodeBarcode");

        if(barcode == null || barcode.equalsIgnoreCase("")){
            return null;
        }
         int index = 0;
        //Range range =  Range(index, 2);
        String str = barcode.substring(index, 2);
        if(str.equalsIgnoreCase("99") && (barcode.length() == 20 || barcode.length() == 40)){
            //Decode as Custom Barcod
            BarcodeLogEntry barcodeLogEntry = decodeAsSFMCustomBarcode(barcode);
            barcodeLogEntry.barcodeType = BarcodeLogEntry.BarcodeType_Enum.BarcodeType_Custom_SFM;
            barcodeLogEntry.quantityReceived = 1.0;
            barcodeLogEntry.quantityScanned = 1.0;
            return  barcodeLogEntry;
        }

        BarcodeLogEntry barcodeLogEntry = new BarcodeLogEntry();
        barcodeLogEntry.barcode = barcode;
        if(barcode.length() == 12){

            Log.v("DM", "UPC Bar Code: " +  barcode);
            //Pad "00" at beginning  to Barcode to make it GTIN -14
            String paddingString = "00";
            String newString = paddingString.concat(barcode);
            Log.v("DM", "GTIN-14 Identifier: "+ newString +" from GTIN-12 (UPC) Barcode: " +  barcode);

            barcodeLogEntry.GTIN14 = newString;
            barcodeLogEntry.barcodeType = BarcodeLogEntry.BarcodeType_Enum.BarcodeType_UPC;
            barcodeLogEntry.quantityReceived = 1.0;
            barcodeLogEntry.quantityScanned = 1.0;

        }else{
            //GS1-128 Bar Code
            GS1Barcode gs1barcode = new GS1Barcode();
            GS1BarcodeComponents barcodeComponents = gs1barcode.decodeAsGS1Barcode(barcode);

            if(barcodeComponents != null && barcodeComponents.valid == true) {
                barcodeLogEntry.barcodeType = BarcodeType_GS1_128;
                barcodeLogEntry.GTIN14 =barcodeComponents.GTIN14;
                barcodeLogEntry.gs1BarcodeComponents = barcodeComponents;

                barcodeLogEntry.quantityReceived =1.0;
                barcodeLogEntry.quantityScanned = 1.0;


            } else {
                Log.e("Dm","Error decoding Barcode as GS1-128");
                barcodeLogEntry.invalidBarcodeType = InvalidBarcode_UnsuppportedFormat;
                barcodeLogEntry.barcodeType = BarcodeType_Unknown;
                barcodeLogEntry.valid = false;
                barcodeLogEntry.matchFound = false;
                barcodeLogEntry.quantityReceived = 1.0;
                barcodeLogEntry.quantityScanned = 1.0;
            }

        }


        Log.v("DM", "Exit Function: decodeBarcode");

        return  barcodeLogEntry;
    }

    public BarcodeLogEntry decodeAsSFMCustomBarcode(String barcodeStr) {
        Log.v("DM", "Enter Function: decodeAsSFMCustomBarcode");
        Log.v("DM", "Decoding Barcode: "+ barcodeStr +" as custom SFM Barcode");

        String dcCustomIdentifier = "";
        int index = 0;
        String str = barcodeStr.substring(index,2);

        if(!(str.equalsIgnoreCase("99"))){
            Log.e("Dm","InValid DC Custom Identifier: " + str);
            return  null;
        }else{
            Log.v("Dm","Valid DC Custom Identifier:: " + str);
            dcCustomIdentifier = str;

            BarcodeLogEntry barcodeEntry = new BarcodeLogEntry();
            barcodeEntry.barcode = barcodeStr;

            if(barcodeStr.length() == 40){
               //SFM Barcode
                index = 2;
                str = barcodeStr.substring(index, 10);
                String POStr = str.replaceAll(". ", "");
                Log.v("DM","PO number is : " + POStr);
                barcodeEntry.PO = POStr;

                index = 12;
                str = barcodeStr.substring(index, 25);
                String itemNumStr = str.replaceAll(". ", "");
                Log.v("DM","Item number is " + itemNumStr);
                barcodeEntry.itemID = itemNumStr;

                index = 37;
                str = barcodeStr.substring(index, 3);
                Log.v("DM"," number is " + str);
                barcodeEntry.numOfValueStr = str;

                return  barcodeEntry;

            }else{
                //Procuro Custom Barcode Format for Testing
                index = 2;
                str = barcodeStr.substring(index, 8);
                String POStr = str.replaceAll("A", "");
                Log.v("DM","PO number is : " + POStr);
                barcodeEntry.PO = POStr;

                index = 10;
                str = barcodeStr.substring(index, 7);
                String itemNumStr = str.replaceAll(". ", "");
                Log.v("DM","Item number is " + itemNumStr);
                barcodeEntry.itemID = itemNumStr;

                index = 17;
                str = barcodeStr.substring(index, 3);
                Log.v("DM"," number is " + str);
                barcodeEntry.numOfValueStr = str;
                return  barcodeEntry;


            }


        }




//        Log.v("DM", "Exit Function: decodeAsSFMCustomBarcode");


    }


    public ArrayList<BarcodeLogEntry> getInvalidScanBarcodeListForBarcodeSession(BarcodeScanSessionInfo barcodeSession){
       Log.v("DM", "Enter Function: getInvalidScanBarcodeListForBarcodeSession");

       ArrayList<BarcodeLogEntry> array = new ArrayList<BarcodeLogEntry>();
       ArrayList<BarcodeLogEntry> barcodeList = barcodeSession.barcodeList;

       for(BarcodeLogEntry barcodeEntry : barcodeList) {

           if (!barcodeEntry.matchFound) {
               if (barcodeEntry.itemID == null) {
                   //set the type of invalid barcode
                   boolean found = false;
                   for (RouteShipmentProduct routeShipmentProduct : this.routeshipmentProductList) {

                       if (routeShipmentProduct.customerProductAltID.equalsIgnoreCase(barcodeEntry.GTIN14)) {
                           barcodeEntry.invalidBarcodeType = InvalidBarcode_NotInPO;
                           barcodeEntry.itemID = routeShipmentProduct.customerProductID;
                           barcodeEntry.itemName = routeShipmentProduct.shortDescription;
                           found = true;

                       }
                   }

                   if (!found) {
                       barcodeEntry.invalidBarcodeType = InvalidBarcode_UnknownItem;
                   }
               } else {
                   //Item ID is set

                   boolean found = false;
                   for (RouteShipmentProduct routeShipmentProduct : this.routeshipmentProductList) {
                       if (routeShipmentProduct.customerProductID.equalsIgnoreCase(barcodeEntry.itemID)) {
                           barcodeEntry.invalidBarcodeType = InvalidBarcode_NotInPO;
                           barcodeEntry.itemName = routeShipmentProduct.shortDescription;
                           found = true;
                       }
                   }

                   if (!found) {
                       barcodeEntry.invalidBarcodeType = InvalidBarcode_UnknownItem;
                   }
               }

               boolean entryAlreadyExists = false;

               for (BarcodeLogEntry entry : array) {
                   if (entry.barcode.equalsIgnoreCase(barcodeEntry.barcode)) {
                       Log.v("DM", "Same Barcode Already  exists in Invalid Barcode List: " + barcodeEntry.barcode);
                       double quantityReceived = entry.quantityReceived + barcodeEntry.quantityReceived;

                       entry.quantityReceived = quantityReceived;
                       entryAlreadyExists = true;
                       break;
                   }
               }
               if(!entryAlreadyExists){
                   array.add(barcodeEntry);
               }

           }
       }

       Log.v("DM", "Exit Function: getInvalidScanBarcodeListForBarcodeSession");
       return array;
   }

   public ArrayList<DeliveryForm_StopProductDetail>  getDuplicateScanBarcodeListForBarcodeSession (BarcodeScanSessionInfo barcodeSession ){

       String POName = barcodeSession.PO;
       ArrayList<DeliveryForm_StopProductDetail> productListForPO = getProductsForPO(POName);

       ArrayList<BarcodeLogEntry> barcodeList = barcodeSession.barcodeList;
       ArrayList<DeliveryForm_StopProductDetail> productListWithDuplicateScans = new ArrayList<DeliveryForm_StopProductDetail>();

       for(BarcodeLogEntry barcodeEntry : barcodeList) {

           if(!barcodeEntry.valid){
               if(barcodeEntry.invalidBarcodeType == InvalidBarcode_DuplicateScan){
                   for(DeliveryForm_StopProductDetail stopProductDetail : productListForPO){
                       if(stopProductDetail.ItemNo.equalsIgnoreCase(barcodeEntry.itemID)){
                           if(stopProductDetail.QuantityScanned < stopProductDetail.QuantityShipped){
                               productListWithDuplicateScans.add(stopProductDetail);
                           }else{
                               //Duplicate Scan but all boxes have been scanned , so ignore the duplicate
                           }
                       }
                   }
               }
           }
       }

       for(DeliveryForm_StopProductDetail stopProductDetail : productListWithDuplicateScans){

           int duplicateCount = 0;
            for(String key : stopProductDetail.barcodeIdentifierList.keySet()){
//                Number val = stopProductDetail.barcodeIdentifierList.get(key);
                List<Object> val = stopProductDetail.barcodeIdentifierList.get(key);

                int scanCount = val.indexOf(0);
                if(scanCount > 1){
                    duplicateCount = duplicateCount + (scanCount - 1);
                }
            }

            stopProductDetail.DuplicateScans = duplicateCount;
       }


           return productListWithDuplicateScans;
   }

   public ArrayList<DeliveryForm_StopProductDetail> getPendingScanItemListForPO(String POName){

       Log.v("DM", "Enter Function: getPendingScanItemListForPO");


       ArrayList<DeliveryForm_StopProductDetail>  productListForPO = getProductsForPO(POName);

       ArrayList<DeliveryForm_StopProductDetail>  productListWithPendingScans = new ArrayList<DeliveryForm_StopProductDetail>();

       ArrayList<BarcodeScanSessionInfo> barcodeSessionList = (ArrayList<BarcodeScanSessionInfo>) barcodeDictionary.get(POName);

       if (barcodeSessionList == null) {
           Log.e("DM","No Barcode Sessions Found for PO: " + POName);
       }else {
           //Add the Items with alteast 1 scan
           for(DeliveryForm_StopProductDetail stopProductDetail : productListForPO){
               if(stopProductDetail.QuantityScanned > 0.0 && stopProductDetail.QuantityScanned != stopProductDetail.QuantityShipped){
                   productListWithPendingScans.add(stopProductDetail);
               }

           }

           //Add the items with duplicate scans
           for(BarcodeScanSessionInfo barcodeSession : barcodeSessionList){
               ArrayList<BarcodeLogEntry> barcodeList = barcodeSession.barcodeList;

               for(BarcodeLogEntry barcodeEntry : barcodeList) {
                   if(!barcodeEntry.valid){
                       if(barcodeEntry.invalidBarcodeType == InvalidBarcode_DuplicateScan){
                           for(DeliveryForm_StopProductDetail stopProductDetail : productListForPO){
                               if(stopProductDetail.ItemNo.equalsIgnoreCase(barcodeEntry.itemID)){

                                   if(stopProductDetail.QuantityScanned < stopProductDetail.QuantityShipped){
                                       boolean entryAlreadyExists = false;
                                       for(DeliveryForm_StopProductDetail entry : productListWithPendingScans){

                                           if(entry.ItemNo.equalsIgnoreCase(stopProductDetail.ItemNo)){
                                               Log.v("DM", "Same Item already  exists in Pending Scan List: " + stopProductDetail.ItemNo);
                                               entryAlreadyExists = true;
                                               break;
                                           }
                                       }

                                       if(!entryAlreadyExists){
                                           productListWithPendingScans.add(stopProductDetail);
                                       }

                                   }else{
                                       //Duplicate Scan but all boxes have been scanned , so ignore the duplicate
                                   }
                               }

                           }
                       }
                   }
               }
           }
           //Set the number of duplicate scans
           for(DeliveryForm_StopProductDetail stopProductDetail : productListWithPendingScans){

               int duplicateCount = 0;
               for(String key : stopProductDetail.barcodeIdentifierList.keySet()){
                   List<Object> val = stopProductDetail.barcodeIdentifierList.get(key);
//                    int val = stopProductDetail.barcodeIdentifierList.get(key);

                   int scanCount = val.indexOf(0);
                   if(scanCount > 1){
                       duplicateCount = duplicateCount + (scanCount - 1);
                   }
               }

               stopProductDetail.DuplicateScans =  duplicateCount;

           }

       } //else

       Log.v("DM", "Exit Function: getPendingScanItemListForPO");

       return productListWithPendingScans;

   }

   public void deleteBarcodeEntryWithId(int barcodeEntryID, String barcode, String POName , BarcodeScanSessionInfo barcodeSession ){
       Log.v("DM", "Enter Function: deleteBarcodeEntryWithId");

       boolean found = false;
       ArrayList<BarcodeLogEntry> barcodeList = barcodeSession.barcodeList;
       int index = 0;

       for(BarcodeLogEntry barcodeEntry : barcodeList){

           if(barcodeEntry.barcodeEntryId == barcodeEntryID && barcodeEntry.barcode.equalsIgnoreCase(barcode)){
               Log.v("DM","Deleting BarcodeEntry with EntryId: "+ barcodeEntryID +" Barcode: "+ barcodeEntry.barcode);
               found = true;
               break;
           }
           index++;
       }

       if(found){
            barcodeList.remove(index);
            barcodeSession.barcodeList = barcodeList;
       }
       if(!found){
           Log.e("DM", "BarcodeEntry with Id: "+ barcodeEntryID +" not found for PO: " + POName);
       }else{
           ArrayList<BarcodeScanSessionInfo> barcodeSessionList = (ArrayList<BarcodeScanSessionInfo>) barcodeDictionary.get(POName);

           if (barcodeSessionList == null) {
               Log.e("DM","No Barcode Sessions Found for PO: " + POName);
           }else {

               for(BarcodeScanSessionInfo barcodeSessionEntry : barcodeSessionList) {
                   if(barcodeSessionEntry.sessionId == barcodeSession.sessionId){
                       barcodeSessionEntry.barcodeList = barcodeList;
                   }

               }
               saveBarcodeDataToDiskForPO(POName);

           }
       }


       Log.v("DM", "Exit Function: deleteBarcodeEntryWithId");

   }

   public void deleteBarcodeEntryWithId(int barcodeEntryID, String barcode, String POName ){
       Log.v("DM", "Enter Function: deleteBarcodeEntryWithId");

       ArrayList<BarcodeScanSessionInfo> barcodeSessionList = (ArrayList<BarcodeScanSessionInfo>) barcodeDictionary.get(POName);


       if (barcodeSessionList == null) {
           Log.e("DM","No Barcode Sessions Found for PO: " + POName);
       }else {

           boolean found = false;
           for (BarcodeScanSessionInfo barcodeSession : barcodeSessionList) {
               ArrayList<BarcodeLogEntry> barcodeList = barcodeSession.barcodeList;
               int index = 0;
               for (BarcodeLogEntry barcodeEntry : barcodeList) {
                   if (barcodeEntry.barcodeEntryId == barcodeEntryID && barcodeEntry.barcode.equalsIgnoreCase(barcode)) {
                       Log.v("DM", "Deleting BarcodeEntry with EntryId: " + barcodeEntryID + " Barcode: " + barcodeEntry.barcode);
                       found = true;
                       break;
                   }
                   index++;
               }

               if(found){
                   barcodeList.remove(index);
                   barcodeSession.barcodeList = barcodeList;
                   break;
               }
           }

           if(!found){
               Log.e("DM", "BarcodeEntry with Id: "+ barcodeEntryID +" not found for PO: " + POName);
           }else{
               processBarCodeDataForPO(POName);
               saveBarcodeDataToDiskForPO(POName);

           }
       }

   }


   public void addCommentToBarcodeEntryWithId (int barcodeEntryID, String barcode, String POName, String comment ){

       Log.v("DM", "Enter Function: addCommentToBarcodeEntryWithId");

       ArrayList<BarcodeScanSessionInfo> barcodeSessionList = (ArrayList<BarcodeScanSessionInfo>) barcodeDictionary.get(POName);

       if (barcodeSessionList == null) {
           Log.e("DM","No Barcode Sessions Found for PO: " + POName);
       }else {

           boolean found = false;
           for(BarcodeScanSessionInfo barcodeSession : barcodeSessionList) {
               ArrayList<BarcodeLogEntry> barcodeList = barcodeSession.barcodeList;

               for(BarcodeLogEntry barcodeEntry : barcodeList) {
                   if(barcodeEntry.barcodeEntryId == barcodeEntryID && barcodeEntry.barcode.equalsIgnoreCase(barcode)){
                       Log.v("DM","Adding Comment to BarcodeEntry with EntryId: "+ barcodeEntryID +" Barcode: " + barcodeEntry.barcode );
                       barcodeEntry.comments = comment;
                       found = true;
                       break;
                   }
               }

               if(found){
                   break;
               }
           }

           if(!found){
               Log.e("DM", "BarcodeEntry with Id: "+ barcodeEntryID +" not found for PO: " + POName);
           }else{
               processBarCodeDataForPO(POName);
               saveBarcodeDataToDiskForPO(POName);
           }


       }
       Log.v("DM", "Exit Function: addCommentToBarcodeEntryWithId");


   }

//   public BarcodeLogEntry decodeAsSFMCustomBarcode(String barcodeStr){
//
//   }

    public void processBarCodeDataForPO(String poName) {
        Log.v("DM", "Enter Function: processBarCodeDataForPO");

        ArrayList<DeliveryForm_StopProductDetail> productsForSelectedPO = getProductsForPO(poName);
        ArrayList<BarcodeScanSessionInfo> barcodeSessionList = (ArrayList<BarcodeScanSessionInfo>) barcodeDictionary.get(poName);

        if(barcodeSessionList == null){
            Log.e("DM","No Barcode Sessions Found for PO: " + poName);
        }else {

            for(BarcodeScanSessionInfo barcodeSession : barcodeSessionList) {
                ArrayList<BarcodeLogEntry> barcodeList = barcodeSession.barcodeList;

                for(BarcodeLogEntry barcodeEntry : barcodeList) {
                    barcodeEntry.matchFound = false;

                }
            }

            for(DeliveryForm_StopProductDetail stopProductDetail :  productsForSelectedPO){

                double barcodeScanReceivedQuantity = 0.0;

                for(BarcodeScanSessionInfo barcodeSession : barcodeSessionList) {
                    ArrayList<BarcodeLogEntry> barcodeList = barcodeSession.barcodeList;

                    for(BarcodeLogEntry barcodeEntry : barcodeList) {

                        if(barcodeEntry.matchFound == true){
                            continue;
                        }

                        if(barcodeEntry.GTIN14.equalsIgnoreCase(stopProductDetail.BarcodeIdentifier)){
                            // gtin match
                            barcodeEntry.itemID = stopProductDetail.ItemNo;
                            barcodeEntry.itemName = stopProductDetail.ProductDescription;
                            barcodeEntry.PO = stopProductDetail.PO;

                            if(barcodeEntry.gs1BarcodeComponents.lotNumber != null && !barcodeEntry.gs1BarcodeComponents.lotNumber.equalsIgnoreCase("") ){

                                if(stopProductDetail.LotID != null && !stopProductDetail.LotID.equalsIgnoreCase("")){
                                    //Lot ID Matched
                                }else{
                                    barcodeEntry.valid = false;
                                    barcodeEntry.invalidBarcodeType = InvalidBarcode_LotMismatch;
                                    continue;
                                }
                            }
                            //LOT ID is not provided in the order detail....

                        } //TBD Check the valid format YYMMDD for the date

                        if(barcodeEntry.gs1BarcodeComponents.bestBeforeDateStr != null && !barcodeEntry.gs1BarcodeComponents.bestBeforeDateStr.equalsIgnoreCase((""))){
                            if(stopProductDetail.BestBefore != null && !stopProductDetail.BestBefore.equalsIgnoreCase((""))) {

                                if(stopProductDetail.BestBefore.equalsIgnoreCase(barcodeEntry.gs1BarcodeComponents.bestBeforeDateStr)){
                                    //BestBefore Date Matched
                                }else{
                                    //Best Before date doesnot match

                                    barcodeEntry.valid = false;
                                    barcodeEntry.invalidBarcodeType = InvalidBarcode_SellByDateMismatch;
                                    continue;
                                }

                            }
                        }

                        if(barcodeEntry.gs1BarcodeComponents.sellByDateStr != null && !barcodeEntry.gs1BarcodeComponents.sellByDateStr.equalsIgnoreCase((""))){
                            if(stopProductDetail.SellByDate != null && !stopProductDetail.SellByDate.equalsIgnoreCase((""))) {

                                if(stopProductDetail.SellByDate.equalsIgnoreCase(barcodeEntry.gs1BarcodeComponents.sellByDateStr)){
                                    //Sell by Date Matched
                                }else{
                                    //Sell by date doesnot match

                                    barcodeEntry.valid = false;
                                    barcodeEntry.invalidBarcodeType = InvalidBarcode_SellByDateMismatch;
                                    continue;
                                }

                            }
                        }

                        if(barcodeEntry.gs1BarcodeComponents.packagingDateStr != null && !barcodeEntry.gs1BarcodeComponents.packagingDateStr.equalsIgnoreCase((""))){
                            if(stopProductDetail.PackagingDate != null && !stopProductDetail.PackagingDate.equalsIgnoreCase((""))) {

                                if(stopProductDetail.PackagingDate.equalsIgnoreCase(barcodeEntry.gs1BarcodeComponents.packagingDateStr)){
                                    //Packaging  Date Matched
                                }else{
                                    //Packaging Date  doesnot match

                                    barcodeEntry.valid = false;
                                    barcodeEntry.invalidBarcodeType = InvalidBarcode_ProductionDateMismatch;
                                    continue;
                                }

                            }
                        }

                        if(barcodeEntry.gs1BarcodeComponents.productionDateStr != null && !barcodeEntry.gs1BarcodeComponents.productionDateStr.equalsIgnoreCase((""))){
                            if(stopProductDetail.ProductionDate != null && !stopProductDetail.ProductionDate.equalsIgnoreCase((""))) {

                                if(stopProductDetail.ProductionDate.equalsIgnoreCase(barcodeEntry.gs1BarcodeComponents.productionDateStr)){
                                    //Production Date  Matched
                                }else{
                                    //Production Date  doesnot match

                                    barcodeEntry.valid = false;
                                    barcodeEntry.invalidBarcodeType = InvalidBarcode_ProductionDateMismatch;
                                    continue;
                                }

                            }
                        }

                        if(barcodeEntry.gs1BarcodeComponents.expirationDateStr != null && !barcodeEntry.gs1BarcodeComponents.expirationDateStr.equalsIgnoreCase((""))){
                            if(stopProductDetail.ExpirationDate != null && !stopProductDetail.ExpirationDate.equalsIgnoreCase((""))) {

                                if(stopProductDetail.ExpirationDate.equalsIgnoreCase(barcodeEntry.gs1BarcodeComponents.expirationDateStr)){
                                    //Expiration Date  Matched
                                }else{
                                    //Expiration Date  doesnot match

                                    barcodeEntry.valid = false;
                                    barcodeEntry.invalidBarcodeType = InvalidBarcode_ExpirationDateMismatch;
                                    continue;
                                }

                            }
                        }

                        if(barcodeEntry.gs1BarcodeComponents.catchWeight > 0.0 ){

                            if(stopProductDetail.CatchWeight > 0.0){

                                if(stopProductDetail.CatchWeight == barcodeEntry.gs1BarcodeComponents.catchWeight){
                                    //Catch Weight matched
                                } else {
                                    //Catch Weight does not match

                                    barcodeEntry.valid = false;
                                    barcodeEntry.invalidBarcodeType = InvalidBarcode_CatchWeightMismatch;
                                    continue;

                                }
                            }
                        }
                        // Match Found update quantity scanned

//                        barcodeScanReceivedQuantity = barcodeScanReceivedQuantity + barcodeEntry.quantityReceived;
                        barcodeScanReceivedQuantity = barcodeScanReceivedQuantity + barcodeEntry.quantityScanned;
                        barcodeEntry.matchFound = true;

                        if(barcodeScanReceivedQuantity > stopProductDetail.QuantityShipped){
                            barcodeEntry.valid = false;
                            barcodeEntry.invalidBarcodeType = InvalidBarcode_Over;
                        }else{
                            barcodeEntry.valid = true;
                        }


                    } //for
                }

                if(barcodeScanReceivedQuantity > 0.0 ){
                     stopProductDetail.QuantityScanned = (double) barcodeScanReceivedQuantity;

                    if(stopProductDetail.hasManualQuantityReceived == false){
                        stopProductDetail.QuantityReceived = (double) barcodeScanReceivedQuantity;
                    }
                }
            }


        }

        routeDataCacheManager.saveDeliveryFormInfo( this.deliveryInfo , this.deliveryStopId);

        Log.v("DM", "Exit Function: processBarCodeDataForPO");

    }

    public ArrayList<DeliveryForm_StopProductDetail> getProductsForPO(String selectedPO) {
        Log.v("DM", "Enter Function: getProductsForPO");
        ArrayList<DeliveryForm_StopProductDetail> productsForSelectedPO = new ArrayList<DeliveryForm_StopProductDetail>();
        DeliveryForm_StopProductInfo stopProductInfo = this.deliveryInfo.deliveryForm.StopProductInfo;

        for(DeliveryForm_StopProductDetail stopProductDetail : stopProductInfo.StopProductDetail){

            String POName = stopProductDetail.PO;
            if(POName == null){
                POName = "NO PO";
            }

            if(POName.equalsIgnoreCase(selectedPO)){
                if(stopProductDetail.DeliveryType != 1){
                    productsForSelectedPO.add(stopProductDetail);
                }
            }

        }

        Log.v("DM", "Exit Function: getProductsForPO");
        return productsForSelectedPO;
    }

    public void saveImageForBarcodeEntryWithId(int barcodeEntryID, String barcode, String POName, byte[] imageData){
       Log.v("DM", "Enter Function: saveImageForBarcodeEntryWithId");
       ArrayList<BarcodeScanSessionInfo> barcodeSessionList = (ArrayList<BarcodeScanSessionInfo>) barcodeDictionary.get(POName);

        if(barcodeSessionList == null){
           Log.e("DM","No Barcode Sessions Found for PO: " + POName);
       }else {
           boolean found = false;
           for(BarcodeScanSessionInfo barcodeSession : barcodeSessionList){

               ArrayList<BarcodeLogEntry> barcodeList = barcodeSession.barcodeList;

               for(BarcodeLogEntry barcodeEntry : barcodeList){

                   if(barcodeEntry.barcodeEntryId == barcodeEntryID && barcodeEntry.barcode.equalsIgnoreCase(barcode)){
                       Log.v("DM","Saving Image for BarcodeEntry with EntryId: "+ barcodeEntryID +" Barcode: "+ barcodeEntry.barcode);
                       found = true;
                       break;
                   }

               }
               if(found){
                   break;
               }

           }
           if(found){
               this.saveBarcodeDataToDiskForPO(POName);
           }else{
               Log.e("DM", "BarcodeEntry with Id: "+ barcodeEntryID +" not found for PO: " + POName);
           }

       }


       Log.v("DM", "Exit Function: saveImageForBarcodeEntryWithId");


   }



    public void saveBarcodeDataToDiskForPO(String PO){
        Log.v("DM", "Enter Function: saveBarcodeDataToDiskForPO");

        ArrayList<BarcodeScanSessionInfo> barcodeSessionList = (ArrayList<BarcodeScanSessionInfo>) barcodeDictionary.get(PO);

        if(barcodeSessionList == null){
            Log.e("DM","No Barcode Sessions Found for PO: " + PO);
        }else{
//            for(BarcodeScanSessionInfo* barcodeSession in barcodeSessionList)
//            {
//                NSDictionary* sessionDic = [barcodeSession dictionaryWithValuesForKeys];
//                if(sessionDic != nil)
//                {
//            [array addObject:sessionDic];
//                }
//
//            }
//
//            if([array count] > 0)
//            {
//                DDLogInfo(@"Saving Barcode Data to Disk For PO:%@ For Stop:%@", PO, _siteName);
//        [_routeDataCacheManager saveBarcodeData:array ForPO:PO DeliveryStopId:_deliveryStopId DeliveryId:_deliveryId];
        }





                Log.v("DM", "Exit Function: saveBarcodeDataToDiskForPO");

    }

   public void  resetBarCodeData(){
       Log.v("DM", "Enter Function: resetBarCodeData");

       for(String PO : barcodeDictionary.keySet()){
           routeDataCacheManager.deleteBarcodeDataforPO(PO, this.deliveryStopId, this.deliveryId);
       }
       barcodeDictionary.clear();
       barcodeDictionary = null;

       Log.v("DM", "Exit Function: resetBarCodeData");

   }

   public Map<String, ArrayList<BarcodeScanSessionInfo>> getValidBarcodeEntriesForAllPOs(){
       Log.v("DM", "Enter Function: getValidBarcodeEntriesForAllPOs");

       //Map<String, ArrayList<BarcodeScanSessionInfo>> barcodeDictionary = new Map<String, ArrayList<BarcodeScanSessionInfo>>();
       for(String POName : barcodeDictionary.keySet()) {
           ArrayList<BarcodeScanSessionInfo> barcodeSessions = (ArrayList<BarcodeScanSessionInfo>) barcodeDictionary.get(POName);
            ArrayList<BarcodeScanSessionInfo> barcodeSessionArray = new ArrayList<BarcodeScanSessionInfo>();
           for (BarcodeScanSessionInfo  barcodeSession : barcodeSessions) {

               BarcodeScanSessionInfo barcodeSessionEntry = new BarcodeScanSessionInfo();
               barcodeSessionEntry.sessionId = barcodeSession.sessionId;
               barcodeSessionEntry.sessionStartTime = barcodeSession.sessionStartTime;
               barcodeSessionEntry.sessionEndTime = barcodeSession.sessionEndTime;
               barcodeSessionEntry.PO = barcodeSession.PO;

               ArrayList<BarcodeLogEntry> validBarcodeList = new ArrayList<BarcodeLogEntry>();
               for(BarcodeLogEntry barcodeEntry : barcodeSession.barcodeList){

                   if(barcodeEntry.matchFound){
                       validBarcodeList.add(barcodeEntry);
                   }
               }

               barcodeSessionEntry.barcodeList = validBarcodeList;
               barcodeSessionArray.add(barcodeSessionEntry);



           }
           barcodeDictionary.put(POName, barcodeSessions);
       }

       Log.v("DM", "Exit Function: getValidBarcodeEntriesForAllPOs");
       return  barcodeDictionary;

    }


    public ArrayList<BarcodeLogEntry> getInvalidBarCodeListForAllPOs(){
        Log.v("DM", "Enter Function: getInvalidBarCodeListForAllPOs");
       ArrayList<BarcodeLogEntry>invalidBarcodeList = new ArrayList<BarcodeLogEntry>();

       for(String POName : barcodeDictionary.keySet()) {

           ArrayList<BarcodeScanSessionInfo> barcodeSessions = (ArrayList<BarcodeScanSessionInfo>) barcodeDictionary.get(POName);

           for (BarcodeScanSessionInfo barcodeSession : barcodeSessions) {

               for (BarcodeLogEntry barcodeEntry : barcodeSession.barcodeList) {
                   if (!barcodeEntry.matchFound) {
                       if (barcodeEntry.itemID == null) {
                           boolean found = false;

                           for (RouteShipmentProduct routeShipmentProduct : this.routeshipmentProductList) {
                               if (routeShipmentProduct.customerProductAltID.equalsIgnoreCase(barcodeEntry.GTIN14)) {
                                   barcodeEntry.invalidBarcodeType = InvalidBarcode_NotInPO;
                                   barcodeEntry.itemID = routeShipmentProduct.customerProductID;
                                   found = true;
                               }

                               if (!found) {
                                   barcodeEntry.invalidBarcodeType = InvalidBarcode_UnknownItem;
                               }
                           }

                           boolean entryAlreadyExists = false;
                           entryAlreadyExists = false;

                           for (BarcodeLogEntry entry : invalidBarcodeList) {

                               if (entry.barcode.equalsIgnoreCase(barcodeEntry.barcode)) {
                                   Log.v("DM", "Same Barcode Already  exists in Invalid Barcode List:  " + barcodeEntry.barcode);

                                   double quantityReceived = entry.quantityReceived + barcodeEntry.quantityReceived;
                                   entry.quantityReceived = quantityReceived;
                                   entryAlreadyExists = true;
                                   break;
                               }
                           }
                           if (!entryAlreadyExists) {
                               invalidBarcodeList.add(barcodeEntry);
                           }

                       }
                   }
               }


           }
       }

        Log.v("DM", "Exit Function: getInvalidBarCodeListForAllPOs");

           return invalidBarcodeList;

    }

   public ArrayList<DeliveryForm_StopProductDetail> getProductsWithBarcodeDiscrepancyForReport(){
       Log.v("DM", "Enter Function: getProductsWithBarcodeDiscrepancyForReport");

       ArrayList<DeliveryForm_StopProductDetail> productsWithBarcodeDiscrepancy = new ArrayList<DeliveryForm_StopProductDetail>();
       DeliveryForm_StopProductInfo stopProductInfo = this.deliveryInfo.deliveryForm.StopProductInfo;
       for(DeliveryForm_StopProductDetail stopProductDetail : stopProductInfo.StopProductDetail){
            if(stopProductDetail.QuantityScanned < stopProductDetail.QuantityReceived){
                productsWithBarcodeDiscrepancy.add(stopProductDetail);
            }
       }

       Log.v("DM", "Exit Function: getProductsWithBarcodeDiscrepancyForReport");
       return productsWithBarcodeDiscrepancy;

   }

   public void addBarcodeScanDiscrepancyDetailForPO(String PO, String itemno, String itemName, String reason, String barcodeIdentifier, byte[] imageData){
       Log.v("DM", "Enter Function: addBarcodeScanDiscrepancyDetailForPO");

       BarcodeDiscrepancyDetail barcodeDiscrepancyDetail = new BarcodeDiscrepancyDetail();
       barcodeDiscrepancyDetail.PO= PO;
       barcodeDiscrepancyDetail.itemNo= itemno;
       barcodeDiscrepancyDetail.itemName = itemName;
       barcodeDiscrepancyDetail.reasonCode = reason;
       barcodeDiscrepancyDetail.barcodeIdentifier = barcodeIdentifier;
       barcodeDiscrepancyDetail.imageData = imageData;

       if(this.deliveryInfo.barcodeDiscrepancyList == null){
           ArrayList<BarcodeDiscrepancyDetail> list = new ArrayList<BarcodeDiscrepancyDetail>();
           deliveryInfo.barcodeDiscrepancyList = list;
       }

       this.deliveryInfo.barcodeDiscrepancyList.add(barcodeDiscrepancyDetail);

       Log.d("DM", "Saving BarcodeScanDiscrepancy To Disk: "+ barcodeDiscrepancyDetail);

       routeDataCacheManager.saveDeliveryFormInfo(this.deliveryInfo, this.deliveryStopId);

       Log.v("DM", "Exit Function: addBarcodeScanDiscrepancyDetailForPO");

   }

   public void removeScanDiscrepancyDetailsForBarcodeIdentifier(String barcodeIdentifier){
       Log.v("DM", "Enter Function: removeScanDiscrepancyDetailsForBarcodeIdentifier");

       ArrayList<BarcodeDiscrepancyDetail> list = new ArrayList<BarcodeDiscrepancyDetail>();

       for(BarcodeDiscrepancyDetail detail : deliveryInfo.barcodeDiscrepancyList){
           if(detail.barcodeIdentifier.equalsIgnoreCase(barcodeIdentifier)){
               //remove from list
           }else{
               list.add(detail);
           }
       }
       this.deliveryInfo.barcodeDiscrepancyList = list;

       routeDataCacheManager.saveDeliveryFormInfo( this.deliveryInfo , this.deliveryStopId);

       Log.v("DM", "Exit Function: removeScanDiscrepancyDetailsForBarcodeIdentifier");

   }

   public ArrayList<BarcodeDiscrepancyDetail>  getBarcodeScanDiscrepancyList(){
        return  this.deliveryInfo.barcodeDiscrepancyList;
    }

   public void savePaymentCollectionType(DeliveryForm_PaymentCollectionInfo.PaymentMethodEnum paymentMethod, String transactionId, double paymentAmount, double invoiceAmount, String comments, byte[] imageData, byte[] signatureData, String spokewith, String creditCardType, String last4Digits  ){
       Log.v("DM", "Enter Function: savePaymentCollectionType");

       this.deliveryInfo.paymentCollectionTaskCompleted = true;

       if(deliveryInfo.deliveryForm.PaymentCollectionInfo == null){
           DeliveryForm_PaymentCollectionInfo paymentCollectionInfo = new DeliveryForm_PaymentCollectionInfo();

           this.deliveryInfo.deliveryForm.PaymentCollectionInfo = paymentCollectionInfo;

       }
       this.deliveryInfo.deliveryForm.PaymentCollectionInfo.paymentMethod = paymentMethod;

       if(paymentMethod == DeliveryForm_PaymentCollectionInfo.PaymentMethodEnum.PaymentMethod_NoPaymentAvailable_Deliver || paymentMethod == DeliveryForm_PaymentCollectionInfo.PaymentMethodEnum.PaymentMethod_NoPaymentAvailable_DoNotDeliver){
           if(paymentMethod == DeliveryForm_PaymentCollectionInfo.PaymentMethodEnum.PaymentMethod_NoPaymentAvailable_DoNotDeliver) {

               Log.v("DM", "Setting TMSDeliveryStatus to NoPaymentAvailable_DoNotDeliver");
               deliveryInfo.tmsDeliveryStatus = TMSDeliveryStatus_NoPaymentAvailable_DonotDeliver;
           }
           this.deliveryInfo.deliveryForm.PaymentCollectionInfo.totalInvoiceAmount = roundNumberToTwoDecimal(invoiceAmount);
           this.deliveryInfo.deliveryForm.PaymentCollectionInfo.spokeWith = spokewith;

       }else{
           if(paymentMethod == DeliveryForm_PaymentCollectionInfo.PaymentMethodEnum.PaymentMethod_CreditCard){
               deliveryInfo.deliveryForm.PaymentCollectionInfo.creditCardType = creditCardType;
               deliveryInfo.deliveryForm.PaymentCollectionInfo.last4Digits = last4Digits;

           }
           deliveryInfo.deliveryForm.PaymentCollectionInfo.paymentAmount = paymentAmount;
           deliveryInfo.deliveryForm.PaymentCollectionInfo.transactionId = transactionId;
           deliveryInfo.deliveryForm.PaymentCollectionInfo.totalInvoiceAmount = roundNumberToTwoDecimal(invoiceAmount);
           deliveryInfo.deliveryForm.PaymentCollectionInfo.imageData = imageData;
           deliveryInfo.deliveryForm.PaymentCollectionInfo.signatureData = signatureData;
           deliveryInfo.deliveryForm.PaymentCollectionInfo.driverComments = comments;

       }
       routeDataCacheManager.saveDeliveryFormInfo(this.deliveryInfo, this.deliveryStopId);

       Log.v("DM", "Exit Function: savePaymentCollectionType");


   }

   public  void sendPODEmailWithPDFFilePath(String pdfFilePath, String  fileName , String text , OnCompleteListeners.completionCallbackListener listener ){
       Log.v("DM", "Enter Function: sendPODEmailWithPDFFilePath");
        Log.v("DM", "TMS is trying to send POD Report for Stop: " + this.siteName);

       final OnCompleteListeners.completionCallbackListener completionCallback = listener;
       String completeFileName = String.format("%@.pdf", fileName);

       //String relativeFilePath

       Log.v("DM", "Exit Function: sendPODEmailWithPDFFilePath");


   }


   public double getInvoiceTotalWithInvoicePriceCalcSetting(int setting){
       double _totalPrice = 0.0;

       DeliveryForm_StopProductInfo stopProductInfo = deliveryInfo.deliveryForm.StopProductInfo;

       for(DeliveryForm_StopProductDetail stopProductDetail : stopProductInfo.StopProductDetail){
           double totalPrice = 0.0;

           switch (setting) {
               case 0:
               {
                   totalPrice = roundNumberToTwoDecimal(stopProductDetail.UnitPrice) * roundNumberToTwoDecimal(stopProductDetail.QuantityReceived);
               }
               break;
               case 1:
               {
                   totalPrice =  roundNumberToTwoDecimal(stopProductDetail.UnitPrice) * roundNumberToTwoDecimal(stopProductDetail.QuantityShipped);
               }
               break;
               case 2:
               {
                   if(stopProductDetail.CatchWeight > 0.0)
                   {
                       totalPrice = stopProductDetail.UnitPrice * stopProductDetail.CatchWeight;
                   }
                else
                   {
                       totalPrice = stopProductDetail.UnitPrice * stopProductDetail.TotalWeight;
                   }

               }
               break;
               default:
                   break;
           }

           _totalPrice = _totalPrice + roundNumberToTwoDecimal(totalPrice);


       }

       return _totalPrice;
   }


   public double roundNumberToTwoDecimal(double num){
       return  round(100 * num) /100 ;
   }


}
