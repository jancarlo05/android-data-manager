package com.procuro.apimmdatamanagerlib.PODData;

import android.util.Log;

import com.procuro.apimmdatamanagerlib.OnCompleteListeners;
import com.procuro.apimmdatamanagerlib.PimmBaseObject;
import com.procuro.apimmdatamanagerlib.PropertyValue;
import com.procuro.apimmdatamanagerlib.SiteSettings;
import com.procuro.apimmdatamanagerlib.User;
import com.procuro.apimmdatamanagerlib.aPimmDataManager;
import com.procuro.apimmdatamanagerlib.iPimmLogManager;

import java.util.ArrayList;

/**
 * Created by jophenmarieweeks on 06/09/2017.
 */

public class PODRecoveryService extends PimmBaseObject {

    public class PODRecoveryStopDetail extends PimmBaseObject{
        public String deliveryId;
        public String deliveryStopId;
    }

    private static final PODRecoveryService ourInstance = new PODRecoveryService();

    public aPimmDataManager dataManager;
    public DeliveryDataCacheManager cacheManager;
    public iPimmLogManager logManager;
    public String recoveryRole;

    public String spid;
    public String appId;
    public String vendorId;

    public static PODRecoveryService getInstance()
    {
        return ourInstance;
    }
    private PODRecoveryService()
    {

    }

    public void initializeWithAppId(String appId, String spid, String vendorId){

        this.appId = appId;
        this.spid = spid;
        this.vendorId = vendorId;
        this.dataManager = aPimmDataManager.getInstance();

        this.dataManager.initializeWithAppName("PODRecovery", "1.0", true, spid, false);

        //this.logManager = iPimmLogManager.getiPimmLogManager();
//        BOOL isSupportMode = [[[NSUserDefaults standardUserDefaults] valueForKey:@"logLevel"] boolValue];
//
//        if(isSupportMode) {
//            DDLogInfo(@"Support mode for Logging is set to true");
//        [_logManager initializeFileLoggersWithVendorId:vendorId ApplicationName:@"PODRecovery" ApplicationLogLevel:ApplicationLogLevel_Debug];
//        } else {
//            DDLogInfo(@"Support mode for Logging is set to false");
//
//        [_logManager initializeFileLoggersWithVendorId:vendorId ApplicationName:@"PODRecovery" ApplicationLogLevel:ApplicationLogLevel_Default];
//        }
//
//        _cacheManager = [DeliveryDataCacheManager getInstance];


    }

    public void setPODDataDirectory(String directory){
        cacheManager.setPodDataDirectory(directory);
    }
    public void loginWithUserName(String username, String password ,  OnCompleteListeners.OnLoginPODRecoveryCompleteListener listener ){

        Log.v("DM", "Enter Function: loginWithUserName");
        Log.v("DM", "Login Called for POD Recovery");

        deleteOldPODData();


        dataManager.loginWithUsernameAndPassword(username, password, new OnCompleteListeners.OnLoginCompleteListener() {
            @Override
            public void onLoginComplete(final User user, Error error) {
                if(error != null){

                }else{
                    Log.i("DM","User is Authenticated to Login, Now checking Authorization for PODRecovery");

                    final String siteId = user.defaultSiteID;
                    dataManager.getSiteSettingsForSiteId(siteId, new OnCompleteListeners.getSiteSettingsForSiteIdListener() {
                        @Override
                        public void getSiteSettingsForSiteId(SiteSettings siteSettings, Error error) {
                            if(error != null){

                            }else{
                                boolean found = false;
                                for(PropertyValue setting : siteSettings.settings)
                                {
                                    if(setting.property.equalsIgnoreCase("Std:TMS:POD:RecoveryRole"))
                                    {
                                        recoveryRole = setting.value;
                                        Log.i("DM","RecoveryRole Setting for Site is: " + recoveryRole);
                                        found = true;
                                    }
                                    if(!found)
                                    {
                                        Log.e("DM","Recovery Role Setting not found for Site: " +  siteId);


//                                        NSError* error = [NSError getErrorWithDescription:@"Recovery Role Setting not configured for the Site" ErrorCode:APPLICATION_ERROR_CODE_SYSTEM_CONFIG_ERROR];
//
//                                        block(error);
                                        return ;
                                    }
                                    boolean authorized = false;
                                    ///check if user is assigned the role for recovery
                                    for(String roleString : user.roles)
                                    {
                                        if(roleString.equalsIgnoreCase(recoveryRole))
                                        {
                                            Log.i("DM","User is authorized for using PODRecovery");
                                            authorized = true;
//                                            block(nil);
                                            return;
                                        }
                                    }

                                    if(!authorized)
                                    {
                                        Log.i("DM","User is not authorized for using PODRecovery");

//                                        NSError* error = [NSError getErrorWithDescription:@"User does not have Recovery Role Assigned" ErrorCode:APPLICATION_ERROR_CODE_AUTHORIZATION_ERROR];

//                                        block(error);
                                        return ;

                                    }

                                }

                            }
                        }
                    });
                }
            }
        });


        Log.v("DM", "Exit Function: loginWithUserName");


    }


    public ArrayList<PODRecoveryInfo> getRouteListForPODRecovery ()
    {
        Log.v("DM", "getRouteListForPODRecovery start");
        ArrayList<PODRecoveryInfo> routeList =   cacheManager.getListOfRoutesForPODRecovery();
        return routeList;

    }

 public void retransmitDataForDeliveryId(String deliveryId, String stopId , OnCompleteListeners.completionCallbackListener listener) {
        Log.v("DM","Enter Function: retransmitDataForDeliveryId " );

        Log.i("DM","Resending DeliveryData to Server for deliveryId: "+ deliveryId +" StopId "+ stopId);

//        StopPODData podData = [[StopPODData alloc] initWithAppId:_appId DeliveryStopId:stopId DeliveryId:deliveryId];

     StopPODData podData = new StopPODData();
     podData.initWithAppId(appId,stopId, deliveryId);

    podData.resendDeliveryDataToServerWithCompletionCallback(new OnCompleteListeners.completionCallbackListener() {
        @Override
        public void completionCallback(Error error) {
            if(error != null)
            {
//                block(error);
            }
            else {
//                block(nil);
            }

        }

    });



    }

    public void deleteOldPODData() {
    }


}
