package com.procuro.apimmdatamanagerlib.PODData;

import android.util.Log;

import com.procuro.apimmdatamanagerlib.PimmBaseObject;

import java.net.URL;
import java.util.ArrayList;

/**
 * Created by jophenmarieweeks on 17/08/2017.
 */

public class DeliveryDataCacheManager extends PimmBaseObject {
    private static DeliveryDataCacheManager context;

    private  DeliveryDataCacheManager() {

    }
  /*
    @class StopPODData;

@class  BarcodeScanSessionInfo;

    @interface DeliveryDataCacheManager : NSObject

*/
    public String podDataDirectory;
//    @property (nonatomic,strong) NSFileManager* fileManager;
//
//    @property (nonatomic,strong) dispatch_queue_t serialQueue;

  public  static DeliveryDataCacheManager getInstance(){
        return (context == null) ? context = new DeliveryDataCacheManager() : context;
  }




//-(void) initializeWithDeliveryId:(NSString* )deliveryId;

 public void saveDeliveryFormInfo (StopPODData.DeliveryInfo form , String deliveryStopId){
     new Thread(new Runnable() {
         @Override
         public void run() {
             // do something here...
         }
     }).start();
 }
/*
-(void) saveImageWithImageInfo:(ImageInfo*) imageInfo
    DeliveryId:(NSString*) deliveryId
    DeliveryStopId:(NSString*) deliveryStopId;

-(DeliveryInfo*) getDeliveryFormInfoForDeliveryId:(NSString*) deliveryId
    DeliveryStopId:(NSString*) deliveryStopId;


-(void) getImageDataforImageName:(NSString*) imageName
    DeliveryId:(NSString*) deliveryId
    DeliveryStopId:(NSString*) deliveryStopId
    CompletionCallback:(void(^)(ImageInfo* imageInfo)) callback;

-(ImageInfo*) getImageDataforImageName:(NSString*) imageName
    DeliveryId:(NSString*) deliveryId
    DeliveryStopId:(NSString*) deliveryStopId;

-(void) getCameraImagesForDeliveryId:(NSString*) deliveryId
    DeliveryStopId:(NSString*) deliveryStopId
    CompletionCallback:(void(^)(NSArray* imageList)) callback;



-(NSString*) getFilePathForImageWithName:(NSString*) imageName
    DeliveryId:(NSString*) deliveryId
    DeliveryStopId:(NSString*) deliveryStopId;


//-(ImageInfo*) getPODSignatureInfoForSiteId:(NSString*) siteId;


//-(StopPODMetaData* ) getPODMetaDataforSiteId:(NSString*) siteId;

-(void) updatePODRecoveryInfo:(PODRecoveryInfo*) info
    ForDeliveryId:(NSString*) deliveryId;

-(PODRecoveryInfo*) getPODRecoveryInfoForDeliveryId:(NSString*) deliveryId;


//Returns Array of PODRecoveryInfo objects
-(NSArray*) getListOfRoutesForPODRecovery;

-(void) deleteImageWithImageInfo:(ImageInfo*) imageInfo
    DeliveryId:(NSString*) deliveryId
    DeliveryStopId:(NSString*) deliveryStopId;

-(void) addCommentsToImageWithImageInfo:(ImageInfo*) updatedImageInfo
    DeliveryId:(NSString*) deliveryId
    DeliveryStopId:(NSString*) deliveryStopId;

-(void) deletePODDataFilesOlderThanDate:(NSDate*) date;

-(void) deletePODDataFilesOlderThanDate:(NSDate*) date DirectoryPath:(NSString*) directoryPath;

-(void) saveBarcodeData:(NSArray<NSDictionary*>*)barcodeSessionList
    ForPO:(NSString*) PO
    DeliveryStopId:(NSString*) deliveryStopId
    DeliveryId:(NSString*) deliveryId;

-(NSArray<BarcodeScanSessionInfo*>*) getBarcodeDataForPO:(NSString*) PO
    DeliveryStopId:(NSString*) deliveryStopId
    DeliveryId:(NSString*) deliveryId;

-(void) deleteBarcodeDataforPO:(NSString*) PO
    DeliveryStopId:(NSString*) deliveryStopId
    DeliveryId:(NSString*) deliveryId;


-(void) saveRouteProductReturnAddOnListForDeliveryId:(NSString*) deliveryId
    RouteReturnedProductsInfo:(RouteReturnedProductsInfo*) returnedProductsInfo;


-(RouteReturnedProductsInfo*)  getRouteProductReturnsInfoForDeliveryId:(NSString*) deliveryId;

*/

    public StopPODData.DeliveryInfo getDeliveryFormInfoForDeliveryId(String deliveryId, String deliveryStopId) {
        Log.v("DM", "Enter Function:getDeliveryFormInfoForDeliveryId");
        // NSURL* deliveryFormLocalURL = [self deliveryFormLocalURLForDeliveryId:deliveryId DeliveryStopId:deliveryStopId];
        URL deliveryFormLocalURL = deliveryFormLocalURLForDeliveryId(deliveryId,deliveryStopId);
////        NSData* fileData = [self readFileWithURL:deliveryFormLocalURL];
////
////        if(fileData)
////        {
////            NSDictionary* deliveryInfoDic = [NSDictionary dictionaryFromJSONData:fileData];
////
////            DeliveryInfo* deliveryInfo = [[DeliveryInfo alloc] init];
////        [deliveryInfo readFromJSONDictionary:deliveryInfoDic];
////            DDLogDebug(@"Successfully Read DeliveryForm from File:%@", [deliveryFormLocalURL path]);
////
////            DDLogVerbose(@"Delivery Form read from Local Cache:\n%@", [deliveryInfoDic toJSONString]);
////
////            return deliveryInfo;
////
////        }
//        else
//        {
//            Log.e("DM","Error reading DeliveryForm from File ", + deliveryFormLocalURL.path);
//            return null;
//        }
        Log.v("DM", "Exit Function : getDeliveryFormInfoForDeliveryId ");
        return  null;


    }

    public URL deliveryFormLocalURLForDeliveryId(String deliveryId, String deliveryStopId) {
        Log.v("DM", "Enter Function: deliveryFormLocalURLForDeliveryId");
        String filename = String.format("DeliveryForm.json");
        URL formURL = applicationStopDataDirectoryForDeliveryId(deliveryId, deliveryStopId);
        StringBuilder urlString = new StringBuilder();
        urlString.append(formURL);
//        return  urlString;
        Log.v("DM", "Exit Function : deliveryFormLocalURLForDeliveryId ");
        return formURL;
    }

    public URL applicationStopDataDirectoryForDeliveryId(String deliveryId, String deliveryStopId) {
        Log.v("DM", "Enter Function: applicationStopDataDirectoryForDeliveryId");

//        NSURL *siteDirectory = [[self applicationRouteDataDirectoryForDeliveryId:deliveryId] URLByAppendingPathComponent:deliveryStopId];
//
//
//        if (![_fileManager fileExistsAtPath:[siteDirectory path]])
//        {
//            NSError *error = nil;
//            if ([_fileManager createDirectoryAtURL:siteDirectory
//            withIntermediateDirectories:YES
//            attributes:nil
//            error:&error])
//            {
//
//                DDLogDebug(@"Successfully created Route Data directory at path:%@",[siteDirectory path]);
//            }
//        else
//            {
//
//                DDLogError(@"FAILED to create Route Data directory: %@", error);
//            }
//        }
//    else
//        {
//            DDLogVerbose(@"Route Data Directory already exists at Path:%@",[siteDirectory path]);
//        }
//        return siteDirectory;

        Log.v("DM", "Exit Function : applicationStopDataDirectoryForDeliveryId ");
        return null;
    }


    public ImageInfo getImageDataforImageName(String imageName, String deliveryId, String deliveryStopId) {
        return  null;
    }

    public ArrayList<BarcodeScanSessionInfo> getBarcodeDataForPO(String po, String deliveryStopId, String deliveryId) {

        return null;
    }

    public String getFilePathForImageWithName(String imageName, String deliveryId, String deliveryStopId) {

        Log.v("DM", "Enter Function : getFilePathForImageWithName ");
        String fileName = String.format("%@_Image.json", imageName);
        URL imageURL = applicationStopDataDirectoryForDeliveryId(deliveryId, deliveryStopId);



        Log.v("DM", "Exit Function : getFilePathForImageWithName ");

        return null;
    }

    public void addCommentsToImageWithImageInfo(ImageInfo imageInfo, String deliveryId, String deliveryStopId) {

    }

    public void deleteImageWithImageInfo(ImageInfo imageInfo, String deliveryId, String deliveryStopId) {
    }

    public void deleteBarcodeDataforPO(String po, String deliveryStopId, String deliveryId) {

    }

    public void saveImageWithImageInfo(ImageInfo imageInfo, String deliveryId, String deliveryStopId) {
    }

    public void setPodDataDirectory(String directory) {

    }

    public ArrayList getListOfRoutesForPODRecovery() {
        ArrayList<PODRecoveryInfo> routeList = new ArrayList<PODRecoveryInfo>();

        return routeList;
    }
}
