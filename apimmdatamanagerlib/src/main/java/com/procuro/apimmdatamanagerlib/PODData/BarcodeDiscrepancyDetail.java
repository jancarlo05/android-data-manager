package com.procuro.apimmdatamanagerlib.PODData;

import com.procuro.apimmdatamanagerlib.Base64Converter;
import com.procuro.apimmdatamanagerlib.DMUtils;
import com.procuro.apimmdatamanagerlib.PimmBaseObject;
import com.procuro.apimmdatamanagerlib.aPimmDataManager;

import java.util.HashMap;

/**
 * Created by jophenmarieweeks on 17/08/2017.
 */

public class BarcodeDiscrepancyDetail extends PimmBaseObject {

    public String PO;
    public String itemNo;
    public String itemName;
    public String reasonCode;
    public String barcodeIdentifier;
    public byte[] imageData;

    public HashMap<String,Object> dictionaryWithValuesForKeys() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();

        dictionary.put("PO",this.PO);
        dictionary.put("itemNo",this.itemNo);
        dictionary.put("itemName",this.itemName);
        dictionary.put("reasonCode",this.reasonCode);
        dictionary.put("barcodeIdentifier",this.barcodeIdentifier);

        if(this.imageData != null){
            dictionary.put("imageData", Base64Converter.ByteToUrlStringBase64(this.imageData));

        }else{
            dictionary.put("imageData", null);
        }


        return dictionary;
    }





    }
