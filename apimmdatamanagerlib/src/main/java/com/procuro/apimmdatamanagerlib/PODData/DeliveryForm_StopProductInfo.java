package com.procuro.apimmdatamanagerlib.PODData;

import android.util.Log;

import com.procuro.apimmdatamanagerlib.PimmBaseObject;
import com.procuro.apimmdatamanagerlib.RouteShipmentProduct;
import com.procuro.apimmdatamanagerlib.RouteShipmentUnit;
import com.procuro.apimmdatamanagerlib.SdrStop;
import com.procuro.apimmdatamanagerlib.SdrStopProduct;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class DeliveryForm_StopProductInfo extends PimmBaseObject {

    public ArrayList<DeliveryForm_StopProductInfo_Groups> Groups;
    public ArrayList<DeliveryForm_StopProductDetail> StopProductDetail;

    @Override
    public void setValueForKey(Object value, String key) {

        if (key.equalsIgnoreCase("Groups")) {
            Groups = new ArrayList<>();
            if (value!=null){
                JSONArray arrStops = (JSONArray) value;
                for (int i = 0; i < arrStops.length(); i++) {
                    try {
                        DeliveryForm_StopProductInfo_Groups deliveryForm_stopProductInfo_groups = new DeliveryForm_StopProductInfo_Groups();
                        deliveryForm_stopProductInfo_groups.readFromJSONObject(arrStops.getJSONObject(i));
                        this.Groups.add(deliveryForm_stopProductInfo_groups);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        else if (key.equalsIgnoreCase("StopProductDetail")) {
            this.StopProductDetail = new ArrayList<>();
            if (value!=null){
                JSONArray arrStops = (JSONArray) value;
                for (int i = 0; i < arrStops.length(); i++) {
                    try {
                        DeliveryForm_StopProductDetail deliveryForm_stopProductDetail = new DeliveryForm_StopProductDetail();
                        deliveryForm_stopProductDetail.readFromJSONObject(arrStops.getJSONObject(i));
                        this.StopProductDetail.add(deliveryForm_stopProductDetail);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        } else {
            super.setValueForKey(value, key);
        }
    }


}
