package com.procuro.apimmdatamanagerlib.PODData;

import com.procuro.apimmdatamanagerlib.PimmBaseObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by jophenmarieweeks on 06/09/2017.
 */

public class SquareTransaction extends PimmBaseObject {

    public class SquareCard extends PimmBaseObject {

        //  NS_ASSUME_NONNULL_BEGIN
        public String card_id;
        public String card_brand;
        public String last_4;
        public int exp_month;
        public int exp_year;
        public String cardholder_name;
    }


    public class SquareTenderCardDetails extends PimmBaseObject {

        public String status;
        public SquareCard card;
        public String entry_method;

        @Override
        public void setValueForKey(Object value, String key) {
                 if(key.equalsIgnoreCase("card")){

                if(!value.equals(null)) {
                    SquareCard squareCard = new SquareCard();
                    JSONObject jsonObject = (JSONObject) value;
                    squareCard.readFromJSONObject(jsonObject);
                    this.card = squareCard;
                }

            } else {
                super.setValueForKey(value, key);
            }
        }

    }

    public class SquareMoney extends PimmBaseObject {

        public int amount;
        public String currency;

    }


    public class SquareTender extends PimmBaseObject {

        public String tender_id;
        public String location_id;
        public String transaction_id;
        public String created_at;
        public String note;
        public SquareMoney amount_money;
        public SquareMoney processing_fee_money;
        public String customer_id;
        public String type;
        public SquareTenderCardDetails card_details;


        @Override
        public void setValueForKey(Object value, String key) {
            if(key.equalsIgnoreCase("amount_money")) {

                if (!value.equals(null)) {
                    SquareMoney squareMoney = new SquareMoney();
                    JSONObject jsonObject = (JSONObject) value;
                    squareMoney.readFromJSONObject(jsonObject);
                    this.amount_money = squareMoney;
                }
            }else if(key.equalsIgnoreCase("processing_fee_money")){

                if(!value.equals(null)) {
                    SquareMoney squareMoney = new SquareMoney();
                    JSONObject jsonObject = (JSONObject) value;
                    squareMoney.readFromJSONObject(jsonObject);
                    this.processing_fee_money = squareMoney;
                }

            }else if(key.equalsIgnoreCase("card_details")){

                if(!value.equals(null)) {
                    SquareTenderCardDetails squareTenderCardDetails = new SquareTenderCardDetails();
                    JSONObject jsonObject = (JSONObject) value;
                    squareTenderCardDetails.readFromJSONObject(jsonObject);
                    this.card_details = squareTenderCardDetails;
                }

            } else {
                super.setValueForKey(value, key);
            }
        }
    }

        public String transactionId;
        public String locationId;
        public String created_at;
        public String reference_id;
        public String product;
        public String client_id;

        public ArrayList<SquareTender> tenders;


    @Override
    public void setValueForKey(Object value, String key) {
        if (key.equalsIgnoreCase("tenders")) {
            if (this.tenders == null) {
                this.tenders = new ArrayList<SquareTender>();
            }

            if (!value.equals(null)) {
                JSONArray arrStops = (JSONArray) value;

                if (arrStops != null) {
                    for (int i = 0; i < arrStops.length(); i++) {
                        try {
                            SquareTender squareTender = new SquareTender();
                            squareTender.readFromJSONObject(arrStops.getJSONObject(i));

                            this.tenders.add(squareTender);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } else {
                super.setValueForKey(value, key);
            }
        }

    }



}
