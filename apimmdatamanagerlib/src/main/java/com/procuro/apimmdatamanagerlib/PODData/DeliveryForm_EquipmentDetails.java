package com.procuro.apimmdatamanagerlib.PODData;

import android.util.Log;

import com.procuro.apimmdatamanagerlib.PimmBaseObject;
import com.procuro.apimmdatamanagerlib.RouteShipmentEquipmentDTO;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class DeliveryForm_EquipmentDetails extends PimmBaseObject {

    public int QuantityDeliveredScheduled; //int
    public int QuantityPickUpScheduled; //int
    public int QuantityDeliveredActual; //int
    public int QuantityPickUpActual; //int
    public String CustomerEquipmentID;
    public String EquipmentDescription;

    @Override
    public void setValueForKey(Object value, String key) {
        super.setValueForKey(value, key);
    }

    public HashMap<String,Object> dictionaryWithValuesForKeys() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();

        dictionary.put("QuantityDeliveredScheduled", this.QuantityDeliveredScheduled);
        dictionary.put("QuantityPickUpScheduled", this.QuantityPickUpScheduled);
        dictionary.put("EquipmentDescription", this.EquipmentDescription);
        dictionary.put("QuantityDeliveredActual", this.QuantityDeliveredActual);
        dictionary.put("QuantityPickUpActual", this.QuantityPickUpActual);
        dictionary.put("CustomerEquipmentID", this.CustomerEquipmentID);

        return dictionary;

    }
}
