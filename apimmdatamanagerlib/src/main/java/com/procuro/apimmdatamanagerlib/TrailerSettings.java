package com.procuro.apimmdatamanagerlib;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jophenmarieweeks on 10/07/2017.
 */

public class TrailerSettings extends PimmBaseObject {
    public ArrayList<PropertyValue> settings;
    public ArrayList<String>  displaySensors;
    public String displaySensorMatch;
    public ArrayList<String>  requiredSensors;
    public ArrayList<String>  freezerProductSensors;
    public ArrayList<String>  freezerAmbientSensors;
    public ArrayList<String>  coolerProductSensors;
    public ArrayList<String>  coolerAmbientSensors;
    public ArrayList<String>  drySensors;

//    @Override
//    public void setValueForKey(Object value, String key)
//    {
//        if(key.equalsIgnoreCase("settings"))
//        {
//            if(this.settings == null)
//            {
//                this.settings = new ArrayList<String>();
//            }
//
//            List<String> settings = (List<String>) value;
//            for(String setting : settings)
//            {
//                this.settings.add(setting);
//            }
//        }
//        else if(key.equalsIgnoreCase("displaySensors"))
//        {
//            if(this.displaySensors == null)
//            {
//                this.displaySensors = new ArrayList<String>();
//            }
//
//            List<String> displaySensors = (List<String>) value;
//            for(String displaySensor : displaySensors)
//            {
//                this.displaySensors.add(displaySensor);
//            }
//        }
//        else if(key.equalsIgnoreCase("requiredSensors"))
//        {
//            if(this.requiredSensors == null)
//            {
//                this.requiredSensors = new ArrayList<String>();
//            }
//
//            List<String> requiredSensors = (List<String>) value;
//            for(String requiredSensor : requiredSensors)
//            {
//                this.requiredSensors.add(requiredSensor);
//            }
//        }
//        else if(key.equalsIgnoreCase("freezerProductSensors"))
//        {
//            if(this.freezerProductSensors == null)
//            {
//                this.freezerProductSensors = new ArrayList<String>();
//            }
//
//            List<String> freezerProductSensors = (List<String>) value;
//            for(String freezerProductSensor : freezerProductSensors)
//            {
//                this.freezerProductSensors.add(freezerProductSensor);
//            }
//        }
//        else if(key.equalsIgnoreCase("freezerAmbientSensors"))
//        {
//            if(this.freezerAmbientSensors == null)
//            {
//                this.freezerAmbientSensors = new ArrayList<String>();
//            }
//
//            List<String> freezerAmbientSensors = (List<String>) value;
//            for(String freezerAmbientSensor : freezerAmbientSensors)
//            {
//                this.freezerAmbientSensors.add(freezerAmbientSensor);
//            }
//        }
//        else if(key.equalsIgnoreCase("coolerProductSensors"))
//        {
//            if(this.coolerProductSensors == null)
//            {
//                this.coolerProductSensors = new ArrayList<String>();
//            }
//
//            List<String> coolerProductSensors = (List<String>) value;
//            for(String coolerProductSensor : coolerProductSensors)
//            {
//                this.coolerProductSensors.add(coolerProductSensor);
//            }
//        }
//        else if(key.equalsIgnoreCase("coolerAmbientSensors"))
//        {
//            if(this.coolerAmbientSensors == null)
//            {
//                this.coolerAmbientSensors = new ArrayList<String>();
//            }
//
//            List<String> coolerAmbientSensors = (List<String>) value;
//            for(String coolerAmbientSensor : coolerAmbientSensors)
//            {
//                this.coolerAmbientSensors.add(coolerAmbientSensor);
//            }
//        }
//        else if(key.equalsIgnoreCase("drySensors"))
//        {
//            if(this.drySensors == null)
//            {
//                this.drySensors = new ArrayList<String>();
//            }
//
//            List<String> drySensors = (List<String>) value;
//            for(String drySensor : drySensors)
//            {
//                this.drySensors.add(drySensor);
//            }
//        }
//        else
//        {
//            super.setValueForKey(value, key);
//        }
//    }

}
