package com.procuro.apimmdatamanagerlib;

import java.util.Date;
import java.util.HashMap;

public class FacilityScorecardAuditDTO extends PimmBaseObject {
    public Date timestamp;
    public Date actualLast;
    public Date practiceLast;
    public int actualPassPercentage;
    public int practicePassPercentage;
    public int actualAuditCount;
    public int practiceAuditCount;
    public int grade;


    @Override
    public void setValueForKey(Object value, String key)
    {
        super.setValueForKey(value, key);
    }

    public HashMap<String,Object> dictionaryWithValuesForKeys() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();


        dictionary.put("timestamp", (this.timestamp == null) ? null : DateTimeFormat.convertToJsonDateTime(this.timestamp));
        dictionary.put("actualLast", (this.actualLast == null) ? null : DateTimeFormat.convertToJsonDateTime(this.actualLast));
        dictionary.put("practiceLast", (this.practiceLast == null) ? null : DateTimeFormat.convertToJsonDateTime(this.practiceLast));


        dictionary.put("actualPassPercentage",this.actualPassPercentage);
        dictionary.put("practicePassPercentage",this.practicePassPercentage);
        dictionary.put("actualAuditCount",this.actualAuditCount);
        dictionary.put("practiceAuditCount",this.practiceAuditCount);
        dictionary.put("grade",this.grade);

        return dictionary;
    }

}
