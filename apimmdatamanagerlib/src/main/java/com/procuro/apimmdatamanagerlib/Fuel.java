package com.procuro.apimmdatamanagerlib;

import java.util.Date;

/**
 * Created by jophenmarieweeks on 10/07/2017.
 */

public class Fuel extends PimmBaseObject {
    public Date Created;
    public String FuelId;
    public Date Modified;
    public Date Timestamp;
    public double TractorCost;
    public double TractorOdometer;
    public double TractorQuantity;
    public double TrailerCost;
    public double TrailerOdometer;
    public double TrailerQuantity;
    public String UserId;
    public String Username;
    public double DEFQuantity;
    public double DEFCost;



    @Override
    public void setValueForKey(Object value, String key) {
        if (key.equalsIgnoreCase("TractorCost")){
            if (ValidObject(value)){
                try {
                    this.TractorCost = Double.parseDouble(value.toString());
                }catch (Exception ignored){}
            }
        }

        else if (key.equalsIgnoreCase("TractorOdometer")){
            if (ValidObject(value)){
                try {
                    this.TractorOdometer = Double.parseDouble(value.toString());
                }catch (Exception ignored){}
            }
        }
        else if (key.equalsIgnoreCase("TractorQuantity")){
            if (ValidObject(value)){
                try {
                    this.TractorQuantity = Double.parseDouble(value.toString());
                }catch (Exception ignored){}
            }
        }
        else if (key.equalsIgnoreCase("TrailerCost")){
            if (ValidObject(value)){
                try {
                    this.TrailerCost = Double.parseDouble(value.toString());
                }catch (Exception ignored){}
            }
        }
        else if (key.equalsIgnoreCase("TrailerOdometer")){
            if (ValidObject(value)){
                try {
                    this.TrailerOdometer = Double.parseDouble(value.toString());
                }catch (Exception ignored){}
            }
        }
        else if (key.equalsIgnoreCase("TrailerQuantity")){
            if (ValidObject(value)){
                try {
                    this.TrailerQuantity = Double.parseDouble(value.toString());
                }catch (Exception ignored){}
            }
        }
        else if (key.equalsIgnoreCase("DEFQuantity")){
            if (ValidObject(value)){
                try {
                    this.DEFQuantity = Double.parseDouble(value.toString());
                }catch (Exception ignored){}
            }
        }
        else if (key.equalsIgnoreCase("DEFCost")){
            if (ValidObject(value)){
                try {
                    this.DEFCost = Double.parseDouble(value.toString());
                }catch (Exception ignored){}
            }
        }
        else if (key.equalsIgnoreCase("Created"))
            this.Created = JSONDate.convertJSONDateToNSDate(String.valueOf(value));
        else if (key.equalsIgnoreCase("Modified"))
            this.Modified = JSONDate.convertJSONDateToNSDate(String.valueOf(value));
        else if (key.equalsIgnoreCase("Timestamp"))
            this.Timestamp = JSONDate.convertJSONDateToNSDate(String.valueOf(value));
        else
            super.setValueForKey(value, key);
    }

    private boolean ValidObject(Object object){
        if (object!=null && !object.toString().equalsIgnoreCase("null") && !object.toString().equalsIgnoreCase("")){
            return true;
        }
        return false;
    }
}
