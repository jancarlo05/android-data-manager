package com.procuro.apimmdatamanagerlib;

import java.util.Date;
import java.util.HashMap;

/**
 * Created by jophenmarieweeks on 10/07/2017.
 */

public class VehicleDataPackage extends PimmBaseObject {
    public double lat;
    public double lon;
    public int speed;
    public int direction;
    public Date timestamp;


    public HashMap<String,Object> dictionaryWithValuesForKeys() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();

        dictionary.put("lat", this.lat);
        dictionary.put("lon", this.lon);
        dictionary.put("speed", this.speed);
        dictionary.put("direction", this.direction);
        dictionary.put("timestamp", this.timestamp);

        return dictionary;
    }
}
