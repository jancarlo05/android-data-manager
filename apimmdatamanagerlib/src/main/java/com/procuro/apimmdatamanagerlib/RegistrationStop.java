package com.procuro.apimmdatamanagerlib;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by jophenmarieweeks on 01/08/2017.
 */

public class RegistrationStop extends PimmBaseObject {

    public String registrationStopId;
    public String trackKey;
    public String dcId;
    public String dcDepotId;
    public String deliveryOrder;
    public String po;
    public String bolNumber;
    public Date scheduledArrival;
    public ArrayList<RegistrationLot> lots;
//@property (nonatomic, strong) NSMutableArray<RegistrationLot *> *lots;

}
