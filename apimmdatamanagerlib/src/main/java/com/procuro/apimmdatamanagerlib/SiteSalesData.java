package com.procuro.apimmdatamanagerlib;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;

public class SiteSalesData extends  PimmBaseObject {

    public String version;

    public int storeOpHrs;
    public double projected;
    public double dss;

    public Date endTime;
    public Date startTime;

    public ArrayList<SiteSalesActual>sales;
    public ArrayList<SiteSalesForecastTime>forecast;
    public ArrayList<SiteSalesDaypart>dayparts;

    public boolean isFromDefault;


    @Override
    public void setValueForKey(Object value, String key) {

        if (key.equalsIgnoreCase("dss")){
            if (value!=null){
                if (!value.toString().equalsIgnoreCase("null")){
                    try {
                        dss = Double.parseDouble(value.toString());
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }
        }
        else if (key.equalsIgnoreCase("projected")){
            if (value!=null){
                if (!value.toString().equalsIgnoreCase("null")){
                    try {
                        projected =  Double.parseDouble(value.toString());
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }
        }

        else if (key.equalsIgnoreCase("startTime")){
            if (value!=null){
                try {
                    startTime = DateTimeFormat.ConvertISOToDate(value.toString());
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }

        else if (key.equalsIgnoreCase("endTime")){
            if (value!=null){
                try {
                    endTime = DateTimeFormat.ConvertISOToDate(value.toString());
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }

        else if (key.equalsIgnoreCase("sales")){

            ArrayList<SiteSalesActual>list = new ArrayList<>();

            if (value!=null){

                JSONArray jsonArray = (JSONArray) value;

                for (int i = 0; i <jsonArray.length() ; i++) {

                    try {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        SiteSalesActual item = new SiteSalesActual();
                        item.readFromJSONObject(jsonObject);
                        list.add(item);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }
            this.sales = list;
        }

        else if (key.equalsIgnoreCase("forecast")){
            ArrayList<SiteSalesForecastTime>list = new ArrayList<>();
            if (value!=null){
                JSONArray jsonArray = (JSONArray) value;
                for (int i = 0; i <jsonArray.length() ; i++) {
                    try {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        SiteSalesForecastTime item = new SiteSalesForecastTime();
                        item.readFromJSONObject(jsonObject);
                        list.add(item);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }
            this.forecast = list;
        }

        else if (key.equalsIgnoreCase("dayparts")){
            ArrayList<SiteSalesDaypart>list = new ArrayList<>();
            if (value!=null){
                JSONArray jsonArray = (JSONArray) value;
                for (int i = 0; i <jsonArray.length() ; i++) {
                    try {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        SiteSalesDaypart item = new SiteSalesDaypart();
                        item.readFromJSONObject(jsonObject);
                        list.add(item);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }
            this.dayparts = list;
        }else {
            super.setValueForKey(value, key);
        }
    }
}


