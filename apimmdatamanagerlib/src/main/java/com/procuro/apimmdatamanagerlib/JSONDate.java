package com.procuro.apimmdatamanagerlib;

import java.util.Date;

/**
 * Created by EmmanKusumi on 7/4/17.
 */

public class JSONDate extends Date
{
    static public Date convertJSONDateToNSDate(String jsonDate)
    {
        if(jsonDate == null)
        {
            return null;
        }
        else
        {
            int startPosition = jsonDate.lastIndexOf('(');
            int lastPosition = jsonDate.lastIndexOf(')');

            if(startPosition == -1 || lastPosition == -1)
            {
                return null;
            }
            else
            {
                long timeInterval = Long.parseLong(jsonDate.substring(startPosition + 1, lastPosition));

                if(timeInterval < 0)
                {
                    return null;
                }
                else
                {
                    Date date = new Date(timeInterval);
                    return date;
                }
            }
        }
    }



    public static String convertJavaDatetoJsonDate(Date date){
        String jsonStringdate = null;

        if (date!=null){
            jsonStringdate = DateTimeFormat.convertToJsonDateTime(date);
        }

        return jsonStringdate;
    }
}
