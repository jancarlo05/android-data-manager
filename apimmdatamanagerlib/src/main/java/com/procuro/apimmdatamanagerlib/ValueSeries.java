package com.procuro.apimmdatamanagerlib;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by jophenmarieweeks on 10/07/2017.
 */

public class ValueSeries extends PimmBaseObject {

    public ArrayList<String> Values;

//Array of ValueSeriesAttribute
    public ArrayList<ValueSeriesAttribute> Attributes;

    public String SourceId;
    public Boolean Compacted;
    public Boolean TimeAsMinutes;
    public int Epoch;
    public String CompactedValues;
    public int SampleRate;
    public Date StartTime;
    public Date EndTime;
    public String ValueType;


    @Override
    public void setValueForKey(Object value, String key)
    {
        if(key.equalsIgnoreCase("Attributes"))
        {
            if(this.Attributes == null)
            {
                this.Attributes = new ArrayList<ValueSeriesAttribute>();
            }

            JSONArray arrAttributes = (JSONArray) value;

            if (arrAttributes != null) {
                for (int i=0; i<arrAttributes.length(); i++) {
                    try {
                        ValueSeriesAttribute valueSeriesAttribute = new ValueSeriesAttribute();
                        valueSeriesAttribute.readFromJSONObject(arrAttributes.getJSONObject(i));

                        this.Attributes.add(valueSeriesAttribute);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        else
        {
            super.setValueForKey(value, key);
        }
    }
}
