package com.procuro.apimmdatamanagerlib;

import android.util.Log;

import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by jophenmarieweeks on 07/12/2017.
 */

public class iPimmDriverLogManager extends PimmBaseObject {



    public String getJSONStringForDriverTimeLog (DriverTimeLog driverTimeLog){
        HashMap<String, Object> driverLogDictionary = driverTimeLog.dictionaryWithValuesForKeys();
        JSONObject jsonObjFB = new JSONObject(driverLogDictionary);
        String jsonString = jsonObjFB.toString();

        Log.v("DM","JSONRepresentation of Driver Time Log: " + jsonString);
        return jsonString;
    }


    public DriverTimeLog getDriverTimeLogFromJSONData(byte[] bodyData) {

        return  null;
    }

    public static iPimmDriverLogManager getDriverLogManager() {
        return null;
    }



}
