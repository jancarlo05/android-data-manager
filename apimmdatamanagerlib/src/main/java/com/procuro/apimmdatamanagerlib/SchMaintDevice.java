package com.procuro.apimmdatamanagerlib;

import java.util.Date;

/**
 * Created by jophenmarieweeks on 07/07/2017.
 */

public class SchMaintDevice extends PimmBaseObject {

    public String schMaintDeviceID ; // PIMM GUID
    public String userID ;             // submitter, PIMM User
    public String DeviceID ;           // PIMM Device for this record
    public Date creationDate;              // timestamp for when this WO was created
    public String assetProfileID ;     // optional, specific asset of the device
    public int repeatType;                   // the trigger for creating WorkOrders
    public int repteatParam;                 // parameter used when RepeatType is applied
    public int service;                      // customer-specific id for the work performed
    public String instructions ;       // free text instructions from submitter
    public int enabled;                  // 0 = disabled, 1 = enabled
}

