package com.procuro.apimmdatamanagerlib;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by jophenmarieweeks on 07/07/2017.
 */

public class RouteShipmentEquipmentDTO extends PimmBaseObject {

    public String RouteShipmentEquipmentID ;// unique ID
    public String CustomerEquipmentID;
    public String DCID ;// customerID that owns this entry
    public String description ; // description for this product.
    public boolean valid;  // true if valid and usable, false if this type has been retired.

    public int QuantityDeliveredActual;
    public int QuantityDeliveredScheduled;
    public int QuantityPickUpActual;
    public int QuantityPickUpScheduled;

    public HashMap<String,Object> dictionaryWithValuesForKeys() {

        HashMap<String, Object> dictionary = new HashMap<String, Object>();

        dictionary.put("RouteShipmentEquipmentID", this.RouteShipmentEquipmentID);
        dictionary.put("CustomerEquipmentID", this.CustomerEquipmentID);
        dictionary.put("DCID", this.DCID);
        dictionary.put("description", this.description);
        dictionary.put("valid", this.valid);
        dictionary.put("QuantityDeliveredActual", this.QuantityDeliveredActual);
        dictionary.put("QuantityDeliveredScheduled", this.QuantityDeliveredScheduled);
        dictionary.put("QuantityPickUpActual", this.QuantityPickUpActual);
        dictionary.put("QuantityPickUpScheduled", this.QuantityPickUpScheduled);

        return dictionary;

    }
}
