package com.procuro.apimmdatamanagerlib;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jophenmarieweeks on 12/07/2017.
 */

public class RuleSet extends PimmBaseObject {


    public class Rule extends PimmBaseObject {
        public String RuleId;
        public String Name;
        public int Bounds;
        public int AlgorithmType;
        public String BoundsText;
        public String AlgorithmName;
        public String TriggerClause;
        public boolean Visible;
        public boolean MetricInclusive;
        public boolean RuleInclusive;
        public boolean IsTrigger;
        public int MetricRed;
        public int MetricYellow;
        public int MetricGreen;
        public int RuleRed;
        public int RuleYellow;
        public int RuleGreen;
        public String MetricRedText;
        public String MetricYellowText;
        public String MetricGreenText;
        public String RuleRedText;
        public String RuleYellowText;
        public String RuleGreenText;

    }


    public String RuleSetId;
    public String Name;
    public String Description;
    public ArrayList<Rule> Rules; //List of RuleObjects


    @Override
    public void setValueForKey(Object value, String key) {
        if (key.equalsIgnoreCase("Rules")) {
            if (this.Rules == null) {
                this.Rules = new ArrayList<Rule>();
            }

            if (!value.equals(null)) {
                JSONArray arrStops = (JSONArray) value;

                if (arrStops != null) {
                    for (int i = 0; i < arrStops.length(); i++) {
                        try {
                            Rule rule = new Rule();
                            rule.readFromJSONObject(arrStops.getJSONObject(i));

                            this.Rules.add(rule);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } else {
                super.setValueForKey(value, key);
            }
        }

    }
}
