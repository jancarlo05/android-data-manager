package com.procuro.apimmdatamanagerlib;

/**
 * Created by jophenmarieweeks on 11/07/2017.
 */

public class iPimmLogManager extends PimmBaseObject {


   public enum ApplicationLogLevelEnum
    {
        ApplicationLogLevel_Default (0),
        ApplicationLogLevel_Verbose (1),
        ApplicationLogLevel_Debug (2),
        ApplicationLogLevel_Info (3),
        ApplicationLogLevel_Error (4);

        private int value;
        private ApplicationLogLevelEnum(int value)
        {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }

        public static ApplicationLogLevelEnum setIntValue (int i) {
            for (ApplicationLogLevelEnum type : ApplicationLogLevelEnum.values()) {
                if (type.value == i) { return type; }
            }
            return ApplicationLogLevel_Default;
        }
    }

   public String applicationName;
   public String vendorId;
   public String userName;
   public String passwd;
   public String baseUrl;


    //Static Method that returns the Singleton Instance of HOSRulesEngine


    public boolean isInitialized;
    public static iPimmLogManager logManager = null;
    /*
    #ifdef DEBUG
    int ddLogLevel = DDLogLevelVerbose | LOG_LEVEL_API;
    #else
    int ddLogLevel = DDLogLevelInfo |LOG_LEVEL_API;
    #endif

    */




}
