package com.procuro.apimmdatamanagerlib;

import java.util.HashMap;


public class SupplierDeliveryDCScorecardDTO extends PimmBaseObject {

    public int grade;
    public int overallGrade;
    public int tagSubmissionPercent;
    public int tagsInTransit;
    public int tagsShipped;
    public int tagsSubmitted;

    @Override
    public void setValueForKey(Object value, String key)
    {
        super.setValueForKey(value, key);
    }

    public HashMap<String,Object> dictionaryWithValuesForKeys() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();

        dictionary.put("grade",this.grade);
        dictionary.put("overallGrade",this.overallGrade);
        dictionary.put("tagSubmissionPercent",this.tagSubmissionPercent);
        dictionary.put("tagsInTransit",this.tagsInTransit);
        dictionary.put("tagsShipped",this.tagsShipped);
        dictionary.put("tagsSubmitted",this.tagsSubmitted);

        return dictionary;
    }

}
