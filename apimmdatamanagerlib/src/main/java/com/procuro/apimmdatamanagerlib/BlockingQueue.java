package com.procuro.apimmdatamanagerlib;

/**
 * Created by jophenmarieweeks on 12/07/2017.
 */

public class BlockingQueue extends PimmBaseObject {

    public CoreDataHelper persistentRequestStore;
    public Boolean isFinished;

///**
// * Enqueues an object to the queue.
// * @param object Object to enqueue
// */
//- (void)enqueue:(id)object;
//- (void)addOperation:(id)object;
//
//
///**
// * Dequeues an object from the queue.  This method will block.
// */
//- (id)dequeue;
//
//
///**
// *Returns the first item in the queue. Item is not removed from the queue
// */
//
//-(id) top;
//
//
//- (void) addRequestsFromPersistentStore;
//- (NSUInteger)count;
//
//-(BOOL) isEmpty;
//
////API can be used to set the flad to indicate that no more items will be added to the queue
//-(void) markFinished;
}
