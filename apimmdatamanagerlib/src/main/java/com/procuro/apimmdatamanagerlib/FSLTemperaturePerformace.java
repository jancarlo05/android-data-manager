package com.procuro.apimmdatamanagerlib;

import java.util.Date;

public class FSLTemperaturePerformace extends PimmBaseObject {

    public String siteID;
    public String deviceID;

    public Date timestamp;
    public double percentInThreshold;
    public double percentInService;
    public int totalMetricCount;
    public int reportingMetricCount;
    public int score;

    @Override
    public void setValueForKey(Object value, String key) {
        super.setValueForKey(value, key);

        if (key.equalsIgnoreCase("timestamp")){
            if (value!=null){
                try {
                    this.timestamp = DateTimeFormat.convertJSONDateToDate(value.toString());
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }

        if (key.equalsIgnoreCase("percentInThreshold")){
            if (value!=null){
                try {
                    this.percentInThreshold = Double.parseDouble(value.toString());
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }

        if (key.equalsIgnoreCase("percentInService")){
            if (value!=null){
                try {
                    this.percentInService = Double.parseDouble(value.toString());
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
    }
}
