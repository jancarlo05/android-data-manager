package com.procuro.apimmdatamanagerlib.DashboardDTOs;

import com.procuro.apimmdatamanagerlib.DashboardDTOs.GradeEnum;
import com.procuro.apimmdatamanagerlib.PimmBaseObject;

import java.util.Date;

/**
 * Created by jophenmarieweeks on 01/08/2017.
 */

public class StD_DistributorDashboardDTO extends PimmBaseObject {

    public Date startDate;
    public Date endDate;
    public String siteId; // if null, data is for "all sites" subject to calling user's permission
    public String siteName; // if null, "all sites"
    public  GradeEnum.DashboardGradeEnum grade ; // TBD change to 'Grade' Enum


//    @property (nonatomic,strong,nonnull) NSDictionary<NSString*, NSNumber*>* driverScoreList; // for each Grade, number of drivers with that score
//    @property (nonatomic,strong,nonnull) NSDictionary<NSString*, NSNumber*>* inspectionStateList; // for each InspectionState, number of shipments with that state
//    @property (nonatomic,strong,nonnull) NSDictionary<NSString*, NSNumber*>* inspectionGradeList; // for each Grade, number of shipments with that inspection grade
//    @property (nonatomic,strong,nonnull) NSDictionary<NSString*, NSNumber*>* assetList;  // for each assetType, count of that asset type
//    @property (nonatomic,strong,nonnull) NSDictionary<NSString*, NSNumber*>* assetUtilizationList;   // for each assetType, percent utilization
//    @property (nonatomic,strong,nonnull) NSDictionary<NSString*, NSNumber*>* assetMpgList;  // for each assetType, average mpg
//    @property (nonatomic,strong,nonnull) NSDictionary<NSString*, NSNumber*>* otdList;// for each otdMetric, count of stops in that category
//
//    @property (nonatomic,strong,nonnull) NSDictionary<NSString*, NSNumber*>* coolerInTempList; // for each inTempMetric, count of stops in that category // for the cooler compartment
//    @property (nonatomic,strong,nonnull) NSDictionary<NSString*, NSNumber*>* freezerInTempList; // for each inTempMetric, count of stops in that category for the freezer compartment
//    @property (nonatomic,strong,nonnull) NSDictionary<NSString*, NSNumber*>* eventList; // for each type of StdEvent, count of events in that category
//    @property (nonatomic,strong,nonnull) NSDictionary<NSString*, NSNumber*>* routeStatusList; // for each routeStatus, count of routes in that status
//
//    @property (nonatomic,strong,nonnull) NSDictionary<NSString*, NSNumber*>* routeStatusOpList; // for each routeStatusOp, count of routes in that status
//    @property (nonatomic,strong,nonnull) NSDictionary<NSString*, NSNumber*>* deviceCountList; // for each deviceType, count of registered devices of that type
//    @property (nonatomic,strong,nonnull) NSDictionary<NSString*, NSNumber*>* devicePerformanceList; // for each deviceType, average percent uptime

//
//+(nonnull NSArray<NSString*>*) GradeLabels;
//
//+(nonnull NSArray<NSString*>*) InspectionStateLabels;
//
//+(nonnull NSArray<NSString*>*) AssetTypeLabels;
//
//+(nonnull NSArray<NSString*>*) OtdMetricLabels;
//
//+(nonnull NSArray<NSString*>*) InTempMetricLabels;
//
//+(nonnull NSArray<NSString*>*) StdEventLabels;
//
//+(nonnull NSArray<NSString*>*) RouteStatusLabels;
//
//+(nonnull NSArray<NSString*>*) RouteStatusOpLabels;
//
//+(nonnull NSArray<NSString*>*) DeviceTypeLabels;
//
}
