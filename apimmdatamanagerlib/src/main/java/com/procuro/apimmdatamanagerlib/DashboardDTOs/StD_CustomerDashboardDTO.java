package com.procuro.apimmdatamanagerlib.DashboardDTOs;

import com.procuro.apimmdatamanagerlib.DashboardDTOs.GradeEnum;
import com.procuro.apimmdatamanagerlib.PimmBaseObject;

import java.util.Date;

/**
 * Created by jophenmarieweeks on 01/08/2017.
 */

public class StD_CustomerDashboardDTO extends PimmBaseObject {

    public Date startDate;
    public Date endDate;
    public String siteId; // if null, data is for "all sites" subject to calling user's permission
    public String siteName; // if null, "all sites"
    public GradeEnum.DashboardGradeEnum grade ; // TBD change to 'Grade' Enum

//    @property (nonatomic,strong,nonnull) NSDictionary<NSString*, NSNumber*>* otdList; //for each OTD Metric, the number of stops
//    @property (nonatomic,strong,nonnull) NSDictionary<NSString*, NSNumber*>* otdPercentList; //for each OTD Metric, the percent of stops
//    @property (nonatomic,strong,nonnull) NSDictionary<NSString*, NSNumber*>* gradeList; //for each grade, count of stops with that grade
//    @property (nonatomic,strong,nonnull) NSDictionary<NSString*, NSNumber*>* gradePercentList;  //for each grade, the percent of stops with that grade


//+(nonnull NSArray<NSString*>*) GradeLabels;
//+(nonnull NSArray<NSString*>*) OtdMetricLabels;

}
