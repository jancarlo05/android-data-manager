package com.procuro.apimmdatamanagerlib.DashboardDTOs;

import com.procuro.apimmdatamanagerlib.PimmBaseObject;

import java.util.Date;

/**
 * Created by jophenmarieweeks on 01/08/2017.
 */

public class FM_FacilityDashboardDTO extends PimmBaseObject {
    public Date startDate;
    public Date endDate;
    public String siteId; // if null, data is for "all sites" subject to calling user's permission
    public String siteName; // if null, "all sites"
    public GradeEnum.DashboardGradeEnum grade ; // average grade for all included sites

//    @property (nonatomic,strong,nonnull) NSDictionary<NSString*, NSNumber*>* gradeList; // for each grade, the number of facilities
//    @property (nonatomic,strong,nonnull) NSDictionary<NSString*, NSNumber*>* eventList; // for each event severity, the number of events
//    @property (nonatomic,strong,nonnull) NSDictionary<NSString*, NSNumber*>* avgTempList;  // for each temp zone, the average temperature
//    @property (nonatomic,strong,nonnull) NSDictionary<NSString*, NSNumber*>* inTargetPercentList; // for each temp zone, the percent of time in target
//    @property (nonatomic,strong,nonnull) NSDictionary<NSString*, NSNumber*>* inServicePercentList; // for each temp zone, the percent of time in service
//
//
//+(nonnull NSArray<NSString*>*) GradeLabels;
//
//+(nonnull NSArray<NSString*>*) EventSeverityLabels;
//+(nonnull NSArray<NSString*>*) TempZoneLabels;


}
