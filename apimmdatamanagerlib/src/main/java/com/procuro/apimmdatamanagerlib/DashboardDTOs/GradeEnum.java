package com.procuro.apimmdatamanagerlib.DashboardDTOs;

import com.procuro.apimmdatamanagerlib.PimmBaseObject;

/**
 * Created by jophenmarieweeks on 01/08/2017.
 */

public class GradeEnum extends PimmBaseObject {
   public enum DashboardGradeEnum
    {
        I(0),
        F(1),
        D(2),
        C(3),
        B(4),
        A(5);

        private int value;
        private DashboardGradeEnum(int value)
        {
            this.value = value;
        }


        public int getValue() {
            return this.value;
        }

        public static DashboardGradeEnum setIntValue (int i) {
            for (DashboardGradeEnum type : DashboardGradeEnum.values()) {
                if (type.value == i) { return type; }
            }
            return I;
        }
    }

    public DashboardGradeEnum dashboardGradeEnum;

    @Override
    public void setValueForKey(Object value, String key) {

        if (key.equalsIgnoreCase("dashboardGradeEnum")) {

            if (!value.equals(null)) {
                DashboardGradeEnum gradeEnum = DashboardGradeEnum.setIntValue((int) value);
                this.dashboardGradeEnum = gradeEnum;
            }

        }else {
            super.setValueForKey(value, key);
        }
    }

}
