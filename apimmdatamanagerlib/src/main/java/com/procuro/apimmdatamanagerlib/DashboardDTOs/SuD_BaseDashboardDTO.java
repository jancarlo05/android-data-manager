package com.procuro.apimmdatamanagerlib.DashboardDTOs;

import com.procuro.apimmdatamanagerlib.DashboardDTOs.GradeEnum;
import com.procuro.apimmdatamanagerlib.PimmBaseObject;

import java.util.Date;

/**
 * Created by jophenmarieweeks on 01/08/2017.
 */

public class SuD_BaseDashboardDTO extends PimmBaseObject {
   public Date startDate;
   public Date endDate;
   public String siteId; // if null, data is for "all sites" subject to calling user's permission
   public String siteName; // if null, "all sites"
    public  GradeEnum.DashboardGradeEnum grade ;// average grade for all included sites
    public double cradleRate; // 0 - 100 percent

//    @property (nonatomic,strong,nonnull) NSDictionary<NSString*, NSNumber*>* shipmentScoreList;  // for each Grade, number of shipments with that score
//    @property (nonatomic,strong,nonnull) NSDictionary<NSString*, NSNumber*>* inspectionStateList; // for each InspectionState, number of shipments with that state
//    @property (nonatomic,strong,nonnull) NSDictionary<NSString*, NSNumber*>* inspectionGradeList; // for each Grade, number of shipments with that inspection grade
//    @property (nonatomic,strong,nonnull) NSDictionary<NSString*, NSNumber*>* pendingShipmentList; // for each CarrierType, number of pending shipments
//    @property (nonatomic,strong,nonnull) NSDictionary<NSString*, NSNumber*>* flaggedShipmentList;  // for each CarrierType, number of flagged shipments
//    @property (nonatomic,strong,nonnull) NSDictionary<NSString*, NSNumber*>* rejectedShipmentList; // for each CarrierType, number of rejected shipments
//    @property (nonatomic,strong,nonnull) NSDictionary<NSString*, NSNumber*>* cradleStatusList;  // for each CradleStatus, number of shipments
//
//    @property (nonatomic,strong,nonnull) NSDictionary<NSString*,NSDictionary<NSString*,NSNumber*>*>* gradeByCarrierTypeList;  // for each CarrierType, number of shipments for each Grade

//
//+(nonnull NSArray<NSString*>*) GradeLabels;
//
//+(nonnull NSArray<NSString*>*) CarrierTypeLabels;
//
//+(nonnull NSArray<NSString*>*) CradleStatusLabels;
//
//+(nonnull NSArray<NSString*>*) InspectionStateLabels;



}
