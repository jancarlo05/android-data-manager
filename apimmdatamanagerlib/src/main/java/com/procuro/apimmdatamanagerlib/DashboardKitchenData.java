package com.procuro.apimmdatamanagerlib;

import android.util.Base64;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.HashMap;

public class DashboardKitchenData extends  PimmBaseObject{

    public  int kitchenWidth;
    public  int kitchenHeight;
    public  byte[] kitchenImageData;
    public  String kitchenTemplateCode;
    public  double version;

    public  ArrayList<KitchenDataItems>kitchenDataItems;
    public  ArrayList<PositionDataItems>positionDataItems;
    public  ArrayList<PositionDataItems>breakfastPositionDataItems;
    public  ArrayList<PositionDataItems>breakfastPositionDataItems_DRClosed;
    public  ArrayList<PositionDataItems>breakfastPositionDataItems_DROpen;
    public  ArrayList<PositionDataItems>lateNightPositionDataItems;


    public  int lateNightPositionDataMinimumEmployees;
    public  int lateNightPositionDataMaximumEmployees;
    public  int positionDataMinimumEmployees;
    public  int breakfastPositionDataMinimumEmployees_DRClosed;
    public  int breakfastPositionDataMaximumEmployees_DRClosed;
    public  int breakfastPositionDataMinimumEmployees_DROpen;
    public  int breakfastPositionDataMaximumEmployees_DROpen;
    public  int positionDataMaximumEmployees;



    @Override
    public void setValueForKey(Object value, String key) {
        super.setValueForKey(value, key);

        if (key.equalsIgnoreCase("breakfastPositionDataItems")){
            ArrayList<PositionDataItems>dataItems = new ArrayList<>();
            if (value!=null){
                try {
                    JSONArray jsonArray = (JSONArray) value;
                    for (int i = 0; i <jsonArray.length() ; i++) {
                        PositionDataItems positionData = new PositionDataItems();
                        positionData.readFromJSONObject(jsonArray.getJSONObject(i));
                        dataItems.add(positionData);
                    }
                }catch (Exception e){
                    e.printStackTrace();

                }
                this.lateNightPositionDataItems = dataItems;
            }
        }


        if (key.equalsIgnoreCase("lateNightPositionDataItems")){
            ArrayList<PositionDataItems>dataItems = new ArrayList<>();
            if (value!=null){
                try {
                    JSONArray jsonArray = (JSONArray) value;
                    for (int i = 0; i <jsonArray.length() ; i++) {
                        PositionDataItems positionData = new PositionDataItems();
                        positionData.readFromJSONObject(jsonArray.getJSONObject(i));
                        dataItems.add(positionData);
                    }
                }catch (Exception e){
                    e.printStackTrace();

                }
                this.lateNightPositionDataItems = dataItems;
            }
        }

        if (key.equalsIgnoreCase("breakfastPositionDataItems_DROpen")){
            ArrayList<PositionDataItems>dataItems = new ArrayList<>();
            if (value!=null){
                try {
                    JSONArray jsonArray = (JSONArray) value;
                    for (int i = 0; i <jsonArray.length() ; i++) {
                        PositionDataItems positionData = new PositionDataItems();
                        positionData.readFromJSONObject(jsonArray.getJSONObject(i));
                        dataItems.add(positionData);
                    }
                }catch (Exception e){
                    e.printStackTrace();

                }
                this.breakfastPositionDataItems_DROpen = dataItems;

            }
        }

        if (key.equalsIgnoreCase("breakfastPositionDataItems_DRClosed")){
            ArrayList<PositionDataItems>dataItems = new ArrayList<>();
            if (value!=null){
                try {
                    JSONArray jsonArray = (JSONArray) value;
                    for (int i = 0; i <jsonArray.length() ; i++) {
                        PositionDataItems positionData = new PositionDataItems();
                        positionData.readFromJSONObject(jsonArray.getJSONObject(i));
                        dataItems.add(positionData);
                    }
                }catch (Exception e){
                    e.printStackTrace();

                }
                this.breakfastPositionDataItems_DRClosed = dataItems;
            }
        }

        if (key.equalsIgnoreCase("kitchenWidth")){
            this.kitchenWidth = (int) value;
        }

        if (key.equalsIgnoreCase("kitchenHeight")){
            this.kitchenHeight = (int) value;
        }

        if (key.equalsIgnoreCase("kitchenImageData")){
            String string = value.toString().replace("\r","")
                    .replace("\n","").replace("\\","");
            this.kitchenImageData = Base64.decode(string, Base64.DEFAULT);
        }

        if (key.equalsIgnoreCase("kitchenTemplateCode")){
            this.kitchenTemplateCode = (String) value;
        }

        if (key.equalsIgnoreCase("version")){
            this.version = Double.parseDouble(value.toString());
        }

        if (key.equalsIgnoreCase("kitchenDataItems")){
            ArrayList<KitchenDataItems>dataItems = new ArrayList<>();
            if (value!=null){
                try {
                    JSONArray jsonArray = (JSONArray) value;
                    for (int i = 0; i <jsonArray.length() ; i++) {
                        KitchenDataItems kitchenDataItem = new KitchenDataItems();
                        kitchenDataItem.readFromJSONObject(jsonArray.getJSONObject(i));
                        dataItems.add(kitchenDataItem);
                    }
                }catch (Exception e){
                    e.printStackTrace();

                }
                this.kitchenDataItems = dataItems;

            }
        }
        if (key.equalsIgnoreCase("positionDataItems")){
            ArrayList<PositionDataItems>dataItems = new ArrayList<>();
            if (value!=null){
                try {
                    JSONArray jsonArray = (JSONArray) value;
                    for (int i = 0; i <jsonArray.length() ; i++) {
                        PositionDataItems positionData = new PositionDataItems();
                        positionData.readFromJSONObject(jsonArray.getJSONObject(i));
                        dataItems.add(positionData);
                    }
                }catch (Exception e){
                    e.printStackTrace();

                }
                this.positionDataItems = dataItems;

            }
        }

    }

    public HashMap<String,Object> dictionaryWithValuesForKeys() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();

        dictionary.put("kitchenWidth",this.kitchenWidth);
        dictionary.put("kitchenHeight",this.kitchenHeight);
        dictionary.put("kitchenImageData",this.kitchenImageData);
        dictionary.put("kitchenTemplateCode",this.kitchenTemplateCode);
        dictionary.put("version",this.version);
        dictionary.put("kitchenDataItems",this.kitchenDataItems);
        dictionary.put("positionDataItems",this.positionDataItems);

        return dictionary;
    }







}
