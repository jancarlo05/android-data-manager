package com.procuro.apimmdatamanagerlib;

/**
 * Created by jophenmarieweeks on 10/07/2017.
 */

public class SdrDriver extends PimmBaseObject {


    public enum DeliveryDriverRank
    {
        DeliveryDriverRank_Driver (0),
        DeliveryDriverRank_CoDriver(1);

        private int value;
        private DeliveryDriverRank(int value)
        {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }

        public static DeliveryDriverRank setIntValue (int i) {
            for (DeliveryDriverRank type : DeliveryDriverRank.values()) {
                if (type.value == i) { return type; }
            }
            return DeliveryDriverRank_Driver;
        }

    }



    public String DriverId;
    public String UserId;
    public String Username;
    public String UserHandle;
    public String FullName;
    public String FirstName;
    public String LastName;
    public DeliveryDriverRank  Rank;

    @Override
    public void setValueForKey(Object value, String key) {

        if(key.equalsIgnoreCase("Rank")) {
            if (!value.equals(null)) {
                DeliveryDriverRank deliveryDriverRank = DeliveryDriverRank.setIntValue((int) value);
                this.Rank = deliveryDriverRank;
            }

        } else {
            super.setValueForKey(value, key);
        }
    }

}
