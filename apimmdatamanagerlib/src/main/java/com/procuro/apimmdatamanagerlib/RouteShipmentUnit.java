package com.procuro.apimmdatamanagerlib;

import java.util.HashMap;

/**
 * Created by jophenmarieweeks on 10/07/2017.
 */

public class RouteShipmentUnit extends PimmBaseObject {

    public String RouteShipmentUnitID;
    public String DCID;
    public String Description;
    public int CustomerUnitID;

    public HashMap<String,Object> dictionaryWithValuesForKeys() {

        HashMap<String, Object> dictionary = new HashMap<String, Object>();

        dictionary.put("RouteShipmentUnitID", this.RouteShipmentUnitID);
        dictionary.put("DCID", this.DCID);
        dictionary.put("Description", this.Description);
        dictionary.put("CustomerUnitID", this.CustomerUnitID);

        return dictionary;

    }
}
