package com.procuro.apimmdatamanagerlib;

import org.json.JSONObject;

import java.util.HashMap;

public class FacilityScorecardDTO extends PimmBaseObject {

    public int flagCountGreen;
    public int flagCountYellow;
    public int flagCountRed;
    public int flagCountBlack;
    public int grade;
    public int percentInService;
    public int percentInThreshold;

    public FacilityScorecardInspectionDTO inspectionScorecard;
    public FacilityScorecardAuditDTO auditScorecard;

    @Override
    public void setValueForKey(Object value, String key)
    {

        if (key.equalsIgnoreCase("inspectionScorecard")) {
            if (!value.equals(null)) {
                FacilityScorecardInspectionDTO inspectionDTO = new FacilityScorecardInspectionDTO();
                JSONObject jsonObject = (JSONObject) value;
                inspectionDTO.readFromJSONObject(jsonObject);
                this.inspectionScorecard = inspectionDTO;
            }
        } else if (key.equalsIgnoreCase("auditScorecard")){
            if (!value.equals(null)) {
                FacilityScorecardAuditDTO auditDTO = new FacilityScorecardAuditDTO();
                JSONObject jsonObject = (JSONObject) value;
                auditDTO.readFromJSONObject(jsonObject);
                this.auditScorecard = auditDTO;
            }
        } else {
            super.setValueForKey(value, key);
        }
    }

    public HashMap<String,Object> dictionaryWithValuesForKeys() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();

        dictionary.put("flagCountGreen",this.flagCountGreen);
        dictionary.put("flagCountYellow",this.flagCountYellow);
        dictionary.put("flagCountRed",this.flagCountRed);
        dictionary.put("flagCountBlack",this.flagCountBlack);
        dictionary.put("grade",this.grade);
        dictionary.put("percentInService",this.percentInService);
        dictionary.put("percentInThreshold",this.percentInThreshold);

        dictionary.put("inspectionScorecard",this.inspectionScorecard.dictionaryWithValuesForKeys());
        dictionary.put("auditScorecard",this.auditScorecard.dictionaryWithValuesForKeys());

        return dictionary;
    }


}
