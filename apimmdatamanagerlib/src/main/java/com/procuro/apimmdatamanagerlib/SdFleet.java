package com.procuro.apimmdatamanagerlib;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jophenmarieweeks on 19/07/2017.
 */

public class SdFleet extends PimmBaseObject {
    public String DCId;
    public ArrayList<SdFleetVehicle> Vehicles; //List of SdFleetVehicle objects
    public ArrayList<SdrGisSite>  Facilities; //List of SdrGisSite objects

    @Override
    public void setValueForKey(Object value, String key)
    {
        if(key.equalsIgnoreCase("Vehicles")) {
            if (this.Vehicles == null) {
                this.Vehicles = new ArrayList<SdFleetVehicle>();
            }

            if (!value.equals(null)) {
                JSONArray arrStops = (JSONArray) value;

                if (arrStops != null) {
                    for (int i = 0; i < arrStops.length(); i++) {
                        try {
                            SdFleetVehicle result = new SdFleetVehicle();
                            result.readFromJSONObject(arrStops.getJSONObject(i));

                            this.Vehicles.add(result);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }

            }
        }else if(key.equalsIgnoreCase("Facilities")) {
            if (this.Facilities == null) {
                this.Facilities = new ArrayList<SdrGisSite>();
            }

            if (!value.equals(null)) {
                JSONArray arrStops = (JSONArray) value;

                if (arrStops != null) {
                    for (int i = 0; i < arrStops.length(); i++) {
                        try {
                            SdrGisSite result = new SdrGisSite();
                            result.readFromJSONObject(arrStops.getJSONObject(i));

                            this.Facilities.add(result);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }

            }
        }else {
            super.setValueForKey(value, key);
        }
    }

}
