package com.procuro.apimmdatamanagerlib;

/**
 * Created by EmmanKusumi on 7/4/17.
 */

import android.content.Context;
import android.os.AsyncTask;
import android.util.JsonToken;
import android.util.Log;


import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

import static com.procuro.apimmdatamanagerlib.OnCompleteListeners.OnTaskCompleteListener;

public class RestConnection extends AsyncTask<String, String, Object>

{
    public String basicAuth;


    private OnTaskCompleteListener taskCompleteListener;
    private OnCompleteListeners.OnRestConnectionCompleteListener restConnectionCompleteListener;
    private String rootObject;
    private String rootObjectList;
    private String requestMethod;
    private Context mContext;
    private String parameter;

    private boolean Mime;

    public RestConnection() { }

    public RestConnection(boolean isMime) {
        this.Mime = isMime;
    }

    public boolean isMime() {
        return Mime;
    }

    public void setMime(boolean mime) {
        Mime = mime;
    }

    @Deprecated
    public void initialize(String basicAuth, String rootObject, OnTaskCompleteListener listener)
    {
        this.basicAuth = basicAuth;
        this.rootObject = rootObject;
        this.taskCompleteListener = listener;
    }

    @Deprecated
    public void initialize(String basicAuth, String rootObject, String rootObjectList, OnTaskCompleteListener listener)
    {
        this.basicAuth = basicAuth;
        this.rootObject = rootObject;
        this.rootObjectList = rootObjectList;
        this.taskCompleteListener = listener;
    }

    public void initialize(String basicAuth, OnCompleteListeners.OnRestConnectionCompleteListener listener)
    {
        this.basicAuth = basicAuth;
        this.requestMethod = "GET";
        this.restConnectionCompleteListener = listener;
    }

    public void setAppContext(Context context){
        this.mContext = context;
    }


    public void initializePOSTRequest(String basicAuth, String parameter, OnCompleteListeners.OnRestConnectionCompleteListener listener)
    {
        this.basicAuth = basicAuth;
        this.requestMethod = "POST";
        this.parameter = parameter;
        this.restConnectionCompleteListener = listener;
    }

    public void initializeDeleteRequest(String basicAuth, String parameter, OnCompleteListeners.OnRestConnectionCompleteListener listener)
    {
        this.basicAuth = basicAuth;
        this.requestMethod = "DELETE";
        this.parameter = parameter;
        this.restConnectionCompleteListener = listener;
    }



    // ICMP
    public boolean isOnline() {
        Runtime runtime = Runtime.getRuntime();
        try {
            Process ipProcess = runtime.exec("/system/bin/ping -c 1 8.8.8.8");
            int     exitValue = ipProcess.waitFor();
            Log.v("DM","There is an internet connection");
            return (exitValue == 0);

        }
        catch (IOException e)          { e.printStackTrace(); }
        catch (InterruptedException e) { e.printStackTrace(); }
        Log.e("DM","No internet connection");
        return false;
    }

    @Override
    protected Object doInBackground(String... strings)
    {
        String urlString = strings[0];
        Log.v("DM", "URL String: " + urlString);

        URL urls = null;

        Error error = null;

        InputStream inputStream = null;

        try
        {
            inputStream = this.getDataFromURL(urlString);
            Log.v("DM", "inputStream Response: " + inputStream);

        }
        catch (NullPointerException e)
        {
            e.printStackTrace();
            Log.e("DM", "Response: error" + e.getMessage());
            error = new Error("NullPointerException");
        }
        catch (IllegalArgumentException e)
        {
            e.printStackTrace();
            Log.e("DM","IllegalArgumentException "+ e.getMessage());
            error = new Error("IllegalArgumentException");
        }
        catch (Exception e)
        {
            e.printStackTrace();
            Log.e("DM","UnknownException "+ e.getMessage());
            error = new Error("UnknownException");
        }

        Log.v("DM", "Error: " + error + " InputStream: " + inputStream);

        if(error != null || inputStream == null)
        {
            return null;
        }
        else
        {
            String inputStr = null;
            try
            {
//                inputStr = readFullyAsString(inputStream, "UTF-8");

                if(!isMime())
                    inputStr = readByStringBuilder(inputStream);

            }
            catch (IOException e)
            {
                e.printStackTrace();
                Log.e("DM","IOException "+ e.getMessage());
                error = new Error("IOException");
            }

            if(error != null)
            {
                return null;
            }
            else
            {
                Object object = null;


                    if(!isMime()) {
                        try {
                            if (!inputStr.equalsIgnoreCase("")) {
                                object = new JSONTokener(inputStr).nextValue();
                            } else {
                                error = new Error("JSONException");
                            }
                        } catch (JSONException e)
                            {
                                e.printStackTrace();
                                error = new Error("JSONException");
                            }
                    } else {
                        try {
                            object = readFully(inputStream).toByteArray();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                if(error != null)
                {
                    return null;
                }
                else
                {
                    return object;
                }


            }
        }
    }

    protected void onPostExecute(Object object)
    {
        if(object != null)
        {
            Log.v("DM", "onPostExecute: object is NOT null");
            if(this.taskCompleteListener != null)
            {
                JSONObject jsonObject = (JSONObject) object;
                this.taskCompleteListener.onTaskComplete(jsonObject);
            }
            else if(this.restConnectionCompleteListener != null)
            {
                this.restConnectionCompleteListener.onRestConnectionComplete(object, null);
            }
        }
        else
        {
            Log.v("DM", "onPostExecute: object is null");
            Error error = new Error("RestConnection Error");
            this.restConnectionCompleteListener.onRestConnectionComplete(null, error);
        }
    }

    private InputStream getDataFromURL(String urlString) {
        try {
            InputStream in = null;
            InputStream errorinputstream = null;

            URL url = new URL(urlString);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestProperty("Authorization", this.basicAuth);
            urlConnection.setRequestMethod(this.requestMethod);
            urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            urlConnection.setRequestProperty("Content-Language", "en-US");
            urlConnection.setUseCaches(false);
            urlConnection.setDoInput(true);
            urlConnection.setDoOutput(false);

            if(this.parameter != null) {
                urlConnection.setDoOutput(true);
                urlConnection.setInstanceFollowRedirects( false );
                byte[] outputInBytes = this.parameter.getBytes(StandardCharsets.US_ASCII);

                try( DataOutputStream wr = new DataOutputStream( urlConnection.getOutputStream())) {
                    wr.write(outputInBytes);

                }catch ( Exception e){
                    e.printStackTrace();
                }
            }

            int status = urlConnection.getResponseCode();
            Log.v("DM", "Response Code: " + status);

            try {

                if( status != HttpURLConnection.HTTP_OK ) {

                    return null;
                    //Get more informations about the problem
                } else {
                    in = new BufferedInputStream(urlConnection.getInputStream());
                }


            } finally {
                // urlConnection.disconnect();
            }


            return in;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }


    private String readFullyAsString(InputStream inputStream, String encoding)
            throws IOException {
        return readFully(inputStream).toString(encoding);
    }

    private byte[] readFullyAsBytes(InputStream inputStream)
            throws IOException {
        return readFully(inputStream).toByteArray();
    }

    private ByteArrayOutputStream readFully(InputStream inputStream)
            throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024 * 1024];
        int length = 0;
        while ((length = inputStream.read(buffer)) != -1) {
            baos.write(buffer, 0, length);
        }
        return baos;
    }

    private String readByStringBuilder(InputStream inputStream)
        throws IOException {
        return readFully(inputStream).toString("UTF-8");
    }



}
