package com.procuro.apimmdatamanagerlib;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

/**
 * Created by jophenmarieweeks on 07/07/2017.
 */

public class SdrChartGroup extends PimmBaseObject {


    public String ChartGroupId;
    public String Title;
    public boolean EnableComposite;
    public SdrMetric.MetricClass metricClass;
   public SdrMetric.MetricCompartment Compartment;

    public ArrayList<SdrChartGroup> Groups; //Array of SdrChartGroup
    public ArrayList<SdrChartSeries> Series; //Array of SdrChartSeries

    @Override
    public void setValueForKey(Object value, String key) {
        if (key.equalsIgnoreCase("Series")) {
            if (this.Series == null) {
                this.Series = new ArrayList<SdrChartSeries>();
            }

            JSONArray arrData = (JSONArray) value;

            if (arrData != null) {
                for (int i = 0; i < arrData.length(); i++) {
                    try {
                        SdrChartSeries sdrChartSeries = new SdrChartSeries();
                        sdrChartSeries.readFromJSONObject(arrData.getJSONObject(i));

                        this.Series.add(sdrChartSeries);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        else if(key.equalsIgnoreCase("Groups"))
        {
            if(this.Groups == null)
            {
                this.Groups = new ArrayList<SdrChartGroup>();
            }

            if (value instanceof  JSONArray) {
                JSONArray arrData = (JSONArray) value;

                if (arrData != null) {
                    for (int i = 0; i < arrData.length(); i++) {
                        try {
                            SdrChartGroup sdrChartGroup = new SdrChartGroup();
                            sdrChartGroup.readFromJSONObject(arrData.getJSONObject(i));

                            this.Groups.add(sdrChartGroup);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
       else if (key.equalsIgnoreCase("metricClass")) {
            if (!value.equals(null)) {
                SdrMetric.MetricClass metricClasss = SdrMetric.MetricClass.setIntValue((int) value);
                this.metricClass = metricClasss;
            }

        }else if (key.equalsIgnoreCase("Compartment")) {
            if (!value.equals(null)) {
                SdrMetric.MetricCompartment metricCompartment = SdrMetric.MetricCompartment.setIntValue((int) value);
                this.Compartment = metricCompartment;
            }

        }else {
            super.setValueForKey(value, key);
        }
    }
}
