package com.procuro.apimmdatamanagerlib;

/**
 * Created by jophenmarieweeks on 01/08/2017.
 */

public class TracDepot extends PimmBaseObject {

    public String tracDepotId;
    public String name;
    public String ownerId;
    public String ownerName;
    public TracContact contact;
    public TracAddress address;


}
