package com.procuro.apimmdatamanagerlib;

import org.json.JSONObject;

import java.util.Date;
import java.util.HashMap;

/**
 * Created by jophenmarieweeks on 11/07/2017.
 */

public class DriverLogTimeInfoEntry extends PimmBaseObject {

    public Date Start;
    public Date End;
    public String StartText;
    public String EndText ;
    public String AuditBy;
    public Date AuditDate;
    public String ShipmentId;

    public DriverLogTimeInfoEntryDetail StartDetail;
    public DriverLogTimeInfoEntryDetail EndDetail;


    @Override
    public void setValueForKey(Object value, String key) {

        if (key.equalsIgnoreCase("StartDetail")) {
            if (!value.equals(null)) {
                DriverLogTimeInfoEntryDetail driverLogTimeInfoEntryDetail = new DriverLogTimeInfoEntryDetail();
                JSONObject jsonObject = (JSONObject) value;
                driverLogTimeInfoEntryDetail.readFromJSONObject(jsonObject);

                this.StartDetail = driverLogTimeInfoEntryDetail;
            }
        }else if (key.equalsIgnoreCase("EndDetail")) {
            if (!value.equals(null)) {
                DriverLogTimeInfoEntryDetail driverLogTimeInfoEntryDetail = new DriverLogTimeInfoEntryDetail();
                JSONObject jsonObject = (JSONObject) value;
                driverLogTimeInfoEntryDetail.readFromJSONObject(jsonObject);

                this.EndDetail = driverLogTimeInfoEntryDetail;
            }

        }else {
            super.setValueForKey(value, key);
        }
    }

    public HashMap<String, Object> dictionaryWithValuesForKeys() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();

        dictionary.put("StartDetail", (this.StartDetail == null) ? null : this.StartDetail.dictionaryWithValuesForKeys());
        dictionary.put("EndDetail", (this.EndDetail == null) ? null : this.EndDetail.dictionaryWithValuesForKeys());
        dictionary.put("Start", this.Start);
        dictionary.put("Start", this.Start);
        dictionary.put("End", this.End);
        dictionary.put("StartText", this.StartText);
        dictionary.put("EndText", this.EndText);
        dictionary.put("AuditDate", this.AuditDate);
        dictionary.put("AuditBy", this.AuditBy);
        dictionary.put("ShipmentId", this.ShipmentId);

        return dictionary;
    }

    public HashMap<String, Object> dictionaryWithValuesForKeysForPrint() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();

        dictionary.put("StartDetail", (this.StartDetail == null) ? null : this.StartDetail.dictionaryWithValuesForKeys());
        dictionary.put("EndDetail", (this.EndDetail == null) ? null : this.EndDetail.dictionaryWithValuesForKeys());
        dictionary.put("Start", this.Start);
        dictionary.put("Start", this.Start);
        dictionary.put("End", this.End);
        dictionary.put("StartText", this.StartText);
        dictionary.put("EndText", this.EndText);
        dictionary.put("AuditDate", this.AuditDate);
        dictionary.put("AuditBy", this.AuditBy);
        dictionary.put("ShipmentId", this.ShipmentId);

        return dictionary;
    }


}
