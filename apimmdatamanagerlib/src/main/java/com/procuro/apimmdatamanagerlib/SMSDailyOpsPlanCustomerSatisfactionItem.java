package com.procuro.apimmdatamanagerlib;

import java.util.Date;
import java.util.HashMap;

public class SMSDailyOpsPlanCustomerSatisfactionItem extends PimmBaseObject {

    public String itemName;
    public Date assignedDate;
    public String task;
    public boolean assigned;
    public String position;
    public Date assignedDateISO;


    @Override
    public void setValueForKey(Object value, String key) {
        super.setValueForKey(value, key);

        if (key.equalsIgnoreCase("itemName")){
            if (value!=null){
                if (!value.toString().equalsIgnoreCase("null")){
                    this.itemName = (String) value.toString();
                }
            }
        }

        if (key.equalsIgnoreCase("assignedDate")){
            if (value!=null){
                this.assignedDate = JSONDate.convertJSONDateToNSDate(value.toString());
            }
        }

        if (key.equalsIgnoreCase("task")){
            if (value!=null){
                if (!value.toString().equalsIgnoreCase("null")){
                    this.task = (String) value.toString();
                }
            }
        }

        if (key.equalsIgnoreCase("assigned")){
            if (value!=null){
                if (!value.toString().equalsIgnoreCase("null")){

                    if (value.toString().equalsIgnoreCase("0")){
                        this.assigned = false;

                    }else if (value.toString().equalsIgnoreCase("1")){
                        this.assigned = true;

                    }else {
                        this.assigned = (boolean) value;
                    }
                }

            }
        }
        if (key.equalsIgnoreCase("position")){
            if (value!=null){
                if (!value.toString().equalsIgnoreCase("null")){
                    this.position = (String) value.toString();
                }
            }
        }
        if (key.equalsIgnoreCase("assignedDateISO")){
            if (value!=null){
                this.assignedDateISO = JSONDate.convertJSONDateToNSDate(value.toString());
            }
        }
    }


    public HashMap<String,Object> dictionaryWithValuesForKeys() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();

        dictionary.put("itemName", this.itemName);
        dictionary.put("assignedDate", this.assignedDate);
        dictionary.put("task", this.task);
        dictionary.put("assigned", this.assigned);
        dictionary.put("position", this.position);
        dictionary.put("assignedDateISO", this.assignedDateISO);


        return dictionary;
    }



}
