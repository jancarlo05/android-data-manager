package com.procuro.apimmdatamanagerlib;

/**
 * Created by jophenmarieweeks on 10/07/2017.
 */

public class Schedule_Buisness extends PimmBaseObject {
    public String schedule_BusinessId ;   // GUID
    public String customerId ;            // CustomerID that this record refers to
    public int dayOfWeek;                // DOW this record refers to, 0 = Sunday
    public int shiftStart0;                // all shift start values are in minutes, measured from midnight on the dayOfWeek
    public int shiftStart1;                // a shift may start on the next calendar day, so for example for DOW=Monday, a shift could start 1:00am Tuesday
    public int shiftStart2;                // this would be 1500
    public int shiftStart3;                // if a shiftStart time is -1, then that shift is not defined for this customer.  All subsequent shifts are ignored.
    public int shiftStart4;
    public int shiftStart5;
    public int dayEnd;                // closing time for the store
}
