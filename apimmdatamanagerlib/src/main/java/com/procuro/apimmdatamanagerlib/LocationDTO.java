package com.procuro.apimmdatamanagerlib;

import org.json.JSONObject;

/**
 * Created by jophenmarieweeks on 06/07/2017.
 */

public class LocationDTO extends PimmBaseObject {

    public BorderCrossings borderXing;

    @Override
    public void setValueForKey(Object value, String key) {
        if (key.equalsIgnoreCase("borderXing")) {
            BorderCrossings borderCrossings = new BorderCrossings();
            JSONObject jsonObject = (JSONObject) value;
            borderCrossings.readFromJSONObject(jsonObject);
            this.borderXing = borderCrossings;

        } else {
            super.setValueForKey(value, key);
        }
    }

}
