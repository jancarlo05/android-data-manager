package com.procuro.apimmdatamanagerlib;

import org.json.JSONObject;

/**
 * Created by jophenmarieweeks on 23/02/2018.
 */

public class StoreDTO extends PimmBaseObject {

   public String siteId;
   public String siteName;
   public String owner;
   public String region;
   public String siteclass;
   public boolean testStore;

    public SiteContact siteContact;
    public AddressComponents address;

    @Override
    public void setValueForKey(Object value, String key) {

        if (key.equalsIgnoreCase("siteContact")) {
            SiteContact result = new SiteContact();
            JSONObject jsonObject = (JSONObject) value;
            result.readFromJSONObject(jsonObject);
            this.siteContact = result;
        }else if (key.equalsIgnoreCase("address")) {
            AddressComponents result = new AddressComponents();
            JSONObject jsonObject = (JSONObject) value;
            result.readFromJSONObject(jsonObject);
            this.address = result;
        } else {
            super.setValueForKey(value, key);
        }
    }


}
