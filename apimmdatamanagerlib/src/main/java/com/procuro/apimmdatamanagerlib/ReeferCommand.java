package com.procuro.apimmdatamanagerlib;

import java.util.Date;

/**
 * Created by jophenmarieweeks on 12/07/2017.
 */

public class ReeferCommand extends PimmBaseObject {

    public enum ReeferCommandResolutionEnum {

        ReeferCommandResolution_Pending(0),
        ReeferCommandResolution_CompletedSuccessfully(1),
        ReeferCommandResolution_CompletedWithError(2),
        ReeferCommandResolution_Timeout(3),
        ReeferCommandResolution_Cancelled(4);

        private int value;
        private ReeferCommandResolutionEnum(int value)
        {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }

        public static ReeferCommandResolutionEnum setIntValue (int i) {
            for (ReeferCommandResolutionEnum type : ReeferCommandResolutionEnum.values()) {
                if (type.value == i) { return type; }
            }
            return ReeferCommandResolution_Pending;
        }
    }

    public String reeferCommandId;
    public String userId;
    public String cancelUserId;
    public String deviceId;
    public int compartment;
    public int command;
    public Date requestTime;
    public Date executionTime;
    public Date ackTime;
    public Date verificationTime;
    public Date cancellationTime;
    public ReeferCommandResolutionEnum resolution;
    public int error;
    public double setPoint;
    public int mode;
    public int powerSetting;
    public int defrost;
    public int alarmCode;
    public String passThrough;

    @Override
    public void setValueForKey(Object value, String key) {

        if(key.equalsIgnoreCase("resolution")) {
            if (!value.equals(null)) {
                ReeferCommandResolutionEnum reeferCommandResolutionEnum = ReeferCommandResolutionEnum.setIntValue((int) value);
                this.resolution = reeferCommandResolutionEnum;
            }

        } else {
            super.setValueForKey(value, key);
        }
    }
}
