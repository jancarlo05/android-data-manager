package com.procuro.apimmdatamanagerlib;

/**
 * Created by jophenmarieweeks on 07/07/2017.
 */

public class RouteShipmentProduct extends PimmBaseObject {

    public String routeShipmentProductID;
    public String DCID;
    public String customerID;
    public String customerProductID;
    public ProductCompartmentEnum.ProductCompartmentsEnum compartment; //    public ProductCompartmentEnum  compartment;
    public ProductCategoryEnum.ProductCategorysEnum category; //    public ProductCategoryEnum  category;

    public int productClass;
    public String shortDescription;
    public String longDescription;
    public boolean valid;
    public String customerProductAltID; // 14 digit GTIN number (barcode) associated with the item


    @Override
    public void setValueForKey(Object value, String key) {

        if(key.equalsIgnoreCase("compartment")) {
            if (!value.equals(null)) {
                ProductCompartmentEnum.ProductCompartmentsEnum productCompartments = ProductCompartmentEnum.ProductCompartmentsEnum.setIntValue((int) value);
                this.compartment = productCompartments;

            }
        }else if(key.equalsIgnoreCase("category")) {
            if (!value.equals(null)) {
                ProductCategoryEnum.ProductCategorysEnum productCategorysEnum = ProductCategoryEnum.ProductCategorysEnum.setIntValue((int) value);
                this.category = productCategorysEnum;

            }

        } else {
            super.setValueForKey(value, key);
        }
    }
}
