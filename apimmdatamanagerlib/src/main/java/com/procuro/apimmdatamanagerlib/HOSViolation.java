package com.procuro.apimmdatamanagerlib;

import java.util.Date;

/**
 * Created by jophenmarieweeks on 06/07/2017.
 */

public class HOSViolation extends PimmBaseObject {

    public String hosViolationID;
    public String deliveryId;
    public String userID;
    public Date violationStart;
    public HOSViolationTypeEnum.HOSViolationTypesEnum violationType;

    public Date creationTimeUTC;
    public Date updateTimeUTC;
    public Date violationTimeUTC;
    public HOSViolationTypeEnum.HOSViolationStatus status;


    @Override
    public void setValueForKey(Object value, String key) {

        if(key.equalsIgnoreCase("violationType")) {
            if (!value.equals(null)) {
                HOSViolationTypeEnum.HOSViolationTypesEnum hosViolationTypesEnum = HOSViolationTypeEnum.HOSViolationTypesEnum.setIntValue((int) value);
                this.violationType = hosViolationTypesEnum;
            }
        } else if(key.equalsIgnoreCase("violationType")) {
            if (!value.equals(null)) {
                HOSViolationTypeEnum.HOSViolationStatus violationStatus = HOSViolationTypeEnum.HOSViolationStatus.setIntValue((int) value);
                this.status = violationStatus;
            }

        } else {
            super.setValueForKey(value, key);
        }
    }


}
