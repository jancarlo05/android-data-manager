package com.procuro.apimmdatamanagerlib;

import java.util.Date;

public class DayOperationSchedule extends PimmBaseObject {


    public boolean isActive;
    public String day;
    public Date openTime;
    public Date closeTime;


    @Override
    public void setValueForKey(Object value, String key) {
        super.setValueForKey(value, key);

        if (key.equalsIgnoreCase("closeTime")){
            if (value!=null){
                if (!value.toString().equalsIgnoreCase("null")){
                    this.closeTime = JSONDate.convertJSONDateToNSDate(value.toString());
                }
            }
        }

        if (key.equalsIgnoreCase("openTime")){
            if (value!=null){
                if (!value.toString().equalsIgnoreCase("null")){
                    this.openTime = JSONDate.convertJSONDateToNSDate(value.toString());
                }
            }
        }
    }


}
