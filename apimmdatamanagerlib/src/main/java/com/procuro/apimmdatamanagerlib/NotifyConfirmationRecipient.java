package com.procuro.apimmdatamanagerlib;

import java.util.Date;

/**
 * Created by jophenmarieweeks on 11/07/2017.
 */

public class NotifyConfirmationRecipient extends PimmBaseObject {

    public String username;
    public String sendTo;
    public Date sendDate;


}
