package com.procuro.apimmdatamanagerlib;

import org.json.JSONObject;

import java.util.HashMap;

public class ApplicationReport extends PimmBaseObject {

    public String ApplicationReportsID;
    public String Application;
    public String Perspective;
    public String Filter;
    public int Selector;
    public int Order;
    public String ReportName;
    public String UrlTemplate;
    public boolean DisplayInShell;
    public String ReportLevel;
    public String Category;
    public boolean CategoryOrder;
    public int ReportType;
    public boolean IsActive;


    @Override
    public void setValueForKey(Object value, String key) {
        super.setValueForKey(value, key);
    }

    public HashMap<String,Object> dictionaryWithValuesForKeys() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();

        dictionary.put("ApplicationReportsID",this.ApplicationReportsID);
        dictionary.put("application",this.Application);
        dictionary.put("Perspective",this.Perspective);
        dictionary.put("Filter",this.Filter);
        dictionary.put("Selector",this.Selector);
        dictionary.put("Order",this.Order);
        dictionary.put("ReportName",this.ReportName);
        dictionary.put("UrlTemplate",this.UrlTemplate);
        dictionary.put("DisplayInShell",this.DisplayInShell);
        dictionary.put("ReportLevel",this.ReportLevel);
        dictionary.put("Category",this.Category);
        dictionary.put("CategoryOrder",this.CategoryOrder);
        dictionary.put("IsActive",this.IsActive);
        dictionary.put("ReportType",this.ReportType);

        return dictionary;
    }


}
