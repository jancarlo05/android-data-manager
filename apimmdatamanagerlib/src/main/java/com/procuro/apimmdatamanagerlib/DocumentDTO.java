package com.procuro.apimmdatamanagerlib;

import java.util.Date;
import java.util.HashMap;


public class DocumentDTO extends PimmBaseObject {
    public String documentId;
    public String customerId;
    public String userId;
    public String name;
    public String userName;
    public String docType ;
    public int version;
    public Date creationDate;
    public String refx;
    public PimmRefTypeEnum.PimmRefTypesEnum refType;
    public String oid;
    public String url;

   public String category;
   public String appString;
   public String refString;

    @Override
    public void setValueForKey(Object value, String key) {

        if(key.equalsIgnoreCase("refType")) {
            if (!value.equals(null)) {
                PimmRefTypeEnum.PimmRefTypesEnum refTypesEnum = PimmRefTypeEnum.PimmRefTypesEnum.setIntValue((int) value);
                this.refType = refTypesEnum;
            }
        } else {
            super.setValueForKey(value, key);
        }
    }


    public HashMap<String,Object> dictionaryWithValuesForKeys() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();

        dictionary.put("documentId", this.documentId);
        dictionary.put("customerId", this.customerId);
        dictionary.put("userId", this.userId);
        dictionary.put("name", this.name);
        dictionary.put("userName", this.userName);
        dictionary.put("docType", this.docType);
        dictionary.put("version", this.version);
        dictionary.put("refx", this.refx);
        dictionary.put("refType", this.refType);
        dictionary.put("oid", this.oid);
        dictionary.put("url", this.url);
        dictionary.put("category", this.category);
        dictionary.put("appString", this.appString);
        dictionary.put("refString", this.refString);

        return dictionary;
    }



}
