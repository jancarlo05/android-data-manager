package com.procuro.apimmdatamanagerlib;

/**
 * Created by jophenmarieweeks on 06/07/2017.
 */

public class PimmMessageStatusEnum extends PimmBaseObject {

    //public enum PimmMessageStatusEnum // duplicate class error
    public enum PimmMessagesStatusEnum
    {
        PimmMessageStatus_Undefined(-1),
        PimmMessageStatus_Sent(0),
        PimmMessageStatus_Delivered(1),
        PimmMessageStatus_Read(2),
        PimmMessageStatus_Acked(3),
        PimmMessageStatus_Deleted(4),
        PimmMessageStatus_Cancelled(5),
        PimmMessageStatusEnum_Queued(6);

        private int value;
        private PimmMessagesStatusEnum(int value)
        {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }

        public static PimmMessagesStatusEnum setIntValue (int i) {
            for (PimmMessagesStatusEnum type : PimmMessagesStatusEnum.values()) {
                if (type.value == i) { return type; }
            }
            return PimmMessageStatus_Undefined;
        }

    }



   public enum PimmMessageRolePartEnum
    {
        RolePart_Sender(0),
        RolePart_Receiver(1),
        RolePart_BothSenderAndReceiver(2);

        private int value;
        private PimmMessageRolePartEnum(int value)
        {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }

        public static PimmMessageRolePartEnum setIntValue (int i) {
            for (PimmMessageRolePartEnum type : PimmMessageRolePartEnum.values()) {
                if (type.value == i) { return type; }
            }
            return RolePart_Sender;
        }
    }

    public PimmMessagesStatusEnum pimmMessagesStatusEnum;
    public PimmMessageRolePartEnum pimmMessageRolePartEnum;



    @Override
    public void setValueForKey(Object value, String key) {

        if(key.equalsIgnoreCase("pimmMessagesStatusEnum")) {
            if (!value.equals(null)) {
                PimmMessagesStatusEnum statusEnum = PimmMessagesStatusEnum.setIntValue((int) value);
                this.pimmMessagesStatusEnum = statusEnum;
            }

        }else if(key.equalsIgnoreCase("pimmMessageRolePartEnum")){
            if (!value.equals(null)) {
                PimmMessageRolePartEnum rolePartEnum = PimmMessageRolePartEnum.setIntValue((int) value);
                this.pimmMessageRolePartEnum = rolePartEnum;
            }


        } else {
            super.setValueForKey(value, key);
        }
    }
}
