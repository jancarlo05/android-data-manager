package com.procuro.apimmdatamanagerlib;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.HashMap;

public class SMSDailyOpsPlan extends  PimmBaseObject {

    public String verifiedBy;
    public String formID;
    public String deviceType;
    public double version;
    public Date startDateISO;
    public Date verifiedDateISO;
    public Date endDateISO;
    public Date endDate;
    public Date startDate;
    public Date verifiedDate;
    public Date creationDateISO;
    public SMSDailyOpsPlanCustomerSatisfacation customerSatisfaction;
    public SMSDailyOpsPlanCommunicationNotes communicationNotes;
    public SMSDailyOpsPlanForcastingAndStaffing forecastingAndStaffing;
    public SMSDailyOpsPlanKeyDriversAndPlay keyDriverAndPlays;
    public SMSDailyOpsPlanSpeedOfService speedOfService;

    @Override
    public void setValueForKey(Object value, String key) {
        super.setValueForKey(value, key);

        if (key.equalsIgnoreCase("verifiedBy")){
            if (value!=null){
                if (!value.toString().equalsIgnoreCase("null")){
                    this.verifiedBy = (String)value.toString();
                }
            }

        }
        if (key.equalsIgnoreCase("deviceType")){
            if (value!=null){
                if (!value.toString().equalsIgnoreCase("null")){
                    this.deviceType = (String)value.toString();
                }
            }

        }
        if (key.equalsIgnoreCase("version")){
            if (value!=null){
                if (!value.toString().equalsIgnoreCase("null")){
                    this.version = Double.parseDouble(value.toString());
                }
            }

        }
        if (key.equalsIgnoreCase("speedOfService")){
            SMSDailyOpsPlanSpeedOfService speedOfService = new SMSDailyOpsPlanSpeedOfService();
            if (value!=null){
                if (!value.toString().equalsIgnoreCase("null")){
                    JSONObject jsonObject = null;
                    try {
                        jsonObject = (JSONObject) new JSONObject(value.toString());
                        speedOfService.readFromJSONObject(jsonObject);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }
            this.speedOfService = speedOfService;
        }

        if (key.equalsIgnoreCase("communicationNotes")){
            SMSDailyOpsPlanCommunicationNotes communicationNotes = new SMSDailyOpsPlanCommunicationNotes();
            if (value!=null){
                if (!value.toString().equalsIgnoreCase("null")){
                    JSONObject jsonObject = null;
                    try {
                        jsonObject = (JSONObject) new JSONObject(value.toString());
                        communicationNotes.readFromJSONObject(jsonObject);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }
            this.communicationNotes = communicationNotes;
        }

        if (key.equalsIgnoreCase("forecastingAndStaffing")){
            SMSDailyOpsPlanForcastingAndStaffing forecastingAndStaffing = new SMSDailyOpsPlanForcastingAndStaffing();
            if (value!=null){
                if (!value.toString().equalsIgnoreCase("null")){
                    JSONObject jsonObject = (JSONObject) value;
                    forecastingAndStaffing.readFromJSONObject(jsonObject);
                }
            }
            this.forecastingAndStaffing = forecastingAndStaffing;

        }
        if (key.equalsIgnoreCase("endDateISO")){
            if (value!=null){
                this.endDateISO = JSONDate.convertJSONDateToNSDate(value.toString());
            }
        }
        if (key.equalsIgnoreCase("endDate")){
            if (value!=null){
                this.endDate = JSONDate.convertJSONDateToNSDate(value.toString());
            }
        }
        if (key.equalsIgnoreCase("startDateISO")){
            if (value!=null){
                this.startDateISO = JSONDate.convertJSONDateToNSDate(value.toString());
            }
        }
        if (key.equalsIgnoreCase("verifiedDateISO")){
            if (value!=null){
                this.verifiedDateISO = JSONDate.convertJSONDateToNSDate(value.toString());
            }
        }
        if (key.equalsIgnoreCase("customerSatisfaction")){
            SMSDailyOpsPlanCustomerSatisfacation customerSatisfaction = new SMSDailyOpsPlanCustomerSatisfacation();
            if (value!=null){
                if (!value.toString().equalsIgnoreCase("null")){
                    JSONObject jsonObject = (JSONObject) value;
                    customerSatisfaction.readFromJSONObject(jsonObject);
                }
            }
            this.customerSatisfaction = customerSatisfaction;
        }
        if (key.equalsIgnoreCase("formID")){
            if (value!=null){
                if (!value.toString().equalsIgnoreCase("null")){
                    this.formID = (String)value.toString();
                }
            }

        }
        if (key.equalsIgnoreCase("keyDriverAndPlays")){
            SMSDailyOpsPlanKeyDriversAndPlay keyDriverAndPlays = new SMSDailyOpsPlanKeyDriversAndPlay();
            if (value!=null){
                if (!value.toString().equalsIgnoreCase("null")){
                    JSONObject jsonObject = (JSONObject) value;
                    keyDriverAndPlays.readFromJSONObject(jsonObject);
                }
            }
            this.keyDriverAndPlays = keyDriverAndPlays;
        }
        if (key.equalsIgnoreCase("startDate")){
            if (value!=null){
                this.startDate = JSONDate.convertJSONDateToNSDate(value.toString());
            }
        }
        if (key.equalsIgnoreCase("verifiedDate")){
            if (value!=null){
                this.verifiedDate = JSONDate.convertJSONDateToNSDate(value.toString());
            }
        }
        if (key.equalsIgnoreCase("creationDateISO")){
            if (value!=null){
                this.creationDateISO = JSONDate.convertJSONDateToNSDate(value.toString());
            }
        }


    }


    public HashMap<String,Object> dictionaryWithValuesForKeys() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();

        dictionary.put("verifiedBy", this.verifiedBy);
        dictionary.put("version", this.version);
        dictionary.put("communicationNotes", this.communicationNotes);
        dictionary.put("forecastingAndStaffing", this.forecastingAndStaffing);
        dictionary.put("endDateISO", this.endDateISO);
        dictionary.put("endDate", this.endDate);
        dictionary.put("startDateISO", this.startDateISO);
        dictionary.put("verifiedDateISO", this.verifiedDateISO);
        dictionary.put("customerSatisfaction", this.customerSatisfaction);
        dictionary.put("formID", this.formID);
        dictionary.put("keyDriverAndPlays", this.keyDriverAndPlays);
        dictionary.put("startDate", this.startDate);
        dictionary.put("verifiedDate", this.verifiedDate);
        dictionary.put("creationDateISO", this.creationDateISO);
        dictionary.put("deviceType", this.deviceType);
        dictionary.put("speedOfService", this.speedOfService);



        return dictionary;
    }
}
