package com.procuro.apimmdatamanagerlib;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by jophenmarieweeks on 10/07/2017.
 */

public class SdrGis extends PimmBaseObject {
    public ArrayList<SdrGisSite> Sites; // List of SdrGISSite
    public ArrayList<SdrStop> Stops; //List of SdrStop objects

    //List of SdrPointOfInterest objects
    public ArrayList<SdrPointOfInterest> PointsOfInterest;
    public ArrayList<SdrSegment> Segments;
    public ArrayList<RouteShipmentStopDNDBEvent> DNDBEvents;
    public ArrayList<RouteShipmentLoadingData> RouteShipmentLoadingData;
    public ArrayList<SdrDCReturn> DCReturns;

    public double UnauthorizedCount;
    public double UnscheduledCount;
    public double UnscheduledTotal;

    public ArrayList<RouteStatusTemperatureAnalysis> RouteStatusTemperatureAnalysisList;
    public ArrayList<SdrGisSite.BillToSite> BillToSites;

    public ReeferData ReeferData;

    public ArrayList<ReeferAlarm> ReeferAlarms;

    public ArrayList<RouteShuttleDTO> RouteShuttles;

    @Override
    public void setValueForKey(Object value, String key) {
        if (key.equalsIgnoreCase("Sites")) {
            if (this.Sites == null)
                this.Sites = new ArrayList<SdrGisSite>();

            JSONArray arrData = (JSONArray) value;

            for (int i = 0; i < arrData.length(); i++) {
                try {
                    SdrGisSite sdrGisSite = new SdrGisSite();
                    JSONObject jsonObject = (JSONObject) arrData.get(i);
                    sdrGisSite.readFromJSONObject(jsonObject);

                    this.Sites.add(sdrGisSite);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } else if (key.equalsIgnoreCase("Stops")) {
            if (this.Stops == null)
                this.Stops = new ArrayList<SdrStop>();
            if (!value.equals(null)) {

                if (value instanceof JSONArray) {
                    JSONArray arrData = (JSONArray) value;

                    for (int i = 0; i < arrData.length(); i++) {
                        try {
                            SdrStop sdrStop = new SdrStop();
                            JSONObject jsonObject = (JSONObject) arrData.get(i);
                            sdrStop.readFromJSONObject(jsonObject);

                            this.Stops.add(sdrStop);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        } else if (key.equalsIgnoreCase("PointsOfInterest")) {
            if (this.PointsOfInterest == null)
                this.PointsOfInterest = new ArrayList<SdrPointOfInterest>();
            if (!value.equals(null)) {

                if (value instanceof JSONArray) {

                    JSONArray arrData = (JSONArray) value;

                    for (int i = 0; i < arrData.length(); i++) {
                        try {
                            SdrPointOfInterest sdrPointOfInterest = new SdrPointOfInterest();
                            JSONObject jsonObject = (JSONObject) arrData.get(i);
                            sdrPointOfInterest.readFromJSONObject(jsonObject);

                            this.PointsOfInterest.add(sdrPointOfInterest);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        } else if (key.equalsIgnoreCase("Segments")) {
            if (this.Segments == null)
                this.Segments = new ArrayList<SdrSegment>();
            if (!value.equals(null)) {

                if (value instanceof JSONArray) {

                    JSONArray arrData = (JSONArray) value;

                    for (int i = 0; i < arrData.length(); i++) {
                        try {
                            SdrSegment sdrSegment = new SdrSegment();
                            JSONObject jsonObject = (JSONObject) arrData.get(i);
                            sdrSegment.readFromJSONObject(jsonObject);

                            this.Segments.add(sdrSegment);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        } else if (key.equalsIgnoreCase("DNDBEvents")) {
            if (this.DNDBEvents == null)
                this.DNDBEvents = new ArrayList<RouteShipmentStopDNDBEvent>();

            JSONArray arrData = (JSONArray) value;

            for (int i = 0; i < arrData.length(); i++) {
                try {
                    RouteShipmentStopDNDBEvent routeShipmentStopDNDBEvent = new RouteShipmentStopDNDBEvent();
                    JSONObject jsonObject = (JSONObject) arrData.get(i);
                    routeShipmentStopDNDBEvent.readFromJSONObject(jsonObject);

                    this.DNDBEvents.add(routeShipmentStopDNDBEvent);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } else if (key.equalsIgnoreCase("RouteShipmentLoadingData")) {
            if (this.RouteShipmentLoadingData == null)
                this.RouteShipmentLoadingData = new ArrayList<RouteShipmentLoadingData>();

            JSONArray arrData = (JSONArray) value;

            for (int i = 0; i < arrData.length(); i++) {
                try {
                    RouteShipmentLoadingData shipmentLoadingData = new RouteShipmentLoadingData();
                    JSONObject jsonObject = (JSONObject) arrData.get(i);
                    shipmentLoadingData.readFromJSONObject(jsonObject);

                    this.RouteShipmentLoadingData.add(shipmentLoadingData);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } else if (key.equalsIgnoreCase("DCReturns")) {
            if (this.DCReturns == null)
                this.DCReturns = new ArrayList<SdrDCReturn>();

            JSONArray arrData = (JSONArray) value;

            for (int i = 0; i < arrData.length(); i++) {
                try {
                    SdrDCReturn sdrDCReturn = new SdrDCReturn();
                    JSONObject jsonObject = (JSONObject) arrData.get(i);
                    sdrDCReturn.readFromJSONObject(jsonObject);

                    this.DCReturns.add(sdrDCReturn);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } else if (key.equalsIgnoreCase("RouteStatusTemperatureAnalysisList")) {
            if (this.RouteStatusTemperatureAnalysisList == null)
                this.RouteStatusTemperatureAnalysisList = new ArrayList<RouteStatusTemperatureAnalysis>();

            JSONArray arrData = (JSONArray) value;

            for (int i = 0; i < arrData.length(); i++) {
                try {
                    RouteStatusTemperatureAnalysis routeStatusTemperatureAnalysis = new RouteStatusTemperatureAnalysis();
                    JSONObject jsonObject = (JSONObject) arrData.get(i);
                    routeStatusTemperatureAnalysis.readFromJSONObject(jsonObject);

                    this.RouteStatusTemperatureAnalysisList.add(routeStatusTemperatureAnalysis);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } else if (key.equalsIgnoreCase("BillToSites")) {
            if (this.BillToSites == null)
                this.BillToSites = new ArrayList<SdrGisSite.BillToSite>();

            JSONArray arrData = (JSONArray) value;

            for (int i = 0; i < arrData.length(); i++) {
                try {
                    SdrGisSite.BillToSite billToSite = new SdrGisSite.BillToSite();
                    JSONObject jsonObject = (JSONObject) arrData.get(i);
                    billToSite.readFromJSONObject(jsonObject);

                    this.BillToSites.add(billToSite);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } else if (key.equalsIgnoreCase("ReeferAlarms")) {
            if (this.ReeferAlarms == null)
                this.ReeferAlarms = new ArrayList<ReeferAlarm>();

            JSONArray arrData = (JSONArray) value;

            for (int i = 0; i < arrData.length(); i++) {
                try {
                    ReeferAlarm reeferAlarm = new ReeferAlarm();
                    JSONObject jsonObject = (JSONObject) arrData.get(i);
                    reeferAlarm.readFromJSONObject(jsonObject);

                    this.ReeferAlarms.add(reeferAlarm);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } else if (key.equalsIgnoreCase("RouteShuttles")) {
            if (this.RouteShuttles == null)
                this.RouteShuttles = new ArrayList<RouteShuttleDTO>();

            JSONArray arrData = (JSONArray) value;

            for (int i = 0; i < arrData.length(); i++) {
                try {
                    RouteShuttleDTO routeShuttleDTO = new RouteShuttleDTO();
                    JSONObject jsonObject = (JSONObject) arrData.get(i);
                    routeShuttleDTO.readFromJSONObject(jsonObject);

                    this.RouteShuttles.add(routeShuttleDTO);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } else if (key.equalsIgnoreCase("ReeferData")) {
            if (value != null) {
                if (value instanceof JSONObject) {
                    ReeferData reeferData = new ReeferData();
                    JSONObject jsonObject = (JSONObject) value;
                    reeferData.readFromJSONObject(jsonObject);
                    this.ReeferData = reeferData;
                }
            }
        } else {
            super.setValueForKey(value, key);
        }
    }


    public HashMap<String, Object> dictionaryWithValuesForKeys() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();

        if (this.Sites != null && this.Sites.size() > 0) {

            ArrayList<HashMap<String, Object>> SitesList = new ArrayList<>();

            for(SdrGisSite siteObject : this.Sites) {

                HashMap<String, Object> siteObjectDictionary = siteObject.dictionaryWithValuesForKeys();
                SitesList.add(siteObjectDictionary);

            }

            dictionary.put("Sites", SitesList);

        }else{
            dictionary.put("Sites", null);
        }

        if (this.Stops != null && this.Stops.size() > 0) {

            ArrayList<HashMap<String, Object>> stopsArray = new ArrayList<>();

            for(SdrStop stopObject : this.Stops) {

                HashMap<String, Object> stopObjectDictionary = stopObject.dictionaryWithValuesForKeys();
                stopsArray.add(stopObjectDictionary);

            }

            dictionary.put("Stops", stopsArray);

        }else{
            dictionary.put("Stops", null);
        }

        if (this.PointsOfInterest != null && this.PointsOfInterest.size() > 0) {

            ArrayList<HashMap<String, Object>> poiArray = new ArrayList<>();

            for(SdrPointOfInterest poiObject : this.PointsOfInterest) {

                HashMap<String, Object> poiObjectDictionary = poiObject.dictionaryWithValuesForKeys();
                poiArray.add(poiObjectDictionary);

            }

            dictionary.put("PointsOfInterest", poiArray);

        }else{
            dictionary.put("PointsOfInterest", null);
        }


        return dictionary;
    }
}