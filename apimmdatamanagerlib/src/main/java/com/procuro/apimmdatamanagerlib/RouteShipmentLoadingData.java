package com.procuro.apimmdatamanagerlib;

import java.util.Date;

/**
 * Created by jophenmarieweeks on 30/08/2017.
 */

public class RouteShipmentLoadingData extends PimmBaseObject {

    public String RouteShipmentLoadingDataId;
    public String DeliveryId;
    public String DeviceId;

    public CompartmentDTO.CompartmentTemperatureProfileEnum C1TemperatureProfile;
    public String C1Loader;
    public Date C1LoadStartTime;
    public Date  C1LoadEndTime;
    public double C1Cases;
    public double C1Cubes;
    public double C1SetPoint;
    public Date  C1SetPointTime;
    public double C1TargetTemp;
    public double C1ActualTemp;
    public Date  C1ActualTime;
    public double C1PrecoolTemp;
    public Date C1PrecoolTime;

    public CompartmentDTO.CompartmentTemperatureProfileEnum C2TemperatureProfile;
    public String C2Loader;
    public Date  C2LoadStartTime;
    public Date  C2LoadEndTime;
    public double   C2Cases;
    public double   C2Cubes;
    public double   C2SetPoint;
    public Date  C2SetPointTime;
    public double   C2TargetTemp;
    public double   C2ActualTemp;
    public Date  C2ActualTime;
    public double   C2PrecoolTemp;
    public Date  C2PrecoolTime;

    public CompartmentDTO.CompartmentTemperatureProfileEnum C3TemperatureProfile;
    public String C3Loader;
    public Date C3LoadStartTime;
    public Date C3LoadEndTime;
    public double C3Cases;
    public double C3Cubes;
    public double C3SetPoint;
    public Date  C3SetPointTime;
    public double C3TargetTemp;
    public double C3ActualTemp;
    public Date  C3ActualTime;
    public double   C3PrecoolTemp;
    public Date  C3PrecoolTime;


    @Override
    public void setValueForKey(Object value, String key) {

        if (key.equalsIgnoreCase("C1TemperatureProfile")) {

            if (!value.equals(null)) {
                CompartmentDTO.CompartmentTemperatureProfileEnum compartmentTemperatureProfileEnum = CompartmentDTO.CompartmentTemperatureProfileEnum.setIntValue((int) value);
                this.C1TemperatureProfile = compartmentTemperatureProfileEnum;
            }
        }else if (key.equalsIgnoreCase("C2TemperatureProfile")) {

            if (!value.equals(null)) {
                CompartmentDTO.CompartmentTemperatureProfileEnum compartmentTemperatureProfileEnum = CompartmentDTO.CompartmentTemperatureProfileEnum.setIntValue((int) value);
                this.C2TemperatureProfile = compartmentTemperatureProfileEnum;
            }

        }else if (key.equalsIgnoreCase("C3TemperatureProfile")) {

            if (!value.equals(null)) {
                CompartmentDTO.CompartmentTemperatureProfileEnum compartmentTemperatureProfileEnum = CompartmentDTO.CompartmentTemperatureProfileEnum.setIntValue((int) value);
                this.C3TemperatureProfile = compartmentTemperatureProfileEnum;
            }

        }else {
            super.setValueForKey(value, key);
        }
    }


}
