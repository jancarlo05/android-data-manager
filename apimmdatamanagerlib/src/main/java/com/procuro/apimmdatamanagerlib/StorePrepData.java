package com.procuro.apimmdatamanagerlib;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

public class StorePrepData extends PimmBaseObject {

    public int totalEmployees;
    public String time;
    public ArrayList<String> positions;

    @Override
    public void setValueForKey(Object value, String key) {
        super.setValueForKey(value, key);

        if (key.equalsIgnoreCase("positions")){
            if (value!=null){
                if (!value.toString().equalsIgnoreCase("null")){
                    ArrayList<String>strings = new ArrayList<>();
                    try {
                        JSONArray array = (JSONArray) value;
                        for (int i = 0; i <array.length() ; i++) {
                            strings.add(array.getString(i));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    this.positions = strings;

                }
            }
        }
    }
}
