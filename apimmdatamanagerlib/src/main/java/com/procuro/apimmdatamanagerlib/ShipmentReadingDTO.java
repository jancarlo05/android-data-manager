package com.procuro.apimmdatamanagerlib;

import java.util.Date;
import java.util.HashMap;

/**
 * Created by jophenmarieweeks on 12/07/2017.
 */

public class ShipmentReadingDTO extends PimmBaseObject {

    public String shipmentReadingId;         // GUID for this reading
    public String shipmentId;                // shipment
    public String address;
    public String objectName;
    public String instanceName;
    public Date timestamp;  // timestamp of reading, in local time for the given perspective
    public String userId;                    // pimm user who created the reading
    public String siteId;                    // optional site where the reading was taken
    public int locationType;              // location type.  types are customer-defined
    public int reading;                   // ordinal for reading at this location for this shipment
    public double value;                        // sensor value


    public HashMap<String,Object> siteDictionary() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();

        dictionary.put("address", this.address);
        dictionary.put("shipmentReadingId", this.shipmentReadingId);
        dictionary.put("shipmentId", this.shipmentId);
        dictionary.put("timestamp", (this.timestamp == null) ? null : DateTimeFormat.convertToJsonDateTime(timestamp));
        dictionary.put("userId", this.userId);
        dictionary.put("siteId", this.siteId);
        dictionary.put("objectName", this.objectName);
        dictionary.put("instanceName", this.instanceName);

        dictionary.put("locationType", this.locationType);
        dictionary.put("reading", this.reading);
        dictionary.put("value", this.value);


        return dictionary;
    }

}
