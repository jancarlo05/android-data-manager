package com.procuro.apimmdatamanagerlib;

public class FSLCompliance extends PimmBaseObject {

   public int onTime;
   public int late;
   public  int incomplete;
   public int partial;
   public int total;
   public  int reviewed;
   public int score;
   public int numberOfSites;

    @Override
    public void setValueForKey(Object value, String key) {
        super.setValueForKey(value, key);
    }
}
