package com.procuro.apimmdatamanagerlib;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by jophenmarieweeks on 06/07/2017.
 */

public class Device extends PimmBaseObject {

    public String deviceID;
    public String deviceName;
    public String siteID;
    public String ipAddress;
    public String os;
    public String Description;
    public String serial;
    public String classColumn;
    public String type;
    public int count;
    public String devclass;

    public ArrayList<PropertyValue> properties;// list of generic properties
    public ArrayList<String> attachedDevices;


    @Override
    public void setValueForKey(Object value, String key) {

        if (key.equalsIgnoreCase("properties")) {
            if (this.properties == null) {
                this.properties = new ArrayList<PropertyValue>();
            }

            if (!value.equals(null)) {

                if (value instanceof JSONArray) {
                    JSONArray arrData = (JSONArray) value;

                    if (arrData != null) {
                        for (int i = 0; i < arrData.length(); i++) {
                            try {
                                PropertyValue propertyValue = new PropertyValue();
                                propertyValue.readFromJSONObject(arrData.getJSONObject(i));

                                this.properties.add(propertyValue);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }

            }else if(key.equalsIgnoreCase("attachedDevices")) {
                    if(this.attachedDevices == null)
                    {
                        this.attachedDevices = new ArrayList<String>();
                    }

                    List<String> attachedDevices = (List<String>) value;
                    for(String attDevice : attachedDevices)
                    {
                        this.attachedDevices.add(attDevice);
                    }
                }
            } else {
                super.setValueForKey(value, key);
            }
        }



    public HashMap<String,Object> dictionaryWithValuesForKeys() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();

        dictionary.put("deviceID", this.deviceID);
        dictionary.put("deviceName", this.deviceName);
        dictionary.put("siteID", this.siteID);
        dictionary.put("ipAddress", this.ipAddress);
        dictionary.put("os", this.os);
        dictionary.put("Description", this.Description);
        dictionary.put("serial", this.serial);
        dictionary.put("classColumn", this.classColumn);
        dictionary.put("type", this.type);
        dictionary.put("devclass", this.devclass);
        dictionary.put("count", this.count);

        dictionary.put("attachedDevices", this.attachedDevices);


        if (this.properties != null && this.properties.size() > 0) {

            ArrayList<HashMap<String, Object>> PropertiesList = new ArrayList<>();

            for(PropertyValue propertiesList : this.properties) {

                HashMap<String, Object> ObjectDictionary = propertiesList.dictionaryWithValuesForKeys();
                PropertiesList.add(ObjectDictionary);

            }

            dictionary.put("properties", PropertiesList);

        }else{
            dictionary.put("properties", null);
        }

        return  dictionary;

    }

}

