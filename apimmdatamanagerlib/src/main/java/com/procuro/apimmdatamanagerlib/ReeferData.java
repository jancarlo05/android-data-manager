package com.procuro.apimmdatamanagerlib;

import java.util.Date;

/**
 * Created by jophenmarieweeks on 12/07/2017.
 */

public class ReeferData extends PimmBaseObject {

   public enum ReeferProgrammingModeEnum
    {
        ReeferProgrammingMode_Undefined(0),
        ReeferProgrammingMode_OFF(1),
        ReeferProgrammingMode_Continuous(2),
        ReeferProgrammingMode_Cycle(3),
        ReeferProgrammingMode_Sleep(4);


        private int value;
        private ReeferProgrammingModeEnum(int value)
        {
            this.value = value;
        }


        public int getValue() {
            return this.value;
        }

        public static ReeferProgrammingModeEnum setIntValue (int i) {
            for (ReeferProgrammingModeEnum type : ReeferProgrammingModeEnum.values()) {
                if (type.value == i) { return type; }
            }
            return ReeferProgrammingMode_Undefined;
        }
    }


    public enum ReeferOperationModeEnum {
        ReeferOperationMode_Off(0),
        ReeferOperationMode_Cool(1),
        ReeferOperationMode_Heat(2),
        ReeferOperationMode_Defrost(3),
        ReeferOperationMode_Null(4),
        ReeferOperationMode_PreTrip(5),
        ReeferOperationMode_Sleep(6),
        ReeferOperationMode_Other(7);

        private int value;
        private ReeferOperationModeEnum(int value)
        {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }

        public static ReeferOperationModeEnum setIntValue (int i) {
            for (ReeferOperationModeEnum type : ReeferOperationModeEnum.values()) {
                if (type.value == i) { return type; }
            }
            return ReeferOperationMode_Off;
        }

    }

    public enum ReeferPowerSourceEnum {
        ReeferPowerSource_Undefined(0),
        ReeferPowerSource_Off(1),
        ReeferPowerSource_Diesel(2),
        ReeferPowerSource_Electric(3);

        private int value;
        private ReeferPowerSourceEnum(int value)
        {
            this.value = value;
        }

        public int getValue() {
            return this.value;
        }

        public static ReeferPowerSourceEnum setIntValue (int i) {
            for (ReeferPowerSourceEnum type : ReeferPowerSourceEnum.values()) {
                if (type.value == i) { return type; }
            }
            return ReeferPowerSource_Undefined;
        }

    }


    public String deviceId;
    public Date startTime;
    public Date endTime;

    public  ReeferProgrammingModeEnum programmingMode;
    //public  ReeferOperationModeEnum operationalMode;
    public  ReeferPowerSourceEnum powerSource;
    public Date powerSourceTime;
    public Date reeferFuel;
    public Date reeferFuelTime;
    public Date reeferBattery;
    public Date reeferBatteryTime;
    public Date dieselHours;
    public Date dieselHoursTime;
    public Date electricHours;
    public Date electricHoursTime;
    public Date totalHours;
    public Date totalHoursTime;
    public Date opModeType;
    public Date c1Discharge;
    public Date c1DischargeTime;
    public Date c1Return;
    public Date c1ReturnTime;
    public Date c1Primary;
    public Date c1PrimaryTime;
    public ReeferOperationModeEnum c1OpMode;
    public Date c1OpModeTime;
    public Date c1SetPoint;
    public Date c2Discharge;
    public Date c2DischargeTime;
    public Date c2Return;
    public Date c2ReturnTime;
    public Date c2Primary;
    public Date c2PrimaryTime;
    public ReeferOperationModeEnum c2OpMode;
    public Date c2OpModeTime;
    public Date c2SetPoint;
    public Date c3Discharge;
    public Date c3DischargeTime;
    public Date c3Return;
    public Date c3ReturnTime;
    public Date c3Primary;
    public Date c3PrimaryTime;
    public ReeferOperationModeEnum c3OpMode;
    public Date c3OpModeTime;
    public Date c3SetPoint;
    public Date ambient;
    public Date ambientTime;



    @Override
    public void setValueForKey(Object value, String key) {

        if (key.equalsIgnoreCase("programmingMode")) {

            if (!value.equals(null)) {
                ReeferProgrammingModeEnum reeferProgrammingModeEnum = ReeferProgrammingModeEnum.setIntValue((int) value);
                this.programmingMode = reeferProgrammingModeEnum;
            }
        }else if (key.equalsIgnoreCase("powerSource")) {

            if (!value.equals(null)) {
                ReeferPowerSourceEnum reeferPowerSourceEnum = ReeferPowerSourceEnum.setIntValue((int) value);
                this.powerSource = reeferPowerSourceEnum;
            }

        }else if (key.equalsIgnoreCase("c1OpMode")) {

            if (!value.equals(null)) {
                ReeferOperationModeEnum reeferOperationModeEnum = ReeferOperationModeEnum.setIntValue((int) value);
                this.c1OpMode = reeferOperationModeEnum;
            }

        }else if (key.equalsIgnoreCase("c2OpMode")) {

            if (!value.equals(null)) {
                ReeferOperationModeEnum reeferOperationModeEnum = ReeferOperationModeEnum.setIntValue((int) value);
                this.c2OpMode = reeferOperationModeEnum;
            }

        }else if (key.equalsIgnoreCase("c3OpMode")) {

            if (!value.equals(null)) {
                ReeferOperationModeEnum reeferOperationModeEnum = ReeferOperationModeEnum.setIntValue((int) value);
                this.c3OpMode = reeferOperationModeEnum;
            }

        }else {
            super.setValueForKey(value, key);
        }
    }


}
