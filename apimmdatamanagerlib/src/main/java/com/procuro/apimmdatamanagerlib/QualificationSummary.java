package com.procuro.apimmdatamanagerlib;

/**
 * Created by jophenmarieweeks on 07/07/2017.
 */

public class QualificationSummary extends PimmBaseObject {

     public String userID; // GUID
     public String userName; // User.email

    /* alarm values
   * 0 = no alarm
   * 1 = yellow alert (in process)
   * 2 = red (missing or expired)
   */
    // highest status state among all qualifications for the user (ignoring deleted qualifications)
        public int alarm;
    // total number of qualifications for this user
        public int count;

}
