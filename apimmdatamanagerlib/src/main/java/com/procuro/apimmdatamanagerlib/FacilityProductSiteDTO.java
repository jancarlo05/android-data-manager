package com.procuro.apimmdatamanagerlib;

/**
 * Created by jophenmarieweeks on 06/07/2017.
 */

public class FacilityProductSiteDTO extends PimmBaseObject {

    public String facilityProductSiteID; // uniqueID
    public String facilityProductID; // product, reference to FacilityProductDTO
    public String siteID;

    public int upperThreshold;  // if not null, product should be at or above this temp
    public int lowerThreshold; // if not null, product should be at or below this temp

    public boolean shift0;  // true if product must be monitored during shift 0
    public boolean shift1;
    public boolean shift2;
    public boolean shift3;
    public boolean shift4;
    public boolean shift5;


}
