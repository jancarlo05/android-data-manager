package com.procuro.apimmdatamanagerlib;

import java.util.HashMap;

/**
 * Created by jophenmarieweeks on 07/07/2017.
 */

public class RouteShipmentDriverDTO2 extends PimmBaseObject {

    public String UserId;
    public String Username;

    public HashMap<String,Object> dictionaryWithValuesForKeys() {
        HashMap<String, Object> dictionary = new HashMap<String, Object>();

        dictionary.put("UserId", this.UserId);
        dictionary.put("Username", this.Username);

        return dictionary;
    }
}
