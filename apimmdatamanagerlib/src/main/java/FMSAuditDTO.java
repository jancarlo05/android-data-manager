import com.procuro.apimmdatamanagerlib.PimmBaseObject;
import com.procuro.apimmdatamanagerlib.RouteShipmentStopDNDBEvent;

import java.util.Date;

public class FMSAuditDTO extends PimmBaseObject {

    public String fmsAuditID;
    public String siteID;
    public String siteName;
    public String createdBy;
    public String createByUserName;
    public String auditor;
    public String auditorUserName;
    public String siteManager;
    public String siteManagerUserName;
    public String pimmFormID;
    public String approvedBy;
    public String approvedByUserName;
    public String followUpAudit;

    public Date creationDate;
    public Date approvedTime;
    public Date windowStart;
    public Date windowEnd;
    public Date scheduledTime;
    public Date startTime;
    public Date endTime;

    public FMSAudit_Type auditType;
    public FMSAudit_Status status;

    public Number grade;

    public boolean requiredApproval;

    public int numberOfQuestions;
    public int numberOfAnswers;
    public int totalPoints;
    public int nonCompliant;

    public boolean allResponsesGiven;
    public boolean criticalIssuesExist;

    @Override
    public void setValueForKey(Object value, String key) {
        super.setValueForKey(value, key);
    }


    public enum FMSAudit_Type {
        FMSAudit_Type_Practice,
        FMSAudit_Type_Internal,
        FMSAudit_External
    }

    public enum FMSAudit_Status {
        FMSAudit_Status_Unscheduled,
        FMSAudit_Status_Scheduled,
        FMSAudit_Status_Started,
        FMSAudit_Status_Completed,
        FMSAudit_Status_PendingApproval,
        FMSAudit_Status_Approved,
        FMSAudit_Status_ApprovalDenied,
        FMSAudit_Status_Cancelled,
        FMSAudit_Status_Late,
    }
}
