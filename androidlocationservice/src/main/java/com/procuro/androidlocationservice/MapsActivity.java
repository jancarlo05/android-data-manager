package com.procuro.androidlocationservice;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;

import android.Manifest;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Bundle;
import android.os.Message;
import android.provider.Settings;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.internal.Constants;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingClient;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import java.util.ArrayList;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    final private String TAG = "LocService";

    private GoogleMap mMap;
    private GoogleApiClient mGoogleApiClient;
    public Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        this.activity = this;

        Log.v(TAG, "onCreate");
    }

    @Override
    protected void onStart() {
        super.onStart();

        Log.v(TAG, "onStart");

        LocationService locationService = LocationService.getInstance();

        locationService.setGeofenceTransitionListener(new LocationService.OnGeofenceTransitionListener() {
//            @Override
//            public void didLocationChanged(Location location) {
//                Log.v(TAG, "didLocationChanged : " + location.getLongitude() + ", "+ location.getLongitude());
////                Toast.makeText(activity, "didLocationChanged : " + location.getLongitude() + ", "+ location.getLongitude(), Toast.LENGTH_LONG).show();
//                showCurrentLocation(location);
//            }

            @Override
            public void didEnterGeofence(Geofence geofence) {
                Log.v(TAG, "triggeringGeofences GEOFENCE_TRANSITION_ENTER: " + geofence.getRequestId());
                Toast.makeText(activity, "GEOFENCE_TRANSITION_ENTER: " + geofence.getRequestId(), Toast.LENGTH_LONG).show();
            }

            @Override
            public void didExitGeofence(Geofence geofence) {
                Log.v(TAG, "triggeringGeofences GEOFENCE_TRANSITION_EXIT: " + geofence.getRequestId());
                Toast.makeText(activity, "GEOFENCE_TRANSITION_EXIT: " + geofence.getRequestId(), Toast.LENGTH_LONG).show();
            }
        });

        Geofence geofence1 = locationService.createGeofence("Royal Duty Free", 14.823774, 120.275971, 50);
        Geofence geofence2 = locationService.createGeofence("House", 14.832874, 120.279116, 50);

        ArrayList<Geofence> geofenceList = new ArrayList<>();
        geofenceList.add(geofence1);
        geofenceList.add(geofence2);

//        locationService.startServiceForActivity(this, geofenceList);
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */

    private void showCurrentLocation(Location location) {
        /*
         * Get the best and most recent location of the device, which may be null in rare
         * cases when a location is not available.
         */
        try {
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                    new LatLng(location.getLatitude(),
                            location.getLongitude()), 17.0f));
        } catch(SecurityException e)  {
            Log.e("Exception: %s", e.getMessage());
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        Log.v("LOCATIONSERVICE", "onMapReady");
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
//        LatLng sydney = new LatLng(-34, 151);
//        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));

        if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(true);
    }
}
