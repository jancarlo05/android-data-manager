package com.procuro.androidlocationservice;

import android.Manifest;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.Settings;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingClient;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import java.util.ArrayList;

public class LocationService {

    static final private String TAG = "LocService";
    static private LocationService instance = null;

    private int unscheduledStopTolerance;
    private int geofenceEnterDelay;
    private boolean storeConfirmArrival;
    private boolean postGeofenceEnter;

    private Activity activity;
    private GeofencingClient geofencingClient;
    private Geofence currentGeofence;
    private ArrayList<Geofence> geofenceList;
    private Location lastKnownLocation;
    private FusedLocationProviderClient fusedLocationClient;

    private CountDownTimer storeArrivalCountDownTimer;
    private CountDownTimer unscheduledStopCountDownTimer;

    private OnUserLocationChangeListener userLocationChangeListener;
    private OnGeofenceTransitionListener geofenceTransitionListener;
    private OnUnscheduledStopDetectedListener unscheduledStopDetectedListener;

    public static LocationService getInstance() {
        if(instance == null) {
            instance = new LocationService();
        }
        return instance;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    public void setGeofenceList(ArrayList<Geofence> geofenceList) {
        this.geofenceList = geofenceList;
        this.createGeofencingClient();
    }

    public void startLocationService() {
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(activity);

        LocationManager locationManager =
                (LocationManager)activity.getSystemService(Context.LOCATION_SERVICE);
        final boolean gpsEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        if (!gpsEnabled) {
            Log.v(TAG, "onStart gpsEnabled = false");
            this.enableLocationSettings();
        } else {
            Log.v(TAG, "onStart gpsEnabled = true");
            if (ActivityCompat.checkSelfPermission(this.activity, Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {

                Log.v(TAG, "onStart checkSelfPermission = true");

                startUnscheduledStopTimer();

                fusedLocationClient.getLastLocation()
                        .addOnSuccessListener(activity, new OnSuccessListener<Location>() {
                            @Override
                            public void onSuccess(Location location) {
                                if (location != null) {
                                    lastKnownLocation = location;
                                }
                            }
                        });

                LocationListener listener = new LocationListener() {

                    @Override
                    public void onLocationChanged(final Location location) {
                        Log.v(TAG, "onLocationChanged: " + location.getLatitude() + ", " + location.getLongitude());

                        lastKnownLocation = location;
                        if(unscheduledStopCountDownTimer != null) {
                            unscheduledStopCountDownTimer.cancel();
                        }
                        unscheduledStopCountDownTimer = null;
                        startUnscheduledStopTimer();

                        if(userLocationChangeListener != null) {
                            userLocationChangeListener.onUserLocationChanged(location);
                        }
                    }

                    @Override
                    public void onStatusChanged(String s, int i, Bundle bundle) {
                        Log.v(TAG, "onStatusChanged: " + s + ", " + i + ", " + bundle.toString());
                    }

                    @Override
                    public void onProviderEnabled(String s) {
                        Log.v(TAG, "onProviderEnabled: " + s);
                    }

                    @Override
                    public void onProviderDisabled(String s) {
                        Log.v(TAG, "onProviderDisabled: " + s);
                    }
                };

                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,10000,10,
                        listener);
            } else {
                Log.v(TAG, "onStart checkSelfPermission = false");
            }
        }
    }

    private void enableLocationSettings() {
        Log.v(TAG, "enableLocationSettings");
        Intent settingsIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        this.activity.startActivity(settingsIntent);
    }

    private void createGeofencingClient() {
        Log.v(TAG, "createGeofencingClient");

        geofencingClient = LocationServices.getGeofencingClient(this.activity);

        if(geofencingClient == null) {
            Log.v(TAG, "geofencingClient is NULL");
        }

        try {
            GeofencingRequest geofencingRequest = getGeofencingRequest();
            PendingIntent pendingIntent = getGeofencePendingIntent();

            if(geofencingRequest == null) {
                Log.v(TAG, "geofencingRequest is NULL");
            } else if(pendingIntent == null) {
                Log.v(TAG, "pendingIntent is NULL");
            } else {
                geofencingClient.addGeofences(geofencingRequest, pendingIntent)
                        .addOnSuccessListener(this.activity, new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                Log.v(TAG, "addGeofences onSuccess");
                            }
                        })
                        .addOnFailureListener(this.activity, new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Log.v(TAG, "addGeofences addOnFailureListener : " + e.getMessage());
                            }
                        });
            }
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    public Geofence createGeofence(String requestId, double latitude, double longitude, float radius) {
        Log.v("createGeofence", "requestId : " + requestId);
        Geofence geofence = new Geofence.Builder()
                .setRequestId(requestId)
                .setCircularRegion(latitude, longitude, radius)
                .setExpirationDuration(Geofence.NEVER_EXPIRE)
                .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER |
                        Geofence.GEOFENCE_TRANSITION_EXIT)
                .build();
        return geofence;
    }

    private GeofencingRequest getGeofencingRequest() {
        Log.v(TAG, "getGeofencingRequest");
        GeofencingRequest.Builder builder = new GeofencingRequest.Builder();
        builder.setInitialTrigger(0);

        if(geofenceList != null) {
            builder.addGeofences(geofenceList);
        } else {
            Log.v(TAG, "geofenceList is NULL");
        }

        return builder.build();
    }

    private PendingIntent getGeofencePendingIntent() {
        Log.v(TAG, "getGeofencePendingIntent");
        PendingIntent geofencePendingIntent = null;

        if (geofencePendingIntent != null) {
            return geofencePendingIntent;
        }

        Intent intent = new Intent(this.activity, GeofenceTransitionsIntentService.class);
        geofencePendingIntent = PendingIntent.getService(this.activity, 0, intent, PendingIntent.
                FLAG_UPDATE_CURRENT);
        return geofencePendingIntent;
    }

    public Location getLastKnownLocation() {
        if(lastKnownLocation != null) {
            Log.v(TAG, "getLastKnownLocation: " + lastKnownLocation.getLatitude() + ", " + lastKnownLocation.getLongitude());
            return lastKnownLocation;
        } else {
            return null;
        }
    }

    private void startStoreArrivalTimer() {
        Log.v("LOCATIONSERVICE", "geofenceEnterDelay: " + geofenceEnterDelay);
        if(storeArrivalCountDownTimer == null) {
            storeArrivalCountDownTimer = new CountDownTimer(geofenceEnterDelay * 1000, 1000) {
                @Override
                public void onTick(long l) {
                }

                @Override
                public void onFinish() {
                    if(storeArrivalCountDownTimer != null) {
                        geofenceTransitionListener.didEnterGeofence(currentGeofence);
                    }
                }
            };
            storeArrivalCountDownTimer.start();
        }
    }

    public void stopStoreArrivalTimer() {
        if(storeArrivalCountDownTimer != null) {
            storeArrivalCountDownTimer.cancel();
            storeArrivalCountDownTimer = null;
        }
    }

    public void startUnscheduledStopTimer() {
        if(unscheduledStopCountDownTimer == null) {
            unscheduledStopCountDownTimer = new CountDownTimer(unscheduledStopTolerance * 1000, 1000) {
                @Override
                public void onTick(long l) {
                }
                @Override
                public void onFinish() {
                    if(unscheduledStopDetectedListener != null) {
                        unscheduledStopDetectedListener.onUnscheduledStopDetectedListener(lastKnownLocation);
                    }
                }
            };
            unscheduledStopCountDownTimer.start();
        }
    }

    public void stopUnscheduledStopTimer(){
        unscheduledStopCountDownTimer.cancel();
    }

    public void setUnscheduledStopTolerance(int unscheduledStopTolerance) {
        this.unscheduledStopTolerance = unscheduledStopTolerance * 3;
    }

    public void setGeofenceEnterDelay(int geofenceEnterDelay) {
        this.geofenceEnterDelay = geofenceEnterDelay;
    }

    public void setStoreConfirmArrival(boolean storeConfirmArrival) {
        this.storeConfirmArrival = storeConfirmArrival;
    }

    public void setPostGeofenceEnter(boolean postGeofenceEnter) {
        this.postGeofenceEnter = postGeofenceEnter;
    }

    // LISTENERS

    public interface OnGeofenceTransitionListener {
        void didEnterGeofence(Geofence geofence);
        void didExitGeofence(Geofence geofence);
    }

    public void setGeofenceTransitionListener(OnGeofenceTransitionListener geofenceTransitionListener) {
        this.geofenceTransitionListener = geofenceTransitionListener;
    }

    public OnGeofenceTransitionListener getGeofenceTransitionListener() {
        return this.geofenceTransitionListener;
    }

    public interface OnUserLocationChangeListener {
        void onUserLocationChanged(Location location);
    }

    public void setUserLocationChangeListener(OnUserLocationChangeListener userLocationChangeListener) {
        this.userLocationChangeListener = userLocationChangeListener;
    }

    public OnUserLocationChangeListener getUserLocationChangeListener() {
        return this.userLocationChangeListener;
    }

    public interface OnUnscheduledStopDetectedListener {
        void onUnscheduledStopDetectedListener(Location location);
    }

    public void setOnUnscheduledStopDetectedListener(OnUnscheduledStopDetectedListener unscheduledStopDetectedListener) {
        this.unscheduledStopDetectedListener = unscheduledStopDetectedListener;
    }

    public OnUnscheduledStopDetectedListener getUnscheduledStopDetectedListener() {
        return this.unscheduledStopDetectedListener;
    }
}
