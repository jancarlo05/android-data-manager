package com.procuro.androidlocationservice;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingEvent;

import java.util.List;

public class GeofenceTransitionsIntentService extends IntentService {

    final private  String TAG = "LocService";

    public GeofenceTransitionsIntentService() {
        super("GeofenceTransitionsIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        Log.v(TAG, "onHandleIntent : " + intent.getDataString());

        GeofencingEvent geofencingEvent = GeofencingEvent.fromIntent(intent);
        if (geofencingEvent.hasError()) {
            Log.v(TAG, "geofencingEvent.hasError()");
            return;
        }

        int geofenceTransition = geofencingEvent.getGeofenceTransition();
        List<Geofence> triggeringGeofences = geofencingEvent.getTriggeringGeofences();

        if (geofenceTransition == Geofence.GEOFENCE_TRANSITION_ENTER) {
            if(triggeringGeofences.size() > 0) {
                Geofence geofence = triggeringGeofences.get(0);
                LocationService.getInstance().getGeofenceTransitionListener().didEnterGeofence(geofence);
            }
        } else if(geofenceTransition == Geofence.GEOFENCE_TRANSITION_EXIT) {
            if(triggeringGeofences.size() > 0) {
                Geofence geofence = triggeringGeofences.get(0);
                LocationService.getInstance().getGeofenceTransitionListener().didExitGeofence(geofence);
            }
        } else {
            Log.e(TAG, "triggeringGeofences ERROR");
        }
    }
}
