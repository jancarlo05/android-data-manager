package com.procuro.apimmlibraryproject;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.procuro.apimmdatamanagerlib.*;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    aPimmDataManager dataManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dataManager = aPimmDataManager.getInstance();
        dataManager.initializeWithSPID("wendys");
        dataManager.loginWithUsernameAndPassword("pimmgm@wendys.com", "pimmgm", new OnCompleteListeners.OnLoginCompleteListener() {
            @Override
            public void onLoginComplete(User user, Error error) {

//                dataManager.getSiteWithSiteId(user.defaultSiteID, user.userId, new OnCompleteListeners.OnGetSiteWithSiteIdListener() {
//                    @Override
//                    public void onGetWithSiteId(Site site, Error error) {
//
//                    }
//                });

//                - [ ] getSiteListForLoggedInUser
//                dataManager.getSiteListForLoggedInUser(new OnCompleteListeners.getSiteListForLoggedInUserWithCallbackListener() {
//                    @Override
//                    public void getSiteListForLoggedInUserWithCallback(ArrayList<Site> siteList, Error error) {
//
//                    }
//                });

//                - [ ] getDocumentListForReferenceId
//                dataManager.getSiteWithSiteId("5d74bffe-d474-48af-ae1e-bd741262b67f", user.userId, new OnCompleteListeners.OnGetSiteWithSiteIdListener() {
//                    @Override
//                    public void onGetWithSiteId(Site site, Error error) {
//                        //site.customerID = referenceID; 14c21b76-d27b-4da5-a610-37a14cdbcf44
//
//                    }
//                });

//                getSiteListForLoggedInUser();
//                getDocumentListForReferenceId("14c21b76-d27b-4da5-a610-37a14cdbcf44");
//                getFacilityStatusForSiteClass();
                getInstanceListForSiteId();

//

//                dataManager.getSiteSettingsForSiteId("5d74bffe-d474-48af-ae1e-bd741262b67f", new OnCompleteListeners.getSiteSettingsForSiteIdListener() {
//                    @Override
//                    public void getSiteSettingsForSiteId(SiteSettings siteSettings, Error error) {
//
//                    }
//                });

//                dataManager.getSMSDashboardScoresForSite("5d74bffe-d474-48af-ae1e-bd741262b67f", new OnCompleteListeners.getSMSDashboardWithCallbackListener() {
//                    @Override
//                    public void getSMSDashboardWithCallback(SMSDashboard smsDashboard, Error error) {
//
//                    }
//                });

//                dataManager.getSMSDashboardCustomerCommentsForSite("5d74bffe-d474-48af-ae1e-bd741262b67f", new OnCompleteListeners.getSMSDashboardCustomerCommentsWithCallbackListener() {
//                    @Override
//                    public void getSMSDashboardCustomerCommentsWithCallback(SMSDashboardCustomerComments smsDashboardCustomerComments, Error error) {
//
//                    }
//                });

            }
        });
    }

    public void getSiteListForLoggedInUser(){
        dataManager.getSiteListForLoggedInUser(new OnCompleteListeners.getSiteListForLoggedInUserWithCallbackListener() {
            @Override
            public void getSiteListForLoggedInUserWithCallback(ArrayList<Site> siteList, Error error) {

            }
        });
    }

    public void getDocumentListForReferenceId(String refx){
        dataManager.getDocumentListForReferenceId(refx, new OnCompleteListeners.getDocumentListForReferenceIdCallbackListener() {
            @Override
            public void getDocumentListForReferenceIdCallback(ArrayList<DocumentDTO> documentDTOArrayList, Error error) {

            }
        });
    }

    public void getFacilityStatusForSiteClass(){
        dataManager.getFacilityStatusForSiteClass("", 50, "", new OnCompleteListeners.getFacilityStatusForSiteClassListener() {
            @Override
            public void getFacilityStatusForSiteClass(ArrayList<PimmDevice> pimmDeviceList, Error error) {

            }
        });
    }

    public void getInstanceListForSiteId(){
        dataManager.getInstanceListForSiteId("dbac2665-b61d-40b7-8ba1-81db7198d3a7", new OnCompleteListeners.getInstanceListForSiteIdCallbackListener() {
            @Override
            public void getInstanceListForSiteIdCallback(ArrayList<PimmInstance> pimmInstanceArrayList, Error error) {

            }
        });
    }

}


